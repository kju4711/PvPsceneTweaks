SETLOCAL ENABLEDELAYEDEXPANSION

rem Prepare
CALL ___configureSettings.bat
rem ----

set _BUILD_PATH=@IFA3_RevertChangesToGUI

rem ----
set _KEYFILENAME=%_BUILD_PATH%
set _KEYFILENAME=%_KEYFILENAME:~1%_%_DATE%
rem ----

Echo %_BUILD_PATH%
Echo %_KEYFILENAME%

"%_SIGN_PATH%\DSCreateKey.exe" %_KEYFILENAME%

FOR /R "%_BUILD_PATH%" %%I IN ("*.pbo") DO (

	Echo %%I
	"%_SIGN_PATH%\DSSignFile.exe" "%_KEYFILENAME%.biprivatekey" %%I

)

copy "%_KEYFILENAME%.bikey" "%_BUILD_PATH%\keys\"

move "%_KEYFILENAME%.bikey" "oldKeys\"
move "%_KEYFILENAME%.biprivatekey" "oldKeys\"

exit




