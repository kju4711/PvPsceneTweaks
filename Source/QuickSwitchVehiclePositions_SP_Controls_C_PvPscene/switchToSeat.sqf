// exit when not in vehicle
if (player == (vehicle player)) exitWith {};

private["_vehicle"];

_vehicle = vehicle player;
if (_vehicle isKindOf "Air") exitWith {};

private["_position","_driver","_gunner","_commander","_isPlayerDriver","_isPlayerGunner","_isPlayerCommander","_switchTo"];

_position = _this select 0;

_driver = driver _vehicle;
_gunner = gunner _vehicle;
_commander = commander _vehicle;

_isPlayerDriver = _driver == player;
_isPlayerGunner = _gunner == player;
_isPlayerCommander = _commander == player;

_switchTo = "commander";
switch (_position) do
{
	case "driver":
	{
		if (!(_isPlayerDriver)) then {_switchTo = "driver";};
	};
	case "gunner":
	{
		if (!(_isPlayerGunner)) then {_switchTo = "gunner";};
	};
};

switch (_switchTo) do
{
	case "driver":
	{
		if (isNull _driver) then
		{
			player action ["moveToDriver",_vehicle];
		}
		else
		{
			if (alive _driver) then
			{
				selectPlayer _driver;
			};
		};
	};
	case "gunner":
	{
		if (isNull _gunner) then
		{
			player action ["moveToGunner",_vehicle];
		}
		else
		{
			if (alive _gunner) then
			{
				selectPlayer _gunner;
			};
		};
	};
	case "commander":
	{
		if (isNull _commander) then
		{
			player action ["moveToCommander",_vehicle];
		}
		else
		{
			if (alive _commander) then
			{
				selectPlayer _commander;
			};
		};
	};
};
