﻿#include "KeyCharCodes.hpp"

private["_pressedButton","_pressedShift","_pressedControl","_pressedAlt","_return","_pressedButtonArray"];

// ------------------ Parse Arguments -----------------

_pressedButton = _this select 1;
_pressedShift = _this select 2;
_pressedControl = _this select 3;
_pressedAlt = _this select 4;

_return = false;
_pressedButtonArray = [_pressedButton];//standard button

// ----------------------- Main Function ---------------------

if (true) then
{
	switch (_pressedButton) do
	{
		case KEYSCODE_END_KEY:
		{
			if (_pressedShift || _pressedControl || _pressedAlt) then
			{
				selectPlayer PvPscene_PlayerEntity;
				_return = true;
			};
		};
	};
	if (!(_return)) then
	{
		// check for key actions
		switch (true) do
		{
			// swapGunner
			case (({_x in _pressedButtonArray} count (actionKeys "swapGunner")) > 0):
			{
				if (_pressedShift || _pressedControl || _pressedAlt) then
				{
					["driver"] spawn PvPscene_fnc_switchToSeat;
				}
				else
				{
					["gunner"] spawn PvPscene_fnc_switchToSeat;
				};
				_return = true;
			};
		};
	};
};
_return;