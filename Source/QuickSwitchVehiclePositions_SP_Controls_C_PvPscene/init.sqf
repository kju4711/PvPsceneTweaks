if (isMultiplayer) exitWith {};

PvPscene_PlayerEntity = player;

PvPscene_fnc_keyDownMainDisplay = compile preprocessFileLineNumbers "\QuickSwitchVehiclePositions_SP_Controls_C_PvPscene\keyDownMainDisplay.sqf";
PvPscene_fnc_switchToSeat = compile preprocessFileLineNumbers "\QuickSwitchVehiclePositions_SP_Controls_C_PvPscene\switchToSeat.sqf";

sleep 1;

waitUntil {sleep 0.01; (!(isNull (findDisplay 46)))};

(findDisplay 46) displayAddEventHandler ["KeyDown","_this call PvPscene_fnc_keyDownMainDisplay"];