class CfgPatches
{
	class Optional_HideFromEditor_Civilians_c
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Data_F_Loadorder","A3_Data_F_Curator_Loadorder","A3_Data_F_Kart_Loadorder","A3_Data_F_Bootcamp_Loadorder","A3_Data_F_Heli_Loadorder","A3_Data_F_Mark_Loadorder","A3_Data_F_Exp_A_Loadorder","A3_Data_F_Exp_B_Loadorder","A3_Data_F_Exp_Loadorder","A3_Data_F_Jets_Loadorder","A3_Data_F_Argo_Loadorder","A3_Data_F_Patrol_Loadorder","A3_Data_F_Orange_Loadorder","A3_Data_F_Tacops_Loadorder","A3_Data_F_Tank_Loadorder"};
		version = "2019-03-28";
	};
};
class CfgVehicles
{
	class C_Driver_1_F;
	class C_IDAP_Man_Base_F;
	class C_journalist_F;
	class C_Man_casual_1_F_euro;
	class C_Man_casual_1_F_tanoan;
	class C_Man_casual_2_F_euro;
	class C_Man_casual_2_F_tanoan;
	class C_Man_casual_3_F_euro;
	class C_Man_casual_3_F_tanoan;
	class C_Man_casual_4_F_euro;
	class C_Man_casual_4_F_tanoan;
	class C_Man_casual_5_F_euro;
	class C_Man_casual_5_F_tanoan;
	class C_Man_casual_6_F_euro;
	class C_Man_casual_6_F_tanoan;
	class C_man_polo_1_F_euro;
	class C_man_polo_2_F_euro;
	class C_man_polo_3_F_euro;
	class C_man_polo_4_F_euro;
	class C_man_polo_5_F_euro;
	class C_man_polo_6_F_euro;
	class Civilian_F;
	class UAV_AI_base_F;

	class C_man_1: Civilian_F
	{
		scope = 1;//2
	};
	class C_Soldier_VR_F: C_man_1
	{
		scope = 1;//2
	};
	class C_Protagonist_VR_F: C_man_1
	{
		scope = 1;//2
	};
	class C_UAV_AI_F: UAV_AI_base_F
	{
//		scope = 1;
	};
	class C_IDAP_UAV_AI_F: UAV_AI_base_F
	{
//		scope = 1;
	};
	class C_IDAP_Man_AidWorker_01_F: C_IDAP_Man_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_IDAP_Man_AidWorker_02_F: C_IDAP_Man_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_IDAP_Man_AidWorker_03_F: C_IDAP_Man_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_IDAP_Man_AidWorker_04_F: C_IDAP_Man_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_IDAP_Man_AidWorker_05_F: C_IDAP_Man_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_IDAP_Man_AidWorker_06_F: C_IDAP_Man_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_IDAP_Man_AidWorker_07_F: C_IDAP_Man_AidWorker_03_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_IDAP_Man_AidWorker_08_F: C_IDAP_Man_AidWorker_04_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_IDAP_Man_AidWorker_09_F: C_IDAP_Man_AidWorker_02_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_Man_ConstructionWorker_01_Red_F: C_Driver_1_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_Paramedic_01_base_F: Civilian_F
	{
//		scope = 1;
	};
	class C_Man_Paramedic_01_F: C_Paramedic_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_IDAP_Man_Paramedic_01_F: C_Paramedic_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_IDAP_Pilot_01_F: C_IDAP_Man_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_Journalist_01_War_F: C_journalist_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_Man_UtilityWorker_01_F: Civilian_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_IDAP_Man_EOD_01_F: C_IDAP_Man_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_IDAP_Man_EOD_01_TrainingMines_F: C_IDAP_Man_EOD_01_F
	{
//		scope = 1;
	};
	class C_IDAP_Man_UAV_01_F: C_IDAP_Man_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_IDAP_Man_UAV_06_F: C_IDAP_Man_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_IDAP_Man_UAV_06_medical_F: C_IDAP_Man_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_IDAP_Man_UAV_06_antimine_F: C_IDAP_Man_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_Man_UAV_06_F: Civilian_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_Man_UAV_06_medical_F: Civilian_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_Man_Fisherman_01_F: Civilian_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_Man_Messenger_01_F: Civilian_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_Story_EOD_01_F: C_IDAP_Man_EOD_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_Story_Mechanic_01_F: Civilian_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class CivilianPresence_C_Man_casual_1_F_tanoan: C_Man_casual_1_F_tanoan
	{
//		scope = 1;
	};
	class CivilianPresence_C_Man_casual_2_F_tanoan: C_Man_casual_2_F_tanoan
	{
//		scope = 1;
	};
	class CivilianPresence_C_Man_casual_3_F_tanoan: C_Man_casual_3_F_tanoan
	{
//		scope = 1;
	};
	class CivilianPresence_C_Man_casual_4_F_tanoan: C_Man_casual_4_F_tanoan
	{
//		scope = 1;
	};
	class CivilianPresence_C_Man_casual_5_F_tanoan: C_Man_casual_5_F_tanoan
	{
//		scope = 1;
	};
	class CivilianPresence_C_Man_casual_6_F_tanoan: C_Man_casual_6_F_tanoan
	{
//		scope = 1;
	};
	class CivilianPresence_C_Man_casual_1_F_euro: C_Man_casual_1_F_euro
	{
//		scope = 1;
	};
	class CivilianPresence_C_Man_casual_2_F_euro: C_Man_casual_2_F_euro
	{
//		scope = 1;
	};
	class CivilianPresence_C_Man_casual_3_F_euro: C_Man_casual_3_F_euro
	{
//		scope = 1;
	};
	class CivilianPresence_C_Man_casual_4_F_euro: C_Man_casual_4_F_euro
	{
//		scope = 1;
	};
	class CivilianPresence_C_Man_casual_5_F_euro: C_Man_casual_5_F_euro
	{
//		scope = 1;
	};
	class CivilianPresence_C_Man_casual_6_F_euro: C_Man_casual_6_F_euro
	{
//		scope = 1;
	};
	class CivilianPresence_C_man_polo_1_F_euro: C_man_polo_1_F_euro
	{
//		scope = 1;
	};
	class CivilianPresence_C_man_polo_2_F_euro: C_man_polo_2_F_euro
	{
//		scope = 1;
	};
	class CivilianPresence_C_man_polo_3_F_euro: C_man_polo_3_F_euro
	{
//		scope = 1;
	};
	class CivilianPresence_C_man_polo_4_F_euro: C_man_polo_4_F_euro
	{
//		scope = 1;
	};
	class CivilianPresence_C_man_polo_5_F_euro: C_man_polo_5_F_euro
	{
//		scope = 1;
	};
	class CivilianPresence_C_man_polo_6_F_euro: C_man_polo_6_F_euro
	{
//		scope = 1;
	};
};
