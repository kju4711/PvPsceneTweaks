class CfgPatches
{
	class DisabledDeflectingGrenades_Gameplay_C_PvPscene
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Data_F_Loadorder","A3_Data_F_Curator_Loadorder","A3_Data_F_Kart_Loadorder","A3_Data_F_Bootcamp_Loadorder","A3_Data_F_Heli_Loadorder","A3_Data_F_Mark_Loadorder","A3_Data_F_Exp_A_Loadorder","A3_Data_F_Exp_B_Loadorder","A3_Data_F_Exp_Loadorder","A3_Data_F_Jets_Loadorder","A3_Data_F_Argo_Loadorder","A3_Data_F_Patrol_Loadorder","A3_Data_F_Orange_Loadorder","A3_Data_F_Tacops_Loadorder","A3_Data_F_Tank_Loadorder"};
		version = "2019-03-28";
	};
};
class CfgAmmo
{
	class Default
	{
//		deflecting = 0;
	};
	class GrenadeHand;
	class FlareBase;
	class GrenadeBase;

	class GrenadeCore: Default
	{
		deflecting = 0;//60;
	};
	class FlareCore: GrenadeCore
	{
		deflecting = 0;//60;
	};
	class SmokeShellCore: GrenadeCore
	{
		deflecting = 0;//60;
	};
	class Grenade: Default
	{
		deflecting = 0;//15;
	};
	class mini_Grenade: GrenadeHand
	{
		deflecting = 0;//30;
	};
	class SmokeShell: GrenadeHand
	{
		deflecting = 0;//60;
	};
	class F_40mm_White: FlareBase
	{
		deflecting = 0;//30;
	};
	class F_20mm_White: FlareBase
	{
		deflecting = 0;//30;
	};
	class F_Signal_Green: FlareBase
	{
		deflecting = 0;//30;
	};
	class G_40mm_HE: GrenadeBase
	{
		deflecting = 0;//5;
	};
	class IRStrobeBase: GrenadeCore
	{
		deflecting = 0;//30;
	};
};
