class CfgPatches
{
	class DisabledButtonClickSound_Sounds_C_PvPscene
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Data_F_Loadorder","A3_Data_F_Curator_Loadorder","A3_Data_F_Kart_Loadorder","A3_Data_F_Bootcamp_Loadorder","A3_Data_F_Heli_Loadorder","A3_Data_F_Mark_Loadorder","A3_Data_F_Exp_A_Loadorder","A3_Data_F_Exp_B_Loadorder","A3_Data_F_Exp_Loadorder","A3_Data_F_Jets_Loadorder","A3_Data_F_Argo_Loadorder","A3_Data_F_Patrol_Loadorder","A3_Data_F_Orange_Loadorder","A3_Data_F_Tacops_Loadorder","A3_Data_F_Tank_Loadorder"};
		version = "2019-03-28";
	};
};//DisabledButtonClickSound_Sounds_C_PvPscene

class ctrlDefault;
class ctrlDefaultText;

class RscButton
{
	soundEnter[] = {"",0.1,1};//{"\A3\ui_f\data\sound\RscButton\soundEnter",0.09,1};
	soundPush[] = {"",0.1,1};//{"\A3\ui_f\data\sound\RscButton\soundPush",0.09,1};
	soundClick[] = {"",0.1,1};//{"\A3\ui_f\data\sound\RscButton\soundClick",0.09,1};
	soundEscape[] = {"",0.1,1};//{"\A3\ui_f\data\sound\RscButton\soundEscape",0.09,1};
};
class RscShortcutButton
{
	soundEnter[] = {"",0.1,1};//{"\A3\ui_f\data\sound\RscButton\soundEnter",0.09,1};
	soundPush[] = {"",0.1,1};//{"\A3\ui_f\data\sound\RscButton\soundPush",0.09,1};
	soundClick[] = {"",0.1,1};//{"\A3\ui_f\data\sound\RscButton\soundClick",0.09,1};
	soundEscape[] = {"",0.1,1};//{"\A3\ui_f\data\sound\RscButton\soundEscape",0.09,1};
};
class RscCombo
{
	soundSelect[] = {"",0.1,1};//{"\A3\ui_f\data\sound\RscCombo\soundSelect",0.1,1};
	soundExpand[] = {"",0.1,1};//{"\A3\ui_f\data\sound\RscCombo\soundExpand",0.1,1};
	soundCollapse[] = {"",0.1,1};//{"\A3\ui_f\data\sound\RscCombo\soundCollapse",0.1,1};
};
class RscListBox
{
	soundSelect[] = {"",0.1,1};//{"\A3\ui_f\data\sound\RscListbox\soundSelect",0.09,1};
};
class RscXListBox
{
	soundSelect[] = {"",0.1,1};//{"\A3\ui_f\data\sound\RscListbox\soundSelect",0.09,1};
};
class RscButtonMenu: RscShortcutButton
{
	soundEnter[] = {"",0.1,1};//{"\A3\ui_f\data\sound\RscButtonMenu\soundEnter",0.09,1};
	soundPush[] = {"",0.1,1};//{"\A3\ui_f\data\sound\RscButtonMenu\soundPush",0.09,1};
	soundClick[] = {"",0.1,1};//{"\A3\ui_f\data\sound\RscButtonMenu\soundClick",0.09,1};
	soundEscape[] = {"",0.1,1};//{"\A3\ui_f\data\sound\RscButtonMenu\soundEscape",0.09,1};
};
class RscButtonMenuOK: RscButtonMenu
{
	soundPush[] = {"",0.1,1};//{"\A3\ui_f\data\sound\RscButtonMenuOK\soundPush",0.09,1};
};
class RscButtonNoColor: RscButton
{
	soundEnter[] = {"",0.1,1};//{"\A3\ui_f\data\sound\ReadOut\readoutClick",0.5,1};
	soundPush[] = {"",0.1,1};//{"\A3\ui_f\data\sound\RscButton\soundPush",0.09,1};
	soundClick[] = {"",0.1,1};//{"\A3\ui_f\data\sound\CfgNotifications\addItemOK",0.5,1};
	soundEscape[] = {"",0.1,1};//{"\A3\ui_f\data\sound\RscButton\soundPush",0.09,1};
};
class ctrlDefaultButton: ctrlDefaultText
{
	soundClick[] = {"",0.1,1};//{"\A3\ui_f\data\sound\RscButton\soundClick",0.09,1};
	soundEnter[] = {"",0.1,1};//{"\A3\ui_f\data\sound\RscButton\soundEnter",0.09,1};
	soundPush[] = {"",0.1,1};//{"\A3\ui_f\data\sound\RscButton\soundPush",0.09,1};
	soundEscape[] = {"",0.1,1};//{"\A3\ui_f\data\sound\RscButton\soundEscape",0.09,1};
};
class ctrlCombo: ctrlDefaultText
{
	soundExpand[] = {"",0.1,1};//{"\A3\ui_f\data\sound\RscCombo\soundExpand",0.1,1};
	soundCollapse[] = {"",0.1,1};//{"\A3\ui_f\data\sound\RscCombo\soundCollapse",0.1,1};
	soundSelect[] = {"",0.1,1};//{"\A3\ui_f\data\sound\RscCombo\soundSelect",0.1,1};
};
class ctrlListbox: ctrlDefaultText
{
	soundSelect[] = {"",0.1,1};//{"\A3\ui_f\data\sound\RscListbox\soundSelect",0.09,1};
};
class ctrlXListbox: ctrlDefaultText
{
	soundSelect[] = {"",0.1,1};//{"\A3\ui_f\data\sound\RscListbox\soundSelect",0.09,1};
};
class ctrlListNBox: ctrlDefaultText
{
	soundSelect[] = {"",0.1,1};//{"\A3\ui_f\data\sound\RscListbox\soundSelect",0.09,1};
};
class ctrlCheckbox: ctrlDefault
{
	soundClick[] = {"",0.1,1};//{"\A3\ui_f\data\sound\RscButton\soundClick",0.09,1};
	soundEnter[] = {"",0.1,1};//{"\A3\ui_f\data\sound\RscButton\soundEnter",0.09,1};
	soundPush[] = {"",0.1,1};//{"\A3\ui_f\data\sound\RscButton\soundPush",0.09,1};
	soundEscape[] = {"",0.1,1};//{"\A3\ui_f\data\sound\RscButton\soundEscape",0.09,1};
};
