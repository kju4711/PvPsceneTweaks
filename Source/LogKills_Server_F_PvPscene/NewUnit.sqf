private["_unit"];

_unit = _this select 0;

if ((typeName _unit) == "ARRAY") then
{
	_unit = _unit select 0;
};

if (!(isNull _unit)) then
{
	private["_name"];
	_name = name _unit;
	_unit setVariable ["LogKills_Server_F_PvPscene_UnitName",_name];
	_unit addMPEventHandler ["MPKilled",
	{
		if (isServer) then
		{
			[_this] call LogKills_Server_F_PvPscene_KilledUnit;
		};
	}];
};