/*
"LogKills_Server_F_PvPscene: 118.951 - 5.08 seconds"
""
"DeadGuy (WEST) has been killed by TeamKiller (WEST)"
""
"TeamKiller's UID: 3056006"
""
"George Jackson position: [1661.14,1371.52,0.00210953] (016013)"
"TeamKiller position: [1666.14,1376.52,0.00156784] (016013)"
"Distance between: 7.1559"
""
"Near players (100m): [B 1-1-A:1 (TeamKiller) REMOTE]"
""
"TeamKiller's magazines: ["30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","HandGrenade_West","HandGrenade_West","HandGrenade_West","HandGrenade_West"]"
"TeamKiller's weapons: ["SCAR_L_STD_HOLO","NVGoggles","ItemMap","ItemCompass","ItemWatch","ItemRadio"]"
"TeamKiller's unitBackpack: "
"TeamKiller's magazineCargo: [[],[]]"
"TeamKiller's weaponCargo: [[],[]]"
*/

private["_parameters","_killed","_killer","_isPlayerKilled","_isPlayerKiller","_skipKiller","_deathType","_nameKilled","_nameKiller","_nearEntities","_nearPlayers","_magazinesAtDeathKilled","_weaponsAtDeathKilled","_backpackAtDeathKilled","_magazinesInBackpackAtDeathKilled","_weaponsInBackpackAtDeathKilled","_magazinesAtDeathKiller","_weaponsAtDeathKiller","_backpackAtDeathKiller","_magazinesInBackpackAtDeathKiller","_weaponsInBackpackAtDeathKiller"];

_parameters = _this select 0;

_killed = _parameters select 0;
_killer = _parameters select 1;

_isPlayerKilled = isPlayer _killed;
_isPlayerKiller = isPlayer _killer;

_skipKiller = false;
if (_killed == _killer) then {_skipKiller = true;};
if (isNull _killer) then {_skipKiller = true;};

_deathType = "unknown";

///////////////////////////////////////////////////////////////////////////////

//name killed
_nameKilled = _killed getVariable ["LogKills_Server_F_PvPscene_UnitName","Unknown DEAD unit"];

//dayZ check
if (true) then
{
	private ["_bodyName"];
	_bodyName = _killed getVariable["bodyName","unknown"];
	_deathType = _killed getVariable["deathType","unknown"];

	if (_bodyName != "unknown") then {_nameKilled = _bodyName;};
};

//equipment
_magazinesAtDeathKilled = magazines _killed;
_weaponsAtDeathKilled = weapons _killed;
_backpackAtDeathKilled = typeOf (unitBackpack _killed);
_magazinesInBackpackAtDeathKilled = getMagazineCargo (unitBackpack _killed);
_weaponsInBackpackAtDeathKilled = getWeaponCargo (unitBackpack _killed);

///////////////////////////////////////////////////////////////////////////////

if (!(_skipKiller)) then
{
	//name killer
	_nameKiller = "KILLER";
	if (alive _killer) then {_nameKiller = name _killer;};
	_nameKiller = _killer getVariable ["LogKills_Server_F_PvPscene_UnitName",_nameKiller];

	//equipment
	_magazinesAtDeathKiller = magazines _killer;
	_weaponsAtDeathKiller = weapons _killer;
	_backpackAtDeathKiller = typeOf (unitBackpack _killer);
	_magazinesInBackpackAtDeathKiller = getMagazineCargo (unitBackpack _killer);
	_weaponsInBackpackAtDeathKiller = getWeaponCargo (unitBackpack _killer);

	//dayZ check
	if (true) then
	{
		private ["_bodyName"];
		_bodyName = _killer getVariable["bodyName","unknown"];
	
		if (_bodyName != "unknown") then {_nameKiller = _bodyName;};
	};
};

///////////////////////////////////////////////////////////////////////////////

//nearby units
_nearEntities = (position _killed) nearEntities [["CAManBase"],100];;
_nearPlayers = [];
{
	private["_unit"];
	_unit = _x;
	if (isPlayer _unit) then {_nearPlayers set [count _nearPlayers,_unit];};

} forEach _nearEntities;

///////////////////////////////////////////////////////////////////////////////
// LOGGING
///////////////////////////////////////////////////////////////////////////////

//timestamp
diag_log format ["LogKills_Server_F_PvPscene: %1 (serverTime) - %2 seconds (time)",serverTime,time];
diag_log "";

//A killed by B
if (!(_skipKiller)) then
{
	private ["_isKilledAI","_isKillerAI"];
	_isKilledAI = " (AI)";
	if (_isPlayerKilled) then {_isKilledAI = "";};
	_isKillerAI = " (AI)";
	if (_isPlayerKiller) then {_isKillerAI = "";};

	diag_log format ["%1 (%2)%3 has been killed by %4 (%5)%6",_nameKilled,side (group _killed),_isKilledAI,_nameKiller,side (group _killer),_isKillerAI];
}
else
{
	if (_isPlayerKilled) then
	{
		diag_log format ["%1 (%2) has been killed (SelfKill/FallToDeath/Scripting)",_nameKilled,side (group _killed)];
	};
};
diag_log "";

//dayZ logging
if (_deathType != "unknown") then
{
	diag_log format ["Deathtype: %1",_deathType];
	diag_log "";
};

///////////////////////////////////////////////////////////////////////////////

//Log UID if player
if (_isPlayerKilled) then
{
	diag_log format ["%1's UID: %2",_nameKilled,getPlayerUID _killed];
	diag_log "";
};

////equipment
if (_isPlayerKilled) then
{
	diag_log format ["%1's magazines: %2",_nameKilled,_magazinesAtDeathKilled];
	diag_log format ["%1's weapons: %2",_nameKilled,_weaponsAtDeathKilled];
	diag_log format ["%1's unitBackpack: %2",_nameKilled,_backpackAtDeathKilled];
	diag_log format ["%1's magazineCargo: %2",_nameKilled,_magazinesInBackpackAtDeathKilled];
	diag_log format ["%1's weaponCargo: %2",_nameKilled,_weaponsInBackpackAtDeathKilled];
	diag_log "";
};

///////////////////////////////////////////////////////////////////////////////

if (!(_skipKiller)) then
{
	//Log UID if player
	if (_isPlayerKiller) then
	{
		diag_log format ["%1's UID: %2",_nameKiller,getPlayerUID _killer];
		diag_log "";
	};

	////equipment
	if (_isPlayerKiller) then
	{
		diag_log format ["%1's magazines: %2",_nameKiller,_magazinesAtDeathKiller];
		diag_log format ["%1's weapons: %2",_nameKiller,_weaponsAtDeathKiller];
		diag_log format ["%1's unitBackpack: %2",_nameKiller,_backpackAtDeathKiller];
		diag_log format ["%1's magazineCargo: %2",_nameKiller,_magazinesInBackpackAtDeathKiller];
		diag_log format ["%1's weaponCargo: %2",_nameKiller,_weaponsInBackpackAtDeathKiller];
		diag_log "";
	};
};

///////////////////////////////////////////////////////////////////////////////

//position and distance
diag_log format ["%1 position: %2 (%3)",_nameKilled,position _killed,mapGridPosition _killed];
if (!(_skipKiller)) then
{
	diag_log format ["%1 position: %2 (%3)",_nameKiller,position _killer,mapGridPosition _killer];
	diag_log format ["Distance between: %1",_killed distance _killer];
};
diag_log "";

//nearby units
diag_log format ["Near players (100m): %1",_nearPlayers];
diag_log "";