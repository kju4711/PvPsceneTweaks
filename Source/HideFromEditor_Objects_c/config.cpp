﻿class CfgPatches
{
	class Optional_HideFromEditor_Objects_c
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Data_F_Loadorder","A3_Data_F_Curator_Loadorder","A3_Data_F_Kart_Loadorder","A3_Data_F_Bootcamp_Loadorder","A3_Data_F_Heli_Loadorder","A3_Data_F_Mark_Loadorder","A3_Data_F_Exp_A_Loadorder","A3_Data_F_Exp_B_Loadorder","A3_Data_F_Exp_Loadorder","A3_Data_F_Jets_Loadorder","A3_Data_F_Argo_Loadorder","A3_Data_F_Patrol_Loadorder","A3_Data_F_Orange_Loadorder","A3_Data_F_Tacops_Loadorder","A3_Data_F_Tank_Loadorder"};
		version = "2019-03-28";
	};
};
class CfgVehicles
{
	class AirIntakePlug_base_F;
	class APERSMine;
	class Banner_01_base_F;
	class Blood_01_Base_F;
	class Box_AAF_Equip_F;
	class Box_IND_AmmoOrd_F;
	class Box_NATO_Uniforms_F;
	class Camping_base_F;
	class Cargo_base_F;
	class Cargo_IDAP_base_F;
	class Cargo_IDAP_ruins_base_F;
	class Cargo10_base_F;
	class CargoNet_01_ammo_base_F;
	class CargoNet_01_base_F;
	class Constructions_base_F;
	class ContainmentArea_base_F;
	class FlexibleTank_base_F;
	class FloatingStructure_F;
	class Furniture_base_F;
	class Furniture_Residental_base_F;
	class Garbage_base_F;
	class GasTank_01_base_F;
	class GasTank_base_F;
	class HelicopterWheels_01_base_F;
	class Helper_Base_F;
	class House;
	class House_Small_F;
	class Items_base_F;
	class Land_AirConditioner_01_base_F;
	class Land_AirConditioner_01_sound_base_F;
	class Land_Bodybag_01_base_F;
	class Land_Bodybag_01_empty_base_F;
	class Land_Bodybag_01_folded_base_F;
	class Land_CampingChair_V2_F;
	class Land_CampingTable_F;
	class Land_CampingTable_small_F;
	class Land_CanisterFuel_F;
	class Land_Crowbar_01_base_F;
	class Land_EmergencyBlanket_01_base_F;
	class Land_FlowerPot_01_base_F;
	class Land_FoodSack_01_cargo_base_F;
	class Land_FoodSack_01_dmg_base_F;
	class Land_FoodSack_01_empty_base_F;
	class Land_FoodSack_01_full_base_F;
	class Land_FoodSack_01_large_base_F;
	class Land_FoodSack_01_small_base_F;
	class Land_Fridge_01_F;
	class Land_IntravenStand_01_base_F;
	class Land_MapBoard_01_Wall_base_F;
	class Land_MedicalTent_01_base_F;
	class Land_MedicalTent_01_floor_base_F;
	class Land_OfficeTable_01_F;
	class Land_Orange_01_Base_F;
	class Land_PaperBox_01_small_closed_base_F;
	class Land_PaperBox_01_small_destroyed_base_F;
	class Land_PaperBox_01_small_open_base_F;
	class Land_PaperBox_01_small_ransacked_base_F;
	class Land_PCSet_01_mousepad_base_F;
	class Land_Photoframe_01_base_F;
	class Land_Photoframe_01_broken_base_F;
	class Land_PlasticBucket_01_base_F;
	class Land_Pumpkin_01_Base_F;
	class Land_Rug_01_base_F;
	class Land_ShootingMat_01_base_F;
	class Land_ShootingMat_01_folded_base_F;
	class Land_Sign_MinesDanger_base_F;
	class Land_Sign_MinesTall_base_F;
	class Land_Sign_WarningMilitaryArea_F;
	class Land_Sleeping_bag_blue_F;
	class Land_Sleeping_bag_brown_F;
	class Land_Sleeping_bag_F;
	class Land_Stretcher_01_base_F;
	class Land_Stretcher_01_folded_base_F;
	class Land_TentA_F;
	class Land_TentDome_F;
	class Land_VASICore;
	class Land_WallSign_01_base_F;
	class Land_WaterPump_01_F;
	class Leaflet_05_Base_F;
	class Machine_base_F;
	class MedicalGarbage_01_Base_F;
	class MetalCase_01_base_F;
	class MineBase;
	class NonStrategic;
	class O_Heli_Transport_04_ammo_F;
	class O_Heli_Transport_04_bench_F;
	class O_Heli_Transport_04_box_F;
	class O_Heli_Transport_04_covered_F;
	class O_Heli_Transport_04_F;
	class O_Heli_Transport_04_fuel_F;
	class O_Heli_Transport_04_medevac_F;
	class O_Heli_Transport_04_repair_F;
	class PalletTrolley_01_base_F;
	class PlaneWreck;
	class PlasticCase_01_base_F;
	class Pod_Heli_Transport_04_base_F;
	class Pod_Heli_Transport_04_crewed_base_F;
	class SatchelCharge_F;
	class Scrapyard_base_F;
	class SignAd_Sponsor_F;
	class SignAd_SponsorS_F;
	class SportItems_base_F;
	class SportsGrounds_base_F;
	class StorageBladder_base_F;
	class Target_PopUp_HVT1_Moving_90deg_F;
	class Target_PopUp_HVT1_Moving_F;
	class Target_PopUp_HVT2_Moving_90deg_F;
	class Target_PopUp_HVT2_Moving_F;
	class Target_PopUp4_Moving_90deg_Acc1_F;
	class Target_PopUp4_Moving_90deg_Acc2_F;
	class Target_PopUp4_Moving_90deg_F;
	class Target_PopUp4_Moving_Acc1_F;
	class Target_PopUp4_Moving_Acc2_F;
	class Target_PopUp4_Moving_F;
	class Target_Swivel_01_base_F;
	class TargetBase;
	class TargetBootcamp_base_F;
	class TargetP_HVT1_F;
	class TargetP_HVT2_F;
	class TargetP_Inf4_Acc1_F;
	class TargetP_Inf4_Acc2_F;
	class TargetP_Inf4_F;
	class ThingX;
	class Tire_Van_02_base_F;
	class ToolTrolley_base_F;
	class UWreck_base_F;
	class VR_Block_base_F;
	class VR_CoverObject_base_F;
	class VR_Helper_base_F;
	class VR_Shape_base_F;
	class Wall_F;
	class Wreck_base_F;

	class Sign_Arrow_F: Helper_Base_F
	{
		scope = 1;//2
	};
	class Sign_Arrow_Large_F: Helper_Base_F
	{
		scope = 1;//2
	};
	class Sign_Arrow_Direction_F: Helper_Base_F
	{
		scope = 1;//2
	};
	class Sign_Pointer_F: Helper_Base_F
	{
		scope = 1;//2
	};
	class Sign_Sphere10cm_F: Helper_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Sign_Sphere25cm_F: Helper_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Sign_Sphere100cm_F: Helper_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Sign_Circle_F: Helper_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class UserTexture1m_F: NonStrategic
	{
		scope = 1;//2
	};
	class Land_NavigLight: House
	{
//		scope = 1;
	};
	class Land_Runway_PAPI: Land_VASICore
	{
//		scope = 1;
	};
	class Land_FinishGate_01_narrow_F: SportsGrounds_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FinishGate_01_wide_F: Land_FinishGate_01_narrow_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PlasticBarrier_01_F: SportsGrounds_base_F
	{
		scope = 1;//2
	};
	class PlasticBarrier_01_red_F: Land_PlasticBarrier_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class PlasticBarrier_01_white_F: Land_PlasticBarrier_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PlasticBarrier_01_line_x2_F: Land_PlasticBarrier_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PlasticBarrier_01_sharp15L_x2_F: Land_PlasticBarrier_01_line_x2_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_sharp15R_x2_F: Land_PlasticBarrier_01_line_x2_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_sharp22L_x2_F: Land_PlasticBarrier_01_line_x2_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_sharp22R_x2_F: Land_PlasticBarrier_01_line_x2_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_sharp30L_x2_F: Land_PlasticBarrier_01_line_x2_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_sharp30R_x2_F: Land_PlasticBarrier_01_line_x2_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_sharp45L_x2_F: Land_PlasticBarrier_01_line_x2_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_sharp45R_x2_F: Land_PlasticBarrier_01_line_x2_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_sharp60L_x2_F: Land_PlasticBarrier_01_line_x2_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_sharp60R_x2_F: Land_PlasticBarrier_01_line_x2_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_line_x4_F: Land_PlasticBarrier_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PlasticBarrier_01_round15L_x4_F: Land_PlasticBarrier_01_line_x4_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_round15R_x4_F: Land_PlasticBarrier_01_line_x4_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_round22L_x4_F: Land_PlasticBarrier_01_line_x4_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_round22R_x4_F: Land_PlasticBarrier_01_line_x4_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_round30L_x4_F: Land_PlasticBarrier_01_line_x4_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_round30R_x4_F: Land_PlasticBarrier_01_line_x4_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_round45L_x4_F: Land_PlasticBarrier_01_line_x4_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_round45R_x4_F: Land_PlasticBarrier_01_line_x4_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_round60L_x4_F: Land_PlasticBarrier_01_line_x4_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_round60R_x4_F: Land_PlasticBarrier_01_line_x4_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_round90L_x4_F: Land_PlasticBarrier_01_line_x4_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_round90R_x4_F: Land_PlasticBarrier_01_line_x4_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_round135L_x4_F: Land_PlasticBarrier_01_line_x4_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_round135R_x4_F: Land_PlasticBarrier_01_line_x4_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_round180L_x4_F: Land_PlasticBarrier_01_line_x4_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_round180R_x4_F: Land_PlasticBarrier_01_line_x4_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_sharp15L_x4_F: Land_PlasticBarrier_01_line_x4_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_sharp15R_x4_F: Land_PlasticBarrier_01_line_x4_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_sharp22L_x4_F: Land_PlasticBarrier_01_line_x4_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_sharp22R_x4_F: Land_PlasticBarrier_01_line_x4_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_sharp30L_x4_F: Land_PlasticBarrier_01_line_x4_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_sharp30R_x4_F: Land_PlasticBarrier_01_line_x4_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_sharp45L_x4_F: Land_PlasticBarrier_01_line_x4_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_sharp45R_x4_F: Land_PlasticBarrier_01_line_x4_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_sharp60L_x4_F: Land_PlasticBarrier_01_line_x4_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_sharp60R_x4_F: Land_PlasticBarrier_01_line_x4_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_snake15L_x4_F: Land_PlasticBarrier_01_line_x4_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_snake15R_x4_F: Land_PlasticBarrier_01_line_x4_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_snake22L_x4_F: Land_PlasticBarrier_01_line_x4_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_snake22R_x4_F: Land_PlasticBarrier_01_line_x4_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_snake30L_x4_F: Land_PlasticBarrier_01_line_x4_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_snake30R_x4_F: Land_PlasticBarrier_01_line_x4_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_snake45L_x4_F: Land_PlasticBarrier_01_line_x4_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_snake45R_x4_F: Land_PlasticBarrier_01_line_x4_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_snake60L_x4_F: Land_PlasticBarrier_01_line_x4_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_snake60R_x4_F: Land_PlasticBarrier_01_line_x4_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_line_x6_F: Land_PlasticBarrier_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PlasticBarrier_01_round15L_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_round15R_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_round22L_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_round22R_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_round30L_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_round30R_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_round45L_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_round45R_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_round60L_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_round60R_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_round90L_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_round90R_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_round135L_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_round135R_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_round180L_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_round180R_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_round270L_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_round270R_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_sharp15L_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_sharp15R_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_sharp22L_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_sharp22R_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_sharp30L_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_sharp30R_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_sharp45L_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_sharp45R_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_sharp60L_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_sharp60R_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_snake15L_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_snake15R_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_snake22L_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_snake22R_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_snake30L_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_snake30R_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_snake45L_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_snake45R_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_snake60L_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_snake60R_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_snake90L_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_01_snake90R_x6_F: Land_PlasticBarrier_01_line_x6_F
	{
//		scope = 1;
	};
	class Land_PlasticBarrier_02_F: SportsGrounds_base_F
	{
		scope = 1;//2
	};
	class PlasticBarrier_02_grey_F: Land_PlasticBarrier_02_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class PlasticBarrier_02_yellow_F: Land_PlasticBarrier_02_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PlasticBarrier_03_F: ThingX
	{
		scope = 1;//2
	};
	class PlasticBarrier_03_blue_F: Land_PlasticBarrier_03_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class PlasticBarrier_03_orange_F: Land_PlasticBarrier_03_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Oil_Spill_F: Helper_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class SignAd_Sponsor_Blueking_F: SignAd_Sponsor_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class SignAd_Sponsor_Fuel_white_F: SignAd_Sponsor_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class SignAd_Sponsor_Fuel_green_F: SignAd_Sponsor_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class SignAd_Sponsor_Burstkoke_F: SignAd_Sponsor_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class SignAd_Sponsor_Redburger_F: SignAd_Sponsor_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class SignAd_Sponsor_Redstone_F: SignAd_Sponsor_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class SignAd_Sponsor_Suatmm_F: SignAd_Sponsor_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class SignAd_SponsorS_Blueking_F: SignAd_SponsorS_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class SignAd_SponsorS_Fuel_white_F: SignAd_SponsorS_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class SignAd_SponsorS_Fuel_green_F: SignAd_SponsorS_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class SignAd_SponsorS_Burstkoke_F: SignAd_SponsorS_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class SignAd_SponsorS_Redburger_F: SignAd_SponsorS_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class SignAd_SponsorS_Redstone_F: SignAd_SponsorS_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class SignAd_SponsorS_Suatmm_F: SignAd_SponsorS_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Plane_Fighter_01_wreck_F: PlaneWreck
	{
//		scope = 1;
	};
	class Plane_Fighter_02_wreck_F: PlaneWreck
	{
//		scope = 1;
	};
	class Plane_Fighter_03_wreck_F: PlaneWreck
	{
//		scope = 1;
	};
	class Plane_Fighter_04_wreck_F: PlaneWreck
	{
//		scope = 1;
	};
	class Land_UWreck_FishingBoat_F: UWreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_UWreck_Heli_Attack_02_F: UWreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_UWreck_MV22_F: UWreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wreck_BMP2_F: Wreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wreck_BRDM2_F: Wreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wreck_Car_F: Wreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wreck_Car2_F: Wreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wreck_Car3_F: Wreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wreck_CarDismantled_F: Wreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wreck_Heli_Attack_01_F: Wreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wreck_Heli_Attack_02_F: Wreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wreck_HMMWV_F: Wreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wreck_Hunter_F: Wreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wreck_Offroad_F: Wreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wreck_Offroad2_F: Wreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wreck_Plane_Transport_01_F: Wreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wreck_Skodovka_F: Wreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wreck_Slammer_F: Wreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wreck_Slammer_hull_F: Wreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wreck_Slammer_turret_F: Wreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wreck_T72_hull_F: Wreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wreck_T72_turret_F: Wreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wreck_Traw_F: UWreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wreck_Traw2_F: UWreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wreck_Truck_dropside_F: Wreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wreck_Truck_F: Wreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wreck_UAZ_F: Wreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wreck_Ural_F: Wreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wreck_Van_F: Wreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wreck_MBT_04_F: Wreck_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Wreck_LT_01_F: Wreck_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Wreck_AFV_Wheeled_01_F: Wreck_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_PartyTent_01_F: Camping_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_GymBench_01_F: SportsGrounds_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_GymRack_01_F: SportsGrounds_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_GymRack_02_F: SportsGrounds_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_GymRack_03_F: SportsGrounds_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_TyreBarrier_01_F: SportsGrounds_base_F
	{
		scope = 1;//2
	};
	class TyreBarrier_01_black_F: Land_TyreBarrier_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class TyreBarrier_01_white_F: Land_TyreBarrier_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_TyreBarrier_01_line_x4_F: Land_TyreBarrier_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_TyreBarrier_01_line_x6_F: Land_TyreBarrier_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WinnersPodium_01_F: SportsGrounds_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo20_vr_F: Cargo_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CarBattery_01_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CarBattery_02_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_VRGoggles_01_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WaterCooler_01_new_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FoodContainer_01_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Camera_01_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FlatTV_01_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Fridge_01_closed_F: Land_Fridge_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Fridge_01_open_F: Land_Fridge_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_GamingSet_01_camera_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_GamingSet_01_console_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_GamingSet_01_controller_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_GamingSet_01_powerSupply_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HDMICable_01_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Microwave_01_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PCSet_01_screen_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PCSet_01_case_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PCSet_01_keyboard_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PCSet_01_mouse_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PortableSpeakers_01_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Printer_01_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Projector_01_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Tablet_01_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Ketchup_01_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Mustard_01_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Tableware_01_cup_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Tableware_01_fork_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Tableware_01_knife_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Tableware_01_napkin_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Tableware_01_spoon_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Tableware_01_stackOfNapkins_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Tableware_01_tray_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Respawn_TentDome_F: Land_TentDome_F
	{
//		scope = 1;
	};
	class Respawn_TentA_F: Land_TentA_F
	{
//		scope = 1;
	};
	class Respawn_Sleeping_bag_F: Land_Sleeping_bag_F
	{
//		scope = 1;
	};
	class Respawn_Sleeping_bag_blue_F: Land_Sleeping_bag_blue_F
	{
//		scope = 1;
	};
	class Respawn_Sleeping_bag_brown_F: Land_Sleeping_bag_brown_F
	{
//		scope = 1;
	};
	class Land_DataTerminal_01_F: NonStrategic
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Tablet_02_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_KartSteertingWheel_01_F: SportItems_base_F
	{
		scope = 1;//2
	};
	class KartSteertingWheel_01_black_F: Land_KartSteertingWheel_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class KartSteertingWheel_01_blue_F: Land_KartSteertingWheel_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class KartSteertingWheel_01_green_F: Land_KartSteertingWheel_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class KartSteertingWheel_01_orange_F: Land_KartSteertingWheel_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class KartSteertingWheel_01_red_F: Land_KartSteertingWheel_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class KartSteertingWheel_01_white_F: Land_KartSteertingWheel_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class KartSteertingWheel_01_yellow_F: Land_KartSteertingWheel_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_KartStand_01_F: SportItems_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_KartTrolly_01_F: SportItems_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_KartTyre_01_F: SportItems_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_KartTyre_01_x4_F: SportItems_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Trophy_01_gold_F: SportItems_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class UserTexture_1x2_F: NonStrategic
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class TargetBootcampHumanSimple_F: TargetBootcamp_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class TargetBootcampHuman_F: TargetBootcampHumanSimple_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Target_PopUp_01_figure_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Target_PopUp_01_mechanism_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_VR_Block_01_F: VR_Block_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_VR_Block_02_F: VR_Block_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_VR_Block_03_F: VR_Block_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_VR_Block_04_F: VR_Block_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_VR_Block_05_F: VR_Block_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_VR_CoverObject_01_kneelLow_F: VR_CoverObject_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_VR_CoverObject_01_kneel_F: VR_CoverObject_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_VR_CoverObject_01_kneelHigh_F: VR_CoverObject_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_VR_CoverObject_01_stand_F: VR_CoverObject_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_VR_CoverObject_01_standHigh_F: VR_CoverObject_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class VR_3DSelector_01_default_F: VR_Helper_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class VR_3DSelector_01_incomplete_F: VR_3DSelector_01_default_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class VR_3DSelector_01_complete_F: VR_3DSelector_01_default_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class VR_3DSelector_01_exit_F: VR_3DSelector_01_default_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class VR_Area_01_circle_4_grey_F: VR_Helper_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class VR_Area_01_circle_4_yellow_F: VR_Area_01_circle_4_grey_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class VR_Area_01_square_1x1_grey_F: VR_Helper_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class VR_Area_01_square_2x2_grey_F: VR_Area_01_square_1x1_grey_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class VR_Area_01_square_4x4_grey_F: VR_Area_01_square_1x1_grey_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class VR_Area_01_square_1x1_yellow_F: VR_Area_01_square_1x1_grey_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class VR_Area_01_square_2x2_yellow_F: VR_Area_01_square_1x1_yellow_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class VR_Area_01_square_4x4_yellow_F: VR_Area_01_square_1x1_yellow_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class VR_Billboard_01_F: VR_Helper_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class VR_GroundIcon_01_F: VR_Helper_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class VR_Sector_01_60deg_50_grey_F: VR_Helper_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class VR_Sector_01_60deg_50_red_F: VR_Sector_01_60deg_50_grey_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Pallet_MilBoxes_F: Scrapyard_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PaperBox_closed_F: Scrapyard_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PaperBox_closed_scripted_F: Land_PaperBox_closed_F
	{
//		scope = 1;
	};
	class Land_PaperBox_open_empty_F: Scrapyard_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PaperBox_open_empty_scripted_F: Land_PaperBox_open_empty_F
	{
//		scope = 1;
	};
	class Land_PaperBox_open_full_F: Scrapyard_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PaperBox_open_full_scripted_F: Land_PaperBox_open_full_F
	{
//		scope = 1;
	};
	class Land_Scrap_MRAP_01_F: Scrapyard_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ScrapHeap_1_F: Scrapyard_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ScrapHeap_2_F: Scrapyard_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_AirconCondenser_01_F: House_Small_F
	{
		scope = 1;//2
	};
	class Land_GasTank_01_blue_F: GasTank_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_GasTank_01_khaki_F: GasTank_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_GasTank_01_yellow_F: GasTank_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_GasTank_02_F: GasTank_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MobileScafolding_01_F: Constructions_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ToolTrolley_01_F: ToolTrolley_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ToolTrolley_02_F: ToolTrolley_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WeldingTrolley_01_F: Constructions_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WheelieBin_01_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PalletTrolley_01_khaki_F: PalletTrolley_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PalletTrolley_01_yellow_F: PalletTrolley_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_OfficeCabinet_01_F: Furniture_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_OfficeChair_01_F: Furniture_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class OfficeTable_01_new_F: Land_OfficeTable_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class OfficeTable_01_old_F: Land_OfficeTable_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_RattanChair_01_F: Furniture_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_RattanTable_01_F: Furniture_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Workbench_01_F: Furniture_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MobileLandingPlatform_01_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Windsock_01_F: NonStrategic
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo10_blue_F: Cargo10_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo10_brick_red_F: Cargo10_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo10_cyan_F: Cargo10_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo10_grey_F: Cargo10_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo10_light_blue_F: Cargo10_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo10_light_green_F: Cargo10_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo10_military_green_F: Cargo10_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo10_orange_F: Cargo10_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo10_red_F: Cargo10_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo10_sand_F: Cargo10_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo10_white_F: Cargo10_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo10_yellow_F: Cargo10_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_DieselGroundPowerUnit_01_F: Machine_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_EngineCrane_01_F: Machine_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_JetEngineStarter_01_F: Machine_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PressureWasher_01_F: Machine_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class WaterPump_01_forest_F: Land_WaterPump_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class WaterPump_01_sand_F: Land_WaterPump_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_DischargeStick_01_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_RotorCoversBag_01_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_AirIntakePlug_01_F: AirIntakePlug_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_AirIntakePlug_02_F: AirIntakePlug_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_AirIntakePlug_03_F: AirIntakePlug_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_AirIntakePlug_04_F: AirIntakePlug_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_AirIntakePlug_05_F: AirIntakePlug_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HelicopterWheels_01_assembled_F: HelicopterWheels_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HelicopterWheels_01_disassembled_F: HelicopterWheels_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PitotTubeCover_01_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PortableHelipadLight_01_F: FloatingStructure_F
	{
		scope = 1;//2
	};
	class PortableHelipadLight_01_blue_F: Land_PortableHelipadLight_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class PortableHelipadLight_01_red_F: Land_PortableHelipadLight_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class PortableHelipadLight_01_white_F: Land_PortableHelipadLight_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class PortableHelipadLight_01_green_F: Land_PortableHelipadLight_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class PortableHelipadLight_01_yellow_F: Land_PortableHelipadLight_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MetalCase_01_large_F: MetalCase_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MetalCase_01_medium_F: MetalCase_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MetalCase_01_small_F: MetalCase_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PlasticCase_01_large_F: PlasticCase_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PlasticCase_01_medium_F: PlasticCase_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PlasticCase_01_small_F: PlasticCase_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Baseball_01_F: SportItems_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BaseballMitt_01_F: SportItems_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Basketball_01_F: SportItems_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Football_01_F: SportItems_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Rugbyball_01_F: SportItems_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Volleyball_01_F: SportItems_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Rope_01_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WheelChock_01_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Sign_Sphere200cm_F: Helper_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ShotTimer_01_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_AirHorn_01_F: SportItems_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Balloon_01_air_F: SportItems_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Balloon_01_air_NoPop_F: Land_Balloon_01_air_F
	{
//		scope = 1;
	};
	class Land_Balloon_01_water_F: Land_Balloon_01_air_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Balloon_01_water_NoPop_F: Land_Balloon_01_water_F
	{
//		scope = 1;
	};
	class TargetP_Inf4_NoPop_F: TargetP_Inf4_F
	{
//		scope = 1;
	};
	class TargetP_Inf4_Acc1_NoPop_F: TargetP_Inf4_Acc1_F
	{
//		scope = 1;
	};
	class TargetP_Inf4_Acc2_NoPop_F: TargetP_Inf4_Acc2_F
	{
//		scope = 1;
	};
	class Target_PopUp4_Moving_NoPop_F: Target_PopUp4_Moving_F
	{
//		scope = 1;
	};
	class Target_PopUp4_Moving_Acc1_NoPop_F: Target_PopUp4_Moving_Acc1_F
	{
//		scope = 1;
	};
	class Target_PopUp4_Moving_Acc2_NoPop_F: Target_PopUp4_Moving_Acc2_F
	{
//		scope = 1;
	};
	class Target_PopUp4_Moving_90deg_NoPop_F: Target_PopUp4_Moving_90deg_F
	{
//		scope = 1;
	};
	class Target_PopUp4_Moving_90deg_Acc1_NoPop_F: Target_PopUp4_Moving_90deg_Acc1_F
	{
//		scope = 1;
	};
	class Target_PopUp4_Moving_90deg_Acc2_NoPop_F: Target_PopUp4_Moving_90deg_Acc2_F
	{
//		scope = 1;
	};
	class TargetP_HVT1_NoPop_F: TargetP_HVT1_F
	{
//		scope = 1;
	};
	class Target_PopUp_HVT1_Moving_NoPop_F: Target_PopUp_HVT1_Moving_F
	{
//		scope = 1;
	};
	class Target_PopUp_HVT1_Moving_90deg_NoPop_F: Target_PopUp_HVT1_Moving_90deg_F
	{
//		scope = 1;
	};
	class TargetP_HVT2_NoPop_F: TargetP_HVT2_F
	{
//		scope = 1;
	};
	class Target_PopUp_HVT2_Moving_NoPop_F: Target_PopUp_HVT2_Moving_F
	{
//		scope = 1;
	};
	class Target_PopUp_HVT2_Moving_90deg_NoPop_F: Target_PopUp_HVT2_Moving_90deg_F
	{
//		scope = 1;
	};
	class Land_BulletTrap_01_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Target_Dueling_01_F: TargetBase
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Target_Swivel_01_F: Target_Swivel_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Target_Swivel_01_NoPop_F: Land_Target_Swivel_01_F
	{
//		scope = 1;
	};
	class Target_Swivel_01_ground_F: Target_Swivel_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Target_Swivel_01_ground_NoPop_F: Target_Swivel_01_ground_F
	{
//		scope = 1;
	};
	class Target_Swivel_01_left_F: Land_Target_Swivel_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Target_Swivel_01_left_NoPop_F: Target_Swivel_01_left_F
	{
//		scope = 1;
	};
	class Target_Swivel_01_right_F: Land_Target_Swivel_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Target_Swivel_01_right_NoPop_F: Target_Swivel_01_right_F
	{
//		scope = 1;
	};
	class ShootingMat_01_Olive_F: Land_ShootingMat_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class ShootingMat_01_Khaki_F: Land_ShootingMat_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class ShootingMat_01_OPFOR_F: Land_ShootingMat_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class ShootingMat_01_folded_Olive_F: Land_ShootingMat_01_folded_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class ShootingMat_01_folded_Khaki_F: Land_ShootingMat_01_folded_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class ShootingMat_01_folded_OPFOR_F: Land_ShootingMat_01_folded_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_VR_Shape_01_cube_1m_F: VR_Shape_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_VR_Target_Dart_01_F: TargetBase
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_VR_Target_MBT_01_cannon_F: TargetBase
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_VR_Target_MRAP_01_F: TargetBase
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_VR_Target_APC_Wheeled_01_F: TargetBase
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_StorageBladder_01_F: StorageBladder_base_F
	{
//		scope = 1;
	};
	class StorageBladder_01_fuel_forest_F: Land_StorageBladder_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class StorageBladder_01_fuel_sand_F: Land_StorageBladder_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_StorageBladder_02_F: StorageBladder_base_F
	{
//		scope = 1;
	};
	class StorageBladder_02_water_forest_F: Land_StorageBladder_02_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class StorageBladder_02_water_sand_F: Land_StorageBladder_02_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ContainmentArea_01_F: ContainmentArea_base_F
	{
//		scope = 1;
	};
	class ContainmentArea_01_forest_F: Land_ContainmentArea_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class ContainmentArea_01_sand_F: Land_ContainmentArea_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ContainmentArea_02_F: ContainmentArea_base_F
	{
//		scope = 1;
	};
	class ContainmentArea_02_forest_F: Land_ContainmentArea_02_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class ContainmentArea_02_sand_F: Land_ContainmentArea_02_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_CargoNet_01_ammo_F: CargoNet_01_ammo_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_CargoNet_01_ammo_F: CargoNet_01_ammo_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_CargoNet_01_ammo_F: CargoNet_01_ammo_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class CargoNet_01_box_F: CargoNet_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class CargoNet_01_barrels_F: CargoNet_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FlexibleTank_01_F: FlexibleTank_base_F
	{
//		scope = 1;
	};
	class FlexibleTank_01_forest_F: Land_FlexibleTank_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class FlexibleTank_01_sand_F: Land_FlexibleTank_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Pod_Heli_Transport_04_ammo_F: Pod_Heli_Transport_04_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Pod_Heli_Transport_04_bench_F: Pod_Heli_Transport_04_crewed_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Pod_Heli_Transport_04_box_F: Pod_Heli_Transport_04_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Pod_Heli_Transport_04_covered_F: Pod_Heli_Transport_04_crewed_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Pod_Heli_Transport_04_fuel_F: Pod_Heli_Transport_04_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Pod_Heli_Transport_04_medevac_F: Pod_Heli_Transport_04_crewed_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Pod_Heli_Transport_04_repair_F: Pod_Heli_Transport_04_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_Heli_Transport_04_black_F: O_Heli_Transport_04_F
	{
//		scope = 1;
	};
	class O_Heli_Transport_04_ammo_black_F: O_Heli_Transport_04_ammo_F
	{
//		scope = 1;
	};
	class O_Heli_Transport_04_bench_black_F: O_Heli_Transport_04_bench_F
	{
//		scope = 1;
	};
	class O_Heli_Transport_04_box_black_F: O_Heli_Transport_04_box_F
	{
//		scope = 1;
	};
	class O_Heli_Transport_04_covered_black_F: O_Heli_Transport_04_covered_F
	{
//		scope = 1;
	};
	class O_Heli_Transport_04_fuel_black_F: O_Heli_Transport_04_fuel_F
	{
//		scope = 1;
	};
	class O_Heli_Transport_04_medevac_black_F: O_Heli_Transport_04_medevac_F
	{
//		scope = 1;
	};
	class O_Heli_Transport_04_repair_black_F: O_Heli_Transport_04_repair_F
	{
//		scope = 1;
	};
	class Land_Pod_Heli_Transport_04_ammo_black_F: Land_Pod_Heli_Transport_04_ammo_F
	{
//		scope = 1;
	};
	class Land_Pod_Heli_Transport_04_bench_black_F: Land_Pod_Heli_Transport_04_bench_F
	{
//		scope = 1;
	};
	class Land_Pod_Heli_Transport_04_box_black_F: Land_Pod_Heli_Transport_04_box_F
	{
//		scope = 1;
	};
	class Land_Pod_Heli_Transport_04_covered_black_F: Land_Pod_Heli_Transport_04_covered_F
	{
//		scope = 1;
	};
	class Land_Pod_Heli_Transport_04_fuel_black_F: Land_Pod_Heli_Transport_04_fuel_F
	{
//		scope = 1;
	};
	class Land_Pod_Heli_Transport_04_medevac_black_F: Land_Pod_Heli_Transport_04_medevac_F
	{
//		scope = 1;
	};
	class Land_Pod_Heli_Transport_04_repair_black_F: Land_Pod_Heli_Transport_04_repair_F
	{
//		scope = 1;
	};
	class Land_VR_Slope_01_F: VR_Block_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_GarbageHeap_01_F: Garbage_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_GarbageHeap_02_F: Garbage_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_GarbageHeap_03_F: Garbage_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_GarbageHeap_04_F: Garbage_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Tyre_01_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Tyre_01_horizontal_F: ThingX
	{
//		scope = 1;
	};
	class Land_Tyre_01_line_x5_F: Garbage_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WoodenCounter_01_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WoodenCrate_01_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WoodenCrate_01_stack_x3_F: NonStrategic
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WoodenCrate_01_stack_x5_F: NonStrategic
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Bulldozer_01_abandoned_F: Wreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Bulldozer_01_wreck_F: Wreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CombineHarvester_01_wreck_F: Wreck_base_F
	{
		scope = 1;//2
	};
	class Land_Excavator_01_abandoned_F: Wreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Excavator_01_wreck_F: Wreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HaulTruck_01_abandoned_F: Wreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MiningShovel_01_abandoned_F: Wreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Locomotive_01_v1_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Locomotive_01_v2_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Locomotive_01_v3_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_RailwayCar_01_passenger_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_RailwayCar_01_sugarcane_empty_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_RailwayCar_01_sugarcane_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_RailwayCar_01_tank_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_RoadCone_01_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_VergeRock_01_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Device_slingloadable_F: FloatingStructure_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_IRMaskingCover_01_F: NonStrategic
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_IRMaskingCover_02_F: NonStrategic
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PortableGenerator_01_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SatelliteAntenna_01_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_TripodScreen_01_dual_v1_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_TripodScreen_01_dual_v2_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_TripodScreen_01_large_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HistoricalPlaneDebris_01_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HistoricalPlaneDebris_02_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HistoricalPlaneDebris_03_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HistoricalPlaneDebris_04_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HistoricalPlaneWreck_01_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HistoricalPlaneWreck_02_front_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HistoricalPlaneWreck_02_front_water_F: Land_HistoricalPlaneWreck_02_front_F
	{
//		scope = 1;
	};
	class Land_HistoricalPlaneWreck_02_rear_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HistoricalPlaneWreck_02_rear_water_F: Land_HistoricalPlaneWreck_02_rear_F
	{
//		scope = 1;
	};
	class Land_HistoricalPlaneWreck_02_wing_left_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HistoricalPlaneWreck_02_wing_right_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HistoricalPlaneWreck_03_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Boat_01_abandoned_blue_F: Wreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Boat_01_abandoned_red_F: Wreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Boat_02_abandoned_F: Wreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Boat_03_abandoned_F: Wreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Boat_03_abandoned_cover_F: NonStrategic
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Boat_04_wreck_F: Wreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Boat_05_wreck_F: Wreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Boat_06_wreck_F: Wreck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_TankWreck_01_turret_F: Wreck_base_F
	{
//		scope = 1;
	};
	class Land_DeckTractor_01_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_TowBar_01_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Bomb_Trolley_01_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Missle_Trolley_02_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CinderBlock_01_F: Constructions_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Brick_01_F: Constructions_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MapBoard_01_Wall_F: Land_MapBoard_01_Wall_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MapBoard_01_Wall_Stratis_F: Land_MapBoard_01_Wall_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MapBoard_01_Wall_Altis_F: Land_MapBoard_01_Wall_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MapBoard_01_Wall_Tanoa_F: Land_MapBoard_01_Wall_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MapBoard_01_Wall_Malden_F: Land_MapBoard_01_Wall_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WoodenBed_01_F: Furniture_Residental_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_TableSmall_01_F: Furniture_Residental_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_TableBig_01_F: Furniture_Residental_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Sofa_01_F: Furniture_Residental_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_OfficeCabinet_02_F: Furniture_Residental_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ArmChair_01_F: Furniture_Residental_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Rug_01_F: Land_Rug_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Rug_01_Traditional_F: Land_Rug_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Stretcher_01_F: Land_Stretcher_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Stretcher_01_olive_F: Land_Stretcher_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Stretcher_01_sand_F: Land_Stretcher_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Stretcher_01_folded_F: Land_Stretcher_01_folded_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Stretcher_01_folded_olive_F: Land_Stretcher_01_folded_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Stretcher_01_folded_sand_F: Land_Stretcher_01_folded_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Bodybag_01_black_F: Land_Bodybag_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Bodybag_01_blue_F: Land_Bodybag_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Bodybag_01_white_F: Land_Bodybag_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Bodybag_01_empty_black_F: Land_Bodybag_01_empty_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Bodybag_01_empty_blue_F: Land_Bodybag_01_empty_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Bodybag_01_empty_white_F: Land_Bodybag_01_empty_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Bodybag_01_folded_black_F: Land_Bodybag_01_folded_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Bodybag_01_folded_blue_F: Land_Bodybag_01_folded_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Bodybag_01_folded_white_F: Land_Bodybag_01_folded_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Stethoscope_01_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_IntravenBag_01_full_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_IntravenBag_01_empty_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_IntravenStand_01_empty_F: Land_IntravenStand_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_IntravenStand_01_1bag_F: Land_IntravenStand_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_IntravenStand_01_2bags_F: Land_IntravenStand_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FirstAidKit_01_closed_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FirstAidKit_01_open_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CampingChair_V2_white_F: Land_CampingChair_V2_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CampingTable_white_F: Land_CampingTable_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CampingTable_small_white_F: Land_CampingTable_small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CanvasCover_01_F: Land_IRMaskingCover_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CanvasCover_02_F: Land_IRMaskingCover_02_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_AirConditioner_01_F: Land_AirConditioner_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_AirConditioner_02_F: Land_AirConditioner_01_sound_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_AirConditioner_03_F: Land_AirConditioner_01_sound_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_AirConditioner_04_F: Land_AirConditioner_01_sound_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_EmergencyBlanket_01_F: Land_EmergencyBlanket_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_EmergencyBlanket_01_stack_F: Land_EmergencyBlanket_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_EmergencyBlanket_01_discarded_F: Land_EmergencyBlanket_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_EmergencyBlanket_02_F: Land_EmergencyBlanket_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_EmergencyBlanket_02_stack_F: Land_EmergencyBlanket_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_EmergencyBlanket_02_discarded_F: Land_EmergencyBlanket_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PaperBox_01_small_closed_brown_F: Land_PaperBox_01_small_closed_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PaperBox_01_small_closed_brown_IDAP_F: Land_PaperBox_01_small_closed_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PaperBox_01_small_closed_brown_food_F: Land_PaperBox_01_small_closed_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PaperBox_01_small_closed_white_IDAP_F: Land_PaperBox_01_small_closed_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PaperBox_01_small_closed_white_med_F: Land_PaperBox_01_small_closed_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PaperBox_01_small_ransacked_brown_IDAP_F: Land_PaperBox_01_small_ransacked_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PaperBox_01_small_ransacked_white_IDAP_F: Land_PaperBox_01_small_ransacked_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PaperBox_01_small_ransacked_brown_F: Land_PaperBox_01_small_ransacked_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PaperBox_01_small_open_brown_F: Land_PaperBox_01_small_open_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PaperBox_01_small_open_brown_IDAP_F: Land_PaperBox_01_small_open_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PaperBox_01_small_open_white_IDAP_F: Land_PaperBox_01_small_open_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PaperBox_01_small_destroyed_brown_IDAP_F: Land_PaperBox_01_small_destroyed_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PaperBox_01_small_destroyed_white_IDAP_F: Land_PaperBox_01_small_destroyed_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PaperBox_01_small_destroyed_brown_F: Land_PaperBox_01_small_destroyed_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PaperBox_01_small_stacked_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PaperBox_01_open_empty_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PaperBox_01_open_boxes_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PaperBox_01_open_water_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_LiquidDispenser_01_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PlasticBucket_01_closed_F: Land_PlasticBucket_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PlasticBucket_01_open_F: Land_PlasticBucket_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FoodSack_01_full_white_idap_F: Land_FoodSack_01_full_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FoodSack_01_full_brown_idap_F: Land_FoodSack_01_full_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FoodSack_01_full_brown_F: Land_FoodSack_01_full_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FoodSack_01_empty_white_idap_F: Land_FoodSack_01_empty_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FoodSack_01_empty_brown_idap_F: Land_FoodSack_01_empty_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FoodSack_01_empty_brown_F: Land_FoodSack_01_empty_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FoodSack_01_dmg_white_idap_F: Land_FoodSack_01_dmg_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FoodSack_01_dmg_brown_idap_F: Land_FoodSack_01_dmg_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FoodSack_01_dmg_brown_F: Land_FoodSack_01_dmg_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FoodSacks_01_small_white_idap_F: Land_FoodSack_01_small_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FoodSacks_01_small_brown_idap_F: Land_FoodSack_01_small_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FoodSacks_01_small_brown_F: Land_FoodSack_01_small_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FoodSacks_01_large_white_idap_F: Land_FoodSack_01_large_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FoodSacks_01_large_brown_idap_F: Land_FoodSack_01_large_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FoodSacks_01_large_brown_F: Land_FoodSack_01_large_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FoodSacks_01_cargo_white_idap_F: Land_FoodSack_01_cargo_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FoodSacks_01_cargo_brown_idap_F: Land_FoodSack_01_cargo_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FoodSacks_01_cargo_brown_F: Land_FoodSack_01_cargo_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WaterBottle_01_full_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WaterBottle_01_empty_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WaterBottle_01_compressed_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WaterBottle_01_cap_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WaterBottle_01_pack_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WaterBottle_01_stack_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CanisterFuel_Red_F: Land_CanisterFuel_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CanisterFuel_Blue_F: Land_CanisterFuel_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CanisterFuel_White_F: Land_CanisterFuel_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FoodContainer_01_White_F: Land_FoodContainer_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PlasticCase_01_large_gray_F: Land_PlasticCase_01_large_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PlasticCase_01_large_idap_F: Land_PlasticCase_01_large_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PlasticCase_01_medium_gray_F: Land_PlasticCase_01_medium_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PlasticCase_01_medium_idap_F: Land_PlasticCase_01_medium_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PlasticCase_01_small_gray_F: Land_PlasticCase_01_small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PlasticCase_01_small_idap_F: Land_PlasticCase_01_small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Orange_01_F: Land_Orange_01_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Orange_01_NoPop_F: Land_Orange_01_Base_F
	{
//		scope = 1;
	};
	class Land_Pumpkin_01_F: Land_Pumpkin_01_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Pumpkin_01_NoPop_F: Land_Pumpkin_01_Base_F
	{
//		scope = 1;
	};
	class Land_Photoframe_01_F: Land_Photoframe_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Photoframe_01_broken_F: Land_Photoframe_01_broken_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FlowerPot_01_F: Land_FlowerPot_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FlowerPot_01_Flower_F: Land_FlowerPot_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wallet_01_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PCSet_01_mousepad_F: Land_PCSet_01_mousepad_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PCSet_01_mousepad_IDAP_F: Land_PCSet_01_mousepad_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Crowbar_01_F: Land_Crowbar_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MedicalTent_01_tropic_closed_F: Land_MedicalTent_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MedicalTent_01_MTP_closed_F: Land_MedicalTent_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MedicalTent_01_greenhex_closed_F: Land_MedicalTent_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MedicalTent_01_brownhex_closed_F: Land_MedicalTent_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MedicalTent_01_digital_closed_F: Land_MedicalTent_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MedicalTent_01_white_IDAP_med_closed_F: Land_MedicalTent_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MedicalTent_01_white_generic_closed_F: Land_MedicalTent_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MedicalTent_01_white_generic_open_F: Land_MedicalTent_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MedicalTent_01_white_generic_outer_F: Land_MedicalTent_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MedicalTent_01_white_generic_inner_F: Land_MedicalTent_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MedicalTent_01_white_IDAP_closed_F: Land_MedicalTent_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MedicalTent_01_white_IDAP_open_F: Land_MedicalTent_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MedicalTent_01_white_IDAP_outer_F: Land_MedicalTent_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MedicalTent_01_floor_light_F: Land_MedicalTent_01_floor_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MedicalTent_01_floor_dark_F: Land_MedicalTent_01_floor_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo10_IDAP_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo20_IDAP_F: Cargo_IDAP_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo40_IDAP_F: Cargo_IDAP_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo20_idap_ruins_F: Cargo_IDAP_ruins_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo40_idap_ruins_F: Cargo_IDAP_ruins_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Sign_MinesTall_F: Land_Sign_MinesTall_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Sign_MinesTall_English_F: Land_Sign_MinesTall_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Sign_MinesTall_Greek_F: Land_Sign_MinesTall_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Sign_MinesDanger_English_F: Land_Sign_MinesDanger_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Sign_MinesDanger_Greek_F: Land_Sign_MinesDanger_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Sign_WarningNoWeapon_F: Land_Sign_WarningMilitaryArea_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WallSign_01_F: Land_WallSign_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WallSign_01_chalkboard_F: Land_WallSign_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PlasticNetFence_01_long_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PlasticNetFence_01_long_d_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PlasticNetFence_01_short_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PlasticNetFence_01_short_d_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PlasticNetFence_01_pole_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PlasticNetFence_01_roll_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class BloodPool_01_Large_Old_F: Blood_01_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class BloodPool_01_Large_New_F: Blood_01_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class BloodPool_01_Medium_Old_F: Blood_01_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class BloodPool_01_Medium_New_F: Blood_01_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class BloodSplatter_01_Large_Old_F: Blood_01_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class BloodSplatter_01_Large_New_F: Blood_01_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class BloodSplatter_01_Medium_Old_F: Blood_01_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class BloodSplatter_01_Medium_New_F: Blood_01_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class BloodSplatter_01_Small_Old_F: Blood_01_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class BloodSplatter_01_Small_New_F: Blood_01_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class BloodSpray_01_Old_F: Blood_01_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class BloodSpray_01_New_F: Blood_01_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class BloodTrail_01_Old_F: Blood_01_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class BloodTrail_01_New_F: Blood_01_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class MedicalGarbage_01_Bandage_F: MedicalGarbage_01_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class MedicalGarbage_01_Injector_F: MedicalGarbage_01_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class MedicalGarbage_01_Packaging_F: MedicalGarbage_01_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class MedicalGarbage_01_Gloves_F: MedicalGarbage_01_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class MedicalGarbage_01_FirstAidKit_F: MedicalGarbage_01_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class MedicalGarbage_01_1x1_v1_F: MedicalGarbage_01_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class MedicalGarbage_01_1x1_v2_F: MedicalGarbage_01_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class MedicalGarbage_01_1x1_v3_F: MedicalGarbage_01_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class MedicalGarbage_01_3x3_v1_F: MedicalGarbage_01_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class MedicalGarbage_01_3x3_v2_F: MedicalGarbage_01_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class MedicalGarbage_01_5x5_v1_F: MedicalGarbage_01_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Tire_Van_02_F: Tire_Van_02_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Tire_Van_02_Spare_F: Tire_Van_02_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Tire_Van_02_Cargo_F: Tire_Van_02_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Tire_Van_02_Transport_F: Tire_Van_02_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Banner_01_F: Banner_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Banner_01_FIA_F: Banner_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Banner_01_CSAT_F: Banner_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Banner_01_AAF_F: Banner_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Banner_01_NATO_F: Banner_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Banner_01_IDAP_F: Banner_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Sign_Sphere10cm_Geometry_F: Helper_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Sign_Sphere25cm_Geometry_F: Helper_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Sign_Sphere100cm_Geometry_F: Helper_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Sign_Sphere200cm_Geometry_F: Helper_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class MemoryFragment_F: Helper_Base_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class Box_IDAP_AmmoOrd_F: Box_IND_AmmoOrd_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Box_IDAP_Equip_F: Box_AAF_Equip_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Box_IDAP_Uniforms_F: Box_NATO_Uniforms_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Leaflet_05_F: Leaflet_05_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Leaflet_05_Old_F: Leaflet_05_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Leaflet_05_New_F: Leaflet_05_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Leaflet_05_Stack_F: Leaflet_05_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class BombCluster_01_UXO1_F: MineBase
	{
		scope = 1;//2
	};
	class APERSMineDispenser_Mine_F: APERSMine
	{
		scope = 1;//2
	};
	class APERSMineDispenser_F: SatchelCharge_F
	{
		scope = 1;//2
	};
	class TrainingMine_01_F: APERSMine
	{
		scope = 1;//2
	};
	class TrainingMine_01_used_F: ThingX
	{
//		scope = 1;
//		scopeCurator = 1;
	};

	class Land_MRL_Magazine_01_F: Items_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_TankRoadWheels_01_single_F: Items_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_TankSprocketWheels_01_single_F: Items_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_TankTracks_01_short_F: Items_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_TankTracks_01_long_F: Items_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_TankEngine_01_F: Items_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_TankEngine_01_used_F: Items_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_TorqueWrench_01_F: Items_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_BoreSighter_01_F: Items_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_RefuelingHose_01_F: Items_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
};
