class CfgPatches
{
	class Optional_HideFromEditor_Sounds_c
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Data_F_Loadorder","A3_Data_F_Curator_Loadorder","A3_Data_F_Kart_Loadorder","A3_Data_F_Bootcamp_Loadorder","A3_Data_F_Heli_Loadorder","A3_Data_F_Mark_Loadorder","A3_Data_F_Exp_A_Loadorder","A3_Data_F_Exp_B_Loadorder","A3_Data_F_Exp_Loadorder","A3_Data_F_Jets_Loadorder","A3_Data_F_Argo_Loadorder","A3_Data_F_Patrol_Loadorder","A3_Data_F_Orange_Loadorder","A3_Data_F_Tacops_Loadorder","A3_Data_F_Tank_Loadorder"};
		version = "2019-03-28";
	};
};
class CfgVehicles
{
	class Sound;

	class Sound_Stream: Sound
	{
//		scope = 1;
	};
	class Sound_Alarm: Sound
	{
		scope = 1;//2
	};
	class Sound_Alarm2: Sound
	{
//		scope = 1;
	};
	class Sound_Fire: Sound
	{
//		scope = 1;
	};
	class Sound_SmokeWreck1: Sound
	{
//		scope = 1;
	};
	class Sound_SparklesWreck1: Sound
	{
//		scope = 1;
	};
	class Sound_SparklesWreck2: Sound
	{
//		scope = 1;
	};
	class Sound_BattlefieldExplosions: Sound
	{
		scope = 1;//2
	};
	class Sound_BattlefieldFirefight: Sound
	{
		scope = 1;//2
	};
	class SoundFlareLoop_F: Sound
	{
//		scope = 1;
	};
	class Simulation_Fatal: Sound
	{
		scope = 1;//2
	};
	class Simulation_Restart: Sound
	{
		scope = 1;//2
	};
	class Topic_Deselection: Sound
	{
		scope = 1;//2
	};
	class Topic_Done: Sound
	{
		scope = 1;//2
	};
	class Topic_Selection: Sound
	{
		scope = 1;//2
	};
	class Topic_Trigger: Sound
	{
		scope = 1;//2
	};
};
