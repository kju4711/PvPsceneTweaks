class CfgPatches
{
	class TransparentBlackBackgroundForChat_Visuals_C_PvPscene
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Data_F_Loadorder","A3_Data_F_Curator_Loadorder","A3_Data_F_Kart_Loadorder","A3_Data_F_Bootcamp_Loadorder","A3_Data_F_Heli_Loadorder","A3_Data_F_Mark_Loadorder","A3_Data_F_Exp_A_Loadorder","A3_Data_F_Exp_B_Loadorder","A3_Data_F_Exp_Loadorder","A3_Data_F_Jets_Loadorder","A3_Data_F_Argo_Loadorder","A3_Data_F_Patrol_Loadorder","A3_Data_F_Orange_Loadorder","A3_Data_F_Tacops_Loadorder","A3_Data_F_Tank_Loadorder"};
		version = "2019-03-28";
	};
};
class RscChatListDefault
{
	colorBackground[] = {0,0,0,0.4};
	colorPlayerBackground[] = {0,0,0,0.3};
};
class RscChatListMission: RscChatListDefault
{
	colorBackground[] = {0,0,0,0.4};
};
class RscChatListBriefing: RscChatListDefault
{
	colorBackground[] = {0,0,0,0.4};
};
class RscChatListMap: RscChatListDefault
{
	colorBackground[] = {0,0,0,0.4};
};
class RscText;
class RscEdit;
class RscDisplayChannel
{
	class controls
	{
		class CA_Channel: RscText
		{
			colorBackground[] = {0,0,0,0};
		};
	};
};
class RscDisplayChat
{
	class controls
	{
		class CA_Background: RscText
		{
			colorBackground[] = {0,0,0,0.4};
		};
		class CA_Line: RscEdit
		{
			colorBackground[] = {0,0,0,0.4};
		};
	};
};
