class CfgPatches
{
	class AddedTankMovementOrderDirectionMarkers_Visuals_C_PvPscene
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Data_F_Loadorder","A3_Data_F_Curator_Loadorder","A3_Data_F_Kart_Loadorder","A3_Data_F_Bootcamp_Loadorder","A3_Data_F_Heli_Loadorder","A3_Data_F_Mark_Loadorder","A3_Data_F_Exp_A_Loadorder","A3_Data_F_Exp_B_Loadorder","A3_Data_F_Exp_Loadorder","A3_Data_F_Jets_Loadorder","A3_Data_F_Argo_Loadorder","A3_Data_F_Patrol_Loadorder","A3_Data_F_Orange_Loadorder","A3_Data_F_Tacops_Loadorder","A3_Data_F_Tank_Loadorder"};
		version = "2019-03-28";
	};
};
class CfgInGameUI
{
	class TankDirection
	{
		imageMoveStop = "\AddedTankMovementOrderDirectionMarkers_Visuals_C_PvPscene\tankdir_stop_ca.paa";
		imageMoveBack = "\AddedTankMovementOrderDirectionMarkers_Visuals_C_PvPscene\tankdir_back_ca.paa";
		imageMoveForward = "\AddedTankMovementOrderDirectionMarkers_Visuals_C_PvPscene\tankdir_forward_ca.paa";
		imageMoveFast = "\AddedTankMovementOrderDirectionMarkers_Visuals_C_PvPscene\tankdir_fast_ca.paa";
		imageMoveLeft = "\AddedTankMovementOrderDirectionMarkers_Visuals_C_PvPscene\tankdir_left_ca.paa";
		imageMoveRight = "\AddedTankMovementOrderDirectionMarkers_Visuals_C_PvPscene\tankdir_right_ca.paa";
		imageMoveAuto = "";
	};
};
