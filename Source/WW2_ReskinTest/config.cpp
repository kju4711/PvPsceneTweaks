﻿class CfgPatches
{
	class WW2_ReskinTest
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"WW2_Terrains_c_Misc_ZZZ_LastLoaded_c","WW2_Objects_c_Misc_I44_ZZZ_LastLoaded_c","WW2_Objects_c_Misc_IF_ZZZ_LastLoaded_c","WW2_Core_c_IF_ZZZ_LastLoaded_c"};
		version = "2017-12-07";
	};
};
class CfgWeapons
{
//	class Throw: GrenadeLauncher
//	{
//		class LIB_rpg6Muzzle: HandGrenadeMuzzle
//		class LIB_pwmMuzzle: HandGrenadeMuzzle
//		class LIB_ThrowMuzzle: ThrowMuzzle
//		class LIB_shg24Muzzle: LIB_ThrowMuzzle
//		class LIB_shg24x7Muzzle: LIB_ThrowMuzzle
//		class LIB_f1Muzzle: LIB_ThrowMuzzle
//		class LIB_rg42Muzzle: LIB_ThrowMuzzle
//		class LIB_m39Muzzle: LIB_ThrowMuzzle
//		class LIB_US_Mk_2Muzzle: LIB_ThrowMuzzle
//	};
//	class Put: Default
//	{
//		class LIB_Muzzle: PutMuzzle
//		class LIB_PP_Muzzle: LIB_Muzzle
//		class LIB_TMI_42_Muzzle: LIB_Muzzle
//		class LIB_SMI_35_1_Muzzle: LIB_Muzzle
//		class LIB_pomzec_Muzzle: LIB_Muzzle
//		class LIB_SMI_35_Muzzle: LIB_Muzzle
//		class LIB_STMI_Muzzle: LIB_Muzzle
//		class LIB_shumine_42_Muzzle: LIB_Muzzle
//		class LIB_Ladung_Small_Muzzle: LIB_Muzzle
//		class LIB_Ladung_Big_Muzzle: LIB_Muzzle
//		class LIB_Ladung_PM_Muzzle: LIB_Muzzle
//		class LIB_M3_Muzzle: LIB_Muzzle
//		class LIB_PMD6_Muzzle: LIB_Muzzle
//		class LIB_TM44_Muzzle: LIB_Muzzle
//		class LIB_TROTIL_Muzzle: LIB_Muzzle
//		class LIB_MARKER_Muzzle: LIB_Muzzle
//		class LIB_US_M1A1_ATMINE_Muzzle: LIB_Muzzle
//		class LIB_US_M3_Muzzle: LIB_Muzzle
//		class LIB_US_TNT_4pound_Muzzle: LIB_Muzzle
//		class LIB_US_BM10_PM_Muzzle: LIB_Muzzle
//	};

	class ItemCore;
	class VestItem;
	class Vest_Camo_Base: ItemCore
	{
		class ItemInfo;
	};
	class V_LIB_Vest_Camo_Base: Vest_Camo_Base
	{
	};

	class H_HelmetB;
	class H_LIB_HelmetB: H_HelmetB
	{
		class ItemInfo;
	};
	class U_BasicBody;
	class U_LIB_BasicBody: U_BasicBody
	{
		class ItemInfo;
	};

	class LIB_Binocular_base;
	class LIB_Head_base;
	class LIB_LAUNCHER;
	class LIB_LMG;
	class LIB_PISTOL;
	class LIB_RIFLE;
	class LIB_Slung_Static_Weapon_Base;
	class LIB_SMG;
	class LIB_SRIFLE;
	class U_LIB_GER_BasicBody_Camo_base;

	class LIB_Headwrap_gloves: LIB_Head_base
	{
//		hiddenSelections[] = {"camo","camo2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Characters\Headgear_t\LEN_Gloves_Wool\Gloves_Wool3_co.paa","WW2\Assets_t\Characters\Headgear_t\LEN_Gloves_Wool\Gloves_Wool3_co.paa"};
	};
	class LIB_GER_Gloves_base: LIB_Head_base
	{
//		hiddenSelections[] = {"camo"};
	};
	class LIB_GER_Gloves1: LIB_GER_Gloves_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Headgear_t\LEN_Gloves_Wool\Gloves_Wool1_co.paa"};
	};
	class LIB_GER_Gloves2: LIB_GER_Gloves_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Headgear_t\LEN_Gloves_Wool\Gloves_Wool2_co.paa"};
	};
	class LIB_GER_Gloves3: LIB_GER_Gloves_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Headgear_t\LEN_Gloves_Wool\Gloves_Wool3_co.paa"};
	};
	class LIB_GER_Gloves4: LIB_GER_Gloves_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Headgear_t\LEN_Gloves_Leather\Gloves_Leather1_co.paa"};
		hiddenSelectionsMaterials[] = {"\WW2\Assets_r\Characters\Headgear_r\LEN_Gloves\Gloves_Leather1.rvmat"};
	};
	class LIB_GER_Gloves5: LIB_GER_Gloves_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Headgear_t\LEN_Gloves_Leather\Gloves_Leather2_co.paa"};
		hiddenSelectionsMaterials[] = {"\WW2\Assets_r\Characters\Headgear_r\LEN_Gloves\Gloves_Leather2.rvmat"};
	};
	class LIB_P38: LIB_PISTOL
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\Pistols_t\IF_P38\Waltherp38_co.paa"};
	};
	class LIB_M1896: LIB_PISTOL
	{
//		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"ww2\assets_t\weapons\pistols_t\csa_m1896\m1896_co.paa"};
	};
	class LIB_WaltherPPK: LIB_PISTOL
	{
//		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"ww2\assets_t\weapons\pistols_t\csa38_ppk\walther_ppk_co.paa"};
	};
	class LIB_TT33: LIB_PISTOL
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\Pistols_t\IF_TT33\TT33_co.paa"};
	};
	class LIB_M1895: LIB_PISTOL
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\Pistols_t\IF_M1895\Nagant_co.paa"};
	};
	class LIB_Colt_M1911: LIB_PISTOL
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\Pistols_t\IF_Colt\Colt1911_Body_co.paa"};
	};
	class LIB_FLARE_PISTOL: LIB_PISTOL
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\Pistols_t\IF_Flarepistol\German_Flarepistol_co.paa"};
	};
	class LIB_MP40: LIB_SMG
	{
//		hiddenSelections[] = {"camo_1","camo_2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"ww2\assets_t\weapons\machinegun_sub_t\csa_mp40\mp40metal_co.paa","ww2\assets_t\weapons\machinegun_sub_t\csa_mp38\mp38bakelite_co.paa"};
	};
	class LIB_MP38: LIB_SMG
	{
//		hiddenSelections[] = {"camo_1","camo_2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"ww2\assets_t\weapons\machinegun_sub_t\csa_mp38\mp38metal_co.paa","ww2\assets_t\weapons\machinegun_sub_t\csa_mp38\mp38bakelite_co.paa"};
	};
	class LIB_PPSh41_m: LIB_SMG
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\MachineGun_Sub_t\IF_PPSh41\PPSh41_co.paa"};
	};
	class LIB_PPSh41_d: LIB_PPSh41_m
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\MachineGun_Sub_t\IF_PPSh41\PPSh41_co.paa"};
	};
	class LIB_M1A1_Thompson: LIB_SMG
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\MachineGun_Sub_t\IF_M1A1_Thompson\M1a1_co.paa"};
	};
	class LIB_K98: LIB_RIFLE
	{
//		hiddenSelections[] = {"camo_1","camo_2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"ww2\assets_t\weapons\rifles_t\csa38_kar98k\mausermetal_co.paa","ww2\assets_t\weapons\rifles_t\csa38_kar98k\mauserwood_co.paa"};
	};
	class LIB_G3340: LIB_K98
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Weapons\Rifles_t\IF_K98\G3340_LEN_co.paa","WW2\Assets_t\Weapons\Rifles_t\IF_K98\Ammo_co.paa"};
	};
	class LIB_M9130: LIB_RIFLE
	{
//		hiddenSelections[] = {"camo_1","camo_2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Weapons\Rifles_t\IF_M9130\M9130_co.paa","WW2\Assets_t\Weapons\Rifles_t\IF_K98\Ammo_co.paa"};
	};
	class LIB_M38: LIB_M9130
	{
//		hiddenSelections[] = {"camo_1","camo_2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Weapons\Rifles_t\IF_M9130\M9130_2_LEN_co.paa","WW2\Assets_t\Weapons\Rifles_t\IF_K98\Ammo_co.paa"};
	};
	class LIB_M44: LIB_M9130
	{
//		hiddenSelections[] = {"camo_1","camo_2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Weapons\Rifles_t\IF_M9130\M9130_3_LEN_co.paa","WW2\Assets_t\Weapons\Rifles_t\IF_K98\Ammo_co.paa"};
	};
	class LIB_M1903A3_Springfield: LIB_RIFLE
	{
//		hiddenSelections[] = {"camo_1","camo_2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Weapons\Rifles_t\WW2_Springfield\Springfield_co.paa","WW2\Assets_t\Weapons\Rifles_t\IF_K98\Ammo_co.paa"};
	};
	class LIB_MP44: LIB_RIFLE
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\Rifles_t\IF_MP44\Mp44_co.paa"};
	};
	class LIB_SVT_40: LIB_RIFLE
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\Rifles_t\IF_Svt40\Svt40_co.paa"};
	};
	class LIB_G43: LIB_RIFLE
	{
//		hiddenSelections[] = {"camo_1","camo_2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Weapons\Rifles_t\IF_G43\G43_Dark_co.paa","WW2\Assets_t\Weapons\Rifles_t\IF_G43\Last_Lod_co.paa"};
	};
	class LIB_M1_Garand: LIB_RIFLE
	{
//		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"ww2\assets_t\weapons\rifles_t\ww2_m1_garand\m1garand_co.paa"};
	};
	class LIB_M1_Carbine: LIB_RIFLE
	{
//		hiddenSelections[] = {"camo_1","camo_2","camo_3","camo_4"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"WW2\Assets_t\Weapons\Rifles_t\IF_M1_Carbine\Metal_co.paa","WW2\Assets_t\Weapons\Rifles_t\IF_M1_Carbine\Wood_co.paa","WW2\Assets_t\Weapons\Rifles_t\IF_M1_Carbine\Magazine_co.paa","WW2\Assets_t\Weapons\Rifles_t\IF_M1_Carbine\Flipsight_co.paa"};
	};
	class LIB_PTRD: LIB_RIFLE
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\SniperRifles_t\LEN_PTRD\PTRD_co.paa"};
	};
	class LIB_DELISLE: LIB_RIFLE
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"ww2\assets_t\weapons\rifles_t\i44_delisle\lisle_co.paa"};
	};
	class LIB_DP28: LIB_LMG
	{
//		hiddenSelections[] = {"camo_1","camo_2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Weapons\MachineGun_Light_t\IF_DP28\Dp28_co.paa","WW2\Assets_t\Weapons\MachineGun_Light_t\IF_DP28\Dp28_Magazine_co.paa"};
	};
	class LIB_DT: LIB_LMG
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\MachineGun_Light_t\IF_DT\DT_co.paa"};
	};
	class LIB_DT_OPTIC: LIB_DT
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\MachineGun_Light_t\IF_DT\DT_Optik_co.paa"};
	};
	class LIB_MG42: LIB_LMG
	{
//		hiddenSelections[] = {"camo_1","camo_2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Weapons\MachineGun_Light_t\IF_MG42\Mg42_co.paa","WW2\Assets_t\Weapons\MachineGun_Light_t\IF_MG42\Drum_co.paa"};
	};
	class LIB_MG34: LIB_LMG
	{
//		hiddenSelections[] = {"camo","camo1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Weapons\MachineGun_Light_t\WW2_MG34\MG34_co.paa","WW2\Assets_t\Weapons\MachineGun_Light_t\IF_MG42\Drum_co.paa"};
	};
	class LIB_M1919A4: LIB_LMG
	{
//		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Weapons\MachineGun_Light_t\WW2_M1919\M1919_co.paa"};
	};
	class LIB_M1918A2_BAR: LIB_LMG
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\MachineGun_Heavy_t\IF_BAR\Bar_1918_co.paa"};
	};
	class LIB_K98ZF39: LIB_SRIFLE
	{
//		hiddenSelections[] = {"camo_1","camo_2","camo_3"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"WW2\Assets_t\Weapons\Rifles_t\IF_K98\K98_co.paa","WW2\Assets_t\Weapons\Rifles_t\IF_K98\Ammo_co.paa","WW2\Assets_t\Weapons\Rifles_t\IF_K98\Zf39_co.paa"};
	};
	class LIB_M9130PU: LIB_SRIFLE
	{
//		hiddenSelections[] = {"camo_1","camo_2","camo_3"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"WW2\Assets_t\Weapons\Rifles_t\IF_M9130\M9130_co.paa","WW2\Assets_t\Weapons\Rifles_t\IF_K98\Ammo_co.paa","WW2\Assets_t\Weapons\Rifles_t\IF_M9130\Pu_co.paa"};
	};
	class LIB_M1903A4_Springfield: LIB_SRIFLE
	{
//		hiddenSelections[] = {"camo_1","camo_2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Weapons\SniperRifles_t\IF_Springfield\Springfield_co.paa","WW2\Assets_t\Weapons\Rifles_t\IF_K98\Ammo_co.paa"};
	};
	class LIB_PzFaust_30m: LIB_LAUNCHER
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\Launchers_t\IF_PzFaust30\Body_co.paa"};
	};
	class LIB_RPzB: LIB_LAUNCHER
	{
//		hiddenSelections[] = {"camo_1","camo_2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Weapons\Launchers_t\IF_RPzB\Panzerschreck_co.paa","WW2\Assets_t\Weapons\Launchers_t\IF_RPzB\Panzerschreck_Rocket_co.paa"};
	};
	class LIB_M1A1_Bazooka: LIB_LAUNCHER
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\Launchers_t\IF_Bazooka\M1a1_Bazooka_co.paa"};
	};
	class LIB_Binocular_GER: LIB_Binocular_base
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\Binoculars_t\IF_German_Binocular\German_Binocular_co.paa"};
	};
	class LIB_Binocular_SU: LIB_Binocular_base
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\Binoculars_t\IF_Russian_Binocular\Russian_Binocular_co.paa"};
	};
	class LIB_M1928_Thompson: LIB_M1A1_Thompson
	{
//		hiddenSelections[] = {"camo","camo1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Weapons\MachineGun_Sub_t\WW2_M1928_Thompson\M1928_co.paa","WW2\Assets_t\Weapons\MachineGun_Sub_t\WW2_M1928_Thompson\M1928A1_co.paa"};
	};
	class LIB_M1928_Thompson_d: LIB_M1928_Thompson
	{
//		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\MachineGun_Sub_t\WW2_M1928_Thompson\M1928_co.paa"};
	};
	class LIB_M1928A1_Thompson: LIB_M1928_Thompson
	{
//		hiddenSelections[] = {"camo","camo1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Weapons\MachineGun_Sub_t\WW2_M1928_Thompson\M1928_co.paa","WW2\Assets_t\Weapons\MachineGun_Sub_t\WW2_M1928_Thompson\M1928A1_co.paa"};
	};
	class LIB_K98_Late: LIB_K98
	{
//		hiddenSelections[] = {"camo_1","camo_2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"ww2\assets_t\weapons\rifles_t\csa38_kar98k\mausermetal_late_co.paa","ww2\assets_t\weapons\rifles_t\csa38_kar98k\mauserwood_late_co.paa"};
	};
	class LIB_M1A1_Carbine: LIB_M1_Carbine
	{
//		hiddenSelections[] = {"camo_1","camo_2","camo_3","camo_4"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"WW2\Assets_t\Weapons\Rifles_t\WW2_M1A1_Carbine\Metal_co.paa","WW2\Assets_t\Weapons\Rifles_t\WW2_M1A1_Carbine\Wood_co.paa","WW2\Assets_t\Weapons\Rifles_t\WW2_M1A1_Carbine\Magazine_co.paa","WW2\Assets_t\Weapons\Rifles_t\WW2_M1A1_Carbine\Flipsight_co.paa"};
	};
	class LIB_MG34_PT: LIB_MG34
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Weapons\MachineGun_Light_t\WW2_MG34\MG34_co.paa","WW2\Assets_t\Weapons\MachineGun_Light_t\WW2_MG34\MG34_Drum_co.paa"};
	};
	class LIB_RPzB_w: LIB_RPzB
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Weapons\Launchers_t\IF_RPzB\Panzerschreck_w_co.paa","WW2\Assets_t\Weapons\Launchers_t\IF_RPzB\Panzerschreck_Rocket_co.paa"};
	};
	class LIB_M2_Tripod: LIB_Slung_Static_Weapon_Base
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"ww2\assets_t\weapons\machinegun_light_t\ww2_m1919\m2_tripod_co.paa"};
	};
	class LIB_BM37_Tripod: LIB_Slung_Static_Weapon_Base
	{
//		hiddenSelections[] = {"camo_0"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\Mortars_t\IF_Bm37\Bm37.paa"};
	};
	class LIB_BM37_Barrel: LIB_Slung_Static_Weapon_Base
	{
//		hiddenSelections[] = {"camo_0"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\Mortars_t\IF_Bm37\Bm37.paa"};
	};
	class LIB_GrWr34_Tripod: LIB_Slung_Static_Weapon_Base
	{
//		hiddenSelections[] = {"camo_0"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\Mortars_t\IF_GrWr34\Grwr_34_co.paa"};
	};
	class LIB_GrWr34_Barrel: LIB_Slung_Static_Weapon_Base
	{
//		hiddenSelections[] = {"camo_0"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\Mortars_t\IF_GrWr34\Grwr_34_co.paa"};
	};
	class LIB_M2_60_Tripod: LIB_Slung_Static_Weapon_Base
	{
//		hiddenSelections[] = {"camo_0"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"ww2\assets_t\weapons\mortars_t\ww2_m2\m2_co.paa"};
	};
	class LIB_M2_60_Barrel: LIB_Slung_Static_Weapon_Base
	{
//		hiddenSelections[] = {"camo_0"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"ww2\assets_t\weapons\mortars_t\ww2_m2\m2_co.paa"};
	};
	class U_LIB_GER_Tank_crew_private: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_0_co.paa"};
	};
	class U_LIB_GER_Tank_crew_unterofficer: U_LIB_GER_Tank_crew_private
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_0_co.paa"};
	};
	class U_LIB_GER_Tank_crew_leutnant: U_LIB_GER_Tank_crew_private
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_0_co.paa"};
	};
	class U_LIB_GER_Spg_crew_private: U_LIB_GER_Tank_crew_private
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Spg_Crew_0_co.paa"};
	};
	class U_LIB_GER_Spg_crew_unterofficer: U_LIB_GER_Tank_crew_private
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Spg_Crew_0_co.paa"};
	};
	class U_LIB_GER_Spg_crew_leutnant: U_LIB_GER_Tank_crew_private
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Spg_Crew_0_co.paa"};
	};
	class H_LIB_GER_TankOfficerCap: H_LIB_HelmetB
	{
//		hiddenSelections[] = {"camo_0","camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_Equipment_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_1_co.paa"};
		class ItemInfo: ItemInfo
		{
//			hiddenSelections[] = {"camo_0","camo_1"};
		};
	};
	class H_LIB_GER_TankPrivateCap: H_LIB_HelmetB
	{
//		hiddenSelections[] = {"camo_0","camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_Equipment_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_1_co.paa"};
		class ItemInfo: ItemInfo
		{
//			hiddenSelections[] = {"camo_0","camo_1"};
		};
	};
	class H_LIB_GER_SPGPrivateCap: H_LIB_HelmetB
	{
//		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_5_co.paa"};
		class ItemInfo: ItemInfo
		{
//			hiddenSelections[] = {"camo"};
		};
	};
	class V_LIB_GER_TankPrivateBelt: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"camo","camoB"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"camo","camoB"};
		};
	};
	class U_LIB_GER_Soldier: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_co.paa"};
	};
	class U_LIB_GER_Soldier2: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Mgunner_0_co.paa"};
	};
	class U_LIB_GER_Soldier3: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Mgunner_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Sniper_0_co.paa"};
	};
	class U_LIB_GER_Schutze: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_co.paa"};
	};
	class U_LIB_GER_Schutze_HBT: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_co.paa"};
	};
	class U_LIB_GER_Soldier_camo: U_LIB_GER_BasicBody_Camo_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa"};
	};
	class U_LIB_GER_Soldier_camo2: U_LIB_GER_BasicBody_Camo_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_co.paa"};
	};
	class U_LIB_GER_Soldier_camo3: U_LIB_GER_BasicBody_Camo_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Sniper_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_co.paa"};
	};
	class U_LIB_GER_Soldier_camo4: U_LIB_GER_BasicBody_Camo_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Mgunner_0_co.paa"};
	};
	class U_LIB_GER_Soldier_camo5: U_LIB_GER_BasicBody_Camo_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Sniper_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Mgunner_0_co.paa"};
	};
	class U_LIB_GER_Pionier: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa"};
	};
	class U_LIB_GER_LW_pilot: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Pilot_0_co.paa"};
	};
	class U_LIB_GER_Officer_camo: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Lieutenant_0_Camo_co.paa"};
	};
	class U_LIB_GER_Funker: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa"};
	};
	class U_LIB_GER_Art_schutze: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Art_Soldier_0_co.paa"};
	};
	class U_LIB_GER_Oberschutze: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_co.paa"};
	};
	class U_LIB_GER_Gefreiter: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_co.paa"};
	};
	class U_LIB_GER_Unterofficer: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_1_co.paa"};
	};
	class U_LIB_GER_Unterofficer_HBT: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_1_co.paa"};
	};
	class U_LIB_GER_Art_unterofficer: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Art_Unterofficer_0_co.paa"};
	};
	class U_LIB_GER_Recruit: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Recruit_0_co.paa"};
	};
	class U_LIB_GER_Medic: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Medic_1_co.paa"};
	};
	class U_LIB_GER_Leutnant: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Officer_0_co.paa"};
	};
	class U_LIB_GER_Art_leutnant: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Art_Officer_0_co.paa"};
	};
	class U_LIB_GER_Oberleutnant: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Officer_0_co.paa"};
	};
	class U_LIB_GER_Hauptmann: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Officer_0_co.paa"};
	};
	class U_LIB_GER_Oberst: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Oberst_co.paa"};
	};
	class U_LIB_GER_Scharfschutze: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Sniper_0_co.paa"};
	};
	class U_LIB_GER_MG_schutze: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Mgunner_0_co.paa"};
	};
	class U_LIB_GER_MG_schutze_HBT: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Mgunner_0_co.paa"};
	};
	class H_LIB_GER_Helmet: H_LIB_HelmetB
	{
//		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\M42_helmet_co.paa"};
		class ItemInfo: ItemInfo
		{
//			hiddenSelections[] = {"camo1"};
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\M42_helmet_co.paa"};
		};
	};
	class H_LIB_GER_Helmet_net: H_LIB_GER_Helmet
	{
//		hiddenSelections[] = {"camo1","camo2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\M42_helmet_co.paa","ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\h_wire_co.paa"};
		class ItemInfo: ItemInfo
		{
//			hiddenSelections[] = {"camo1","camo2"};
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\M42_helmet_co.paa","ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\h_wire_co.paa"};
		};
	};
	class H_LIB_GER_Helmet_ns: H_LIB_GER_Helmet
	{
		class ItemInfo: ItemInfo
		{
		};
	};
	class H_LIB_GER_Helmet_os: H_LIB_GER_Helmet
	{
		class ItemInfo: ItemInfo
		{
		};
	};
	class H_LIB_GER_Helmet_Medic: H_LIB_GER_Helmet
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\M42_helmet_medic_co.paa"};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\M42_helmet_medic_co.paa"};
		};
	};
	class H_LIB_GER_Helmet_painted: H_LIB_GER_Helmet
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\M42_helmet_painted1_co.paa"};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\M42_helmet_painted1_co.paa"};
		};
	};
	class H_LIB_GER_Helmet_net_painted: H_LIB_GER_Helmet_net
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\M42_helmet_painted2_co.paa","ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\h_wire_co.paa"};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\M42_helmet_painted2_co.paa"};
		};
	};
	class H_LIB_GER_Helmet_ns_painted: H_LIB_GER_Helmet_ns
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\M42_helmet_painted2_co.paa"};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\M42_helmet_painted2_co.paa"};
		};
	};
	class H_LIB_GER_Helmet_os_painted: H_LIB_GER_Helmet_os
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\M42_helmet_painted3_co.paa"};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\M42_helmet_painted3_co.paa"};
		};
	};
	class H_LIB_GER_Helmet_Glasses: H_LIB_GER_Helmet
	{
//		hiddenSelections[] = {"camo1","camo2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\M42_helmet_co.paa","ww2\assets_t\characters\germans_t\if_ger_wehrmacht\german_pilot_0_co.paa"};
		class ItemInfo: ItemInfo
		{
//			hiddenSelections[] = {"camo1","camo2"};
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\M42_helmet_co.paa","ww2\assets_t\characters\germans_t\if_ger_wehrmacht\german_pilot_0_co.paa"};
		};
	};
	class H_LIB_GER_HelmetUtility: H_LIB_GER_Helmet
	{
//		hiddenSelections[] = {"camo1","camo2","camo3","camo4"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","",""};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\M42_helmet_co.paa","ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\H_UtilityStrap_co.paa","",""};
		class ItemInfo: ItemInfo
		{
//			hiddenSelections[] = {"camo1","camo2","camo3","camo4"};
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","",""};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\M42_helmet_co.paa","ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\H_UtilityStrap_co.paa","",""};
		};
	};
	class H_LIB_GER_HelmetUtility_Grass: H_LIB_GER_HelmetUtility
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)",""};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\M42_helmet_co.paa","ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\H_UtilityStrap_co.paa","ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\v_grassdeadbunch_ca.paa",""};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)",""};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\M42_helmet_co.paa","ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\H_UtilityStrap_co.paa","ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\v_grassdeadbunch_ca.paa",""};
		};
	};
	class H_LIB_GER_HelmetUtility_Oak: H_LIB_GER_HelmetUtility
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","","ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\V_Oak_ca.paa"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\M42_helmet_co.paa","ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\H_UtilityStrap_co.paa","","ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\V_Oak_ca.paa"};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","","ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\V_Oak_ca.paa"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\M42_helmet_co.paa","ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\H_UtilityStrap_co.paa","","ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\V_Oak_ca.paa"};
		};
	};
	class H_LIB_GER_HelmetCamo: H_LIB_GER_Helmet
	{
//		hiddenSelections[] = {"camo1","camo2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\M42_helmet_co.paa","ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\h_suz_splinter_co.paa"};
		class ItemInfo: ItemInfo
		{
//			hiddenSelections[] = {"camo1","camo2"};
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\M42_helmet_co.paa","ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\h_suz_splinter_co.paa"};
		};
	};
	class H_LIB_GER_HelmetCamo2: H_LIB_GER_Helmet
	{
//		hiddenSelections[] = {"camo1","camo2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\M42_helmet_co.paa","ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\h_suz_ss_splinter_co.paa"};
		class ItemInfo: ItemInfo
		{
//			hiddenSelections[] = {"camo1","camo2"};
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\M42_helmet_co.paa","ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\h_suz_ss_splinter_co.paa"};
		};
	};
	class H_LIB_GER_HelmetCamo3: H_LIB_GER_Helmet
	{
//		hiddenSelections[] = {"camo1","camo2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\M42_helmet_co.paa","ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\H_cover_burlap_co.paa"};
		class ItemInfo: ItemInfo
		{
//			hiddenSelections[] = {"camo1","camo2"};
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\M42_helmet_co.paa","ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\H_cover_burlap_co.paa"};
		};
	};
	class H_LIB_GER_HelmetCamo4: H_LIB_GER_Helmet
	{
//		hiddenSelections[] = {"camo1","camo2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\M42_helmet_co.paa","ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\H_cover_zeltbahn_splinter_co.paa"};
		hiddenSelectionsMaterials[] = {"\ww2\assets_r\characters\germans_r\ww2_ger_wehrmacht\m42.rvmat","ww2\assets_r\characters\germans_r\ww2_ger_wehrmacht\H_Cover_Zeltbahn.rvmat"};
		class ItemInfo: ItemInfo
		{
//			hiddenSelections[] = {"camo1","camo2"};
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\M42_helmet_co.paa","ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\H_cover_zeltbahn_splinter_co.paa"};
			hiddenSelectionsMaterials[] = {"\ww2\assets_r\characters\germans_r\ww2_ger_wehrmacht\m42.rvmat","ww2\assets_r\characters\germans_r\ww2_ger_wehrmacht\H_Cover_Zeltbahn.rvmat"};
		};
	};
	class H_LIB_GER_OfficerCap: H_LIB_HelmetB
	{
//		hiddenSelections[] = {"Camo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_1_co.paa"};
	};
	class H_LIB_GER_Cap: H_LIB_HelmetB
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_5_co.paa"};
	};
	class H_LIB_GER_Fieldcap: H_LIB_HelmetB
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Sturmtroopers_SS_FC_w_LEN_co.paa"};
	};
	class H_LIB_GER_LW_PilotHelmet: H_LIB_HelmetB
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Pilot_0_co.paa"};
	};
	class H_LIB_GER_Ushanka: H_LIB_HelmetB
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Ushanka_LEN_co.paa"};
	};
	class V_LIB_GER_VestMP40: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"Camo","CamoB"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"Camo","CamoB"};
		};
	};
	class V_LIB_GER_VestSTG: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"Camo","CamoB"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"Camo","CamoB"};
		};
	};
	class V_LIB_GER_VestKar98: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"Camo","CamoB"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"Camo","CamoB"};
		};
	};
	class V_LIB_GER_VestG43: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"Camo","CamoB"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"Camo","CamoB"};
		};
	};
	class V_LIB_GER_SniperBelt: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"Camo","CamoB"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"Camo","CamoB"};
		};
	};
	class V_LIB_GER_VestMG: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"Camo","CamoB"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"Camo","CamoB"};
		};
	};
	class V_LIB_GER_VestUnterofficer: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"Camo","CamoB","Camo1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_Equipment_0_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"Camo","CamoB","Camo1"};
		};
	};
	class V_LIB_GER_FieldOfficer: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"Camo","CamoB","Camo1","Camo2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Oberst_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Officer_belt.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"Camo","CamoB","Camo1","Camo2"};
		};
	};
	class V_LIB_GER_OfficerVest: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"Camo","CamoB","Camo1","Camo2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Oberst_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Officer_belt.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"Camo","CamoB","Camo1","Camo2"};
		};
	};
	class V_LIB_GER_OfficerBelt: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"Camo1","Camo2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Oberst_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Officer_belt.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"Camo1","Camo2"};
		};
	};
	class V_LIB_GER_PrivateBelt: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"Camo","CamoB"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"Camo","CamoB"};
		};
	};
	class V_LIB_GER_PioneerVest: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"Camo","CamoB"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"Camo","CamoB"};
		};
	};
	class U_LIB_WP_Soldier_camo_1: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Polish_t\IF_GUER_PolishTroops\Guer_Soldier_0_Camo_co.paa"};
	};
	class U_LIB_WP_Soldier_camo_2: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Polish_t\IF_GUER_PolishTroops\Guer_Soldier_1_Camo_co.paa"};
	};
	class U_LIB_WP_Soldier_camo_3: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Polish_t\IF_GUER_PolishTroops\Guer_Soldier_2_Camo_co.paa"};
	};
	class H_LIB_WP_Helmet: H_LIB_HelmetB
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Polish_t\IF_GUER_PolishTroops\Guer_Equipment_2_co.paa"};
	};
	class H_LIB_WP_Helmet_camo: H_LIB_HelmetB
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Polish_t\IF_GUER_PolishTroops\Guer_Equipment_1_co.paa"};
	};
	class H_LIB_WP_Helmet_med: H_LIB_HelmetB
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Polish_t\IF_GUER_PolishTroops\Guer_Equipment_1_co.paa"};
	};
	class H_LIB_WP_Cap: H_LIB_HelmetB
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Polish_t\IF_GUER_PolishTroops\Rogatywka_co.paa"};
	};
	class V_LIB_WP_MP40Vest: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"camo_0","camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"camo_0","camo_1"};
		};
	};
	class V_LIB_WP_STGVest: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"Camo","CamoB"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"Camo","CamoB"};
		};
	};
	class V_LIB_WP_Kar98Vest: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"camo_0","camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"camo_0","camo_1"};
		};
	};
	class V_LIB_WP_G43Vest: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"camo_0","camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"camo_0","camo_1"};
		};
	};
	class V_LIB_WP_SniperBela: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"camo_0","camo_1","camo_2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_Equipment_0_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"camo_0","camo_1","camo_2"};
		};
	};
	class V_LIB_WP_MGVest: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"Camo","CamoB"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"Camo","CamoB"};
		};
	};
	class V_LIB_WP_OfficerVest: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"Camo","CamoB","Camo1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_Equipment_0_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"Camo","CamoB","Camo1"};
		};
	};
	class U_LIB_SOV_Strelok: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_1_co.paa"};
	};
	class U_LIB_SOV_Strelok_summer: U_LIB_SOV_Strelok
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_0_co.paa"};
	};
	class U_LIB_SOV_Strelokart: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Arty_Tank_Crew_co.paa"};
	};
	class U_LIB_SOV_Tank_private_field: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Tank_Crew_0_co.paa"};
	};
	class U_LIB_SOV_Starshina: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_0_co.paa"};
	};
	class U_LIB_SOV_Efreitor: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_1_co.paa"};
	};
	class U_LIB_SOV_Efreitor_summer: U_LIB_SOV_Efreitor
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_0_co.paa"};
	};
	class U_LIB_SOV_Sergeant: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_0_co.paa"};
	};
	class U_LIB_SOV_Sergeant_inset_pocket: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Tank_Crew_0_co.paa"};
	};
	class U_LIB_SOV_Stsergeant: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_0_co.paa"};
	};
	class U_LIB_SOV_Leutenant: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Lieutenant_1_co.paa"};
	};
	class U_LIB_SOV_Pilot: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Pilot_0_co.paa"};
	};
	class U_LIB_SOV_Leutenant_inset_pocket: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Tank_Crew_0_co.paa"};
	};
	class U_LIB_SOV_Artleutenant: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Officer_0_co.paa"};
	};
	class U_LIB_SOV_Stleutenant: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Lieutenant_1_co.paa"};
	};
	class U_LIB_SOV_Kapitan: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Officer_1_co.paa"};
	};
	class U_LIB_SOV_Kapitan_summer: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Officer_0_co.paa"};
	};
	class U_LIB_SOV_Razvedchik_am: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_co.paa"};
	};
	class U_LIB_SOV_Razvedchik_lis: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_1_co.paa"};
	};
	class U_LIB_SOV_Razvedchik_mix: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_2_co.paa"};
	};
	class U_LIB_SOV_Razvedchik_autumn: U_LIB_SOV_Razvedchik_mix
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_3_co.paa"};
	};
	class U_LIB_SOV_Sniper: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Sniper_0_co.paa"};
	};
	class U_LIB_SOV_Sniper_spring: U_LIB_SOV_Sniper
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Sniper_1_co.paa"};
	};
	class U_LIB_SOV_Sniper_autumn: U_LIB_SOV_Sniper
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Sniper_2_co.paa"};
	};
	class U_LIB_SOV_Tank_ryadovoi: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_co.paa"};
	};
	class U_LIB_SOV_Tank_sergeant: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_co.paa"};
	};
	class U_LIB_SOV_Tank_leutenant: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_co.paa"};
	};
	class U_LIB_SOV_Tank_kapitan: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_co.paa"};
	};
	class H_LIB_SOV_RA_PrivateCap: H_LIB_HelmetB
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_2_co.paa"};
	};
	class H_LIB_SOV_RA_OfficerCap: H_LIB_HelmetB
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Furajka_co.paa"};
	};
	class H_LIB_SOV_RA_Helmet: H_LIB_HelmetB
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_1_co.paa"};
	};
	class H_LIB_SOV_TankHelmet: H_LIB_HelmetB
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_2_co.paa"};
	};
	class H_LIB_SOV_PilotHelmet: H_LIB_HelmetB
	{
//		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Pilot_Eqip_0_co.paa"};
	};
	class H_LIB_SOV_Ushanka: H_LIB_HelmetB
	{
//		hiddenSelections[] = {"camo_0","camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Ushanka_LEN_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_2_co.paa"};
	};
	class H_LIB_SOV_Ushanka2: H_LIB_SOV_Ushanka
	{
//		hiddenSelections[] = {"camo_0","camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Ushanka2_LEN_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_2_co.paa"};
	};
	class V_LIB_SOV_IShBrVestMG: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"camo_0","camo_1","camo_2","camo_3"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_3_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Cuirass_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"camo_0","camo_1","camo_2","camo_3"};
		};
	};
	class V_LIB_SOV_IShBrVestPPShMag: V_LIB_SOV_IShBrVestMG
	{
//		hiddenSelections[] = {"camo_0","camo_1","camo_2","camo_3","camo_4"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)"};//{"WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_3_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Cuirass_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Furajka_co.paa"};
		class ItemInfo: ItemInfo
		{
//			hiddenSelections[] = {"camo_0","camo_1","camo_2","camo_3","camo_4"};
		};
	};
	class V_LIB_SOV_IShBrVestPPShDisc: V_LIB_SOV_IShBrVestMG
	{
//		hiddenSelections[] = {"camo_0","camo_1","camo_2","camo_3"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_3_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Cuirass_co.paa"};
		class ItemInfo: ItemInfo
		{
//			hiddenSelections[] = {"camo_0","camo_1","camo_2","camo_3"};
		};
	};
	class V_LIB_SOV_RA_OfficerVest: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"camo_0","camo_1","camo_2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Officer_1_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_3_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"camo_0","camo_1","camo_2"};
		};
	};
	class V_LIB_SOV_RA_SniperVest: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"camo_0","camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_2_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_3_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"camo_0","camo_1"};
		};
	};
	class V_LIB_SOV_RA_Belt: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"camo_0"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_2_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"camo_0"};
		};
	};
	class V_LIB_SOV_RA_TankOfficerSet: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"camo_0","camo_1","camo_2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Officer_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_3_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"camo_0","camo_1","camo_2"};
		};
	};
	class V_LIB_SOV_RA_PPShBelt: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"camo_0","camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_2_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_0_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"camo_0","camo_1"};
		};
	};
	class V_LIB_SOV_RA_MosinBelt: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"camo_0","camo_1","camo_2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_2_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_3_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"camo_0","camo_1","camo_2"};
		};
	};
	class V_LIB_SOV_RA_SVTBelt: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"camo_0","camo_1","camo_2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_2_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_3_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"camo_0","camo_1","camo_2"};
		};
	};
	class V_LIB_SOV_RA_MGBelt: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"camo_0","camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_2_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_0_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"camo_0","camo_1"};
		};
	};
	class V_LIB_SOV_RAZV_SVTBelt: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"camo_0","camo_1","camo_2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_2_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_3_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"camo_0","camo_1","camo_2"};
		};
	};
	class V_LIB_SOV_RAZV_OfficerVest: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"camo_0","camo_1","camo_2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Officer_1_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_3_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"camo_0","camo_1","camo_2"};
		};
	};
	class V_LIB_SOV_RAZV_MGBelt: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"camo_0","camo_1","camo_2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_2_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_3_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"camo_0","camo_1","camo_2"};
		};
	};
	class V_LIB_SOV_RAZV_PPShBelt: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"camo_0","camo_1","camo_2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_2_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_3_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"camo_0","camo_1","camo_2"};
		};
	};
	class U_LIB_US_AB_Uniform_M42: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Tankcrew_01_co.paa"};
	};
	class U_LIB_US_AB_Uniform_M43: U_LIB_US_AB_Uniform_M42
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Tankcrew_01_co.paa"};
	};
	class H_LIB_US_AB_Helmet: H_LIB_HelmetB
	{
//		hiddenSelections[] = {"camo","camo0","camo1","camo2","camo3","camo4","camo5","camo6"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa"};//{"WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\US_Helmet_co.paa","WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\us_helmet_net1_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\us_helmet_net1_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\US_Para_First_Aid_co.paa","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa"};
		class ItemInfo: ItemInfo
		{
//			hiddenSelections[] = {"camo","camo0","camo1","camo2","camo3","camo4","camo5","camo6"};
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa"};//{"WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\US_Helmet_co.paa","WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\us_helmet_net1_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\us_helmet_net1_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\US_Para_First_Aid_co.paa","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa"};
		};
	};
	class H_LIB_US_AB_Helmet_2: H_LIB_US_AB_Helmet
	{
		class ItemInfo: ItemInfo
		{
		};
	};
	class H_LIB_US_AB_Helmet_3: H_LIB_US_AB_Helmet
	{
		class ItemInfo: ItemInfo
		{
		};
	};
	class H_LIB_US_AB_Helmet_4: H_LIB_US_AB_Helmet
	{
		class ItemInfo: ItemInfo
		{
		};
	};
	class H_LIB_US_AB_Helmet_5: H_LIB_US_AB_Helmet
	{
		class ItemInfo: ItemInfo
		{
		};
	};
	class H_LIB_US_AB_Helmet_Clear_1: H_LIB_US_AB_Helmet_3
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","","","","","",""};//{"WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\US_Helmet_co.paa","WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\us_helmet_net1_ca.paa","","","","","",""};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","","","","","",""};//{"WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\US_Helmet_co.paa","WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\us_helmet_net1_ca.paa","","","","","",""};
		};
	};
	class H_LIB_US_AB_Helmet_Clear_2: H_LIB_US_AB_Helmet_4
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","","","","","",""};//{"WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\US_Helmet_co.paa","WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\us_helmet_net1_ca.paa","","","","","",""};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","","","","","",""};//{"WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\US_Helmet_co.paa","WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\us_helmet_net1_ca.paa","","","","","",""};
		};
	};
	class H_LIB_US_AB_Helmet_Clear_3: H_LIB_US_AB_Helmet_5
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","","","","","",""};//{"WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\US_Helmet_co.paa","WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\us_helmet_net1_ca.paa","","","","","",""};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","","","","","",""};//{"WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\US_Helmet_co.paa","WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\us_helmet_net1_ca.paa","","","","","",""};
		};
	};
	class H_LIB_US_AB_Helmet_Medic_1: H_LIB_US_AB_Helmet_4
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","","","","","",""};//{"WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\US_Helmet_medic_co.paa","WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\us_helmet_net1_ca.paa","","","","","",""};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","","","","","",""};//{"WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\US_Helmet_medic_co.paa","WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\us_helmet_net1_ca.paa","","","","","",""};
		};
	};
	class H_LIB_US_AB_Helmet_Plain_1: H_LIB_US_AB_Helmet_3
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","","","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa"};//{"WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\US_Helmet_co.paa","","","","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa"};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","","","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa"};//{"WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\US_Helmet_co.paa","","","","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa"};
		};
	};
	class H_LIB_US_AB_Helmet_Plain_2: H_LIB_US_AB_Helmet_4
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","","","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa"};//{"WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\US_Helmet_co.paa","","","","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa"};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","","","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa"};//{"WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\US_Helmet_co.paa","","","","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa"};
		};
	};
	class H_LIB_US_AB_Helmet_Plain_3: H_LIB_US_AB_Helmet_5
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","","","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa"};//{"WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\US_Helmet_co.paa","","","","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa"};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","","","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa"};//{"WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\US_Helmet_co.paa","","","","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa"};
		};
	};
	class H_LIB_US_AB_Helmet_NCO_1: H_LIB_US_AB_Helmet_3
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_nco_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa"};//{"WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\US_Helmet_co.paa","WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\us_helmet_net1_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\us_helmet_net1_ca.paa","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_nco_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa"};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_nco_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa"};//{"WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\US_Helmet_co.paa","WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\us_helmet_net1_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\us_helmet_net1_ca.paa","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_nco_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa"};
		};
	};
	class H_LIB_US_AB_Helmet_NCO_2: H_LIB_US_AB_Helmet_5
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_nco_ca.paa","",""};//{"WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\US_Helmet_co.paa","WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\us_helmet_net1_ca.paa","","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_nco_ca.paa","",""};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_nco_ca.paa","",""};//{"WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\US_Helmet_co.paa","WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\us_helmet_net1_ca.paa","","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_nco_ca.paa","",""};
		};
	};
	class H_LIB_US_AB_Helmet_CO_1: H_LIB_US_AB_Helmet_4
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_co_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa"};//{"WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\US_Helmet_co.paa","WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\us_helmet_net1_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\us_helmet_net1_ca.paa","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_co_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa"};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_co_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa"};//{"WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\US_Helmet_co.paa","WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\us_helmet_net1_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\us_helmet_net1_ca.paa","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_co_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Para_Spades_ca.paa"};
		};
	};
	class H_LIB_US_AB_Helmet_CO_2: H_LIB_US_AB_Helmet_2
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_co_ca.paa","",""};//{"WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\US_Helmet_co.paa","WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\us_helmet_net1_ca.paa","","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_co_ca.paa","",""};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_co_ca.paa","",""};//{"WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\US_Helmet_co.paa","WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\us_helmet_net1_ca.paa","","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_co_ca.paa","",""};
		};
	};
	class V_LIB_US_AB_Vest_Bar: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"Camo","CamoB"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"Camo","CamoB"};
		};
	};
	class V_LIB_US_AB_Vest_Carbine: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"Camo","CamoB"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"Camo","CamoB"};
		};
	};
	class V_LIB_US_AB_Vest_Carbine_nco: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"Camo","CamoB"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"Camo","CamoB"};
		};
	};
	class V_LIB_US_AB_Vest_Garand: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"Camo","CamoB"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"Camo","CamoB"};
		};
	};
	class V_LIB_US_AB_Vest_Grenadier: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"Camo","CamoB"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"Camo","CamoB"};
		};
	};
	class V_LIB_US_AB_Vest_Thompson: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"Camo","CamoB"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"Camo","CamoB"};
		};
	};
	class V_LIB_US_AB_Vest_Thompson_nco: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"Camo","CamoB"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"Camo","CamoB"};
		};
	};
	class U_LIB_US_Private: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Tankcrew_01_co.paa"};
	};
	class U_LIB_US_Private_1st: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Tankcrew_01_co.paa"};
	};
	class U_LIB_US_Corp: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Tankcrew_01_co.paa"};
	};
	class U_LIB_US_Sergant: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Tankcrew_01_co.paa"};
	};
	class U_LIB_US_Snipe: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Tankcrew_01_co.paa"};
	};
	class U_LIB_US_Eng: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Tankcrew_01_co.paa"};
	};
	class U_LIB_US_Med: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Tankcrew_01_co.paa"};
	};
	class U_LIB_US_Off: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Tankcrew_01_co.paa"};
	};
	class U_LIB_US_Cap: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Tankcrew_01_co.paa"};
	};
	class H_LIB_US_Helmet: H_LIB_HelmetB
	{
//		hiddenSelections[] = {"camo","camo0","camo3","camo4","camo5","camo6"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","","","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_co.paa","","","","",""};
		class ItemInfo: ItemInfo
		{
//			hiddenSelections[] = {"camo","camo0","camo3","camo4","camo5","camo6"};
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","","","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_co.paa","","","","",""};
		};
	};
	class H_LIB_US_Helmet_ns: H_LIB_US_Helmet
	{
		class ItemInfo: ItemInfo
		{
		};
	};
	class H_LIB_US_Helmet_os: H_LIB_US_Helmet
	{
		class ItemInfo: ItemInfo
		{
		};
	};
	class H_LIB_US_Helmet_Net: H_LIB_US_Helmet
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","","","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_co.paa","WW2\Assets_t\Characters\Americans_t\WW2_US_Army\us_helmet_net1_ca.paa","","","",""};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","","","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_co.paa","WW2\Assets_t\Characters\Americans_t\WW2_US_Army\us_helmet_net1_ca.paa","","","",""};
		};
	};
	class H_LIB_US_Helmet_Net_ns: H_LIB_US_Helmet_ns
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","","","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_co.paa","WW2\Assets_t\Characters\Americans_t\WW2_US_Army\us_helmet_net1_ca.paa","","","",""};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","","","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_co.paa","WW2\Assets_t\Characters\Americans_t\WW2_US_Army\us_helmet_net1_ca.paa","","","",""};
		};
	};
	class H_LIB_US_Helmet_Net_os: H_LIB_US_Helmet_os
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","","","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_co.paa","WW2\Assets_t\Characters\Americans_t\WW2_US_Army\us_helmet_net1_ca.paa","","","",""};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","","","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_co.paa","WW2\Assets_t\Characters\Americans_t\WW2_US_Army\us_helmet_net1_ca.paa","","","",""};
		};
	};
	class H_LIB_US_Helmet_Med: H_LIB_US_Helmet
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","","","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_medic_co.paa","","","","",""};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","","","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_medic_co.paa","","","","",""};
		};
	};
	class H_LIB_US_Helmet_Med_ns: H_LIB_US_Helmet_ns
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","","","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_medic_co.paa","","","","",""};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","","","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_medic_co.paa","","","","",""};
		};
	};
	class H_LIB_US_Helmet_Med_os: H_LIB_US_Helmet_os
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","","","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_medic_co.paa","","","","",""};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","","","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_medic_co.paa","","","","",""};
		};
	};
	class H_LIB_US_Helmet_Cap: H_LIB_US_Helmet_ns
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Captain_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_co_ca.paa","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_co.paa","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Captain_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_co_ca.paa","",""};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Captain_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_co_ca.paa","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_co.paa","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Captain_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_co_ca.paa","",""};
		};
	};
	class H_LIB_US_Helmet_CO: H_LIB_US_Helmet_ns
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_co_ca.paa","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_co.paa","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_co_ca.paa","",""};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_co_ca.paa","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_co.paa","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_co_ca.paa","",""};
		};
	};
	class H_LIB_US_Helmet_NCO: H_LIB_US_Helmet_CO
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_nco_ca.paa","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_co.paa","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_nco_ca.paa","",""};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_nco_ca.paa","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_co.paa","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_nco_ca.paa","",""};
		};
	};
	class H_LIB_US_Helmet_First_lieutenant: H_LIB_US_Helmet
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_1stLieut_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_co_ca.paa","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_co.paa","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_1stLieut_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_co_ca.paa","",""};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_1stLieut_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_co_ca.paa","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_co.paa","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_1stLieut_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_co_ca.paa","",""};
		};
	};
	class H_LIB_US_Helmet_Second_lieutenant: H_LIB_US_Helmet
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_2ndLieut_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_co_ca.paa","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_co.paa","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_2ndLieut_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_co_ca.paa","",""};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_2ndLieut_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_co_ca.paa","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_co.paa","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_2ndLieut_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_co_ca.paa","",""};
		};
	};
	class U_LIB_US_Tank_Crew: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Tankcrew_01_co.paa"};
	};
	class U_LIB_US_Tank_Crew2: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Tankcrew_02_co.paa"};
	};
	class H_LIB_US_Helmet_Tank: H_LIB_HelmetB
	{
//		hiddenSelections[] = {"camo","camo1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Tankhelmet_co.paa","WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Tankhelmet_co.paa"};
	};
	class H_LIB_US_Helmet_Pilot: H_LIB_HelmetB
	{
//		hiddenSelections[] = {"camo","camo1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Pilothelmet_co.paa","WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Pilothelmet_co.paa"};
	};
	class V_LIB_US_Vest_Bar: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"Camo","CamoB"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"Camo","CamoB"};
		};
	};
	class V_LIB_US_Vest_Asst_MG: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"Camo","CamoB"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"Camo","CamoB"};
		};
	};
	class V_LIB_US_Vest_Carbine: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"Camo","CamoB"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"Camo","CamoB"};
		};
	};
	class V_LIB_US_Vest_Carbine_eng: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"Camo","CamoB"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"Camo","CamoB"};
		};
	};
	class V_LIB_US_Vest_Carbine_nco: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"Camo","CamoB"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"Camo","CamoB"};
		};
	};
	class V_LIB_US_Vest_Garand: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"Camo","CamoB"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"Camo","CamoB"};
		};
	};
	class V_LIB_US_Vest_Grenadier: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"Camo","CamoB"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"Camo","CamoB"};
		};
	};
	class V_LIB_US_Vest_Medic: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"Camo","CamoB"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"Camo","CamoB"};
		};
	};
	class V_LIB_US_Vest_Medic2: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"Camo","CamoB"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"Camo","CamoB"};
		};
	};
	class V_LIB_US_Vest_Thompson: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"Camo","CamoB"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"Camo","CamoB"};
		};
	};
	class V_LIB_US_Vest_Thompson_nco: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"Camo","CamoB"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"Camo","CamoB"};
		};
	};
	class V_LIB_US_Vest_45: V_LIB_Vest_Camo_Base
	{
//		hiddenSelections[] = {"Camo","CamoB"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_co.paa"};
		class ItemInfo: VestItem
		{
//			hiddenSelections[] = {"Camo","CamoB"};
		};
	};
	class U_LIB_US_Rangers_Uniform: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Tankcrew_01_co.paa"};
	};
	class U_LIB_US_Rangers_Private_1st: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Tankcrew_01_co.paa"};
	};
	class U_LIB_US_Rangers_Corp: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Tankcrew_01_co.paa"};
	};
	class U_LIB_US_Rangers_Sergant: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Tankcrew_01_co.paa"};
	};
	class U_LIB_US_Rangers_Eng: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Tankcrew_01_co.paa"};
	};
	class U_LIB_US_Rangers_Med: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Tankcrew_01_co.paa"};
	};
	class H_LIB_US_Rangers_Helmet: H_LIB_HelmetB
	{
//		hiddenSelections[] = {"camo","camo0","camo3","camo4","camo5","camo6"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_2ndRanger_Pvt_ca.paa","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_co.paa","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_2ndRanger_Pvt_ca.paa","",""};
		class ItemInfo: ItemInfo
		{
//			hiddenSelections[] = {"camo","camo0","camo3","camo4","camo5","camo6"};
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_2ndRanger_Pvt_ca.paa","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_co.paa","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_2ndRanger_Pvt_ca.paa","",""};
		};
	};
	class H_LIB_US_Rangers_Helmet_ns: H_LIB_US_Rangers_Helmet
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_2ndRanger_Pvt_ca.paa","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_co.paa","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_2ndRanger_Pvt_ca.paa","",""};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_2ndRanger_Pvt_ca.paa","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_co.paa","","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_2ndRanger_Pvt_ca.paa","",""};
		};
	};
	class H_LIB_US_Rangers_Helmet_os: H_LIB_US_Rangers_Helmet
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_2ndRanger_Pvt_ca.paa","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_co.paa","WW2\Assets_t\Characters\Americans_t\WW2_US_Army\us_helmet_net1_ca.paa","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_2ndRanger_Pvt_ca.paa","",""};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_2ndRanger_Pvt_ca.paa","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_co.paa","WW2\Assets_t\Characters\Americans_t\WW2_US_Army\us_helmet_net1_ca.paa","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_2ndRanger_Pvt_ca.paa","",""};
		};
	};
	class H_LIB_US_Rangers_Helmet_Cap: H_LIB_US_Rangers_Helmet_ns
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Captain_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_2ndRanger_ca.paa","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_co.paa","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Captain_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_2ndRanger_ca.paa","",""};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Captain_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_2ndRanger_ca.paa","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_co.paa","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Captain_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_2ndRanger_ca.paa","",""};
		};
	};
	class H_LIB_US_Rangers_Helmet_First_lieutenant: H_LIB_US_Rangers_Helmet
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_1stLieut_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_2ndRanger_ca.paa","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_co.paa","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_1stLieut_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_2ndRanger_ca.paa","",""};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_1stLieut_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_2ndRanger_ca.paa","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_co.paa","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_1stLieut_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_2ndRanger_ca.paa","",""};
		};
	};
	class H_LIB_US_Rangers_Helmet_Second_lieutenant: H_LIB_US_Rangers_Helmet
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_2ndLieut_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_2ndRanger_ca.paa","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_co.paa","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_2ndLieut_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_2ndRanger_ca.paa","",""};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_2ndLieut_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_2ndRanger_ca.paa","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_co.paa","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_2ndLieut_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_2ndRanger_ca.paa","",""};
		};
	};
	class H_LIB_CIV_Worker_Cap_1: H_LIB_HelmetB
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Characters\Civilians_t\RDS_JOA_Worker\Worker_co.paa"};
//		hiddenSelections[] = {"Camo"};
	};
	class H_LIB_CIV_Worker_Cap_2: H_LIB_CIV_Worker_Cap_1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Characters\Civilians_t\RDS_JOA_Worker\Worker2_co.paa"};
	};
	class H_LIB_CIV_Worker_Cap_3: H_LIB_CIV_Worker_Cap_1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Characters\Civilians_t\RDS_JOA_Worker\Worker3_co.paa"};
	};
	class H_LIB_CIV_Worker_Cap_4: H_LIB_CIV_Worker_Cap_1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Characters\Civilians_t\RDS_JOA_Worker\Worker4_co.paa"};
	};
	class H_LIB_CIV_Villager_Cap_1: H_LIB_CIV_Worker_Cap_1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Characters\Civilians_t\RDS_JOA_Villager\Villager_co.paa"};
	};
	class H_LIB_CIV_Villager_Cap_2: H_LIB_CIV_Villager_Cap_1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Characters\Civilians_t\RDS_JOA_Villager\Villager2_co.paa"};
	};
	class H_LIB_CIV_Villager_Cap_3: H_LIB_CIV_Villager_Cap_1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Characters\Civilians_t\RDS_JOA_Villager\Villager3_co.paa"};
	};
	class H_LIB_CIV_Villager_Cap_4: H_LIB_CIV_Villager_Cap_1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Characters\Civilians_t\RDS_JOA_Villager\Villager4_co.paa"};
	};
	class U_LIB_DAK_Soldier: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Mgunner_0_LEN_co.paa"};
	};
	class U_LIB_DAK_Soldier_2: U_LIB_DAK_Soldier
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Mgunner_1_LEN_co.paa"};
	};
	class U_LIB_DAK_Soldier_3: U_LIB_DAK_Soldier
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Mgunner_2_LEN_co.paa"};
	};
	class U_LIB_DAK_Sentry: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Soldier_0_LEN_co.paa"};
	};
	class U_LIB_DAK_Sentry_2: U_LIB_DAK_Soldier
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Soldier_1_LEN_co.paa"};
	};
	class U_LIB_DAK_Medic: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Medic_0_LEN_co.paa"};
	};
	class U_LIB_DAK_NCO: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_NCO_0_LEN_co.paa"};
	};
	class U_LIB_DAK_NCO_2: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_NCO_1_LEN_co.paa"};
	};
	class U_LIB_DAK_lieutenant: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Oberst_LEN_co.paa"};
	};
	class U_LIB_DAK_Spg_crew_private: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Spg_Crew_0_LEN_co.paa"};
	};
	class U_LIB_DAK_Spg_crew_unterofficer: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Spg_Crew_0_LEN_co.paa"};
	};
	class U_LIB_DAK_Spg_crew_leutnant: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Spg_Crew_0_LEN_co.paa"};
	};
	class H_LIB_DAK_PithHelmet: H_LIB_HelmetB
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Pith_Helmet_LEN_co.paa"};
	};
	class H_LIB_DAK_Helmet: H_LIB_GER_Helmet
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\m42_helmet_dak_co.paa"};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\m42_helmet_dak_co.paa"};
		};
	};
	class H_LIB_DAK_Helmet_2: H_LIB_DAK_Helmet
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\m42_helmet_dak2_co.paa"};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\m42_helmet_dak2_co.paa"};
		};
	};
	class H_LIB_DAK_Helmet_net: H_LIB_GER_Helmet_net
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\m42_helmet_dak_co.paa","ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\h_wire_co.paa"};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\m42_helmet_dak_co.paa","ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\h_wire_co.paa"};
		};
	};
	class H_LIB_DAK_Helmet_net_2: H_LIB_DAK_Helmet_net
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\m42_helmet_dak2_co.paa","ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\h_wire_co.paa"};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\m42_helmet_dak_co.paa","ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\h_wire_co.paa"};
		};
	};
	class H_LIB_DAK_Helmet_ns: H_LIB_GER_Helmet_ns
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\m42_helmet_dak_co.paa"};
	};
	class H_LIB_DAK_Helmet_ns_2: H_LIB_DAK_Helmet_ns
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\m42_helmet_dak2_co.paa"};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\m42_helmet_dak2_co.paa"};
		};
	};
	class H_LIB_DAK_Helmet_Glasses: H_LIB_GER_Helmet_Glasses
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\m42_helmet_dak2_co.paa","ww2\assets_t\characters\germans_t\if_ger_wehrmacht\german_pilot_0_co.paa"};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\m42_helmet_dak2_co.paa","ww2\assets_t\characters\germans_t\if_ger_wehrmacht\german_pilot_0_co.paa"};
		};
	};
	class H_LIB_DAK_OfficerCap: H_LIB_GER_OfficerCap
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Equipment_5_LEN_co.paa"};
	};
	class H_LIB_DAK_Cap: H_LIB_GER_Cap
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Equipment_5_LEN_co.paa"};
	};
	class V_LIB_DAK_VestMP40: V_LIB_GER_VestMP40
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Equipment_0_LEN_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Soldier_1_Camo_LEN_co.paa"};
	};
	class V_LIB_DAK_VestSTG: V_LIB_GER_VestSTG
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Equipment_0_LEN_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Soldier_1_Camo_LEN_co.paa"};
	};
	class V_LIB_DAK_VestKar98: V_LIB_GER_VestKar98
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Equipment_0_LEN_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Soldier_1_Camo_LEN_co.paa"};
	};
	class V_LIB_DAK_VestG43: V_LIB_GER_VestG43
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Equipment_0_LEN_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Soldier_1_Camo_LEN_co.paa"};
	};
	class V_LIB_DAK_SniperBelt: V_LIB_GER_SniperBelt
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Equipment_0_LEN_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Soldier_1_Camo_LEN_co.paa"};
	};
	class V_LIB_DAK_VestMG: V_LIB_GER_VestMG
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Equipment_0_LEN_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Soldier_1_Camo_LEN_co.paa"};
	};
	class V_LIB_DAK_VestUnterofficer: V_LIB_GER_VestUnterofficer
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Equipment_0_LEN_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Soldier_1_Camo_LEN_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_Equipment_0_co.paa"};
	};
	class V_LIB_DAK_FieldOfficer: V_LIB_GER_FieldOfficer
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Equipment_0_LEN_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Oberst_LEN_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Officer_belt.paa"};
	};
	class V_LIB_DAK_OfficerVest: V_LIB_GER_OfficerVest
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Equipment_0_LEN_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_Equipment_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Oberst_LEN_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Officer_belt.paa"};
	};
	class V_LIB_DAK_OfficerBelt: V_LIB_GER_OfficerBelt
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Oberst_LEN_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Officer_belt.paa"};
	};
	class V_LIB_DAK_PrivateBelt: V_LIB_GER_PrivateBelt
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Equipment_0_LEN_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Soldier_1_Camo_LEN_co.paa"};
	};
	class V_LIB_DAK_PioneerVest: V_LIB_GER_PioneerVest
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Equipment_0_LEN_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Soldier_1_Camo_LEN_co.paa"};
	};
	class H_LIB_NKVD_PrivateCap: H_LIB_SOV_RA_PrivateCap
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_NKVD_Equipment_2_LEN_co.paa"};
	};
	class H_LIB_NKVD_OfficerCap: H_LIB_SOV_RA_OfficerCap
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_NKVD_Furajka_LEN_co.paa"};
	};
	class U_LIB_GER_Soldier3_w: U_LIB_BasicBody
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Soldier_0_co.paa","\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Mgunner_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Sniper_0_W2_co.paa"};
	};
	class U_LIB_GER_Soldier_camo_w: U_LIB_GER_BasicBody_Camo_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_w_co.paa"};
	};
	class U_LIB_GER_Scharfschutze_w: U_LIB_GER_Scharfschutze
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Sniper_0_w_co.paa"};
	};
	class U_LIB_GER_Scharfschutze_2_w: U_LIB_GER_Scharfschutze
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Sniper_0_W2_co.paa"};
	};
	class U_LIB_SOV_Strelok_w: U_LIB_SOV_Strelok
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_w_co.paa"};
	};
	class U_LIB_SOV_Strelok_2_w: U_LIB_SOV_Strelok
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_W2_co.paa"};
	};
	class U_LIB_SOV_Sniper_w: U_LIB_SOV_Sniper
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Sniper_w_co.paa"};
	};
	class H_LIB_GER_Helmet_w: H_LIB_GER_Helmet
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\M42_helmet_w2_co.paa"};
	};
	class H_LIB_GER_Helmet_net_w: H_LIB_GER_Helmet_net
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\M42_helmet_w2_co.paa","WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\H_Wire_co.paa"};
	};
	class H_LIB_GER_Helmet_ns_w: H_LIB_GER_Helmet_ns
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\M42_helmet_w_co.paa"};
	};
	class H_LIB_GER_HelmetCamo_w: H_LIB_GER_HelmetCamo
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\M42_helmet_co.paa","ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\h_suz_white_co.paa"};
	};
	class H_LIB_GER_HelmetCamob_w: H_LIB_GER_HelmetCamo_w
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\M42_helmet_co.paa","ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\h_suz_maus_co.paa"};
	};
	class H_LIB_GER_HelmetCamo2_w: H_LIB_GER_HelmetCamo2
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\M42_helmet_co.paa","ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\h_suz_ss_white_co.paa"};
	};
	class H_LIB_GER_HelmetCamo2b_w: H_LIB_GER_HelmetCamo2_w
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\M42_helmet_co.paa","ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\h_suz_ss_maus_co.paa"};
	};
	class H_LIB_GER_HelmetCamo4_w: H_LIB_GER_HelmetCamo4
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\M42_helmet_co.paa","ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\H_cover_zeltbahn_w_co.paa"};
		hiddenSelectionsMaterials[] = {"\ww2\assets_r\characters\germans_r\ww2_ger_wehrmacht\m42.rvmat","ww2\assets_r\characters\germans_r\ww2_ger_wehrmacht\H_Cover_Zeltbahn.rvmat"};
	};
	class H_LIB_GER_Helmet_Glasses_w: H_LIB_GER_Helmet_Glasses
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\M42_helmet_w2_co.paa","ww2\assets_t\characters\germans_t\if_ger_wehrmacht\german_pilot_0_co.paa"};
		class ItemInfo: ItemInfo
		{
			hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\ww2\assets_t\characters\germans_t\ww2_ger_wehrmacht\M42_helmet_w2_co.paa","ww2\assets_t\characters\germans_t\if_ger_wehrmacht\german_pilot_0_co.paa"};
		};
	};
	class H_LIB_SOV_RA_Helmet_w: H_LIB_SOV_RA_Helmet
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_1_w_co.paa"};
	};
	class H_LIB_US_Helmet_w: H_LIB_US_Helmet_ns
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","","","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_co.paa","","","","",""};
	};
	class H_LIB_US_Helmet_Net_w: H_LIB_US_Helmet_Net_ns
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","","","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_co.paa","ww2\Assets_t\Characters\Americans_t\WW2_US_Army\US_helmet_Net1_ca.paa","","","",""};
	};
	class H_LIB_US_Helmet_Med_w: H_LIB_US_Helmet_Net_ns
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","","","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_medic_co.paa","","","","",""};
	};
	class H_LIB_US_Helmet_Cap_w: H_LIB_US_Helmet_Cap
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Captain_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_co_ca.paa","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_co.paa","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Captain_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_co_ca.paa","",""};
	};
	class H_LIB_US_Helmet_First_lieutenant_w: H_LIB_US_Helmet_First_lieutenant
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_1stLieut_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_co_ca.paa","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_co.paa","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_1stLieut_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_co_ca.paa","",""};
	};
	class H_LIB_US_Helmet_Second_lieutenant_w: H_LIB_US_Helmet_Second_lieutenant
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_2ndLieut_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_co_ca.paa","",""};//{"WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Helmet_co.paa","","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_2ndLieut_ca.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Helm_Rear_co_ca.paa","",""};
	};
};
class CfgVehicles
{
	class B_LIB_AssaultPack_Base;
	class B_LIB_GER_Bag;
	class B_LIB_SOV_Bag;
	class B_Soldier_base_F;
	class C_man_1;
	class LIB_AmmoOrd_F;
	class LIB_Boat_base;
	class LIB_Car_base;
	class LIB_DAK_sapper;
	class LIB_DAK_Soldier_base;
	class LIB_FlaK_30;
	class LIB_FlaK_38;
	class LIB_Flakvierling_38;
	class LIB_GER_Mortar_base;
	class LIB_GER_Plane_base;
	class LIB_GER_radioman;
	class LIB_GER_rifleman;
	class LIB_GER_rifleman_ADS;
	class LIB_GER_sapper_gefr;
	class LIB_GER_scout_mgunner;
	class LIB_GER_scout_ober_rifleman;
	class LIB_GER_scout_rifleman;
	class LIB_GER_scout_smgunner;
	class LIB_GER_scout_unterofficer;
	class LIB_GER_smgunner;
	class LIB_GER_tank_crew;
	class LIB_GER_unequip;
	class LIB_JS2_43;
	class LIB_Kfz1_Hood;
	class LIB_M4A3_75;
	class LIB_M4A3_75_Tubes;
	class LIB_Objects_Military_Bunker_base;
	class LIB_Objects_Misc_NonStrategic_base;
	class LIB_OpelBlitz_Ammo;
	class LIB_OpelBlitz_Open_G_Camo;
	class LIB_OpelBlitz_Open_Y_Camo;
	class LIB_OpelBlitz_Tent_Y_Camo;
	class LIB_Parachute_base;
	class LIB_PlaneWreck_base;
	class LIB_PzKpfwIV_H;
	class LIB_PzKpfwV;
	class LIB_PzKpfwVI_B;
	class LIB_PzKpfwVI_B_destruct_chassis_1;
	class LIB_PzKpfwVI_B_kriloL2;
	class LIB_PzKpfwVI_B_kriloL3;
	class LIB_PzKpfwVI_B_kriloL4;
	class LIB_PzKpfwVI_B_kriloL5;
	class LIB_PzKpfwVI_B_kriloL6;
	class LIB_PzKpfwVI_B_kriloL7;
	class LIB_PzKpfwVI_B_kriloP1;
	class LIB_PzKpfwVI_B_kriloP2;
	class LIB_PzKpfwVI_B_kriloP3;
	class LIB_PzKpfwVI_B_kriloP4;
	class LIB_PzKpfwVI_B_kriloP5;
	class LIB_PzKpfwVI_B_kriloP6;
	class LIB_PzKpfwVI_B_kriloP7;
	class LIB_PzKpfwVI_E;
	class LIB_ReammoBox_base;
	class LIB_Scout_M3;
	class LIB_Scout_M3_FFV;
	class LIB_SdKfz251_FFV_base;
	class LIB_SOV_AT_grenadier;
	class LIB_SOV_rifleman;
	class LIB_SOV_rifleman_ADS;
	class LIB_SOV_sapper;
	class LIB_SOV_unequip;
	class LIB_StaticCannon_base;
	class LIB_StaticCanon_base;
	class LIB_StaticMGWeapon_base;
	class LIB_StuG_III_G;
	class LIB_StuG_III_G_WS;
	class LIB_SU_Mortar_base;
	class LIB_SU_Plane_base;
	class LIB_SU85;
	class LIB_T34_76;
	class LIB_T34_85;
	class LIB_Tank_base;
	class LIB_Tank_Destruct_Tower_base;
	class LIB_Tripod_Bag;
	class LIB_Truck_base;
	class LIB_US_AB_M42_base;
	class LIB_US_AB_M43_Flag_base;
	class LIB_US_AB_M43_Medic_base;
	class LIB_US_Mortar_base;
	class LIB_US_Plane_base;
	class LIB_US6_Unarmed_base;
	class LIB_Weapon_Bag_Base;
	class LIB_WheeledTracked_APC_base;
	class LIB_WP_Strzelec;
	class LIB_Zis3;
	class O_Soldier_base_F;
	class ProtectionZone_Ep1;
	class Static;
	class StaticMortar;
	class Strategic;
	class Thing;

	class Land_WW2_Zeltbahn: LIB_Objects_Misc_NonStrategic_base
	{
//		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Objects_t\Structures\IF_Buildings_t\palatka_nem\zeltbahn.paa"};
	};
	class Land_WW2_Zeltbahn_w: Land_WW2_Zeltbahn
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Objects_t\Structures\IF_Buildings_t\palatka_nem\zeltbahn.paa"};
	};
	class LIB_M4A3_75_destruct_chassis_1: Thing
	{
//		hiddenSelections[] = {"camo_2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_M4A3_75\Wheels_co.paa"};
	};
	class LIB_M4A3_75_back_motor_tube: LIB_M4A3_75_destruct_chassis_1
	{
//		hiddenSelections[] = {"camo_4"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_M4A3_75\Tubes_co.paa"};
	};
	class LIB_M4A2_SOV_destruct_chassis_1: LIB_M4A3_75_destruct_chassis_1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_M4A3_75\Wheels_Soviet_co.paa"};
	};
	class LIB_PzKpfwVI_B_kriloL1: LIB_PzKpfwVI_B_destruct_chassis_1
	{
//		hiddenSelections[] = {"camo_0"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_B\Body_co.paa"};
	};
	class LIB_PzKpfwVI_B_camo_kriloL1: LIB_PzKpfwVI_B_kriloL1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_B\Body_1_co.paa"};
	};
	class LIB_PzKpfwVI_B_camo_kriloL2: LIB_PzKpfwVI_B_kriloL2
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_B\Body_1_co.paa"};
	};
	class LIB_PzKpfwVI_B_camo_kriloL3: LIB_PzKpfwVI_B_kriloL3
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_B\Body_1_co.paa"};
	};
	class LIB_PzKpfwVI_B_camo_kriloL4: LIB_PzKpfwVI_B_kriloL4
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_B\Body_1_co.paa"};
	};
	class LIB_PzKpfwVI_B_camo_kriloL5: LIB_PzKpfwVI_B_kriloL5
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_B\Body_1_co.paa"};
	};
	class LIB_PzKpfwVI_B_camo_kriloL6: LIB_PzKpfwVI_B_kriloL6
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_B\Body_1_co.paa"};
	};
	class LIB_PzKpfwVI_B_camo_kriloL7: LIB_PzKpfwVI_B_kriloL7
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_B\Body_1_co.paa"};
	};
	class LIB_PzKpfwVI_B_camo_kriloP1: LIB_PzKpfwVI_B_kriloP1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_B\Body_1_co.paa"};
	};
	class LIB_PzKpfwVI_B_camo_kriloP2: LIB_PzKpfwVI_B_kriloP2
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_B\Body_1_co.paa"};
	};
	class LIB_PzKpfwVI_B_camo_kriloP3: LIB_PzKpfwVI_B_kriloP3
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_B\Body_1_co.paa"};
	};
	class LIB_PzKpfwVI_B_camo_kriloP4: LIB_PzKpfwVI_B_kriloP4
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_B\Body_1_co.paa"};
	};
	class LIB_PzKpfwVI_B_camo_kriloP5: LIB_PzKpfwVI_B_kriloP5
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_B\Body_1_co.paa"};
	};
	class LIB_PzKpfwVI_B_camo_kriloP6: LIB_PzKpfwVI_B_kriloP6
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_B\Body_1_co.paa"};
	};
	class LIB_PzKpfwVI_B_camo_kriloP7: LIB_PzKpfwVI_B_kriloP7
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_B\Body_1_co.paa"};
	};
	class LIB_PzKpfwVI_E_destruct_chassis_1: Thing
	{
//		hiddenSelections[] = {"camo_3"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_E\Wheels_co.paa"};
	};
	class LIB_PzKpfwVI_E_destruct_left_wing_0: LIB_PzKpfwVI_E_destruct_chassis_1
	{
//		hiddenSelections[] = {"camo_0"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_E\Body_co.paa"};
	};
	class LIB_T34_76_destruct_chassis_1: Thing
	{
//		hiddenSelections[] = {"camo_0"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_T34_76\Body_co.paa"};
	};
	class LIB_4Rnd_Panzerfaust30: LIB_ReammoBox_base
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\Ammoboxes_t\IF_Nem_Ammo\Nem_ammo.paa"};
	};
	class LIB_4Rnd_RPzB: LIB_ReammoBox_base
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\Ammoboxes_t\IF_Nem_Ammo\Nem_ammo.paa"};
	};
	class LIB_BasicAmmunitionBox_SU: LIB_ReammoBox_base
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\Ammoboxes_t\IF_Su_Ammo\SU_Ammo_co.paa"};
	};
	class LIB_BasicAmmunitionBox_GER: LIB_ReammoBox_base
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\Ammoboxes_t\IF_Nem_Ammo\Nem_ammo.paa"};
	};
	class LIB_BasicAmmunitionBox_US: LIB_ReammoBox_base
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\Ammoboxes_t\IF_Su_Ammo\SU_Ammo_co.paa"};
	};
	class LIB_BasicWeaponsBox_SU: LIB_BasicAmmunitionBox_SU
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\Ammoboxes_t\IF_Su_Weapons\SU_Weapons_co.paa"};
	};
	class LIB_BasicWeaponsBox_GER: LIB_BasicAmmunitionBox_GER
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\Ammoboxes_t\IF_Nem_Ammo\Nem_ammo.paa"};
	};
	class LIB_BasicWeaponsBox_US: LIB_BasicAmmunitionBox_US
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\Ammoboxes_t\IF_Su_Weapons\SU_Weapons_co.paa"};
	};
	class LIB_WeaponsBox_Big_SU: LIB_BasicWeaponsBox_SU
	{
//		hiddenSelections[] = {"camo_1","camo_2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Weapons\Ammoboxes_t\IF_Nazem_Yash\Nazem_yash.paa","WW2\Assets_t\Weapons\Ammoboxes_t\IF_RU_Ammo\RUS_Ammo.paa"};
	};
	class LIB_Lone_Big_Box: LIB_WeaponsBox_Big_SU
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\Ammoboxes_t\IF_Nazem_Yash\Nazem_yash.paa"};
	};
	class LIB_WeaponsBox_Big_GER: LIB_BasicWeaponsBox_GER
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\Ammoboxes_t\IF_Nem_Ammo\Nem_ammo.paa"};
	};
	class LIB_AmmoCrate_Arty_GER: LIB_ReammoBox_base
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\Ammoboxes_t\IF_Nem_Ammo\Nem_ammo.paa"};
	};
	class LIB_AmmoCrate_Arty_SU: LIB_ReammoBox_base
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\Ammoboxes_t\IF_Su_Arty\SU_Arty_co.paa"};
	};
	class LIB_AmmoCrate_Mortar_GER: LIB_ReammoBox_base
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\Ammoboxes_t\IF_Nem_Ammo\Nem_ammo.paa"};
	};
	class LIB_AmmoCrate_Mortar_SU: LIB_ReammoBox_base
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\Ammoboxes_t\IF_Su_Arty\SU_Arty_co.paa"};
	};
	class LIB_AmmoCrates_NoInteractive_Large: Strategic
	{
//		hiddenSelections[] = {"camo_1","camo_2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Weapons\Ammoboxes_t\IF_Nazem_Yash\Nazem_yash.paa","WW2\Assets_t\Weapons\Ammoboxes_t\IF_RU_Ammo\RUS_Ammo.paa"};
	};
	class LIB_Box_82mm_Mo_HE: LIB_AmmoOrd_F
	{
//		hiddenSelections[] = {"camo","camo1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Weapons\Ammoboxes_t\WW2_Mortars\Ammobox_82_he_co.paa","WW2\Assets_t\Weapons\Ammoboxes_t\WW2_Mortars\Shell80_he_co.paa"};
	};
	class LIB_Box_82mm_Mo_Smoke: LIB_Box_82mm_Mo_HE
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Weapons\Ammoboxes_t\WW2_Mortars\Ammobox_82_smoke_co.paa","WW2\Assets_t\Weapons\Ammoboxes_t\WW2_Mortars\Shell80_smoke_co.paa"};
	};
	class LIB_Box_82mm_Mo_Illum: LIB_Box_82mm_Mo_HE
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Weapons\Ammoboxes_t\WW2_Mortars\Ammobox_82_illum_co.paa","WW2\Assets_t\Weapons\Ammoboxes_t\WW2_Mortars\Shell80_illum_co.paa"};
	};
	class LIB_Box_81mm_Mo_HE: LIB_Box_82mm_Mo_HE
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Weapons\Ammoboxes_t\WW2_Mortars\Ammobox_81_he_co.paa","WW2\Assets_t\Weapons\Ammoboxes_t\WW2_Mortars\Shell80_he_co.paa"};
	};
	class LIB_Box_81mm_Mo_Smoke: LIB_Box_81mm_Mo_HE
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Weapons\Ammoboxes_t\WW2_Mortars\Ammobox_81_smoke_co.paa","WW2\Assets_t\Weapons\Ammoboxes_t\WW2_Mortars\Shell80_smoke_co.paa"};
	};
	class LIB_Box_81mm_Mo_Illum: LIB_Box_81mm_Mo_HE
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Weapons\Ammoboxes_t\WW2_Mortars\Ammobox_81_illum_co.paa","WW2\Assets_t\Weapons\Ammoboxes_t\WW2_Mortars\Shell80_illum_co.paa"};
	};
	class LIB_Box_60mm_Mo_HE: LIB_Box_82mm_Mo_HE
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Weapons\Ammoboxes_t\WW2_Mortars\Ammobox_60_he_co.paa","WW2\Assets_t\Weapons\Ammoboxes_t\WW2_Mortars\Shell60_he_co.paa"};
	};
	class LIB_Box_60mm_Mo_Smoke: LIB_Box_60mm_Mo_HE
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Weapons\Ammoboxes_t\WW2_Mortars\Ammobox_60_smoke_co.paa","WW2\Assets_t\Weapons\Ammoboxes_t\WW2_Mortars\Shell60_smoke_co.paa"};
	};
	class LIB_Box_60mm_Mo_Illum: LIB_Box_60mm_Mo_HE
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Weapons\Ammoboxes_t\WW2_Mortars\Ammobox_60_illum_co.paa","WW2\Assets_t\Weapons\Ammoboxes_t\WW2_Mortars\Shell60_illum_co.paa"};
	};
	class B_LIB_BasicBag: B_LIB_AssaultPack_Base
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa"};
	};
	class B_LIB_GER_GrenadesBag: B_LIB_GER_Bag
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\Backpacks_t\IF_German_Rucksack\German_Rucksack_co.paa"};
	};
	class B_LIB_GER_K89AmmoBag: B_LIB_GER_Bag
	{
//		hiddenSelections[] = {};
		hiddenSelectionsTextures[] = {};//{};
	};
	class B_LIB_GER_MGAmmoBag: B_LIB_GER_Bag
	{
//		hiddenSelections[] = {};
		hiddenSelectionsTextures[] = {};//{};
	};
	class B_LIB_GER_MineBag: B_LIB_GER_Bag
	{
//		hiddenSelections[] = {};
		hiddenSelectionsTextures[] = {};//{};
	};
	class B_LIB_GER_ExplosivesBag: B_LIB_GER_Bag
	{
//		hiddenSelections[] = {};
		hiddenSelectionsTextures[] = {};//{};
	};
	class B_LIB_SOV_GrenadesBag: B_LIB_SOV_Bag
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_0_co.paa"};
	};
	class B_LIB_SOV_PPShAmmoBag: B_LIB_SOV_Bag
	{
//		hiddenSelections[] = {};
		hiddenSelectionsTextures[] = {};//{};
	};
	class B_LIB_SOV_MinesBag: B_LIB_SOV_Bag
	{
//		hiddenSelections[] = {};
		hiddenSelectionsTextures[] = {};//{};
	};
	class B_LIB_SOV_ExplosivesBag: B_LIB_SOV_Bag
	{
//		hiddenSelections[] = {};
		hiddenSelectionsTextures[] = {};//{};
	};
	class B_LIB_SOV_MosinAmmoBag: B_LIB_SOV_Bag
	{
//		hiddenSelections[] = {};
		hiddenSelectionsTextures[] = {};//{};
	};
	class B_LIB_SOV_DPAmmoBag: B_LIB_SOV_Bag
	{
//		hiddenSelections[] = {};
		hiddenSelectionsTextures[] = {};//{};
	};
	class B_LIB_US_Bag: B_LIB_AssaultPack_Base
	{
//		hiddenSelections[] = {};
		hiddenSelectionsTextures[] = {};//{};
	};
	class B_LIB_FunkBag: B_LIB_AssaultPack_Base
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_3_co.paa"};
	};
	class B_LIB_RadioBag: B_LIB_AssaultPack_Base
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\Backpacks_t\IF_Soviet_Ratsia\Soviet_Ratsia_co.paa"};
	};
	class LIB_M2_Tripod_Bag: LIB_Tripod_Bag
	{
//		hiddenSelections[] = {"camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"ww2\assets_t\weapons\machinegun_light_t\ww2_m1919\m2_tripod_co.paa"};
	};
	class LIB_BM37_Bag: LIB_Tripod_Bag
	{
//		hiddenSelections[] = {"camo_0"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\Mortars_t\IF_Bm37\Bm37.paa"};
	};
	class LIB_BM37_Bar: LIB_Weapon_Bag_Base
	{
//		hiddenSelections[] = {"camo_0"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\Mortars_t\IF_Bm37\Bm37.paa"};
	};
	class LIB_GrWr34_Bag: LIB_Tripod_Bag
	{
//		hiddenSelections[] = {"camo_0"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\Mortars_t\IF_GrWr34\Grwr_34_co.paa"};
	};
	class LIB_GrWr34_Bar: LIB_Weapon_Bag_Base
	{
//		hiddenSelections[] = {"camo_0"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\Mortars_t\IF_GrWr34\Grwr_34_co.paa"};
	};
	class LIB_M2_60_Bag: LIB_Tripod_Bag
	{
//		hiddenSelections[] = {"camo_0"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"ww2\assets_t\weapons\mortars_t\ww2_m2\m2_co.paa"};
	};
	class LIB_M2_60_Bar: LIB_Weapon_Bag_Base
	{
//		hiddenSelections[] = {"camo_0"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"ww2\assets_t\weapons\mortars_t\ww2_m2\m2_co.paa"};
	};
	class LIB_MG42_Bag: LIB_Weapon_Bag_Base
	{
//		hiddenSelections[] = {"camo_0"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\MachineGun_Light_t\IF_MG42\Tripod_co.paa"};
	};
	class LIB_MG42_Tripod_Disasm: LIB_Tripod_Bag
	{
//		hiddenSelections[] = {"camo_0"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\MachineGun_Light_t\IF_MG42\Tripod_co.paa"};
	};
	class LIB_MG42_Tripod_High: LIB_Tripod_Bag
	{
//		hiddenSelections[] = {"camo_0"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\MachineGun_Light_t\IF_MG42\Tripod_co.paa"};
	};
	class LIB_Maxim_Bag: LIB_Tripod_Bag
	{
//		hiddenSelections[] = {"camo_0"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\StaticWeapons_t\IF_Maxim_M30\Maxim_m30.paa"};
	};
	class LIB_Maxim_Bar: LIB_Weapon_Bag_Base
	{
//		hiddenSelections[] = {"camo_0"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\StaticWeapons_t\IF_Maxim_M30\Maxim_m30.paa"};
	};
	class LIB_SdKfz222_base: LIB_Truck_base
	{
//		hiddenSelections[] = {"camo_1","camo_2","camo_3","camo_4"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"ww2\assets_t\vehicles\trucks_t\da_sdkfz222\camo1_co.paa","ww2\assets_t\vehicles\trucks_t\da_sdkfz222\camo2_co.paa","ww2\assets_t\vehicles\trucks_t\da_sdkfz222\tyre_co.paa","ww2\assets_t\vehicles\trucks_t\da_sdkfz222\mesh_co.paa"};
	};
	class LIB_SdKfz234_base: LIB_Truck_base
	{
//		hiddenSelections[] = {"camo_1","camo_2","camo_3"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"WW2\Assets_t\Vehicles\Trucks_t\DA_SdKfz234\da_sdkfz234_2_camo_1_co.paa","WW2\Assets_t\Vehicles\Trucks_t\DA_SdKfz234\da_sdkfz234_2_camo_2_co.paa","WW2\Assets_t\Vehicles\Trucks_t\DA_SdKfz234\da_sdkfz234_2_camo_3_co.paa"};
	};
	class LIB_SdKfz124_base: LIB_Tank_base
	{
//		hiddenSelections[] = {"camo1","camo2","camo3"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"ww2\assets_t\vehicles\tanks_t\i44_sdkfz124\wespe_co.paa","ww2\assets_t\vehicles\tanks_t\i44_sdkfz124\wespewheels_co.paa","ww2\assets_t\vehicles\tanks_t\i44_sdkfz124\lefh18_co.paa"};
	};
	class WW2_ProtectionZone_Invisible: ProtectionZone_Ep1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(0,0,0,0,ca)"};//{"#(argb,8,8,3)color(0,0,0,0,ca)"};
	};
	class Land_WW2_Bunker_Gun_R: LIB_Objects_Military_Bunker_base
	{
//		hiddenSelections[] = {"roof_mud"};
	};
	class B_LIB_GER_A_frame: B_LIB_AssaultPack_Base
	{
//		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa"};
	};
	class B_LIB_GER_A_frame_kit: B_LIB_GER_A_frame
	{
//		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa"};
	};
	class B_LIB_GER_A_frame_zeltbahn: B_LIB_GER_A_frame
	{
//		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa"};
	};
	class B_LIB_GER_Radio: B_LIB_AssaultPack_Base
	{
//		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_3_co.paa"};
	};
	class B_LIB_GER_Backpack: B_LIB_AssaultPack_Base
	{
//		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Rucksack_LEN_co.paa"};
	};
	class B_LIB_GER_SapperBackpack_empty: B_LIB_GER_Backpack
	{
//		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Pionier_Pack_co.paa"};
	};
	class B_LIB_GER_MedicBackpack: B_LIB_AssaultPack_Base
	{
//		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Equipment_0_co.paa"};
	};
	class B_LIB_GER_LW_Paradrop: B_LIB_AssaultPack_Base
	{
//		hiddenSelections[] = {"camo_0"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\Paradrop_co.paa"};
	};
	class B_LIB_GER_Panzer: B_LIB_AssaultPack_Base
	{
//		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Rucksack_LEN_co.paa"};
	};
	class B_LIB_SOV_RA_GasBag: B_LIB_AssaultPack_Base
	{
//		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_0_co.paa"};
	};
	class B_LIB_SOV_RA_Paradrop: B_LIB_AssaultPack_Base
	{
//		hiddenSelections[] = {"camo_0"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Pilot_Eqip_0_co.paa"};
	};
	class B_LIB_SOV_RA_MedicalBag: B_LIB_AssaultPack_Base
	{
//		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_4_co.paa"};
	};
	class B_LIB_SOV_RA_Radio: B_LIB_AssaultPack_Base
	{
//		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Ratsia_co.paa"};
	};
	class B_LIB_SOV_RA_Rucksack: B_LIB_AssaultPack_Base
	{
//		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_0_co.paa"};
	};
	class B_LIB_SOV_RA_Rucksack_Green: B_LIB_SOV_RA_Rucksack
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_0b_co.paa"};
	};
	class B_LIB_SOV_RA_Rucksack2: B_LIB_SOV_RA_Rucksack
	{
//		hiddenSelections[] = {"camo","camo2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_0_co.paa","\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_M24_co.paa"};
	};
	class B_LIB_SOV_RA_Rucksack2_Green: B_LIB_SOV_RA_Rucksack2
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_0b_co.paa","\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_M24_co.paa"};
	};
	class B_LIB_SOV_RA_Shinel: B_LIB_SOV_RA_Rucksack
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Blanket_co.paa"};
	};
	class B_LIB_SOV_RA_MGAmmoBag: B_LIB_AssaultPack_Base
	{
//		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Equipment_4_co.paa"};
	};
	class LIB_Static_zis6_radar: Strategic
	{
//		hiddenSelections[] = {"Camo_0","Camo_1","Camo_2","Camo_3","Camo_4","Camo_5","Camo_6"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","#(argb,8,8,3)color(1,1,1,1,ca)","#(argb,8,8,3)color(0,1,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Parm.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Zis5_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Zis52_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Zis53_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Kol1_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Kol2_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Radar.paa"};
	};
	class LIB_Static_opelblitz_radio: LIB_Static_zis6_radar
	{
//		hiddenSelections[] = {"Camo_0","Camo_1","Camo_2","Camo_3","Camo_4","Camo_5"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","#(argb,8,8,3)color(1,1,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel1_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel2_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel3_No_kamo.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel4_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opelbenzavoz.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel_radio.paa"};
	};
	class B_LIB_US_M36: B_LIB_AssaultPack_Base
	{
//		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\ww2\assets_t\characters\americans_t\ww2_us_airborne\m36_co.paa"};
	};
	class B_LIB_US_M36_Rope: B_LIB_US_M36
	{
//		hiddenSelections[] = {"camo","camo1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\ww2\assets_t\characters\americans_t\ww2_us_airborne\m36_co.paa","\WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\Rope_co.paa"};
	};
	class B_LIB_US_Backpack: B_LIB_AssaultPack_Base
	{
//		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\ww2\assets_t\characters\americans_t\ww2_us_airborne\m36_co.paa"};
	};
	class B_LIB_US_Backpack_dday: B_LIB_US_Backpack
	{
//		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\ww2\assets_t\characters\americans_t\ww2_us_airborne\m36_co.paa"};
	};
	class B_LIB_US_RocketBag: B_LIB_AssaultPack_Base
	{
//		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\ww2\assets_t\characters\americans_t\ww2_us_airborne\m36_co.paa"};
	};
	class B_LIB_US_Radio: B_LIB_AssaultPack_Base
	{
//		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\ww2\assets_t\characters\americans_t\ww2_us_airborne\m36_co.paa"};
	};
	class B_LIB_US_Bandoleer: B_LIB_AssaultPack_Base
	{
//		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\ww2\assets_t\characters\americans_t\ww2_us_airborne\m36_co.paa"};
	};
	class B_LIB_US_MGbag: B_LIB_AssaultPack_Base
	{
//		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\ww2\assets_t\characters\americans_t\ww2_us_airborne\m36_co.paa"};
	};
	class B_LIB_DAK_A_frame: B_LIB_GER_A_frame
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Equipment_0_LEN_co.paa"};
	};
	class B_LIB_DAK_A_frame_kit: B_LIB_GER_A_frame_kit
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Equipment_0_LEN_co.paa"};
	};
	class LIB_GER_Soldier_base: B_Soldier_base_F
	{
//		hiddenSelections[] = {"camo","camoB","badge"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)",""};//{"\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Soldier_0_co.paa","\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Soldier_0_co.paa",""};
	};
	class LIB_GER_Soldier2_base: LIB_GER_Soldier_base
	{
//		hiddenSelections[] = {"camo","camo2","badge"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Soldier_2_co.paa","\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Mgunner_1_co.paa"};
	};
	class LIB_GER_Soldier3_base: LIB_GER_Soldier_base
	{
//		hiddenSelections[] = {"camo","camo2","camo3","badge"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Soldier_1_co.paa","\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Mgunner_0_co.paa","\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Sniper_0_co.paa"};
	};
	class LIB_GER_soldier_camo_base: LIB_GER_Soldier_base
	{
//		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Soldier_0_Camo_co.paa"};
	};
	class LIB_GER_soldier_camo2_base: LIB_GER_soldier_camo_base
	{
//		hiddenSelections[] = {"camo","camo2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Soldier_0_Camo_co.paa","WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Soldier_2_co.paa"};
	};
	class LIB_GER_soldier_camo3_base: LIB_GER_soldier_camo_base
	{
//		hiddenSelections[] = {"camo","camo2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Sniper_0_co.paa","WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Soldier_2_co.paa"};
	};
	class LIB_GER_soldier_camo4_base: LIB_GER_soldier_camo_base
	{
//		hiddenSelections[] = {"camo","camo2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Soldier_0_Camo_co.paa","WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Mgunner_1_co.paa"};
	};
	class LIB_GER_soldier_camo5_base: LIB_GER_soldier_camo_base
	{
//		hiddenSelections[] = {"camo","camo2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Sniper_0_co.paa","WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Mgunner_0_co.paa"};
	};
	class LIB_GER_soldier_camo_MP40: LIB_GER_Soldier_base
	{
//		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Soldier_0_Camo_co.paa"};
	};
	class LIB_GER_soldier_MP40: LIB_GER_Soldier_base
	{
//		hiddenSelections[] = {"camo","badge"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)",""};//{"\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Mgunner_HBT_co.paa",""};
		hiddenSelectionsMaterials[] = {"\WW2\Assets_r\Characters\Germans_r\WW2_GER_Wehrmacht\WW2_German_MGunner_HBT.rvmat"};
	};
	class LIB_GER_soldier_K98: LIB_GER_Soldier_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)",""};//{"\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Soldier_HBT_co.paa","\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Soldier_HBT_co.paa",""};
		hiddenSelectionsMaterials[] = {"\WW2\Assets_r\Characters\Germans_r\WW2_GER_Wehrmacht\WW2_German_Soldier_HBT.rvmat"};
	};
	class LIB_GER_inf_officer_0: LIB_GER_Soldier_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)",""};//{"\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\german_Unterofficier_HBT_co.paa","\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\german_Unterofficier_HBT_co.paa",""};
		hiddenSelectionsMaterials[] = {"\WW2\Assets_r\Characters\Germans_r\WW2_GER_Wehrmacht\WW2_German_Unterofficier_HBT.rvmat"};
	};
	class LIB_GER_inf_officer_1: LIB_GER_inf_officer_0
	{
//		hiddenSelections[] = {"camo","e_st_leut"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Officer_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Officer_0_co.paa"};
	};
	class LIB_GER_recruit: LIB_GER_Soldier_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)",""};//{"\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Soldier_2_co.paa","\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Soldier_2_co.paa",""};
	};
	class LIB_GER_ober_rifleman: LIB_GER_rifleman
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Soldier_1_co.paa","\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Soldier_1_co.paa","\WW2\Assets_t\Characters\Germans_t\WW2_Badges\German_Oberschutze_Badge_ca.paa"};
	};
	class LIB_GER_mgunner: LIB_GER_rifleman
	{
//		hiddenSelections[] = {"camo","badge"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)",""};//{"\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Mgunner_2_co.paa",""};
	};
	class LIB_GER_stggunner: LIB_GER_mgunner
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Mgunner_0_co.paa","\WW2\Assets_t\Characters\Germans_t\WW2_Badges\German_Gefreitor_Badge_ca.paa"};
	};
	class LIB_GER_AT_soldier: LIB_GER_mgunner
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)",""};//{"\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Mgunner_0_co.paa",""};
	};
	class LIB_GER_AT_grenadier: LIB_GER_rifleman
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Soldier_0_co.paa","\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Soldier_0_co.paa","WW2\Assets_t\Characters\Germans_t\WW2_Badges\German_Gefreitor_Badge_ca.paa"};
	};
	class LIB_GER_unterofficer: LIB_GER_Soldier_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)",""};//{"\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\german_Unterofficier_0_co.paa","\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\german_Unterofficier_0_co.paa",""};
		hiddenSelectionsMaterials[] = {"\WW2\Assets_r\Characters\Germans_r\WW2_GER_Wehrmacht\WW2_German_Unterofficier.rvmat"};
	};
	class LIB_GER_lieutenant: LIB_GER_Soldier_base
	{
//		hiddenSelections[] = {"camo","e_st_leut"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Officer_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Officer_0_co.paa"};
	};
	class LIB_GER_ober_lieutenant: LIB_GER_lieutenant
	{
//		hiddenSelections[] = {"camo","e_st_oberleut"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Officer_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Officer_0_co.paa"};
	};
	class LIB_GER_hauptmann: LIB_GER_lieutenant
	{
//		hiddenSelections[] = {"camo","e_st_hauptmann"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Officer_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Officer_0_co.paa"};
	};
	class LIB_GER_oberst: LIB_GER_lieutenant
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Oberst_co.paa"};
	};
	class LIB_GER_scout_lieutenant: LIB_GER_scout_smgunner
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Lieutenant_0_Camo_co.paa"};
	};
	class LIB_GER_scout_sniper: LIB_GER_soldier_camo_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Sniper_0_co.paa"};
	};
	class LIB_GER_medic: LIB_GER_Soldier_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)",""};//{"\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Medic_1_co.paa","\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Medic_1_co.paa",""};
		hiddenSelectionsMaterials[] = {"\WW2\Assets_r\Characters\Germans_r\WW2_GER_Wehrmacht\WW2_German_Medic.rvmat"};
	};
	class LIB_GER_sapper: LIB_GER_Soldier_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Soldier_0_co.paa","\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Soldier_0_co.paa","\WW2\Assets_t\Characters\Germans_t\WW2_Badges\German_Gefreitor_Badge_ca.paa"};
	};
	class LIB_GER_gun_crew: LIB_GER_rifleman
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)",""};//{"\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Art_Soldier_0_co.paa","\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Art_Soldier_0_co.paa",""};
	};
	class LIB_GER_gun_unterofficer: LIB_GER_unterofficer
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)",""};//{"\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\german_Art_Unterofficier_2_co.paa","\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\german_Art_Unterofficier_2_co.paa",""};
		hiddenSelectionsMaterials[] = {"\WW2\Assets_r\Characters\Germans_r\WW2_GER_Wehrmacht\WW2_German_Unterofficier.rvmat"};
	};
	class LIB_GER_gun_lieutenant: LIB_GER_lieutenant
	{
//		hiddenSelections[] = {"camo","e_st_leut"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Art_Officer_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Art_Officer_0_co.paa"};
	};
	class LIB_GER_tank_crew_base: LIB_GER_Soldier_base
	{
//		hiddenSelections[] = {"camo","eqip_decal_1","eqip_decal_2","eqip_decal_3"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_Decals_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_Decals_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_0_co.paa"};
	};
	class LIB_GER_tank_unterofficer: LIB_GER_tank_crew
	{
//		hiddenSelections[] = {"camo","eqip_decal_1","eqip_decal_2","eqip_decal_4"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_Decals_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_Decals_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_0_co.paa"};
	};
	class LIB_GER_tank_lieutenant: LIB_GER_tank_unterofficer
	{
//		hiddenSelections[] = {"camo","eqip_decal_0","eqip_decal_1","eqip_decal_2","eqip_decal_5"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_Decals_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_Decals_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_Decals_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_0_co.paa"};
	};
	class LIB_GER_spg_crew: LIB_GER_tank_crew
	{
//		hiddenSelections[] = {"camo","eqip_decal_1","eqip_decal_2","eqip_decal_3"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Spg_Crew_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_Decals_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_Decals_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Spg_Crew_0_co.paa"};
	};
	class LIB_GER_spg_unterofficer: LIB_GER_tank_unterofficer
	{
//		hiddenSelections[] = {"camo","eqip_decal_1","eqip_decal_2","eqip_decal_4"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Spg_Crew_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_Decals_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_Decals_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Spg_Crew_0_co.paa"};
	};
	class LIB_GER_spg_lieutenant: LIB_GER_tank_lieutenant
	{
//		hiddenSelections[] = {"camo","eqip_decal_0","eqip_decal_1","eqip_decal_2","eqip_decal_5"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Spg_Crew_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_Decals_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_Decals_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_Decals_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Spg_Crew_0_co.paa"};
	};
	class LIB_GER_pilot: LIB_GER_Soldier_base
	{
//		hiddenSelections[] = {"camo","decal_straps"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Pilot_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Pilot_0_co.paa"};
	};
	class LIB_WP_base: LIB_GER_soldier_camo_base
	{
//		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Polish_t\IF_GUER_PolishTroops\Guer_Soldier_0_Camo_co.paa"};
	};
	class LIB_WP_Starszy_strzelec: LIB_WP_Strzelec
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Polish_t\IF_GUER_PolishTroops\Guer_Soldier_1_Camo_co.paa"};
	};
	class LIB_WP_AT_grenadier: LIB_WP_Strzelec
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Polish_t\IF_GUER_PolishTroops\Guer_Soldier_2_Camo_co.paa"};
	};
	class LIB_WP_Stggunner: LIB_WP_Strzelec
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Polish_t\IF_GUER_PolishTroops\Guer_Soldier_1_Camo_co.paa"};
	};
	class LIB_WP_Sniper: LIB_WP_Strzelec
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Polish_t\IF_GUER_PolishTroops\Guer_Soldier_1_Camo_co.paa"};
	};
	class LIB_WP_Radioman: LIB_WP_Strzelec
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Polish_t\IF_GUER_PolishTroops\Guer_Soldier_2_Camo_co.paa"};
	};
	class LIB_WP_Starszy_saper: LIB_WP_Strzelec
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Polish_t\IF_GUER_PolishTroops\Guer_Soldier_1_Camo_co.paa"};
	};
	class LIB_SOV_Soldier_base: O_Soldier_base_F
	{
//		hiddenSelections[] = {"camo","decal_private_straps"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_0_co.paa"};
	};
	class LIB_SOV_soldier_PPSH41: LIB_SOV_Soldier_base
	{
//		hiddenSelections[] = {"camo","decal_efr_straps","decal_wound_stripes"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Decal_Wound_Stripes_ca.paa"};
	};
	class LIB_SOV_soldier_mosin_rifle_0: LIB_SOV_Soldier_base
	{
//		hiddenSelections[] = {"camo","decal_private_straps"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_0_co.paa"};
	};
	class LIB_SOV_sergeant_PPSH41_0: LIB_SOV_Soldier_base
	{
//		hiddenSelections[] = {"camo","decal_sergeant_straps","decal_wound_stripes"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Decal_Wound_Stripes_ca.paa"};
	};
	class LIB_SOV_lieutenant_PPSH41: LIB_SOV_Soldier_base
	{
//		hiddenSelections[] = {"camo","decal_straps_artleit","decal_straps_leit"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Lieutenant_1_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Lieutenant_1_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Lieutenant_1_co.paa"};
	};
	class LIB_SOV_smgunner: LIB_SOV_Soldier_base
	{
//		hiddenSelections[] = {"camo","decal_private_straps"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_1_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_1_co.paa"};
	};
	class LIB_SOV_smgunner_summer: LIB_SOV_smgunner
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_0_co.paa"};
	};
	class LIB_SOV_LC_rifleman: LIB_SOV_rifleman
	{
//		hiddenSelections[] = {"camo","decal_efr_straps","decal_wound_stripes"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_1_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_1_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Decal_Wound_Stripes_ca.paa"};
	};
	class LIB_SOV_LC_rifleman_summer: LIB_SOV_LC_rifleman
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Decal_Wound_Stripes_ca.paa"};
	};
	class LIB_SOV_mgunner: LIB_SOV_Soldier_base
	{
//		hiddenSelections[] = {"camo","decal_efr_straps","decal_wound_stripes"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Decal_Wound_Stripes_ca.paa"};
	};
	class LIB_SOV_medic: LIB_SOV_mgunner
	{
//		hiddenSelections[] = {"camo","decal_private_straps"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_0_co.paa"};
	};
	class LIB_SOV_AT_soldier: LIB_SOV_Soldier_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_co.paa"};
	};
	class LIB_SOV_staff_sergeant: LIB_SOV_Soldier_base
	{
//		hiddenSelections[] = {"camo","decal_efr_straps","decal_stsergeant_straps","decal_wound_stripes"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Decal_Wound_Stripes_ca.paa"};
	};
	class LIB_SOV_sergeant: LIB_SOV_Soldier_base
	{
//		hiddenSelections[] = {"camo","decal_sergeant_straps","decal_wound_stripes"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Decal_Wound_Stripes_ca.paa"};
	};
	class LIB_SOV_p_officer: LIB_SOV_Soldier_base
	{
//		hiddenSelections[] = {"camo","decal_efr_straps","decal_red_star_1","decal_star_straps","decal_wound_stripes"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Decal_Red_Star_ca.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Decal_Wound_Stripes_ca.paa"};
	};
	class LIB_SOV_lieutenant: LIB_SOV_Soldier_base
	{
//		hiddenSelections[] = {"camo","decal_straps_artleit","decal_straps_leit"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Officer_1_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Officer_1_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Officer_1_co.paa"};
	};
	class LIB_SOV_first_lieutenant: LIB_SOV_lieutenant
	{
//		hiddenSelections[] = {"camo","decal_medal_of_valor","decal_straps_artleit","decal_straps_stleit"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Officer_1_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Decal_Medal_Of_Valor_ca.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Officer_1_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Officer_1_co.paa"};
	};
	class LIB_SOV_captain: LIB_SOV_lieutenant
	{
//		hiddenSelections[] = {"camo","decal_medal_of_valor","decal_order_red_banner","decal_straps_artleit","decal_straps_capt","decal_wound_stripes"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","#(argb,8,8,3)color(1,1,1,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Officer_1_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Decal_Medal_Of_Valor_ca.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Decal_Order_Red_Banner_ca.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Officer_1_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Officer_1_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Decal_Wound_Stripes_ca.paa"};
	};
	class LIB_SOV_captain_summer: LIB_SOV_captain
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","#(argb,8,8,3)color(1,1,1,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Officer_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Decal_Medal_Of_Valor_ca.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Decal_Order_Red_Banner_ca.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Officer_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Officer_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Decal_Wound_Stripes_ca.paa"};
	};
	class LIB_SOV_operator: LIB_SOV_rifleman
	{
//		hiddenSelections[] = {"camo","decal_private_straps"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_0_co.paa"};
	};
	class LIB_SOV_scout_rifleman: LIB_SOV_rifleman
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_1_co.paa"};
	};
	class LIB_SOV_scout_rifleman_autumn: LIB_SOV_scout_rifleman
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_3_co.paa"};
	};
	class LIB_SOV_scout_smgunner: LIB_SOV_scout_rifleman
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_co.paa"};
	};
	class LIB_SOV_scout_mgunner: LIB_SOV_scout_rifleman
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_co.paa"};
	};
	class LIB_SOV_scout_sergeant: LIB_SOV_scout_rifleman
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_1_co.paa"};
	};
	class LIB_SOV_scout_p_officer: LIB_SOV_scout_rifleman
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_co.paa"};
	};
	class LIB_SOV_scout_lieutenant: LIB_SOV_scout_p_officer
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_2_co.paa"};
	};
	class LIB_SOV_scout_sniper: LIB_SOV_scout_rifleman
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Sniper_0_co.paa"};
	};
	class LIB_SOV_scout_sniper_spring: LIB_SOV_scout_sniper
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Sniper_1_co.paa"};
	};
	class LIB_SOV_scout_sniper_autumn: LIB_SOV_scout_sniper
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Sniper_2_co.paa"};
	};
	class LIB_SOV_assault_smgunner: LIB_SOV_smgunner
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_1_co.paa"};
	};
	class LIB_SOV_assault_mgunner: LIB_SOV_assault_smgunner
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_co.paa"};
	};
	class LIB_SOV_assault_sergeant: LIB_SOV_assault_smgunner
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_2_co.paa"};
	};
	class LIB_SOV_tank_crew: LIB_SOV_Soldier_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Tank_Crew_1_co.paa"};
	};
	class LIB_SOV_tank_sergeant: LIB_SOV_tank_crew
	{
//		hiddenSelections[] = {"camo","decal_wound_stripes"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Tank_Crew_1_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Decal_Wound_Stripes_ca.paa"};
	};
	class LIB_SOV_tank_crew_lieutenant: LIB_SOV_tank_crew
	{
//		hiddenSelections[] = {"camo","decal_medal_of_valor"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Tank_Crew_1_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Decal_Medal_Of_Valor_ca.paa"};
	};
	class LIB_SOV_tank_lieutenant: LIB_SOV_tank_crew
	{
//		hiddenSelections[] = {"camo","decal_medal_of_valor"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Tank_Crew_1_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Decal_Medal_Of_Valor_ca.paa"};
	};
	class LIB_SOV_tank_captain: LIB_SOV_tank_lieutenant
	{
//		hiddenSelections[] = {"camo","decal_lieutenant_straps","decal_medal_of_valor","decal_order_red_banner"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Tank_Crew_1_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Tank_Crew_1_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Decal_Medal_Of_Valor_ca.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Decal_Order_Red_Banner_ca.paa"};
	};
	class LIB_SOV_tank_overall_crew: LIB_SOV_tank_crew
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Tank_Crew_0_co.paa"};
	};
	class LIB_SOV_tank_overall_sergeant: LIB_SOV_tank_overall_crew
	{
//		hiddenSelections[] = {"camo","decal_sergeant_straps"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Tank_Crew_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Tank_Crew_0_co.paa"};
	};
	class LIB_SOV_tank_overall_lieutenant: LIB_SOV_tank_overall_crew
	{
//		hiddenSelections[] = {"camo","decal_lieutenant_straps","decal_red_star_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Tank_Crew_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Tank_Crew_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Decal_Red_Star_ca.paa"};
	};
	class LIB_SOV_gun_crew: LIB_SOV_rifleman
	{
//		hiddenSelections[] = {"camo","decal_private_straps"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Arty_Tank_Crew_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Arty_Tank_Crew_co.paa"};
	};
	class LIB_SOV_gun_sergeant: LIB_SOV_sergeant
	{
//		hiddenSelections[] = {"camo","decal_sergeant_straps"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Tank_Crew_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Tank_Crew_0_co.paa"};
	};
	class LIB_SOV_gun_lieutenant: LIB_SOV_lieutenant
	{
//		hiddenSelections[] = {"camo","decal_medal_of_valor","decal_order_red_banner","decal_straps_artleit","decal_wound_stripes"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Officer_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Decal_Medal_Of_Valor_ca.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Decal_Order_Red_Banner_ca.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Officer_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Decal_Wound_Stripes_ca.paa"};
	};
	class LIB_SOV_pilot: LIB_SOV_Soldier_base
	{
//		hiddenSelections[] = {"camo","decal_straps","decal_straps_artleit"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Pilot_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Pilot_0_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Pilot_0_co.paa"};
	};
	class LIB_MG42_Lafette: LIB_StaticMGWeapon_base
	{
//		hiddenSelections[] = {"camo_0","camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Weapons\MachineGun_Light_t\IF_MG42\Tripod_co.paa","WW2\Assets_t\Weapons\MachineGun_Light_t\IF_MG42\Mg42_co.paa"};
	};
	class LIB_Maxim_M30_base: LIB_StaticMGWeapon_base
	{
//		hiddenSelections[] = {"camo_0"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\StaticWeapons_t\IF_Maxim_M30\Maxim_m30.paa"};
	};
	class LIB_M1919_M2: LIB_StaticMGWeapon_base
	{
//		hiddenSelections[] = {"camo_0","camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"ww2\assets_t\weapons\machinegun_light_t\ww2_m1919\m1919_co.paa","ww2\assets_t\weapons\machinegun_light_t\ww2_m1919\m2_tripod_co.paa"};
	};
	class LIB_StaticMortar_base: StaticMortar
	{
//		hiddenSelections[] = {};
		hiddenSelectionsTextures[] = {};//{};
	};
	class LIB_BM37: LIB_SU_Mortar_base
	{
//		hiddenSelections[] = {"camo_0","camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Weapons\Mortars_t\IF_Bm37\Bm37.paa","WW2\Assets_t\Weapons\Mortars_t\IF_Bm37\Mp41pris.paa"};
	};
	class LIB_GrWr34: LIB_GER_Mortar_base
	{
//		hiddenSelections[] = {"camo_0"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\Mortars_t\IF_GrWr34\Grwr_34_co.paa"};
	};
	class LIB_M2_60: LIB_US_Mortar_base
	{
//		hiddenSelections[] = {"camo_0"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"ww2\assets_t\weapons\mortars_t\ww2_m2\m2_co.paa"};
	};
	class LIB_BM37_Tripod_Deployed: LIB_Tripod_Bag
	{
//		hiddenSelections[] = {"camo_0"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\Mortars_t\IF_Bm37\Bm37.paa"};
	};
	class LIB_GrWr34_Tripod_Deployed: LIB_Tripod_Bag
	{
//		hiddenSelections[] = {"camo_0"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Weapons\Mortars_t\IF_GrWr34\Grwr_34_co.paa"};
	};
	class LIB_M2_60_Tripod_Deployed: LIB_Tripod_Bag
	{
//		hiddenSelections[] = {"camo_0"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"ww2\assets_t\weapons\mortars_t\ww2_m2\m2_co.paa"};
	};
	class LIB_61k_base: LIB_StaticCannon_base
	{
//		hiddenSelections[] = {"camo_0","camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Vehicles\StaticWeapons_t\IF_61k\Shassi_co.paa","WW2\Assets_t\Vehicles\StaticWeapons_t\IF_61k\Orudie_co.paa"};
	};
	class LIB_FlaK_38_base: LIB_StaticCanon_base
	{
//		hiddenSelections[] = {"camo_0","camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Vehicles\StaticWeapons_t\IF_Flak_38\Flak_38_Basement_co.paa","WW2\Assets_t\Vehicles\StaticWeapons_t\IF_Flak_38\Flak_38_Gun_co.paa"};
	};
	class LIB_Flakvierling_38_base: LIB_StaticCanon_base
	{
//		hiddenSelections[] = {"camo_0","camo_1","camo_2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"WW2\Assets_t\Vehicles\StaticWeapons_t\IF_FlakVierling_38\Flakvierling_Armor_co.paa","WW2\Assets_t\Vehicles\StaticWeapons_t\IF_FlakVierling_38\Flakvierling_Gun_co.paa","WW2\Assets_t\Vehicles\StaticWeapons_t\IF_FlakVierling_38\Flakvierling_Sight_co.paa"};
	};
	class LIB_Zis3_base: LIB_StaticCannon_base
	{
//		hiddenSelections[] = {"camo_0","camo_1","camo_2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"WW2\Assets_t\Vehicles\StaticWeapons_t\IF_Zis3\Niz_co.paa","WW2\Assets_t\Vehicles\StaticWeapons_t\IF_Zis3\Verh_co.paa","WW2\Assets_t\Vehicles\StaticWeapons_t\IF_Zis3\Pricel_co.paa"};
	};
	class LIB_SU85_Destruct_Tower: LIB_Tank_Destruct_Tower_base
	{
//		hiddenSelections[] = {"karoserie"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_T34_85\Turret_co.paa"};
	};
	class LIB_T34_76_Destruct_Tower: LIB_Tank_Destruct_Tower_base
	{
//		hiddenSelections[] = {"camo_0"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_T34_76\Body_co.paa"};
	};
	class LIB_T34_85_Destruct_Tower: LIB_Tank_Destruct_Tower_base
	{
//		hiddenSelections[] = {"karoserie"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_T34_85\Turret_co.paa"};
	};
	class LIB_PzKpfwVI_E_Destruct_Tower: LIB_Tank_Destruct_Tower_base
	{
//		hiddenSelections[] = {"karoserie"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_E\Turret_co.paa"};
	};
	class LIB_PzKpfwVI_B_Destruct_Tower: LIB_Tank_Destruct_Tower_base
	{
//		hiddenSelections[] = {"karoserie"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_B\Turret_co.paa"};
	};
	class LIB_PzKpfwVI_B_camo_Destruct_Tower: LIB_PzKpfwVI_B_Destruct_Tower
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_B\Turret_1_co.paa"};
	};
	class LIB_M4A3_75_Destruct_Tower: LIB_Tank_Destruct_Tower_base
	{
//		hiddenSelections[] = {"camo_0","camo_1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_M4A3_75\Body_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_M4A3_75\Turret_co.paa"};
	};
	class LIB_M4A2_SOV_Destruct_Tower: LIB_M4A3_75_Destruct_Tower
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_M4A3_75\Body_Soviet_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_M4A3_75\Turret_Soviet_co.paa"};
	};
	class LIB_JS2_43_Destruct_Tower: LIB_Tank_Destruct_Tower_base
	{
//		hiddenSelections[] = {"karoserie"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_JS2_43\Turret_co.paa"};
	};
	class LIB_JS2_43_base: LIB_Tank_base
	{
//		hiddenSelections[] = {"camo_1","camo_2","camo_3","num1","num2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"WW2\Assets_t\Vehicles\Tanks_t\IF_JS2_43\Body_co.paa","WW2\Assets_t\Vehicles\Tanks_t\IF_JS2_43\Turret_co.paa","WW2\Assets_t\Vehicles\Tanks_t\IF_JS2_43\Track_co.paa"};
	};
	class LIB_PzKpfwV_base: LIB_Tank_base
	{
//		hiddenSelections[] = {"camo_1","camo_2","camo_3","num1","num2","num3"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwV\Body_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwV\Turret_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwV\Tracks_co.paa"};
	};
	class LIB_PzKpfwVI_B_base: LIB_Tank_base
	{
//		hiddenSelections[] = {"camo_0","camo_1","camo_2","camo_3","camo_4","num1","num2","num3"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_B\Body_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_B\Shanc_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_B\Turret_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_B\Wheels_co.paa","WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_B\Track_co.paa"};
	};
	class LIB_PzKpfwVI_B_camo: LIB_PzKpfwVI_B
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_B\Body_1_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_B\Shanc_1_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_B\Turret_1_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_B\Wheels_1_co.paa","WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_B\Track_co.paa"};
	};
	class LIB_PzKpfwVI_E_base: LIB_Tank_base
	{
//		hiddenSelections[] = {"camo_0","camo_1","camo_2","camo_3","camo_4","num1","num2","num3"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)"};//{"WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_E\Body_co.paa","WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_E\Tools_co.paa","WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_E\Turret_co.paa","WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_E\Wheels_co.paa","WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_E\Tracks_co.paa"};
	};
	class LIB_PzKpfwVI_E_1: LIB_PzKpfwVI_E
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)"};//{"WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_E\Body_1_co.paa","WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_E\Tools_co.paa","WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_E\Turret_1_co.paa","WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_E\Wheels_1_co.paa","WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_E\Tracks_co.paa"};
	};
	class LIB_PzKpfwVI_E_2: LIB_PzKpfwVI_E
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)"};//{"WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_E\Body_2_co.paa","WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_E\Tools_co.paa","WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_E\Turret_2_co.paa","WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_E\Wheels_2_co.paa","WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_E\Tracks_co.paa"};
	};
	class LIB_StuG_III_G_base: LIB_Tank_base
	{
//		hiddenSelections[] = {"camo_1","camo_2","camo_3","camo_4","num1","num2","num3"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_StuG_III_G\Main_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_StuG_III_G\Misc_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_StuG_III_G\Wheels_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_StuG_III_G\Tracks_co.paa"};
	};
	class LIB_SU85_base: LIB_Tank_base
	{
//		hiddenSelections[] = {"camo_1","camo_2","camo_3","baki2","num1","num2","num3","logo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"WW2\Assets_t\Vehicles\Tanks_t\IF_SU85\SU83_1_co.paa","WW2\Assets_t\Vehicles\Tanks_t\IF_SU85\SU83_2_co.paa","WW2\Assets_t\Vehicles\Tanks_t\IF_SU85\Traks_co.paa"};
	};
	class LIB_T34_76_base: LIB_Tank_base
	{
//		hiddenSelections[] = {"camo_1","camo_2","camo_3","num1","num2","num3"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_T34_76\Body_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_T34_76\Turret_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_T34_76\Track_co.paa"};
	};
	class LIB_T34_85_base: LIB_Tank_base
	{
//		hiddenSelections[] = {"camo_1","camo_2","camo_3","baki2","num1","num2","num3","logo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"WW2\Assets_t\Vehicles\Tanks_t\IF_T34_85\Body_co.paa","WW2\Assets_t\Vehicles\Tanks_t\IF_T34_85\Turret_co.paa","WW2\Assets_t\Vehicles\Tanks_t\IF_T34_85\Track_co.paa"};
	};
	class LIB_M8_Greyhound_base: LIB_Truck_base
	{
//		hiddenSelections[] = {"camo_0","camo_1","camo_2","camo_3"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\assets_t\vehicles\Trucks_t\I44_m8_greyhound\m8_nt_1_co.paa","\WW2\assets_t\vehicles\Trucks_t\I44_m8_greyhound\m8_nt_2_co.paa","\WW2\assets_t\vehicles\Trucks_t\I44_m8_greyhound\m8_nt_3_co.paa","\WW2\assets_t\vehicles\Trucks_t\I44_m8_greyhound\m8_t_1_ca.paa"};
	};
	class LIB_SdKfz234_1: LIB_SdKfz234_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"WW2\Assets_t\Vehicles\Trucks_t\DA_SdKfz234\da_sdkfz234_1_camo_1_co.paa","WW2\Assets_t\Vehicles\Trucks_t\DA_SdKfz234\da_sdkfz234_1_camo_2_co.paa","WW2\Assets_t\Vehicles\Trucks_t\DA_SdKfz234\da_sdkfz234_1_camo_3_co.paa"};
	};
	class LIB_SdKfz234_2: LIB_SdKfz234_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"WW2\Assets_t\Vehicles\Trucks_t\DA_SdKfz234\da_sdkfz234_2_camo_1_co.paa","WW2\Assets_t\Vehicles\Trucks_t\DA_SdKfz234\da_sdkfz234_2_camo_2_co.paa","WW2\Assets_t\Vehicles\Trucks_t\DA_SdKfz234\da_sdkfz234_2_camo_3_co.paa"};
	};
	class LIB_SdKfz234_3: LIB_SdKfz234_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"WW2\Assets_t\Vehicles\Trucks_t\DA_SdKfz234\da_sdkfz234_3_camo_1_co.paa","WW2\Assets_t\Vehicles\Trucks_t\DA_SdKfz234\da_sdkfz234_3_camo_2_co.paa","WW2\Assets_t\Vehicles\Trucks_t\DA_SdKfz234\da_sdkfz234_3_camo_3_co.paa"};
	};
	class LIB_SdKfz234_4: LIB_SdKfz234_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"WW2\Assets_t\Vehicles\Trucks_t\DA_SdKfz234\da_sdkfz234_4_camo_1_co.paa","WW2\Assets_t\Vehicles\Trucks_t\DA_SdKfz234\da_sdkfz234_4_camo_2_co.paa","WW2\Assets_t\Vehicles\Trucks_t\DA_SdKfz234\da_sdkfz234_4_camo_3_co.paa"};
	};
	class LIB_US6_base: LIB_Truck_base
	{
//		hiddenSelections[] = {"Camo_1","Camo_2","Camo_3","Camo_4","usa","labels","num0","Hid_litera","Hid_num1","Hid_num2","Hid_num3","Hid_num4","Hid_num5","num1","num2","num3","num4","num5","num6","Hid_Line"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","#(argb,8,8,3)color(1,1,1,1,ca)","#(argb,8,8,3)color(0,1,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Stud1.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Stud2.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Studcol.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Studtent.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Img\Usa.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Img\Marker_Na_dver.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Img\4.paa"};
	};
	class LIB_US6_Open: LIB_US6_Unarmed_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","","","",""};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Stud1zel.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Stud2zel.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Studcol.paa","","","",""};
	};
	class LIB_US6_BM13: LIB_US6_base
	{
//		hiddenSelections[] = {"Camo_1","Camo_2","Camo_3","Camo_4","Hid_litera","Hid_num1","Hid_num2","Hid_num3","Hid_num4","Hid_num5"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Stud1zel.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Stud2zel.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Studcol.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Napravlaushaya.paa"};
	};
	class LIB_Zis5v_base: LIB_Truck_base
	{
//		hiddenSelections[] = {"Camo_0","Camo_1","Camo_2","Camo_3","Camo_4","Hid_litera","Hid_num1","Hid_num2","Hid_num3","Hid_num4","Hid_num5","hid_orlova","Hid_line"};
	};
	class LIB_Zis5v: LIB_Zis5v_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)"};//{"WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Zis5_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Zis52_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Zis53_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Kol1_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Kol2_co.paa"};
	};
	class LIB_Zis5v_Med: LIB_Zis5v_base
	{
//		hiddenSelections[] = {"Camo_0","Camo_1","Camo_2","Camo_3","Camo_4","Camo_5","Camo_6","Camo_7","Hid_litera","Hid_num1","Hid_num2","Hid_num3","Hid_num4","Hid_num5","hid_orlova","Hid_line"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","#(argb,8,8,3)color(1,1,1,1,ca)","#(argb,8,8,3)color(0,1,1,1,ca)","#(argb,8,8,3)color(0.5,0.5,0.5,1,ca)"};//{"WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Zis5_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Zis52_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Zis53_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Kol1_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Kol2_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Parm.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Zapravka.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Sanitarka.paa"};
	};
	class LIB_Zis6_Parm: LIB_Zis5v_base
	{
//		hiddenSelections[] = {"Camo_0","Camo_1","Camo_2","Camo_3","Camo_4","Camo_5","Hid_litera","Hid_num1","Hid_num2","Hid_num3","Hid_num4","Hid_num5","hid_orlova","Hid_line"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","#(argb,8,8,3)color(1,1,1,1,ca)"};//{"WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Zis5_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Zis52_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Zis53_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Kol1_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Kol2_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Parm.paa"};
	};
	class LIB_Zis5v_Fuel: LIB_Zis5v_base
	{
//		hiddenSelections[] = {"Camo_0","Camo_1","Camo_2","Camo_3","Camo_4","Camo_5","Camo_6","Hid_litera","Hid_num1","Hid_num2","Hid_num3","Hid_num4","Hid_num5","hid_orlova","Hid_line"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","#(argb,8,8,3)color(1,1,1,1,ca)","#(argb,8,8,3)color(0,1,1,1,ca)"};//{"WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Zis5_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Zis52_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Zis53_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Kol1_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Kol2_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Parm.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Zapravka.paa"};
	};
	class LIB_CIV_FFI_CitC4: LIB_Zis5v_base
	{
//		hiddenSelections[] = {"Camo_0","Camo_1","Camo_2","Camo_3","Camo_4","Hid_litera","Hid_num1","Hid_num2","Hid_num3","Hid_num4","Hid_num5","hid_orlova","Hid_line"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)"};//{"WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\French_Citc4_Camo01a_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\French_Citc42_Camo01a_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\French_Citc43_Camo01a_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Kol1_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Kol2_co.paa"};
	};
	class LIB_CIV_FFI_CitC4_2: LIB_CIV_FFI_CitC4
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)"};//{"WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\French_Citc4_Camo02a_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\French_Citc42_Camo02a_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\French_Citc43_Camo02a_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Kol1_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Kol2_co.paa"};
	};
	class LIB_CIV_FFI_CitC4_3: LIB_CIV_FFI_CitC4
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)"};//{"WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\French_Citc4_Camo03a_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\French_Citc42_Camo03a_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\French_Citc43_Camo03a_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Kol1_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Kol2_co.paa"};
	};
	class LIB_CIV_FFI_CitC4_4: LIB_CIV_FFI_CitC4
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)"};//{"WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\French_Citc4_Camo01a_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\French_FFI_Citc42_Camo01a_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\French_Citc43_Camo01a_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Kol1_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Kol2_co.paa"};
	};
	class LIB_CIV_FFI_CitC4_5: LIB_CIV_FFI_CitC4
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)"};//{"WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\French_Citc4_Camo03a_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\French_FFI_Citc42_Camo03a_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\French_Citc43_Camo03a_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Kol1_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Kol2_co.paa"};
	};
	class LIB_US_Soldier_base: B_Soldier_base_F
	{
//		hiddenSelections[] = {"camo","insignia"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Tankcrew_01_co.paa"};
	};
	class LIB_US_tank_crew: LIB_US_Soldier_base
	{
//		hiddenSelections[] = {"camo","insignia"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Tankcrew_01_co.paa"};
	};
	class LIB_US_tank_sergeant: LIB_US_tank_crew
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Americans_t\IF_US_Army\US_Tankcrew_02_co.paa"};
	};
	class LIB_US_Helmet: Thing
	{
//		hiddenSelections[] = {"eqip_helmet","decal_helmet_back","decal_helmet_front"};
		hiddenSelectionsTextures[] = {"","",""};//{"","",""};
	};
	class LIB_US_Tank_Helmet: LIB_US_Helmet
	{
//		hiddenSelections[] = {"eqip_tank_glasses_up","eqip_tank_helmet"};
		hiddenSelectionsTextures[] = {"",""};//{"",""};
	};
	class LIB_US_Pilot_Helmet: LIB_US_Tank_Helmet
	{
//		hiddenSelections[] = {"eqip_pilot_glasses_up","eqip_pilot_helmet"};
	};
	class LIB_Civilian_Base: C_man_1
	{
//		hiddenSelections[] = {"Camo"};
	};
	class LIB_CIV_Citizen_1: LIB_Civilian_Base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Characters\Civilians_t\RDS_JOA_Citizen\Citizen_co.paa"};
	};
	class LIB_CIV_Citizen_2: LIB_CIV_Citizen_1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Civilians_t\RDS_JOA_Citizen\Citizen2_co.paa"};
	};
	class LIB_CIV_Citizen_3: LIB_CIV_Citizen_1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Civilians_t\RDS_JOA_Citizen\Citizen3_co.paa"};
	};
	class LIB_CIV_Citizen_4: LIB_CIV_Citizen_1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Civilians_t\RDS_JOA_Citizen\Citizen4_co.paa"};
	};
	class LIB_CIV_Citizen_5: LIB_CIV_Citizen_1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Civilians_t\RDS_JOA_Citizen\Citizen5_co.paa"};
	};
	class LIB_CIV_Citizen_6: LIB_CIV_Citizen_1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Civilians_t\RDS_JOA_Citizen\Citizen6_co.paa"};
	};
	class LIB_CIV_Citizen_7: LIB_CIV_Citizen_1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Civilians_t\RDS_JOA_Citizen\Citizen7_co.paa"};
	};
	class LIB_CIV_Citizen_8: LIB_CIV_Citizen_1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Civilians_t\RDS_JOA_Citizen\Citizen8_co.paa"};
	};
	class LIB_CIV_Worker_1: LIB_Civilian_Base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Civilians_t\RDS_JOA_Worker\Worker_co.paa"};
	};
	class LIB_CIV_Worker_2: LIB_CIV_Worker_1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Civilians_t\RDS_JOA_Worker\Worker2_co.paa"};
	};
	class LIB_CIV_Worker_3: LIB_CIV_Worker_1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Civilians_t\RDS_JOA_Worker\Worker3_co.paa"};
	};
	class LIB_CIV_Worker_4: LIB_CIV_Worker_1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Civilians_t\RDS_JOA_Worker\Worker4_co.paa"};
	};
	class LIB_CIV_Woodlander_1: LIB_Civilian_Base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Civilians_t\RDS_JOA_Woodlander\Woodlander_co.paa"};
	};
	class LIB_CIV_Woodlander_2: LIB_CIV_Woodlander_1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Civilians_t\RDS_JOA_Woodlander\Woodlander2_co.paa"};
	};
	class LIB_CIV_Woodlander_3: LIB_CIV_Woodlander_1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Civilians_t\RDS_JOA_Woodlander\Woodlander3_co.paa"};
	};
	class LIB_CIV_Woodlander_4: LIB_CIV_Woodlander_1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Civilians_t\RDS_JOA_Woodlander\Woodlander4_co.paa"};
	};
	class LIB_CIV_Functionary_1: LIB_Civilian_Base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Civilians_t\RDS_JOA_Functionary\Functionary_co.paa"};
	};
	class LIB_CIV_Functionary_2: LIB_CIV_Functionary_1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Civilians_t\RDS_JOA_Functionary\Functionary2_co.paa"};
	};
	class LIB_CIV_Functionary_3: LIB_CIV_Functionary_1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Civilians_t\RDS_JOA_Functionary\Functionary3_co.paa"};
	};
	class LIB_CIV_Functionary_4: LIB_CIV_Functionary_1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Civilians_t\RDS_JOA_Functionary\Functionary4_co.paa"};
	};
	class LIB_CIV_Villager_1: LIB_Civilian_Base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Civilians_t\RDS_JOA_Villager\Villager_co.paa"};
	};
	class LIB_CIV_Villager_2: LIB_CIV_Villager_1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Civilians_t\RDS_JOA_Villager\Villager2_co.paa"};
	};
	class LIB_CIV_Villager_3: LIB_CIV_Villager_1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Civilians_t\RDS_JOA_Villager\Villager3_co.paa"};
	};
	class LIB_CIV_Villager_4: LIB_CIV_Villager_1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Civilians_t\RDS_JOA_Villager\Villager4_co.paa"};
	};
	class LIB_CIV_Rocker: LIB_Civilian_Base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Civilians_t\RDS_JOA_Rocker\Rocker_co.paa"};
	};
	class LIB_CIV_Doctor: LIB_Civilian_Base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Civilians_t\RDS_JOA_Doctor\Doctor_co.paa"};
	};
	class LIB_CIV_SchoolTeacher: LIB_CIV_Doctor
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Civilians_t\RDS_JOA_Doctor\Doctor2_co.paa"};
	};
	class LIB_CIV_SchoolTeacher_2: LIB_CIV_Doctor
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Civilians_t\RDS_JOA_Doctor\Doctor5_co.paa"};
	};
	class LIB_CIV_Assistant: LIB_CIV_Doctor
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Civilians_t\RDS_JOA_Doctor\Doctor3_co.paa"};
	};
	class LIB_CIV_Assistant_2: LIB_CIV_Assistant
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Civilians_t\RDS_JOA_Doctor\Doctor4_co.paa"};
	};
	class LIB_DAK_Soldier: LIB_DAK_Soldier_base
	{
//		hiddenSelections[] = {"camo","badge"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)",""};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Mgunner_0_LEN_co.paa",""};
	};
	class LIB_DAK_Soldier_2: LIB_DAK_Soldier
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Mgunner_1_LEN_co.paa"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Mgunner_1_LEN_co.paa","","WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Mgunner_1_LEN_co.paa"};
	};
	class LIB_DAK_Soldier_3: LIB_DAK_Soldier
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Mgunner_2_LEN_co.paa"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Mgunner_2_LEN_co.paa","","WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Mgunner_2_LEN_co.paa"};
	};
	class LIB_DAK_Sentry: LIB_DAK_Soldier
	{
//		hiddenSelections[] = {"camo","camoB"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Soldier_0_LEN_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Soldier_0_LEN_co.paa"};
	};
	class LIB_DAK_Sentry_2: LIB_DAK_Sentry
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Soldier_1_LEN_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Soldier_1_LEN_co.paa"};
	};
	class LIB_DAK_Sniper: LIB_DAK_Soldier
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Soldier_1_LEN_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Soldier_1_LEN_co.paa"};
	};
	class LIB_DAK_medic: LIB_DAK_Soldier
	{
//		hiddenSelections[] = {"camo","camoB"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Medic_0_LEN_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Medic_0_LEN_co.paa"};
	};
	class LIB_DAK_NCO: LIB_DAK_Sentry
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_NCO_0_LEN_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_NCO_0_LEN_co.paa"};
	};
	class LIB_DAK_NCO_2: LIB_DAK_NCO
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_NCO_1_LEN_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_NCO_1_LEN_co.paa"};
	};
	class LIB_DAK_Lieutenant: LIB_DAK_Soldier
	{
//		hiddenSelections[] = {"camo","e_st_leut"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Oberst_LEN_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Oberst_LEN_co.paa"};
	};
	class LIB_DAK_sapper_gefr: LIB_DAK_sapper
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Mgunner_2_LEN_co.paa","\WW2\Assets_t\Characters\Germans_t\WW2_Badges\German_Gefreitor_Badge_ca.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Mgunner_2_LEN_co.paa"};
	};
	class LIB_DAK_spg_crew: LIB_GER_spg_crew
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Spg_Crew_0_LEN_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_Decals_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_Decals_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Spg_Crew_0_LEN_co.paa"};
	};
	class LIB_DAK_spg_unterofficer: LIB_GER_spg_unterofficer
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Spg_Crew_0_LEN_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_Decals_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_Decals_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Spg_Crew_0_LEN_co.paa"};
	};
	class LIB_DAK_spg_lieutenant: LIB_GER_spg_lieutenant
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Spg_Crew_0_LEN_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_Decals_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_Decals_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Tank_Crew_Decals_0_co.paa","WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\DAK_Spg_Crew_0_LEN_co.paa"};
	};
	class SG_sturmpanzer_base: LIB_GER_tank_crew_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Spg_Crew_0_co.paa"};
	};
	class SG_sturmpanzer_crew: SG_sturmpanzer_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Spg_Crew_0_co.paa"};
	};
	class SG_sturmpanzer_unterofficer: SG_sturmpanzer_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Spg_Crew_0_co.paa"};
	};
	class SG_sturmpanzer_officer: SG_sturmpanzer_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_TankTroops\German_Spg_Crew_0_co.paa"};
	};
	class LIB_NKVD_rifleman: LIB_SOV_rifleman
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_NKVD_Soldier_0_LEN_co.paa","\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_NKVD_Soldier_0_LEN_co.paa","\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_NKVD_Soldier_0_LEN_co.paa"};
	};
	class LIB_NKVD_LC_rifleman: LIB_SOV_LC_rifleman
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_NKVD_Soldier_0_LEN_co.paa","\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_NKVD_Soldier_0_LEN_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Decal_Wound_Stripes_ca.paa"};
	};
	class LIB_NKVD_p_officer: LIB_SOV_p_officer
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_NKVD_Soldier_0_LEN_co.paa","\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_NKVD_Soldier_0_LEN_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Decal_Red_Star_ca.paa","\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_NKVD_Soldier_0_LEN_co.paa","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Decal_Wound_Stripes_ca.paa"};
	};
	class LIB_NKVD_sergeant: LIB_SOV_sergeant
	{
//		hiddenSelections[] = {"camo","decal_sergeant_straps"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_NKVD_Soldier_0_LEN_co.paa","\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_NKVD_Soldier_0_LEN_co.paa"};
	};
	class LIB_NKVD_lieutenant: LIB_SOV_lieutenant
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_NKVD_Officer_1_LEN_co.paa","\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_NKVD_Officer_1_LEN_co.paa","\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_NKVD_Officer_1_LEN_co.paa"};
	};
	class LIB_NKVD_first_lieutenant: LIB_SOV_first_lieutenant
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Decal_Medal_Of_Valor_ca.paa","\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_NKVD_Officer_0_LEN_co.paa","\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_NKVD_Officer_0_LEN_co.paa"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_NKVD_Officer_0_LEN_co.paa","","WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Decal_Medal_Of_Valor_ca.paa","\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_NKVD_Officer_0_LEN_co.paa","\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_NKVD_Officer_0_LEN_co.paa"};
	};
	class LIB_GER_Soldier_camo_MP40_w: LIB_GER_soldier_camo_MP40
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_w_co.paa"};
	};
	class LIB_GER_Rifleman_w: LIB_GER_rifleman
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_w_co.paa"};
	};
	class LIB_GER_Rifleman3_w: LIB_GER_rifleman
	{
//		hiddenSelections[] = {"camo","camo2","camo3"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Soldier_1_co.paa","\WW2\Assets_t\Characters\Germans_t\WW2_GER_Wehrmacht\German_Mgunner_0_co.paa","\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Sniper_0_W2_co.paa"};
	};
	class LIB_GER_Recruit_w: LIB_GER_recruit
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_w_co.paa"};
	};
	class LIB_GER_Unequip_w: LIB_GER_unequip
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_w_co.paa"};
	};
	class LIB_GER_Ober_rifleman_w: LIB_GER_ober_rifleman
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_w_co.paa","\WW2\Assets_t\Characters\Germans_t\WW2_Badges\German_Gefreitor_Badge_ca.paa"};
	};
	class LIB_GER_Mgunner_w: LIB_GER_mgunner
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_w_co.paa","\WW2\Assets_t\Characters\Germans_t\WW2_Badges\German_Gefreitor_Badge_ca.paa"};
	};
	class LIB_GER_Stggunner_w: LIB_GER_stggunner
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_w_co.paa","\WW2\Assets_t\Characters\Germans_t\WW2_Badges\German_Gefreitor_Badge_ca.paa"};
	};
	class LIB_GER_AT_soldier_w: LIB_GER_AT_soldier
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_w_co.paa","\WW2\Assets_t\Characters\Germans_t\WW2_Badges\German_Gefreitor_Badge_ca.paa"};
	};
	class LIB_GER_AT_grenadier_w: LIB_GER_AT_grenadier
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_w_co.paa","\WW2\Assets_t\Characters\Germans_t\WW2_Badges\German_Gefreitor_Badge_ca.paa"};
	};
	class LIB_GER_Smgunner_w: LIB_GER_smgunner
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_w_co.paa","\WW2\Assets_t\Characters\Germans_t\WW2_Badges\German_Gefreitor_Badge_ca.paa"};
	};
	class LIB_GER_Unterofficer_w: LIB_GER_unterofficer
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_w_co.paa"};
	};
	class LIB_GER_Lieutenant_w: LIB_GER_lieutenant
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_w_co.paa"};
	};
	class LIB_GER_Ober_lieutenant_w: LIB_GER_ober_lieutenant
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_w_co.paa"};
	};
	class LIB_GER_Hauptmann_w: LIB_GER_hauptmann
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_w_co.paa"};
	};
	class LIB_GER_Oberst_w: LIB_GER_oberst
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_w_co.paa"};
	};
	class LIB_GER_Scout_rifleman_w: LIB_GER_scout_rifleman
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_w_co.paa"};
	};
	class LIB_GER_Scout_ober_rifleman_w: LIB_GER_scout_ober_rifleman
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_w_co.paa"};
	};
	class LIB_GER_Scout_mgunner_w: LIB_GER_scout_mgunner
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_w_co.paa"};
	};
	class LIB_GER_Scout_smgunner_w: LIB_GER_scout_smgunner
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_w_co.paa"};
	};
	class LIB_GER_Scout_unterofficer_w: LIB_GER_scout_unterofficer
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_w_co.paa"};
	};
	class LIB_GER_Scout_lieutenant_w: LIB_GER_scout_lieutenant
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_w_co.paa"};
	};
	class LIB_GER_Scout_sniper_w: LIB_GER_scout_sniper
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Sniper_0_w_co.paa"};
	};
	class LIB_GER_Scout_sniper_2_w: LIB_GER_scout_sniper
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Sniper_0_W2_co.paa"};
	};
	class LIB_GER_Medic_w: LIB_GER_medic
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_w_co.paa"};
	};
	class LIB_GER_Radioman_w: LIB_GER_radioman
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_w_co.paa"};
	};
	class LIB_GER_Sapper_w: LIB_GER_sapper
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_w_co.paa"};
	};
	class LIB_GER_Sapper_gefr_w: LIB_GER_sapper_gefr
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_w_co.paa"};
	};
	class LIB_GER_Gun_crew_w: LIB_GER_gun_crew
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_w_co.paa"};
	};
	class LIB_GER_Gun_unterofficer_w: LIB_GER_gun_unterofficer
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_w_co.paa"};
	};
	class LIB_GER_Gun_lieutenant_w: LIB_GER_gun_lieutenant
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_w_co.paa"};
	};
	class LIB_GER_Rifleman_ADS_w: LIB_GER_rifleman_ADS
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Germans_t\IF_GER_Wehrmacht\German_Soldier_0_Camo_w_co.paa"};
	};
	class LIB_SOV_Soldier_PPSH41_w: LIB_SOV_soldier_PPSH41
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_w_co.paa"};
	};
	class LIB_SOV_Soldier_mosin_rifle_0_w: LIB_SOV_soldier_mosin_rifle_0
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_w_co.paa"};
	};
	class LIB_SOV_Sergeant_PPSH41_0_w: LIB_SOV_sergeant_PPSH41_0
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_w_co.paa"};
	};
	class LIB_SOV_Lieutenant_PPSH41_w: LIB_SOV_lieutenant_PPSH41
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_w_co.paa"};
	};
	class LIB_SOV_Rifleman_w: LIB_SOV_rifleman
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_w_co.paa"};
	};
	class LIB_SOV_Unequip_w: LIB_SOV_unequip
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_w_co.paa"};
	};
	class LIB_SOV_Smgunner_w: LIB_SOV_smgunner
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_w_co.paa"};
	};
	class LIB_SOV_Smgunner_w2: LIB_SOV_smgunner
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_W2_co.paa"};
	};
	class LIB_SOV_LC_rifleman_w: LIB_SOV_LC_rifleman
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_w_co.paa"};
	};
	class LIB_SOV_Mgunner_w: LIB_SOV_mgunner
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_w_co.paa"};
	};
	class LIB_SOV_Medic_w: LIB_SOV_medic
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_w_co.paa"};
	};
	class LIB_SOV_AT_soldier_w: LIB_SOV_AT_soldier
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_w_co.paa"};
	};
	class LIB_SOV_AT_grenadier_w: LIB_SOV_AT_grenadier
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_w_co.paa"};
	};
	class LIB_SOV_Staff_sergeant_w: LIB_SOV_staff_sergeant
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_w_co.paa"};
	};
	class LIB_SOV_Sergeant_w: LIB_SOV_sergeant
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_w_co.paa"};
	};
	class LIB_SOV_P_officer_w: LIB_SOV_p_officer
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_w_co.paa"};
	};
	class LIB_SOV_Lieutenant_w: LIB_SOV_lieutenant
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_w_co.paa"};
	};
	class LIB_SOV_First_lieutenant_w: LIB_SOV_first_lieutenant
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_w_co.paa"};
	};
	class LIB_SOV_Captain_w: LIB_SOV_captain
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_w_co.paa"};
	};
	class LIB_SOV_Scout_rifleman_w: LIB_SOV_scout_rifleman
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_w_co.paa"};
	};
	class LIB_SOV_Scout_smgunner_w: LIB_SOV_scout_smgunner
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_w_co.paa"};
	};
	class LIB_SOV_Scout_mgunner_w: LIB_SOV_scout_mgunner
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_w_co.paa"};
	};
	class LIB_SOV_Scout_sergeant_w: LIB_SOV_scout_sergeant
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_w_co.paa"};
	};
	class LIB_SOV_Scout_p_officer_w: LIB_SOV_scout_p_officer
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_w_co.paa"};
	};
	class LIB_SOV_Scout_lieutenant_w: LIB_SOV_scout_lieutenant
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_w_co.paa"};
	};
	class LIB_SOV_Scout_sniper_w: LIB_SOV_scout_sniper
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Sniper_w_co.paa"};
	};
	class LIB_SOV_Operator_w: LIB_SOV_operator
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_w_co.paa"};
	};
	class LIB_SOV_Sapper_w: LIB_SOV_sapper
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_w_co.paa"};
	};
	class LIB_SOV_Assault_smgunner_w: LIB_SOV_assault_smgunner
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_w_co.paa"};
	};
	class LIB_SOV_Assault_mgunner_w: LIB_SOV_assault_mgunner
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_w_co.paa"};
	};
	class LIB_SOV_Assault_sergeant_w: LIB_SOV_assault_sergeant
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_w_co.paa"};
	};
	class LIB_SOV_Gun_lieutenant_w: LIB_SOV_gun_lieutenant
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_w_co.paa"};
	};
	class LIB_SOV_Rifleman_ADS_w: LIB_SOV_rifleman_ADS
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Characters\Soviets_t\IF_SOV_RKKA\Soviet_Soldier_Camo_0_w_co.paa"};
	};
	class LIB_LCVP: LIB_Boat_base
	{
//		hiddenSelections[] = {"camo_0","camo_1","num0","num1","num2","num3"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Boats_t\IF_LCVP\Lcvp_Korpus_co.paa","\WW2\Assets_t\Vehicles\Boats_t\IF_LCVP\Lcvp_Misc_co.paa"};
	};
	class LIB_C47_Skytrain: LIB_US_Plane_base
	{
//		hiddenSelections[] = {"camo","camo1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Planes_t\WW2_C47\DC3_Body_01_co.paa","\WW2\Assets_t\Vehicles\Planes_t\WW2_C47\DC3_Body_02_co.paa"};
	};
	class LIB_C47_RAF: LIB_C47_Skytrain
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Planes_t\WW2_C47\DC3_Body_01_Raf_co.paa","\WW2\Assets_t\Vehicles\Planes_t\WW2_C47\DC3_Body_02_Raf_co.paa"};
	};
	class LIB_C47_Skytrain_wreck: LIB_PlaneWreck_base
	{
//		hiddenSelections[] = {"camo","camo1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Planes_t\WW2_C47\DC3_Body_01_co.paa","\WW2\Assets_t\Vehicles\Planes_t\WW2_C47\DC3_Body_02_co.paa"};
	};
	class LIB_FW190F8: LIB_GER_Plane_base
	{
//		hiddenSelections[] = {"camo1","camo2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Vehicles\Planes_t\IF_FW190F8\FW190F8_Mn_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_FW190F8\FW190F8_Sd_co.paa"};
	};
	class LIB_FW190F8_2: LIB_FW190F8
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Vehicles\Planes_t\IF_FW190F8\Fw190f8_Mn_1_Ns_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_FW190F8\Fw190f8_Sd_3_co.paa"};
	};
	class LIB_FW190F8_3: LIB_FW190F8
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Vehicles\Planes_t\IF_FW190F8\Fw190f8_Mn_2_Ns_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_FW190F8\FW190F8_Sd_co.paa"};
	};
	class LIB_FW190F8_4: LIB_FW190F8
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Vehicles\Planes_t\IF_FW190F8\Fw190f8_Mn_3_Ns_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_FW190F8\FW190F8_Sd_co.paa"};
	};
	class LIB_FW190F8_5: LIB_FW190F8
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Vehicles\Planes_t\IF_FW190F8\Fw190f8_Mn_4_Ns_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_FW190F8\FW190F8_Sd_co.paa"};
	};
	class LIB_FW190F8_Italy: LIB_FW190F8
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Vehicles\Planes_t\IF_FW190F8\Fw190f8_Mn_Ita1_Ns_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_FW190F8\Fw190f8_Sd_3_co.paa"};
	};
	class LIB_DAK_FW190F8: LIB_FW190F8
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Vehicles\Planes_t\IF_FW190F8\Fw190f8_Mn_Ita1_Ns_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_FW190F8\Fw190f8_Sd_3_co.paa"};
	};
	class LIB_HORSA: LIB_US_Plane_base
	{
//		hiddenSelections[] = {"camo","num1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Planes_t\I44_HORSA\horsa_co.paa","\WW2\Assets_t\Vehicles\Planes_t\I44_HORSA\code\0.paa"};
	};
	class LIB_HORSA_RAF: LIB_HORSA
	{
//		hiddenSelections[] = {"camo","num1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Planes_t\I44_HORSA\horsa_raf_co.paa","\WW2\Assets_t\Vehicles\Planes_t\I44_HORSA\code\0.paa"};
	};
	class LIB_HORSA_Wreck: LIB_PlaneWreck_base
	{
//		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Planes_t\I44_HORSA\horsa_co.paa"};
	};
	class LIB_Ju87: LIB_GER_Plane_base
	{
//		hiddenSelections[] = {"camo1","camo2","camo3","camo4"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"WW2\Assets_t\Vehicles\Planes_t\IF_Ju87\Ju87_Korpus_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_Ju87\Ju87_Wings_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_Ju87\Ju87_Cabine_1_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_Ju87\Ju87_Cabine_2_co.paa"};
	};
	class LIB_ARR_Ju87: LIB_Ju87
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"WW2\Assets_t\Vehicles\Planes_t\IF_Ju87\Ju87_Korpus_Arr_1_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_Ju87\Ju87_Wings_Arr_1_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_Ju87\Ju87_Cabine_1_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_Ju87\Ju87_Cabine_2_co.paa"};
	};
	class LIB_MKHL_Ju87: LIB_Ju87
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"WW2\Assets_t\Vehicles\Planes_t\IF_Ju87\Ju87_Korpus_Mkhl_1_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_Ju87\Ju87_Wings_Mkhl_1_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_Ju87\Ju87_Cabine_1_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_Ju87\Ju87_Cabine_2_co.paa"};
	};
	class LIB_RBAF_Ju87: LIB_Ju87
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"WW2\Assets_t\Vehicles\Planes_t\IF_Ju87\Ju87_Korpus_Rbaf_1_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_Ju87\Ju87_Wings_Rbaf_1_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_Ju87\Ju87_Cabine_1_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_Ju87\Ju87_Cabine_2_co.paa"};
	};
	class LIB_Ju87_Italy: LIB_Ju87
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"WW2\Assets_t\Vehicles\Planes_t\IF_Ju87\Ju87_Korpus_Med1_Ns_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_Ju87\Ju87_Wings_Med1_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_Ju87\Ju87_Cabine_1_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_Ju87\Ju87_Cabine_2_co.paa"};
	};
	class LIB_Ju87_Italy2: LIB_Ju87
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"WW2\Assets_t\Vehicles\Planes_t\IF_Ju87\Ju87_Korpus_Med2_Ns_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_Ju87\Ju87_Wings_Med2_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_Ju87\Ju87_Cabine_1_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_Ju87\Ju87_Cabine_2_co.paa"};
	};
	class LIB_DAK_Ju87: LIB_Ju87
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"WW2\Assets_t\Vehicles\Planes_t\IF_Ju87\Ju87_Korpus_Dak1_Ns_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_Ju87\Ju87_Wings_Dak1_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_Ju87\Ju87_Cabine_1_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_Ju87\Ju87_Cabine_2_co.paa"};
	};
	class LIB_DAK_Ju87_2: LIB_Ju87
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"WW2\Assets_t\Vehicles\Planes_t\IF_Ju87\Ju87_Korpus_Dak2_Ns_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_Ju87\Ju87_Wings_Dak2_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_Ju87\Ju87_Cabine_1_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_Ju87\Ju87_Cabine_2_co.paa"};
	};
	class LIB_Li2: LIB_C47_Skytrain
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Planes_t\WW2_C47\DC3_Body_01_Soviet_co.paa","\WW2\Assets_t\Vehicles\Planes_t\WW2_C47\DC3_Body_02_Soviet_co.paa"};
	};
	class LIB_P39: LIB_SU_Plane_base
	{
//		hiddenSelections[] = {"camo1","camo2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Vehicles\Planes_t\IF_P39\P39_Fusellage_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_P39\P39_Wings_co.paa"};
	};
	class LIB_RA_P39_2: LIB_P39
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Vehicles\Planes_t\IF_P39\P39_Ra_Fusellage_1_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_P39\P39_Ra_Wings_1_co.paa"};
	};
	class LIB_RA_P39_3: LIB_P39
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Vehicles\Planes_t\IF_P39\P39_Ra_Fusellage_2_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_P39\P39_Ra_Wings_2_co.paa"};
	};
	class LIB_US_P39: LIB_P39
	{
//		hiddenSelections[] = {"camo1","camo2","camo3"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Vehicles\Planes_t\IF_P39\P39_US_Fusellage_4_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_P39\P39_US_Wings_4_co.paa"};
	};
	class LIB_US_P39_2: LIB_US_P39
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Vehicles\Planes_t\IF_P39\P39_US_Fusellage_5_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_P39\P39_US_Wings_5_co.paa"};
	};
	class LIB_RAF_P39: LIB_US_P39
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Vehicles\Planes_t\IF_P39\P39_Raf_Fusellage_1_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_P39\P39_Raf_Wings_1_co.paa"};
	};
	class LIB_RAAF_P39: LIB_US_P39
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Vehicles\Planes_t\IF_P39\P39_Raaf_Fusellage_1_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_P39\P39_Raaf_Wings_1_co.paa"};
	};
	class LIB_ACI_P39: LIB_US_P39
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Vehicles\Planes_t\IF_P39\P39_Aci_Fusellage_1_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_P39\P39_Aci_Wings_1_co.paa"};
	};
	class LIB_US_NAC_P39: LIB_P39
	{
//		hiddenSelections[] = {"camo1","camo2","camo3"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"WW2\Assets_t\Vehicles\Planes_t\IF_P39\P39_US_Fusellage_1_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_P39\P39_US_Wings_1_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_P39\P39_Cabine2_co.paa"};
	};
	class LIB_US_NAC_P39_2: LIB_US_NAC_P39
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Vehicles\Planes_t\IF_P39\P39_US_Fusellage_2_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_P39\P39_US_Wings_2_co.paa"};
	};
	class LIB_US_NAC_P39_3: LIB_US_NAC_P39
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Vehicles\Planes_t\IF_P39\P39_US_Fusellage_3_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_P39\P39_US_Wings_3_co.paa"};
	};
	class LIB_P47: LIB_US_Plane_base
	{
//		hiddenSelections[] = {"camo1","camo2","camo3"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"WW2\Assets_t\Vehicles\Planes_t\IF_P47\P47_Fusellage_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_P47\P47_Wings_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_P47\P47_Cabine_co.paa"};
	};
	class LIB_Pe2: LIB_SU_Plane_base
	{
//		hiddenSelections[] = {"camo_1","camo_2"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Vehicles\Planes_t\IF_Pe2\Pe_2texture2048.paa","WW2\Assets_t\Vehicles\Planes_t\IF_Pe2\Pe_2cabinmaintexture.paa"};
	};
	class LIB_US_Parachute: LIB_Parachute_base
	{
//		hiddenSelections[] = {};
		hiddenSelectionsTextures[] = {};//{};
	};
	class LIB_GER_Parachute: LIB_Parachute_base
	{
//		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Planes_t\IF_Para\German_Parachute_WW2_co.paa"};
	};
	class LIB_SOV_Parachute: LIB_Parachute_base
	{
//		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Planes_t\IF_Para\Green_Parachute_WW2_co.paa"};
	};
	class LIB_ParachuteLanded_base: Thing
	{
//		hiddenSelections[] = {"camo"};
	};
	class LIB_US_ParachuteLanded: LIB_ParachuteLanded_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Planes_t\IF_Para\US_Parachute_WW2_co.paa"};
	};
	class LIB_GER_ParachuteLanded: LIB_ParachuteLanded_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Planes_t\IF_Para\German_Parachute_WW2_co.paa"};
	};
	class LIB_SOV_ParachuteLanded: LIB_ParachuteLanded_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Planes_t\IF_Para\Green_Parachute_WW2_co.paa"};
	};
	class LIB_CG4_WACO: LIB_US_Plane_base
	{
//		hiddenSelections[] = {"camo","num1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Planes_t\I44_WACO\waco_co.paa","\WW2\Assets_t\Vehicles\Planes_t\I44_WACO\code\0.paa"};
	};
	class LIB_MKI_HADRIAN: LIB_CG4_WACO
	{
//		hiddenSelections[] = {"camo","num1"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Planes_t\I44_WACO\hadrian_camo_co.paa","\WW2\Assets_t\Vehicles\Planes_t\I44_WACO\code\0.paa"};
	};
	class LIB_CG4_WACO_Wreck: LIB_PlaneWreck_base
	{
//		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Planes_t\I44_WACO\waco_co.paa"};
	};
	class LIB_Nebelwerfer41_base: LIB_StaticCannon_base
	{
//		hiddenSelections[] = {"camo"};
	};
	class LIB_Nebelwerfer41: LIB_Nebelwerfer41_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\StaticWeapons_t\I44_Nebelwerfer\i44_nebelwerfer_co.paa"};
	};
	class LIB_Pak40_base: LIB_StaticCannon_base
	{
//		hiddenSelections[] = {"camo_0"};
	};
	class LIB_Pak40: LIB_Pak40_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\StaticWeapons_t\IF_Pak40\Pak_40_co.paa"};
	};
	class LIB_Pak40_g: LIB_Pak40
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\StaticWeapons_t\IF_Pak40\Pak_40_Grau_co.paa"};
	};
	class LIB_DAK_Pak40: LIB_Pak40
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\StaticWeapons_t\IF_Pak40\Pak_40_DAK_co.paa"};
	};
	class LIB_Kfz1_base: LIB_Car_base
	{
//		hiddenSelections[] = {"camo_1","camo_2","num1","num2","num3","num4","num5","num6"};
	};
	class LIB_Kfz1: LIB_Kfz1_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Cars_t\IF_Kfz1\Kubelwagen_Main_co.paa","\WW2\Assets_t\Vehicles\Cars_t\IF_Kfz1\Kubelwagen_Interior_co.paa"};
	};
	class LIB_Kfz1_MG42: LIB_Kfz1_base
	{
//		hiddenSelections[] = {"camo_1","camo_2","camo_3","camo_4","camo_5","num1","num2","num3","num4","num5","num6"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Cars_t\IF_Kfz1\Kubelwagen_Main_co.paa","\WW2\Assets_t\Vehicles\Cars_t\IF_Kfz1\Kubelwagen_Interior_co.paa","WW2\Assets_t\Vehicles\Cars_t\IF_Kfz1\Kubelwagen_turret.paa","WW2\Assets_t\Weapons\MachineGun_Light_t\IF_MG42\Mg42_co.paa","WW2\Assets_t\Weapons\MachineGun_Light_t\IF_MG42\Drum_co.paa"};
	};
	class LIB_Willys_MB_base: LIB_Car_base
	{
//		hiddenSelections[] = {"camo_1","n0","n1","n2","n3","n4","n5"};
	};
	class LIB_Willys_MB: LIB_Willys_MB_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"WW2\Assets_t\Vehicles\Cars_t\IF_Willys_MB\Willys_co.paa"};
	};
	class LIB_OpelBlitz_base: LIB_Truck_base
	{
//		hiddenSelections[] = {"Camo_1","Camo_2","Camo_3","Camo_4","num1","num2","num3","num4","num5","num6"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel1_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel2_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel3_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel4_co.paa"};
	};
	class LIB_Scout_M3_base: LIB_Truck_base
	{
//		hiddenSelections[] = {"camo_1","camo_2","camo_3","n0","n1","n2","n3","n4","n5"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3\M3skout_Korpus_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3\M3skout_Interior_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3\BrowningM2_co.paa"};
	};
	class LIB_US_Scout_M3: LIB_Scout_M3_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M3skout_Korpus_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M3skout_Interior_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\Browningm2_co.paa"};
	};
	class LIB_US_GMC_Base: LIB_Truck_base
	{
//		hiddenSelections[] = {"Camo_1","Camo_2","usa","labels","num0","num1","num2","num3","num4","num5","num6"};
	};
	class LIB_US_GMC_Open: LIB_US_GMC_Base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Img\Usa.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\marker_na_dver.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\4.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\1.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\3.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\0.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\0.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\4.paa"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\Gmc353_co.paa","","\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Img\Usa.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\marker_na_dver.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\4.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\1.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\3.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\0.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\0.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\4.paa"};
	};
	class LIB_US_GMC_Tent: LIB_US_GMC_Open
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Img\Usa.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\marker_na_dver.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\7.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\1.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\3.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\0.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\1.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\6.paa"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\Gmc353_co.paa","","\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Img\Usa.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\marker_na_dver.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\7.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\1.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\3.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\0.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\1.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\6.paa"};
	};
	class LIB_SdKfz251_base: LIB_WheeledTracked_APC_base
	{
//		hiddenSelections[] = {"camo_0","camo_1","camo_2","camo_3","camo_4","camo_5","num1","num2","num3"};
	};
	class LIB_SdKfz_7_base: LIB_WheeledTracked_APC_base
	{
//		hiddenSelections[] = {"camo_1","camo_2","camo_3"};
	};
	class LIB_SdKfz_7_AA_base: LIB_SdKfz_7_base
	{
//		hiddenSelections[] = {"camo_1","camo_2","camo_3","camo_4","camo_5","camo_6"};
	};
	class LIB_Halftrack_base: LIB_WheeledTracked_APC_base
	{
//		hiddenSelections[] = {"camo1","camo2","camo3","camo4","camo5","num0","num1","num2","num3","num4","num5","usa","labels"};
	};
	class LIB_M4A3_75_base: LIB_Tank_base
	{
//		hiddenSelections[] = {"camo_0","camo_1","camo_2","camo_3","num_usa","num_0","num_1","num_2","num_3","num_4","num_5","num_6","num_7","num_8","num_9"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_M4A3_75\Body_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_M4A3_75\Turret_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_M4A3_75\Wheels_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_M4A3_75\Tracks_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_M4A3_75\Usa_ca.paa"};
	};
	class LIB_US_NAC_M4A3_75: LIB_M4A3_75_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_M4A3_75\Body_Nac_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_M4A3_75\Turret_Nac_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_M4A3_75\Wheels_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_M4A3_75\Tracks_T41_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_M4A3_75\Usa_ca.paa"};
		hiddenSelectionsMaterials[] = {"","","","\WW2\Assets_r\Vehicles\Tanks_r\IF_M4A3_75\Tracks_T41.rvmat",""};
	};
	class LIB_M4A2_SOV: LIB_M4A3_75_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_M4A3_75\Body_Soviet_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_M4A3_75\Turret_Soviet_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_M4A3_75\Wheels_Soviet_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_M4A3_75\Tracks_co.paa"};
	};
	class LIB_PzKpfwIV_H_base: LIB_Tank_base
	{
//		hiddenSelections[] = {"camo_1","camo_2","camo_3","camo_4","num1","num2","num3"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwIV_H\Body_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwIV_H\Turret_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwIV_H\Wheels_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwIV_H\Tracks_co.paa"};
	};
	class LIB_DAK_PzKpfwIV_H: LIB_PzKpfwIV_H
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwIV_H\Body_DAK_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwIV_H\Turret_DAK_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwIV_H\Wheels_DAK_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwIV_H\Tracks_Nac_co.paa"};
	};
	class LIB_GazM1_base: LIB_Car_base
	{
//		hiddenSelections[] = {"camo","camo1","camo2"};
	};
	class LIB_GazM1: LIB_GazM1_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\ww2\assets_t\vehicles\cars_t\ww2_gazm1\gazm1_co.paa","\ww2\assets_t\vehicles\cars_t\ww2_gazm1\base_co.paa","\ww2\assets_t\vehicles\cars_t\ww2_gazm1\pribor_co.paa"};
	};
	class LIB_GazM1_SOV: LIB_GazM1_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\ww2\assets_t\vehicles\cars_t\ww2_gazm1\gazm1_camo_co.paa","\ww2\assets_t\vehicles\cars_t\ww2_gazm1\base_co.paa","\ww2\assets_t\vehicles\cars_t\ww2_gazm1\pribor_co.paa"};
	};
	class LIB_GazM1_FFI: LIB_GazM1_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\ww2\assets_t\vehicles\cars_t\ww2_gazm1\gazm1_ffi_co.paa","\ww2\assets_t\vehicles\cars_t\ww2_gazm1\base_co.paa","\ww2\assets_t\vehicles\cars_t\ww2_gazm1\pribor_co.paa"};
	};
	class LIB_US_GMC_Ammo: LIB_US_GMC_Base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Img\Usa.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\marker_na_dver.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\1.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\4.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\4.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\2.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\8.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\9.paa"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\Gmc353_co.paa","","\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Img\Usa.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\marker_na_dver.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\1.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\4.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\4.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\2.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\8.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\9.paa"};
	};
	class LIB_US_GMC_Ambulance: LIB_US_GMC_Base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","#(argb,8,8,3)color(1,1,1,1,ca)","#(argb,8,8,3)color(0,1,1,1,ca)","#(argb,8,8,3)color(0.5,0.5,0.5,1,ca)","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\1.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\4.paa"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\Gmc353_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\gmc353_medic.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Img\Usa.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\marker_na_dver.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\8.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\6.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\5.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\0.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\1.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\4.paa"};
	};
	class LIB_US_GMC_Parm: LIB_US_GMC_Base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","#(argb,8,8,3)color(1,1,1,1,ca)","#(argb,8,8,3)color(0,1,1,1,ca)","#(argb,8,8,3)color(0.5,0.5,0.5,1,ca)","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\1.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\4.paa"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\Gmc353_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\gmcst6ruck.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Img\Usa.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\marker_na_dver.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\8.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\6.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\5.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\0.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\1.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\4.paa"};
	};
	class LIB_US_GMC_Fuel: LIB_US_GMC_Base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","#(argb,8,8,3)color(1,1,1,1,ca)","#(argb,8,8,3)color(0,1,1,1,ca)","#(argb,8,8,3)color(0.5,0.5,0.5,1,ca)","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\8.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\9.paa"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\Gmc353_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\gmc_fuel_truck_w_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Img\Usa.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\marker_na_dver.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\1.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\4.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\4.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\2.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\8.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\9.paa"};
	};
	class LIB_US_NAC_GMC_Open: LIB_US_GMC_Open
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Img\Usa.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\marker_na_dver.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\4.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\1.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\3.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\0.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\0.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\4.paa"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\Gmc353_d_co.paa","","\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Img\Usa.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\marker_na_dver.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\4.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\1.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\3.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\0.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\0.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\4.paa"};
	};
	class LIB_US_NAC_GMC_Tent: LIB_US_GMC_Tent
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Img\Usa.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\marker_na_dver.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\7.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\1.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\3.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\0.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\1.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\6.paa"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\Gmc353_d_co.paa","","\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Img\Usa.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\marker_na_dver.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\7.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\1.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\3.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\0.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\1.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\6.paa"};
	};
	class LIB_US_NAC_GMC_Ammo: LIB_US_GMC_Ammo
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Img\Usa.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\marker_na_dver.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\1.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\4.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\4.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\2.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\8.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\9.paa"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\Gmc353_d_co.paa","","\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Img\Usa.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\marker_na_dver.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\1.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\4.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\4.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\2.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\8.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\9.paa"};
	};
	class LIB_US_NAC_GMC_Ambulance: LIB_US_GMC_Ambulance
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","#(argb,8,8,3)color(1,1,1,1,ca)","#(argb,8,8,3)color(0,1,1,1,ca)","#(argb,8,8,3)color(0.5,0.5,0.5,1,ca)","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\1.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\4.paa"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\Gmc353_d_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\gmc353_medic.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Img\Usa.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\marker_na_dver.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\8.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\6.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\5.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\0.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\1.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\4.paa"};
	};
	class LIB_US_NAC_GMC_Parm: LIB_US_GMC_Parm
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","#(argb,8,8,3)color(1,1,1,1,ca)","#(argb,8,8,3)color(0,1,1,1,ca)","#(argb,8,8,3)color(0.5,0.5,0.5,1,ca)","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\1.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\4.paa"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\Gmc353_d_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\gmcst6ruck.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Img\Usa.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\marker_na_dver.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\8.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\6.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\5.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\0.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\1.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\4.paa"};
	};
	class LIB_US_NAC_GMC_Fuel: LIB_US_GMC_Fuel
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","#(argb,8,8,3)color(1,1,1,1,ca)","#(argb,8,8,3)color(0,1,1,1,ca)","#(argb,8,8,3)color(0.5,0.5,0.5,1,ca)","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\1.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\4.paa"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\Gmc353_d_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\gmc_fuel_truck.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Img\Usa.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\marker_na_dver.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\8.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\6.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\5.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\0.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\1.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\4.paa"};
	};
	class LIB_DAK_Kfz1: LIB_Kfz1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Cars_t\IF_Kfz1\Kubelwagen_Main_DAK_co.paa","\WW2\Assets_t\Vehicles\Cars_t\IF_Kfz1\Kubelwagen_Interior_co.paa"};
	};
	class LIB_DAK_opelblitz_open: LIB_opelblitz_open_y_camo
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel1_DAK_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel2_DAK_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel3_DAK_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel4_co.paa"};
	};
	class LIB_DAK_opelblitz_open_2: LIB_DAK_opelblitz_open
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel1_DAK_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel2_DAK_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel3_Dak2_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel4_co.paa"};
	};
	class LIB_DAK_opelblitz_open_3: LIB_DAK_opelblitz_open
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel1_DAK_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel2_DAK_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel3_Dak3_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel4_co.paa"};
	};
	class LIB_DAK_opelblitz_tent: LIB_opelblitz_tent_y_camo
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel1_DAK_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel2_DAK_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel3_DAK_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel4_co.paa"};
	};
	class LIB_DAK_opelblitz_tent_2: LIB_DAK_opelblitz_tent
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel1_DAK_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel2_DAK_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel3_Dak2_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel4_co.paa"};
	};
	class LIB_DAK_opelblitz_tent_3: LIB_DAK_opelblitz_tent
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel1_DAK_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel2_DAK_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel3_Dak3_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel4_co.paa"};
	};
	class LIB_opelblitz_fuel: LIB_OpelBlitz_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel1_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel2_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel3_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opelbenzavoz.paa"};
	};
	class LIB_DAK_opelblitz_fuel: LIB_opelblitz_fuel
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel1_DAK_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel2_DAK_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel3_DAK_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel_Fuel_DAK_co.paa"};
	};
	class LIB_opelblitz_ambulance: LIB_OpelBlitz_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel1_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel2_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel3_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel_ambulance.paa"};
	};
	class LIB_DAK_opelblitz_ambulance: LIB_opelblitz_ambulance
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel1_DAK_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel2_DAK_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel3_Dak2_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel_Ambulance_DAK_co.paa"};
	};
	class LIB_opelblitz_parm: LIB_OpelBlitz_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel1_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel2_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel3_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel_parm.paa"};
	};
	class LIB_DAK_opelblitz_parm: LIB_opelblitz_parm
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel1_DAK_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel2_DAK_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel3_DAK_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel_Parm_DAK_co.paa"};
	};
	class LIB_DAK_opelblitz_ammo: LIB_opelblitz_ammo
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel1_DAK_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel2_DAK_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel3_DAK_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel4_co.paa"};
	};
	class LIB_DAK_Scout_M3: LIB_US_Scout_M3
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3\M3skout_Korpus_DAK_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M3skout_Interior_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\Browningm2_co.paa"};
	};
	class LIB_US_NAC_Scout_M3: LIB_US_Scout_M3
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M3skout_Korpus_Nac_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M3skout_Interior_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\Browningm2_co.paa"};
	};
	class LIB_US_Scout_M3_FFV: LIB_Scout_M3_FFV
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M3skout_Korpus_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M3skout_Interior_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\Browningm2_co.paa"};
	};
	class LIB_DAK_Scout_M3_FFV: LIB_US_Scout_M3_FFV
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3\M3skout_Korpus_DAK_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M3skout_Interior_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\Browningm2_co.paa"};
	};
	class LIB_US_NAC_Scout_M3_FFV: LIB_US_Scout_M3_FFV
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M3skout_Korpus_Nac_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M3skout_Interior_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\Browningm2_co.paa"};
	};
	class LIB_US_Willys_MB: LIB_Willys_MB_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Cars_t\IF_Willys_MB\Willys_US_co.paa"};
	};
	class LIB_US_NAC_Willys_MB: LIB_Willys_MB_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Cars_t\IF_Willys_MB\Willys_Nac_co.paa"};
	};
	class LIB_SdKfz251: LIB_SdKfz251_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","#(argb,8,8,3)color(1,1,1,1,ca)","#(argb,8,8,3)color(0,1,1,1,ca)","#(argb,8,8,3)color(0.5,0.5,0.5,1,ca)","\WW2\Core_t\IF_Decals_t\German\_num\5.paa"};//{"\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Hull_co.paa","\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Interrior_co.paa","\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Panel_co.paa","WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Track\0_ca.paa","\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Wheels_co.paa","WW2\Assets_t\Weapons\MachineGun_Light_t\IF_MG42\Mg42_co.paa","\WW2\Core_t\IF_Decals_t\German\_num\2.paa","\WW2\Core_t\IF_Decals_t\German\_num\5.paa","\WW2\Core_t\IF_Decals_t\German\_num\5.paa"};
	};
	class LIB_DAK_SdKfz251: LIB_SdKfz251_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","#(argb,8,8,3)color(1,1,1,1,ca)","#(argb,8,8,3)color(0,1,1,1,ca)","#(argb,8,8,3)color(0.5,0.5,0.5,1,ca)","\WW2\Core_t\IF_Decals_t\German\_num\5.paa"};//{"\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Hull_DAK_co.paa","\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Interrior_co.paa","\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Panel_co.paa","WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Track\0_ca.paa","\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Wheels_co.paa","WW2\Assets_t\Weapons\MachineGun_Light_t\IF_MG42\Mg42_co.paa","\WW2\Core_t\IF_Decals_t\German\_num\2.paa","\WW2\Core_t\IF_Decals_t\German\_num\5.paa","\WW2\Core_t\IF_Decals_t\German\_num\5.paa"};
	};
	class LIB_SdKfz251_captured: LIB_SdKfz251_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","#(argb,8,8,3)color(1,1,1,1,ca)","","",""};//{"\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Hull_Soviet_co.paa","\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Interrior_co.paa","\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Panel_co.paa","WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Track\0_ca.paa","\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Wheels_co.paa","WW2\Assets_t\Weapons\MachineGun_Light_t\IF_MG42\Mg42_co.paa","","",""};
	};
	class LIB_SdKfz251_FFV: LIB_SdKfz251_FFV_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","#(argb,8,8,3)color(1,1,1,1,ca)","#(argb,8,8,3)color(0,1,1,1,ca)","#(argb,8,8,3)color(0.5,0.5,0.5,1,ca)","\WW2\Core_t\IF_Decals_t\German\_num\5.paa"};//{"\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Hull_co.paa","\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Interrior_co.paa","\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Panel_co.paa","WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Track\0_ca.paa","\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Wheels_co.paa","WW2\Assets_t\Weapons\MachineGun_Light_t\IF_MG42\Mg42_co.paa","\WW2\Core_t\IF_Decals_t\German\_num\2.paa","\WW2\Core_t\IF_Decals_t\German\_num\5.paa","\WW2\Core_t\IF_Decals_t\German\_num\5.paa"};
	};
	class LIB_DAK_SdKfz251_FFV: LIB_SdKfz251_FFV_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","#(argb,8,8,3)color(1,1,1,1,ca)","#(argb,8,8,3)color(0,1,1,1,ca)","#(argb,8,8,3)color(0.5,0.5,0.5,1,ca)","\WW2\Core_t\IF_Decals_t\German\_num\5.paa"};//{"\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Hull_DAK_co.paa","\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Interrior_co.paa","\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Panel_co.paa","WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Track\0_ca.paa","\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Wheels_co.paa","WW2\Assets_t\Weapons\MachineGun_Light_t\IF_MG42\Mg42_co.paa","\WW2\Core_t\IF_Decals_t\German\_num\2.paa","\WW2\Core_t\IF_Decals_t\German\_num\5.paa","\WW2\Core_t\IF_Decals_t\German\_num\5.paa"};
	};
	class LIB_SdKfz251_captured_FFV: LIB_SdKfz251_FFV_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","#(argb,8,8,3)color(1,1,1,1,ca)","","",""};//{"\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Hull_Soviet_co.paa","\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Interrior_co.paa","\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Panel_co.paa","WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Track\0_ca.paa","\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Wheels_co.paa","WW2\Assets_t\Weapons\MachineGun_Light_t\IF_MG42\Mg42_co.paa","","",""};
	};
	class LIB_SdKfz_7: LIB_SdKfz_7_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz7\SdKfz_7_1_co.paa","WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz7\SdKfz_7_2_co.paa","WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz7\SdKfz_7_Track_co.paa"};
	};
	class LIB_DAK_SdKfz_7: LIB_SdKfz_7_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz7\Sdkfz_7_1_DAK_co.paa","WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz7\Sdkfz_7_2_DAK_co.paa","WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz7\SdKfz_7_Track_co.paa"};
	};
	class LIB_SdKfz_7_AA: LIB_SdKfz_7_AA_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","#(argb,8,8,3)color(1,1,1,1,ca)"};//{"WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz7\SdKfz_7_1_co.paa","WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz7\SdKfz_7_2_co.paa","WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz7\SdKfz_7_Track_co.paa","WW2\Assets_t\Vehicles\StaticWeapons_t\IF_FlakVierling_38\Flakvierling_Armor_co.paa","WW2\Assets_t\Vehicles\StaticWeapons_t\IF_FlakVierling_38\Flakvierling_Gun_co.paa","WW2\Assets_t\Vehicles\StaticWeapons_t\IF_FlakVierling_38\Flakvierling_Sight_co.paa"};
	};
	class LIB_DAK_SdKfz_7_AA: LIB_SdKfz_7_AA_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","#(argb,8,8,3)color(1,1,1,1,ca)"};//{"WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz7\Sdkfz_7_1_DAK_co.paa","WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz7\Sdkfz_7_2_DAK_co.paa","WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz7\SdKfz_7_Track_co.paa","WW2\Assets_t\Vehicles\StaticWeapons_t\IF_FlakVierling_38\Flakvierling_Armor_co.paa","WW2\Assets_t\Vehicles\StaticWeapons_t\IF_FlakVierling_38\Flakvierling_Gun_co.paa","WW2\Assets_t\Vehicles\StaticWeapons_t\IF_FlakVierling_38\Flakvierling_Sight_co.paa"};
	};
	class LIB_US_101AB_FC_rifleman: LIB_US_AB_M42_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","","\WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Badg_101st_ca.paa"};//{"\WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\Ropa6_co.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Shld_1stclass_ca.paa","","\WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Badg_101st_ca.paa"};
	};
	class LIB_US_101AB_corporal: LIB_US_AB_M42_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","","\WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Badg_101st_ca.paa"};//{"\WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\Ropa6_co.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Shld_Corporal_ca.paa","","\WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Badg_101st_ca.paa"};
	};
	class LIB_US_101AB_mgunner: LIB_US_AB_M42_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","","\WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Badg_101st_ca.paa"};//{"\WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\Ropa6_co.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Shld_1stclass_ca.paa","","\WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Badg_101st_ca.paa"};
	};
	class LIB_US_101AB_AT_soldier: LIB_US_AB_M42_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","","\WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Badg_101st_ca.paa"};//{"\WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\Ropa6_co.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Shld_1stclass_ca.paa","","\WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Badg_101st_ca.paa"};
	};
	class LIB_US_101AB_NCO: LIB_US_AB_M42_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","","\WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Badg_101st_ca.paa"};//{"\WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\Ropa6_co.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Shld_Sergeant_ca.paa","","\WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Badg_101st_ca.paa"};
	};
	class LIB_US_82AB_FC_rifleman: LIB_US_AB_M43_Flag_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","","\WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Badg_82nd_ca.paa"};//{"\WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\Ropa6_M43_co.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Shld_1stclass_ca.paa","","\WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Badg_82nd_ca.paa"};
	};
	class LIB_US_82AB_medic: LIB_US_AB_M43_Medic_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\Ropa6_M43_co.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Shld_1stclassT_ca.paa","\WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\Armband_Medic_co.paa","\WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Badg_82nd_ca.paa"};
	};
	class LIB_US_82AB_corporal: LIB_US_AB_M43_Flag_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","","\WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Badg_82nd_ca.paa"};//{"\WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\Ropa6_M43_co.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Shld_Corporal_ca.paa","","\WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Badg_82nd_ca.paa"};
	};
	class LIB_US_82AB_mgunner: LIB_US_AB_M43_Flag_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","","\WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Badg_82nd_ca.paa"};//{"\WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\Ropa6_M43_co.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Shld_1stclass_ca.paa","","\WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Badg_82nd_ca.paa"};
	};
	class LIB_US_82AB_AT_soldier: LIB_US_AB_M43_Flag_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","","\WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Badg_82nd_ca.paa"};//{"\WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\Ropa6_M43_co.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Shld_1stclass_ca.paa","","\WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Badg_82nd_ca.paa"};
	};
	class LIB_US_82AB_NCO: LIB_US_AB_M43_Flag_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","","\WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Badg_82nd_ca.paa"};//{"\WW2\Assets_t\Characters\Americans_t\WW2_US_Airborne\Ropa6_M43_co.paa","WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Shld_Sergeant_ca.paa","","\WW2\Assets_t\Characters\Americans_t\WW2_Badges\US_Badg_82nd_ca.paa"};
	};
	class WW2_Cow_Dead1_1: Static
	{
//		hiddenSelections[] = {"Camo"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Misc\Dead_t\WW2_Cows\cow1_co.paa"};
	};
	class WW2_Cow_Dead1_2: WW2_Cow_Dead1_1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Misc\Dead_t\WW2_Cows\cow2_co.paa"};
	};
	class WW2_Cow_Dead1_3: WW2_Cow_Dead1_1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Misc\Dead_t\WW2_Cows\cow3_co.paa"};
	};
	class WW2_Cow_Dead1_4: WW2_Cow_Dead1_1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Misc\Dead_t\WW2_Cows\cow4_co.paa"};
	};
	class WW2_Cow_Dead1_5: WW2_Cow_Dead1_1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Misc\Dead_t\WW2_Cows\cow5_co.paa"};
	};
	class WW2_Cow_Dead1_6: WW2_Cow_Dead1_1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Misc\Dead_t\WW2_Cows\cow6_co.paa"};
	};
	class WW2_Cow_Dead2_1: WW2_Cow_Dead1_1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Misc\Dead_t\WW2_Cows\cow1_co.paa"};
	};
	class WW2_Cow_Dead2_2: WW2_Cow_Dead2_1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Misc\Dead_t\WW2_Cows\cow2_co.paa"};
	};
	class WW2_Cow_Dead2_3: WW2_Cow_Dead2_1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Misc\Dead_t\WW2_Cows\cow3_co.paa"};
	};
	class WW2_Cow_Dead2_4: WW2_Cow_Dead2_1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Misc\Dead_t\WW2_Cows\cow4_co.paa"};
	};
	class WW2_Cow_Dead2_5: WW2_Cow_Dead2_1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Misc\Dead_t\WW2_Cows\cow5_co.paa"};
	};
	class WW2_Cow_Dead2_6: WW2_Cow_Dead2_1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Misc\Dead_t\WW2_Cows\cow6_co.paa"};
	};
	class LIB_FlakPanzerIV_Wirbelwind: LIB_PzKpfwIV_H_base
	{
//		hiddenSelections[] = {"camo_1","camo_2","camo_3","camo_4","camo_5","camo_6","camo_7","camo_8"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","#(argb,8,8,3)color(1,1,1,1,ca)","#(argb,8,8,3)color(0,1,1,1,ca)","#(argb,8,8,3)color(0.5,0.5,0.5,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwIV_H\Body_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwIV_H\Turret_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwIV_H\Wheels_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwIV_H\Tracks_co.paa","\WW2\Assets_t\Vehicles\StaticWeapons_t\IF_FlakVierling_38\Flakvierling_Armor_co.paa","\WW2\Assets_t\Vehicles\StaticWeapons_t\IF_FlakVierling_38\Flakvierling_Gun_co.paa","\WW2\Assets_t\Vehicles\StaticWeapons_t\IF_FlakVierling_38\Flakvierling_Sight_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\WW2_Flakpanzer\Flakpanzer_Turret_co.paa"};
	};
	class LIB_DAK_FlakPanzerIV_Wirbelwind: LIB_FlakPanzerIV_Wirbelwind
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","#(argb,8,8,3)color(1,1,1,1,ca)","#(argb,8,8,3)color(0,1,1,1,ca)","#(argb,8,8,3)color(0.5,0.5,0.5,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwIV_H\Body_DAK_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwIV_H\Turret_DAK_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwIV_H\Wheels_DAK_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwIV_H\Tracks_Nac_co.paa","\WW2\Assets_t\Vehicles\StaticWeapons_t\IF_FlakVierling_38\Flakvierling_Armor_co.paa","\WW2\Assets_t\Vehicles\StaticWeapons_t\IF_FlakVierling_38\Flakvierling_Gun_co.paa","\WW2\Assets_t\Vehicles\StaticWeapons_t\IF_FlakVierling_38\Flakvierling_Sight_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\WW2_Flakpanzer\Flakpanzer_Turret_DAK_co.paa"};
	};
	class LIB_M3A3_Stuart: LIB_M4A3_75
	{
//		hiddenSelections[] = {"camo_0","camo_1","camo_2","camo_3"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\assets_t\vehicles\Tanks_t\I44_m3_stuart\stuartkopie_co.paa","\WW2\assets_t\vehicles\Tanks_t\I44_m3_stuart\track_co.paa","\WW2\assets_t\vehicles\Tanks_t\I44_m3_stuart\alpha_co.paa","\WW2\assets_t\vehicles\Tanks_t\I44_m3_stuart\lod01_co.paa"};
	};
	class LIB_M5A1_Stuart: LIB_M3A3_Stuart
	{
//		hiddenSelections[] = {"camo_0","camo_1","camo_2","camo_3","camo_4","camo_5"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","#(argb,8,8,3)color(1,1,1,1,ca)"};//{"\WW2\assets_t\vehicles\Tanks_t\I44_m3_stuart\stuartm5a1_co.paa","\WW2\assets_t\vehicles\Tanks_t\I44_m3_stuart\stuartkopie_co.paa","\WW2\assets_t\vehicles\Tanks_t\I44_m3_stuart\turretm5_co.paa","\WW2\assets_t\vehicles\Tanks_t\I44_m3_stuart\track_co.paa","\WW2\assets_t\vehicles\Tanks_t\I44_m3_stuart\alpha_co.paa","\WW2\assets_t\vehicles\Tanks_t\I44_m3_stuart\lod01_co.paa"};
	};
	class LIB_M4A3_76: LIB_M4A3_75
	{
//		hiddenSelections[] = {"camo_0","camo_1","camo_2","camo_3","camo_4"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)"};//{"\WW2\assets_t\vehicles\Tanks_t\I44_m4_sherman\m4a3t32_co.paa","\WW2\assets_t\vehicles\Tanks_t\I44_m4_sherman\m4a3turret_co.paa","\WW2\assets_t\vehicles\Tanks_t\I44_m4_sherman\tx2_co.paa","\WW2\assets_t\vehicles\Tanks_t\I44_m4_sherman\track_co.paa","\WW2\assets_t\vehicles\Tanks_t\I44_m4_sherman\alpha2_co.paa"};
	};
	class LIB_M4A3_76_HVSS: LIB_M4A3_76
	{
//		hiddenSelections[] = {"camo_0","camo_1","camo_2","camo_3"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\assets_t\vehicles\Tanks_t\I44_m4_sherman\hvss\hvsssherman_co.paa","\WW2\assets_t\vehicles\Tanks_t\I44_m4_sherman\hvss\hvsstside_co.paa","\WW2\assets_t\vehicles\Tanks_t\I44_m4_sherman\hvss\hvsstracks_co.paa","\WW2\assets_t\vehicles\Tanks_t\I44_m4_sherman\hvss\alpha_co.paa"};
	};
	class LIB_M4A4_FIREFLY: LIB_M4A3_75
	{
//		hiddenSelections[] = {"camo_0","camo_1","camo_2","camo_3"};
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\assets_t\vehicles\Tanks_t\I44_m4_sherman\firefly_co.paa","\WW2\assets_t\vehicles\Tanks_t\I44_m4_sherman\track_co.paa","\WW2\assets_t\vehicles\Tanks_t\I44_m4_sherman\m4a3_co.paa","\WW2\assets_t\vehicles\Tanks_t\I44_m4_sherman\alpha_co.paa"};
	};
	class LIB_US_M3_Halftrack: LIB_Halftrack_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","","","","","","","\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\Img\Usa.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\Img\Dekali.paa"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M3a1_Korpus_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M3a1_Interior_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M3a1_Tracks_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\Browningm2_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M2_Ammobox_co.paa","","","","","","","\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\Img\Usa.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\Img\Dekali.paa"};
	};
	class LIB_DAK_M3_Halftrack: LIB_Halftrack_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","","","","","","","\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\Img\Usa.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\Img\Dekali.paa"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M3a1_Korpus_DAK_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M3a1_Interior_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M3a1_Tracks_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\Browningm2_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M2_Ammobox_co.paa","","","","","","","\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\Img\Usa.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\Img\Dekali.paa"};
	};
	class LIB_US_NAC_M3_Halftrack: LIB_Halftrack_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","","","","","","","\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\Img\Usa.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\Img\Dekali.paa"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M3a1_Korpus_Nac_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M3a1_Interior_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M3a1_Tracks_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\Browningm2_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M2_Ammobox_co.paa","","","","","","","\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\Img\Usa.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\Img\Dekali.paa"};
	};
	class LIB_SOV_M3_Halftrack: LIB_Halftrack_base
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","","","","","","","",""};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\Soviet_M3a1_Korpus_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\Soviet_M3a1_Interior_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M3a1_Tracks_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\Browningm2_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M2_Ammobox_co.paa","","","","","","","",""};
	};
	class LIB_Kfz1_w: LIB_Kfz1
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Cars_t\IF_Kfz1\Kubelwagen_Main_w_co.paa","\WW2\Assets_t\Vehicles\Cars_t\IF_Kfz1\Kubelwagen_Interior_Sernyt_co.paa","WW2\Assets_t\Vehicles\Cars_t\IF_Kfz1\Kubelwagen_turret.paa","WW2\Assets_t\Weapons\MachineGun_Light_t\IF_MG42\Mg42_co.paa","WW2\Assets_t\Weapons\MachineGun_Light_t\IF_MG42\Drum_co.paa"};
	};
	class LIB_Kfz1_hood_w: LIB_Kfz1_Hood
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Cars_t\IF_Kfz1\Kubelwagen_Main_w_co.paa","\WW2\Assets_t\Vehicles\Cars_t\IF_Kfz1\Kubelwagen_Interior_Sernyt_co.paa","WW2\Assets_t\Vehicles\Cars_t\IF_Kfz1\Kubelwagen_turret.paa","WW2\Assets_t\Weapons\MachineGun_Light_t\IF_MG42\Mg42_co.paa","WW2\Assets_t\Weapons\MachineGun_Light_t\IF_MG42\Drum_co.paa"};
	};
	class LIB_Willys_MB_w: LIB_Willys_MB
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Cars_t\IF_Willys_MB\Willys_w_co.paa"};
	};
	class LIB_US_Willys_MB_w: LIB_US_Willys_MB
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Cars_t\IF_Willys_MB\Willys_w_co.paa"};
	};
	class LIB_Scout_m3_w: LIB_Scout_M3
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M3skout_Korpus_w_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M3skout_Interior_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\Browningm2_co.paa"};
	};
	class LIB_Scout_M3_FFV_w: LIB_Scout_M3_FFV
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M3skout_Korpus_w_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M3skout_Interior_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\Browningm2_co.paa"};
	};
	class LIB_US_Scout_m3_w: LIB_US_Scout_M3
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M3skout_Korpus_w_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M3skout_Interior_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\Browningm2_co.paa"};
	};
	class LIB_US_Scout_M3_FFV_w: LIB_US_Scout_M3_FFV
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M3skout_Korpus_w_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M3skout_Interior_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\Browningm2_co.paa"};
	};
	class LIB_OpelBlitz_Open_Y_Camo_w: LIB_OpelBlitz_Open_Y_Camo
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel1_w_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel2_w_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel3_w_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel4_w_co.paa"};
	};
	class LIB_OpelBlitz_Open_G_Camo_w: LIB_OpelBlitz_Open_G_Camo
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel1_w_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel2_w_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel3_w_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel4_w_co.paa"};
	};
	class LIB_OpelBlitz_Tent_Y_Camo_w: LIB_OpelBlitz_Tent_Y_Camo
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel1_w_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel2_w_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel3_w_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel4_w_co.paa"};
	};
	class LIB_OpelBlitz_Fuel_w: LIB_OpelBlitz_Fuel
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel1_w_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel2_w_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel3_w_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel_Fuel_w_co.paa"};
	};
	class LIB_OpelBlitz_Ambulance_w: LIB_OpelBlitz_Ambulance
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel1_w_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel2_w_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel3_w_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel_ambulance_w_co.paa"};
	};
	class LIB_OpelBlitz_Parm_w: LIB_OpelBlitz_Parm
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel1_w_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel2_w_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel3_w_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel_parm_w_co.paa"};
	};
	class LIB_OpelBlitz_Ammo_w: LIB_OpelBlitz_Ammo
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel1_w_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel2_w_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel3_w_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Opelblitz\Opel4_w_co.paa"};
	};
	class LIB_US_GMC_Open_w: LIB_US_GMC_Open
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Img\Usa.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\marker_na_dver.paa","num4","num1","num3","num0","num0","num4"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\Gmc353_w_co.paa","","\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Img\Usa.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\marker_na_dver.paa","num4","num1","num3","num0","num0","num4"};
	};
	class LIB_US_GMC_Tent_w: LIB_US_GMC_Tent
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Img\Usa.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\marker_na_dver.paa","num7","num1","num3","num0","num1","num6"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\Gmc353_w_co.paa","","\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Img\Usa.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\marker_na_dver.paa","num7","num1","num3","num0","num1","num6"};
	};
	class LIB_US_GMC_Ammo_w: LIB_US_GMC_Ammo
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","","\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Img\Usa.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\marker_na_dver.paa","num1","num4","num4","num2","num8","num9"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\Gmc353_w_co.paa","","\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Img\Usa.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\marker_na_dver.paa","num1","num4","num4","num2","num8","num9"};
	};
	class LIB_US_GMC_Ambulance_w: LIB_US_GMC_Ambulance
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(0.75,0.75,0.75,1,ca)","#(argb,8,8,3)color(0.25,0.25,0.25,1,ca)","#(argb,8,8,3)color(0.5,0.5,0.5,1,ca)","#(argb,8,8,3)color(0,1,1,1,ca)","#(argb,8,8,3)color(1,1,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\Gmc353_w_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\gmc353_medic_w_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Img\Usa.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\marker_na_dver.paa","num8","num6","num5","num0","num1","num4"};
	};
	class LIB_US_GMC_Parm_w: LIB_US_GMC_Parm
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(0.75,0.75,0.75,1,ca)","#(argb,8,8,3)color(0.25,0.25,0.25,1,ca)","#(argb,8,8,3)color(0.5,0.5,0.5,1,ca)","#(argb,8,8,3)color(0,1,1,1,ca)","#(argb,8,8,3)color(1,1,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\Gmc353_w_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\gmcst6ruck_w_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Img\Usa.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\marker_na_dver.paa","num8","num6","num5","num0","num1","num4"};
	};
	class LIB_US_GMC_Fuel_w: LIB_US_GMC_Fuel
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(0.75,0.75,0.75,1,ca)","#(argb,8,8,3)color(0.25,0.25,0.25,1,ca)","#(argb,8,8,3)color(0.5,0.5,0.5,1,ca)","#(argb,8,8,3)color(0,1,1,1,ca)","#(argb,8,8,3)color(1,1,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\Gmc353_w_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\gmc_fuel_truck_w_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_US6\Img\Usa.paa","\ww2\assets_t\vehicles\trucks_t\if_gmc353\img\marker_na_dver.paa","num8","num6","num5","num0","num1","num4"};
	};
	class LIB_Zis5v_w: LIB_Zis5v
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)"};//{"WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Zis5_w_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Zis52_w_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Zis53_w_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Kol1_w_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Kol2_w_co.paa"};
	};
	class LIB_Zis5v_med_w: LIB_Zis5v_Med
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","#(argb,8,8,3)color(1,1,1,1,ca)","#(argb,8,8,3)color(0,1,1,1,ca)","#(argb,8,8,3)color(0.5,0.5,0.5,1,ca)"};//{"WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Zis5_w_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Zis52_w_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Zis53_w_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Kol1_w_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Kol2_w_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Parm_w_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Zapravka_w_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Sanitarka_w_co.paa"};
	};
	class LIB_Zis6_parm_w: LIB_Zis6_Parm
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","#(argb,8,8,3)color(1,1,1,1,ca)"};//{"WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Zis5_w_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Zis52_w_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Zis53_w_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Kol1_w_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Kol2_w_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Parm_w_co.paa"};
	};
	class LIB_Zis5v_fuel_w: LIB_Zis5v_Fuel
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","#(argb,8,8,3)color(1,1,1,1,ca)","#(argb,8,8,3)color(0,1,1,1,ca)"};//{"WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Zis5_w_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Zis52_w_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Zis53_w_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Kol1_w_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Kol2_w_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Parm_w_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Zis5v\Zapravka_w_co.paa"};
	};
	class LIB_Sdkfz251_w: LIB_SdKfz251
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","#(argb,8,8,3)color(1,1,1,1,ca)","#(argb,8,8,3)color(0,1,1,1,ca)","#(argb,8,8,3)color(0.5,0.5,0.5,1,ca)","\WW2\Core_t\IF_Decals_t\German\_num\5.paa"};//{"\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Hull_w_co.paa","\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Interrior_co.paa","\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Panel_co.paa","WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Track\0_ca.paa","\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Wheels_w_co.paa","WW2\Assets_t\Weapons\MachineGun_Light_t\IF_MG42\Mg42_co.paa","\WW2\Core_t\IF_Decals_t\German\_num\2.paa","\WW2\Core_t\IF_Decals_t\German\_num\5.paa","\WW2\Core_t\IF_Decals_t\German\_num\5.paa"};
	};
	class LIB_SdKfz251_FFV_w: LIB_SdKfz251_FFV
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","#(argb,8,8,3)color(1,1,1,1,ca)","#(argb,8,8,3)color(0,1,1,1,ca)","#(argb,8,8,3)color(0.5,0.5,0.5,1,ca)","\WW2\Core_t\IF_Decals_t\German\_num\5.paa"};//{"\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Hull_w_co.paa","\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Interrior_co.paa","\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Panel_co.paa","WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Track\0_ca.paa","\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Wheels_co.paa","WW2\Assets_t\Weapons\MachineGun_Light_t\IF_MG42\Mg42_co.paa","\WW2\Core_t\IF_Decals_t\German\_num\2.paa","\WW2\Core_t\IF_Decals_t\German\_num\5.paa","\WW2\Core_t\IF_Decals_t\German\_num\5.paa"};
	};
	class LIB_SdKfz251_captured_w: LIB_SdKfz251_captured
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","#(argb,8,8,3)color(1,1,1,1,ca)","#(argb,8,8,3)color(0,1,1,1,ca)","#(argb,8,8,3)color(0.5,0.5,0.5,1,ca)","\WW2\Core_t\IF_Decals_t\German\_num\5.paa"};//{"\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Hull_w_co.paa","\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Interrior_co.paa","\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Panel_co.paa","WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Track\0_ca.paa","\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Wheels_co.paa","WW2\Assets_t\Weapons\MachineGun_Light_t\IF_MG42\Mg42_co.paa","\WW2\Core_t\IF_Decals_t\German\_num\2.paa","\WW2\Core_t\IF_Decals_t\German\_num\5.paa","\WW2\Core_t\IF_Decals_t\German\_num\5.paa"};
	};
	class LIB_SdKfz251_captured_FFV_w: LIB_SdKfz251_captured_FFV
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","#(argb,8,8,3)color(1,1,1,1,ca)","#(argb,8,8,3)color(0,1,1,1,ca)","#(argb,8,8,3)color(0.5,0.5,0.5,1,ca)","\WW2\Core_t\IF_Decals_t\German\_num\5.paa"};//{"\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Hull_w_co.paa","\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Interrior_co.paa","\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Panel_co.paa","WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Track\0_ca.paa","\WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz251\Wheels_co.paa","WW2\Assets_t\Weapons\MachineGun_Light_t\IF_MG42\Mg42_co.paa","\WW2\Core_t\IF_Decals_t\German\_num\2.paa","\WW2\Core_t\IF_Decals_t\German\_num\5.paa","\WW2\Core_t\IF_Decals_t\German\_num\5.paa"};
	};
	class LIB_SdKfz_7_w: LIB_SdKfz_7
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz7\Sdkfz_7_1_w_co.paa","WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz7\SdKfz_7_2_w_co.paa","WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz7\SdKfz_7_Track_co.paa"};
	};
	class LIB_SdKfz_7_AA_w: LIB_SdKfz_7_AA
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","#(argb,8,8,3)color(1,1,1,1,ca)"};//{"WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz7\Sdkfz_7_1_w_co.paa","WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz7\SdKfz_7_2_w_co.paa","WW2\Assets_t\Vehicles\WheeledAPC_t\IF_SdKfz7\SdKfz_7_Track_co.paa","WW2\Assets_t\Vehicles\StaticWeapons_t\IF_FlakVierling_38\Flakvierling_Armor_w_co.paa","WW2\Assets_t\Vehicles\StaticWeapons_t\IF_FlakVierling_38\Flakvierling_Gun_w_co.paa","WW2\Assets_t\Vehicles\StaticWeapons_t\IF_FlakVierling_38\Flakvierling_Sight_w_co.paa"};
	};
	class LIB_US_M3_Halftrack_w: LIB_US_M3_Halftrack
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","","","","","","","\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\Img\Usa.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\Img\Dekali.paa"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M3a1_Korpus_w_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M3a1_Interior_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M3a1_Tracks_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\Browningm2_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M2_Ammobox_co.paa","","","","","","","\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\Img\Usa.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\Img\Dekali.paa"};
	};
	class LIB_SOV_M3_Halftrack_w: LIB_SOV_M3_Halftrack
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","","","","","","","\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\Img\Usa.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\Img\Dekali.paa"};//{"\WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M3a1_Korpus_w_co.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M3a1_Interior_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M3a1_Tracks_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\Browningm2_co.paa","WW2\Assets_t\Vehicles\Trucks_t\IF_Scout_M3_US\M2_Ammobox_co.paa","","","","","","","\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\Img\Usa.paa","\WW2\Assets_t\Vehicles\Trucks_t\IF_GMC353\Img\Dekali.paa"};
	};
	class LIB_JS2_43_w: LIB_JS2_43
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_JS2_43\Body_w_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_JS2_43\Turret_w_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_JS2_43\Track_co.paa"};
	};
	class LIB_M4A3_75_w: LIB_M4A3_75
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_M4A3_75\Body_w_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_M4A3_75\Turret_w_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_M4A3_75\Wheels_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_M4A3_75\Tracks_co.paa"};
	};
	class LIB_M4A3_75_Tubes_w: LIB_M4A3_75_Tubes
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_M4A3_75\Body_Soviet_w_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_M4A3_75\Turret_Soviet_w_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_M4A3_75\Wheels_Soviet_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_M4A3_75\Tracks_co.paa"};
	};
	class LIB_M4A2_SOV_w: LIB_M4A2_SOV
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_M4A3_75\Body_Soviet_w_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_M4A3_75\Turret_Soviet_w_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_M4A3_75\Wheels_Soviet_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_M4A3_75\Tracks_co.paa"};
	};
	class LIB_PzKpfwIV_H_w: LIB_PzKpfwIV_H
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwIV_H\Body_w_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwIV_H\Turret_w_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwIV_H\Wheels_w_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwIV_H\Tracks_co.paa"};
	};
	class LIB_FlakPanzerIV_Wirbelwind_w: LIB_FlakPanzerIV_Wirbelwind
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)","#(argb,8,8,3)color(1,1,1,1,ca)","#(argb,8,8,3)color(0,1,1,1,ca)","#(argb,8,8,3)color(0.5,0.5,0.5,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwIV_H\Body_w_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwIV_H\Turret_w_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwIV_H\Wheels_w_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwIV_H\Tracks_co.paa","\WW2\Assets_t\Vehicles\StaticWeapons_t\IF_FlakVierling_38\Flakvierling_Armor_w_co.paa","\WW2\Assets_t\Vehicles\StaticWeapons_t\IF_FlakVierling_38\Flakvierling_Gun_w_co.paa","\WW2\Assets_t\Vehicles\StaticWeapons_t\IF_FlakVierling_38\Flakvierling_Sight_w_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\WW2_Flakpanzer\Flakpanzer_Turret_w_co.paa"};
	};
	class LIB_PzKpfwV_w: LIB_PzKpfwV
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwV\Body_w_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwV\Turret_w_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwV\Tracks_co.paa"};
	};
	class LIB_PzKpfwVI_B_w: LIB_PzKpfwVI_B
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_B\Body_w_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_B\Shanc_w_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_B\Turret_w_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_B\Wheels_w_co.paa","WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_B\Track_co.paa"};
	};
	class LIB_PzKpfwVI_B_camo_w: LIB_PzKpfwVI_B_camo
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_B\Body_W2_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_B\Shanc_w_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_B\Turret_W2_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_B\Wheels_w_co.paa","WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_B\Track_co.paa"};
	};
	class LIB_PzKpfwVI_E_w: LIB_PzKpfwVI_E
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)","#(argb,8,8,3)color(1,1,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_E\Body_w_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_E\Tools_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_E\Turret_w_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_E\Wheels_w_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_PzKpfwVI_E\Tracks_co.paa"};
	};
	class LIB_StuG_III_G_w: LIB_StuG_III_G
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_StuG_III_G\Main_w_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_StuG_III_G\Misc_w_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_StuG_III_G\Wheels_w_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_StuG_III_G\Tracks_co.paa"};
	};
	class LIB_StuG_III_G_WS_w: LIB_StuG_III_G_WS
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_StuG_III_G\Main_w_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_StuG_III_G\Misc_w_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_StuG_III_G\Wheels_w_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_StuG_III_G\Tracks_co.paa"};
	};
	class LIB_SU85_w: LIB_SU85
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_SU85\SU83_1_w_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_SU85\SU83_2_w_co.paa","WW2\Assets_t\Vehicles\Tanks_t\IF_SU85\Traks_co.paa"};
	};
	class LIB_T34_76_w: LIB_T34_76
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_T34_76\Body_w_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_T34_76\Turret_w_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_T34_76\Track_co.paa"};
	};
	class LIB_T34_85_w: LIB_T34_85
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"\WW2\Assets_t\Vehicles\Tanks_t\IF_T34_85\Body_w_co.paa","\WW2\Assets_t\Vehicles\Tanks_t\IF_T34_85\Turret_w_co.paa","WW2\Assets_t\Vehicles\Tanks_t\IF_T34_85\Track_co.paa"};
	};
	class LIB_FW190F8_w: LIB_FW190F8
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Vehicles\Planes_t\IF_FW190F8\FW190F8_Mn_2_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_FW190F8\FW190F8_Sd_co.paa"};
	};
	class LIB_FW190F8_2_W: LIB_FW190F8_w
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Vehicles\Planes_t\IF_FW190F8\Fw190f8_Mn_Win1_Ns_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_FW190F8\Fw190f8_Sd_1_co.paa"};
	};
	class LIB_FW190F8_3_W: LIB_FW190F8_w
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Vehicles\Planes_t\IF_FW190F8\Fw190f8_Mn_Win2_Ns_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_FW190F8\Fw190f8_Sd_1_co.paa"};
	};
	class LIB_Ju87_w: LIB_Ju87
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)","#(argb,8,8,3)color(1,0,1,1,ca)"};//{"WW2\Assets_t\Vehicles\Planes_t\IF_Ju87\Ju87_Korpus_Win1_Ns_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_Ju87\Ju87_Wingw_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_Ju87\Ju87_Cabine_1_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_Ju87\Ju87_Cabine_2_co.paa"};
	};
	class LIB_P39_w: LIB_P39
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Vehicles\Planes_t\IF_P39\P39_Ra_Fusellage_Winter_1_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_P39\P39_Ra_Wings_Winter_1_co.paa"};
	};
	class LIB_Pe2_w: LIB_Pe2
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Vehicles\Planes_t\IF_Pe2\Pe2_Ra_Winter_1_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_Pe2\Pe_2cabinmaintexture.paa"};
	};
	class LIB_Pe2_2_w: LIB_Pe2_w
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Vehicles\Planes_t\IF_Pe2\Pe2_Ra_Winter_2_co.paa","WW2\Assets_t\Vehicles\Planes_t\IF_Pe2\Pe_2cabinmaintexture.paa"};
	};
	class LIB_Zis3_w: LIB_Zis3
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"WW2\Assets_t\Vehicles\StaticWeapons_t\IF_Zis3\Niz_w_co.paa","WW2\Assets_t\Vehicles\StaticWeapons_t\IF_Zis3\Verh_w_co.paa","WW2\Assets_t\Vehicles\StaticWeapons_t\IF_Zis3\Pricel_co.paa"};
	};
	class LIB_Pak40_w: LIB_Pak40
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)"};//{"\WW2\Assets_t\Vehicles\StaticWeapons_t\IF_Pak40\Pak_40_w_co.paa"};
	};
	class LIB_FlaK_38_w: LIB_FlaK_38
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Vehicles\StaticWeapons_t\IF_Flak_38\Flak_38_Basement_w_co.paa","WW2\Assets_t\Vehicles\StaticWeapons_t\IF_Flak_38\Flak_38_Gun_w_co.paa"};
	};
	class LIB_FlaK_30_w: LIB_FlaK_30
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)"};//{"WW2\Assets_t\Vehicles\StaticWeapons_t\IF_Flak_38\Flak_38_Basement_w_co.paa","WW2\Assets_t\Vehicles\StaticWeapons_t\IF_Flak_38\Flak_38_Gun_w_co.paa"};
	};
	class LIB_Flakvierling_38_w: LIB_Flakvierling_38
	{
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(1,0,0,1,ca)","#(argb,8,8,3)color(0,1,0,1,ca)","#(argb,8,8,3)color(0,0,1,1,ca)"};//{"WW2\Assets_t\Vehicles\StaticWeapons_t\IF_FlakVierling_38\Flakvierling_Armor_w_co.paa","WW2\Assets_t\Vehicles\StaticWeapons_t\IF_FlakVierling_38\Flakvierling_Gun_w_co.paa","WW2\Assets_t\Vehicles\StaticWeapons_t\IF_FlakVierling_38\Flakvierling_Sight_w_co.paa"};
	};
};

//		hiddenSelections[] = {"eqip_helmet","decal_helmet_back","decal_helmet_front"}; - LIB_US_Helmet
//		hiddenSelections[] = {"eqip_pilot_glasses_up","eqip_pilot_helmet"}; - LIB_US_Pilot_Helmet
//		hiddenSelections[] = {"eqip_radio"}; - B_LIB_GER_Radio
//		hiddenSelections[] = {"eqip_tank_glasses_up","eqip_tank_helmet"}; - LIB_US_Tank_Helmet
//		hiddenSelections[] = {"equipment"}; - B_LIB_GER_A_frame
//		hiddenSelections[] = {"karoserie"};
