﻿class CfgPatches
{
	class Optional_HideFromEditor_Glasses_c
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Data_F_Loadorder","A3_Data_F_Curator_Loadorder","A3_Data_F_Kart_Loadorder","A3_Data_F_Bootcamp_Loadorder","A3_Data_F_Heli_Loadorder","A3_Data_F_Mark_Loadorder","A3_Data_F_Exp_A_Loadorder","A3_Data_F_Exp_B_Loadorder","A3_Data_F_Exp_Loadorder","A3_Data_F_Jets_Loadorder","A3_Data_F_Argo_Loadorder","A3_Data_F_Patrol_Loadorder","A3_Data_F_Orange_Loadorder","A3_Data_F_Tacops_Loadorder","A3_Data_F_Tank_Loadorder"};
		version = "2019-03-28";
	};
};
class CfgGlasses
{
	class None;
	class G_EyeProtectors_base_F;
	class G_Respirator_base_F;
	class G_WirelessEarpiece_base_F;

	class G_Spectacles: None
	{
		scope = 1;
	};
	class G_Spectacles_Tinted: None
	{
		scope = 1;
	};
	class G_Combat: None
	{
		scope = 1;
	};
	class G_Lowprofile: None
	{
		scope = 1;
	};
	class G_Shades_Black: None
	{
		scope = 1;
	};
	class G_Shades_Green: None
	{
		scope = 1;
	};
	class G_Shades_Red: None
	{
		scope = 1;
	};
	class G_Squares: None
	{
		scope = 1;
	};
	class G_Squares_Tinted: None
	{
		scope = 1;
	};
	class G_Sport_BlackWhite: None
	{
		scope = 1;
	};
	class G_Sport_Blackyellow: None
	{
		scope = 1;
	};
	class G_Sport_Greenblack: None
	{
		scope = 1;
	};
	class G_Sport_Checkered: None
	{
		scope = 1;
	};
	class G_Sport_Red: None
	{
		scope = 1;
	};
	class G_Tactical_Black: None
	{
		scope = 1;
	};
	class G_Aviator: None
	{
		scope = 1;
	};
	class G_Lady_Blue: None
	{
		scope = 1;
	};
	class G_Diving
	{
		scope = 1;
	};
	class G_Goggles_VR: None
	{
		scope = 1;
		scopeCurator = 1;
		scopeArsenal = 1;
	};
	class G_Balaclava_blk: None
	{
		scope = 1;
		scopeCurator = 1;
		scopeArsenal = 1;
	};
	class G_Shades_Blue: None
	{
		scope = 1;
	};
	class G_Sport_Blackred: None
	{
		scope = 1;
	};
	class G_Tactical_Clear: None
	{
		scope = 1;
	};
	class G_Balaclava_TI_blk_F: None
	{
		scope = 1;
		scopeCurator = 1;
		scopeArsenal = 1;
	};
	class G_Combat_Goggles_tna_F: None
	{
		scope = 1;
		scopeCurator = 1;
		scopeArsenal = 1;
	};
	class G_Respirator_white_F: G_Respirator_base_F
	{
		scope = 1;
		scopeCurator = 1;
		scopeArsenal = 1;
	};
	class G_Respirator_yellow_F: G_Respirator_base_F
	{
		scope = 1;
		scopeCurator = 1;
		scopeArsenal = 1;
	};
	class G_Respirator_blue_F: G_Respirator_base_F
	{
		scope = 1;
		scopeCurator = 1;
		scopeArsenal = 1;
	};
	class G_EyeProtectors_F: G_EyeProtectors_base_F
	{
		scope = 1;
		scopeCurator = 1;
		scopeArsenal = 1;
	};
	class G_EyeProtectors_Earpiece_F: G_EyeProtectors_base_F
	{
		scope = 1;
		scopeCurator = 1;
		scopeArsenal = 1;
	};
	class G_WirelessEarpiece_F: G_WirelessEarpiece_base_F
	{
		scope = 1;
		scopeCurator = 1;
		scopeArsenal = 1;
	};
};
