class CfgPatches
{
	class OFPTankDirectionLook_Visuals_C_PvPscene
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Data_F_Loadorder","A3_Data_F_Curator_Loadorder","A3_Data_F_Kart_Loadorder","A3_Data_F_Bootcamp_Loadorder","A3_Data_F_Heli_Loadorder","A3_Data_F_Mark_Loadorder","A3_Data_F_Exp_A_Loadorder","A3_Data_F_Exp_B_Loadorder","A3_Data_F_Exp_Loadorder","A3_Data_F_Jets_Loadorder","A3_Data_F_Argo_Loadorder","A3_Data_F_Patrol_Loadorder","A3_Data_F_Orange_Loadorder","A3_Data_F_Tacops_Loadorder","A3_Data_F_Tank_Loadorder"};
		version = "2019-03-28";
	};
};
class CfgInGameUI
{
	class TankDirection
	{
		color[] = {1,1,1,1};
		colorHalfDammage[] = {0.5,0,0,1};
		colorFullDammage[] = {1,0,0,1};
		left = "(40/100)	* SafeZoneW + SafeZoneX";//"SafeZoneX";
		top = "(20/100)	* SafeZoneH + SafeZoneY";//"0.02 + SafeZoneY";
		width = "(20/100)	* SafeZoneW";//"0.235294125 * 0.70";
		height = "(20/100)	* SafeZoneH";//"0.313725525 * 0.70";
		imageTower = "\OFPTankDirectionLook_Visuals_C_PvPscene\tankdir_tower_ca.paa";
		imageTurret = "\OFPTankDirectionLook_Visuals_C_PvPscene\tankdir_turret_ca.paa";
		imageGun = "\OFPTankDirectionLook_Visuals_C_PvPscene\tankdir_gun_ca.paa";
		imageObsTurret = "\OFPTankDirectionLook_Visuals_C_PvPscene\tankdir_turret_commander_ca.paa";
		imageEngine = "\OFPTankDirectionLook_Visuals_C_PvPscene\tankdir_engine_ca.paa";
		imageHull = "\OFPTankDirectionLook_Visuals_C_PvPscene\tankdir_hull_ca.paa";
		imageLTrack = "\OFPTankDirectionLook_Visuals_C_PvPscene\tankdir_track_left_ca.paa";
		imageRTrack = "\OFPTankDirectionLook_Visuals_C_PvPscene\tankdir_track_right_ca.paa";
	};
	//TODO: remove new basic tank display
};
