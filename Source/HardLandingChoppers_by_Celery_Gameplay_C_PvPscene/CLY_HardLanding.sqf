//HardLanding script by Celery, SQF conversion by T_D
//Used to do some very rough landings without wrecking your helicopter in the process
//
//http://www.flashpoint1985.com/cgi-bin/ikonboard311/ikonboard.cgi?act=ST;f=71;t=76648

sleep 1;

private["_chopper","_hurtDescent","_maxDescent","_maxSpeed"];

_chopper = vehicle player;

if ((_chopper isKindOf "Helicopter") && (alive player)) then
{
	_hurtDescent = -4.5;
	_maxDescent = -12;
	_maxSpeed = 80;
	
	while {((driver _chopper) == player) && (alive player)} do
	{
		if (((speed _chopper) < _maxSpeed) && (((getPos _chopper) select 2) < 2) && (((velocity _chopper) select 2) < _hurtDescent) && (((velocity _chopper) select 2) > _maxDescent)) then
		{
			while {((driver _chopper) == player) && (alive player)} do
			{
				private ["_descent","_alt"];
				scopeName "CLY_hardLanding";
				_descent = (velocity _chopper select 2);
				_alt = (getPos _chopper select 2);
				
				if ((_alt > 0.01) && (_alt < 0.8) && (_descent < _hurtDescent) && (_descent > _maxDescent)) then
				{
					_chopper setVelocity [(velocity _chopper) select 0,(velocity _chopper) select 1,_hurtDescent + 2];
					sleep 0.5;
					breakOut "CLY_hardLanding";
				};
				
				if ((_alt > 2) || (_descent < _maxDescent)) then {breakOut "CLY_hardLanding"};

				sleep 0.01;
			};
		};

		sleep 0.05;
	};
};