﻿class CfgPatches
{
	class Optional_HideFromEditor_Infantry_c
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Data_F_Loadorder","A3_Data_F_Curator_Loadorder","A3_Data_F_Kart_Loadorder","A3_Data_F_Bootcamp_Loadorder","A3_Data_F_Heli_Loadorder","A3_Data_F_Mark_Loadorder","A3_Data_F_Exp_A_Loadorder","A3_Data_F_Exp_B_Loadorder","A3_Data_F_Exp_Loadorder","A3_Data_F_Jets_Loadorder","A3_Data_F_Argo_Loadorder","A3_Data_F_Patrol_Loadorder","A3_Data_F_Orange_Loadorder","A3_Data_F_Tacops_Loadorder","A3_Data_F_Tank_Loadorder"};
		version = "2019-03-28";
	};
};
class CfgVehicles
{
	class Alsatian_Base_F;
	class Animal_Base_F;
	class B_CTRG_Soldier_base_F;
	class B_GEN_Soldier_base_F;
	class B_ghillie_base_F;
	class B_Soldier_02_f;
	class B_Soldier_03_f;
	class B_Soldier_04_f;
	class B_Soldier_05_f;
	class B_Soldier_base_F;
	class B_Soldier_diver_base_F;
	class B_Soldier_recon_base;
	class B_Soldier_sniper_base_F;
	class B_Soldier_support_base_F;
	class B_T_Soldier_F;
	class C_Driver_1_F;
	class C_Man_casual_1_F;
	class Civilian_F;
	class Fin_Base_F;
	class Fish_Base_F;
	class Fowl_Base_F;
	class Goat_Base_F;
	class I_C_Soldier_base_F;
	class I_G_Soldier_base_F;
	class I_ghillie_base_F;
	class I_Soldier_02_F;
	class I_Soldier_03_F;
	class I_Soldier_04_F;
	class I_Soldier_base_F;
	class I_Soldier_diver_base_F;
	class I_Soldier_sniper_base_F;
	class I_Soldier_support_base_F;
	class O_A_soldier_base_F;
	class O_ghillie_base_F;
	class O_Soldier_02_F;
	class O_Soldier_base_F;
	class O_Soldier_diver_base_F;
	class O_Soldier_recon_base;
	class O_Soldier_sniper_base_F;
	class O_Soldier_support_base_F;
	class O_Soldier_Urban_base;
	class O_T_Soldier_F;
	class O_V_Soldier_base_F;
	class UAV_AI_base_F;

	class Salema_F: Fish_Base_F
	{
//		scope = 1;
	};
	class Ornate_random_F: Fish_Base_F
	{
//		scope = 1;
	};
	class Mackerel_F: Fish_Base_F
	{
//		scope = 1;
	};
	class Tuna_F: Fish_Base_F
	{
//		scope = 1;
	};
	class Mullet_F: Fish_Base_F
	{
//		scope = 1;
	};
	class CatShark_F: Fish_Base_F
	{
//		scope = 1;
	};
	class Rabbit_F: Animal_Base_F
	{
//#		scope = 1;//2
	};
	class Snake_random_F: Animal_Base_F
	{
//		scope = 1;
	};
	class Turtle_F: Animal_Base_F
	{
//		scope = 1;
	};
	class Hen_random_F: Fowl_Base_F
	{
//		scope = 1;
	};
	class Cock_random_F: Fowl_Base_F
	{
//		scope = 1;
	};
	class Cock_white_F: Cock_random_F
	{
//		scope = 1;
	};
	class Fin_sand_F: Fin_Base_F
	{
//		scope = 1;
	};
	class Fin_blackwhite_F: Fin_Base_F
	{
//		scope = 1;
	};
	class Fin_ocherwhite_F: Fin_Base_F
	{
//		scope = 1;
	};
	class Fin_tricolour_F: Fin_Base_F
	{
//		scope = 1;
	};
	class Fin_random_F: Fin_Base_F
	{
//		scope = 1;
	};
	class Alsatian_Sand_F: Alsatian_Base_F
	{
//		scope = 1;
	};
	class Alsatian_Black_F: Alsatian_Base_F
	{
//		scope = 1;
	};
	class Alsatian_Sandblack_F: Alsatian_Base_F
	{
//		scope = 1;
	};
	class Alsatian_Random_F: Alsatian_Base_F
	{
//		scope = 1;
	};
	class Goat_random_F: Goat_Base_F
	{
//		scope = 1;
	};
	class Sheep_random_F: Animal_Base_F
	{
//		scope = 1;
	};
	class VirtualMan_F: Civilian_F
	{
//		scope = 1;
	};
	class HeadlessClient_F: VirtualMan_F
	{
		scope = 1;//2
	};
	class B_Soldier_F: B_Soldier_base_F
	{
		scope = 1;//2
	};
	class B_RangeMaster_F: B_Soldier_base_F
	{
		scope = 1;//2
	};
	class B_Soldier_lite_F: B_Soldier_03_f
	{
		scope = 1;//2
	};
	class B_Soldier_GL_F: B_Soldier_base_F
	{
		scope = 1;//2
	};
	class B_soldier_AR_F: B_Soldier_02_f
	{
		scope = 1;//2
	};
	class B_Soldier_SL_F: B_Soldier_03_f
	{
		scope = 1;//2
	};
	class B_Soldier_TL_F: B_Soldier_03_f
	{
		scope = 1;//2
	};
	class B_soldier_M_F: B_Soldier_base_F
	{
		scope = 1;//2
	};
	class B_soldier_LAT_F: B_Soldier_base_F
	{
		scope = 1;//2
	};
	class B_medic_F: B_Soldier_02_f
	{
		scope = 1;//2
	};
	class B_soldier_repair_F: B_Soldier_base_F
	{
		scope = 1;//2
	};
	class B_soldier_exp_F: B_Soldier_base_F
	{
		scope = 1;//2
	};
	class B_Helipilot_F: B_Soldier_04_f
	{
		scope = 1;//2
	};
	class B_UAV_AI: B_Helipilot_F
	{
//		scope = 1;
	};
	class B_Soldier_A_F: B_Soldier_base_F
	{
		scope = 1;//2
	};
	class B_soldier_AT_F: B_Soldier_base_F
	{
		scope = 1;//2
	};
	class B_soldier_AA_F: B_Soldier_base_F
	{
		scope = 1;//2
	};
	class B_engineer_F: B_Soldier_03_f
	{
		scope = 1;//2
	};
	class B_crew_F: B_Soldier_03_f
	{
		scope = 1;//2
	};
	class B_officer_F: B_Soldier_base_F
	{
		scope = 1;//2
	};
	class B_Pilot_F: B_Soldier_05_f
	{
		scope = 1;//2
	};
	class B_soldier_PG_F: B_Soldier_base_F
	{
		scope = 1;//2
	};
	class B_soldier_UAV_F: B_Soldier_base_F
	{
		scope = 1;//2
	};
	class B_Soldier_universal_F: B_Soldier_F
	{
//		scope = 1;
	};
	class B_diver_F: B_Soldier_diver_base_F
	{
		scope = 1;//2
	};
	class B_diver_TL_F: B_Soldier_diver_base_F
	{
		scope = 1;//2
	};
	class B_diver_exp_F: B_Soldier_diver_base_F
	{
		scope = 1;//2
	};
	class B_recon_F: B_Soldier_recon_base
	{
		scope = 1;//2
	};
	class B_recon_LAT_F: B_Soldier_recon_base
	{
		scope = 1;//2
	};
	class B_recon_exp_F: B_Soldier_recon_base
	{
		scope = 1;//2
	};
	class B_recon_medic_F: B_Soldier_recon_base
	{
		scope = 1;//2
	};
	class B_recon_TL_F: B_Soldier_recon_base
	{
		scope = 1;//2
	};
	class B_recon_M_F: B_Soldier_recon_base
	{
		scope = 1;//2
	};
	class B_recon_JTAC_F: B_Soldier_recon_base
	{
		scope = 1;//2
	};
	class B_spotter_F: B_Soldier_sniper_base_F
	{
		scope = 1;//2
	};
	class B_sniper_F: B_Soldier_sniper_base_F
	{
		scope = 1;//2
	};
	class B_Story_SF_Captain_F: B_Soldier_02_f
	{
		scope = 1;//2
	};
	class B_Story_Protagonist_F: B_Soldier_02_f
	{
		scope = 1;//2
	};
	class B_Story_Engineer_F: B_Soldier_base_F
	{
//		scope = 1;
	};
	class B_Story_Colonel_F: B_Soldier_base_F
	{
//		scope = 1;
	};
	class B_Story_Pilot_F: B_Soldier_base_F
	{
		scope = 1;//2
	};
	class B_Story_Tank_Commander_F: B_Soldier_base_F
	{
//		scope = 1;
	};
	class b_soldier_survival_F: B_Soldier_base_F
	{
//		scope = 1;
	};
	class B_CTRG_soldier_GL_LAT_F: B_Soldier_base_F
	{
		scope = 1;//2
	};
	class B_CTRG_soldier_engineer_exp_F: B_Soldier_02_f
	{
		scope = 1;//2
	};
	class B_CTRG_soldier_M_medic_F: B_Soldier_03_f
	{
		scope = 1;//2
	};
	class B_CTRG_soldier_AR_A_F: B_Soldier_03_f
	{
		scope = 1;//2
	};
	class B_soldier_AAR_F: B_Soldier_support_base_F
	{
		scope = 1;//2
	};
	class B_soldier_AAT_F: B_Soldier_support_base_F
	{
		scope = 1;//2
	};
	class B_soldier_AAA_F: B_soldier_AAT_F
	{
		scope = 1;//2
	};
	class B_support_MG_F: B_Soldier_support_base_F
	{
		scope = 1;//2
	};
	class B_support_GMG_F: B_Soldier_support_base_F
	{
		scope = 1;//2
	};
	class B_support_Mort_F: B_Soldier_support_base_F
	{
		scope = 1;//2
	};
	class B_support_AMG_F: B_Soldier_support_base_F
	{
		scope = 1;//2
	};
	class B_support_AMort_F: B_Soldier_support_base_F
	{
		scope = 1;//2
	};
	class I_G_Soldier_F: I_G_Soldier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_G_Soldier_lite_F: I_G_Soldier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_G_Soldier_SL_F: I_G_Soldier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_G_Soldier_TL_F: I_G_Soldier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_G_Soldier_AR_F: I_G_Soldier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_G_medic_F: I_G_Soldier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_G_engineer_F: I_G_Soldier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_G_Soldier_exp_F: I_G_Soldier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_G_Soldier_GL_F: I_G_Soldier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_G_Soldier_M_F: I_G_Soldier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_G_Soldier_LAT_F: I_G_Soldier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_G_Soldier_A_F: I_G_Soldier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_G_officer_F: I_G_Soldier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_G_Soldier_F: I_G_Soldier_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_G_Soldier_F: I_G_Soldier_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_G_Soldier_lite_F: I_G_Soldier_lite_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_G_Soldier_lite_F: I_G_Soldier_lite_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_G_Soldier_SL_F: I_G_Soldier_SL_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_G_Soldier_SL_F: I_G_Soldier_SL_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_G_Soldier_TL_F: I_G_Soldier_TL_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_G_Soldier_TL_F: I_G_Soldier_TL_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_G_Soldier_AR_F: I_G_Soldier_AR_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_G_Soldier_AR_F: I_G_Soldier_AR_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_G_medic_F: I_G_medic_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_G_medic_F: I_G_medic_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_G_engineer_F: I_G_engineer_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_G_engineer_F: I_G_engineer_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_G_Soldier_exp_F: I_G_Soldier_exp_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_G_Soldier_exp_F: I_G_Soldier_exp_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_G_Soldier_GL_F: I_G_Soldier_GL_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_G_Soldier_GL_F: I_G_Soldier_GL_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_G_Soldier_M_F: I_G_Soldier_M_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_G_Soldier_M_F: I_G_Soldier_M_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_G_Soldier_LAT_F: I_G_Soldier_LAT_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_G_Soldier_LAT_F: I_G_Soldier_LAT_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_G_Soldier_A_F: I_G_Soldier_A_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_G_Soldier_A_F: I_G_Soldier_A_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_G_officer_F: I_G_officer_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_G_officer_F: I_G_officer_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_G_Soldier_universal_F: B_G_Soldier_F
	{
//		scope = 1;
	};
	class O_G_Soldier_universal_F: O_G_Soldier_F
	{
//		scope = 1;
	};
	class I_G_Soldier_universal_F: I_G_Soldier_F
	{
//		scope = 1;
	};
	class I_G_Story_Protagonist_F: B_G_Soldier_F
	{
		scope = 1;//2
	};
	class I_G_Story_SF_Captain_F: B_G_Soldier_F
	{
		scope = 1;//2
	};
	class I_G_resistanceLeader_F: I_G_Story_Protagonist_F
	{
		scope = 1;//2
	};
	class I_G_resistanceCommander_F: I_G_Story_Protagonist_F
	{
//		scope = 1;
	};
	class I_soldier_F: I_Soldier_base_F
	{
		scope = 1;//2
	};
	class I_Soldier_lite_F: I_Soldier_02_F
	{
		scope = 1;//2
	};
	class I_Soldier_A_F: I_Soldier_02_F
	{
		scope = 1;//2
	};
	class I_Soldier_GL_F: I_Soldier_base_F
	{
		scope = 1;//2
	};
	class I_Soldier_AR_F: I_Soldier_02_F
	{
		scope = 1;//2
	};
	class I_Soldier_SL_F: I_Soldier_02_F
	{
		scope = 1;//2
	};
	class I_Soldier_TL_F: I_Soldier_base_F
	{
		scope = 1;//2
	};
	class I_Soldier_M_F: I_Soldier_base_F
	{
		scope = 1;//2
	};
	class I_Soldier_LAT_F: I_Soldier_base_F
	{
		scope = 1;//2
	};
	class I_Soldier_AT_F: I_Soldier_base_F
	{
		scope = 1;//2
	};
	class I_Soldier_AA_F: I_Soldier_02_F
	{
		scope = 1;//2
	};
	class I_medic_F: I_Soldier_02_F
	{
		scope = 1;//2
	};
	class I_Soldier_repair_F: I_Soldier_02_F
	{
		scope = 1;//2
	};
	class I_Soldier_exp_F: I_Soldier_02_F
	{
		scope = 1;//2
	};
	class I_engineer_F: I_Soldier_base_F
	{
		scope = 1;//2
	};
	class I_crew_F: I_Soldier_base_F
	{
		scope = 1;//2
	};
	class I_helipilot_F: I_Soldier_03_F
	{
		scope = 1;//2
	};
	class I_pilot_F: I_Soldier_04_F
	{
		scope = 1;//2
	};
	class I_officer_F: I_Soldier_base_F
	{
		scope = 1;//2
	};
	class I_Story_Colonel_F: I_officer_F
	{
		scope = 1;//2
	};
	class I_soldier_UAV_F: I_Soldier_base_F
	{
		scope = 1;//2
	};
	class I_Soldier_universal_F: I_soldier_F
	{
//		scope = 1;
	};
	class I_diver_F: I_Soldier_diver_base_F
	{
		scope = 1;//2
	};
	class I_diver_exp_F: I_Soldier_diver_base_F
	{
		scope = 1;//2
	};
	class I_diver_TL_F: I_Soldier_diver_base_F
	{
		scope = 1;//2
	};
	class I_Spotter_F: I_Soldier_sniper_base_F
	{
		scope = 1;//2
	};
	class I_Sniper_F: I_Soldier_sniper_base_F
	{
		scope = 1;//2
	};
	class I_Soldier_AAR_F: I_Soldier_support_base_F
	{
		scope = 1;//2
	};
	class I_Soldier_AAT_F: I_Soldier_support_base_F
	{
		scope = 1;//2
	};
	class I_Soldier_AAA_F: I_Soldier_AAT_F
	{
		scope = 1;//2
	};
	class I_support_MG_F: I_Soldier_support_base_F
	{
		scope = 1;//2
	};
	class I_support_GMG_F: I_Soldier_support_base_F
	{
		scope = 1;//2
	};
	class I_support_Mort_F: I_Soldier_support_base_F
	{
		scope = 1;//2
	};
	class I_support_AMG_F: I_Soldier_support_base_F
	{
		scope = 1;//2
	};
	class I_support_AMort_F: I_Soldier_support_base_F
	{
		scope = 1;//2
	};
	class O_Soldier_F: O_Soldier_base_F
	{
		scope = 1;//2
	};
	class O_officer_F: O_Soldier_base_F
	{
		scope = 1;//2
	};
	class O_Soldier_lite_F: O_Soldier_base_F
	{
		scope = 1;//2
	};
	class O_Soldier_GL_F: O_Soldier_base_F
	{
		scope = 1;//2
	};
	class O_Soldier_AR_F: O_Soldier_base_F
	{
		scope = 1;//2
	};
	class O_Soldier_SL_F: O_Soldier_base_F
	{
		scope = 1;//2
	};
	class O_Soldier_TL_F: O_Soldier_base_F
	{
		scope = 1;//2
	};
	class O_soldier_M_F: O_Soldier_base_F
	{
		scope = 1;//2
	};
	class O_Soldier_LAT_F: O_Soldier_base_F
	{
		scope = 1;//2
	};
	class O_medic_F: O_Soldier_base_F
	{
		scope = 1;//2
	};
	class O_soldier_repair_F: O_Soldier_base_F
	{
		scope = 1;//2
	};
	class O_soldier_exp_F: O_Soldier_base_F
	{
		scope = 1;//2
	};
	class O_helipilot_F: O_Soldier_02_F
	{
		scope = 1;//2
	};
	class O_UAV_AI: O_helipilot_F
	{
//		scope = 1;
	};
	class O_Soldier_A_F: O_Soldier_base_F
	{
		scope = 1;//2
	};
	class O_Soldier_AT_F: O_Soldier_base_F
	{
		scope = 1;//2
	};
	class O_Soldier_AA_F: O_Soldier_base_F
	{
		scope = 1;//2
	};
	class O_engineer_F: O_Soldier_base_F
	{
		scope = 1;//2
	};
	class O_crew_F: O_Soldier_base_F
	{
		scope = 1;//2
	};
	class O_soldier_PG_F: O_Soldier_base_F
	{
		scope = 1;//2
	};
	class O_Story_Colonel_F: O_Soldier_base_F
	{
		scope = 1;//2
	};
	class O_Story_CEO_F: O_Soldier_base_F
	{
		scope = 1;//2
	};
	class O_soldier_UAV_F: O_Soldier_base_F
	{
		scope = 1;//2
	};
	class O_Soldier_universal_F: O_Soldier_F
	{
//		scope = 1;
	};
	class O_diver_F: O_Soldier_diver_base_F
	{
		scope = 1;//2
	};
	class O_diver_TL_F: O_Soldier_diver_base_F
	{
		scope = 1;//2
	};
	class O_diver_exp_F: O_Soldier_diver_base_F
	{
		scope = 1;//2
	};
	class O_spotter_F: O_Soldier_sniper_base_F
	{
		scope = 1;//2
	};
	class O_sniper_F: O_Soldier_sniper_base_F
	{
		scope = 1;//2
	};
	class O_recon_F: O_Soldier_recon_base
	{
		scope = 1;//2
	};
	class O_recon_M_F: O_Soldier_recon_base
	{
		scope = 1;//2
	};
	class O_recon_LAT_F: O_Soldier_recon_base
	{
		scope = 1;//2
	};
	class O_recon_medic_F: O_Soldier_recon_base
	{
		scope = 1;//2
	};
	class O_recon_exp_F: O_Soldier_recon_base
	{
		scope = 1;//2
	};
	class O_recon_JTAC_F: O_Soldier_recon_base
	{
		scope = 1;//2
	};
	class O_recon_TL_F: O_Soldier_recon_base
	{
		scope = 1;//2
	};
	class O_Soldier_AAR_F: O_Soldier_support_base_F
	{
		scope = 1;//2
	};
	class O_Soldier_AAT_F: O_Soldier_support_base_F
	{
		scope = 1;//2
	};
	class O_Soldier_AAA_F: O_Soldier_AAT_F
	{
		scope = 1;//2
	};
	class O_support_MG_F: O_Soldier_support_base_F
	{
		scope = 1;//2
	};
	class O_support_GMG_F: O_Soldier_support_base_F
	{
		scope = 1;//2
	};
	class O_support_Mort_F: O_Soldier_support_base_F
	{
		scope = 1;//2
	};
	class O_support_AMG_F: O_Soldier_support_base_F
	{
		scope = 1;//2
	};
	class O_support_AMort_F: O_Soldier_support_base_F
	{
		scope = 1;//2
	};
	class O_soldierU_F: O_Soldier_Urban_base
	{
		scope = 1;//2
	};
	class O_soldierU_AR_F: O_Soldier_Urban_base
	{
		scope = 1;//2
	};
	class O_soldierU_AAR_F: O_Soldier_Urban_base
	{
		scope = 1;//2
	};
	class O_soldierU_LAT_F: O_Soldier_Urban_base
	{
		scope = 1;//2
	};
	class O_soldierU_AT_F: O_Soldier_Urban_base
	{
		scope = 1;//2
	};
	class O_soldierU_AAT_F: O_Soldier_Urban_base
	{
		scope = 1;//2
	};
	class O_soldierU_AA_F: O_Soldier_Urban_base
	{
		scope = 1;//2
	};
	class O_soldierU_AAA_F: O_Soldier_Urban_base
	{
		scope = 1;//2
	};
	class O_soldierU_TL_F: O_Soldier_Urban_base
	{
		scope = 1;//2
	};
	class O_SoldierU_SL_F: O_Soldier_Urban_base
	{
		scope = 1;//2
	};
	class O_soldierU_medic_F: O_Soldier_Urban_base
	{
		scope = 1;//2
	};
	class O_soldierU_repair_F: O_Soldier_Urban_base
	{
		scope = 1;//2
	};
	class O_soldierU_exp_F: O_Soldier_Urban_base
	{
		scope = 1;//2
	};
	class O_engineer_U_F: O_Soldier_Urban_base
	{
		scope = 1;//2
	};
	class O_soldierU_M_F: O_Soldier_Urban_base
	{
		scope = 1;//2
	};
	class O_soldierU_A_F: O_Soldier_Urban_base
	{
		scope = 1;//2
	};
	class O_SoldierU_GL_F: O_Soldier_Urban_base
	{
		scope = 1;//2
	};
	class C_Driver_1_random_base_F: C_Driver_1_F
	{
//		scope = 1;
	};
	class C_Driver_1_black_F: C_Driver_1_random_base_F
	{
//		scope = 1;
	};
	class C_Driver_1_blue_F: C_Driver_1_random_base_F
	{
//		scope = 1;
	};
	class C_Driver_1_green_F: C_Driver_1_random_base_F
	{
//		scope = 1;
	};
	class C_Driver_1_red_F: C_Driver_1_random_base_F
	{
//		scope = 1;
	};
	class C_Driver_1_white_F: C_Driver_1_random_base_F
	{
//		scope = 1;
	};
	class C_Driver_1_yellow_F: C_Driver_1_random_base_F
	{
//		scope = 1;
	};
	class C_Driver_1_orange_F: C_Driver_1_random_base_F
	{
//		scope = 1;
	};
	class C_Marshal_F: B_RangeMaster_F
	{
		scope = 1;//2
	};
	class B_Soldier_VR_F: B_Soldier_base_F
	{
		scope = 1;//2
	};
	class O_Soldier_VR_F: O_Soldier_base_F
	{
		scope = 1;//2
	};
	class I_Soldier_VR_F: I_Soldier_base_F
	{
		scope = 1;//2
	};
	class B_Protagonist_VR_F: B_Soldier_base_F
	{
		scope = 1;//2
	};
	class O_Protagonist_VR_F: O_Soldier_base_F
	{
		scope = 1;//2
	};
	class I_Protagonist_VR_F: I_Soldier_base_F
	{
		scope = 1;//2
	};
	class B_ghillie_lsh_F: B_ghillie_base_F
	{
		scope = 1;//2
	};
	class B_ghillie_sard_F: B_ghillie_base_F
	{
		scope = 1;//2
	};
	class B_ghillie_ard_F: B_ghillie_base_F
	{
		scope = 1;//2
	};
	class O_ghillie_lsh_F: O_ghillie_base_F
	{
		scope = 1;//2
	};
	class O_ghillie_sard_F: O_ghillie_base_F
	{
		scope = 1;//2
	};
	class O_ghillie_ard_F: O_ghillie_base_F
	{
		scope = 1;//2
	};
	class I_ghillie_lsh_F: I_ghillie_base_F
	{
		scope = 1;//2
	};
	class I_ghillie_sard_F: I_ghillie_base_F
	{
		scope = 1;//2
	};
	class I_ghillie_ard_F: I_ghillie_base_F
	{
		scope = 1;//2
	};
	class B_Sharpshooter_F: B_Soldier_base_F
	{
		scope = 1;//2
	};
	class B_Recon_Sharpshooter_F: B_Soldier_recon_base
	{
		scope = 1;//2
	};
	class B_CTRG_Sharphooter_F: B_Soldier_base_F
	{
		scope = 1;//2
	};
	class B_HeavyGunner_F: B_Soldier_base_F
	{
		scope = 1;//2
	};
	class O_Sharpshooter_F: O_Soldier_base_F
	{
		scope = 1;//2
	};
	class O_Urban_Sharpshooter_F: O_Soldier_Urban_base
	{
		scope = 1;//2
	};
	class O_Pathfinder_F: O_Soldier_recon_base
	{
		scope = 1;//2
	};
	class O_HeavyGunner_F: O_Soldier_base_F
	{
		scope = 1;//2
	};
	class O_Urban_HeavyGunner_F: O_Soldier_Urban_base
	{
		scope = 1;//2
	};
	class I_G_Sharpshooter_F: I_G_Soldier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_G_Sharpshooter_F: I_G_Sharpshooter_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_G_Sharpshooter_F: I_G_Sharpshooter_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_Captain_Pettka_F: B_Soldier_02_f
	{
		scope = 1;//2
	};
	class B_Captain_Jay_F: B_Soldier_02_f
	{
		scope = 1;//2
	};
	class I_Captain_Hladas_F: I_officer_F
	{
		scope = 1;//2
	};

	class Underwear_F: B_Soldier_F
	{
//		scope = 1;
	};
	class VirtualCurator_F: VirtualMan_F
	{
//		scope = 1;//2
	};
	class VirtualSpectator_F: VirtualMan_F
	{
//		scope = 1;//2
	};
	class I_C_Soldier_Para_1_F: I_C_Soldier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_C_Soldier_Para_2_F: I_C_Soldier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_C_Soldier_Para_3_F: I_C_Soldier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_C_Soldier_Para_4_F: I_C_Soldier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_C_Soldier_Para_5_F: I_C_Soldier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_C_Soldier_Para_6_F: I_C_Soldier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_C_Soldier_Para_7_F: I_C_Soldier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_C_Soldier_Para_8_F: I_C_Soldier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_C_Soldier_Bandit_1_F: I_C_Soldier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_C_Soldier_Bandit_2_F: I_C_Soldier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_C_Soldier_Bandit_3_F: I_C_Soldier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_C_Soldier_Bandit_4_F: I_C_Soldier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_C_Soldier_Bandit_5_F: I_C_Soldier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_C_Soldier_Bandit_6_F: I_C_Soldier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_C_Soldier_Bandit_7_F: I_C_Soldier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_C_Soldier_Bandit_8_F: I_C_Soldier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_C_Soldier_Camo_F: I_C_Soldier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_C_Pilot_F: I_C_Soldier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_C_Helipilot_F: I_C_Soldier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_C_Soldier_universal_F: I_C_Soldier_Para_1_F
	{
//		scope = 1;
	};
	class B_T_ghillie_tna_F: B_ghillie_base_F
	{
		scope = 1;//2
	};
	class B_T_Soldier_universal_F: B_T_Soldier_F
	{
//		scope = 1;
	};
	class O_T_ghillie_tna_F: O_ghillie_base_F
	{
		scope = 1;//2
	};
	class O_V_Soldier_Viper_F: O_Soldier_base_F
	{
//		scope = 1;
	};
	class O_T_Soldier_universal_F: O_T_Soldier_F
	{
//		scope = 1;
	};
	class B_CTRG_Soldier_F: B_CTRG_Soldier_base_F
	{
//		scope = 1;
	};
	class B_CTRG_Soldier_2_F: B_CTRG_Soldier_base_F
	{
//		scope = 1;
	};
	class B_CTRG_Soldier_3_F: B_CTRG_Soldier_base_F
	{
//		scope = 1;
	};
	class B_CTRG_Soldier_TL_tna_F: B_CTRG_Soldier_F
	{
		scope = 1;//2
	};
	class B_CTRG_Soldier_Exp_tna_F: B_CTRG_Soldier_F
	{
		scope = 1;//2
	};
	class B_CTRG_Soldier_Medic_tna_F: B_CTRG_Soldier_F
	{
		scope = 1;//2
	};
	class B_CTRG_Soldier_M_tna_F: B_CTRG_Soldier_F
	{
		scope = 1;//2
	};
	class B_CTRG_Soldier_tna_F: B_CTRG_Soldier_F
	{
		scope = 1;//2
	};
	class B_CTRG_Soldier_LAT_tna_F: B_CTRG_Soldier_F
	{
		scope = 1;//2
	};
	class B_CTRG_Soldier_AR_tna_F: B_CTRG_Soldier_F
	{
		scope = 1;//2
	};
	class B_CTRG_Soldier_JTAC_tna_F: B_CTRG_Soldier_F
	{
		scope = 1;//2
	};
	class B_CTRG_Miller_F: B_CTRG_Soldier_3_F
	{
		scope = 1;//2
	};
	class B_CTRG_Soldier_urb_1_F: B_CTRG_Soldier_F
	{
//		scope = 1;
	};
	class B_CTRG_Soldier_urb_2_F: B_CTRG_Soldier_2_F
	{
//		scope = 1;
	};
	class B_CTRG_Soldier_urb_3_F: B_CTRG_Soldier_3_F
	{
//		scope = 1;
	};
	class B_CTRG_Soldier_universal_F: B_CTRG_Soldier_tna_F
	{
//		scope = 1;
	};
	class O_V_Soldier_hex_F: O_V_Soldier_base_F
	{
		scope = 1;//2
	};
	class O_V_Soldier_TL_hex_F: O_V_Soldier_base_F
	{
		scope = 1;//2
	};
	class O_V_Soldier_Exp_hex_F: O_V_Soldier_base_F
	{
		scope = 1;//2
	};
	class O_V_Soldier_Medic_hex_F: O_V_Soldier_base_F
	{
		scope = 1;//2
	};
	class O_V_Soldier_M_hex_F: O_V_Soldier_base_F
	{
		scope = 1;//2
	};
	class O_V_Soldier_LAT_hex_F: O_V_Soldier_base_F
	{
		scope = 1;//2
	};
	class O_V_Soldier_JTAC_hex_F: O_V_Soldier_base_F
	{
		scope = 1;//2
	};
	class B_GEN_Soldier_F: B_GEN_Soldier_base_F
	{
		scope = 1;//2
	};
	class B_GEN_Commander_F: B_GEN_Soldier_base_F
	{
		scope = 1;//2
	};
	class B_Captain_Dwarden_F: B_GEN_Commander_F
	{
		scope = 1;//2
	};
	class B_G_Captain_Ivan_F: I_C_Soldier_Para_2_F
	{
		scope = 1;//2
	};
	class B_GEN_Soldier_universal_F: B_GEN_Soldier_F
	{
//		scope = 1;
	};
	class C_Man_French_universal_F: C_Man_casual_1_F
	{
//		scope = 1;
	};

	class B_UAV_AI_F: UAV_AI_base_F
	{
//		scope = 1;
	};
	class O_UAV_AI_F: UAV_AI_base_F
	{
//		scope = 1;
	};
	class I_UAV_AI_F: UAV_AI_base_F
	{
//		scope = 1;
	};

	class B_G_Story_Guerilla_01_F: B_G_Soldier_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_Story_Officer_01_F: I_officer_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_A_soldier_A_F: O_A_soldier_base_F
	{
//		scope = 1;
	};
	class O_A_soldier_AR_F: O_A_soldier_base_F
	{
//		scope = 1;
	};
	class O_A_medic_F: O_A_soldier_base_F
	{
//		scope = 1;
	};
	class O_A_soldier_GL_F: O_A_soldier_base_F
	{
//		scope = 1;
	};
	class O_A_soldier_M_F: O_A_soldier_base_F
	{
//		scope = 1;
	};
	class O_A_officer_F: O_A_soldier_base_F
	{
//		scope = 1;
	};
	class O_A_soldier_F: O_A_soldier_base_F
	{
		scope = 1;//2
	};
	class O_A_soldier_LAT_F: O_A_soldier_base_F
	{
//		scope = 1;
	};
	class O_A_soldier_SL_F: O_A_soldier_base_F
	{
//		scope = 1;
	};
	class O_A_soldier_TL_F: O_A_soldier_base_F
	{
		scope = 1;//2
	};
	class O_A_soldier_AA_F: O_A_soldier_base_F
	{
//		scope = 1;
	};
	class O_A_soldier_AT_F: O_A_soldier_base_F
	{
//		scope = 1;
	};

	class B_soldier_LAT2_F: B_Soldier_base_F
	{
		scope = 1;//2;
	};
	class B_CTRG_Soldier_LAT2_tna_F: B_CTRG_Soldier_F
	{
		scope = 1;//2;
	};
	class I_G_Soldier_LAT2_F: I_G_Soldier_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class B_G_Soldier_LAT2_F: I_G_Soldier_LAT2_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class O_G_Soldier_LAT2_F: I_G_Soldier_LAT2_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class I_Soldier_LAT2_F: I_Soldier_base_F
	{
		scope = 1;//2;
	};
	class O_Soldier_HAT_F: O_Soldier_base_F
	{
		scope = 1;//2;
	};
	class O_Soldier_AHAT_F: O_Soldier_support_base_F
	{
		scope = 1;//2;
	};
	class I_Story_Crew_F: I_crew_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
};
