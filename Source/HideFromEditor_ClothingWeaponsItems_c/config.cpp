﻿class CfgPatches
{
	class Optional_HideFromEditor_ClothingWeaponsItems_c
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Data_F_Loadorder","A3_Data_F_Curator_Loadorder","A3_Data_F_Kart_Loadorder","A3_Data_F_Bootcamp_Loadorder","A3_Data_F_Heli_Loadorder","A3_Data_F_Mark_Loadorder","A3_Data_F_Exp_A_Loadorder","A3_Data_F_Exp_B_Loadorder","A3_Data_F_Exp_Loadorder","A3_Data_F_Jets_Loadorder","A3_Data_F_Argo_Loadorder","A3_Data_F_Patrol_Loadorder","A3_Data_F_Orange_Loadorder","A3_Data_F_Tacops_Loadorder","A3_Data_F_Tank_Loadorder"};
		version = "2019-03-28";
	};
};
class CfgUnitInsignia
{
	class 111thID
	{
		scope = 1;
	};
	class TFAegis
	{
		scope = 1;
	};
	class GryffinRegiment
	{
		scope = 1;
	};
	class Curator
	{
		scope = 1;
	};
	class BI
	{
		scope = 1;
	};
	class MANW
	{
		scope = 1;
	};
	class CTRG15
	{
		scope = 1;
	};
	class CTRG14
	{
		scope = 1;
	};
	class CTRG
	{
		scope = 1;
	};
	class Jets_ID
	{
		scope = 1;
	};
	class CombatPatrol
	{
		scope = 1;
	};
	class IDAP
	{
		scope = 1;
	};
	class AAF_3rdRegiment
	{
		scope = 1;
	};
	class CSAT_ScimitarRegiment
	{
		scope = 1;
	};
	class AAF_1stRegiment
	{
		scope = 1;
	};
};
class CfgMagazines
{
	class CA_Magazine;
	class CA_LauncherMagazine;
	class APERSMine_Range_Mag;

	class 30Rnd_556x45_Stanag: CA_Magazine
	{
		scope = 1;//2
	};
	class 20Rnd_556x45_UW_mag: 30Rnd_556x45_Stanag
	{
		scope = 1;//2
	};
	class 30Rnd_65x39_caseless_mag: CA_Magazine
	{
		scope = 1;//2
	};
	class 20Rnd_762x51_Mag: CA_Magazine
	{
		scope = 1;//2
	};
	class 7Rnd_408_Mag: CA_Magazine
	{
		scope = 1;//2
	};
	class 5Rnd_127x108_Mag: CA_Magazine
	{
		scope = 1;//2
	};
	class 100Rnd_65x39_caseless_mag: CA_Magazine
	{
		scope = 1;//2
	};
	class 30Rnd_9x21_Mag: CA_Magazine
	{
		scope = 1;//2
	};
	class RPG32_F: CA_LauncherMagazine
	{
		scope = 1;//2
	};
	class NLAW_F: CA_LauncherMagazine
	{
		scope = 1;//2
	};
	class 1Rnd_HE_Grenade_shell: CA_Magazine
	{
		scope = 1;//2
	};
	class HandGrenade: CA_Magazine
	{
		scope = 1;//2
	};
	class MiniGrenade: CA_Magazine
	{
		scope = 1;//2
	};
	class HandGrenade_Stone: HandGrenade
	{
//		scope = 1;
	};
	class UGL_FlareWhite_F: CA_Magazine
	{
		scope = 1;//2
	};
	class FlareWhite_F: CA_Magazine
	{
		scope = 1;//2
	};
	class Laserbatteries: CA_Magazine
	{
		scope = 1;//2
	};
	class 150Rnd_762x51_Box: CA_Magazine
	{
		scope = 1;//2
	};
	class Titan_AA: CA_LauncherMagazine
	{
		scope = 1;//2
	};
	class 11Rnd_45ACP_Mag: CA_Magazine
	{
		scope = 1;//2
	};
	class B_IR_Grenade: CA_Magazine
	{
		scope = 1;//2
	};
	class ATMine_Range_Mag: CA_Magazine
	{
		scope = 1;//2
	};
	class ClaymoreDirectionalMine_Remote_Mag: CA_Magazine
	{
		scope = 1;//2
	};
	class SatchelCharge_Remote_Mag: CA_Magazine
	{
		scope = 1;//2
	};
	class DemoCharge_Remote_Mag: SatchelCharge_Remote_Mag
	{
		scope = 1;//2
	};
	class 10Rnd_338_Mag: CA_Magazine
	{
		scope = 1;//2
	};
	class 130Rnd_338_Mag: CA_Magazine
	{
		scope = 1;//2
	};
	class 10Rnd_127x54_Mag: CA_Magazine
	{
		scope = 1;//2
	};
	class 150Rnd_93x64_Mag: CA_Magazine
	{
		scope = 1;//2
	};
	class 50Rnd_570x28_SMG_03: CA_Magazine
	{
		scope = 1;//2
	};
	class 30Rnd_580x42_Mag_F: CA_Magazine
	{
		scope = 1;//2
	};
	class 20Rnd_650x39_Cased_Mag_F: CA_Magazine
	{
		scope = 1;//2
	};
	class 10Rnd_50BW_Mag_F: CA_Magazine
	{
		scope = 1;//2
	};
	class 150Rnd_556x45_Drum_Mag_F: CA_Magazine
	{
		scope = 1;//2
	};
	class 30Rnd_762x39_Mag_F: CA_Magazine
	{
		scope = 1;//2
	};
	class 30Rnd_545x39_Mag_F: CA_Magazine
	{
		scope = 1;//2
	};
	class 200Rnd_556x45_Box_F: CA_Magazine
	{
		scope = 1;//2
	};
	class RPG7_F: RPG32_F
	{
		scope = 1;//2
	};
	class APERSMineDispenser_Mag: SatchelCharge_Remote_Mag
	{
		scope = 1;//2
	};
	class TrainingMine_Mag: APERSMine_Range_Mag
	{
		scope = 1;//2
	};
	class Vorona_HEAT: CA_LauncherMagazine
	{
		scope = 1;//2
	};
	class Vorona_HE: Vorona_HEAT
	{
		scope = 1;//2
	};
	class MRAWS_HEAT_F: CA_LauncherMagazine
	{
		scope = 1;//2
	};
};
class CfgWeapons
{
	class arifle_AK12_base_F;
	class arifle_AK12_GL_base_F;
	class arifle_AKM_base_F;
	class arifle_AKS_base_F;
	class arifle_ARX_base_F;
	class arifle_CTAR_base_F;
	class arifle_CTAR_GL_base_F;
	class arifle_CTARS_base_F;
	class arifle_Katiba_Base_F;
	class arifle_MX_Base_F;
	class arifle_SPAR_01_base_F;
	class arifle_SPAR_01_GL_base_F;
	class arifle_SPAR_02_base_F;
	class arifle_SPAR_03_base_F;
	class Default;
	class DetectorCore;
	class DMR_01_base_F;
	class DMR_02_base_F;
	class DMR_03_base_F;
	class DMR_04_base_F;
	class DMR_05_base_F;
	class DMR_06_base_F;
	class DMR_07_base_F;
	class EBR_base_F;
	class GM6_base_F;
	class H_Cap_blk;
	class H_Cap_grn;
	class H_Cap_oli;
	class H_Cap_tan;
	class H_Construction_basic_base_F;
	class H_Construction_earprot_base_F;
	class H_Construction_headset_base_F;
	class H_EarProtectors_base_F;
	class H_Hat_Safari_base_F;
	class H_HeadBandage_base_F;
	class H_HeadSet_base_F;
	class H_HelmetIA;
	class H_PASGT_basic_base_F;
	class H_PASGT_neckprot_base_F;
	class H_Tank_base_F;
	class H_WirelessEarpiece_base_F;
	class HelmetBase;
	class hgun_P07_khk_F;
	class ItemCore;
	class launch_MRAWS_base_F;
	class launch_Titan_base;
	class launch_Titan_short_base;
	class launch_Vorona_base_F;
	class Launcher_Base_F;
	class LMG_03_base_F;
	class LRR_base_F;
	class mk20_base_F;
	class MMG_01_base_F;
	class MMG_02_base_F;
	class muzzle_snds_58_blk_F;
	class optic_AMS_base;
	class optic_KHS_base;
	class pdw2000_base_F;
	class Pistol_Base_F;
	class Rifle_Long_Base_F;
	class SDAR_base_F;
	class SMG_01_Base;
	class SMG_02_base_F;
	class SMG_03_TR_BASE;
	class SMG_03C_BASE;
	class SMG_05_base_F;
	class Tavor_base_F;
	class UavTerminal_base;
	class Uniform_Base;
	class V_EOD_base_F;
	class V_HarnessO_gry;
	class V_LegStrapBag_base_F;
	class V_Plain_base_F;
	class V_PlateCarrierIA2_dgtl;
	class V_Pocketed_base_F;
	class V_Safety_base_F;
	class Vest_Camo_Base;
	class Vest_NoCamo_Base;

	class ItemWatch: ItemCore
	{
//		scope = 1;//2
	};
	class ItemCompass: ItemCore
	{
//		scope = 1;//2
	};
	class ItemGPS: ItemCore
	{
		scope = 1;//2
	};
	class ItemRadio: ItemCore
	{
//		scope = 1;//2
	};
	class ItemMap: ItemCore
	{
//		scope = 1;//2
	};
	class MineDetector: DetectorCore
	{
//		scope = 1;//2
	};
	class Binocular: Default
	{
		scope = 1;//2
	};
	class Rangefinder: Binocular
	{
		scope = 1;//2
	};
	class NVGoggles: Binocular
	{
		scope = 1;//
	};
	class Laserdesignator: Binocular
	{
		scope = 1;//
	};
	class Integrated_NVG_F: NVGoggles
	{
//		scope = 1;
	};
	class Integrated_NVG_TI_0_F: NVGoggles
	{
//		scope = 1;
	};
	class Integrated_NVG_TI_1_F: NVGoggles
	{
//		scope = 1;
	};
	class FirstAidKit: ItemCore
	{
//		scope = 1;//2
	};
	class Medikit: ItemCore
	{
//		scope = 1;//2
	};
	class ToolKit: ItemCore
	{
//		scope = 1;//2
	};

	class launch_NLAW_F: Launcher_Base_F
	{
		scope = 1;//2
	};
	class launch_RPG32_F: Launcher_Base_F
	{
		scope = 1;//2
	};
	class launch_B_Titan_F: launch_Titan_base
	{
		scope = 1;//2
	};
	class launch_I_Titan_F: launch_Titan_base
	{
		scope = 1;//2
	};
	class launch_O_Titan_F: launch_Titan_base
	{
		scope = 1;//2
	};
	class launch_Titan_F: launch_Titan_base
	{
//		scope = 1;
	};
	class launch_B_Titan_short_F: launch_Titan_short_base
	{
		scope = 1;//2
	};
	class launch_I_Titan_short_F: launch_Titan_short_base
	{
		scope = 1;//2
	};
	class launch_O_Titan_short_F: launch_Titan_short_base
	{
		scope = 1;//2
	};
	class launch_Titan_short_F: launch_Titan_short_base
	{
//		scope = 1;
	};
	class srifle_DMR_01_F: DMR_01_base_F
	{
		scope = 1;//2
	};
	class srifle_EBR_F: EBR_base_F
	{
		scope = 1;//2
	};
	class srifle_GM6_F: GM6_base_F
	{
		scope = 1;//2
	};
	class srifle_GM6_camo_F: srifle_GM6_F
	{
		scope = 1;//2
	};
	class srifle_LRR_F: LRR_base_F
	{
		scope = 1;//2
	};
	class srifle_LRR_camo_F: srifle_LRR_F
	{
		scope = 1;//2
	};
	class LMG_Mk200_F: Rifle_Long_Base_F
	{
		scope = 1;//2
	};
	class LMG_Zafir_F: Rifle_Long_Base_F
	{
		scope = 1;//2
	};
	class hgun_ACPC2_F: Pistol_Base_F
	{
		scope = 1;//2
	};
	class hgun_P07_F: Pistol_Base_F
	{
		scope = 1;//2
	};
	class hgun_Pistol_heavy_01_F: Pistol_Base_F
	{
		scope = 1;//2
	};
	class hgun_Pistol_heavy_02_F: Pistol_Base_F
	{
		scope = 1;//2
	};
	class hgun_Rook40_F: Pistol_Base_F
	{
		scope = 1;//2
	};
	class arifle_Katiba_F: arifle_Katiba_Base_F
	{
		scope = 1;//2
	};
	class arifle_Katiba_C_F: arifle_Katiba_Base_F
	{
		scope = 1;//2
	};
	class arifle_Katiba_GL_F: arifle_Katiba_Base_F
	{
		scope = 1;//2
	};
	class arifle_Mk20_F: mk20_base_F
	{
		scope = 1;//2
	};
	class arifle_Mk20C_F: mk20_base_F
	{
		scope = 1;//2
	};
	class arifle_Mk20_GL_F: mk20_base_F
	{
		scope = 1;//2
	};
	class arifle_MXC_F: arifle_MX_Base_F
	{
		scope = 1;//2
	};
	class arifle_MX_F: arifle_MX_Base_F
	{
		scope = 1;//2
	};
	class arifle_MX_GL_F: arifle_MX_Base_F
	{
		scope = 1;//2
	};
	class arifle_MX_SW_F: arifle_MX_Base_F
	{
		scope = 1;//2
	};
	class arifle_MXM_F: arifle_MX_Base_F
	{
		scope = 1;//2
	};
	class arifle_SDAR_F: SDAR_base_F
	{
		scope = 1;//2
	};
	class arifle_TRG21_F: Tavor_base_F
	{
		scope = 1;//2
	};
	class arifle_TRG20_F: Tavor_base_F
	{
		scope = 1;//2
	};
	class arifle_TRG21_GL_F: arifle_TRG21_F
	{
		scope = 1;//2
	};
	class hgun_PDW2000_F: pdw2000_base_F
	{
		scope = 1;//2
	};
	class SMG_01_F: SMG_01_Base
	{
		scope = 1;//2
	};
	class SMG_02_F: SMG_02_base_F
	{
		scope = 1;//2
	};
	class U_BasicBody: Uniform_Base
	{
//		scope = 1;
	};
	class U_B_CombatUniform_mcam: Uniform_Base
	{
		scope = 1;//2
	};
	class U_B_CombatUniform_mcam_tshirt: Uniform_Base
	{
		scope = 1;//2
	};
	class U_B_CombatUniform_mcam_vest: Uniform_Base
	{
		scope = 1;//2
	};
	class U_B_GhillieSuit: Uniform_Base
	{
		scope = 1;//2
	};
	class U_B_HeliPilotCoveralls: Uniform_Base
	{
		scope = 1;//2
	};
	class U_B_Wetsuit: Uniform_Base
	{
		scope = 1;//2
	};
	class U_O_CombatUniform_ocamo: Uniform_Base
	{
		scope = 1;//2
	};
	class U_O_GhillieSuit: Uniform_Base
	{
		scope = 1;//2
	};
	class U_O_PilotCoveralls: Uniform_Base
	{
		scope = 1;//2
	};
	class U_O_Wetsuit: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_Poloshirt_blue: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_Poloshirt_burgundy: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_Poloshirt_stripped: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_Poloshirt_tricolour: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_Poloshirt_salmon: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_Poloshirt_redwhite: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_Commoner1_1: Uniform_Base
	{
//		scope = 1;
	};
	class U_C_Commoner1_2: Uniform_Base
	{
//		scope = 1;
	};
	class U_C_Commoner1_3: Uniform_Base
	{
//		scope = 1;
	};
	class U_Rangemaster: Uniform_Base
	{
		scope = 1;//2
	};
	class U_NikosBody: Uniform_Base
	{
//		scope = 1;
	};
	class U_OrestesBody: Uniform_Base
	{
		scope = 1;//2
	};
	class U_B_CombatUniform_mcam_worn: Uniform_Base
	{
		scope = 1;//2
	};
	class U_B_CombatUniform_wdl: Uniform_Base
	{
//		scope = 1;
	};
	class U_B_CombatUniform_wdl_tshirt: Uniform_Base
	{
//		scope = 1;
	};
	class U_B_CombatUniform_wdl_vest: Uniform_Base
	{
//		scope = 1;
	};
	class U_B_CombatUniform_sgg: Uniform_Base
	{
//		scope = 1;
	};
	class U_B_CombatUniform_sgg_tshirt: Uniform_Base
	{
//		scope = 1;
	};
	class U_B_CombatUniform_sgg_vest: Uniform_Base
	{
//		scope = 1;
	};
	class U_B_SpecopsUniform_sgg: Uniform_Base
	{
//		scope = 1;
	};
	class U_B_PilotCoveralls: Uniform_Base
	{
		scope = 1;//2
	};
	class U_O_CombatUniform_oucamo: Uniform_Base
	{
		scope = 1;//2
	};
	class U_O_SpecopsUniform_ocamo: Uniform_Base
	{
		scope = 1;//2
	};
	class U_O_SpecopsUniform_blk: Uniform_Base
	{
//		scope = 1;
	};
	class U_O_OfficerUniform_ocamo: Uniform_Base
	{
		scope = 1;//2
	};
	class U_I_CombatUniform: Uniform_Base
	{
		scope = 1;//2
	};
	class U_I_CombatUniform_tshirt: Uniform_Base
	{
//		scope = 1;
	};
	class U_I_CombatUniform_shortsleeve: Uniform_Base
	{
		scope = 1;//2
	};
	class U_I_pilotCoveralls: Uniform_Base
	{
		scope = 1;//2
	};
	class U_I_HeliPilotCoveralls: Uniform_Base
	{
		scope = 1;//2
	};
	class U_I_GhillieSuit: Uniform_Base
	{
		scope = 1;//2
	};
	class U_I_OfficerUniform: Uniform_Base
	{
		scope = 1;//2
	};
	class U_I_Wetsuit: Uniform_Base
	{
		scope = 1;//2
	};
	class U_Competitor: Uniform_Base
	{
		scope = 1;//2
	};
	class U_MillerBody: Uniform_Base
	{
//		scope = 1;
	};
	class U_KerryBody: Uniform_Base
	{
//		scope = 1;
	};
	class U_AttisBody: Uniform_Base
	{
//		scope = 1;
	};
	class U_AntigonaBody: Uniform_Base
	{
//		scope = 1;
	};
	class U_IG_Menelaos: Uniform_Base
	{
//		scope = 1;
	};
	class U_C_Novak: Uniform_Base
	{
//		scope = 1;
	};
	class U_OI_Scientist: Uniform_Base
	{
//		scope = 1;
	};
	class U_IG_Guerilla1_1: Uniform_Base
	{
//		scope = 1;
	};
	class U_IG_Guerilla2_1: Uniform_Base
	{
//		scope = 1;
	};
	class U_IG_Guerilla2_2: Uniform_Base
	{
//		scope = 1;
	};
	class U_IG_Guerilla2_3: Uniform_Base
	{
//		scope = 1;
	};
	class U_IG_Guerilla3_1: Uniform_Base
	{
//		scope = 1;
	};
	class U_IG_Guerilla3_2: Uniform_Base
	{
//		scope = 1;
	};
	class U_IG_leader: Uniform_Base
	{
//		scope = 1;
	};
	class U_BG_Guerilla1_1: Uniform_Base
	{
		scope = 1;//2
	};
	class U_BG_Guerilla2_1: Uniform_Base
	{
		scope = 1;//2
	};
	class U_BG_Guerilla2_2: Uniform_Base
	{
		scope = 1;//2
	};
	class U_BG_Guerilla2_3: Uniform_Base
	{
		scope = 1;//2
	};
	class U_BG_Guerilla3_1: Uniform_Base
	{
		scope = 1;//2
	};
	class U_BG_Guerilla3_2: Uniform_Base
	{
//		scope = 1;
	};
	class U_BG_leader: Uniform_Base
	{
		scope = 1;//2
	};
	class U_OG_Guerilla1_1: Uniform_Base
	{
//		scope = 1;
	};
	class U_OG_Guerilla2_1: Uniform_Base
	{
//		scope = 1;
	};
	class U_OG_Guerilla2_2: Uniform_Base
	{
//		scope = 1;
	};
	class U_OG_Guerilla2_3: Uniform_Base
	{
//		scope = 1;
	};
	class U_OG_Guerilla3_1: Uniform_Base
	{
//		scope = 1;
	};
	class U_OG_Guerilla3_2: Uniform_Base
	{
//		scope = 1;
	};
	class U_OG_leader: Uniform_Base
	{
//		scope = 1;
	};
	class U_C_Poor_1: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_Poor_2: Uniform_Base
	{
//		scope = 1;
	};
	class U_C_Scavenger_1: Uniform_Base
	{
//		scope = 1;
	};
	class U_C_Scavenger_2: Uniform_Base
	{
//		scope = 1;
	};
	class U_C_Farmer: Uniform_Base
	{
//		scope = 1;
	};
	class U_C_Fisherman: Uniform_Base
	{
//		scope = 1;
	};
	class U_C_WorkerOveralls: Uniform_Base
	{
//		scope = 1;
	};
	class U_C_FishermanOveralls: Uniform_Base
	{
//		scope = 1;
	};
	class U_C_WorkerCoveralls: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_HunterBody_grn: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_HunterBody_brn: Uniform_Base
	{
//		scope = 1;
	};
	class U_C_Commoner2_1: Uniform_Base
	{
//		scope = 1;
	};
	class U_C_Commoner2_2: Uniform_Base
	{
//		scope = 1;
	};
	class U_C_Commoner2_3: Uniform_Base
	{
//		scope = 1;
	};
	class U_C_PriestBody: Uniform_Base
	{
//		scope = 1;
	};
	class U_C_Poor_shorts_1: Uniform_Base
	{
//		scope = 1;
	};
	class U_C_Poor_shorts_2: Uniform_Base
	{
//		scope = 1;
	};
	class U_C_Commoner_shorts: Uniform_Base
	{
//		scope = 1;
	};
	class U_C_ShirtSurfer_shorts: Uniform_Base
	{
//		scope = 1;
	};
	class U_C_TeeSurfer_shorts_1: Uniform_Base
	{
//		scope = 1;
	};
	class U_C_TeeSurfer_shorts_2: Uniform_Base
	{
//		scope = 1;
	};
	class U_B_CTRG_1: Uniform_Base
	{
		scope = 1;//2
	};
	class U_B_CTRG_2: Uniform_Base
	{
		scope = 1;//2
	};
	class U_B_CTRG_3: Uniform_Base
	{
		scope = 1;//2
	};
	class U_B_survival_uniform: Uniform_Base
	{
		scope = 1;//2
	};
	class U_I_G_Story_Protagonist_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_I_G_resistanceLeader_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_Journalist: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_Scientist: Uniform_Base
	{
		scope = 1;//2
	};
	class U_NikosAgedBody: Uniform_Base
	{
//		scope = 1;
	};
	class U_B_Protagonist_VR: Uniform_Base
	{
		scope = 1;//2
	};
	class U_O_Protagonist_VR: Uniform_Base
	{
		scope = 1;//2
	};
	class U_I_Protagonist_VR: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_Protagonist_VR: Uniform_Base
	{
		scope = 1;//2
	};
	class U_IG_Guerrilla_6_1: Uniform_Base
	{
//		scope = 1;
	};
	class U_BG_Guerrilla_6_1: Uniform_Base
	{
		scope = 1;//2
	};
	class U_OG_Guerrilla_6_1: Uniform_Base
	{
//		scope = 1;
	};
	class U_B_Soldier_VR: Uniform_Base
	{
//		scope = 1;
	};
	class U_O_Soldier_VR: Uniform_Base
	{
//		scope = 1;
	};
	class U_I_Soldier_VR: Uniform_Base
	{
//		scope = 1;
	};
	class U_C_Soldier_VR: Uniform_Base
	{
//		scope = 1;
	};
	class U_C_Driver_1: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_Driver_2: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_Driver_3: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_Driver_4: Uniform_Base
	{
		scope = 1;//2
	};
	class U_Marshal: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_Driver_1_black: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_Driver_1_blue: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_Driver_1_green: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_Driver_1_red: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_Driver_1_white: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_Driver_1_yellow: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_Driver_1_orange: Uniform_Base
	{
		scope = 1;//2
	};
	class U_B_FullGhillie_lsh: Uniform_Base
	{
		scope = 1;//2
	};
	class U_B_FullGhillie_sard: Uniform_Base
	{
		scope = 1;//2
	};
	class U_B_FullGhillie_ard: Uniform_Base
	{
		scope = 1;//2
	};
	class U_O_FullGhillie_lsh: Uniform_Base
	{
		scope = 1;//2
	};
	class U_O_FullGhillie_sard: Uniform_Base
	{
		scope = 1;//2
	};
	class U_O_FullGhillie_ard: Uniform_Base
	{
		scope = 1;//2
	};
	class U_I_FullGhillie_lsh: Uniform_Base
	{
		scope = 1;//2
	};
	class U_I_FullGhillie_sard: Uniform_Base
	{
		scope = 1;//2
	};
	class U_I_FullGhillie_ard: Uniform_Base
	{
		scope = 1;//2
	};
	class V_Rangemaster_belt: Vest_NoCamo_Base
	{
		scope = 1;//2
	};
	class V_BandollierB_khk: Vest_Camo_Base
	{
		scope = 1;//2
	};
	class V_PlateCarrier1_rgr: Vest_NoCamo_Base
	{
		scope = 1;//2
	};
	class V_PlateCarrier2_rgr: V_PlateCarrier1_rgr
	{
		scope = 1;//2
	};
	class V_PlateCarrier2_blk: V_PlateCarrier2_rgr
	{
		scope = 1;//2
	};
	class V_PlateCarrier3_rgr: Vest_NoCamo_Base
	{
//		scope = 1;
	};
	class V_PlateCarrierGL_rgr: Vest_NoCamo_Base
	{
		scope = 1;//2
	};
	class V_PlateCarrier1_blk: Vest_Camo_Base
	{
		scope = 1;//2
	};
	class V_PlateCarrierSpec_rgr: Vest_NoCamo_Base
	{
		scope = 1;//2
	};
	class V_Chestrig_khk: Vest_Camo_Base
	{
		scope = 1;//2
	};
	class V_Chestrig_rgr: V_Chestrig_khk
	{
		scope = 1;//2
	};
	class V_Chestrig_blk: V_Chestrig_khk
	{
		scope = 1;//2
	};
	class V_Chestrig_oli: Vest_Camo_Base
	{
		scope = 1;//2
	};
	class V_TacVest_khk: Vest_Camo_Base
	{
		scope = 1;//2
	};
	class V_TacVest_camo: Vest_Camo_Base
	{
		scope = 1;//2
	};
	class V_TacVest_blk_POLICE: Vest_Camo_Base
	{
		scope = 1;//2
	};
	class V_TacVestIR_blk: Vest_NoCamo_Base
	{
		scope = 1;//2
	};
	class V_TacVestCamo_khk: Vest_Camo_Base
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class V_HarnessO_brn: Vest_NoCamo_Base
	{
		scope = 1;//2
	};
	class V_HarnessOGL_brn: Vest_NoCamo_Base
	{
		scope = 1;//2
	};
	class V_HarnessOSpec_brn: V_HarnessO_brn
	{
//		scope = 1;
	};
	class V_HarnessOSpec_gry: V_HarnessO_gry
	{
//		scope = 1;
	};
	class V_PlateCarrierIA1_dgtl: Vest_NoCamo_Base
	{
		scope = 1;//2
	};
	class V_PlateCarrierIAGL_dgtl: V_PlateCarrierIA2_dgtl
	{
		scope = 1;//2
	};
	class V_RebreatherB: Vest_Camo_Base
	{
		scope = 1;//2
	};
	class V_PlateCarrier_Kerry: V_PlateCarrier1_rgr
	{
		scope = 1;//2
	};
	class V_Press_F: Vest_Camo_Base
	{
		scope = 1;//2
	};
	class H_HelmetB: ItemCore
	{
		scope = 1;//2
	};
	class H_HelmetB_paint: H_HelmetB
	{
//		scope = 1;
	};
	class H_Booniehat_khk: HelmetBase
	{
		scope = 1;//2
	};
	class H_Booniehat_indp: H_Booniehat_khk
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class H_Booniehat_grn: H_Booniehat_khk
	{
//		scope = 1;
	};
	class H_Booniehat_tan: H_Booniehat_khk
	{
		scope = 1;//2
	};
	class H_Booniehat_dirty: H_Booniehat_khk
	{
//		scope = 1;
	};
	class H_HelmetB_plain_mcamo: H_HelmetB
	{
//		scope = 1;
	};
	class H_HelmetB_plain_blk: H_HelmetB_plain_mcamo
	{
//		scope = 1;
	};
	class H_HelmetSpecB: H_HelmetB_plain_mcamo
	{
		scope = 1;//2
	};
	class H_HelmetIA_net: H_HelmetIA
	{
//		scope = 1;
	};
	class H_HelmetIA_camo: H_HelmetIA
	{
//		scope = 1;
	};
	class H_Helmet_Kerry: H_HelmetB
	{
//		scope = 1;
	};
	class H_Cap_red: HelmetBase
	{
		scope = 1;//2
	};
	class H_Cap_headphones: HelmetBase
	{
		scope = 1;//2
	};
	class H_MilCap_ocamo: HelmetBase
	{
		scope = 1;//2
	};
	class H_MilCap_oucamo: H_MilCap_ocamo
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class H_MilCap_rucamo: H_MilCap_oucamo
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class H_MilCap_gry: H_MilCap_oucamo
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class H_MilCap_dgtl: H_MilCap_oucamo
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class H_MilCap_blue: H_MilCap_oucamo
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class H_BandMask_blk: HelmetBase
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class H_Bandanna_surfer: HelmetBase
	{
		scope = 1;//2
	};
	class H_Shemag_khk: HelmetBase
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class H_Shemag_tan: H_Shemag_khk
	{
//		scope = 1;
	};
	class H_Shemag_olive: H_Shemag_khk
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class H_ShemagOpen_khk: HelmetBase
	{
		scope = 1;//2
	};
	class H_ShemagOpen_tan: H_ShemagOpen_khk
	{
		scope = 1;//2
	};
	class H_Beret_blk: HelmetBase
	{
		scope = 1;//2
	};
	class H_Beret_blk_POLICE: H_Beret_blk
	{
//		scope = 1;
	};
	class H_Beret_red: H_Beret_blk
	{
//		scope = 1;
	};
	class H_Beret_grn: H_Beret_blk
	{
//		scope = 1;
	};
	class H_Beret_grn_SF: H_Beret_blk
	{
//		scope = 1;
	};
	class H_Beret_brn_SF: H_Beret_blk
	{
//		scope = 1;
	};
	class H_Beret_ocamo: H_Beret_blk
	{
//		scope = 1;
	};
	class H_Watchcap_blk: HelmetBase
	{
		scope = 1;//2
	};
	class H_Watchcap_sgg: H_Watchcap_blk
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class H_TurbanO_blk: HelmetBase
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class H_StrawHat: HelmetBase
	{
		scope = 1;//2
	};
	class H_Hat_blue: HelmetBase
	{
		scope = 1;//2
	};
	class Zasleh2: ItemCore
	{
		scope = 1;//2
	};
	class muzzle_snds_H: ItemCore
	{
		scope = 1;//2
	};
	class muzzle_snds_H_MG: muzzle_snds_H
	{
//		scope = 1;
	};
	class muzzle_snds_H_SW: muzzle_snds_H_MG
	{
//		scope = 1;
	};
	class muzzle_snds_570: ItemCore
	{
		scope = 1;//2
	};
	class optic_Arco: ItemCore
	{
		scope = 1;//2
	};
	class optic_Hamr: ItemCore
	{
		scope = 1;//2
	};
	class optic_Aco: ItemCore
	{
		scope = 1;//2
	};
	class optic_ACO_grn: ItemCore
	{
		scope = 1;//2
	};
	class optic_Aco_smg: ItemCore
	{
		scope = 1;//2
	};
	class optic_ACO_grn_smg: ItemCore
	{
		scope = 1;//2
	};
	class optic_Holosight: ItemCore
	{
		scope = 1;//2
	};
	class optic_Holosight_smg: ItemCore
	{
		scope = 1;//2
	};
	class optic_SOS: ItemCore
	{
		scope = 1;//2
	};
	class acc_flashlight: ItemCore
	{
		scope = 1;//2
	};
	class acc_flashlight_smg_01: acc_flashlight
	{
		scope = 1;//2
	};
	class acc_flashlight_pistol: ItemCore
	{
		scope = 1;//2
	};
	class acc_pointer_IR: ItemCore
	{
		scope = 1;//2
	};
	class optic_MRCO: ItemCore
	{
		scope = 1;//2
	};
	class optic_NVS: ItemCore
	{
		scope = 1;//2
	};
	class optic_Nightstalker: ItemCore
	{
		scope = 1;//2
	};
	class optic_tws: ItemCore
	{
		scope = 1;//2
	};
	class optic_tws_mg: ItemCore
	{
		scope = 1;//2
	};
	class optic_DMS: ItemCore
	{
		scope = 1;//2
	};
	class optic_Yorris: ItemCore
	{
		scope = 1;//2
	};
	class optic_MRD: ItemCore
	{
		scope = 1;//2
	};
	class optic_LRPS: ItemCore
	{
		scope = 1;//2
	};
	class B_UavTerminal: UavTerminal_base
	{
		scope = 1;//2
	};
	class O_UavTerminal: UavTerminal_base
	{
		scope = 1;//2
	};
	class I_UavTerminal: UavTerminal_base
	{
		scope = 1;//2
	};
	class FakeWeapon_moduleTracers_F: LMG_Mk200_F
	{
//		scope = 1;
	};
	class U_VirtualMan_F: Uniform_Base
	{
//		scope = 1;
	};
	class hgun_Pistol_Signal_F: Pistol_Base_F
	{
		scope = 1;//2
	};
	class muzzle_snds_338_black: ItemCore
	{
		scope = 1;//2
	};
	class muzzle_snds_93mmg: ItemCore
	{
		scope = 1;//2
	};
	class optic_AMS: optic_AMS_base
	{
		scope = 1;//2
//		scopeCurator = 1;
		scopeArsenal = 1;//2
	};
	class optic_KHS_blk: optic_KHS_base
	{
		scope = 1;//2
//		scopeCurator = 1;
		scopeArsenal = 1;//2
	};
	class optic_KHS_old: ItemCore
	{
		scope = 1;//2
//		scopeCurator = 1;
		scopeArsenal = 1;//2
	};
	class bipod_01_F_snd: ItemCore
	{
		scope = 1;//2
	};
	class srifle_DMR_02_F: DMR_02_base_F
	{
		scope = 1;//2
	};
	class srifle_DMR_03_F: DMR_03_base_F
	{
		scope = 1;//2
	};
	class srifle_DMR_04_F: DMR_04_base_F
	{
		scope = 1;//2
	};
	class srifle_DMR_05_blk_F: DMR_05_base_F
	{
		scope = 1;//2
	};
	class srifle_DMR_06_camo_F: DMR_06_base_F
	{
		scope = 1;//2
	};
	class MMG_01_hex_F: MMG_01_base_F
	{
		scope = 1;//2
	};
	class MMG_02_camo_F: MMG_02_base_F
	{
		scope = 1;//2
	};
	class SMG_03_TR_black: SMG_03_TR_BASE
	{
		scope = 1;//2
	};
	class SMG_03_TR_hex: SMG_03_TR_BASE
	{
		scope = 1;//2
	};
	class SMG_03C_TR_black: SMG_03C_BASE
	{
		scope = 1;//2
	};
	class SMG_03_black: SMG_03_TR_BASE
	{
		scope = 1;//2
	};
	class SMG_03C_black: SMG_03C_BASE
	{
		scope = 1;//2
	};
	class srifle_GM6_ghex_F: srifle_GM6_F
	{
		scope = 1;//2
	};
	class hgun_P07_khk_Snds_F: hgun_P07_khk_F
	{
//		scope = 1;
	};
	class muzzle_snds_58_wdm_F: muzzle_snds_58_blk_F
	{
//		scope = 1;
	};
	class muzzle_snds_58_ghex_F: muzzle_snds_58_wdm_F
	{
		scope = 1;//2
	};
	class optic_ERCO_blk_F: optic_Arco
	{
		scope = 1;//2
	};
	class launch_RPG7_F: Launcher_Base_F
	{
		scope = 1;//2
	};
	class srifle_DMR_07_blk_F: DMR_07_base_F
	{
		scope = 1;//2
	};
	class srifle_DMR_07_hex_F: DMR_07_base_F
	{
		scope = 1;//2
	};
	class srifle_DMR_07_ghex_F: DMR_07_base_F
	{
		scope = 1;//2
	};
	class LMG_03_F: LMG_03_base_F
	{
		scope = 1;//2
	};
	class hgun_Pistol_01_F: Pistol_Base_F
	{
		scope = 1;//2
	};
	class arifle_AK12_F: arifle_AK12_base_F
	{
		scope = 1;//2
	};
	class arifle_AK12_GL_F: arifle_AK12_GL_base_F
	{
		scope = 1;//2
	};
	class arifle_AKM_F: arifle_AKM_base_F
	{
		scope = 1;//2
	};
	class arifle_AKM_FL_F: arifle_AKM_F
	{
//		scope = 1;
	};
	class arifle_AKS_F: arifle_AKS_base_F
	{
		scope = 1;//2
	};
	class arifle_ARX_blk_F: arifle_ARX_base_F
	{
		scope = 1;//2
	};
	class arifle_ARX_ghex_F: arifle_ARX_base_F
	{
		scope = 1;//2
	};
	class arifle_ARX_hex_F: arifle_ARX_base_F
	{
		scope = 1;//2
	};
	class arifle_ARX_hex_ARCO_Pointer_Snds_F: arifle_ARX_hex_F
	{
//		scope = 1;
	};
	class arifle_ARX_ghex_ARCO_Pointer_Snds_F: arifle_ARX_ghex_F
	{
//		scope = 1;
	};
	class arifle_ARX_hex_ACO_Pointer_Snds_F: arifle_ARX_hex_F
	{
//		scope = 1;
	};
	class arifle_ARX_ghex_ACO_Pointer_Snds_F: arifle_ARX_ghex_F
	{
//		scope = 1;
	};
	class arifle_ARX_hex_DMS_Pointer_Snds_Bipod_F: arifle_ARX_hex_F
	{
//		scope = 1;
	};
	class arifle_ARX_ghex_DMS_Pointer_Snds_Bipod_F: arifle_ARX_ghex_F
	{
//		scope = 1;
	};
	class arifle_ARX_Viper_F: arifle_ARX_ghex_F
	{
//		scope = 1;
	};
	class arifle_ARX_Viper_hex_F: arifle_ARX_hex_F
	{
//		scope = 1;
	};
	class arifle_CTAR_blk_F: arifle_CTAR_base_F
	{
		scope = 1;//2
	};
	class arifle_CTAR_hex_F: arifle_CTAR_base_F
	{
		scope = 1;//2
	};
	class arifle_CTAR_ghex_F: arifle_CTAR_base_F
	{
		scope = 1;//2
	};
	class arifle_CTAR_GL_blk_F: arifle_CTAR_GL_base_F
	{
		scope = 1;//2
	};
	class arifle_CTAR_GL_hex_F: arifle_CTAR_GL_base_F
	{
		scope = 1;//2
	};
	class arifle_CTAR_GL_ghex_F: arifle_CTAR_GL_base_F
	{
		scope = 1;//2
	};
	class arifle_CTARS_blk_F: arifle_CTARS_base_F
	{
		scope = 1;//2
	};
	class arifle_CTARS_hex_F: arifle_CTARS_base_F
	{
		scope = 1;//2
	};
	class arifle_CTARS_ghex_F: arifle_CTARS_base_F
	{
		scope = 1;//2
	};
	class arifle_SPAR_01_blk_F: arifle_SPAR_01_base_F
	{
		scope = 1;//2
	};
	class arifle_SPAR_01_khk_F: arifle_SPAR_01_base_F
	{
		scope = 1;//2
	};
	class arifle_SPAR_01_snd_F: arifle_SPAR_01_base_F
	{
		scope = 1;//2
	};
	class arifle_SPAR_01_GL_blk_F: arifle_SPAR_01_GL_base_F
	{
		scope = 1;//2
	};
	class arifle_SPAR_01_GL_khk_F: arifle_SPAR_01_GL_base_F
	{
		scope = 1;//2
	};
	class arifle_SPAR_01_GL_snd_F: arifle_SPAR_01_GL_base_F
	{
		scope = 1;//2
	};
	class arifle_SPAR_01_blk_ERCO_Pointer_F: arifle_SPAR_01_blk_F
	{
//		scope = 1;
	};
	class arifle_SPAR_01_blk_ACO_Pointer_F: arifle_SPAR_01_blk_F
	{
//		scope = 1;
	};
	class arifle_SPAR_01_GL_blk_ACO_Pointer_F: arifle_SPAR_01_GL_blk_F
	{
//		scope = 1;
	};
	class arifle_SPAR_01_GL_blk_ERCO_Pointer_F: arifle_SPAR_01_GL_blk_F
	{
//		scope = 1;
	};
	class arifle_SPAR_02_blk_F: arifle_SPAR_02_base_F
	{
		scope = 1;//2
	};
	class arifle_SPAR_02_khk_F: arifle_SPAR_02_base_F
	{
		scope = 1;//2
	};
	class arifle_SPAR_02_snd_F: arifle_SPAR_02_base_F
	{
		scope = 1;//2
	};
	class arifle_SPAR_02_blk_Pointer_F: arifle_SPAR_02_blk_F
	{
//		scope = 1;
	};
	class arifle_SPAR_02_blk_ERCO_Pointer_F: arifle_SPAR_02_blk_F
	{
//		scope = 1;
	};
	class arifle_SPAR_03_blk_F: arifle_SPAR_03_base_F
	{
		scope = 1;//2
	};
	class arifle_SPAR_03_khk_F: arifle_SPAR_03_base_F
	{
		scope = 1;//2
	};
	class arifle_SPAR_03_snd_F: arifle_SPAR_03_base_F
	{
		scope = 1;//2
	};
	class arifle_SPAR_03_blk_MOS_Pointer_Bipod_F: arifle_SPAR_03_blk_F
	{
//		scope = 1;
	};
	class SMG_05_F: SMG_05_base_F
	{
		scope = 1;//2
	};
	class U_B_T_Soldier_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_B_T_Soldier_AR_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_B_T_Soldier_SL_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_B_T_Sniper_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_B_T_FullGhillie_tna_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_B_CTRG_Soldier_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_B_CTRG_Soldier_2_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_B_CTRG_Soldier_3_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_B_GEN_Soldier_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_B_GEN_Commander_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_O_T_Soldier_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_O_T_Officer_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_O_T_Sniper_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_O_T_FullGhillie_tna_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_O_V_Soldier_Viper_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_O_V_Soldier_Viper_hex_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_I_C_Soldier_Para_1_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_I_C_Soldier_Para_2_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_I_C_Soldier_Para_3_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_I_C_Soldier_Para_4_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_I_C_Soldier_Para_5_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_I_C_Soldier_Bandit_1_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_I_C_Soldier_Bandit_2_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_I_C_Soldier_Bandit_3_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_I_C_Soldier_Bandit_4_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_I_C_Soldier_Bandit_5_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_I_C_Soldier_Camo_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_man_sport_1_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_man_sport_2_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_man_sport_3_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_Man_casual_1_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_Man_casual_2_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_Man_casual_3_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_Man_casual_4_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_Man_casual_5_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_Man_casual_6_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_B_CTRG_Soldier_urb_1_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_B_CTRG_Soldier_urb_2_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_B_CTRG_Soldier_urb_3_F: Uniform_Base
	{
		scope = 1;//2
	};
	class H_Helmet_Skate: H_HelmetB
	{
		scope = 1;//2
	};
	class H_HelmetO_ViperSP_hex_F: H_HelmetB
	{
		scope = 1;//2
	};
	class H_Cap_oli_Syndikat_F: H_Cap_oli
	{
//		scope = 1;
	};
	class H_Cap_tan_Syndikat_F: H_Cap_tan
	{
//		scope = 1;
	};
	class H_Cap_blk_Syndikat_F: H_Cap_blk
	{
//		scope = 1;
	};
	class H_Cap_grn_Syndikat_F: H_Cap_grn
	{
//		scope = 1;
	};
	class V_TacChestrig_grn_F: Vest_Camo_Base
	{
		scope = 1;//2
	};
	class V_DeckCrew_base_F: Vest_Camo_Base
	{
	};
	class V_DeckCrew_yellow_F: V_DeckCrew_base_F
	{
		scope = 1;//2
	};
	class V_DeckCrew_blue_F: V_DeckCrew_base_F
	{
		scope = 1;//2
	};
	class V_DeckCrew_green_F: V_DeckCrew_base_F
	{
		scope = 1;//2
	};
	class V_DeckCrew_red_F: V_DeckCrew_base_F
	{
		scope = 1;//2
	};
	class V_DeckCrew_white_F: V_DeckCrew_base_F
	{
		scope = 1;//2
	};
	class V_DeckCrew_brown_F: V_DeckCrew_base_F
	{
		scope = 1;//2
	};
	class V_DeckCrew_violet_F: V_DeckCrew_base_F
	{
		scope = 1;//2
	};
	class C_UavTerminal: UavTerminal_base
	{
		scope = 1;//2
	};
	class U_C_IDAP_Man_shorts_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_IDAP_Man_casual_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_IDAP_Man_cargo_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_IDAP_Man_Tee_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_IDAP_Man_Jeans_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_IDAP_Man_TeeShorts_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_Paramedic_01_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_Mechanic_01_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_BG_Guerilla1_2_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_ConstructionCoverall_Red_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_ConstructionCoverall_Vrana_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_ConstructionCoverall_Black_F: Uniform_Base
	{
		scope = 1;//2
	};
	class U_C_ConstructionCoverall_Blue_F: Uniform_Base
	{
		scope = 1;//2
	};
	class H_Hat_Safari_sand_F: H_Hat_Safari_base_F
	{
		scope = 1;//2
	};
	class H_Hat_Safari_olive_F: H_Hat_Safari_base_F
	{
		scope = 1;//2
	};
	class H_Construction_basic_yellow_F: H_Construction_basic_base_F
	{
		scope = 1;//2
	};
	class H_Construction_basic_white_F: H_Construction_basic_base_F
	{
		scope = 1;//2
	};
	class H_Construction_basic_orange_F: H_Construction_basic_base_F
	{
		scope = 1;//2
	};
	class H_Construction_basic_red_F: H_Construction_basic_base_F
	{
		scope = 1;//2
	};
	class H_Construction_basic_vrana_F: H_Construction_basic_base_F
	{
		scope = 1;//2
	};
	class H_Construction_basic_black_F: H_Construction_basic_base_F
	{
		scope = 1;//2
	};
	class H_Construction_earprot_yellow_F: H_Construction_earprot_base_F
	{
		scope = 1;//2
	};
	class H_Construction_earprot_white_F: H_Construction_earprot_base_F
	{
		scope = 1;//2
	};
	class H_Construction_earprot_orange_F: H_Construction_earprot_base_F
	{
		scope = 1;//2
	};
	class H_Construction_earprot_red_F: H_Construction_earprot_base_F
	{
		scope = 1;//2
	};
	class H_Construction_earprot_vrana_F: H_Construction_earprot_base_F
	{
		scope = 1;//2
	};
	class H_Construction_earprot_black_F: H_Construction_earprot_base_F
	{
		scope = 1;//2
	};
	class H_Construction_headset_yellow_F: H_Construction_headset_base_F
	{
		scope = 1;//2
	};
	class H_Construction_headset_white_F: H_Construction_headset_base_F
	{
		scope = 1;//2
	};
	class H_Construction_headset_orange_F: H_Construction_headset_base_F
	{
		scope = 1;//2
	};
	class H_Construction_headset_red_F: H_Construction_headset_base_F
	{
		scope = 1;//2
	};
	class H_Construction_headset_vrana_F: H_Construction_headset_base_F
	{
		scope = 1;//2
	};
	class H_Construction_headset_black_F: H_Construction_headset_base_F
	{
		scope = 1;//2
	};
	class H_EarProtectors_yellow_F: H_EarProtectors_base_F
	{
		scope = 1;//2
	};
	class H_EarProtectors_white_F: H_EarProtectors_base_F
	{
		scope = 1;//2
	};
	class H_EarProtectors_orange_F: H_EarProtectors_base_F
	{
		scope = 1;//2
	};
	class H_EarProtectors_red_F: H_EarProtectors_base_F
	{
		scope = 1;//2
	};
	class H_EarProtectors_black_F: H_EarProtectors_base_F
	{
		scope = 1;//2
	};
	class H_HeadSet_yellow_F: H_HeadSet_base_F
	{
		scope = 1;//2
	};
	class H_HeadSet_white_F: H_HeadSet_base_F
	{
		scope = 1;//2
	};
	class H_HeadSet_orange_F: H_HeadSet_base_F
	{
		scope = 1;//2
	};
	class H_HeadSet_red_F: H_HeadSet_base_F
	{
		scope = 1;//2
	};
	class H_HeadSet_black_F: H_HeadSet_base_F
	{
		scope = 1;//2
	};
	class H_PASGT_basic_blue_press_F: H_PASGT_basic_base_F
	{
		scope = 1;//2
	};
	class H_PASGT_basic_blue_F: H_PASGT_basic_base_F
	{
		scope = 1;//2
	};
	class H_PASGT_basic_white_F: H_PASGT_basic_base_F
	{
		scope = 1;//2
	};
	class H_PASGT_basic_olive_F: H_PASGT_basic_base_F
	{
		scope = 1;//2
	};
	class H_PASGT_basic_black_F: H_PASGT_basic_base_F
	{
		scope = 1;//2
	};
	class H_PASGT_neckprot_blue_press_F: H_PASGT_neckprot_base_F
	{
		scope = 1;//2
	};
	class H_HeadBandage_stained_F: H_HeadBandage_base_F
	{
		scope = 1;//2
	};
	class H_HeadBandage_clean_F: H_HeadBandage_base_F
	{
		scope = 1;//2
	};
	class H_HeadBandage_bloody_F: H_HeadBandage_base_F
	{
		scope = 1;//2
	};
	class H_Cap_White_IDAP_F: H_Cap_red
	{
		scope = 1;//2
	};
	class H_Cap_Orange_IDAP_F: H_Cap_White_IDAP_F
	{
		scope = 1;//2
	};
	class H_Cap_Black_IDAP_F: H_Cap_White_IDAP_F
	{
		scope = 1;//2
	};
	class H_WirelessEarpiece_F: H_WirelessEarpiece_base_F
	{
		scope = 1;//2
	};
	class V_Plain_medical_F: V_Plain_base_F
	{
		scope = 1;//2
	};
	class V_Plain_crystal_F: V_Plain_base_F
	{
		scope = 1;//2
	};
	class V_Pocketed_olive_F: V_Pocketed_base_F
	{
		scope = 1;//2
	};
	class V_Pocketed_coyote_F: V_Pocketed_base_F
	{
		scope = 1;//2
	};
	class V_Pocketed_black_F: V_Pocketed_base_F
	{
		scope = 1;//2
	};
	class V_Safety_yellow_F: V_Safety_base_F
	{
		scope = 1;//2
	};
	class V_Safety_orange_F: V_Safety_base_F
	{
		scope = 1;//2
	};
	class V_Safety_blue_F: V_Safety_base_F
	{
		scope = 1;//2
	};
	class V_LegStrapBag_black_F: V_LegStrapBag_base_F
	{
		scope = 1;//2
	};
	class V_LegStrapBag_coyote_F: V_LegStrapBag_base_F
	{
		scope = 1;//2
	};
	class V_LegStrapBag_olive_F: V_LegStrapBag_base_F
	{
		scope = 1;//2
	};
	class V_EOD_blue_F: V_EOD_base_F
	{
		scope = 1;//2
	};
	class V_EOD_olive_F: V_EOD_base_F
	{
		scope = 1;//2
	};
	class V_EOD_coyote_F: V_EOD_base_F
	{
		scope = 1;//2
	};
	class V_EOD_IDAP_blue_F: V_EOD_base_F
	{
		scope = 1;//2
	};
	class U_O_officer_noInsignia_hex_F: Uniform_Base
	{
		scope = 1;//2
	};
	class launch_MRAWS_olive_F: launch_MRAWS_base_F
	{
		scope = 1;//2
	};
	class launch_MRAWS_olive_rail_F: launch_MRAWS_olive_F
	{
		scope = 1;//2
	};
	class launch_MRAWS_green_F: launch_MRAWS_base_F
	{
		scope = 1;//2
	};
	class launch_MRAWS_green_rail_F: launch_MRAWS_olive_rail_F
	{
		scope = 1;//2
	};
	class launch_MRAWS_sand_F: launch_MRAWS_base_F
	{
		scope = 1;//2
	};
	class launch_MRAWS_sand_rail_F: launch_MRAWS_olive_rail_F
	{
		scope = 1;//2
	};
	class launch_O_Vorona_brown_F: launch_Vorona_base_F
	{
		scope = 1;//2
	};
	class launch_O_Vorona_green_F: launch_Vorona_base_F
	{
		scope = 1;//2
	};
	class H_Tank_black_F: H_Tank_base_F
	{
		scope = 1;//2
	};
	class U_Tank_green_F: Uniform_Base
	{
		scope = 1;//2
	};
};
class CfgVehicles
{
	class All;
	class B_AssaultPack_Base;
	class B_Bergen_Base;
	class B_Bergen_Base_F;
	class B_BergenC_Base;
	class B_Carryall_Base;
	class B_FieldPack_Base;
	class B_Kitbag_Base;
	class B_LegStrapBag_base_F;
	class B_Messenger_Base_F;
	class B_OutdoorPack_Base;
	class B_TacticalPack_Base;
	class B_ViperHarness_base_F;
	class B_ViperLightHarness_base_F;
	class Box_UAV_06_base_F;
	class Box_UAV_06_medical_base_F;
	class C_Bergen_Base;
	class CargoNet_01_ammo_base_F;
	class EAST_Box_Base;
	class FIA_Box_Base_F;
	class Headgear_Base_F;
	class I_GMG_01_weapon_F;
	class I_HMG_01_support_F;
	class I_HMG_01_weapon_F;
	class I_Mortar_01_support_F;
	class I_Mortar_01_weapon_F;
	class IND_Box_Base;
	class Item_Base_F;
	class Land_File1_F;
	class Land_File2_F;
	class Land_Photos_V3_F;
	class Launcher_Base_F;
	class MineBase;
	class NATO_Box_Base;
	class NVG_TargetCBase;
	class NVG_TargetEBase;
	class NVG_TargetGBase;
	class NVG_TargetWBase;
	class Pistol_Base_F;
	class ReammoBox;
	class Slingload_01_Base_F;
	class Thing;
	class ThingX;
	class UAV_06_backpack_base_F;
	class UAV_06_medical_backpack_base_F;
	class Vest_Base_F;
	class Weapon_Bag_Base;
	class Weapon_Base_F;
	class WeaponHolder;

	class WeaponHolder_Single_F: WeaponHolder
	{
//		scope = 1;
	};
	class ContainerSupply: WeaponHolder
	{
//		scope = 1;
	};
	class placed_chemlight_green: All
	{
//		scope = 1;
	};
	class test_EmptyObjectForBubbles: Thing
	{
//		scope = 1;
	};
	class NVG_TargetC: NVG_TargetCBase
	{
//		scope = 1;
	};
	class NVG_TargetW: NVG_TargetWBase
	{
//		scope = 1;
	};
	class NVG_TargetE: NVG_TargetEBase
	{
//		scope = 1;
	};
	class NVG_TargetG: NVG_TargetGBase
	{
//		scope = 1;
	};
	class placed_B_IR_grenade: All
	{
		scope = 1;//2
	};
	class Item_ItemWatch: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_ItemCompass: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_ItemGPS: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_ItemRadio: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_ItemMap: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_MineDetector: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_Binocular: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_Rangefinder: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_NVGoggles: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_NVGoggles_OPFOR: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_NVGoggles_INDEP: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_FirstAidKit: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_Medikit: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_ToolKit: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_launch_NLAW_F: Launcher_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_launch_RPG32_F: Launcher_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_srifle_EBR_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_srifle_GM6_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_srifle_LRR_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_LMG_Mk200_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_hgun_P07_F: Pistol_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_hgun_Rook40_F: Pistol_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_Katiba_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_Katiba_C_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_Katiba_GL_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_MXC_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_MXC_Black_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_MX_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_MX_Black_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_MX_GL_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_MX_GL_Black_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_MX_SW_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_MX_SW_Black_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_MXM_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_MXM_Black_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_SDAR_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_TRG21_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_TRG20_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_TRG21_GL_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_Laserdesignator: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_launch_B_Titan_F: Launcher_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_launch_launch_B_Titan_short_F: Launcher_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_LMG_Zafir_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_hgun_ACPC2_F: Pistol_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_Mk20_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_Mk20_plain_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_Mk20C_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_Mk20C_plain_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_Mk20_GL_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_Mk20_GL_plain_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_SMG_01_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_SMG_02_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_srifle_DMR_01_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_hgun_Pistol_heavy_01_F: Pistol_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_hgun_Pistol_heavy_02_F: Pistol_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_hgun_PDW2000_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_BasicBody: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_B_CombatUniform_mcam: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_B_CombatUniform_mcam_tshirt: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_B_CombatUniform_mcam_vest: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_B_GhillieSuit: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_B_HeliPilotCoveralls: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_B_Wetsuit: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_O_CombatUniform_ocamo: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_O_GhillieSuit: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_O_PilotCoveralls: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_O_Wetsuit: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_Poloshirt_blue: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_Poloshirt_burgundy: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_Poloshirt_stripped: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_Poloshirt_tricolour: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_Poloshirt_salmon: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_Poloshirt_redwhite: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_I_G_resistanceLeader_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_B_Protagonist_VR: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_O_Protagonist_VR: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_I_Protagonist_VR: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_I_G_Story_Protagonist_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_Commoner1_1: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_C_Commoner1_2: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_C_Commoner1_3: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_Rangemaster: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_Rangemaster_belt: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_BandollierB_khk: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_BandollierB_cbr: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_BandollierB_rgr: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_BandollierB_blk: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_PlateCarrier1_rgr: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_PlateCarrier2_rgr: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_PlateCarrier2_blk: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_Chestrig_khk: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_Chestrig_rgr: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_Chestrig_blk: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_TacVest_khk: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_TacVest_brn: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_TacVest_oli: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_TacVest_blk: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_HarnessO_brn: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_HarnessOGL_brn: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_RebreatherB: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_RebreatherIR: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_I_G_resistanceLeader_F: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetB: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetB_camo: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Booniehat_khk: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Booniehat_oli: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Booniehat_mcamo: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetB_light: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Cap_red: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Cap_blu: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Cap_oli: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Cap_headphones: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_PilotHelmetHeli_B: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_PilotHelmetHeli_O: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetO_ocamo: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetLeaderO_ocamo: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_MilCap_ocamo: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_MilCap_mcamo: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_B_CombatUniform_mcam_worn: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_B_CombatUniform_wdl: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_B_CombatUniform_wdl_tshirt: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_B_CombatUniform_wdl_vest: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_B_CombatUniform_sgg: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_B_CombatUniform_sgg_tshirt: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_B_CombatUniform_sgg_vest: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_B_SpecopsUniform_sgg: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_B_PilotCoveralls: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_O_CombatUniform_oucamo: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_O_SpecopsUniform_ocamo: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_O_SpecopsUniform_blk: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_O_OfficerUniform_ocamo: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_I_CombatUniform: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_I_CombatUniform_tshirt: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_I_CombatUniform_shortsleeve: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_I_pilotCoveralls: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_I_HeliPilotCoveralls: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_I_GhillieSuit: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_I_OfficerUniform: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_I_Wetsuit: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_Competitor: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_NikosBody: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_MillerBody: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_KerryBody: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_OrestesBody: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_AttisBody: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_AntigonaBody: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_OI_Scientist: Item_Base_F
	{
//		scope = 1;
	};
	class Vest_V_BandollierB_oli: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_PlateCarrier1_blk: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_Chestrig_oli: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_TacVest_camo: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_TacVest_blk_POLICE: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_TacVestIR_blk: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_HarnessO_gry: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_HarnessOGL_gry: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_PlateCarrierIA1_dgtl: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_PlateCarrierIA2_dgtl: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_RebreatherIA: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Booniehat_tan: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Booniehat_dgtl: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Booniehat_khk_hs: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetSpecB: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetSpecB_paint1: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetSpecB_paint2: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetSpecB_blk: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetSpecB_snakeskin: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetSpecB_sand: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetIA: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Cap_tan: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Cap_blk: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Cap_blk_CMMG: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Cap_brn_SPECOPS: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Cap_tan_specops_US: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Cap_khaki_specops_UK: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Cap_grn: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Cap_grn_BI: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Cap_blk_Raven: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Cap_blk_ION: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Cap_oli_hs: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Cap_press: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Cap_usblack: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Cap_surfer: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Cap_police: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetCrew_B: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetCrew_O: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetCrew_I: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_PilotHelmetFighter_B: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_PilotHelmetFighter_O: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_PilotHelmetFighter_I: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_PilotHelmetHeli_I: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_CrewHelmetHeli_B: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_CrewHelmetHeli_O: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_CrewHelmetHeli_I: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetO_oucamo: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetLeaderO_oucamo: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetSpecO_ocamo: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetSpecO_blk: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_MilCap_gry: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_MilCap_dgtl: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_MilCap_blue: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Bandanna_surfer: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Bandanna_khk: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Bandanna_khk_hs: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Bandanna_cbr: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Bandanna_sgg: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Bandanna_sand: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Bandanna_gry: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Bandanna_camo: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Bandanna_mcamo: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Bandanna_surfer_blk: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Bandanna_surfer_grn: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Bandanna_blu: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Shemag_olive: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Shemag_olive_hs: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_ShemagOpen_khk: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_ShemagOpen_tan: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Beret_blk: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Beret_02: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Beret_Colonel: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Watchcap_blk: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Watchcap_cbr: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Watchcap_khk: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Watchcap_camo: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_StrawHat: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_StrawHat_dark: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Hat_blue: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Hat_brown: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Hat_camo: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Hat_grey: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Hat_checker: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Hat_tan: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_IG_Guerilla1_1: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_IG_Guerilla2_1: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_IG_Guerilla2_2: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_IG_Guerilla2_3: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_IG_Guerilla3_1: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_IG_Guerilla3_2: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_IG_leader: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_BG_Guerilla1_1: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_BG_Guerilla2_1: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_BG_Guerilla2_2: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_BG_Guerilla2_3: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_BG_Guerilla3_1: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_BG_Guerilla3_2: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_BG_leader: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_OG_Guerilla1_1: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_OG_Guerilla2_1: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_OG_Guerilla2_2: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_OG_Guerilla2_3: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_OG_Guerilla3_1: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_OG_Guerilla3_2: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_OG_leader: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_C_Poor_1: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_Poor_2: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_C_Scavenger_1: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_C_Scavenger_2: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_C_Farmer: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_C_Fisherman: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_C_WorkerOveralls: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_C_FishermanOveralls: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_C_WorkerCoveralls: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_HunterBody_grn: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_HunterBody_brn: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_C_Commoner2_1: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_C_Commoner2_2: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_C_Commoner2_3: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_C_PriestBody: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_C_Poor_shorts_1: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_C_Poor_shorts_2: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_C_Commoner_shorts: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_C_ShirtSurfer_shorts: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_C_TeeSurfer_shorts_1: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_C_TeeSurfer_shorts_2: Item_Base_F
	{
//		scope = 1;
	};
	class Item_B_UavTerminal: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_muzzle_snds_H: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_muzzle_snds_L: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_muzzle_snds_M: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_muzzle_snds_B: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_muzzle_snds_H_MG: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_optic_Arco: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_optic_Hamr: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_optic_Aco: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_optic_ACO_grn: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_optic_Aco_smg: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_optic_ACO_grn_smg: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_optic_Holosight: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_optic_Holosight_smg: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_optic_SOS: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_acc_flashlight: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_acc_pointer_IR: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_acc_flashlight_pistol: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_optic_MRCO: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_muzzle_snds_acp: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_optic_DMS: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_optic_Yorris: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_optic_MRD: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_optic_NVS: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_optic_Nightstalker: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_optic_tws: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_optic_tws_mg: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_optic_LRPS: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_B_CTRG_1: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_B_CTRG_2: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_B_CTRG_3: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_B_survival_uniform: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_PlateCarrier_Kerry: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_PlateCarrierL_CTRG: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_PlateCarrierH_CTRG: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetB_grass: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetB_snakeskin: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetB_desert: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetB_black: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetB_sand: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetB_light_grass: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetB_light_snakeskin: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetB_light_desert: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetB_light_black: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetB_light_sand: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_Empty: Weapon_Base_F
	{
//		scope = 1;
	};
	class ReammoBox_F: ThingX
	{
//		scope = 1;
	};
	class Box_NATO_Wps_F: NATO_Box_Base
	{
		scope = 1;//2
	};
	class Box_NATO_WpsSpecial_F: NATO_Box_Base
	{
		scope = 1;//2
	};
	class Box_NATO_Ammo_F: NATO_Box_Base
	{
		scope = 1;//2
	};
	class Box_NATO_AmmoOrd_F: NATO_Box_Base
	{
		scope = 1;//2
	};
	class Box_NATO_Grenades_F: NATO_Box_Base
	{
		scope = 1;//2
	};
	class Box_NATO_Support_F: NATO_Box_Base
	{
		scope = 1;//2
	};
	class Box_East_Wps_F: EAST_Box_Base
	{
		scope = 1;//2
	};
	class Box_East_WpsSpecial_F: EAST_Box_Base
	{
		scope = 1;//2
	};
	class Box_East_Ammo_F: EAST_Box_Base
	{
		scope = 1;//2
	};
	class Box_East_AmmoOrd_F: EAST_Box_Base
	{
		scope = 1;//2
	};
	class Box_East_Grenades_F: EAST_Box_Base
	{
		scope = 1;//2
	};
	class Box_East_Support_F: EAST_Box_Base
	{
		scope = 1;//2
	};
	class Box_Ammo_F: ReammoBox_F
	{
//		scope = 1;
	};
	class Land_Box_AmmoOld_F: ReammoBox_F
	{
//		scope = 1;
	};
	class Box_NATO_WpsLaunch_F: NATO_Box_Base
	{
		scope = 1;//2
	};
	class Box_NATO_AmmoVeh_F: NATO_Box_Base
	{
		scope = 1;//2
	};
	class Box_East_WpsLaunch_F: EAST_Box_Base
	{
		scope = 1;//2
	};
	class Box_East_AmmoVeh_F: EAST_Box_Base
	{
		scope = 1;//2
	};
	class Box_IND_Wps_F: IND_Box_Base
	{
		scope = 1;//2
	};
	class Box_IND_WpsSpecial_F: IND_Box_Base
	{
		scope = 1;//2
	};
	class Box_IND_WpsLaunch_F: IND_Box_Base
	{
		scope = 1;//2
	};
	class Box_IND_Ammo_F: IND_Box_Base
	{
		scope = 1;//2
	};
	class Box_IND_AmmoOrd_F: IND_Box_Base
	{
		scope = 1;//2
	};
	class Box_IND_Grenades_F: IND_Box_Base
	{
		scope = 1;//2
	};
	class Box_IND_Support_F: IND_Box_Base
	{
		scope = 1;//2
	};
	class Box_IND_AmmoVeh_F: IND_Box_Base
	{
		scope = 1;//2
	};
	class B_supplyCrate_F: ReammoBox_F
	{
		scope = 1;//2
	};
	class IG_supplyCrate_F: ReammoBox_F
	{
		scope = 1;//2
	};
	class C_supplyCrate_F: ReammoBox_F
	{
		scope = 1;//2
	};
	class Bag_Base: ReammoBox
	{
//		scope = 1;
	};
	class B_AssaultPack_khk: B_AssaultPack_Base
	{
		scope = 1;//2
	};
	class B_AssaultPack_dgtl: B_AssaultPack_Base
	{
		scope = 1;//2
	};
	class B_AssaultPack_rgr: B_AssaultPack_Base
	{
		scope = 1;//2
	};
	class B_AssaultPack_sgg: B_AssaultPack_Base
	{
		scope = 1;//2
	};
	class B_AssaultPack_blk: B_AssaultPack_Base
	{
		scope = 1;//2
	};
	class B_AssaultPack_cbr: B_AssaultPack_Base
	{
		scope = 1;//2
	};
	class B_AssaultPack_mcamo: B_AssaultPack_Base
	{
		scope = 1;//2
	};
	class B_AssaultPack_ocamo: B_AssaultPack_Base
	{
		scope = 1;//2
	};
	class B_Kitbag_rgr: B_Kitbag_Base
	{
		scope = 1;//2
	};
	class B_Kitbag_mcamo: B_Kitbag_Base
	{
		scope = 1;//2
	};
	class B_Kitbag_sgg: B_Kitbag_Base
	{
		scope = 1;//2
	};
	class B_Kitbag_cbr: B_Kitbag_Base
	{
		scope = 1;//2
	};
	class B_Kitbag_tan: B_Kitbag_Base
	{
		scope = 1;//2
	};
	class B_TacticalPack_rgr: B_TacticalPack_Base
	{
		scope = 1;//2
	};
	class B_TacticalPack_mcamo: B_TacticalPack_Base
	{
		scope = 1;//2
	};
	class B_TacticalPack_ocamo: B_TacticalPack_Base
	{
		scope = 1;//2
	};
	class B_TacticalPack_blk: B_TacticalPack_Base
	{
		scope = 1;//2
	};
	class B_TacticalPack_oli: B_TacticalPack_Base
	{
		scope = 1;//2
	};
	class B_FieldPack_khk: B_FieldPack_Base
	{
		scope = 1;//2
	};
	class B_FieldPack_ocamo: B_FieldPack_Base
	{
		scope = 1;//2
	};
	class B_FieldPack_oucamo: B_FieldPack_Base
	{
		scope = 1;//2
	};
	class B_FieldPack_cbr: B_FieldPack_Base
	{
		scope = 1;//2
	};
	class B_FieldPack_blk: B_FieldPack_Base
	{
		scope = 1;//2
	};
	class B_Carryall_ocamo: B_Carryall_Base
	{
		scope = 1;//2
	};
	class B_Carryall_oucamo: B_Carryall_Base
	{
		scope = 1;//2
	};
	class B_Carryall_mcamo: B_Carryall_Base
	{
		scope = 1;//2
	};
	class B_Carryall_khk: B_Carryall_Base
	{
		scope = 1;//2
	};
	class B_Carryall_cbr: B_Carryall_Base
	{
		scope = 1;//2
	};
	class B_Bergen_sgg: B_Bergen_Base
	{
//		scope = 1;
	};
	class B_Bergen_mcamo: B_Bergen_Base
	{
//		scope = 1;
	};
	class B_Bergen_rgr: B_Bergen_Base
	{
//		scope = 1;
	};
	class B_Bergen_blk: B_Bergen_Base
	{
//		scope = 1;
	};
	class B_OutdoorPack_blk: B_OutdoorPack_Base
	{
//		scope = 1;
	};
	class B_OutdoorPack_tan: B_OutdoorPack_Base
	{
//		scope = 1;
	};
	class B_OutdoorPack_blu: B_OutdoorPack_Base
	{
//		scope = 1;
	};
	class B_HuntingBackpack: Bag_Base
	{
//		scope = 1;
	};
	class B_AssaultPackG: Bag_Base
	{
//		scope = 1;
	};
	class B_BergenG: Bag_Base
	{
//		scope = 1;
	};
	class B_BergenC_red: B_BergenC_Base
	{
//		scope = 1;
	};
	class B_BergenC_grn: B_BergenC_Base
	{
//		scope = 1;
	};
	class B_BergenC_blu: B_BergenC_Base
	{
//		scope = 1;
	};
	class B_Parachute: Bag_Base
	{
		scope = 1;//2
	};
	class GroundWeaponHolder: WeaponHolder
	{
//		scope = 1;
	};
	class WeaponHolderSimulated: ThingX
	{
//		scope = 1;
	};
	class B_FieldPack_oli: B_FieldPack_Base
	{
		scope = 1;//2
	};
	class B_Carryall_oli: B_Carryall_Base
	{
		scope = 1;//2
	};
	class C_Bergen_red: C_Bergen_Base
	{
//		scope = 1;
	};
	class C_Bergen_grn: C_Bergen_Base
	{
//		scope = 1;
	};
	class C_Bergen_blu: C_Bergen_Base
	{
//		scope = 1;
	};
	class B_AssaultPack_rgr_LAT: B_AssaultPack_rgr
	{
//		scope = 1;
	};
	class B_AssaultPack_rgr_Medic: B_AssaultPack_rgr
	{
//		scope = 1;
	};
	class B_AssaultPack_rgr_Repair: B_AssaultPack_rgr
	{
//		scope = 1;
	};
	class B_Assault_Diver: B_AssaultPack_blk
	{
//		scope = 1;
	};
	class B_AssaultPack_blk_DiverExp: B_AssaultPack_blk
	{
//		scope = 1;
	};
	class B_Kitbag_rgr_Exp: B_Kitbag_rgr
	{
//		scope = 1;
	};
	class B_AssaultPack_mcamo_AT: B_Kitbag_rgr
	{
//		scope = 1;
	};
	class B_AssaultPack_rgr_ReconMedic: B_AssaultPack_rgr
	{
//		scope = 1;
	};
	class B_AssaultPack_rgr_ReconExp: B_Kitbag_rgr
	{
//		scope = 1;
	};
	class B_AssaultPack_rgr_ReconLAT: B_AssaultPack_rgr
	{
//		scope = 1;
	};
	class B_AssaultPack_mcamo_AA: B_Kitbag_rgr
	{
//		scope = 1;
	};
	class B_AssaultPack_mcamo_AAR: B_TacticalPack_mcamo
	{
//		scope = 1;
	};
	class B_AssaultPack_mcamo_Ammo: B_Carryall_mcamo
	{
//		scope = 1;
	};
	class B_Kitbag_mcamo_Eng: B_Kitbag_mcamo
	{
//		scope = 1;
	};
	class B_Carryall_mcamo_AAA: B_Carryall_mcamo
	{
//		scope = 1;
	};
	class B_Carryall_mcamo_AAT: B_Carryall_mcamo
	{
//		scope = 1;
	};
	class B_Kitbag_rgr_AAR: B_Kitbag_rgr
	{
//		scope = 1;
	};
	class B_FieldPack_blk_DiverExp: B_FieldPack_blk
	{
//		scope = 1;
	};
	class O_Assault_Diver: B_FieldPack_blk
	{
//		scope = 1;
	};
	class B_FieldPack_ocamo_Medic: B_FieldPack_ocamo
	{
//		scope = 1;
	};
	class B_FieldPack_cbr_LAT: B_FieldPack_ocamo
	{
//		scope = 1;
	};
	class B_FieldPack_cbr_Repair: B_FieldPack_ocamo
	{
//		scope = 1;
	};
	class B_Carryall_ocamo_Exp: B_Carryall_ocamo
	{
//		scope = 1;
	};
	class B_FieldPack_ocamo_AA: B_FieldPack_ocamo
	{
//		scope = 1;
	};
	class B_FieldPack_ocamo_AAR: B_Carryall_ocamo
	{
//		scope = 1;
	};
	class B_FieldPack_ocamo_ReconMedic: B_FieldPack_ocamo
	{
//		scope = 1;
	};
	class B_FieldPack_cbr_AT: B_FieldPack_cbr
	{
//		scope = 1;
	};
	class B_FieldPack_cbr_AAT: B_FieldPack_cbr
	{
//		scope = 1;
	};
	class B_FieldPack_cbr_AA: B_FieldPack_cbr
	{
//		scope = 1;
	};
	class B_FieldPack_cbr_AAA: B_FieldPack_cbr
	{
//		scope = 1;
	};
	class B_FieldPack_cbr_Medic: B_FieldPack_cbr
	{
//		scope = 1;
	};
	class B_FieldPack_ocamo_ReconExp: B_Carryall_ocamo
	{
//		scope = 1;
	};
	class B_FieldPack_cbr_Ammo: B_Carryall_ocamo
	{
//		scope = 1;
	};
	class B_FieldPack_cbr_RPG_AT: B_FieldPack_cbr
	{
//		scope = 1;
	};
	class B_Carryall_ocamo_AAA: B_Carryall_ocamo
	{
//		scope = 1;
	};
	class B_Carryall_ocamo_Eng: B_Carryall_ocamo
	{
//		scope = 1;
	};
	class B_Carryall_cbr_AAT: B_Carryall_cbr
	{
//		scope = 1;
	};
	class B_FieldPack_oucamo_AT: B_FieldPack_oucamo
	{
//		scope = 1;
	};
	class B_FieldPack_oucamo_LAT: B_FieldPack_oucamo
	{
//		scope = 1;
	};
	class B_Carryall_oucamo_AAT: B_Carryall_oucamo
	{
//		scope = 1;
	};
	class B_FieldPack_oucamo_AA: B_FieldPack_oucamo
	{
//		scope = 1;
	};
	class B_Carryall_oucamo_AAA: B_Carryall_oucamo
	{
//		scope = 1;
	};
	class B_FieldPack_oucamo_AAR: B_Carryall_oucamo
	{
//		scope = 1;
	};
	class B_FieldPack_oucamo_Medic: B_FieldPack_oucamo
	{
//		scope = 1;
	};
	class B_FieldPack_oucamo_Ammo: B_Carryall_oucamo
	{
//		scope = 1;
	};
	class B_FieldPack_oucamo_Repair: B_FieldPack_oucamo
	{
//		scope = 1;
	};
	class B_Carryall_oucamo_Exp: B_Carryall_oucamo
	{
//		scope = 1;
	};
	class B_Carryall_oucamo_Eng: B_Carryall_oucamo
	{
//		scope = 1;
	};
	class B_Carryall_ocamo_AAR: B_Carryall_ocamo
	{
//		scope = 1;
	};
	class B_Carryall_oucamo_AAR: B_Carryall_oucamo
	{
//		scope = 1;
	};
	class I_Fieldpack_oli_AA: B_FieldPack_oli
	{
//		scope = 1;
	};
	class I_Assault_Diver: B_FieldPack_blk
	{
//		scope = 1;
	};
	class I_Fieldpack_oli_Ammo: B_Carryall_oli
	{
//		scope = 1;
	};
	class I_Fieldpack_oli_Medic: B_FieldPack_oli
	{
//		scope = 1;
	};
	class I_Fieldpack_oli_Repair: B_FieldPack_oli
	{
//		scope = 1;
	};
	class I_Fieldpack_oli_LAT: B_AssaultPack_dgtl
	{
//		scope = 1;
	};
	class I_Fieldpack_oli_AT: B_FieldPack_oli
	{
//		scope = 1;
	};
	class I_Fieldpack_oli_AAR: B_TacticalPack_oli
	{
//		scope = 1;
	};
	class I_Carryall_oli_AAT: B_Carryall_oli
	{
//		scope = 1;
	};
	class I_Carryall_oli_Exp: B_Carryall_oli
	{
//		scope = 1;
	};
	class I_Carryall_oli_AAA: B_Carryall_oli
	{
//		scope = 1;
	};
	class I_Carryall_oli_Eng: B_Carryall_oli
	{
//		scope = 1;
	};
	class G_TacticalPack_Eng: B_Carryall_oli
	{
//		scope = 1;
	};
	class G_FieldPack_Medic: B_TacticalPack_blk
	{
//		scope = 1;
	};
	class G_FieldPack_LAT: B_TacticalPack_blk
	{
//		scope = 1;
	};
	class G_Carryall_Ammo: B_Carryall_oli
	{
//		scope = 1;
	};
	class G_Carryall_Exp: B_Carryall_oli
	{
//		scope = 1;
	};
	class B_TacticalPack_oli_AAR: B_TacticalPack_oli
	{
//		scope = 1;
	};
	class B_BergenG_TEST_B_Soldier_overloaded: B_Carryall_oli
	{
//		scope = 1;
	};
	class ATMine: MineBase
	{
		scope = 1;//2
	};
	class APERSMine: MineBase
	{
		scope = 1;//2
	};
	class APERSBoundingMine: MineBase
	{
		scope = 1;//2
	};
	class SLAMDirectionalMine: MineBase
	{
		scope = 1;//2
	};
	class APERSTripMine: MineBase
	{
		scope = 1;//2
	};
	class UnderwaterMine: MineBase
	{
		scope = 1;//2
	};
	class UnderwaterMineAB: MineBase
	{
		scope = 1;//2
	};
	class UnderwaterMinePDM: MineBase
	{
		scope = 1;//2
	};
	class SatchelCharge_F: MineBase
	{
		scope = 1;//2
	};
	class DemoCharge_F: MineBase
	{
		scope = 1;//2
	};
	class Claymore_F: MineBase
	{
		scope = 1;//2
	};
	class IEDUrbanBig_F: MineBase
	{
		scope = 1;//2
	};
	class IEDLandBig_F: MineBase
	{
		scope = 1;//2
	};
	class IEDUrbanSmall_F: MineBase
	{
		scope = 1;//2
	};
	class IEDLandSmall_F: MineBase
	{
		scope = 1;//2
	};
	class Weapon_srifle_GM6_camo_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_srifle_LRR_camo_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_Scientist: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_Journalist: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_IG_Guerrilla_6_1: Item_Base_F
	{
//		scope = 1;
	};
	class Item_U_BG_Guerrilla_6_1: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_OG_Guerrilla_6_1: Item_Base_F
	{
//		scope = 1;
	};
	class Vest_V_Press_F: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_UAV_01_backpack_F: Weapon_Bag_Base
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Intel_File1_F: Land_File1_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Intel_File2_F: Land_File2_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Intel_Photos_F: Land_Photos_V3_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_hgun_Pistol_Signal_F: Pistol_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_Driver_1: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_Driver_2: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_Driver_3: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_Driver_4: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_Driver_1_black: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_Driver_1_blue: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_Driver_1_green: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_Driver_1_red: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_Driver_1_white: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_Driver_1_yellow: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_Driver_1_orange: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_Marshal: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_RacingHelmet_1_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_RacingHelmet_2_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_RacingHelmet_3_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_RacingHelmet_4_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_RacingHelmet_1_black_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_RacingHelmet_1_blue_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_RacingHelmet_1_green_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_RacingHelmet_1_red_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_RacingHelmet_1_white_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_RacingHelmet_1_yellow_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_RacingHelmet_1_orange_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Cap_marshal: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Box_FIA_Wps_F: FIA_Box_Base_F
	{
		scope = 1;//2
	};
	class Box_FIA_Ammo_F: FIA_Box_Base_F
	{
		scope = 1;//2
	};
	class Box_FIA_Support_F: FIA_Box_Base_F
	{
		scope = 1;//2
	};
	class B_Slingload_01_Ammo_F: Slingload_01_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_Slingload_01_Cargo_F: Slingload_01_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_Slingload_01_Fuel_F: Slingload_01_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_Slingload_01_Medevac_F: Slingload_01_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_Slingload_01_Repair_F: Slingload_01_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_srifle_DMR_02_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_srifle_DMR_02_camo_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_srifle_DMR_02_sniper_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_srifle_DMR_03_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_srifle_DMR_03_khaki_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_srifle_DMR_03_tan_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_srifle_DMR_03_multicam_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_srifle_DMR_03_woodland_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_srifle_DMR_04_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_srifle_DMR_04_Tan_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_srifle_DMR_05_blk_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_srifle_DMR_05_hex_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_srifle_DMR_05_tan_f: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_srifle_DMR_06_camo_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_srifle_DMR_06_olive_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_MMG_01_hex_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_MMG_01_tan_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_MMG_02_camo_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_MMG_02_black_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_MMG_02_sand_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_muzzle_snds_338_black: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_muzzle_snds_338_green: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_muzzle_snds_338_sand: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_muzzle_snds_93mmg: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_muzzle_snds_93mmg_tan: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_optic_AMS: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_optic_AMS_khk: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_optic_AMS_snd: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_optic_KHS_blk: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_optic_KHS_tan: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_optic_KHS_hex: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_optic_KHS_old: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_Laserdesignator_02: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_Laserdesignator_03: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_bipod_01_F_snd: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_bipod_01_F_blk: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_bipod_01_F_mtp: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_bipod_02_F_blk: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_bipod_02_F_tan: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_bipod_02_F_hex: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_bipod_03_F_blk: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_bipod_03_F_oli: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_B_FullGhillie_lsh: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_B_FullGhillie_sard: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_B_FullGhillie_ard: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_O_FullGhillie_lsh: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_O_FullGhillie_sard: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_O_FullGhillie_ard: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_I_FullGhillie_lsh: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_I_FullGhillie_sard: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_I_FullGhillie_ard: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_PlateCarrierGL_rgr: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_PlateCarrierGL_blk: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_PlateCarrierGL_mtp: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_PlateCarrierSpec_rgr: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_PlateCarrierSpec_blk: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_PlateCarrierSpec_mtp: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_PlateCarrierIAGL_dgtl: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_SMG_03_TR_black: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_SMG_03_TR_camo: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_SMG_03_TR_khaki: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_SMG_03_TR_hex: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_SMG_03C_TR_black: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_SMG_03C_TR_camo: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_SMG_03C_TR_khaki: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_SMG_03C_TR_hex: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_SMG_03_black: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_SMG_03_camo: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_SMG_03_khaki: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_SMG_03_hex: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_SMG_03C_black: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_SMG_03C_camo: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_SMG_03C_khaki: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_SMG_03C_hex: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_muzzle_snds_570: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_PlateCarrierIAGL_oli: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_Bergen_mcamo_F: B_Bergen_Base_F
	{
		scope = 1;//2
	};
	class B_Bergen_dgtl_F: B_Bergen_Base_F
	{
		scope = 1;//2
	};
	class B_Bergen_hex_F: B_Bergen_Base_F
	{
		scope = 1;//2
	};
	class B_Bergen_tna_F: B_Bergen_Base_F
	{
		scope = 1;//2
	};
	class B_AssaultPack_tna_F: B_AssaultPack_Base
	{
		scope = 1;//2
	};
	class B_Carryall_ghex_F: B_Carryall_Base
	{
		scope = 1;//2
	};
	class B_FieldPack_ghex_F: B_FieldPack_Base
	{
		scope = 1;//2
	};
	class B_ViperHarness_blk_F: B_ViperHarness_base_F
	{
		scope = 1;//2
	};
	class B_ViperHarness_ghex_F: B_ViperHarness_base_F
	{
		scope = 1;//2
	};
	class B_ViperHarness_hex_F: B_ViperHarness_base_F
	{
		scope = 1;//2
	};
	class B_ViperHarness_khk_F: B_ViperHarness_base_F
	{
		scope = 1;//2
	};
	class B_ViperHarness_oli_F: B_ViperHarness_base_F
	{
		scope = 1;//2
	};
	class B_ViperLightHarness_blk_F: B_ViperLightHarness_base_F
	{
		scope = 1;//2
	};
	class B_ViperLightHarness_ghex_F: B_ViperLightHarness_base_F
	{
		scope = 1;//2
	};
	class B_ViperLightHarness_hex_F: B_ViperLightHarness_base_F
	{
		scope = 1;//2
	};
	class B_ViperLightHarness_khk_F: B_ViperLightHarness_base_F
	{
		scope = 1;//2
	};
	class B_ViperLightHarness_oli_F: B_ViperLightHarness_base_F
	{
		scope = 1;//2
	};
	class B_Carryall_oli_BTAmmo_F: B_Carryall_oli
	{
//		scope = 1;
	};
	class B_Carryall_oli_BTAAA_F: B_Carryall_oli
	{
//		scope = 1;
	};
	class B_Carryall_oli_BTAAT_F: B_Carryall_oli
	{
//		scope = 1;
	};
	class B_AssaultPack_tna_BTMedic_F: B_AssaultPack_tna_F
	{
//		scope = 1;
	};
	class B_Kitbag_rgr_BTEng_F: B_Kitbag_rgr
	{
//		scope = 1;
	};
	class B_Kitbag_rgr_BTExp_F: B_Kitbag_rgr
	{
//		scope = 1;
	};
	class B_Kitbag_rgr_BTAA_F: B_Kitbag_rgr
	{
//		scope = 1;
	};
	class B_Kitbag_rgr_BTAT_F: B_Kitbag_rgr
	{
//		scope = 1;
	};
	class B_AssaultPack_tna_BTRepair_F: B_AssaultPack_tna_F
	{
//		scope = 1;
	};
	class B_AssaultPack_rgr_BTLAT_F: B_AssaultPack_rgr
	{
//		scope = 1;
	};
	class B_Kitbag_rgr_BTReconExp_F: B_Kitbag_rgr
	{
//		scope = 1;
	};
	class B_AssaultPack_rgr_BTReconMedic: B_AssaultPack_rgr
	{
//		scope = 1;
	};
	class B_HMG_01_support_grn_F: I_HMG_01_support_F
	{
//		scope = 1;
	};
	class B_Mortar_01_support_grn_F: I_Mortar_01_support_F
	{
//		scope = 1;
	};
	class B_GMG_01_Weapon_grn_F: I_GMG_01_weapon_F
	{
//		scope = 1;
	};
	class B_HMG_01_Weapon_grn_F: I_HMG_01_weapon_F
	{
//		scope = 1;
	};
	class B_Mortar_01_Weapon_grn_F: I_Mortar_01_weapon_F
	{
//		scope = 1;
	};
	class B_Kitbag_rgr_CTRGExp_F: B_Kitbag_rgr
	{
//		scope = 1;
	};
	class B_AssaultPack_rgr_CTRGMedic_F: B_AssaultPack_rgr
	{
//		scope = 1;
	};
	class B_AssaultPack_rgr_CTRGLAT_F: B_AssaultPack_rgr
	{
//		scope = 1;
	};
	class B_Carryall_ghex_OTAmmo_F: B_Carryall_ghex_F
	{
//		scope = 1;
	};
	class B_Carryall_ghex_OTAAR_AAR_F: B_Carryall_ghex_F
	{
//		scope = 1;
	};
	class B_Carryall_ghex_OTAAA_F: B_Carryall_ghex_F
	{
//		scope = 1;
	};
	class B_Carryall_ghex_OTAAT_F: B_Carryall_ghex_F
	{
//		scope = 1;
	};
	class B_FieldPack_ghex_OTMedic_F: B_FieldPack_ghex_F
	{
//		scope = 1;
	};
	class B_Carryall_ghex_OTEng_F: B_Carryall_ghex_F
	{
//		scope = 1;
	};
	class B_Carryall_ghex_OTExp_F: B_Carryall_ghex_F
	{
//		scope = 1;
	};
	class B_FieldPack_ghex_OTAA_F: B_FieldPack_ghex_F
	{
//		scope = 1;
	};
	class B_FieldPack_ghex_OTAT_F: B_FieldPack_ghex_F
	{
//		scope = 1;
	};
	class B_FieldPack_ghex_OTRepair_F: B_FieldPack_ghex_F
	{
//		scope = 1;
	};
	class B_FieldPack_ghex_OTLAT_F: B_FieldPack_ghex_F
	{
//		scope = 1;
	};
	class B_Carryall_ghex_OTReconExp_F: B_Carryall_ghex_F
	{
//		scope = 1;
	};
	class B_FieldPack_ghex_OTReconMedic_F: B_FieldPack_ghex_F
	{
//		scope = 1;
	};
	class B_FieldPack_ghex_OTRPG_AT_F: B_FieldPack_ghex_F
	{
//		scope = 1;
	};
	class B_ViperHarness_hex_TL_F: B_ViperHarness_hex_F
	{
//		scope = 1;
	};
	class B_ViperHarness_ghex_TL_F: B_ViperHarness_ghex_F
	{
//		scope = 1;
	};
	class B_ViperHarness_hex_Exp_F: B_ViperHarness_hex_F
	{
//		scope = 1;
	};
	class B_ViperHarness_ghex_Exp_F: B_ViperHarness_ghex_F
	{
//		scope = 1;
	};
	class B_ViperHarness_hex_Medic_F: B_ViperHarness_hex_F
	{
//		scope = 1;
	};
	class B_ViperHarness_ghex_Medic_F: B_ViperHarness_ghex_F
	{
//		scope = 1;
	};
	class B_ViperHarness_hex_M_F: B_ViperHarness_hex_F
	{
//		scope = 1;
	};
	class B_ViperHarness_ghex_M_F: B_ViperHarness_ghex_F
	{
//		scope = 1;
	};
	class B_ViperHarness_hex_LAT_F: B_ViperHarness_hex_F
	{
//		scope = 1;
	};
	class B_ViperHarness_ghex_LAT_F: B_ViperHarness_ghex_F
	{
//		scope = 1;
	};
	class B_ViperHarness_hex_JTAC_F: B_ViperHarness_hex_F
	{
//		scope = 1;
	};
	class B_ViperHarness_ghex_JTAC_F: B_ViperHarness_ghex_F
	{
//		scope = 1;
	};
	class B_Kitbag_rgr_Para_3_F: B_Kitbag_rgr
	{
//		scope = 1;
	};
	class B_Kitbag_cbr_Para_5_F: B_Kitbag_cbr
	{
//		scope = 1;
	};
	class B_Kitbag_rgr_Para_8_F: B_Kitbag_rgr
	{
//		scope = 1;
	};
	class B_FieldPack_cb_Bandit_3_F: B_FieldPack_cbr
	{
//		scope = 1;
	};
	class B_Kitbag_cbr_Bandit_2_F: B_Kitbag_cbr
	{
//		scope = 1;
	};
	class B_FieldPack_khk_Bandit_1_F: B_FieldPack_khk
	{
//		scope = 1;
	};
	class B_FieldPack_blk_Bandit_8_F: B_FieldPack_blk
	{
//		scope = 1;
	};
	class Box_NATO_Equip_F: ReammoBox_F
	{
		scope = 1;//2
	};
	class Box_NATO_Uniforms_F: ReammoBox_F
	{
		scope = 1;//2
	};
	class Box_T_NATO_Wps_F: Box_NATO_Wps_F
	{
		scope = 1;//2
	};
	class Box_T_East_Wps_F: Box_East_Wps_F
	{
		scope = 1;//2
	};
	class Box_T_East_Ammo_F: Box_East_Ammo_F
	{
		scope = 1;//2
	};
	class Box_T_East_WpsSpecial_F: Box_East_WpsSpecial_F
	{
		scope = 1;//2
	};
	class Box_Syndicate_Wps_F: ReammoBox_F
	{
		scope = 1;//2
	};
	class Box_Syndicate_Ammo_F: ReammoBox_F
	{
		scope = 1;//2
	};
	class Box_Syndicate_WpsLaunch_F: ReammoBox_F
	{
		scope = 1;//2
	};
	class Box_IED_Exp_F: ReammoBox_F
	{
		scope = 1;//2
	};
	class C_T_supplyCrate_F: ReammoBox_F
	{
		scope = 1;//2
	};
	class Weapon_arifle_MX_khk_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_MX_GL_khk_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_MX_SW_khk_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_MXC_khk_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_MXM_khk_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_AK12_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_AK12_GL_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_ARX_blk_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_ARX_ghex_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_ARX_hex_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_CTAR_blk_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_CTAR_hex_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_CTAR_ghex_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_CTAR_GL_blk_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_CTAR_GL_hex_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_CTAR_GL_ghex_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_CTARS_blk_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_CTARS_hex_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_CTARS_ghex_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_srifle_DMR_07_blk_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_srifle_DMR_07_hex_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_srifle_DMR_07_ghex_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_SPAR_01_blk_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_SPAR_01_khk_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_SPAR_01_snd_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_SPAR_01_GL_blk_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_SPAR_01_GL_khk_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_SPAR_01_GL_snd_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_SPAR_02_blk_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_SPAR_02_khk_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_SPAR_02_snd_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_SPAR_03_blk_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_SPAR_03_khk_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_SPAR_03_snd_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_srifle_LRR_tna_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_srifle_GM6_ghex_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_LMG_03_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_hgun_P07_khk_F: Pistol_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_launch_RPG7_F: Launcher_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_hgun_Pistol_01_F: Pistol_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_AKM_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_arifle_AKS_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_launch_B_Titan_tna_F: Launcher_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_launch_B_Titan_short_tna_F: Launcher_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_launch_O_Titan_ghex_F: Launcher_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_launch_O_Titan_short_ghex_F: Launcher_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_launch_RPG32_ghex_F: Launcher_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_SMG_05_F: Weapon_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Weapon_launch_MRAWS_olive_F: Launcher_Base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Weapon_launch_MRAWS_olive_rail_F: Launcher_Base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Weapon_launch_MRAWS_green_F: Launcher_Base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Weapon_launch_MRAWS_green_rail_F: Launcher_Base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Weapon_launch_MRAWS_sand_F: Launcher_Base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Weapon_launch_MRAWS_sand_rail_F: Launcher_Base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Weapon_launch_O_Vorona_brown_F: Launcher_Base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Weapon_launch_O_Vorona_green_F: Launcher_Base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Item_muzzle_snds_H_khk_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_muzzle_snds_m_khk_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_muzzle_snds_m_snd_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_muzzle_snds_B_khk_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_muzzle_snds_B_snd_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_muzzle_snds_H_MG_blk_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_muzzle_snds_H_MG_khk_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_muzzle_snds_65_TI_blk_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_muzzle_snds_65_TI_hex_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_muzzle_snds_65_TI_ghex_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_muzzle_snds_58_blk_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_muzzle_snds_58_wdm_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_optic_Arco_blk_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_optic_Arco_ghex_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_optic_DMS_ghex_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_optic_ERCO_blk_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_optic_ERCO_khk_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_optic_ERCO_snd_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_optic_Hamr_khk_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_optic_SOS_khk_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_optic_LRPS_tna_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_optic_LRPS_ghex_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_optic_Holosight_blk_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_optic_Holosight_khk_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_optic_Holosight_smg_blk_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_bipod_01_F_khk: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_Laserdesignator_01_khk_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_Laserdesignator_02_ghex_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_NVGoggles_tna_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_O_NVGoggles_hex_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_O_NVGoggles_urb_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_O_NVGoggles_ghex_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_NVGogglesB_blk_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_NVGogglesB_grn_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_NVGogglesB_gry_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_B_T_Soldier_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_B_T_Soldier_AR_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_B_T_Soldier_SL_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_B_T_Sniper_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_B_T_FullGhillie_tna_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_B_CTRG_Soldier_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_B_CTRG_Soldier_2_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_B_CTRG_Soldier_3_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_B_GEN_Soldier_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_B_GEN_Commander_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_O_T_Soldier_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_O_T_Officer_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_O_T_Sniper_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_O_T_FullGhillie_tna_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_O_V_Soldier_Viper_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_O_V_Soldier_Viper_hex_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_I_C_Soldier_Para_1_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_I_C_Soldier_Para_2_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_I_C_Soldier_Para_3_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_I_C_Soldier_Para_4_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_I_C_Soldier_Para_5_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_I_C_Soldier_Bandit_1_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_I_C_Soldier_Bandit_2_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_I_C_Soldier_Bandit_3_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_I_C_Soldier_Bandit_4_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_I_C_Soldier_Bandit_5_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_I_C_Soldier_Camo_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_man_sport_1_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_man_sport_2_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_man_sport_3_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_Man_casual_1_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_Man_casual_2_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_Man_casual_3_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_Man_casual_4_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_Man_casual_5_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_Man_casual_6_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_B_CTRG_Soldier_urb_1_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_B_CTRG_Soldier_urb_2_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_B_CTRG_Soldier_urb_3_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetB_tna_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetB_Enh_tna_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetB_Light_tna_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetB_TI_tna_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetSpecO_ghex_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetCrew_O_ghex_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetLeaderO_ghex_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetO_ghex_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetO_ViperSP_hex_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HelmetO_ViperSP_ghex_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Helmet_Skate: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_MilCap_tna_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_MilCap_ghex_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Booniehat_tna_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Beret_gen_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_MilCap_gen_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_TacChestrig_grn_F: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_TacChestrig_oli_F: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_TacChestrig_cbr_F: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_PlateCarrier1_tna_F: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_PlateCarrierSpec_tna_F: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_PlateCarrierGL_tna_F: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_HarnessO_ghex_F: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_HarnessOGL_ghex_F: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_BandollierB_ghex_F: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_PlateCarrier1_rgr_noflag_F: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_PlateCarrier2_rgr_noflag_F: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_TacVest_gen_F: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_DeckCrew_yellow_F: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_DeckCrew_blue_F: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_DeckCrew_green_F: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_DeckCrew_red_F: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_DeckCrew_white_F: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_DeckCrew_brown_F: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_DeckCrew_violet_F: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_Patrol_Medic_bag_F: B_Kitbag_cbr
	{
//		scope = 1;
	};
	class B_Patrol_Leader_bag_F: B_Kitbag_rgr
	{
//		scope = 1;
	};
	class B_Patrol_Supply_bag_F: B_Kitbag_mcamo
	{
//		scope = 1;
	};
	class B_Patrol_Launcher_bag_F: B_AssaultPack_rgr
	{
//		scope = 1;
	};
	class Item_C_UavTerminal: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_IDAP_Man_Casual_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_IDAP_Man_shorts_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_IDAP_Man_cargo_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_IDAP_Man_tee_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_IDAP_Man_teeshorts_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_IDAP_Man_jeans_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_Paramedic_01_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_Mechanic_01_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_BG_Guerilla1_2_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_ConstructionCoverall_Red_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_ConstructionCoverall_Vrana_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_ConstructionCoverall_Black_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_C_ConstructionCoverall_Blue_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Hat_Safari_sand_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Hat_Safari_olive_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Construction_basic_yellow_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Construction_basic_white_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Construction_basic_orange_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Construction_basic_red_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Construction_basic_vrana_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Construction_basic_black_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Construction_earprot_yellow_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Construction_earprot_white_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Construction_earprot_orange_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Construction_earprot_red_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Construction_earprot_vrana_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Construction_earprot_black_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Construction_headset_yellow_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Construction_headset_white_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Construction_headset_orange_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Construction_headset_red_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Construction_headset_vrana_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Construction_headset_black_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_EarProtectors_yellow_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_EarProtectors_white_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_EarProtectors_orange_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_EarProtectors_red_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_EarProtectors_black_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HeadSet_yellow_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HeadSet_white_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HeadSet_orange_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HeadSet_red_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HeadSet_black_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_PASGT_basic_blue_press_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_PASGT_basic_blue_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_PASGT_basic_white_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_PASGT_basic_olive_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_PASGT_basic_black_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_PASGT_neckprot_blue_press_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HeadBandage_stained_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HeadBandage_clean_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_HeadBandage_bloody_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Cap_White_IDAP_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Cap_Orange_IDAP_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Cap_Black_IDAP_F: Headgear_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Headgear_H_Tank_black_F: Headgear_Base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Headgear_H_WirelessEarpiece_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_Plain_medical_F: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_Plain_crystal_F: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_Pocketed_olive_F: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_Pocketed_coyote_F: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_Pocketed_black_F: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_Safety_yellow_F: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_Safety_orange_F: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_Safety_blue_F: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_LegStrapBag_black_F: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_LegStrapBag_coyote_F: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_LegStrapBag_olive_F: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_EOD_blue_F: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_EOD_olive_F: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_EOD_coyote_F: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Vest_V_EOD_IDAP_blue_F: Vest_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_O_officer_noInsignia_hex_F: Item_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Item_U_Tank_green_F: Item_Base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class C_IDAP_UAV_01_backpack_F: Weapon_Bag_Base
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_UAV_06_backpack_F: UAV_06_backpack_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_UAV_06_backpack_F: UAV_06_backpack_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_UAV_06_backpack_F: UAV_06_backpack_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_IDAP_UAV_06_backpack_F: UAV_06_backpack_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_UAV_06_backpack_F: UAV_06_backpack_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_IDAP_UAV_06_antimine_backpack_F: C_IDAP_UAV_06_backpack_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_UAV_06_medical_backpack_F: UAV_06_medical_backpack_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_UAV_06_medical_backpack_F: UAV_06_medical_backpack_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_UAV_06_medical_backpack_F: UAV_06_medical_backpack_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_IDAP_UAV_06_medical_backpack_F: UAV_06_medical_backpack_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_UAV_06_medical_backpack_F: UAV_06_medical_backpack_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_LegStrapBag_black_F: B_LegStrapBag_base_F
	{
		scope = 1;//2
	};
	class B_LegStrapBag_coyote_F: B_LegStrapBag_base_F
	{
		scope = 1;//2
	};
	class B_LegStrapBag_olive_F: B_LegStrapBag_base_F
	{
		scope = 1;//2
	};
	class B_LegStrapBag_black_repair_F: B_LegStrapBag_black_F
	{
//		scope = 1;
	};
	class B_LegStrapBag_coyote_repair_F: B_LegStrapBag_coyote_F
	{
//		scope = 1;
	};
	class B_LegStrapBag_olive_repair_F: B_LegStrapBag_olive_F
	{
//		scope = 1;
	};
	class B_Carryall_oucamo_Repair: B_Carryall_oucamo
	{
//		scope = 1;
	};
	class B_Kitbag_rgr_Mine: B_Kitbag_rgr
	{
//		scope = 1;
	};
	class B_Carryall_khk_Mine: B_Carryall_khk
	{
//		scope = 1;
	};
	class B_Carryall_oli_Mine: B_Carryall_oli
	{
//		scope = 1;
	};
	class B_Carryall_ocamo_Mine: B_Carryall_ocamo
	{
//		scope = 1;
	};
	class B_Carryall_ghex_Mine: B_Carryall_ghex_F
	{
//		scope = 1;
	};
	class B_FieldPack_cbr_Ammo_F: B_FieldPack_cbr_Ammo
	{
//		scope = 1;
	};
	class B_AssaultPack_ocamo_Medic_F: B_AssaultPack_ocamo
	{
//		scope = 1;
	};
	class B_FieldPack_ocamo_LAT_F: B_FieldPack_ocamo
	{
//		scope = 1;
	};
	class B_TacticalPack_ocamo_AA_F: B_TacticalPack_ocamo
	{
//		scope = 1;
	};
	class B_TacticalPack_ocamo_AT_F: B_TacticalPack_ocamo
	{
//		scope = 1;
	};
	class C_IDAP_supplyCrate_F: B_supplyCrate_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_Messenger_Coyote_F: B_Messenger_Base_F
	{
		scope = 1;//2
	};
	class B_Messenger_Olive_F: B_Messenger_Base_F
	{
		scope = 1;//2
	};
	class B_Messenger_Black_F: B_Messenger_Base_F
	{
		scope = 1;//2
	};
	class B_Messenger_Gray_F: B_Messenger_Base_F
	{
		scope = 1;//2
	};
	class B_Messenger_Gray_Medical_F: B_Messenger_Gray_F
	{
//		scope = 1;
	};
	class B_Messenger_IDAP_F: B_Messenger_Base_F
	{
		scope = 1;//2
	};
	class B_Messenger_IDAP_Medical_F: B_Messenger_IDAP_F
	{
//		scope = 1;
	};
	class B_Messenger_IDAP_TrainingMines_F: B_Messenger_IDAP_F
	{
//		scope = 1;
	};
	class C_IDAP_CargoNet_01_supplies_F: CargoNet_01_ammo_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Box_B_UAV_06_F: Box_UAV_06_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Box_O_UAV_06_F: Box_UAV_06_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Box_I_UAV_06_F: Box_UAV_06_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Box_C_IDAP_UAV_06_F: Box_UAV_06_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Box_C_UAV_06_F: Box_UAV_06_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Box_C_UAV_06_Swifd_F: Box_UAV_06_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Box_B_UAV_06_medical_F: Box_UAV_06_medical_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Box_O_UAV_06_medical_F: Box_UAV_06_medical_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Box_I_UAV_06_medical_F: Box_UAV_06_medical_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Box_C_IDAP_UAV_06_medical_F: Box_UAV_06_medical_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Box_C_UAV_06_medical_F: Box_UAV_06_medical_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
};
