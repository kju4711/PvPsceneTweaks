﻿class CfgPatches
{
	class Optional_HideMPMissions_c
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Data_F_Loadorder","A3_Data_F_Curator_Loadorder","A3_Data_F_Kart_Loadorder","A3_Data_F_Bootcamp_Loadorder","A3_Data_F_Heli_Loadorder","A3_Data_F_Mark_Loadorder","A3_Data_F_Exp_A_Loadorder","A3_Data_F_Exp_B_Loadorder","A3_Data_F_Exp_Loadorder","A3_Data_F_Jets_Loadorder","A3_Data_F_Argo_Loadorder","A3_Data_F_Patrol_Loadorder","A3_Data_F_Orange_Loadorder","A3_Data_F_Tacops_Loadorder","A3_Data_F_Tank_Loadorder"};
		version = "2019-03-28";
	};
};
class CfgMissions
{
	class MPMissions
	{
		delete MP_COOP_m01;
		delete MP_COOP_m02;
		delete MP_COOP_m04;
		delete Showcase_Combined_arms;
		delete MP_COOP_m03;
		delete MP_COOP_m05;
		delete MP_COOP_m06;
		delete MP_COOP_m07;
		delete MP_COOP_m08;
		delete MP_ZSC_m10;
		delete MP_ZGM_m11;
		delete MP_ZGM_m11_EAST;
		delete MP_ZGM_m11_WEST;
		delete MP_ZGM_m11_GUER;
		delete MP_ZGM_m12;
		delete MP_ZGM_m12_EAST;
		delete MP_ZGM_m12_WEST;
		delete MP_ZGM_m12_GUER;
		delete MP_ZR_8_Karts01;
		delete MP_ZR_8_Karts02;
		delete MP_Bootcamp_01;
		delete MP_ZGM_m13;
		delete MP_ZGM_m13_EAST;
		delete MP_ZGM_m13_WEST;
		delete MP_ZGM_m13_GUER;
		delete MP_GroundSupport01;
		delete MP_GroundSupport02;
		delete MP_GroundSupport03;
		delete MP_GroundSupport04;
		delete MP_GroundSupport05;
		delete Showcase_FiringFromVehicles;
		delete MP_Marksmen_01;
		delete MP_End_Game_02;
		delete MP_End_Game_03;
		delete Apex;
		delete MP_ZGM_m14;
		delete MP_ZGM_m14_EAST;
		delete MP_ZGM_m14_WEST;
		delete MP_ZGM_m14_GUER;
		delete MP_End_Game_04;
		delete MP_End_Game_05;
		delete EscapeFromTanoa;
		delete MP_CombatPatrol_01;
		delete MP_CombatPatrol_02;
		delete MP_CombatPatrol_03;
		delete MP_CombatPatrol_04;
		delete EscapeFromMalden;
		delete MP_ZGM_m15;
		delete MP_ZGM_m15_EAST;
		delete MP_ZGM_m15_WEST;
		delete MP_ZGM_m15_GUER;
		delete MP_VANGUARD_TANK_Syrta;
		delete MP_VANGUARD_APC_Airport;
		delete MP_VANGUARD_LV_Chapoi;
		delete MP_Warlords_official_01_large;
		delete MP_Warlords_official_01a;
		delete MP_Warlords_official_02_large;
		delete MP_Warlords_official_04_large;
		delete MP_Warlords_official_01;
		delete MP_Warlords_official_01b;
		delete MP_Warlords_official_02;
		delete MP_Warlords_official_03;
		delete MP_Warlords_official_03a;
		delete MP_Warlords_official_04;
	};
};
