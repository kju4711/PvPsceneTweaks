class CfgPatches
{
	class BlackBackgroundForMenus_Visuals_C_PvPscene
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Data_F_Loadorder","A3_Data_F_Curator_Loadorder","A3_Data_F_Kart_Loadorder","A3_Data_F_Bootcamp_Loadorder","A3_Data_F_Heli_Loadorder","A3_Data_F_Mark_Loadorder","A3_Data_F_Exp_A_Loadorder","A3_Data_F_Exp_B_Loadorder","A3_Data_F_Exp_Loadorder","A3_Data_F_Jets_Loadorder","A3_Data_F_Argo_Loadorder","A3_Data_F_Patrol_Loadorder","A3_Data_F_Orange_Loadorder","A3_Data_F_Tacops_Loadorder","A3_Data_F_Tank_Loadorder"};
		version = "2019-03-28";
	};
};
class RscStandardDisplay;
class RscMap;
class RscText;
class RscPicture;
class RscDisplayMain: RscStandardDisplay
{
	enableSimulation = 0;
	class controlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplaySingleplayer
{
	enableSimulation = 0;
	class controlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplaySingleMission: RscStandardDisplay
{
	enableSimulation = 0;
	class controlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayCampaignSelect: RscStandardDisplay
{
	enableSimulation = 0;
	class controlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayCampaignLoad: RscStandardDisplay
{
	enableSimulation = 0;
	class controlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayMultiplayer: RscStandardDisplay
{
	enableSimulation = 0;
	class controlsBackground
	{
		class MainBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayQuickPlay: RscStandardDisplay
{
	enableSimulation = 0;
	class controlsbackground
	{
		class MainBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayIPAddress: RscStandardDisplay
{
	enableSimulation = 0;
	class controlsbackground
	{
		class MainBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
//		class PvPscene_BlackBack: RscText
//		{
//			x = "SafeZoneX";
//			y = "SafeZoneY";
//			w = "SafeZoneW";
//			h = "SafeZoneH";
//			colorBackground[] = {0,0,0,1};
//			text = "";
//		};
	};
};
class RscDisplaySelectIsland: RscStandardDisplay
{
	enableSimulation = 0;
	class controlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscArmorySelectIsland: RscStandardDisplay
{
	enableSimulation = 0;
	class controlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayOptions
{
	class controlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayOptionsVideo
{
	enableSimulation = 0;
	class controlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayOptionsAudio
{
	class controlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayConfigure
{
	class controlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayConfigureControllers
{
	class controlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayCustomizeController
{
	class controlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayGameOptions
{
	class controlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplaySelectDifficulty: RscStandardDisplay
{
	enableSimulation = 0;
	class controlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayDifficulty
{
	class controlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayDifficultySelect
{
	class controlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayConfigureAction
{
	class ControlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayHostSettings: RscStandardDisplay
{
	enableSimulation = 0;
	class controlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayFilter: RscStandardDisplay
{
	enableSimulation = 0;
	class controlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayRemoteMissions: RscStandardDisplay
{
	enableSimulation = 0;
	class controlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayMultiplayerSetup: RscStandardDisplay
{
	enableSimulation = 0;
	class controlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayDSinterface: RscStandardDisplay
{
	enableSimulation = 0;
	class controlsBackground
	{
		class Mainback: RscPicture
		{
			text = "#(argb,8,8,3)color(0,0,0,1)";
		};
	};
};
class RscDisplayXWizardTemplate: RscStandardDisplay
{
	enableSimulation = 0;
	class controlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayXWizardIntel: RscStandardDisplay
{
	enableSimulation = 0;
	class controlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayXWizardIntelName: RscStandardDisplay
{
	enableSimulation = 0;
	class controlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayXWizardIntelIsland: RscStandardDisplay
{
	enableSimulation = 0;
	class controlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayXWizardUnitSelect: RscStandardDisplay
{
	enableSimulation = 0;
	class controlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayXWizardUnit: RscStandardDisplay
{
	enableSimulation = 0;
	class controlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayXWizardIntelWeather: RscStandardDisplay
{
	enableSimulation = 0;
	class controlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayXWizardIntelTime: RscStandardDisplay
{
	enableSimulation = 0;
	class controlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayXWizardParams: RscStandardDisplay
{
	enableSimulation = 0;
	class controlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayInterrupt: RscStandardDisplay
{
	enableSimulation = 0;
	class controlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayMovieInterrupt: RscStandardDisplay
{
	enableSimulation = 0;
	class controlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayMissionEnd: RscStandardDisplay
{
	enableSimulation = 0;
	class controlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayMultiplayerSetupParams: RscStandardDisplay
{
	enableSimulation = 0;
	class controlsBackground
	{
		delete ParamsBack;
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayMultiplayerSetupParameter: RscStandardDisplay
{
	enableSimulation = 0;
	class controlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayLogin: RscStandardDisplay
{
	enableSimulation = 0;
	class controlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayNewUser: RscStandardDisplay
{
	enableSimulation = 0;
	class controlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayModLauncher: RscStandardDisplay
{
	enableSimulation = 0;
	class controlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayDebriefing: RscStandardDisplay
{
	enableSimulation = 0;
	class ControlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayMissionFail: RscStandardDisplay
{
	enableSimulation = 0;
	class ControlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
class RscDisplayArtillery: RscStandardDisplay{};
class RscDisplayTeamSwitch: RscStandardDisplay{};
class RscDisplayMPInterrupt: RscStandardDisplay
{
	enableSimulation = 0;
};
class RscDisplayInteruptReceiving: RscStandardDisplay
{
	enableSimulation = 0;
	class controlsBackground
	{
		class PvPscene_BlackBack: RscText
		{
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			colorBackground[] = {0,0,0,1};
			text = "";
		};
	};
};
