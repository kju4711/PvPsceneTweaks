﻿class CfgPatches
{
	class Optional_HideFromEditor_Groups_c
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Data_F_Loadorder","A3_Data_F_Curator_Loadorder","A3_Data_F_Kart_Loadorder","A3_Data_F_Bootcamp_Loadorder","A3_Data_F_Heli_Loadorder","A3_Data_F_Mark_Loadorder","A3_Data_F_Exp_A_Loadorder","A3_Data_F_Exp_B_Loadorder","A3_Data_F_Exp_Loadorder","A3_Data_F_Jets_Loadorder","A3_Data_F_Argo_Loadorder","A3_Data_F_Patrol_Loadorder","A3_Data_F_Orange_Loadorder","A3_Data_F_Tacops_Loadorder","A3_Data_F_Tank_Loadorder"};
		version = "2019-03-28";
	};
};
class CfgGroups
{
	class West
	{
		delete BLU_F;
		delete Guerilla;
		delete BLU_T_F;
		delete BLU_CTRG_F;
		delete Gendarmerie;
//		class BLU_F
//		{
//			class Infantry
//			{
//				delete BUS_InfSquad;
//				delete BUS_InfSquad_Weapons;
//				delete BUS_InfTeam;
//				delete BUS_InfTeam_AT;
//				delete BUS_InfTeam_AA;
//				delete BUS_InfSentry;
//				delete BUS_ReconTeam;
//				delete BUS_ReconPatrol;
//				delete BUS_ReconSentry;
//				delete BUS_SniperTeam;
//				delete BUS_InfAssault;
//				delete BUS_ReconSquad;
//				delete B_InfTeam_Light;
//			};
//			class SpecOps
//			{
//				delete BUS_DiverTeam;
//				delete BUS_DiverTeam_Boat;
//				delete BUS_DiverTeam_SDV;
//				delete BUS_SmallTeam_UAV;
//				delete BUS_ReconTeam_UGV;
//				delete BUS_AttackTeam_UGV;
//				delete BUS_ReconTeam_UAV;
//				delete BUS_AttackTeam_UAV;
//			};
//			class Support
//			{
//				delete BUS_Support_CLS;
//				delete BUS_Support_EOD;
//				delete BUS_Support_ENG;
//				delete BUS_Recon_EOD;
//				delete BUS_Support_MG;
//				delete BUS_Support_GMG;
//				delete BUS_Support_Mort;
//			};
//			class Motorized
//			{
//				delete BUS_MotInf_Team;
//				delete BUS_MotInf_AT;
//				delete BUS_MotInf_AA;
//				delete BUS_MotInf_MGTeam;
//				delete BUS_MotInf_GMGTeam;
//				delete BUS_MotInf_MortTeam;
//				delete BUS_MotInf_Reinforce;
//			};
//			class Mechanized
//			{
//				delete BUS_MechInfSquad;
//				delete BUS_MechInf_AT;
//				delete BUS_MechInf_AA;
//				delete BUS_MechInf_Support;
//			};
//			class Armored
//			{
//				delete BUS_TankPlatoon;
//				delete BUS_TankPlatoon_AA;
//				delete BUS_TankSection;
//				delete BUS_SPGPlatoon_Scorcher;
//				delete BUS_SPGSection_Scorcher;
//				delete BUS_SPGSection_MLRS;
//				delete B_TankDestrSection_Rhino;
//				delete B_TankDestrSection_RhinoUP;
//			};
//		};
//		class Guerilla
//		{
//			class Infantry
//			{
//				delete IRG_InfSquad;
//				delete IRG_InfSquad_Weapons;
//				delete IRG_InfTeam;
//				delete IRG_InfTeam_AT;
//				delete IRG_InfSentry;
//				delete IRG_ReconSentry;
//				delete IRG_SniperTeam_M;
//				delete IRG_InfAssault;
//				delete B_G_InfSquad_Assault;
//				delete B_G_InfTeam_Light;
//			};
//			class Support
//			{
//				delete IRG_Support_CLS;
//				delete IRG_Support_EOD;
//				delete IRG_Support_ENG;
//			};
//			class Motorized_MTP
//			{
//				delete IRG_MotInf_Team;
//				delete IRG_Technicals;
//			};
//		};
//		class BLU_T_F
//		{
//			class Infantry
//			{
//				delete B_T_InfSquad;
//				delete B_T_InfSquad_Weapons;
//				delete B_T_InfTeam;
//				delete B_T_InfTeam_AT;
//				delete B_T_InfTeam_AA;
//				delete B_T_InfSentry;
//				delete B_T_ReconTeam;
//				delete B_T_ReconPatrol;
//				delete B_T_ReconSentry;
//				delete B_T_SniperTeam;
//				delete B_T_InfTeam_Light;
//			};
//			class SpecOps
//			{
//				delete B_T_DiverTeam;
//				delete B_T_DiverTeam_Boat;
//				delete B_T_DiverTeam_SDV;
//				delete B_T_SmallTeam_UAV;
//				delete B_T_ReconTeam_UGV;
//				delete B_T_AttackTeam_UGV;
//				delete B_T_ReconTeam_UAV;
//				delete B_T_AttackTeam_UAV;
//			};
//			class Support
//			{
//				delete B_T_Support_CLS;
//				delete B_T_Support_EOD;
//				delete B_T_Support_ENG;
//				delete B_T_Recon_EOD;
//				delete B_T_Support_MG;
//				delete B_T_Support_GMG;
//				delete B_T_Support_Mort;
//			};
//			class Motorized
//			{
//				delete B_T_MotInf_Team;
//				delete B_T_MotInf_AT;
//				delete B_T_MotInf_AA;
//				delete B_T_MotInf_MGTeam;
//				delete B_T_MotInf_GMGTeam;
//				delete B_T_MotInf_MortTeam;
//				delete B_T_MotInf_Reinforcements;
//			};
//			class Mechanized
//			{
//				delete B_T_MechInfSquad;
//				delete B_T_MechInf_AT;
//				delete B_T_MechInf_AA;
//				delete B_T_MechInf_Support;
//			};
//			class Armored
//			{
//				delete B_T_TankPlatoon;
//				delete B_T_TankPlatoon_AA;
//				delete B_T_TankSection;
//				delete B_T_SPGPlatoon_Scorcher;
//				delete B_T_SPGSection_Scorcher;
//				delete B_T_SPGSection_MLRS;
//				delete B_T_TankDestrSection_Rhino;
//				delete B_T_TankDestrSection_RhinoUP;
//			};
//		};
//		class BLU_CTRG_F
//		{
//			class Infantry
//			{
//				delete CTRG_InfSquad;
//				delete CTRG_InfTeam;
//				delete CTRG_InfSentry;
//				delete B_CTRG_InfTeam_Light;
//			};
//			class Motorized
//			{
//				delete CTRG_MotInf_ReconTeam;
//				delete CTRG_MotInf_AssaultTeam;
//			};
//		};
//		class Gendarmerie
//		{
//			class Infantry
//			{
//				delete GENDARME_Inf_Patrol;
//			};
//			class Motorized
//			{
//				delete GENDARME_MotInf_Patrol;
//			};
//		};
	};
	class East
	{
		delete OPF_F;
		delete OPF_T_F;
		delete OPF_G_F;
//		class OPF_F
//		{
//			class Infantry
//			{
//				delete OIA_InfSquad;
//				delete OIA_InfSquad_Weapons;
//				delete OIA_InfTeam;
//				delete OIA_InfTeam_AT;
//				delete OIA_InfTeam_AA;
//				delete OIA_InfSentry;
//				delete OI_reconTeam;
//				delete OI_reconPatrol;
//				delete OI_reconSentry;
//				delete OI_SniperTeam;
//				delete OIA_InfAssault;
//				delete OIA_ReconSquad;
//				delete O_InfTeam_AT_Heavy;
//			};
//			class UInfantry
//			{
//				delete OIA_GuardSquad;
//				delete OIA_GuardTeam;
//				delete OIA_GuardSentry;
//			};
//			class SpecOps
//			{
//				delete OI_diverTeam;
//				delete OI_diverTeam_Boat;
//				delete OI_diverTeam_SDV;
//				delete OI_SmallTeam_UAV;
//				delete OI_ReconTeam_UGV;
//				delete OI_AttackTeam_UGV;
//				delete OI_ReconTeam_UAV;
//				delete OI_AttackTeam_UAV;
//				delete OI_ViperTeam;
//			};
//			class Support
//			{
//				delete OI_support_CLS;
//				delete OI_support_EOD;
//				delete OI_support_ENG;
//				delete OI_recon_EOD;
//				delete OI_support_MG;
//				delete OI_support_GMG;
//				delete OI_support_Mort;
//			};
//			class Motorized_MTP
//			{
//				delete OIA_MotInf_Team;
//				delete OIA_MotInf_AT;
//				delete OIA_MotInf_AA;
//				delete OIA_MotInf_MGTeam;
//				delete OIA_MotInf_GMGTeam;
//				delete OIA_MotInf_MortTeam;
//				delete OIA_MotInf_Reinforce;
//				delete O_MotInf_ReconViperTeam;
//				delete O_MotInf_AssaultViperTeam;
//			};
//			class Mechanized
//			{
//				delete OIA_MechInfSquad;
//				delete OIA_MechInf_AT;
//				delete OIA_MechInf_AA;
//				delete OIA_MechInf_Support;
//			};
//			class Armored
//			{
//				delete OIA_TankPlatoon;
//				delete OIA_TankPlatoon_AA;
//				delete OIA_TankSection;
//				delete OIA_SPGPlatoon_Scorcher;
//				delete OIA_SPGSection_Scorcher;
//				delete O_TankSection_Heavy;
//				delete O_TankPlatoon_Heavy;
//			};
//		};
//		class OPF_T_F
//		{
//			class Infantry
//			{
//				delete O_T_InfSquad;
//				delete O_T_InfSquad_Weapons;
//				delete O_T_InfTeam;
//				delete O_T_InfTeam_AT;
//				delete O_T_InfTeam_AA;
//				delete O_T_InfSentry;
//				delete O_T_reconTeam;
//				delete O_T_reconPatrol;
//				delete O_T_reconSentry;
//				delete O_T_SniperTeam;
//				delete O_T_InfTeam_AT_Heavy;
//			};
//			class SpecOps
//			{
//				delete O_T_diverTeam;
//				delete O_T_diverTeam_Boat;
//				delete O_T_diverTeam_SDV;
//				delete O_T_SmallTeam_UAV;
//				delete O_T_ReconTeam_UGV;
//				delete O_T_AttackTeam_UGV;
//				delete O_T_ReconTeam_UAV;
//				delete O_T_AttackTeam_UAV;
//				delete O_T_ViperTeam;
//			};
//			class Support
//			{
//				delete O_T_support_CLS;
//				delete O_T_support_EOD;
//				delete O_T_support_ENG;
//				delete O_T_recon_EOD;
//				delete O_T_support_MG;
//				delete O_T_support_GMG;
//				delete O_T_support_Mort;
//			};
//			class Motorized_MTP
//			{
//				delete O_T_MotInf_Team;
//				delete O_T_MotInf_AT;
//				delete O_T_MotInf_AA;
//				delete O_T_MotInf_MGTeam;
//				delete O_T_MotInf_GMGTeam;
//				delete O_T_MotInf_MortTeam;
//				delete O_T_MotInf_ReconViperTeam;
//				delete O_T_MotInf_AssaultViperTeam;
//				delete O_T_MotInf_Reinforcements;
//			};
//			class Mechanized
//			{
//				delete O_T_MechInfSquad;
//				delete O_T_MechInf_AT;
//				delete O_T_MechInf_AA;
//				delete O_T_MechInf_Support;
//			};
//			class Armored
//			{
//				delete O_T_TankPlatoon;
//				delete O_T_TankPlatoon_AA;
//				delete O_T_TankSection;
//				delete O_T_SPGPlatoon_Scorcher;
//				delete O_T_SPGSection_Scorcher;
//				delete O_T_TankSection_Heavy;
//				delete O_T_TankPlatoon_Heavy;
//			};
//		};
//		class OPF_G_F
//		{
//			class Infantry
//			{
//				delete O_G_InfSquad_Assault;
//				delete O_G_InfTeam_Light;
//			};
//		};
	};
	class Indep
	{
		delete IND_F;
		delete IND_C_F;
		delete IND_G_F;
//		class IND_F
//		{
//			class Infantry
//			{
//				delete HAF_InfSquad;
//				delete HAF_InfSquad_Weapons;
//				delete HAF_InfTeam;
//				delete HAF_InfTeam_AT;
//				delete HAF_InfTeam_AA;
//				delete HAF_InfSentry;
//				delete HAF_SniperTeam;
//				delete I_InfTeam_Light;
//			};
//			class SpecOps
//			{
//				delete HAF_DiverTeam;
//				delete HAF_DiverTeam_Boat;
//				delete HAF_DiverTeam_SDV;
//				delete HAF_SmallTeam_UAV;
//				delete HAF_ReconTeam_UGV;
//				delete HAF_AttackTeam_UGV;
//				delete HAF_ReconTeam_UAV;
//				delete HAF_AttackTeam_UAV;
//			};
//			class Support
//			{
//				delete HAF_Support_CLS;
//				delete HAF_Support_EOD;
//				delete HAF_Support_ENG;
//				delete HAF_Support_MG;
//				delete HAF_Support_GMG;
//				delete HAF_Support_Mort;
//			};
//			class Motorized
//			{
//				delete HAF_MotInf_Team;
//				delete HAF_MotInf_AT;
//				delete HAF_MotInf_AA;
//				delete HAF_MotInf_MGTeam;
//				delete HAF_MotInf_GMGTeam;
//				delete HAF_MotInf_MortTeam;
//				delete HAF_MotInf_Reinforce;
//			};
//			class Mechanized
//			{
//				delete HAF_MechInfSquad;
//				delete HAF_MechInf_AT;
//				delete HAF_MechInf_AA;
//				delete HAF_MechInf_Support;
//			};
//			class Armored
//			{
//				delete HAF_TankPlatoon;
//				delete HAF_TankPlatoon_AA;
//				delete HAF_TankSection;
//				delete I_LTankSection_Assault;
//				delete I_LTankSection_Recon;
//				delete I_LTankSection_AA;
//				delete I_LTankSection_AT;
//				delete I_LTankPlatoon_combined;
//				delete I_LTankPlatoon_AA;
//			};
//		};
//		class IND_C_F
//		{
//			class Infantry
//			{
//				delete ParaFireTeam;
//				delete ParaShockTeam;
//				delete ParaCombatGroup;
//				delete BanditFireTeam;
//				delete BanditShockTeam;
//				delete BanditCombatGroup;
//			};
//		};
//		class IND_G_F
//		{
//			class Infantry
//			{
//				delete I_G_InfSquad_Assault;
//				delete I_G_InfTeam_Light;
//			};
//		};
	};
	class Empty
	{
		class Military
		{
			class Outposts
			{
				delete OutpostA;
				delete OutpostB;
				delete OutpostC;
				delete OutpostD;
				delete OutpostE;
				delete OutpostF;
			};
			class RoadBlocks
			{
				delete RoadBlock1;
				delete RoadBlock2;
			};
		};
		class Guerrilla
		{
			class Camps
			{
				delete CampA;
				delete CampB;
				delete CampC;
				delete CampD;
				delete CampE;
				delete CampF;
			};
		};
		class Civilian
		{
			class Checkpoints
			{
				delete CheckpointSmall;
				delete CheckpointMedium;
				delete CheckpointLarge;
			};
		};
	};
};
