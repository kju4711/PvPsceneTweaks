class CfgPatches
{
	class ReducedFatigue_Gameplay_C_PvPscene
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Data_F_Loadorder","A3_Data_F_Curator_Loadorder","A3_Data_F_Kart_Loadorder","A3_Data_F_Bootcamp_Loadorder","A3_Data_F_Heli_Loadorder","A3_Data_F_Mark_Loadorder","A3_Data_F_Exp_A_Loadorder","A3_Data_F_Exp_B_Loadorder","A3_Data_F_Exp_Loadorder","A3_Data_F_Jets_Loadorder","A3_Data_F_Argo_Loadorder","A3_Data_F_Patrol_Loadorder","A3_Data_F_Orange_Loadorder","A3_Data_F_Tacops_Loadorder","A3_Data_F_Tank_Loadorder"};
		version = "2019-03-28";
	};
};
/*
class CfgMovesFatigue
{
	staminaDuration = 2* 60;
	staminaCooldown = 1/2 * 10;
	staminaRestoration = 1/2 * 30;
	aimPrecisionSpeedCoef = 1/2 * 5;
//	terrainDrainSprint = -1;
//	terrainDrainRun = -1;
//	terrainSpeedCoef = 0.9;
};
*/
class CfgMovesBasic
{
	class Default
	{
		stamina = 1/2 * 1;
	};
	class StandBase;
};
class CfgMovesMaleSdr: CfgMovesBasic
{
	class States
	{
		class AbdvPercMstpSnonWnonDnon;
		class AbdvPercMstpSnonWrflDnon;
		class AbswPercMstpSnonWnonDnon;
		class AdvePercMstpSnonWnonDnon;
		class AdvePercMstpSnonWrflDnon;
		class AidlPercMstpSrasWlnrDnon_G0S;
		class AidlPknlMstpSlowWrflDnon_G0S;
		class AidlPknlMstpSrasWlnrDnon_G0S;
		class AidlPknlMstpSrasWpstDnon_G0S;
		class AidlPpneMstpSnonWnonDnon_G0S;
		class AidlPpneMstpSrasWpstDnon_G0S;
		class AmovPercMstpSlowWrflDnon;
		class AmovPercMstpSnonWnonDnon;
		class AmovPercMstpSrasWpstDnon;
		class AmovPercMstpSrasWrflDnon;
		class AmovPercMwlkSlowWpstDf;
		class AmovPercMwlkSlowWrflDf_ver2;
		class AmovPercMwlkSlowWrflDfl_ver2;
		class AmovPercMwlkSoptWbinDf;
		class AmovPercMwlkSrasWpstDf;
		class AmovPercMwlkSrasWrflDf;
		class AmovPknlMstpSnonWnonDnon;
		class AmovPknlMstpSrasWrflDnon;
		class AmovPknlMwlkSlowWpstDf;
		class AmovPknlMwlkSoptWbinDf;
		class AmovPknlMwlkSrasWpstDf;
		class AmovPpneMstpSrasWpstDnon;
		class AmovPpneMstpSrasWrflDnon;
		class AmovPpneMwlkSoptWbinDf;
		class AsdvPercMstpSnonWnonDnon;
		class AsdvPercMstpSnonWrflDnon;
		class AsswPercMstpSnonWnonDnon;
		class AswmPercMstpSnonWnonDnon;

		class SprintBaseDf: StandBase
		{
			stamina = 2 * -1;
		};
		class AmovPknlMtacSlowWpstDf: AmovPknlMwlkSlowWpstDf
		{
			stamina = 2 * -0.1;
		};
		class AmovPknlMrunSlowWpstDf: AmovPknlMwlkSlowWpstDf
		{
			stamina = 2 * -0.1;
		};
		class AmovPercMrunSlowWrflDf: AmovPercMstpSlowWrflDnon
		{
			stamina = 2 * -0.1;
		};
		class AmovPercMtacSrasWrflDf: AmovPercMwlkSrasWrflDf
		{
			stamina = 2 * -0.1;
		};
		class AmovPercMtacSlowWrflDf_ver2: AmovPercMwlkSlowWrflDf_ver2
		{
			stamina = 2 * -0.1;
		};
		class AmovPercMtacSlowWrflDfl_ver2: AmovPercMwlkSlowWrflDfl_ver2
		{
			stamina = 2 * -0.1;
		};
		class AmovPercMrunSrasWrflDf: AmovPercMstpSrasWrflDnon
		{
			stamina = 2 * -0.1;
		};
		class AmovPercMrunSnonWnonDf: AmovPercMstpSnonWnonDnon
		{
			stamina = 2 * -0.1;
		};
		class AmovPknlMrunSlowWrflDf: AidlPknlMstpSlowWrflDnon_G0S
		{
			stamina = 2 * -0.1;
		};
		class AmovPknlMtacSlowWrflDf: AmovPknlMrunSlowWrflDf
		{
			stamina = 2 * -0.1;
		};
		class AmovPknlMrunSrasWrflDf: AmovPknlMstpSrasWrflDnon
		{
			stamina = 2 * -0.1;
		};
		class AmovPknlMtacSrasWrflDf: AmovPknlMrunSrasWrflDf
		{
			stamina = 2 * -0.1;
		};
		class AmovPercMtacSlowWpstDf: AmovPercMwlkSlowWpstDf
		{
			stamina = 2 * -0.1;
		};
		class AmovPercMtacSrasWpstDf: AmovPercMwlkSrasWpstDf
		{
			stamina = 2 * -0.1;
		};
		class AmovPercMrunSrasWpstDf: AmovPercMstpSrasWpstDnon
		{
			stamina = 2 * -0.1;
		};
		class AmovPercMrunSlowWpstDf: AmovPercMrunSrasWpstDf
		{
			stamina = 2 * -0.1;
		};
		class AmovPknlMrunSrasWpstDf: AidlPknlMstpSrasWpstDnon_G0S
		{
			stamina = 2 * -0.1;
		};
		class AmovPknlMtacSrasWpstDf: AmovPknlMwlkSrasWpstDf
		{
			stamina = 2 * -0.1;
		};
		class AmovPercMrunSlowWlnrDf: AidlPknlMstpSrasWlnrDnon_G0S
		{
			stamina = 2 * -0.1;
		};
		class AmovPknlMrunSrasWlnrDf: AmovPercMrunSlowWlnrDf
		{
			stamina = 2 * -0.1;
		};
		class AmovPercMevaSlowWlnrDf: AmovPercMrunSlowWlnrDf
		{
			stamina = 2 * -1;
		};
		class AmovPknlMevaSlowWlnrDf: AmovPercMrunSlowWlnrDf
		{
			stamina = 2 * -1;
		};
		class AmovPknlMtacSrasWlnrDf: AidlPknlMstpSrasWlnrDnon_G0S
		{
			stamina = 2 * -0.1;
		};
		class AmovPknlMrunSnonWnonDf: AmovPknlMstpSnonWnonDnon
		{
			stamina = 2 * -0.1;
		};
		class AmovPknlMwlkSnonWnonDf: AmovPknlMrunSnonWnonDf
		{
			stamina = 1/2 * 1;
		};
		class AmovPpneMrunSlowWrflDf: AmovPpneMstpSrasWrflDnon
		{
			stamina = 2 * -0.1;
		};
		class AmovPpneMevaSlowWrflDf: AmovPpneMrunSlowWrflDf
		{
			stamina = 2 * -1;
		};
		class AmovPpneMrunSlowWpstDf: AidlPpneMstpSrasWpstDnon_G0S
		{
			stamina = 2 * -0.1;
		};
		class AmovPpneMsprSlowWpstDf: AmovPpneMrunSlowWpstDf
		{
			stamina = 2 * -1;
		};
		class AmovPpneMrunSnonWnonDf: AidlPpneMstpSnonWnonDnon_G0S
		{
			stamina = 2 * -0.1;
		};
		class AmovPpneMsprSnonWnonDf: AmovPpneMrunSnonWnonDf
		{
			stamina = 2 * -1;
		};
		class AmovPpneMstpSrasWrflDnon_AmovPpneMevaSlowWrflDl: AmovPpneMstpSrasWrflDnon
		{
			stamina = 2 * -1;
		};
		class AmovPpneMstpSrasWpstDnon_AmovPpneMevaSlowWpstDl: AmovPpneMstpSrasWpstDnon
		{
			stamina = 2 * -1;
		};
		class AmovPpneMstpSnonWnonDnon_AmovPpneMevaSnonWnonDl: AidlPpneMstpSnonWnonDnon_G0S
		{
			stamina = 2 * -1;
		};
		class AmovPpneMstpSnonWnonDnon_AmovPpneMevaSnonWnonDr: AidlPpneMstpSnonWnonDnon_G0S
		{
			stamina = 2 * -1;
		};
		class AswmPercMstpSnonWnonDnon_goup: AswmPercMstpSnonWnonDnon
		{
			stamina = 2 * -0.1;
		};
		class AswmPercMstpSnonWnonDnon_godown: AswmPercMstpSnonWnonDnon
		{
			stamina = 2 * -0.1;
		};
		class AswmPercMrunSnonWnonDf: AswmPercMstpSnonWnonDnon
		{
			stamina = 2 * -0.1;
		};
		class AsswPercMstpSnonWnonDnon_goup: AsswPercMstpSnonWnonDnon
		{
			stamina = 2 * -0.1;
		};
		class AsswPercMstpSnonWnonDnon_goDown: AsswPercMstpSnonWnonDnon
		{
			stamina = 2 * -0.1;
		};
		class AbswPercMstpSnonWnonDnon_goup: AbswPercMstpSnonWnonDnon
		{
			stamina = 2 * -0.1;
		};
		class AbswPercMstpSnonWnonDnon_goDown: AbswPercMstpSnonWnonDnon
		{
			stamina = 2 * -0.1;
		};
		class AbswPercMrunSnonWnonDf: AbswPercMstpSnonWnonDnon
		{
			stamina = 2 * -0.1;
		};
		class AdvePercMstpSnonWnonDnon_goup: AdvePercMstpSnonWnonDnon
		{
			stamina = 2 * -0.1;
		};
		class AdvePercMstpSnonWnonDnon_godown: AdvePercMstpSnonWnonDnon
		{
			stamina = 2 * -0.1;
		};
		class AdvePercMwlkSnonWnonDf: AdvePercMstpSnonWnonDnon
		{
			stamina = 2 * -0.1;
		};
		class AdvePercMrunSnonWnonDf: AdvePercMstpSnonWnonDnon
		{
			stamina = 2 * -0.1;
		};
		class AdvePercMsprSnonWnonDf: AdvePercMrunSnonWnonDf
		{
			stamina = 2 * -1;
		};
		class AdvePercMstpSnonWrflDnon_goup: AdvePercMstpSnonWrflDnon
		{
			stamina = 2 * -0.1;
		};
		class AdvePercMstpSnonWrflDnon_godown: AdvePercMstpSnonWrflDnon
		{
			stamina = 2 * -0.1;
		};
		class AdvePercMwlkSnonWrflDf: AdvePercMstpSnonWrflDnon
		{
			stamina = 2 * -0.1;
		};
		class AdvePercMrunSnonWrflDf: AdvePercMstpSnonWrflDnon
		{
			stamina = 2 * -0.1;
		};
		class AdvePercMsprSnonWrflDf: AdvePercMrunSnonWrflDf
		{
			stamina = 2 * -1;
		};
		class AsdvPercMstpSnonWnonDnon_goup: AsdvPercMstpSnonWnonDnon
		{
			stamina = 2 * -0.1;
		};
		class AsdvPercMstpSnonWnonDnon_godown: AsdvPercMstpSnonWnonDnon
		{
			stamina = 2 * -0.1;
		};
		class AsdvPercMwlkSnonWnonDf: AsdvPercMstpSnonWnonDnon
		{
			stamina = 2 * -0.1;
		};
		class AsdvPercMtacSnonWnonDf: AsdvPercMstpSnonWnonDnon
		{
			stamina = 2 * -0.1;
		};
		class AsdvPercMrunSnonWnonDf: AsdvPercMstpSnonWnonDnon
		{
			stamina = 2 * -0.1;
		};
		class AsdvPercMsprSnonWnonDf: AsdvPercMrunSnonWnonDf
		{
			stamina = 2 * -1;
		};
		class AsdvPercMstpSnonWrflDnon_goup: AsdvPercMstpSnonWrflDnon
		{
			stamina = 2 * -0.1;
		};
		class AsdvPercMstpSnonWrflDnon_godown: AsdvPercMstpSnonWrflDnon
		{
			stamina = 2 * -0.1;
		};
		class AsdvPercMwlkSnonWrflDf: AsdvPercMstpSnonWrflDnon
		{
			stamina = 2 * -0.1;
		};
		class AsdvPercMtacSnonWrflDf: AsdvPercMstpSnonWrflDnon
		{
			stamina = 2 * -0.1;
		};
		class AsdvPercMrunSnonWrflDf: AsdvPercMstpSnonWrflDnon
		{
			stamina = 2 * -0.1;
		};
		class AsdvPercMsprSnonWrflDf: AsdvPercMrunSnonWrflDf
		{
			stamina = 2 * -1;
		};
		class AbdvPercMstpSnonWnonDnon_goup: AbdvPercMstpSnonWnonDnon
		{
			stamina = 2 * -0.1;
		};
		class AbdvPercMstpSnonWnonDnon_godown: AbdvPercMstpSnonWnonDnon
		{
			stamina = 2 * -0.1;
		};
		class AbdvPercMwlkSnonWnonDf: AbdvPercMstpSnonWnonDnon
		{
			stamina = 2 * -0.1;
		};
		class AbdvPercMtacSnonWnonDf: AbdvPercMstpSnonWnonDnon
		{
			stamina = 2 * -0.1;
		};
		class AbdvPercMrunSnonWnonDf: AbdvPercMstpSnonWnonDnon
		{
			stamina = 2 * -0.1;
		};
		class AbdvPercMsprSnonWnonDf: AbdvPercMrunSnonWnonDf
		{
			stamina = 2 * -1;
		};
		class AbdvPercMstpSnonWrflDnon_goup: AbdvPercMstpSnonWrflDnon
		{
			stamina = 2 * -0.1;
		};
		class AbdvPercMstpSnonWrflDnon_godown: AbdvPercMstpSnonWrflDnon
		{
			stamina = 2 * -0.1;
		};
		class AbdvPercMwlkSnonWrflDf: AbdvPercMstpSnonWrflDnon
		{
			stamina = 2 * -0.1;
		};
		class AbdvPercMtacSnonWrflDf: AbdvPercMstpSnonWrflDnon
		{
			stamina = 2 * -0.1;
		};
		class AbdvPercMrunSnonWrflDf: AbdvPercMstpSnonWrflDnon
		{
			stamina = 2 * -0.1;
		};
		class AbdvPercMsprSnonWrflDf: AbdvPercMrunSnonWrflDf
		{
			stamina = 2 * -1;
		};
		class AmovPercMrunSnonWbinDf: AmovPercMwlkSoptWbinDf
		{
			stamina = 2 * -0.1;
		};
		class AmovPercMevaSnonWbinDf: AmovPercMrunSnonWbinDf
		{
			stamina = 2 * -1;
		};
		class AmovPknlMrunSnonWbinDf: AmovPknlMwlkSoptWbinDf
		{
			stamina = 2 * -0.1;
		};
		class AmovPknlMevaSnonWbinDf: AmovPknlMrunSnonWbinDf
		{
			stamina = 2 * -1;
		};
		class AmovPpneMrunSnonWbinDf: AmovPpneMwlkSoptWbinDf
		{
			stamina = 2 * -0.1;
		};
		class AmovPpneMevaSnonWbinDf: AmovPpneMrunSnonWbinDf
		{
			stamina = 2 * -1;
		};
		class AmovPercMtacSrasWlnrDf: AidlPercMstpSrasWlnrDnon_G0S
		{
			stamina = 2 * -0.1;
		};
	};
};
/*
class CfgMovesBasic
{
	class Default
	{
		duty = 2 * -0.5;
	};
	class HealBase: Default
	{
		duty = 1/2 * 0.2;
	};
	class InjuredMovedBase;
	class StandBase;
};
class CfgMovesMaleSdr: CfgMovesBasic
{
	class States
	{
		class AadjPercMstpSrasWpstDdown;
		class AadjPercMstpSrasWpstDleft;
		class AadjPercMstpSrasWpstDright;
		class AadjPercMstpSrasWpstDup;
		class AadjPercMstpSrasWrflDdown;
		class AadjPercMstpSrasWrflDleft;
		class AadjPercMstpSrasWrflDright;
		class AadjPercMstpSrasWrflDup;
		class AadjPknlMstpSrasWpstDdown;
		class AadjPknlMstpSrasWpstDleft;
		class AadjPknlMstpSrasWpstDright;
		class AadjPknlMstpSrasWpstDup;
		class AadjPknlMstpSrasWrflDdown;
		class AadjPknlMstpSrasWrflDleft;
		class AadjPknlMstpSrasWrflDright;
		class AadjPknlMstpSrasWrflDup;
		class AadjPpneMstpSrasWpstDup;
		class AadjPpneMstpSrasWrflDup;
		class AbdvPercMstpSnonWnonDnon;
		class AbdvPercMstpSnonWrflDnon;
		class AbswPercMstpSnonWnonDnon;
		class AcinPercMrunSnonWnonDf_death;
		class AcinPercMrunSrasWrflDf_death;
		class AdvePercMstpSnonWnonDnon;
		class AdvePercMstpSnonWrflDnon;
		class AidlPercMstpSlowWpstDnon_G0S;
		class AidlPercMstpSlowWrflDnon_G0S;
		class AidlPercMstpSnonWnonDnon_G0S;
		class AidlPercMstpSrasWlnrDnon_G0S;
		class AidlPercMstpSrasWpstDnon_G0S;
		class AidlPercMstpSrasWrflDnon_G0S;
		class AidlPknlMstpSlowWpstDnon_G0S;
		class AidlPknlMstpSlowWrflDnon_G0S;
		class AidlPknlMstpSrasWlnrDnon_G0S;
		class AidlPknlMstpSrasWpstDnon_G0S;
		class AidlPpneMstpSnonWnonDnon_G0S;
		class AidlPpneMstpSrasWpstDnon_G0S;
		class AidlPpneMstpSrasWrflDnon_G0S;
		class AinjPfalMstpSnonWnonDnon_carried_Up;
		class AinjPfalMstpSnonWrflDnon_carried_Up;
		class AinjPpneMstpSnonWnonDnon;
		class AinjPpneMstpSnonWrflDnon;
		class AmovPercMrunSlowWlnrDfr;
		class AmovPercMstpSlowWlnrDnon;
		class AmovPercMstpSoptWbinDnon;
		class AmovPercMstpSrasWpstDnon_AmovPercMstpSrasWrflDnon_end;
		class AmovPercMstpSrasWrflDnon_AmovPercMstpSrasWpstDnon_end;
		class AmovPercMwlkSlowWrflDfl_ver2;
		class AmovPknlMrunSlowWrflDb;
		class AmovPknlMrunSlowWrflDbl;
		class AmovPknlMrunSlowWrflDbr;
		class AmovPknlMrunSlowWrflDfl;
		class AmovPknlMrunSlowWrflDfr;
		class AmovPknlMrunSlowWrflDl;
		class AmovPknlMrunSlowWrflDr;
		class AmovPknlMstpSoptWbinDnon;
		class AmovPpneMstpSnonWnonDnon_injured;
		class AmovPpneMstpSoptWbinDnon;
		class AmovPpneMstpSrasWrflDnon_injured;
		class ApanPercMstpSnonWnonDnon;
		class ApanPknlMstpSnonWnonDnon;
		class ApanPpneMstpSnonWnonDnon;
		class AsdvPercMstpSnonWnonDnon;
		class AsdvPercMstpSnonWrflDnon;
		class AsswPercMstpSnonWnonDnon;
		class AwopPercMstpSoptWbinDnon_lnr;
		class AwopPercMstpSoptWbinDnon_non;
		class AwopPercMstpSoptWbinDnon_pst;
		class AwopPercMstpSoptWbinDnon_rfl;
		class AwopPknlMstpSoptWbinDnon_lnr;
		class AwopPknlMstpSoptWbinDnon_non;
		class AwopPknlMstpSoptWbinDnon_pst;
		class AwopPknlMstpSoptWbinDnon_rfl;
		class AwopPpneMstpSoptWbinDnon_lnr;
		class AwopPpneMstpSoptWbinDnon_non;
		class AwopPpneMstpSoptWbinDnon_pst;
		class AwopPpneMstpSoptWbinDnon_rfl;
		class CutSceneAnimationBase;
		class SprintCivilBaseDf;
		class TransAnimBase;
		class TransAnimBase_noIK;

		class SprintBaseDf: StandBase
		{
			duty = 1/2 * 0.5;
		};
		class AmovPercMstpSlowWrflDnon: StandBase
		{
			duty = 2 * -0.4;
		};
		class AovrPercMstpSlowWrflDf: AmovPercMstpSlowWrflDnon
		{
			duty = 1/2 * 1;
		};
		class AmovPercMstpSlowWrflDnon_turnL: AidlPercMstpSlowWrflDnon_G0S
		{
			duty = 2 * -0.2;
		};
		class AmovPercMstpSlowWrflDnon_turnR: AidlPercMstpSlowWrflDnon_G0S
		{
			duty = 2 * -0.2;
		};
		class AmovPercMstpSrasWrflDnon: AmovPercMstpSlowWrflDnon
		{
			duty = 2 * -0.3;
		};
		class AmovPercMstpSrasWrflDnon_turnL: AidlPercMstpSrasWrflDnon_G0S
		{
			duty = 1/2 * 0;
		};
		class AmovPercMstpSrasWrflDnon_turnR: AidlPercMstpSrasWrflDnon_G0S
		{
			duty = 1/2 * 0;
		};
		class AovrPercMstpSrasWrflDf: AmovPercMstpSrasWrflDnon
		{
			duty = 1/2 * 1;
		};
		class AovrPercMrunSrasWrflDf: AovrPercMstpSrasWrflDf
		{
			duty = 1/2 * 10;
		};
		class AmovPknlMstpSlowWrflDnon: AmovPercMstpSlowWrflDnon
		{
			duty = 2 * -0.6;
		};
		class AmovPknlMstpSlowWrflDnon_turnL: AidlPknlMstpSlowWrflDnon_G0S
		{
			duty = 2 * -0.4;
		};
		class AmovPknlMstpSlowWrflDnon_turnR: AidlPknlMstpSlowWrflDnon_G0S
		{
			duty = 2 * -0.4;
		};
		class AmovPknlMstpSrasWrflDnon: AmovPknlMstpSlowWrflDnon
		{
			duty = 2 * -0.4;
		};
		class AmovPknlMstpSrasWrflDnon_turnL: AmovPknlMstpSrasWrflDnon
		{
			duty = 2 * -0.2;
		};
		class AmovPknlMstpSrasWrflDnon_turnR: AmovPknlMstpSrasWrflDnon
		{
			duty = 2 * -0.2;
		};
		class AmovPpneMstpSrasWrflDnon: AmovPercMstpSrasWrflDnon
		{
			duty = 2 * -0.8;
		};
		class AmovPpneMstpSrasWrflDnon_turnL: AidlPpneMstpSrasWrflDnon_G0S
		{
			duty = 2 * -0.6;
		};
		class AmovPpneMstpSrasWrflDnon_turnR: AidlPpneMstpSrasWrflDnon_G0S
		{
			duty = 2 * -0.6;
		};
		class AmovPercMstpSrasWpstDnon: StandBase
		{
			duty = 2 * -0.4;
		};
		class AovrPercMstpSrasWpstDf: AidlPercMstpSrasWpstDnon_G0S
		{
			duty = 1/2 * 1;
		};
		class AmovPercMstpSlowWpstDnon: AmovPercMstpSrasWpstDnon
		{
			duty = 2 * -0.6;
		};
		class AmovPercMstpSlowWpstDnon_turnL: AidlPercMstpSlowWpstDnon_G0S
		{
			duty = 2 * -0.4;
		};
		class AmovPercMstpSlowWpstDnon_turnR: AidlPercMstpSlowWpstDnon_G0S
		{
			duty = 2 * -0.4;
		};
		class AmovPercMstpSrasWpstDnon_turnL: AmovPercMstpSrasWpstDnon
		{
			duty = 2 * -0.2;
		};
		class AmovPercMstpSrasWpstDnon_turnR: AmovPercMstpSrasWpstDnon
		{
			duty = 2 * -0.2;
		};
		class AmovPknlMstpSrasWpstDnon: AmovPercMstpSrasWpstDnon
		{
			duty = 2 * -0.6;
		};
		class AmovPknlMstpSlowWpstDnon: AmovPknlMstpSrasWpstDnon
		{
			duty = 2 * -0.8;
		};
		class AmovPknlMstpSlowWpstDnon_turnL: AidlPknlMstpSlowWpstDnon_G0S
		{
			duty = 2 * -0.6;
		};
		class AmovPknlMstpSlowWpstDnon_turnR: AidlPknlMstpSlowWpstDnon_G0S
		{
			duty = 2 * -0.8;
		};
		class AmovPknlMwlkSlowWpstDf: AidlPknlMstpSlowWpstDnon_G0S
		{
			duty = 1/2 * 0.1;
		};
		class AmovPknlMtacSlowWpstDf: AmovPknlMwlkSlowWpstDf
		{
			duty = 1/2 * 0.2;
		};
		class AmovPknlMrunSlowWpstDf: AmovPknlMwlkSlowWpstDf
		{
			duty = 1/2 * 0.3;
		};
		class AmovPknlMstpSrasWpstDnon_turnL: AmovPknlMstpSrasWpstDnon
		{
			duty = 2 * -0.4;
		};
		class AmovPknlMstpSrasWpstDnon_turnR: AmovPknlMstpSrasWpstDnon
		{
			duty = 2 * -0.4;
		};
		class AmovPpneMstpSrasWpstDnon: AmovPercMstpSrasWpstDnon
		{
			duty = 2 * -1;
		};
		class AmovPpneMstpSrasWpstDnon_turnL: AidlPpneMstpSrasWpstDnon_G0S
		{
			duty = 2 * -0.8;
		};
		class AmovPpneMstpSrasWpstDnon_turnR: AidlPpneMstpSrasWpstDnon_G0S
		{
			duty = 2 * -0.8;
		};
		class AmovPknlMstpSrasWlnrDnon: Default
		{
			duty = 2 * -0.4;
		};
		class ReloadRPGKneel: AidlPknlMstpSrasWlnrDnon_G0S
		{
			duty = 1/2 * 0.6;
		};
		class ReloadRPG7VKneel: AidlPknlMstpSrasWlnrDnon_G0S
		{
			duty = 1/2 * 0.6;
		};
		class AmovPknlMstpSrasWlnrDnon_turnL: AidlPknlMstpSrasWlnrDnon_G0S
		{
			duty = 2 * -0.2;
		};
		class AmovPknlMstpSrasWlnrDnon_turnR: AidlPknlMstpSrasWlnrDnon_G0S
		{
			duty = 2 * -0.2;
		};
		class AmovPpneMstpSrasWlnrDnon: AidlPknlMstpSrasWlnrDnon_G0S
		{
			duty = 2 * -1.2;
		};
		class AmovPpneMrunSrasWlnrDf: AmovPpneMstpSrasWlnrDnon
		{
			duty = 1/2 * 0.6;
		};
		class AmovPercMstpSnonWnonDnon: StandBase
		{
			duty = 2 * -0.8;
		};
		class AovrPercMstpSnonWnonDf: AmovPercMstpSnonWnonDnon
		{
			duty = 1/2 * 2;
		};
		class AmovPknlMstpSnonWnonDnon: AmovPercMstpSnonWnonDnon
		{
			duty = 2 * -1;
		};
		class AmovPpneMstpSnonWnonDnon: AmovPercMstpSnonWnonDnon
		{
			duty = 2 * -1;
		};
		class AmovPercMwlkSlowWrflDf_ver2: AmovPercMstpSlowWrflDnon
		{
			duty = 2 * -0.3;
		};
		class AmovPercMrunSlowWrflDf: AmovPercMstpSlowWrflDnon
		{
			duty = 1/2 * 0.2;
		};
		class AmovPercMrunSlowWrflDfl: AmovPercMrunSlowWrflDf
		{
			duty = 1/2 * 0.2;
		};
		class AmovPercMrunSlowWrflDbl: AmovPercMrunSlowWrflDfl
		{
			duty = 1/2 * 0.2;
		};
		class AmovPercMrunSlowWrflDb: AmovPercMrunSlowWrflDfl
		{
			duty = 1/2 * 0.2;
		};
		class AmovPercMrunSlowWrflDbr: AmovPercMrunSlowWrflDfl
		{
			duty = 1/2 * 0.2;
		};
		class AmovPercMrunSlowWrflDfr: AmovPercMrunSlowWrflDfl
		{
			duty = 1/2 * 0.2;
		};
		class AmovPercMwlkSrasWrflDf: AmovPercMstpSrasWrflDnon
		{
			duty = 2 * -0.2;
		};
		class AmovPercMtacSrasWrflDf: AmovPercMwlkSrasWrflDf
		{
			duty = 1/2 * 0.5;
		};
		class AmovPercMtacSlowWrflDf_ver2: AmovPercMwlkSlowWrflDf_ver2
		{
			duty = 1/2 * 0.15;
		};
		class AmovPercMtacSlowWrflDfl_ver2: AmovPercMwlkSlowWrflDfl_ver2
		{
			duty = 1/2 * 0.15;
		};
		class AmovPercMrunSrasWrflDf: AmovPercMstpSrasWrflDnon
		{
			duty = 1/2 * 0.3;
		};
		class AmovPercMwlkSnonWnonDf: AidlPercMstpSnonWnonDnon_G0S
		{
			duty = 2 * -0.6;
		};
		class AmovPercMrunSnonWnonDf: AmovPercMstpSnonWnonDnon
		{
			duty = 1/2 * 0.1;
		};
		class AmovPknlMwlkSlowWrflDf: AidlPknlMstpSlowWpstDnon_G0S
		{
			duty = 1/2 * 0.2;
		};
		class AmovPknlMrunSlowWrflDf: AidlPknlMstpSlowWrflDnon_G0S
		{
			duty = 1/2 * 0.3;
		};
		class AmovPknlMtacSlowWrflDf: AmovPknlMrunSlowWrflDf
		{
			duty = 1/2 * 0.25;
		};
		class AmovPknlMtacSlowWrflDfl: AmovPknlMrunSlowWrflDfl
		{
			duty = 1/2 * 0.2;
		};
		class AmovPknlMtacSlowWrflDl: AmovPknlMrunSlowWrflDl
		{
			duty = 1/2 * 0.2;
		};
		class AmovPknlMtacSlowWrflDbl: AmovPknlMrunSlowWrflDbl
		{
			duty = 1/2 * 0.2;
		};
		class AmovPknlMtacSlowWrflDb: AmovPknlMrunSlowWrflDb
		{
			duty = 1/2 * 0.2;
		};
		class AmovPknlMtacSlowWrflDbr: AmovPknlMrunSlowWrflDbr
		{
			duty = 1/2 * 0.2;
		};
		class AmovPknlMtacSlowWrflDr: AmovPknlMrunSlowWrflDr
		{
			duty = 1/2 * 0.2;
		};
		class AmovPknlMtacSlowWrflDfr: AmovPknlMrunSlowWrflDfr
		{
			duty = 1/2 * 0.2;
		};
		class AmovPknlMwlkSrasWrflDf: AmovPknlMstpSrasWrflDnon
		{
			duty = 1/2 * 0.25;
		};
		class AmovPknlMrunSrasWrflDf: AmovPknlMstpSrasWrflDnon
		{
			duty = 1/2 * 0.4;
		};
		class AmovPknlMtacSrasWrflDf: AmovPknlMrunSrasWrflDf
		{
			duty = 1/2 * 0.6;
		};
		class AmovPercMwlkSrasWpstDf: AmovPercMstpSrasWpstDnon
		{
			duty = 2 * -0.3;
		};
		class AmovPercMwlkSlowWpstDf: AmovPercMwlkSrasWpstDf
		{
			duty = 2 * -0.5;
		};
		class AmovPercMtacSlowWpstDf: AmovPercMwlkSlowWpstDf
		{
			duty = 1/2 * 0;
		};
		class AmovPercMtacSrasWpstDf: AmovPercMwlkSrasWpstDf
		{
			duty = 1/2 * 0.2;
		};
		class AmovPercMrunSrasWpstDf: AmovPercMstpSrasWpstDnon
		{
			duty = 1/2 * 0.15;
		};
		class AmovPknlMwlkSrasWpstDf: AmovPknlMstpSrasWpstDnon
		{
			duty = 1/2 * 0.15;
		};
		class AmovPknlMrunSrasWpstDf: AidlPknlMstpSrasWpstDnon_G0S
		{
			duty = 1/2 * 0.3;
		};
		class AmovPknlMtacSrasWpstDf: AmovPknlMwlkSrasWpstDf
		{
			duty = 1/2 * 0.4;
		};
		class AmovPercMrunSlowWlnrDf: AidlPknlMstpSrasWlnrDnon_G0S
		{
			duty = 1/2 * 0.3;
		};
		class AmovPknlMrunSrasWlnrDf: AmovPercMrunSlowWlnrDf
		{
			duty = 1/2 * 0.5;
		};
		class AmovPknlMrunSrasWlnrDfr: AmovPercMrunSlowWlnrDfr
		{
			duty = 1/2 * 0.5;
		};
		class AmovPercMevaSlowWlnrDf: AmovPercMrunSlowWlnrDf
		{
			duty = 1/2 * 1.4;
		};
		class AmovPknlMevaSlowWlnrDf: AmovPercMrunSlowWlnrDf
		{
			duty = 1/2 * 1.6;
		};
		class AmovPknlMwlkSrasWlnrDf: AidlPknlMstpSrasWlnrDnon_G0S
		{
			duty = 1/2 * 0.3;
		};
		class AmovPknlMtacSrasWlnrDf: AidlPknlMstpSrasWlnrDnon_G0S
		{
			duty = 1/2 * 0.5;
		};
		class AmovPknlMrunSnonWnonDf: AmovPknlMstpSnonWnonDnon
		{
			duty = 1/2 * 0.2;
		};
		class AmovPknlMwlkSnonWnonDf: AmovPknlMrunSnonWnonDf
		{
			duty = 1/2 * 0;
		};
		class AmovPpneMrunSlowWrflDf: AmovPpneMstpSrasWrflDnon
		{
			duty = 1/2 * 1;
		};
		class AmovPpneMsprSlowWrflDf: AmovPpneMrunSlowWrflDf
		{
			duty = 1/2 * 1;
		};
		class AmovPpneMevaSlowWrflDf: AmovPpneMrunSlowWrflDf
		{
			duty = 1/2 * 1.4;
		};
		class AmovPpneMsprSlowWrflDf_injured: AmovPpneMstpSrasWrflDnon_injured
		{
			duty = 1/2 * 2.6;
		};
		class AmovPpneMrunSlowWpstDf: AidlPpneMstpSrasWpstDnon_G0S
		{
			duty = 1/2 * 0.8;
		};
		class AmovPpneMsprSlowWpstDf: AmovPpneMrunSlowWpstDf
		{
			duty = 1/2 * 1.2;
		};
		class AmovPpneMrunSnonWnonDf: AidlPpneMstpSnonWnonDnon_G0S
		{
			duty = 1/2 * 0.3;
		};
		class AmovPpneMsprSnonWnonDf: AmovPpneMrunSnonWnonDf
		{
			duty = 1/2 * 1;
		};
		class RifleReloadProneBase: Default
		{
			duty = 2 * -1;
		};
		class WeaponMagazineReloadStand: Default
		{
			duty = 2 * -1;
		};
		class PistolMagazineReloadStand: Default
		{
			duty = 2 * -1;
		};
		class LauncherReloadKneel: Default
		{
			duty = 2 * -1;
		};
		class AadjPpneMstpSrasWrflDup_AadjPknlMstpSrasWrflDdown: AadjPknlMstpSrasWrflDdown
		{
			duty = 1/2 * 0.2;
		};
		class AadjPknlMstpSrasWrflDdown_AadjPpneMstpSrasWrflDup: AadjPpneMstpSrasWrflDup
		{
			duty = 1/2 * 0.2;
		};
		class AmovPercMstpSrasWlnrDnon_AmovPercMstpSrasWrflDnon: TransAnimBase
		{
			duty = 1/2 * 0.5;
		};
		class AmovPercMstpSrasWlnrDnon_AmovPercMstpSrasWrflDnon_end: TransAnimBase
		{
			duty = 1/2 * 0.5;
		};
		class AmovPercMstpSrasWrflDnon_AmovPercMstpSrasWlnrDnon_end: TransAnimBase
		{
			duty = 1/2 * 0.5;
		};
		class AmovPercMstpSrasWrflDnon_AmovPercMstpSrasWlnrDnon: TransAnimBase_noIK
		{
			duty = 1/2 * 0.5;
		};
		class AmovPercMstpSrasWlnrDnon_AmovPknlMstpSrasWlnrDnon: TransAnimBase
		{
			duty = 1/2 * 0.6;
		};
		class AmovPpneMstpSrasWlnrDnon_AmovPercMstpSrasWlnrDnon: TransAnimBase
		{
			duty = 1/2 * 2;
		};
		class AmovPercMstpSrasWlnrDnon_AmovPpneMstpSrasWlnrDnon: TransAnimBase
		{
			duty = 1/2 * 0.8;
		};
		class AmovPpneMstpSrasWlnrDnon_AmovPknlMstpSrasWlnrDnon: TransAnimBase
		{
			duty = 1/2 * 1;
		};
		class AmovPknlMstpSrasWlnrDnon_AmovPercMstpSrasWlnrDnon: TransAnimBase
		{
			duty = 1/2 * 0.8;
		};
		class AmovPercMstpSrasWlnrDnon_AmovPpneMstpSnonWnonDnon: TransAnimBase
		{
			duty = 1/2 * 2;
		};
		class AmovPercMstpSrasWrflDnon_AmovPknlMstpSrasWrflDnon: AmovPknlMstpSrasWrflDnon
		{
			duty = 1/2 * 0.4;
		};
		class AmovPercMstpSrasWrflDnon_AmovPpneMstpSrasWrflDnon: AmovPpneMstpSrasWrflDnon
		{
			duty = 1/2 * 0.5;
		};
		class AmovPercMsprSlowWrflDf_AmovPpneMstpSrasWrflDnon: TransAnimBase
		{
			duty = 1/2 * 1;
		};
		class AmovPercMrunSlowWrflDf_AmovPpneMstpSrasWrflDnon: AmovPercMstpSrasWrflDnon
		{
			duty = 1/2 * 2;
		};
		class AmovPercMrunSlowWrflDf_AmovPpneMstpSrasWrflDnon_end: AmovPpneMstpSrasWrflDnon
		{
			duty = 1/2 * 2;
		};
		class AmovPercMrunSlowWrflDf_AmovPpneMstpSrasWrflDnon_old: AmovPpneMstpSrasWrflDnon
		{
			duty = 1/2 * 2;
		};
		class AmovPknlMstpSrasWrflDnon_AmovPercMstpSrasWrflDnon: AmovPercMstpSrasWrflDnon
		{
			duty = 1/2 * 0.6;
		};
		class AmovPknlMstpSrasWrflDnon_AmovPpneMstpSrasWrflDnon: AmovPpneMstpSrasWrflDnon
		{
			duty = 1/2 * 0.4;
		};
		class AmovPpneMstpSrasWrflDnon_AmovPercMstpSrasWrflDnon: AmovPercMstpSrasWrflDnon
		{
			duty = 1/2 * 2;
		};
		class AmovPpneMstpSrasWrflDnon_AmovPercMsprSlowWrflDf: TransAnimBase
		{
			duty = 1/2 * 2;
		};
		class AmovPpneMstpSrasWrflDnon_AmovPknlMstpSrasWrflDnon: AmovPknlMstpSrasWrflDnon
		{
			duty = 1/2 * 1;
		};
		class AmovPercMstpSrasWpstDnon_AmovPknlMstpSrasWpstDnon: AmovPknlMstpSrasWpstDnon
		{
			duty = 1/2 * 0.2;
		};
		class AmovPercMsprSlowWpstDf_AmovPpneMstpSrasWpstDnon: TransAnimBase
		{
			duty = 1/2 * 0.8;
		};
		class AmovPercMstpSrasWpstDnon_AmovPpneMstpSrasWpstDnon: AmovPpneMstpSrasWpstDnon
		{
			duty = 1/2 * 0.4;
		};
		class AmovPknlMstpSrasWpstDnon_AmovPercMstpSrasWpstDnon: AmovPercMstpSrasWpstDnon
		{
			duty = 1/2 * 0.4;
		};
		class AmovPknlMstpSrasWpstDnon_AmovPpneMstpSrasWpstDnon: AmovPpneMstpSrasWpstDnon
		{
			duty = 1/2 * 0.2;
		};
		class AmovPpneMstpSrasWpstDnon_AmovPercMstpSrasWpstDnon: AmovPercMstpSrasWpstDnon
		{
			duty = 1/2 * 1;
		};
		class AmovPpneMstpSrasWpstDnon_AmovPknlMstpSrasWpstDnon: AmovPknlMstpSrasWpstDnon
		{
			duty = 1/2 * 0.8;
		};
		class AmovPpneMstpSrasWpstDnon_AmovPercMsprSlowWpstDf: TransAnimBase
		{
			duty = 1/2 * 2;
		};
		class AmovPercMstpSnonWnonDnon_AmovPknlMstpSnonWnonDnon: AmovPknlMstpSnonWnonDnon
		{
			duty = 1/2 * 0.1;
		};
		class AmovPercMsprSnonWnonDf_AmovPpneMstpSnonWnonDnon: TransAnimBase
		{
			duty = 1/2 * 0.6;
		};
		class AmovPercMsprSnonWnonDf_AmovPpneMstpSnonWnonDnon_2: AmovPpneMstpSnonWnonDnon
		{
			duty = 1/2 * 2;
		};
		class AmovPercMstpSnonWnonDnon_AmovPpneMstpSnonWnonDnon: AmovPpneMstpSnonWnonDnon
		{
			duty = 1/2 * 0.6;
		};
		class AmovPknlMstpSnonWnonDnon_AmovPercMstpSnonWnonDnon: AmovPercMstpSnonWnonDnon
		{
			duty = 1/2 * 0.2;
		};
		class AmovPknlMstpSnonWnonDnon_AmovPpneMstpSnonWnonDnon: AmovPpneMstpSnonWnonDnon
		{
			duty = 1/2 * 0.1;
		};
		class AmovPpneMstpSnonWnonDnon_AmovPercMstpSnonWnonDnon: AmovPercMstpSnonWnonDnon
		{
			duty = 1/2 * 1;
		};
		class AmovPpneMstpSnonWnonDnon_AmovPknlMstpSnonWnonDnon: AmovPknlMstpSnonWnonDnon
		{
			duty = 1/2 * 0.4;
		};
		class AmovPercMstpSrasWrflDnon_AmovPercMstpSrasWpstDnon: TransAnimBase
		{
			duty = 1/2 * 0.5;
		};
		class AmovPknlMstpSrasWrflDnon_AmovPknlMstpSrasWpstDnon: AmovPercMstpSrasWrflDnon_AmovPercMstpSrasWpstDnon
		{
			duty = 1/2 * 1;
		};
		class AmovPknlMstpSrasWrflDnon_AmovPknlMstpSrasWpstDnon_end: AmovPercMstpSrasWrflDnon_AmovPercMstpSrasWpstDnon_end
		{
			duty = 1/2 * 1;
		};
		class AmovPpneMstpSrasWrflDnon_AmovPpneMstpSrasWpstDnon: TransAnimBase
		{
			duty = 1/2 * 1.5;
		};
		class AmovPknlMstpSrasWrflDnon_AmovPknlMstpSrasWlnrDnon: AmovPercMstpSrasWrflDnon_AmovPercMstpSrasWlnrDnon
		{
			duty = 1/2 * 1;
		};
		class AmovPknlMstpSrasWrflDnon_AmovPknlMstpSrasWlnrDnon_end: AmovPercMstpSrasWrflDnon_AmovPercMstpSrasWlnrDnon_end
		{
			duty = 1/2 * 1;
		};
		class AmovPercMstpSrasWpstDnon_AmovPercMstpSrasWrflDnon: TransAnimBase
		{
			duty = 1/2 * 0.5;
		};
		class AmovPknlMstpSrasWpstDnon_AmovPknlMstpSrasWrflDnon: AmovPercMstpSrasWpstDnon_AmovPercMstpSrasWrflDnon
		{
			duty = 1/2 * 1;
		};
		class AmovPknlMstpSrasWpstDnon_AmovPknlMstpSrasWrflDnon_end: AmovPercMstpSrasWpstDnon_AmovPercMstpSrasWrflDnon_end
		{
			duty = 1/2 * 1;
		};
		class AmovPpneMstpSrasWpstDnon_AmovPpneMstpSrasWrflDnon: TransAnimBase
		{
			duty = 1/2 * 1.5;
		};
		class AmovPercMstpSrasWlnrDnon_AmovPercMstpSrasWpstDnon: TransAnimBase
		{
			duty = 1/2 * 0.5;
		};
		class AmovPercMstpSrasWlnrDnon_AmovPercMstpSrasWpstDnon_end: TransAnimBase
		{
			duty = 1/2 * 0.5;
		};
		class AmovPercMstpSrasWpstDnon_AmovPercMstpSrasWlnrDnon: TransAnimBase
		{
			duty = 1/2 * 0.5;
		};
		class AmovPercMstpSrasWpstDnon_AmovPercMstpSrasWlnrDnon_end: TransAnimBase
		{
			duty = 1/2 * 0.5;
		};
		class AmovPknlMstpSrasWpstDnon_AmovPknlMstpSrasWlnrDnon: AmovPercMstpSrasWpstDnon_AmovPercMstpSrasWlnrDnon
		{
			duty = 1/2 * 1;
		};
		class AmovPknlMstpSrasWpstDnon_AmovPknlMstpSrasWlnrDnon_end: AmovPercMstpSrasWpstDnon_AmovPercMstpSrasWlnrDnon_end
		{
			duty = 1/2 * 1;
		};
		class AmovPknlMstpSrasWlnrDnon_AmovPknlMstpSrasWrflDnon: AmovPercMstpSrasWlnrDnon_AmovPercMstpSrasWrflDnon
		{
			duty = 1/2 * 1;
		};
		class AmovPknlMstpSrasWlnrDnon_AmovPknlMstpSrasWrflDnon_end: AmovPercMstpSrasWlnrDnon_AmovPercMstpSrasWrflDnon_end
		{
			duty = 1/2 * 1;
		};
		class AmovPknlMstpSrasWlnrDnon_AmovPknlMstpSrasWpstDnon: AmovPercMstpSrasWlnrDnon_AmovPercMstpSrasWpstDnon
		{
			duty = 1/2 * 1;
		};
		class AmovPknlMstpSrasWlnrDnon_AmovPknlMstpSrasWpstDnon_end: AmovPercMstpSrasWlnrDnon_AmovPercMstpSrasWpstDnon_end
		{
			duty = 1/2 * 1;
		};
		class AmovPercMevaSrasWrflDf: SprintBaseDf
		{
			duty = 1/2 * 1;
		};
		class AmovPknlMevaSrasWrflDf: AmovPercMevaSrasWrflDf
		{
			duty = 1/2 * 1.2;
		};
		class AmovPpneMstpSrasWrflDnon_AmovPpneMevaSlowWrflDl: AmovPpneMstpSrasWrflDnon
		{
			duty = 1/2 * 3;
		};
		class AmovPercMevaSrasWpstDf: SprintCivilBaseDf
		{
			duty = 1/2 * 0.8;
		};
		class AmovPknlMevaSrasWpstDf: AmovPercMevaSrasWpstDf
		{
			duty = 1/2 * 1;
		};
		class AmovPpneMstpSrasWpstDnon_AmovPpneMevaSlowWpstDl: AmovPpneMstpSrasWpstDnon
		{
			duty = 1/2 * 2.5;
		};
		class AmovPercMevaSnonWnonDf: SprintCivilBaseDf
		{
			duty = 1/2 * 0.6;
		};
		class AmovPknlMevaSnonWnonDf: SprintCivilBaseDf
		{
			duty = 1/2 * 0.8;
		};
		class AmovPpneMstpSnonWnonDnon_AmovPpneMevaSnonWnonDl: AidlPpneMstpSnonWnonDnon_G0S
		{
			duty = 1/2 * 2;
		};
		class AmovPpneMstpSnonWnonDnon_AmovPpneMevaSnonWnonDr: AidlPpneMstpSnonWnonDnon_G0S
		{
			duty = 1/2 * 2;
		};
		class AmovPercMstpSnonWnonDnon_exercisekneeBendA: CutSceneAnimationBase
		{
			duty = 1/2 * 0.55;
		};
		class AmovPercMstpSnonWnonDnon_exercisePushup: CutSceneAnimationBase
		{
			duty = 1/2 * 0.5;
		};
		class TestDance: AmovPercMstpSrasWrflDnon
		{
			duty = 2 * -0.7;
		};
		class TestSurrender: TestDance
		{
			duty = 2 * -1;
		};
		class AwopPercMstpSgthWrflDnon_Start1: Default
		{
			duty = 1/2 * 0.6;
		};
		class AmovPercMrunSlowWrflDf_AmovPercMstpSrasWrflDnon_gthStart: Default
		{
			duty = 1/2 * 0.6;
		};
		class AmovPercMwlkSrasWrflDf_AmovPercMstpSrasWrflDnon_gthStart: Default
		{
			duty = 1/2 * 0.6;
		};
		class AwopPknlMstpSgthWrflDnon_Start: Default
		{
			duty = 1/2 * 0.6;
		};
		class AwopPpneMstpSgthWrflDnon_Fast_Start: Default
		{
			duty = 1/2 * 0.6;
		};
		class AwopPpneMstpSgthWpstDnon_Fast_Start: Default
		{
			duty = 1/2 * 0.6;
		};
		class AwopPpneMstpSgthWnonDnon_Fast_Start: Default
		{
			duty = 1/2 * 0.6;
		};
		class AwopPpneMstpSgthWrflDnon_Start: Default
		{
			duty = 1/2 * 0.6;
		};
		class AwopPercMstpSgthWpstDnon_Part1: Default
		{
			duty = 1/2 * 0.6;
		};
		class AmovPercMrunSlowWpstDf_AmovPercMstpSrasWpstDnon_gthStart: Default
		{
			duty = 1/2 * 0.6;
		};
		class AmovPercMwlkSrasWpstDf_AwopPercMrunSgthWnonDf_1: Default
		{
			duty = 1/2 * 0.6;
		};
		class AmovPercMwlkSrasWpstDf_AmovPercMstpSrasWpstDnon_gthStart: Default
		{
			duty = 1/2 * 0.6;
		};
		class AwopPknlMstpSgthWpstDnon_Part1: Default
		{
			duty = 1/2 * 0.6;
		};
		class AwopPpneMstpSgthWpstDnon_Part1: Default
		{
			duty = 1/2 * 0.6;
		};
		class AwopPercMstpSgthWnonDnon_start: Default
		{
			duty = 1/2 * 0.6;
		};
		class AmovPercMrunSnonWnonDf_AmovPercMstpSnonWnonDnon_gthStart: Default
		{
			duty = 1/2 * 0.6;
		};
		class AwopPpneMstpSgthWnonDnon_start: Default
		{
			duty = 1/2 * 0.6;
		};
		class LadderCivilStatic: StandBase
		{
			duty = 2 * -0.4;
		};
		class LadderCivilUpLoop: LadderCivilStatic
		{
			duty = 1/2 * 0.4;
		};
		class LadderCivilDownLoop: LadderCivilUpLoop
		{
			duty = 1/2 * 0;
		};
		class AswmPercMstpSnonWnonDnon: AmovPercMstpSrasWrflDnon
		{
			duty = 2 * -0.2;
		};
		class AswmPercMstpSnonWnonDnon_goup: AswmPercMstpSnonWnonDnon
		{
			duty = 1/2 * 0.4;
		};
		class AswmPercMstpSnonWnonDnon_godown: AswmPercMstpSnonWnonDnon
		{
			duty = 1/2 * 0.8;
		};
		class AswmPercMstpSnonWnonDnon_putDown: AswmPercMstpSnonWnonDnon
		{
			duty = 1/2 * 0.3;
		};
		class AswmPercMrunSnonWnonDf: AswmPercMstpSnonWnonDnon
		{
			duty = 1/2 * 0.6;
		};
		class AswmPercMstpSnonWnonDnon_AswmPercMrunSnonWnonDf: AswmPercMstpSnonWnonDnon
		{
			duty = 1/2 * 0.3;
		};
		class AsswPercMstpSnonWnonDnon_goup: AsswPercMstpSnonWnonDnon
		{
			duty = 1/2 * 0.1;
		};
		class AsswPercMstpSnonWnonDnon_goDown: AsswPercMstpSnonWnonDnon
		{
			duty = 1/2 * 0.1;
		};
		class AsswPercMstpSnonWnonDnon_putDown: AsswPercMstpSnonWnonDnon
		{
			duty = 1/2 * 0.1;
		};
		class AsswPercMrunSnonWnonDf: AsswPercMstpSnonWnonDnon
		{
			duty = 1/2 * 0.3;
		};
		class AsswPercMstpSnonWnonDnon_AsswPercMrunSnonWnonDf: AsswPercMstpSnonWnonDnon
		{
			duty = 1/2 * 0.1;
		};
		class AbswPercMstpSnonWnonDnon_goup: AbswPercMstpSnonWnonDnon
		{
			duty = 1/2 * 0.3;
		};
		class AbswPercMstpSnonWnonDnon_goDown: AbswPercMstpSnonWnonDnon
		{
			duty = 1/2 * 0.3;
		};
		class AbswPercMstpSnonWnonDnon_putDown: AbswPercMstpSnonWnonDnon
		{
			duty = 1/2 * 0.3;
		};
		class AbswPercMrunSnonWnonDf: AbswPercMstpSnonWnonDnon
		{
			duty = 1/2 * 0.3;
		};
		class AbswPercMstpSnonWnonDnon_AbswPercMrunSnonWnonDf: AbswPercMstpSnonWnonDnon
		{
			duty = 1/2 * 0.3;
		};
		class AdvePercMstpSnonWnonDnon_GetInSDV: AdvePercMstpSnonWnonDnon
		{
			duty = 1/2 * 0.5;
		};
		class AdvePercMstpSnonWnonDnon_GetOutSDV: AdvePercMstpSnonWnonDnon
		{
			duty = 1/2 * 0.5;
		};
		class AdvePercMstpSnonWnonDnon_goup: AdvePercMstpSnonWnonDnon
		{
			duty = 1/2 * 0.4;
		};
		class AdvePercMstpSnonWnonDnon_godown: AdvePercMstpSnonWnonDnon
		{
			duty = 1/2 * 0.8;
		};
		class AmovPercMwlkSrasWrflDf_AdvePercMrunSnonWnonDf: AmovPercMstpSrasWrflDnon
		{
			duty = 1/2 * 0.06;
		};
		class AdvePercMstpSnonWnonDnon_turnL: AdvePercMstpSnonWnonDnon
		{
			duty = 2 * -0.2;
		};
		class AdvePercMstpSnonWnonDnon_turnR: AdvePercMstpSnonWnonDnon
		{
			duty = 2 * -0.2;
		};
		class AdvePercMwlkSnonWnonDf: AdvePercMstpSnonWnonDnon
		{
			duty = 2 * -0.2;
		};
		class AdvePercMtacSnonWnonDf: AdvePercMstpSnonWnonDnon
		{
			duty = 2 * -0.2;
		};
		class AdvePercMrunSnonWnonDf: AdvePercMstpSnonWnonDnon
		{
			duty = 1/2 * 0.3;
		};
		class AdvePercMsprSnonWnonDf: AdvePercMrunSnonWnonDf
		{
			duty = 1/2 * 0.8;
		};
		class AdvePercMstpSnonWrflDnon_GetInSDV: AdvePercMstpSnonWrflDnon
		{
			duty = 1/2 * 0.5;
		};
		class AdvePercMstpSnonWrflDnon_GetOutSDV: AdvePercMstpSnonWrflDnon
		{
			duty = 1/2 * 0.5;
		};
		class AdvePercMstpSnonWrflDnon_goup: AdvePercMstpSnonWrflDnon
		{
			duty = 1/2 * 0.6;
		};
		class AdvePercMstpSnonWrflDnon_godown: AdvePercMstpSnonWrflDnon
		{
			duty = 1/2 * 1;
		};
		class AmovPercMwlkSrasWrflDf_AdvePercMrunSnonWrflDf: AmovPercMstpSrasWrflDnon
		{
			duty = 1/2 * 0.06;
		};
		class AdvePercMstpSnonWrflDnon_turnL: AdvePercMstpSnonWrflDnon
		{
			duty = 2 * -0.1;
		};
		class AdvePercMstpSnonWrflDnon_turnR: AdvePercMstpSnonWrflDnon
		{
			duty = 2 * -0.1;
		};
		class AdvePercMwlkSnonWrflDf: AdvePercMstpSnonWrflDnon
		{
			duty = 2 * -0.2;
		};
		class AdvePercMtacSnonWrflDf: AdvePercMstpSnonWrflDnon
		{
			duty = 2 * -0.2;
		};
		class AdvePercMrunSnonWrflDf: AdvePercMstpSnonWrflDnon
		{
			duty = 1/2 * 0.6;
		};
		class AdvePercMsprSnonWrflDf: AdvePercMrunSnonWrflDf
		{
			duty = 1/2 * 1;
		};
		class AsdvPercMstpSnonWnonDnon_GetInSDV: AsdvPercMstpSnonWnonDnon
		{
			duty = 1/2 * 0.5;
		};
		class AsdvPercMstpSnonWnonDnon_GetOutSDV: AsdvPercMstpSnonWnonDnon
		{
			duty = 1/2 * 0.5;
		};
		class AsdvPercMstpSnonWnonDnon_goup: AsdvPercMstpSnonWnonDnon
		{
			duty = 1/2 * 0.4;
		};
		class AsdvPercMstpSnonWnonDnon_godown: AsdvPercMstpSnonWnonDnon
		{
			duty = 1/2 * 0.8;
		};
		class AsdvPercMstpSnonWnonDnon_turnL: AsdvPercMstpSnonWnonDnon
		{
			duty = 2 * -0.2;
		};
		class AsdvPercMstpSnonWnonDnon_turnR: AsdvPercMstpSnonWnonDnon
		{
			duty = 2 * -0.2;
		};
		class AsdvPercMwlkSnonWnonDf: AsdvPercMstpSnonWnonDnon
		{
			duty = 2 * -0.2;
		};
		class AsdvPercMtacSnonWnonDf: AsdvPercMstpSnonWnonDnon
		{
			duty = 2 * -0.2;
		};
		class AsdvPercMrunSnonWnonDf: AsdvPercMstpSnonWnonDnon
		{
			duty = 1/2 * 0.2;
		};
		class AsdvPercMsprSnonWnonDf: AsdvPercMrunSnonWnonDf
		{
			duty = 1/2 * 0.8;
		};
		class AsdvPercMstpSnonWrflDnon_GetInSDV: AsdvPercMstpSnonWrflDnon
		{
			duty = 1/2 * 0.5;
		};
		class AsdvPercMstpSnonWrflDnon_GetOutSDV: AsdvPercMstpSnonWrflDnon
		{
			duty = 1/2 * 0.5;
		};
		class AsdvPercMstpSnonWrflDnon_goup: AsdvPercMstpSnonWrflDnon
		{
			duty = 1/2 * 0.6;
		};
		class AsdvPercMstpSnonWrflDnon_godown: AsdvPercMstpSnonWrflDnon
		{
			duty = 1/2 * 1;
		};
		class AsdvPercMstpSnonWrflDnon_turnL: AsdvPercMstpSnonWrflDnon
		{
			duty = 2 * -0.1;
		};
		class AsdvPercMstpSnonWrflDnon_turnR: AsdvPercMstpSnonWrflDnon
		{
			duty = 2 * -0.1;
		};
		class AsdvPercMwlkSnonWrflDf: AsdvPercMstpSnonWrflDnon
		{
			duty = 2 * -0.2;
		};
		class AsdvPercMtacSnonWrflDf: AsdvPercMstpSnonWrflDnon
		{
			duty = 2 * -0.2;
		};
		class AsdvPercMrunSnonWrflDf: AsdvPercMstpSnonWrflDnon
		{
			duty = 1/2 * 0.4;
		};
		class AsdvPercMsprSnonWrflDf: AsdvPercMrunSnonWrflDf
		{
			duty = 1/2 * 0.6;
		};
		class AbdvPercMstpSnonWnonDnon_GetInSDV: AbdvPercMstpSnonWnonDnon
		{
			duty = 1/2 * 0.06;
		};
		class AbdvPercMstpSnonWnonDnon_GetOutSDV: AbdvPercMstpSnonWnonDnon
		{
			duty = 1/2 * 0.06;
		};
		class AbdvPercMstpSnonWnonDnon_goup: AbdvPercMstpSnonWnonDnon
		{
			duty = 1/2 * 0.06;
		};
		class AbdvPercMstpSnonWnonDnon_godown: AbdvPercMstpSnonWnonDnon
		{
			duty = 1/2 * 0.06;
		};
		class AbdvPercMstpSnonWnonDnon_turnL: AbdvPercMstpSnonWnonDnon
		{
			duty = 1/2 * 0.06;
		};
		class AbdvPercMstpSnonWnonDnon_turnR: AbdvPercMstpSnonWnonDnon
		{
			duty = 1/2 * 0.06;
		};
		class AbdvPercMwlkSnonWnonDf: AbdvPercMstpSnonWnonDnon
		{
			duty = 2 * -0.2;
		};
		class AbdvPercMtacSnonWnonDf: AbdvPercMstpSnonWnonDnon
		{
			duty = 2 * -0.2;
		};
		class AbdvPercMrunSnonWnonDf: AbdvPercMstpSnonWnonDnon
		{
			duty = 2 * -0.2;
		};
		class AbdvPercMsprSnonWnonDf: AbdvPercMrunSnonWnonDf
		{
			duty = 1/2 * 0.6;
		};
		class AbdvPercMstpSnonWrflDnon_GetInSDV: AbdvPercMstpSnonWrflDnon
		{
			duty = 1/2 * 0.06;
		};
		class AbdvPercMstpSnonWrflDnon_GetOutSDV: AbdvPercMstpSnonWrflDnon
		{
			duty = 1/2 * 0.06;
		};
		class AbdvPercMstpSnonWrflDnon_goup: AbdvPercMstpSnonWrflDnon
		{
			duty = 1/2 * 0.06;
		};
		class AbdvPercMstpSnonWrflDnon_godown: AbdvPercMstpSnonWrflDnon
		{
			duty = 1/2 * 0.06;
		};
		class AbdvPercMstpSnonWrflDnon_turnL: AbdvPercMstpSnonWrflDnon
		{
			duty = 1/2 * 0.06;
		};
		class AbdvPercMstpSnonWrflDnon_turnR: AbdvPercMstpSnonWrflDnon
		{
			duty = 1/2 * 0.06;
		};
		class AbdvPercMwlkSnonWrflDf: AbdvPercMstpSnonWrflDnon
		{
			duty = 2 * -0.2;
		};
		class AbdvPercMtacSnonWrflDf: AbdvPercMstpSnonWrflDnon
		{
			duty = 2 * -0.2;
		};
		class AbdvPercMrunSnonWrflDf: AbdvPercMstpSnonWrflDnon
		{
			duty = 2 * -0.2;
		};
		class AbdvPercMsprSnonWrflDf: AbdvPercMrunSnonWrflDf
		{
			duty = 1/2 * 0.6;
		};
		class AmovPercMwlkSoptWbinDf: AmovPercMstpSoptWbinDnon
		{
			duty = 2 * -0.4;
		};
		class AmovPercMrunSnonWbinDf: AmovPercMwlkSoptWbinDf
		{
			duty = 1/2 * 0.1;
		};
		class AmovPercMevaSnonWbinDf: AmovPercMrunSnonWbinDf
		{
			duty = 1/2 * 0.6;
		};
		class AovrPercMstpSoptWbinDf: AmovPercMstpSoptWbinDnon
		{
			duty = 1/2 * 2;
		};
		class AmovPknlMwlkSoptWbinDf: AmovPknlMstpSoptWbinDnon
		{
			duty = 1/2 * 0.1;
		};
		class AmovPknlMrunSnonWbinDf: AmovPknlMwlkSoptWbinDf
		{
			duty = 1/2 * 0.2;
		};
		class AmovPknlMevaSnonWbinDf: AmovPknlMrunSnonWbinDf
		{
			duty = 1/2 * 0.8;
		};
		class AmovPpneMwlkSoptWbinDf: AmovPpneMstpSoptWbinDnon
		{
			duty = 1/2 * 0.3;
		};
		class AmovPpneMrunSnonWbinDf: AmovPpneMwlkSoptWbinDf
		{
			duty = 1/2 * 0.3;
		};
		class AmovPpneMevaSnonWbinDf: AmovPpneMrunSnonWbinDf
		{
			duty = 1/2 * 1;
		};
		class AmovPercMwlkSoptWbinDf_rfl: AwopPercMstpSoptWbinDnon_rfl
		{
			duty = 2 * -0.1;
		};
		class AmovPercMrunSnonWbinDf_rfl: AmovPercMwlkSoptWbinDf_rfl
		{
			duty = 1/2 * 0.4;
		};
		class AmovPercMevaSnonWbinDf_rfl: AmovPercMrunSnonWbinDf_rfl
		{
			duty = 1/2 * 1;
		};
		class AmovPknlMwlkSoptWbinDf_rfl: AwopPknlMstpSoptWbinDnon_rfl
		{
			duty = 2 * -0.1;
		};
		class AmovPknlMrunSnonWbinDf_rfl: AmovPknlMwlkSoptWbinDf_rfl
		{
			duty = 1/2 * 0.4;
		};
		class AmovPknlMevaSnonWbinDf_rfl: AmovPknlMrunSnonWbinDf_rfl
		{
			duty = 1/2 * 1;
		};
		class AmovPpneMwlkSoptWbinDf_rfl: AwopPpneMstpSoptWbinDnon_rfl
		{
			duty = 2 * -0.1;
		};
		class AmovPpneMrunSnonWbinDf_rfl: AmovPpneMwlkSoptWbinDf_rfl
		{
			duty = 1/2 * 0.4;
		};
		class AmovPpneMevaSnonWbinDf_rfl: AmovPpneMrunSnonWbinDf_rfl
		{
			duty = 1/2 * 1;
		};
		class AmovPercMwlkSoptWbinDf_pst: AwopPercMstpSoptWbinDnon_pst
		{
			duty = 2 * -0.1;
		};
		class AmovPercMrunSnonWbinDf_pst: AmovPercMwlkSoptWbinDf_pst
		{
			duty = 1/2 * 0.4;
		};
		class AmovPercMevaSnonWbinDf_pst: AmovPercMrunSnonWbinDf_pst
		{
			duty = 1/2 * 1;
		};
		class AmovPknlMwlkSoptWbinDf_pst: AwopPknlMstpSoptWbinDnon_pst
		{
			duty = 2 * -0.1;
		};
		class AmovPknlMrunSnonWbinDf_pst: AmovPknlMwlkSoptWbinDf_pst
		{
			duty = 1/2 * 0.4;
		};
		class AmovPknlMevaSnonWbinDf_pst: AmovPknlMrunSnonWbinDf_pst
		{
			duty = 1/2 * 1;
		};
		class AmovPpneMwlkSoptWbinDf_pst: AwopPpneMstpSoptWbinDnon_pst
		{
			duty = 2 * -0.1;
		};
		class AmovPpneMrunSnonWbinDf_pst: AmovPpneMwlkSoptWbinDf_pst
		{
			duty = 1/2 * 0.4;
		};
		class AmovPpneMevaSnonWbinDf_pst: AmovPpneMrunSnonWbinDf_pst
		{
			duty = 1/2 * 1;
		};
		class AmovPercMwlkSoptWbinDf_lnr: AwopPercMstpSoptWbinDnon_lnr
		{
			duty = 2 * -0.1;
		};
		class AmovPercMrunSnonWbinDf_lnr: AmovPercMwlkSoptWbinDf_lnr
		{
			duty = 1/2 * 0.4;
		};
		class AmovPercMevaSnonWbinDf_lnr: AmovPercMrunSnonWbinDf_lnr
		{
			duty = 1/2 * 1;
		};
		class AmovPknlMwlkSoptWbinDf_lnr: AwopPknlMstpSoptWbinDnon_lnr
		{
			duty = 2 * -0.1;
		};
		class AmovPknlMrunSnonWbinDf_lnr: AmovPknlMwlkSoptWbinDf_lnr
		{
			duty = 1/2 * 0.4;
		};
		class AmovPknlMevaSnonWbinDf_lnr: AmovPknlMrunSnonWbinDf_lnr
		{
			duty = 1/2 * 1;
		};
		class AmovPpneMwlkSoptWbinDf_lnr: AwopPpneMstpSoptWbinDnon_lnr
		{
			duty = 2 * -0.1;
		};
		class AmovPpneMrunSnonWbinDf_lnr: AmovPpneMwlkSoptWbinDf_lnr
		{
			duty = 1/2 * 0.4;
		};
		class AmovPpneMevaSnonWbinDf_lnr: AmovPpneMrunSnonWbinDf_lnr
		{
			duty = 1/2 * 1;
		};
		class AmovPercMwlkSoptWbinDf_non: AwopPercMstpSoptWbinDnon_non
		{
			duty = 2 * -0.1;
		};
		class AmovPercMrunSnonWbinDf_non: AmovPercMwlkSoptWbinDf_non
		{
			duty = 1/2 * 0.4;
		};
		class AmovPercMevaSnonWbinDf_non: AmovPercMrunSnonWbinDf_non
		{
			duty = 1/2 * 1;
		};
		class AmovPknlMwlkSoptWbinDf_non: AwopPknlMstpSoptWbinDnon_non
		{
			duty = 2 * -0.1;
		};
		class AmovPknlMrunSnonWbinDf_non: AmovPknlMwlkSoptWbinDf_non
		{
			duty = 1/2 * 0.4;
		};
		class AmovPknlMevaSnonWbinDf_non: AmovPknlMrunSnonWbinDf_non
		{
			duty = 1/2 * 1;
		};
		class AmovPpneMwlkSoptWbinDf_non: AwopPpneMstpSoptWbinDnon_non
		{
			duty = 2 * -0.1;
		};
		class AmovPpneMrunSnonWbinDf_non: AmovPpneMwlkSoptWbinDf_non
		{
			duty = 1/2 * 0.4;
		};
		class AmovPpneMevaSnonWbinDf_non: AmovPpneMrunSnonWbinDf_non
		{
			duty = 1/2 * 1;
		};
		class AmovPercMstpSoptWbinDnon_AmovPknlMstpSoptWbinDnon: AmovPknlMstpSoptWbinDnon
		{
			duty = 1/2 * 0.1;
		};
		class AmovPercMstpSoptWbinDnon_AmovPpneMstpSoptWbinDnon: AmovPpneMstpSoptWbinDnon
		{
			duty = 1/2 * 0.6;
		};
		class AmovPknlMstpSoptWbinDnon_AmovPercMstpSoptWbinDnon: AmovPercMstpSoptWbinDnon
		{
			duty = 1/2 * 0.2;
		};
		class AmovPknlMstpSoptWbinDnon_AmovPpneMstpSoptWbinDnon: AmovPpneMstpSoptWbinDnon
		{
			duty = 1/2 * 0.1;
		};
		class AmovPpneMstpSoptWbinDnon_AmovPercMstpSoptWbinDnon: AmovPercMstpSoptWbinDnon
		{
			duty = 1/2 * 1;
		};
		class AmovPpneMstpSoptWbinDnon_AmovPknlMstpSoptWbinDnon: AmovPknlMstpSoptWbinDnon
		{
			duty = 1/2 * 0.6;
		};
		class AinjPpneMstpSnonWrflDnon_injuredHealed: AinjPpneMstpSnonWrflDnon
		{
			duty = 1/2 * 0;
		};
		class AinjPpneMstpSnonWrflDnon_rolltoback: AinjPpneMstpSnonWrflDnon
		{
			duty = 1/2 * 3;
		};
		class AmovPercMstpSlowWrflDnon_AcinPknlMwlkSlowWrflDb_1: Default
		{
			duty = 1/2 * 0.2;
		};
		class AcinPknlMstpSrasWrflDnon: AmovPercMstpSlowWrflDnon_AcinPknlMwlkSlowWrflDb_1
		{
			duty = 2 * -0.3;
		};
		class DraggerBase: Default
		{
			duty = 1/2 * 0.2;
		};
		class AcinPknlMwlkSlowWrflDb_AmovPercMstpSlowWrflDnon: AmovPercMstpSlowWrflDnon_AcinPknlMwlkSlowWrflDb_1
		{
			duty = 2 * -0.5;
		};
		class AcinPknlMwlkSrasWrflDb: AmovPercMstpSlowWrflDnon_AcinPknlMwlkSlowWrflDb_1
		{
			duty = 1/2 * 0.6;
		};
		class AcinPercMrunSrasWrflDf: InjuredMovedBase
		{
			duty = 1/2 * 0.5;
		};
		class AcinPercMrunSrasWrflDf_agony: AcinPercMrunSrasWrflDf_death
		{
			duty = 1/2 * 10;
		};
		class AcinPknlMstpSrasWrflDnon_AcinPercMrunSrasWrflDnon: InjuredMovedBase
		{
			duty = 1/2 * 1;
		};
		class AcinPercMstpSrasWrflDnon: AcinPknlMstpSrasWrflDnon_AcinPercMrunSrasWrflDnon
		{
			duty = 2 * -0.2;
		};
		class AinjPfalMstpSnonWrflDnon_carried_still: AinjPfalMstpSnonWrflDnon_carried_Up
		{
			duty = 1/2 * 0;
		};
		class AmovPpneMsprSnonWnonDf_injured: AmovPpneMstpSnonWnonDnon_injured
		{
			duty = 1/2 * 2.6;
		};
		class AinjPpneMstpSnonWnonDnon_injuredHealed: AinjPpneMstpSnonWnonDnon
		{
			duty = 1/2 * 0;
		};
		class AinjPpneMstpSnonWnonDnon_rolltoback: AinjPpneMstpSnonWnonDnon
		{
			duty = 1/2 * 3;
		};
		class AmovPercMstpSnonWnonDnon_AcinPknlMwlkSnonWnonDb_1: Default
		{
			duty = 1/2 * 0.2;
		};
		class AcinPknlMstpSnonWnonDnon: AmovPercMstpSnonWnonDnon_AcinPknlMwlkSnonWnonDb_1
		{
			duty = 2 * -0.3;
		};
		class AcinPknlMwlkSnonWnonDb_AmovPercMstpSnonWnonDnon: AmovPercMstpSnonWnonDnon_AcinPknlMwlkSnonWnonDb_1
		{
			duty = 2 * -0.5;
		};
		class AcinPknlMwlkSnonWnonDb: AmovPercMstpSnonWnonDnon_AcinPknlMwlkSnonWnonDb_1
		{
			duty = 1/2 * 6;
		};
		class AcinPercMrunSnonWnonDf: InjuredMovedBase
		{
			duty = 1/2 * 0.5;
		};
		class AcinPercMrunSnonWnonDf_agony: AcinPercMrunSnonWnonDf_death
		{
			duty = 1/2 * 10;
		};
		class AcinPknlMstpSnonWnonDnon_AcinPercMrunSnonWnonDnon: InjuredMovedBase
		{
			duty = 1/2 * 1;
		};
		class AcinPercMstpSnonWnonDnon: AcinPknlMstpSnonWnonDnon_AcinPercMrunSnonWnonDnon
		{
			duty = 2 * -0.2;
		};
		class AinjPfalMstpSnonWnonDnon_carried_still: AinjPfalMstpSnonWnonDnon_carried_Up
		{
			duty = 1/2 * 0;
		};
		class AmovPercMstpSrasWlnrDnon: AmovPknlMstpSrasWlnrDnon
		{
			duty = 2 * -0.2;
		};
		class ReloadRPG: AidlPercMstpSrasWlnrDnon_G0S
		{
			duty = 1/2 * 0.6;
		};
		class ReloadRPG7V: AidlPercMstpSrasWlnrDnon_G0S
		{
			duty = 1/2 * 0.6;
		};
		class AovrPercMstpSrasWlnrDf: AmovPercMstpSrasWlnrDnon
		{
			duty = 1/2 * 2;
		};
		class AovrPercMstpSlowWlnrDf: AmovPercMstpSlowWlnrDnon
		{
			duty = 1/2 * 2;
		};
		class AmovPercMwlkSlowWlnrDf: AmovPercMstpSlowWlnrDnon
		{
			duty = 2 * -0.2;
		};
		class AmovPercMwlkSrasWlnrDf: AidlPercMstpSrasWlnrDnon_G0S
		{
			duty = 2 * -0.1;
		};
		class AmovPercMtacSrasWlnrDf: AidlPercMstpSrasWlnrDnon_G0S
		{
			duty = 1/2 * 0.5;
		};
		class AmovPercMrunSrasWlnrDf: AmovPercMrunSlowWlnrDf
		{
			duty = 1/2 * 0.4;
		};
		class AmovPercMrunSrasWlnrDfl: AmovPercMrunSrasWlnrDf
		{
			duty = 1/2 * 0.4;
		};
		class AmovPercMrunSrasWlnrDfr: AmovPercMrunSrasWlnrDf
		{
			duty = 1/2 * 0.4;
		};
		class AmovPercMstpSrasWlnrDnon_turnL: AmovPercMstpSrasWlnrDnon
		{
			duty = 1/2 * 0;
		};
		class AmovPercMstpSrasWlnrDnon_turnR: AmovPercMstpSrasWlnrDnon
		{
			duty = 1/2 * 0;
		};
		class AadjPercMstpSrasWrflDleft_AadjPknlMstpSrasWrflDleft: AadjPknlMstpSrasWrflDleft
		{
			duty = 1/2 * 0.3;
		};
		class AadjPknlMstpSrasWrflDleft_AadjPercMstpSrasWrflDleft: AadjPercMstpSrasWrflDleft
		{
			duty = 1/2 * 0.3;
		};
		class AadjPercMstpSrasWrflDright_AadjPknlMstpSrasWrflDright: AadjPknlMstpSrasWrflDright
		{
			duty = 1/2 * 0.3;
		};
		class AadjPknlMstpSrasWrflDright_AadjPercMstpSrasWrflDright: AadjPercMstpSrasWrflDright
		{
			duty = 1/2 * 0.3;
		};
		class AadjPercMstpSrasWrflDdown_AadjPknlMstpSrasWrflDdown: AadjPknlMstpSrasWrflDdown
		{
			duty = 1/2 * 0.3;
		};
		class AadjPercMstpSrasWrflDdown_AadjPknlMstpSrasWrflDup: AadjPknlMstpSrasWrflDup
		{
			duty = 1/2 * 0.3;
		};
		class AadjPknlMstpSrasWrflDdown_AadjPercMstpSrasWrflDdown: AadjPercMstpSrasWrflDdown
		{
			duty = 1/2 * 0.3;
		};
		class AadjPercMstpSrasWrflDup_AadjPknlMstpSrasWrflDup: AadjPknlMstpSrasWrflDup
		{
			duty = 1/2 * 0.3;
		};
		class AadjPknlMstpSrasWrflDup_AadjPercMstpSrasWrflDup: AadjPercMstpSrasWrflDup
		{
			duty = 1/2 * 0.3;
		};
		class AadjPknlMstpSrasWrflDup_AadjPercMstpSrasWrflDdown: AadjPercMstpSrasWrflDdown
		{
			duty = 1/2 * 0.3;
		};
		class AadjPercMstpSrasWpstDleft_AadjPknlMstpSrasWpstDleft: AadjPknlMstpSrasWpstDleft
		{
			duty = 1/2 * 0.3;
		};
		class AadjPknlMstpSrasWpstDleft_AadjPercMstpSrasWpstDleft: AadjPercMstpSrasWpstDleft
		{
			duty = 1/2 * 0.3;
		};
		class AadjPercMstpSrasWpstDright_AadjPknlMstpSrasWpstDright: AadjPknlMstpSrasWpstDright
		{
			duty = 1/2 * 0.3;
		};
		class AadjPknlMstpSrasWpstDright_AadjPercMstpSrasWpstDright: AadjPercMstpSrasWpstDright
		{
			duty = 1/2 * 0.3;
		};
		class AadjPercMstpSrasWpstDdown_AadjPknlMstpSrasWpstDdown: AadjPknlMstpSrasWpstDdown
		{
			duty = 1/2 * 0.3;
		};
		class AadjPercMstpSrasWpstDdown_AadjPknlMstpSrasWpstDup: AadjPknlMstpSrasWpstDup
		{
			duty = 1/2 * 0.3;
		};
		class AadjPknlMstpSrasWpstDdown_AadjPercMstpSrasWpstDdown: AadjPercMstpSrasWpstDdown
		{
			duty = 1/2 * 0.3;
		};
		class AadjPercMstpSrasWpstDup_AadjPknlMstpSrasWpstDup: AadjPknlMstpSrasWpstDup
		{
			duty = 1/2 * 0.3;
		};
		class AadjPknlMstpSrasWpstDup_AadjPercMstpSrasWpstDup: AadjPercMstpSrasWpstDup
		{
			duty = 1/2 * 0.3;
		};
		class AadjPknlMstpSrasWpstDup_AadjPercMstpSrasWpstDdown: AadjPercMstpSrasWpstDdown
		{
			duty = 1/2 * 0.3;
		};
		class AadjPknlMstpSrasWpstDdown_AadjPpneMstpSrasWpstDup: AadjPpneMstpSrasWpstDup
		{
			duty = 1/2 * 0.3;
		};
		class AadjPpneMstpSrasWpstDup_AadjPknlMstpSrasWpstDdown: AadjPknlMstpSrasWpstDdown
		{
			duty = 1/2 * 0.3;
		};
		class LaceyTest2a: CutSceneAnimationBase
		{
			duty = 2 * -0.8;
		};
		class ApanPercMrunSnonWnonDf: ApanPercMstpSnonWnonDnon
		{
			duty = 1/2 * 0.1;
		};
		class ApanPknlMrunSnonWnonDf: ApanPknlMstpSnonWnonDnon
		{
			duty = 1/2 * 0.2;
		};
		class ApanPpneMrunSnonWnonDf: ApanPpneMstpSnonWnonDnon
		{
			duty = 1/2 * 0.3;
		};
		class ApanPercMwlkSnonWnonDf: ApanPercMrunSnonWnonDf
		{
			duty = 2 * -0.6;
		};
		class ApanPknlMwlkSnonWnonDf: ApanPknlMrunSnonWnonDf
		{
			duty = 1/2 * 0;
		};
		class ApanPercMsprSnonWnonDf: ApanPercMrunSnonWnonDf
		{
			duty = 1/2 * 0.6;
		};
		class ApanPknlMsprSnonWnonDf: ApanPknlMrunSnonWnonDf
		{
			duty = 1/2 * 0.8;
		};
		class ApanPpneMsprSnonWnonDf: ApanPpneMrunSnonWnonDf
		{
			duty = 1/2 * 1;
		};
		class ApanPercMstpSnonWnonDnon_ApanPknlMstpSnonWnonDnon: ApanPknlMstpSnonWnonDnon
		{
			duty = 1/2 * 0.1;
		};
		class ApanPercMstpSnonWnonDnon_ApanPpneMstpSnonWnonDnon: ApanPpneMstpSnonWnonDnon
		{
			duty = 1/2 * 0.6;
		};
		class ApanPknlMstpSnonWnonDnon_ApanPercMstpSnonWnonDnon: ApanPercMstpSnonWnonDnon
		{
			duty = 1/2 * 0.2;
		};
		class ApanPknlMstpSnonWnonDnon_ApanPpneMstpSnonWnonDnon: ApanPpneMstpSnonWnonDnon
		{
			duty = 1/2 * 0.1;
		};
		class ApanPpneMstpSnonWnonDnon_ApanPercMstpSnonWnonDnon: ApanPercMstpSnonWnonDnon
		{
			duty = 1/2 * 1;
		};
		class ApanPpneMstpSnonWnonDnon_ApanPknlMstpSnonWnonDnon: ApanPknlMstpSnonWnonDnon
		{
			duty = 1/2 * 0.4;
		};
	};
};
*/
