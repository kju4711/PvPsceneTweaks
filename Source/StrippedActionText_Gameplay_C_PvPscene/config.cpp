class CfgPatches
{
	class StrippedActionText_Gameplay_C_PvPscene
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Data_F_Loadorder","A3_Data_F_Curator_Loadorder","A3_Data_F_Kart_Loadorder","A3_Data_F_Bootcamp_Loadorder","A3_Data_F_Heli_Loadorder","A3_Data_F_Mark_Loadorder","A3_Data_F_Exp_A_Loadorder","A3_Data_F_Exp_B_Loadorder","A3_Data_F_Exp_Loadorder","A3_Data_F_Jets_Loadorder","A3_Data_F_Argo_Loadorder","A3_Data_F_Patrol_Loadorder","A3_Data_F_Orange_Loadorder","A3_Data_F_Tacops_Loadorder","A3_Data_F_Tank_Loadorder"};
		version = "2019-03-28";
	};
};
class CfgActions
{
	class None;

	class GetInCommander: None
	{
		text = "Commander %1";
	};
	class GetInDriver: None
	{
		text = "Driver %1";
	};
	class GetInPilot: None
	{
		text = "Pilot %1";
	};
	class GetInGunner: None
	{
		text = "Gunner %1";
	};
	class GetInCargo: None
	{
		text = "Cargo %1";
	};
	class GetInTurret: None
	{
		text = "%2 %1";
	};
	class MoveToDriver: None
	{
		text = "Driver";
	};
	class MoveToPilot: MoveToDriver
	{
		text = "Pilot";
	};
	class MoveToGunner: None
	{
		text = "Gunner";
	};
	class MoveToCommander: None
	{
		text = "Commander";
	};
	class MoveToCargo: None
	{
		text = "Cargo";
	};
	class MoveToTurret: None
	{
		text = "Turret";
	};
};
