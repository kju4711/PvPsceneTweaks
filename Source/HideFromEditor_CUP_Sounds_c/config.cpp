class CfgPatches
{
	class Optional_HideFromEditor_CUP_Sounds_c
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"CUP_A10_Data","CUP_Air2_Data","CUP_Air3_Data","CUP_Air_Data","CUP_Air_d_baf_Data","CUP_Air_e_Data","CUP_Air_pmc_Data","CUP_CA_animals2","CUP_Buildings2_Data","CUP_Buildings2_Ind_Cementworks_Data","CUP_Buildings_Data","CUP_Ca_acr_Data","CUP_Ca_e_Data","CUP_Ca_pmc_Data","CUP_Characters2_Data","CUP_Cti_buildings_Data","CUP_CA_Data","CUP_Data_baf_Data","CUP_dbe1_data","CUP_Hotfix_Data","CUP_L39_Data","CUP_CALanguage","CUP_CALanguage_ACR","CUP_CALanguage_missions","CUP_CALanguage_missions_e","CUP_Misc3_Data","CUP_Misc_acr_Data","CUP_Misc_e_Data","CUP_CA_MPA","CUP_CA_Plants2_Clutter","CUP_CA_Plants_E_Clutter","CUP_CARoads2","CUP_CARoads2Dam","CUP_CARoads","CUP_CARoads_E","CUP_CARoads_PMC","CUP_CARocks2","CUP_CARocks","CUP_CARocks_E","CUP_CASigns","CUP_Sounds_Data","CUP_Sounds_Config","CUP_Sounds_e_Data","CUP_Structures_Data","CUP_jbad_sounds_doors","cup_jbad_light_switches","CUP_jbad_sounds_misc","CUP_Structures_e_Data","CUP_Structures_pmc_Data","CUP_Tracked2_Data","CUP_Tracked_Data","CUP_Tracked_e_Data","CUP_CAFonts","CUP_Water2_Data","CUP_Water_Data","CUP_Weapons2_Data","CUP_Weapons_Data","CUP_Weapons_e_Data","CUP_Weapons_pmc_Data","CUP_Wheeled2_Data","CUP_Wheeled_Data","CUP_Wheeled_e_Data","CUP_A1AlwaysDummy","CUP_AiA_compat","CUP_StandaloneTerrains_Dummy","CUP_Terrains_Plants","CUP_Models_DBE1_Data","CUP_Anims_DBE1","CUP_HMMWV_DBE1","CUP_Mercenary_DBE1","CUP_Music_DBE1","CUP_NPCs_DBE1","CUP_ploty_DBE1","CUP_Prisoners_DBE1","CUP_Roads_DBE1","CUP_UH60Desert","CUP_TKOH_Dummy","CUP_Hsim_Language_H","CUP_Hsim_Language_missions_H","CUP_DBE1_Hotfix","CUP_CALanguage_e","CUP_CALanguage_PMC","CUP_CALanguage_missions_PMC","CUP_CAWater2_seafox_EP1","CUP_CA_Plants2","CUP_CA_Plants2_Plant","CUP_CA_Plants2_Tree","CUP_CA_Plants_E2","CUP_CA_Plants_E","CUP_CA_Plants_E_Misc","CUP_CA_Plants_E_Plant","CUP_CA_Plants_E_Tree","CUP_CASigns2","CUP_CAUI","CUP_CAWater2_LHD","CUP_Models_DBE1_Config","CUP_UI_DBE1","CUP_CA_Config","CUP_CAData_ParticleEffects","CUP_CAMisc","CUP_Misc_Data","CUP_CA_Plants2_Bush","CUP_CAPlants","CUP_CA_Plants_E_Bush","CUP_pond_test","CUP_Editor_Config","CUP_Editor_A1_Roads_Config","CUP_Editor_A2_Roads_Config","CUP_Editor_A2_Railway_Config","CUP_Editor_Sidewalks_Config","CUP_Misc_DBE1","CUP_Buildings_Config","CUP_CABuildings_Misc","CUP_Hotfix_Config","CUP_CA_QGClutterHotfix","CUP_CA_Hotfix_vez_ropa","CUP_jbad_sounds","CUP_Structures_Config","CUP_CAStructures_A_BuildingWIP","CUP_CAStructures_A_CraneCon","CUP_CAStructuresLand_A_MunicipalOffice","CUP_CAStructuresBarn_W","CUP_CAStructures_Castle","CUP_CAStructuresHouse","CUP_CAStructuresHouse_A_FuelStation","CUP_CAStructuresHouse_A_Hospital","CUP_CAStructuresHouse_A_Office01","CUP_CAStructuresHouse_A_Office02","CUP_CAStructuresHouse_a_stationhouse","CUP_CAStructuresHouse_Church_02","CUP_CAStructuresHouse_Church_03","CUP_CAStructuresHouse_Church_05R","CUP_CAStructuresHouse_HouseBT","CUP_CAStructuresHouse_HouseV2","CUP_CAStructuresHouse_HouseV","CUP_CAStructuresLand_Ind_Stack_Big","CUP_CAStructures_IndPipe1","CUP_CAStructuresInd_Quarry","CUP_Ind_SawMill","CUP_CAStructures_Mil","CUP_CAStructures_Misc","CUP_CAStructures_Misc_Armory","CUP_CAStructures_Misc_Armory_Armor_Popuptarget","CUP_CAStructures_Misc_Powerlines","CUP_CAStructures_Nav","CUP_CAStructuresLand_Nav_Boathouse","CUP_CAStructures_Proxy_BuildingParts","CUP_CAStructures_Proxy_Ruins","CUP_CAStructures_Rail","CUP_CAStructuresHouse_rail_station_big","CUP_CAStructures_Ruins","CUP_CAStructuresShed_Small","CUP_CAStructuresHouse_Shed_Ind","CUP_CAStructures_Wall","CUP_New_Buildings","CUP_Editor_Buildings_Config","CUP_Editor_Structures_Config","CUP_Kamenolom_DBE1","CUP_Pila_DBE1","CUP_Vysilac_DBE1","CUP_Zakladna_DBE1","CUP_Buildings2_Config","CUP_A_Crane_02","CUP_A_GeneralStore_01","CUP_CABuildings2_A_Pub","CUP_A_statue","CUP_Barn_Metal","CUP_CABuildingParts","CUP_CABuildingParts_Signs","CUP_CATEC","CUP_Church_01","CUP_Farm_Cowshed","CUP_Farm_WTower","CUP_CAHouseBlock_A","CUP_CAHouseBlock_B","CUP_CAHouseBlock_C","CUP_CAHouseBlock_D","CUP_HouseRuins","CUP_Ind_Dopravnik","CUP_Ind_Expedice","CUP_Ind_MalyKomin","CUP_Ind_Mlyn","CUP_Ind_Pec","CUP_ind_silomale","CUP_Ind_SiloVelke","CUP_Ind_Vysypka","CUP_Ind_Garage01","CUP_CAStructures_IndPipe1_todo_delete","CUP_IndPipe2","CUP_Ind_Shed_01","CUP_Ind_Shed_02","CUP_Ind_Tank","CUP_Ind_Workshop01","CUP_CABuildings2_Misc_Cargo","CUP_Misc_PowerStation","CUP_Misc_WaterStation","CUP_Rail_House_01","CUP_Shed_small","CUP_Shed_wooden","CUP_particle_effects","CUP_CAMisc2","CUP_Misc3_Config","CUP_WarfareBuildings","CUP_Misc_e_Config","CUP_CAMisc_E_WF","CUP_CAMP_Armory_Misc","CUP_CAMP_Armory_Misc_Concrete_Wall","CUP_CAMP_Armory_Misc_Entrance_Gate","CUP_CAMP_Armory_Misc_Info_Board","CUP_CAMP_Armory_Misc_Infostands","CUP_CAMP_Armory_Misc_Laptop","CUP_CAMP_Armory_Misc_Loudspeakers","CUP_CAMP_Armory_Misc_Plasticpole","CUP_CAMP_Armory_Misc_Red_Light","CUP_CAMP_Armory_Misc_Sign_Armex","CUP_CAMP_Armory_Misc_Sign_Direction","CUP_CA_Plants2_Misc","CUP_CA_Plants_PMC","CUP_CARoads2Bridge","CUP_CARoads_PMC_Bridge","CUP_CASigns_E","CUP_A_TVTower","CUP_CAStructures_Nav_pier","CUP_CAStructures_Railway","CUP_Structures_e_Config","CUP_CAStructures_E_HouseA","CUP_CAStructures_E_HouseA_A_BuildingWIP","CUP_CAStructures_E_HouseA_A_CityGate1","CUP_CAStructures_E_HouseA_A_Minaret","CUP_CAStructures_E_HouseA_A_Minaret_Porto","CUP_CAStructures_E_HouseA_A_Mosque_big","CUP_CAStructures_E_HouseA_A_Mosque_small","CUP_CAStructures_E_HouseA_A_Office01","CUP_CAStructures_E_HouseA_a_stationhouse","CUP_CAStructures_E_HouseA_A_Statue","CUP_CAStructures_E_HouseA_A_Villa","CUP_CAStructures_E_HouseC","CUP_CAStructures_E_HouseK","CUP_CAStructures_E_HouseL","CUP_CAStructures_E_Ind","CUP_CAStructures_E_Ind_Ind_Coltan_Mine","CUP_CAStructures_E_Ind_Ind_FuelStation","CUP_CAStructures_E_Ind_Ind_Garage01","CUP_CAStructures_E_Ind_Oil_Mine","CUP_CAStructures_E_Ind_IndPipes","CUP_CAStructures_E_Ind_Misc_PowerStation","CUP_CAStructures_E_Ind_Ind_Shed","CUP_CAStructures_E_Mil","CUP_CAStructures_E_Misc","CUP_CAStructures_E_Misc_Misc_cables","CUP_CAStructures_E_Misc_Misc_Construction","CUP_CAStructures_E_Misc_Misc_Garbage","CUP_CAStructures_E_Misc_Misc_Interier","CUP_CAStructures_E_Misc_Misc_Lamp","CUP_CAStructures_E_Misc_Misc_Market","CUP_CAStructures_E_Misc_Misc_powerline","CUP_CAStructures_E_Misc_Misc_Water","CUP_CAStructures_E_Misc_Misc_Well","CUP_CAStructures_E_Wall","CUP_CAStructures_E_Wall_Wall_L","CUP_Structures_pmc_Config","CUP_CAStructures_PMC_Buildings","CUP_CAStructures_PMC_Buildings_Bunker","CUP_CAStructures_PMC_Buildings_GeneralStore_PMC","CUP_CAStructures_PMC_Buildings_Ruin_Cowshed","CUP_CAStructures_PMC_Ind","CUP_CAStructures_PMC_FuelStation","CUP_CAStructures_PMC_Misc","CUP_CAStructures_PMC_Misc_Shed","CUP_CAStructures_PMC_Ruins","CUP_CAStructures_PMC_Walls","CUP_Editor_Plants_Config","CUP_Editor_Rocks_Config","CUP_Editor_Signs_Config","CUP_ibr_plants","CUP_Data_baf_Config","CUP_CALanguage_Baf","CUP_CALanguageMissions_baf","CUP_Misc_acr_Config","CUP_CAMisc_ACR_3DMarkers","CUP_CAMisc_ACR_Container","CUP_CAMisc_ACR_Dog","CUP_CAMisc_ACR_Helpers","CUP_CAMisc_ACR_PBX","CUP_CAMisc_ACR_ScaffoldingSmall","CUP_CAMisc_ACR_Shooting_range","CUP_CAMisc_ACR_Sign_Mines","CUP_CAMisc_ACR_Targets","CUP_CAMisc_ACR_Targets_InvisibleTarget","CUP_CAMisc_ACR_TestSphere","CUP_CAMisc_BAF","CUP_BaseConfig_F","CUP_Core","CUP_StandaloneTerrains_Core","CUP_StandaloneTerrains_Core_Faction","CUP_StandaloneTerrains_Core_VehicleClass","CUP_Worlds","CUP_Worlds_Ambient","CUP_Worlds_Author","CUP_Worlds_Clutter","CUP_Worlds_ClutterDist","CUP_Worlds_Delete","CUP_Worlds_DustEffects","CUP_Worlds_EnvSounds","CUP_Worlds_FullDetailDist","CUP_Worlds_Grid","CUP_Worlds_Intros","CUP_Worlds_Lighting","CUP_CWA_Lighting","CUP_Desert_Lighting","CUP_European_Lighting","CUP_Tropical_Lighting","CUP_Worlds_MapSize","CUP_Worlds_MidDetailTexture","CUP_Worlds_NoDetailDist","CUP_Worlds_PictureMap","CUP_Worlds_PictureShot","CUP_Worlds_PreviewVideo","CUP_Worlds_Seabed","CUP_Worlds_SkyTexture","CUP_Worlds_StreetLamp","CUP_Worlds_Surfaces","CUP_Worlds_Water","CUP_A1_EditorObjects","CUP_A2_EditorObjects"};
		version = "2017-12-07";
	};
};
class CfgVehicles
{
	class Sound;

	class Sound_Owl: Sound
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Sound_Frog: Sound
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Sound_Frogs: Sound
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Sound_Fire: Sound
	{
		scopeCurator = 1;//2;
	};
	class Sound_BirdSinging: Sound
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Sound_Crickets1: Sound
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Sound_Crickets2: Sound
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Sound_Crickets3: Sound
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Sound_Crickets4: Sound
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Sound_Chicken: Sound
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Sound_Cock: Sound
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Sound_Cow: Sound
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Sound_Crow: Sound
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Sound_Wolf: Sound
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Sound_Dog: Sound
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Sound_BadDog: Sound
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Sound_SorrowDog: Sound
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Sound_LittleDog: Sound
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Sound_Music: Sound
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
};
