﻿class CfgPatches
{
	class Optional_HideFromEditor_Vehicles_c
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Data_F_Loadorder","A3_Data_F_Curator_Loadorder","A3_Data_F_Kart_Loadorder","A3_Data_F_Bootcamp_Loadorder","A3_Data_F_Heli_Loadorder","A3_Data_F_Mark_Loadorder","A3_Data_F_Exp_A_Loadorder","A3_Data_F_Exp_B_Loadorder","A3_Data_F_Exp_Loadorder","A3_Data_F_Jets_Loadorder","A3_Data_F_Argo_Loadorder","A3_Data_F_Patrol_Loadorder","A3_Data_F_Orange_Loadorder","A3_Data_F_Tacops_Loadorder","A3_Data_F_Tank_Loadorder","A3_Data_F_Destroyer_Loadorder","A3_Data_F_Sams_Loadorder"};
		version = "2019-03-28";
	};
};
class CfgVehicles
{
	class AA_01_base_F;
	class AAA_System_01_base_F;
	class AFV_Wheeled_01_base_F;
	class AFV_Wheeled_01_up_base_F;
	class APC_Wheeled_02_base_v2_F;
	class AT_01_base_F;
	class B_APC_Tracked_01_base_F;
	class B_APC_Wheeled_01_base_F;
	class B_APC_Wheeled_03_base_F;
	class B_MBT_01_arty_base_F;
	class B_MBT_01_base_F;
	class B_MBT_01_mlrs_base_F;
	class B_Parachute;
	class B_Ship_Gun_01_base_F;
	class B_Ship_MRLS_01_base_F;
	class B_Truck_01_ammo_F;
	class B_Truck_01_box_F;
	class B_Truck_01_covered_F;
	class B_Truck_01_fuel_F;
	class B_Truck_01_medical_F;
	class B_Truck_01_mover_F;
	class B_Truck_01_Repair_F;
	class Bag_Base;
	class Boat_Armed_01_base_F;
	class Boat_Armed_01_minigun_base_F;
	class Boat_Civil_01_base_F;
	class Boat_Transport_02_base_F;
	class C_Kart_01_F_Base;
	class Ejection_Seat_Plane_CAS_01_base_F;
	class Ejection_Seat_Plane_CAS_02_base_F;
	class Ejection_Seat_Plane_Fighter_01_base_F;
	class Ejection_Seat_Plane_Fighter_02_base_F;
	class Ejection_Seat_Plane_Fighter_03_base_F;
	class Ejection_Seat_Plane_Fighter_04_base_F;
	class FloatingStructure_F;
	class GMG_01_A_base_F;
	class GMG_01_base_F;
	class GMG_01_high_base_F;
	class Hatchback_01_base_F;
	class Hatchback_01_sport_base_F;
	class Heli_Attack_01_base_F;
	class Heli_Attack_01_dynamicLoadout_base_F;
	class Heli_Attack_02_base_F;
	class Heli_Attack_02_dynamicLoadout_base_F;
	class Heli_Light_01_armed_base_F;
	class Heli_Light_01_civil_base_F;
	class Heli_Light_01_dynamicLoadout_base_F;
	class Heli_Light_01_unarmed_base_F;
	class Heli_Light_02_base_F;
	class Heli_Light_02_dynamicLoadout_base_F;
	class Heli_Light_02_unarmed_base_F;
	class Heli_light_03_base_F;
	class Heli_light_03_dynamicLoadout_base_F;
	class Heli_light_03_unarmed_base_F;
	class Heli_Transport_01_base_F;
	class Heli_Transport_02_base_F;
	class Heli_Transport_03_base_F;
	class Heli_Transport_03_unarmed_base_F;
	class Heli_Transport_04_base_F;
	class HMG_01_A_base_F;
	class HMG_01_base_F;
	class HMG_01_high_base_F;
	class I_APC_tracked_03_base_F;
	class I_APC_Wheeled_03_base_F;
	class I_MBT_03_base_F;
	class LSV_01_armed_base_F;
	class LSV_01_AT_base_F;
	class LSV_01_light_base_F;
	class LSV_01_unarmed_base_F;
	class LSV_02_armed_base_F;
	class LSV_02_AT_base_F;
	class LSV_02_unarmed_base_F;
	class LT_01_AA_base_F;
	class LT_01_AT_base_F;
	class LT_01_cannon_base_F;
	class LT_01_scout_base_F;
	class MBT_04_cannon_base_F;
	class MBT_04_command_base_F;
	class Mortar_01_base_F;
	class MRAP_01_base_F;
	class MRAP_01_gmg_base_F;
	class MRAP_01_hmg_base_F;
	class MRAP_02_base_F;
	class MRAP_02_gmg_base_F;
	class MRAP_02_hmg_base_F;
	class MRAP_03_base_F;
	class MRAP_03_gmg_base_F;
	class MRAP_03_hmg_base_F;
	class O_APC_Tracked_02_base_F;
	class O_APC_Wheeled_02_base_F;
	class O_MBT_02_arty_base_F;
	class O_MBT_02_base_F;
	class Offroad_01_armed_base_F;
	class Offroad_01_AT_base_F;
	class Offroad_01_civil_base_F;
	class Offroad_01_military_base_F;
	class Offroad_01_repair_base_F;
	class Offroad_01_repair_military_base_F;
	class Offroad_02_AT_base_F;
	class Offroad_02_LMG_base_F;
	class Offroad_02_unarmed_base_F;
	class Parachute;
	class Parachute_02_base_F;
	class Paraglide;
	class Plane_Canopy_Base_F;
	class Plane_CAS_01_base_F;
	class Plane_CAS_01_dynamicLoadout_base_F;
	class Plane_CAS_02_base_F;
	class Plane_CAS_02_dynamicLoadout_base_F;
	class Plane_Civil_01_base_F;
	class Plane_Fighter_01_Base_F;
	class Plane_Fighter_02_Base_F;
	class Plane_Fighter_03_base_F;
	class Plane_Fighter_03_dynamicLoadout_base_F;
	class Plane_Fighter_04_Base_F;
	class Quadbike_01_base_F;
	class Radar_System_01_base_F;
	class Radar_System_02_base_F;
	class Rescue_duck_base_F;
	class Rubber_duck_base_F;
	class SAM_System_01_base_F;
	class SAM_System_02_base_F;
	class SAM_System_03_base_F;
	class SAM_System_04_base_F;
	class Scooter_Transport_01_base_F;
	class SDV_01_base_F;
	class Static_Designator_01_base_F;
	class Static_Designator_02_base_F;
	class SUV_01_base_F;
	class Truck_01_base_F;
	class Truck_02_Ammo_base_F;
	class Truck_02_base_F;
	class Truck_02_box_base_F;
	class Truck_02_fuel_base_F;
	class Truck_02_medical_base_F;
	class Truck_02_MRL_base_F;
	class Truck_02_transport_base_F;
	class Truck_02_water_base_F;
	class Truck_03_base_F;
	class UAV_01_base_F;
	class UAV_02_base_F;
	class UAV_02_CAS_base_F;
	class UAV_02_dynamicLoadout_base_F;
	class UAV_03_base_F;
	class UAV_03_dynamicLoadout_base_F;
	class UAV_04_base_F;
	class UAV_05_Base_F;
	class UAV_06_antimine_base_F;
	class UAV_06_base_F;
	class UAV_06_medical_base_F;
	class UGV_01_base_F;
	class UGV_01_rcws_base_F;
	class Van_01_box_base_F;
	class Van_01_fuel_base_F;
	class Van_01_transport_base_F;
	class Van_02_medevac_base_F;
	class Van_02_service_base_F;
	class Van_02_transport_base_F;
	class Van_02_vehicle_base_F;
	class VTOL_01_armed_base_F;
	class VTOL_01_infantry_base_F;
	class VTOL_01_vehicle_base_F;
	class VTOL_02_infantry_base_F;
	class VTOL_02_infantry_dynamicLoadout_base_F;
	class VTOL_02_vehicle_base_F;
	class VTOL_02_vehicle_dynamicLoadout_base_F;
	class Weapon_Bag_Base;

	class B_HMG_01_F: HMG_01_base_F
	{
		scope = 1;//2
	};
	class O_HMG_01_F: HMG_01_base_F
	{
		scope = 1;//2
	};
	class I_HMG_01_F: HMG_01_base_F
	{
		scope = 1;//2
	};
	class B_HMG_01_high_F: HMG_01_high_base_F
	{
		scope = 1;//2
	};
	class O_HMG_01_high_F: HMG_01_high_base_F
	{
		scope = 1;//2
	};
	class I_HMG_01_high_F: HMG_01_high_base_F
	{
		scope = 1;//2
	};
	class B_HMG_01_A_F: HMG_01_A_base_F
	{
		scope = 1;//2
	};
	class O_HMG_01_A_F: HMG_01_A_base_F
	{
		scope = 1;//2
	};
	class I_HMG_01_A_F: HMG_01_A_base_F
	{
		scope = 1;//2
	};
	class B_GMG_01_F: GMG_01_base_F
	{
		scope = 1;//2
	};
	class O_GMG_01_F: GMG_01_base_F
	{
		scope = 1;//2
	};
	class I_GMG_01_F: GMG_01_base_F
	{
		scope = 1;//2
	};
	class B_GMG_01_high_F: GMG_01_high_base_F
	{
		scope = 1;//2
	};
	class O_GMG_01_high_F: GMG_01_high_base_F
	{
		scope = 1;//2
	};
	class I_GMG_01_high_F: GMG_01_high_base_F
	{
		scope = 1;//2
	};
	class B_GMG_01_A_F: GMG_01_A_base_F
	{
		scope = 1;//2
	};
	class O_GMG_01_A_F: GMG_01_A_base_F
	{
		scope = 1;//2
	};
	class I_GMG_01_A_F: GMG_01_A_base_F
	{
		scope = 1;//2
	};
	class B_HMG_01_support_F: Bag_Base
	{
		scope = 1;//2
	};
	class B_HMG_01_weapon_F: Weapon_Bag_Base
	{
		scope = 1;//2
	};
	class B_Mortar_01_F: Mortar_01_base_F
	{
		scope = 1;//2
	};
	class O_Mortar_01_F: Mortar_01_base_F
	{
		scope = 1;//2
	};
	class I_Mortar_01_F: Mortar_01_base_F
	{
		scope = 1;//2
	};
	class I_G_Mortar_01_F: Mortar_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_G_Mortar_01_F: I_G_Mortar_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_G_Mortar_01_F: I_G_Mortar_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_Mortar_01_support_F: Bag_Base
	{
		scope = 1;//2
	};
	class B_Mortar_01_weapon_F: Weapon_Bag_Base
	{
		scope = 1;//2
	};
	class B_Heli_Light_01_F: Heli_Light_01_unarmed_base_F
	{
		scope = 1;//2
	};
	class B_Heli_Light_01_armed_F: Heli_Light_01_armed_base_F
	{
//		scope = 1;
	};
	class B_Heli_Light_01_dynamicLoadout_F: Heli_Light_01_dynamicLoadout_base_F
	{
		scope = 1;//2
	};
	class C_Heli_Light_01_civil_F: Heli_Light_01_civil_base_F
	{
		scope = 1;//2
	};
	class C_Heli_light_01_blue_F: C_Heli_Light_01_civil_F
	{
//		scope = 1;
	};
	class C_Heli_light_01_red_F: C_Heli_Light_01_civil_F
	{
//		scope = 1;
	};
	class C_Heli_light_01_ion_F: C_Heli_Light_01_civil_F
	{
//		scope = 1;
	};
	class C_Heli_light_01_blueLine_F: C_Heli_Light_01_civil_F
	{
//		scope = 1;
	};
	class C_Heli_light_01_digital_F: C_Heli_Light_01_civil_F
	{
//		scope = 1;
	};
	class C_Heli_light_01_elliptical_F: C_Heli_Light_01_civil_F
	{
//		scope = 1;
	};
	class C_Heli_light_01_furious_F: C_Heli_Light_01_civil_F
	{
//		scope = 1;
	};
	class C_Heli_light_01_graywatcher_F: C_Heli_Light_01_civil_F
	{
//		scope = 1;
	};
	class C_Heli_light_01_jeans_F: C_Heli_Light_01_civil_F
	{
//		scope = 1;
	};
	class C_Heli_light_01_light_F: C_Heli_Light_01_civil_F
	{
//		scope = 1;
	};
	class C_Heli_light_01_shadow_F: C_Heli_Light_01_civil_F
	{
//		scope = 1;
	};
	class C_Heli_light_01_sheriff_F: C_Heli_Light_01_civil_F
	{
//		scope = 1;
	};
	class C_Heli_light_01_speedy_F: C_Heli_Light_01_civil_F
	{
//		scope = 1;
	};
	class C_Heli_light_01_sunset_F: C_Heli_Light_01_civil_F
	{
//		scope = 1;
	};
	class C_Heli_light_01_vrana_F: C_Heli_Light_01_civil_F
	{
//		scope = 1;
	};
	class C_Heli_light_01_wasp_F: C_Heli_Light_01_civil_F
	{
//		scope = 1;
	};
	class C_Heli_light_01_wave_F: C_Heli_Light_01_civil_F
	{
//		scope = 1;
	};
	class C_Heli_light_01_stripped_F: C_Heli_Light_01_civil_F
	{
//		scope = 1;
	};
	class C_Heli_light_01_luxe_F: C_Heli_Light_01_civil_F
	{
//		scope = 1;
	};
	class B_Heli_Light_01_stripped_F: B_Heli_Light_01_F
	{
//		scope = 1;
	};
	class O_Heli_Light_02_dynamicLoadout_F: Heli_Light_02_dynamicLoadout_base_F
	{
		scope = 1;//2
	};
	class O_Heli_Light_02_F: Heli_Light_02_base_F
	{
//		scope = 1;
	};
	class O_Heli_Light_02_unarmed_F: Heli_Light_02_unarmed_base_F
	{
		scope = 1;//2
	};
	class O_Heli_Light_02_v2_F: Heli_Light_02_base_F
	{
//		scope = 1;
	};
	class NonSteerable_Parachute_F: Parachute
	{
//		scope = 1;
	};
	class Steerable_Parachute_F: Paraglide
	{
//		scope = 1;
	};
	class B_Heli_Attack_01_F: Heli_Attack_01_base_F
	{
//		scope = 1;
	};
	class B_Heli_Attack_01_dynamicLoadout_F: Heli_Attack_01_dynamicLoadout_base_F
	{
		scope = 1;//2
	};
	class O_Heli_Attack_02_F: Heli_Attack_02_base_F
	{
//		scope = 1;
	};
	class O_Heli_Attack_02_black_F: Heli_Attack_02_base_F
	{
//		scope = 1;
	};
	class O_Heli_Attack_02_dynamicLoadout_F: Heli_Attack_02_dynamicLoadout_base_F
	{
		scope = 1;//2
	};
	class O_Heli_Attack_02_dynamicLoadout_black_F: Heli_Attack_02_dynamicLoadout_base_F
	{
//		scope = 1;
	};
	class B_Heli_Transport_01_F: Heli_Transport_01_base_F
	{
		scope = 1;//2
	};
	class B_Heli_Transport_01_camo_F: Heli_Transport_01_base_F
	{
//		scope = 1;
	};
	class I_Heli_Transport_02_F: Heli_Transport_02_base_F
	{
		scope = 1;//2
	};
	class B_Parachute_02_F: Parachute_02_base_F
	{
//		scope = 1;
	};
	class O_Parachute_02_F: Parachute_02_base_F
	{
//		scope = 1;
	};
	class I_Parachute_02_F: Parachute_02_base_F
	{
//		scope = 1;
	};
	class B_B_Parachute_02_F: B_Parachute
	{
//		scope = 1;
	};
	class B_O_Parachute_02_F: B_Parachute
	{
//		scope = 1;
	};
	class B_I_Parachute_02_F: B_Parachute
	{
//		scope = 1;
	};
	class Plane_Fighter_03_Canopy_F: Plane_Canopy_Base_F
	{
//		scope = 1;
	};
	class I_Ejection_Seat_Plane_Fighter_03_F: Ejection_Seat_Plane_Fighter_03_base_F
	{
//		scope = 1;
	};
	class I_Plane_Fighter_03_CAS_F: Plane_Fighter_03_base_F
	{
//		scope = 1;
		scopeCurator = 1;//2
	};
	class I_Plane_Fighter_03_dynamicLoadout_F: Plane_Fighter_03_dynamicLoadout_base_F
	{
		scope = 1;//2
	};
	class B_APC_Tracked_01_rcws_F: B_APC_Tracked_01_base_F
	{
		scope = 1;//2
	};
	class B_APC_Tracked_01_CRV_F: B_APC_Tracked_01_base_F
	{
		scope = 1;//2
	};
	class B_APC_Tracked_01_AA_F: B_APC_Tracked_01_base_F
	{
		scope = 1;//2
	};
	class O_APC_Tracked_02_cannon_F: O_APC_Tracked_02_base_F
	{
		scope = 1;//2
	};
	class O_APC_Tracked_02_AA_F: O_APC_Tracked_02_base_F
	{
		scope = 1;//2
	};
	class B_MBT_01_cannon_F: B_MBT_01_base_F
	{
		scope = 1;//2
	};
	class B_MBT_01_arty_F: B_MBT_01_arty_base_F
	{
		scope = 1;//2
	};
	class B_MBT_01_mlrs_F: B_MBT_01_mlrs_base_F
	{
		scope = 1;//2
	};
	class O_MBT_02_cannon_F: O_MBT_02_base_F
	{
		scope = 1;//2
	};
	class O_MBT_02_arty_F: O_MBT_02_arty_base_F
	{
		scope = 1;//2
	};
	class B_Boat_Armed_01_minigun_F: Boat_Armed_01_minigun_base_F
	{
		scope = 1;//2
	};
	class O_Boat_Armed_01_hmg_F: Boat_Armed_01_base_F
	{
		scope = 1;//2
	};
	class B_Boat_Transport_01_F: Rubber_duck_base_F
	{
		scope = 1;//2
	};
	class O_Boat_Transport_01_F: Rubber_duck_base_F
	{
		scope = 1;//2
	};
	class B_Lifeboat: Rescue_duck_base_F
	{
		scope = 1;//2
	};
	class O_Lifeboat: Rescue_duck_base_F
	{
		scope = 1;//2
	};
	class C_Rubberboat: Rescue_duck_base_F
	{
		scope = 1;//2
	};
	class I_Boat_Armed_01_minigun_F: Boat_Armed_01_minigun_base_F
	{
		scope = 1;//2
	};
	class I_Boat_Transport_01_F: Rubber_duck_base_F
	{
		scope = 1;//2
	};
	class B_SDV_01_F: SDV_01_base_F
	{
		scope = 1;//2
	};
	class O_SDV_01_F: SDV_01_base_F
	{
		scope = 1;//2
	};
	class I_SDV_01_F: SDV_01_base_F
	{
		scope = 1;//2
	};
	class C_Boat_Civil_01_F: Boat_Civil_01_base_F
	{
		scope = 1;//2
	};
	class C_Boat_Civil_01_rescue_F: Boat_Civil_01_base_F
	{
		scope = 1;//2
	};
	class C_Boat_Civil_01_police_F: Boat_Civil_01_base_F
	{
		scope = 1;//2
	};
	class C_Boat_Civil_04_F: FloatingStructure_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_G_Boat_Transport_01_F: Rubber_duck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_G_Boat_Transport_01_F: I_G_Boat_Transport_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_G_Boat_Transport_01_F: I_G_Boat_Transport_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_MRAP_01_F: MRAP_01_base_F
	{
		scope = 1;//2
	};
	class B_MRAP_01_gmg_F: MRAP_01_gmg_base_F
	{
		scope = 1;//2
	};
	class B_MRAP_01_hmg_F: MRAP_01_hmg_base_F
	{
		scope = 1;//2
	};
	class O_MRAP_02_F: MRAP_02_base_F
	{
		scope = 1;//2
	};
	class O_MRAP_02_hmg_F: MRAP_02_hmg_base_F
	{
		scope = 1;//2
	};
	class O_MRAP_02_gmg_F: MRAP_02_gmg_base_F
	{
		scope = 1;//2
	};
	class C_Offroad_01_F: Offroad_01_civil_base_F
	{
		scope = 1;//2
	};
	class C_Offroad_01_repair_F: Offroad_01_repair_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_G_Offroad_01_repair_F: Offroad_01_repair_military_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_G_Offroad_01_repair_F: Offroad_01_repair_military_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_G_Offroad_01_repair_F: Offroad_01_repair_military_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_G_Offroad_01_F: Offroad_01_military_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_G_Offroad_01_armed_F: Offroad_01_armed_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_G_Offroad_01_F: I_G_Offroad_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_G_Offroad_01_F: I_G_Offroad_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_G_Offroad_01_armed_F: I_G_Offroad_01_armed_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_G_Offroad_01_armed_F: I_G_Offroad_01_armed_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_Offroad_01_red_F: C_Offroad_01_F
	{
//		scope = 1;
	};
	class C_Offroad_01_sand_F: C_Offroad_01_F
	{
//		scope = 1;
	};
	class C_Offroad_01_white_F: C_Offroad_01_F
	{
//		scope = 1;
	};
	class C_Offroad_01_blue_F: C_Offroad_01_F
	{
//		scope = 1;
	};
	class C_Offroad_01_darkred_F: C_Offroad_01_F
	{
//		scope = 1;
	};
	class C_Offroad_01_bluecustom_F: C_Offroad_01_F
	{
//		scope = 1;
	};
	class C_Offroad_luxe_F: C_Offroad_01_F
	{
//		scope = 1;
	};
	class C_Offroad_default_F: C_Offroad_01_F
	{
//		scope = 1;
	};
	class C_Offroad_stripped_F: C_Offroad_01_F
	{
//		scope = 1;
	};
	class C_Quadbike_01_F: Quadbike_01_base_F
	{
		scope = 1;//2
	};
	class B_Quadbike_01_F: Quadbike_01_base_F
	{
		scope = 1;//2
	};
	class O_Quadbike_01_F: Quadbike_01_base_F
	{
		scope = 1;//2
	};
	class I_Quadbike_01_F: Quadbike_01_base_F
	{
		scope = 1;//2
	};
	class I_G_Quadbike_01_F: Quadbike_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_G_Quadbike_01_F: I_G_Quadbike_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_G_Quadbike_01_F: I_G_Quadbike_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_Quadbike_01_black_F: C_Quadbike_01_F
	{
//		scope = 1;
	};
	class C_Quadbike_01_blue_F: C_Quadbike_01_F
	{
//		scope = 1;
	};
	class C_Quadbike_01_red_F: C_Quadbike_01_F
	{
//		scope = 1;
	};
	class C_Quadbike_01_white_F: C_Quadbike_01_F
	{
//		scope = 1;
	};
	class I_MRAP_03_F: MRAP_03_base_F
	{
		scope = 1;//2
	};
	class I_MRAP_03_hmg_F: MRAP_03_hmg_base_F
	{
		scope = 1;//2
	};
	class I_MRAP_03_gmg_F: MRAP_03_gmg_base_F
	{
		scope = 1;//2
	};
	class B_Truck_01_transport_F: Truck_01_base_F
	{
		scope = 1;//2
	};
	class O_Truck_02_covered_F: Truck_02_base_F
	{
		scope = 1;//2
	};
	class O_Truck_02_transport_F: Truck_02_transport_base_F
	{
		scope = 1;//2
	};
	class I_Truck_02_covered_F: Truck_02_base_F
	{
		scope = 1;//2
	};
	class I_Truck_02_transport_F: Truck_02_transport_base_F
	{
		scope = 1;//2
	};
	class C_Truck_02_covered_F: Truck_02_base_F
	{
		scope = 1;//2
	};
	class C_Truck_02_transport_F: Truck_02_transport_base_F
	{
		scope = 1;//2
	};
	class C_Hatchback_01_F: Hatchback_01_base_F
	{
		scope = 1;//2
	};
	class C_Hatchback_01_sport_F: Hatchback_01_sport_base_F
	{
		scope = 1;//2
	};
	class C_Hatchback_01_grey_F: C_Hatchback_01_F
	{
//		scope = 1;
	};
	class C_Hatchback_01_green_F: C_Hatchback_01_F
	{
//		scope = 1;
	};
	class C_Hatchback_01_blue_F: C_Hatchback_01_F
	{
//		scope = 1;
	};
	class C_Hatchback_01_bluecustom_F: C_Hatchback_01_F
	{
//		scope = 1;
	};
	class C_Hatchback_01_beigecustom_F: C_Hatchback_01_F
	{
//		scope = 1;
	};
	class C_Hatchback_01_yellow_F: C_Hatchback_01_F
	{
//		scope = 1;
	};
	class C_Hatchback_01_white_F: C_Hatchback_01_F
	{
//		scope = 1;
	};
	class C_Hatchback_01_black_F: C_Hatchback_01_F
	{
//		scope = 1;
	};
	class C_Hatchback_01_dark_F: C_Hatchback_01_F
	{
//		scope = 1;
	};
	class C_Hatchback_01_sport_red_F: C_Hatchback_01_sport_F
	{
//		scope = 1;
	};
	class C_Hatchback_01_sport_blue_F: C_Hatchback_01_sport_F
	{
//		scope = 1;
	};
	class C_Hatchback_01_sport_orange_F: C_Hatchback_01_sport_F
	{
//		scope = 1;
	};
	class C_Hatchback_01_sport_white_F: C_Hatchback_01_sport_F
	{
//		scope = 1;
	};
	class C_Hatchback_01_sport_grey_F: C_Hatchback_01_sport_F
	{
//		scope = 1;
	};
	class C_Hatchback_01_sport_green_F: C_Hatchback_01_sport_F
	{
//		scope = 1;
	};
	class C_SUV_01_F: SUV_01_base_F
	{
		scope = 1;//2
	};
	class SUV_01_base_red_F: C_SUV_01_F
	{
//		scope = 1;
	};
	class SUV_01_base_black_F: C_SUV_01_F
	{
//		scope = 1;
	};
	class SUV_01_base_grey_F: C_SUV_01_F
	{
//		scope = 1;
	};
	class SUV_01_base_orange_F: C_SUV_01_F
	{
//		scope = 1;
	};
	class O_Truck_02_box_F: Truck_02_box_base_F
	{
		scope = 1;//2
	};
	class O_Truck_02_medical_F: Truck_02_medical_base_F
	{
		scope = 1;//2
	};
	class O_Truck_02_Ammo_F: Truck_02_Ammo_base_F
	{
		scope = 1;//2
	};
	class O_Truck_02_fuel_F: Truck_02_fuel_base_F
	{
		scope = 1;//2
	};
	class I_Truck_02_ammo_F: Truck_02_Ammo_base_F
	{
		scope = 1;//2
	};
	class I_Truck_02_box_F: Truck_02_box_base_F
	{
		scope = 1;//2
	};
	class I_Truck_02_medical_F: Truck_02_medical_base_F
	{
		scope = 1;//2
	};
	class I_Truck_02_fuel_F: Truck_02_fuel_base_F
	{
		scope = 1;//2
	};
	class C_Truck_02_fuel_F: Truck_02_fuel_base_F
	{
		scope = 1;//2
	};
	class C_Truck_02_box_F: Truck_02_box_base_F
	{
		scope = 1;//2
	};
	class C_Van_01_transport_F: Van_01_transport_base_F
	{
		scope = 1;//2
	};
	class I_G_Van_01_transport_F: Van_01_transport_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_Van_01_box_F: Van_01_box_base_F
	{
		scope = 1;//2
	};
	class C_Van_01_fuel_F: Van_01_fuel_base_F
	{
		scope = 1;//2
	};
	class I_G_Van_01_fuel_F: Van_01_fuel_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_G_Van_01_transport_F: I_G_Van_01_transport_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_G_Van_01_transport_F: I_G_Van_01_transport_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_G_Van_01_fuel_F: I_G_Van_01_fuel_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_G_Van_01_fuel_F: I_G_Van_01_fuel_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_Van_01_transport_white_F: C_Van_01_transport_F
	{
//		scope = 1;
	};
	class C_Van_01_transport_red_F: C_Van_01_transport_F
	{
//		scope = 1;
	};
	class C_Van_01_box_white_F: C_Van_01_box_F
	{
//		scope = 1;
	};
	class C_Van_01_box_red_F: C_Van_01_box_F
	{
//		scope = 1;
	};
	class C_Van_01_fuel_white_F: C_Van_01_fuel_F
	{
//		scope = 1;
	};
	class C_Van_01_fuel_red_F: C_Van_01_fuel_F
	{
//		scope = 1;
	};
	class C_Van_01_fuel_white_v2_F: C_Van_01_fuel_F
	{
//		scope = 1;
	};
	class C_Van_01_fuel_red_v2_F: C_Van_01_fuel_F
	{
//		scope = 1;
	};
	class B_static_AA_F: AA_01_base_F
	{
		scope = 1;//2
	};
	class O_static_AA_F: AA_01_base_F
	{
		scope = 1;//2
	};
	class I_static_AA_F: AA_01_base_F
	{
		scope = 1;//2
	};
	class B_AA_01_weapon_F: Weapon_Bag_Base
	{
		scope = 1;//2
	};
	class O_AA_01_weapon_F: Weapon_Bag_Base
	{
		scope = 1;//2
	};
	class I_AA_01_weapon_F: Weapon_Bag_Base
	{
		scope = 1;//2
	};
	class B_static_AT_F: AT_01_base_F
	{
		scope = 1;//2
	};
	class O_static_AT_F: AT_01_base_F
	{
		scope = 1;//2
	};
	class I_static_AT_F: AT_01_base_F
	{
		scope = 1;//2
	};
	class B_AT_01_weapon_F: Weapon_Bag_Base
	{
		scope = 1;//2
	};
	class O_AT_01_weapon_F: Weapon_Bag_Base
	{
		scope = 1;//2
	};
	class I_AT_01_weapon_F: Weapon_Bag_Base
	{
		scope = 1;//2
	};
	class I_Heli_light_03_F: Heli_light_03_base_F
	{
//		scope = 1;
	};
	class I_Heli_light_03_dynamicLoadout_F: Heli_light_03_dynamicLoadout_base_F
	{
		scope = 1;//2
	};
	class I_Heli_light_03_unarmed_F: Heli_light_03_unarmed_base_F
	{
		scope = 1;//2
	};
	class Plane_CAS_01_Canopy_F: Plane_Canopy_Base_F
	{
//		scope = 1;
	};
	class B_Ejection_Seat_Plane_CAS_01_F: Ejection_Seat_Plane_CAS_01_base_F
	{
//		scope = 1;
	};
	class B_Plane_CAS_01_F: Plane_CAS_01_base_F
	{
//		scope = 1;
	};
	class B_Plane_CAS_01_dynamicLoadout_F: Plane_CAS_01_dynamicLoadout_base_F
	{
		scope = 1;//2
	};
	class Plane_CAS_02_Canopy_F: Plane_Canopy_Base_F
	{
//		scope = 1;
	};
	class O_Ejection_Seat_Plane_CAS_02_F: Ejection_Seat_Plane_CAS_02_base_F
	{
//		scope = 1;
	};
	class O_Plane_CAS_02_F: Plane_CAS_02_base_F
	{
//		scope = 1;
	};
	class O_Plane_CAS_02_dynamicLoadout_F: Plane_CAS_02_dynamicLoadout_base_F
	{
		scope = 1;//2
	};
	class B_APC_Wheeled_01_cannon_F: B_APC_Wheeled_01_base_F
	{
		scope = 1;//2
	};
	class O_APC_Wheeled_02_rcws_F: O_APC_Wheeled_02_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_APC_tracked_03_cannon_F: I_APC_tracked_03_base_F
	{
		scope = 1;//2
	};
	class I_MBT_03_cannon_F: I_MBT_03_base_F
	{
		scope = 1;//2
	};
	class B_MBT_01_TUSK_F: B_MBT_01_cannon_F
	{
		scope = 1;//2
	};
	class I_APC_Wheeled_03_cannon_F: I_APC_Wheeled_03_base_F
	{
		scope = 1;//2
	};
	class Submarine_01_F: FloatingStructure_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_UAV_01_F: UAV_01_base_F
	{
		scope = 1;//2
	};
	class O_UAV_01_F: UAV_01_base_F
	{
		scope = 1;//2
	};
	class I_UAV_01_F: UAV_01_base_F
	{
		scope = 1;//2
	};
	class B_UAV_02_F: UAV_02_base_F
	{
//		scope = 1;
		scopeCurator = 1;//2
	};
	class O_UAV_02_F: UAV_02_base_F
	{
//		scope = 1;
		scopeCurator = 1;//2
	};
	class I_UAV_02_F: UAV_02_base_F
	{
//		scope = 1;
		scopeCurator = 1;//2
	};
	class B_UAV_02_CAS_F: UAV_02_CAS_base_F
	{
//		scope = 1;
		scopeCurator = 1;//2
	};
	class O_UAV_02_CAS_F: UAV_02_CAS_base_F
	{
//		scope = 1;
		scopeCurator = 1;//2
	};
	class I_UAV_02_CAS_F: UAV_02_CAS_base_F
	{
//		scope = 1;
		scopeCurator = 1;//2
	};
	class B_UAV_02_dynamicLoadout_F: UAV_02_dynamicLoadout_base_F
	{
		scope = 1;//2
	};
	class O_UAV_02_dynamicLoadout_F: UAV_02_dynamicLoadout_base_F
	{
		scope = 1;//2
	};
	class I_UAV_02_dynamicLoadout_F: UAV_02_dynamicLoadout_base_F
	{
		scope = 1;//2
	};
	class B_UGV_01_F: UGV_01_base_F
	{
		scope = 1;//2
	};
	class O_UGV_01_F: UGV_01_base_F
	{
		scope = 1;//2
	};
	class I_UGV_01_F: UGV_01_base_F
	{
		scope = 1;//2
	};
	class B_UGV_01_rcws_F: UGV_01_rcws_base_F
	{
		scope = 1;//2
	};
	class O_UGV_01_rcws_F: UGV_01_rcws_base_F
	{
		scope = 1;//2
	};
	class I_UGV_01_rcws_F: UGV_01_rcws_base_F
	{
		scope = 1;//2
	};
	class O_Truck_03_transport_F: Truck_03_base_F
	{
		scope = 1;//2
	};
	class O_Truck_03_covered_F: Truck_03_base_F
	{
		scope = 1;//2
	};
	class O_Truck_03_repair_F: Truck_03_base_F
	{
		scope = 1;//2
	};
	class O_Truck_03_ammo_F: Truck_03_base_F
	{
		scope = 1;//2
	};
	class O_Truck_03_fuel_F: Truck_03_base_F
	{
		scope = 1;//2
	};
	class O_Truck_03_medical_F: Truck_03_base_F
	{
		scope = 1;//2
	};
	class O_Truck_03_device_F: Truck_03_base_F
	{
		scope = 1;//2
	};
	class C_Kart_01_F: C_Kart_01_F_Base
	{
		scope = 1;//2
	};
	class C_Kart_01_Fuel_F: C_Kart_01_F_Base
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_Kart_01_Blu_F: C_Kart_01_F_Base
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_Kart_01_Red_F: C_Kart_01_F_Base
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_Kart_01_Vrana_F: C_Kart_01_F_Base
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_Kart_01_green_F: C_Kart_01_F_Base
	{
//		scope = 1;
	};
	class C_Kart_01_orange_F: C_Kart_01_F_Base
	{
//		scope = 1;
	};
	class C_Kart_01_white_F: C_Kart_01_F_Base
	{
//		scope = 1;
	};
	class C_Kart_01_yellow_F: C_Kart_01_F_Base
	{
//		scope = 1;
	};
	class C_Kart_01_black_F: C_Kart_01_F_Base
	{
//		scope = 1;
	};
	class B_Heli_Transport_03_F: Heli_Transport_03_base_F
	{
		scope = 1;//2
	};
	class B_Heli_Transport_03_unarmed_F: Heli_Transport_03_unarmed_base_F
	{
		scope = 1;//2
	};
	class B_Heli_Transport_03_black_F: B_Heli_Transport_03_F
	{
//		scope = 1;
	};
	class B_Heli_Transport_03_unarmed_green_F: B_Heli_Transport_03_unarmed_F
	{
//		scope = 1;
	};
	class O_Heli_Transport_04_F: Heli_Transport_04_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_Heli_Transport_04_ammo_F: Heli_Transport_04_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_Heli_Transport_04_bench_F: Heli_Transport_04_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_Heli_Transport_04_box_F: Heli_Transport_04_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_Heli_Transport_04_covered_F: Heli_Transport_04_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_Heli_Transport_04_fuel_F: Heli_Transport_04_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_Heli_Transport_04_medevac_F: Heli_Transport_04_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_Heli_Transport_04_repair_F: Heli_Transport_04_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_Static_Designator_01_F: Static_Designator_01_base_F
	{
		scope = 1;//2
	};
	class B_Static_Designator_01_weapon_F: Weapon_Bag_Base
	{
		scope = 1;//2
	};
	class O_Static_Designator_02_F: Static_Designator_02_base_F
	{
		scope = 1;//2
	};
	class O_Static_Designator_02_weapon_F: Weapon_Bag_Base
	{
		scope = 1;//2
	};
	class O_Heli_Transport_04_black_F: O_Heli_Transport_04_F
	{
//		scope = 1;
	};
	class O_Heli_Transport_04_ammo_black_F: O_Heli_Transport_04_ammo_F
	{
//		scope = 1;
	};
	class O_Heli_Transport_04_bench_black_F: O_Heli_Transport_04_bench_F
	{
//		scope = 1;
	};
	class O_Heli_Transport_04_box_black_F: O_Heli_Transport_04_box_F
	{
//		scope = 1;
	};
	class O_Heli_Transport_04_covered_black_F: O_Heli_Transport_04_covered_F
	{
//		scope = 1;
	};
	class O_Heli_Transport_04_fuel_black_F: O_Heli_Transport_04_fuel_F
	{
//		scope = 1;
	};
	class O_Heli_Transport_04_medevac_black_F: O_Heli_Transport_04_medevac_F
	{
//		scope = 1;
	};
	class O_Heli_Transport_04_repair_black_F: O_Heli_Transport_04_repair_F
	{
//		scope = 1;
	};
	class I_C_Heli_Light_01_civil_F: Heli_Light_01_civil_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_CTRG_Heli_Transport_01_sand_F: Heli_Transport_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_CTRG_Heli_Transport_01_tropic_F: Heli_Transport_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_Plane_Civil_01_F: Plane_Civil_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_Plane_Civil_01_racing_F: Plane_Civil_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_C_Plane_Civil_01_F: Plane_Civil_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_T_UAV_03_F: UAV_03_base_F
	{
//		scope = 1;
	};
	class B_T_UAV_03_dynamicLoadout_F: UAV_03_dynamicLoadout_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_T_UAV_04_CAS_F: UAV_04_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_T_VTOL_01_infantry_F: VTOL_01_infantry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_T_VTOL_01_vehicle_F: VTOL_01_vehicle_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_T_VTOL_01_armed_F: VTOL_01_armed_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_T_VTOL_01_infantry_blue_F: B_T_VTOL_01_infantry_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class B_T_VTOL_01_infantry_olive_F: B_T_VTOL_01_infantry_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class B_T_VTOL_01_vehicle_blue_F: B_T_VTOL_01_vehicle_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class B_T_VTOL_01_vehicle_olive_F: B_T_VTOL_01_vehicle_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class B_T_VTOL_01_armed_blue_F: B_T_VTOL_01_armed_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class B_T_VTOL_01_armed_olive_F: B_T_VTOL_01_armed_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class O_T_VTOL_02_infantry_F: VTOL_02_infantry_base_F
	{
//		scope = 1;
	};
	class O_T_VTOL_02_vehicle_F: VTOL_02_vehicle_base_F
	{
//		scope = 1;
	};
	class O_T_VTOL_02_infantry_dynamicLoadout_F: VTOL_02_infantry_dynamicLoadout_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_T_VTOL_02_vehicle_dynamicLoadout_F: VTOL_02_vehicle_dynamicLoadout_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_T_VTOL_02_infantry_hex_F: O_T_VTOL_02_infantry_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class O_T_VTOL_02_infantry_ghex_F: O_T_VTOL_02_infantry_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class O_T_VTOL_02_infantry_grey_F: O_T_VTOL_02_infantry_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class O_T_VTOL_02_vehicle_hex_F: O_T_VTOL_02_vehicle_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class O_T_VTOL_02_vehicle_ghex_F: O_T_VTOL_02_vehicle_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class O_T_VTOL_02_vehicle_grey_F: O_T_VTOL_02_vehicle_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class B_T_APC_Tracked_01_AA_F: B_APC_Tracked_01_AA_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_T_APC_Tracked_01_CRV_F: B_APC_Tracked_01_CRV_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_T_APC_Tracked_01_rcws_F: B_APC_Tracked_01_rcws_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_T_APC_Tracked_02_cannon_ghex_F: O_APC_Tracked_02_cannon_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_T_APC_Tracked_02_AA_ghex_F: O_APC_Tracked_02_AA_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_T_APC_Wheeled_01_cannon_F: B_APC_Wheeled_01_cannon_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_T_APC_Wheeled_02_rcws_ghex_F: O_APC_Wheeled_02_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_T_MBT_01_arty_F: B_MBT_01_arty_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_T_MBT_01_mlrs_F: B_MBT_01_mlrs_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_T_MBT_01_cannon_F: B_MBT_01_cannon_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_T_MBT_01_TUSK_F: B_MBT_01_TUSK_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_T_MBT_02_cannon_ghex_F: O_MBT_02_cannon_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_T_MBT_02_arty_ghex_F: O_MBT_02_arty_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_T_Boat_Armed_01_minigun_F: B_Boat_Armed_01_minigun_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_T_Boat_Armed_01_hmg_F: O_Boat_Armed_01_hmg_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_T_Boat_Transport_01_F: Rubber_duck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_C_Boat_Transport_01_F: Rubber_duck_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_T_Boat_Transport_01_F: B_Boat_Transport_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_T_Lifeboat: B_Lifeboat
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_T_Lifeboat: O_Lifeboat
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_G_Boat_Transport_02_F: Boat_Transport_02_base_F
	{
//		scope = 1;
	};
	class O_G_Boat_Transport_02_F: Boat_Transport_02_base_F
	{
//		scope = 1;
	};
	class I_G_Boat_Transport_02_F: Boat_Transport_02_base_F
	{
//		scope = 1;
	};
	class I_C_Boat_Transport_02_F: Boat_Transport_02_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_Boat_Transport_02_F: Boat_Transport_02_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_Scooter_Transport_01_F: Scooter_Transport_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_T_LSV_01_armed_F: LSV_01_armed_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_T_LSV_01_armed_CTRG_F: B_T_LSV_01_armed_F
	{
//		scope = 1;
	};
	class B_T_LSV_01_unarmed_F: LSV_01_unarmed_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_T_LSV_01_unarmed_CTRG_F: B_T_LSV_01_unarmed_F
	{
//		scope = 1;
	};
	class B_LSV_01_armed_F: LSV_01_armed_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_LSV_01_unarmed_F: LSV_01_unarmed_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_CTRG_LSV_01_light_F: LSV_01_light_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_LSV_01_armed_black_F: B_LSV_01_armed_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class B_LSV_01_armed_olive_F: B_LSV_01_armed_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class B_LSV_01_armed_sand_F: B_LSV_01_armed_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class B_LSV_01_unarmed_black_F: B_LSV_01_unarmed_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class B_LSV_01_unarmed_olive_F: B_LSV_01_unarmed_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class B_LSV_01_unarmed_sand_F: B_LSV_01_unarmed_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class B_T_LSV_01_armed_black_F: B_T_LSV_01_armed_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class B_T_LSV_01_armed_olive_F: B_T_LSV_01_armed_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class B_T_LSV_01_armed_sand_F: B_T_LSV_01_armed_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class B_T_LSV_01_unarmed_black_F: B_T_LSV_01_unarmed_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class B_T_LSV_01_unarmed_olive_F: B_T_LSV_01_unarmed_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class B_T_LSV_01_unarmed_sand_F: B_T_LSV_01_unarmed_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class O_T_LSV_02_armed_F: LSV_02_armed_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_T_LSV_02_armed_viper_F: O_T_LSV_02_armed_F
	{
//		scope = 1;
	};
	class O_T_LSV_02_unarmed_F: LSV_02_unarmed_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_T_LSV_02_unarmed_viper_F: O_T_LSV_02_unarmed_F
	{
//		scope = 1;
	};
	class O_LSV_02_armed_F: LSV_02_armed_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_LSV_02_armed_viper_F: O_LSV_02_armed_F
	{
//		scope = 1;
	};
	class O_LSV_02_unarmed_F: LSV_02_unarmed_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_LSV_02_unarmed_viper_F: O_LSV_02_unarmed_F
	{
//		scope = 1;
	};
	class O_T_LSV_02_armed_black_F: O_T_LSV_02_armed_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class O_T_LSV_02_armed_ghex_F: O_T_LSV_02_armed_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class O_T_LSV_02_armed_arid_F: O_T_LSV_02_armed_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class O_T_LSV_02_unarmed_black_F: O_T_LSV_02_unarmed_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class O_T_LSV_02_unarmed_ghex_F: O_T_LSV_02_unarmed_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class O_T_LSV_02_unarmed_arid_F: O_T_LSV_02_unarmed_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class O_LSV_02_armed_black_F: O_LSV_02_armed_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class O_LSV_02_armed_ghex_F: O_LSV_02_armed_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class O_LSV_02_armed_arid_F: O_LSV_02_armed_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class O_LSV_02_unarmed_black_F: O_LSV_02_unarmed_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class O_LSV_02_unarmed_ghex_F: O_LSV_02_unarmed_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class O_LSV_02_unarmed_arid_F: O_LSV_02_unarmed_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class B_T_MRAP_01_F: B_MRAP_01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_T_MRAP_01_gmg_F: B_MRAP_01_gmg_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_T_MRAP_01_hmg_F: B_MRAP_01_hmg_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_T_MRAP_02_ghex_F: MRAP_02_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_T_MRAP_02_hmg_ghex_F: MRAP_02_hmg_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_T_MRAP_02_gmg_ghex_F: MRAP_02_gmg_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_GEN_Offroad_01_gen_F: Offroad_01_civil_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_Offroad_02_unarmed_F: Offroad_02_unarmed_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_Offroad_02_unarmed_black_F: C_Offroad_02_unarmed_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class C_Offroad_02_unarmed_blue_F: C_Offroad_02_unarmed_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class C_Offroad_02_unarmed_green_F: C_Offroad_02_unarmed_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class C_Offroad_02_unarmed_orange_F: C_Offroad_02_unarmed_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class C_Offroad_02_unarmed_red_F: C_Offroad_02_unarmed_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class C_Offroad_02_unarmed_white_F: C_Offroad_02_unarmed_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class I_C_Offroad_02_unarmed_F: Offroad_02_unarmed_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_C_Offroad_02_unarmed_brown_F: I_C_Offroad_02_unarmed_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class I_C_Offroad_02_unarmed_olive_F: I_C_Offroad_02_unarmed_F
	{
//		scope = 1;
//		scopeCurator = 1;
	};
	class O_T_Quadbike_01_ghex_F: Quadbike_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_T_Quadbike_01_F: Quadbike_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_T_Truck_01_mover_F: B_Truck_01_mover_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_T_Truck_01_ammo_F: B_Truck_01_ammo_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_T_Truck_01_box_F: B_Truck_01_box_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_T_Truck_01_fuel_F: B_Truck_01_fuel_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_T_Truck_01_medical_F: B_Truck_01_medical_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_T_Truck_01_Repair_F: B_Truck_01_Repair_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_T_Truck_01_transport_F: B_Truck_01_transport_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_T_Truck_01_covered_F: B_Truck_01_covered_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_T_Truck_03_transport_ghex_F: O_Truck_03_transport_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_T_Truck_03_covered_ghex_F: O_Truck_03_covered_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_T_Truck_03_repair_ghex_F: O_Truck_03_repair_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_T_Truck_03_ammo_ghex_F: O_Truck_03_ammo_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_T_Truck_03_fuel_ghex_F: O_Truck_03_fuel_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_T_Truck_03_medical_ghex_F: O_Truck_03_medical_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_T_Truck_03_device_ghex_F: O_Truck_03_device_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_T_UGV_01_ghex_F: UGV_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_T_UGV_01_rcws_ghex_F: UGV_01_rcws_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_C_Van_01_transport_F: Van_01_transport_base_F
	{
		scope = 1;//2
	};
	class I_C_Van_01_transport_brown_F: I_C_Van_01_transport_F
	{
//		scope = 1;
	};
	class I_C_Van_01_transport_olive_F: I_C_Van_01_transport_F
	{
//		scope = 1;
	};
	class B_AAA_System_01_F: AAA_System_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_SAM_System_01_F: SAM_System_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_SAM_System_02_F: SAM_System_02_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_Plane_Fighter_01_F: Plane_Fighter_01_Base_F
	{
		scope = 1;//2
	};
	class B_Plane_Fighter_01_Stealth_F: Plane_Fighter_01_Base_F
	{
		scope = 1;//2
	};
	class Plane_Fighter_01_Canopy_F: Plane_Canopy_Base_F
	{
//		scope = 1;
	};
	class B_Ejection_Seat_Plane_Fighter_01_F: Ejection_Seat_Plane_Fighter_01_base_F
	{
//		scope = 1;
	};
	class O_Plane_Fighter_02_F: Plane_Fighter_02_Base_F
	{
		scope = 1;//2
	};
	class O_Plane_Fighter_02_Stealth_F: Plane_Fighter_02_Base_F
	{
		scope = 1;//2
	};
	class Plane_Fighter_02_Canopy_F: Plane_Canopy_Base_F
	{
//		scope = 1;
	};
	class O_Ejection_Seat_Plane_Fighter_02_F: Ejection_Seat_Plane_Fighter_02_base_F
	{
//		scope = 1;
	};
	class I_Plane_Fighter_04_F: Plane_Fighter_04_Base_F
	{
		scope = 1;//2
	};
	class Plane_Fighter_04_Canopy_F: Plane_Canopy_Base_F
	{
//		scope = 1;
	};
	class I_Ejection_Seat_Plane_Fighter_04_F: Ejection_Seat_Plane_Fighter_04_base_F
	{
//		scope = 1;
	};
	class B_UAV_05_F: UAV_05_Base_F
	{
		scope = 1;//2
	};
	class C_IDAP_Offroad_01_F: Offroad_01_civil_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_IDAP_Offroad_02_unarmed_F: Offroad_02_unarmed_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_IDAP_Truck_02_F: Truck_02_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_IDAP_Truck_02_transport_F: Truck_02_transport_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_IDAP_Truck_02_water_F: Truck_02_water_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_IDAP_UGV_01_F: UGV_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_Van_02_transport_F: Van_02_transport_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_IDAP_Van_02_transport_F: Van_02_transport_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_G_Van_02_transport_F: Van_02_transport_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_G_Van_02_transport_F: Van_02_transport_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_GEN_Van_02_transport_F: Van_02_transport_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_G_Van_02_transport_F: Van_02_transport_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_C_Van_02_transport_F: Van_02_transport_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_Van_02_vehicle_F: Van_02_vehicle_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_IDAP_Van_02_vehicle_F: Van_02_vehicle_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_G_Van_02_vehicle_F: Van_02_vehicle_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_G_Van_02_vehicle_F: Van_02_vehicle_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_GEN_Van_02_vehicle_F: Van_02_vehicle_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_G_Van_02_vehicle_F: Van_02_vehicle_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class I_C_Van_02_vehicle_F: Van_02_vehicle_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_Van_02_medevac_F: Van_02_medevac_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_IDAP_Van_02_medevac_F: Van_02_medevac_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_Van_02_service_F: Van_02_service_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_Plane_CAS_01_Cluster_F: B_Plane_CAS_01_dynamicLoadout_F
	{
//		scope = 1;
		scopeCurator = 1;//2
	};
	class O_Plane_CAS_02_Cluster_F: O_Plane_CAS_02_dynamicLoadout_F
	{
//		scope = 1;
		scopeCurator = 1;//2
	};
	class I_Plane_Fighter_03_Cluster_F: I_Plane_Fighter_03_dynamicLoadout_F
	{
//		scope = 1;
		scopeCurator = 1;//2
	};
	class B_Plane_Fighter_01_Cluster_F: B_Plane_Fighter_01_F
	{
//		scope = 1;
		scopeCurator = 1;//2
	};
	class O_Plane_Fighter_02_Cluster_F: O_Plane_Fighter_02_F
	{
//		scope = 1;
		scopeCurator = 1;//2
	};
	class I_Plane_Fighter_04_Cluster_F: I_Plane_Fighter_04_F
	{
//		scope = 1;
		scopeCurator = 1;//2
	};
	class C_IDAP_Heli_Transport_02_F: Heli_Transport_02_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class C_IDAP_UAV_01_F: UAV_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_UAV_06_F: UAV_06_base_F
	{
		scope = 1;//2
	};
	class O_UAV_06_F: UAV_06_base_F
	{
		scope = 1;//2
	};
	class I_UAV_06_F: UAV_06_base_F
	{
		scope = 1;//2
	};
	class C_IDAP_UAV_06_F: UAV_06_base_F
	{
		scope = 1;//2
	};
	class C_UAV_06_F: UAV_06_base_F
	{
		scope = 1;//2
	};
	class C_IDAP_UAV_06_antimine_F: UAV_06_antimine_base_F
	{
		scope = 1;//2
	};
	class B_UAV_06_medical_F: UAV_06_medical_base_F
	{
		scope = 1;//2
	};
	class O_UAV_06_medical_F: UAV_06_medical_base_F
	{
		scope = 1;//2
	};
	class I_UAV_06_medical_F: UAV_06_medical_base_F
	{
		scope = 1;//2
	};
	class C_IDAP_UAV_06_medical_F: UAV_06_medical_base_F
	{
		scope = 1;//2
	};
	class C_UAV_06_medical_F: UAV_06_medical_base_F
	{
		scope = 1;//2
	};
	class B_APC_Wheeled_03_cannon_F: B_APC_Wheeled_03_base_F
	{
//		scope = 1;
	};

	class I_G_Offroad_01_AT_F: Offroad_01_AT_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class B_G_Offroad_01_AT_F: I_G_Offroad_01_AT_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class O_G_Offroad_01_AT_F: I_G_Offroad_01_AT_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class I_Truck_02_MRL_F: Truck_02_MRL_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class B_T_LSV_01_AT_F: LSV_01_AT_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class B_LSV_01_AT_F: LSV_01_AT_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class O_T_LSV_02_AT_F: LSV_02_AT_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class O_LSV_02_AT_F: LSV_02_AT_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class I_C_Offroad_02_LMG_F: Offroad_02_LMG_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class I_C_Offroad_02_AT_F: Offroad_02_AT_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class O_APC_Wheeled_02_rcws_v2_F: APC_Wheeled_02_base_v2_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class O_T_APC_Wheeled_02_rcws_v2_ghex_F: APC_Wheeled_02_base_v2_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class B_AFV_Wheeled_01_cannon_F: AFV_Wheeled_01_base_F
	{
		scope = 1;//2;
	};
	class B_T_AFV_Wheeled_01_cannon_F: AFV_Wheeled_01_base_F
	{
		scope = 1;//2;
	};
	class B_AFV_Wheeled_01_up_cannon_F: AFV_Wheeled_01_up_base_F
	{
		scope = 1;//2;
	};
	class B_T_AFV_Wheeled_01_up_cannon_F: AFV_Wheeled_01_up_base_F
	{
		scope = 1;//2;
	};
	class I_LT_01_AT_F: LT_01_AT_base_F
	{
		scope = 1;//2;
	};
	class I_LT_01_scout_F: LT_01_scout_base_F
	{
		scope = 1;//2;
	};
	class I_LT_01_AA_F: LT_01_AA_base_F
	{
		scope = 1;//2;
	};
	class I_LT_01_cannon_F: LT_01_cannon_base_F
	{
		scope = 1;//2;
	};
	class O_MBT_04_cannon_F: MBT_04_cannon_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class O_T_MBT_04_cannon_F: MBT_04_cannon_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class O_MBT_04_command_F: MBT_04_command_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class O_T_MBT_04_command_F: MBT_04_command_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class B_Ship_Gun_01_F: B_Ship_Gun_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_Ship_MRLS_01_F: B_Ship_MRLS_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_Radar_System_01_F: Radar_System_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_Radar_System_02_F: Radar_System_02_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_SAM_System_03_F: SAM_System_03_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class O_SAM_System_04_F: SAM_System_04_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};

	class B_T_UGV_01_olive_F: UGV_01_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_T_UGV_01_rcws_olive_F: UGV_01_rcws_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
};
