//EasyFly script by Celery, SQF conversion by i0n0s, tweaked by rocko
//Used to simplify fixed wing flight models
//
//Smaller 7 and 4 values decrease inertia
//_stallSpeed is speed in km/h: script stops working at lower speeds

sleep 1;

private["_plane","_vehicleClass","_fullSpeed","_stallSpeed","_speedRange","_planeSpeed","_coeff","_inertiaxy","_inertiaz","_vectorDirPlane","_planeSpeedKMH","_velocityPlane"];

_plane = vehicle player;

if ((_plane isKindOf "Plane") && (alive player)) then
{
	_vehicleClass = typeOf _plane;
	_fullSpeed = getNumber (configFile/"CfgVehicles"/_vehicleClass/"landingSpeed");
	_stallSpeed = _fullSpeed * 0.65;
	_speedRange = _fullSpeed - _stallSpeed;

	while {(player == (driver _plane)) && ((damage _plane) < 1)} do
	{
		_planeSpeed = speed _plane;

		if ((_planeSpeed <= _fullSpeed) /*&& (_planeSpeed > _stallSpeed)*/) then
		{
			_coeff = (_planeSpeed - _stallSpeed) / _speedRange;
			_inertiaxy = 7 / _coeff;
			_inertiaz = 4 / _coeff;
		}
		else
		{
			_inertiaxy = 7;
			_inertiaz = 4;
		};
		/*
		if (_planeSpeed > _fullSpeed) then
		{
			_inertiaxy = 7;
			_inertiaz = 4;
		};
		*/
		if (_planeSpeed > _stallSpeed) then
		{
			_vectorDirPlane = vectorDir _plane;
			_planeSpeedKMH = _planeSpeed / 3.6;
			_velocityPlane = velocity _plane;
			_plane setVelocity
			[
				(((_vectorDirPlane select 0) * _planeSpeedKMH) + ((_velocityPlane select 0) * _inertiaxy)) / (_inertiaxy + 1),
				(((_vectorDirPlane select 1) * _planeSpeedKMH) + ((_velocityPlane select 1) * _inertiaxy)) / (_inertiaxy + 1),
				(((_vectorDirPlane select 2) * _planeSpeedKMH) + ((_velocityPlane select 2) * _inertiaz)) / (_inertiaz + 1)
			];
		};

		sleep 0.04;
	};
};