class CfgPatches
{
	class EasyFlyPlanes_by_Celery_Gameplay_C_PvPscene
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Data_F_Loadorder","A3_Data_F_Curator_Loadorder","A3_Data_F_Kart_Loadorder","A3_Data_F_Bootcamp_Loadorder","A3_Data_F_Heli_Loadorder","A3_Data_F_Mark_Loadorder","A3_Data_F_Exp_A_Loadorder","A3_Data_F_Exp_B_Loadorder","A3_Data_F_Exp_Loadorder","A3_Data_F_Jets_Loadorder","A3_Data_F_Argo_Loadorder","A3_Data_F_Patrol_Loadorder","A3_Data_F_Orange_Loadorder","A3_Data_F_Tacops_Loadorder","A3_Data_F_Tank_Loadorder"};
		version = "2019-03-28";
	};
};
class CfgFunctions
{
	class PvPscene
	{
		tag = "PvPscene";
		project = "arma3";
		class EasyFlyPlanes_by_Celery_Gameplay_C_PvPscene_PreInit_EventHandlers
		{
			file = "\EasyFlyPlanes_by_Celery_Gameplay_C_PvPscene";
			class EasyFlyPlanes_by_Celery_Gameplay_C_PvPscene_preInit
			{
				description = "preInit code";
				preInit = 1; //(formerly known as "forced") 1 to call the function upon mission start, before objects are initialized. Passed arguments are ["preInit"]
//				postInit = 1; //1 to call the function upon mission start, after objects are initialized. Passed arguments are ["postInit"]
//				preStart = 1; //1 to call the function upon game start, before title screen, but after all addons are loaded.
//				recompile = 1; //1 to recompile the function upon mission start
//				ext = ".sqf"; //Set file type, can be ".sqf" or ".fsm" (meaning scripted FSM). Default is ".sqf".
//				headerType = -1; //Set function header type: -1 - no header; 0 - default header; 1 - system header.
			};
		};
	};
};
class DefaultEventHandlers;
class CfgVehicles
{
	class Air;
	class Plane: Air
	{
		class EventHandlers: DefaultEventHandlers
		{
			class EasyFlyPlanes_by_Celery_Gameplay_C_PvPscene
			{
				init = "[] spawn EasyFlyPlanes_by_Celery_Gameplay_C_PvPscene_Init_Pre;";
				getIn = "if (player == (_this select 2)) then { [] spawn EasyFlyPlanes_by_Celery_Gameplay_C_PvPscene_Init_Pre };";
			};
		};
	};
};
