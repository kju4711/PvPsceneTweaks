class CfgPatches
{
	class Optional_HideFromEditor_CUP_TerrainObjects_c
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"CUP_A10_Data","CUP_Air2_Data","CUP_Air3_Data","CUP_Air_Data","CUP_Air_d_baf_Data","CUP_Air_e_Data","CUP_Air_pmc_Data","CUP_CA_animals2","CUP_Buildings2_Data","CUP_Buildings2_Ind_Cementworks_Data","CUP_Buildings_Data","CUP_Ca_acr_Data","CUP_Ca_e_Data","CUP_Ca_pmc_Data","CUP_Characters2_Data","CUP_Cti_buildings_Data","CUP_CA_Data","CUP_Data_baf_Data","CUP_dbe1_data","CUP_Hotfix_Data","CUP_L39_Data","CUP_CALanguage","CUP_CALanguage_ACR","CUP_CALanguage_missions","CUP_CALanguage_missions_e","CUP_Misc3_Data","CUP_Misc_acr_Data","CUP_Misc_e_Data","CUP_CA_MPA","CUP_CA_Plants2_Clutter","CUP_CA_Plants_E_Clutter","CUP_CARoads2","CUP_CARoads2Dam","CUP_CARoads","CUP_CARoads_E","CUP_CARoads_PMC","CUP_CARocks2","CUP_CARocks","CUP_CARocks_E","CUP_CASigns","CUP_Sounds_Data","CUP_Sounds_Config","CUP_Sounds_e_Data","CUP_Structures_Data","CUP_jbad_sounds_doors","cup_jbad_light_switches","CUP_jbad_sounds_misc","CUP_Structures_e_Data","CUP_Structures_pmc_Data","CUP_Tracked2_Data","CUP_Tracked_Data","CUP_Tracked_e_Data","CUP_CAFonts","CUP_Water2_Data","CUP_Water_Data","CUP_Weapons2_Data","CUP_Weapons_Data","CUP_Weapons_e_Data","CUP_Weapons_pmc_Data","CUP_Wheeled2_Data","CUP_Wheeled_Data","CUP_Wheeled_e_Data","CUP_A1AlwaysDummy","CUP_AiA_compat","CUP_StandaloneTerrains_Dummy","CUP_Terrains_Plants","CUP_Models_DBE1_Data","CUP_Anims_DBE1","CUP_HMMWV_DBE1","CUP_Mercenary_DBE1","CUP_Music_DBE1","CUP_NPCs_DBE1","CUP_ploty_DBE1","CUP_Prisoners_DBE1","CUP_Roads_DBE1","CUP_UH60Desert","CUP_TKOH_Dummy","CUP_Hsim_Language_H","CUP_Hsim_Language_missions_H","CUP_DBE1_Hotfix","CUP_CALanguage_e","CUP_CALanguage_PMC","CUP_CALanguage_missions_PMC","CUP_CAWater2_seafox_EP1","CUP_CA_Plants2","CUP_CA_Plants2_Plant","CUP_CA_Plants2_Tree","CUP_CA_Plants_E2","CUP_CA_Plants_E","CUP_CA_Plants_E_Misc","CUP_CA_Plants_E_Plant","CUP_CA_Plants_E_Tree","CUP_CASigns2","CUP_CAUI","CUP_CAWater2_LHD","CUP_Models_DBE1_Config","CUP_UI_DBE1","CUP_CA_Config","CUP_CAData_ParticleEffects","CUP_CAMisc","CUP_Misc_Data","CUP_CA_Plants2_Bush","CUP_CAPlants","CUP_CA_Plants_E_Bush","CUP_pond_test","CUP_Editor_Config","CUP_Editor_A1_Roads_Config","CUP_Editor_A2_Roads_Config","CUP_Editor_A2_Railway_Config","CUP_Editor_Sidewalks_Config","CUP_Misc_DBE1","CUP_Buildings_Config","CUP_CABuildings_Misc","CUP_Hotfix_Config","CUP_CA_QGClutterHotfix","CUP_CA_Hotfix_vez_ropa","CUP_jbad_sounds","CUP_Structures_Config","CUP_CAStructures_A_BuildingWIP","CUP_CAStructures_A_CraneCon","CUP_CAStructuresLand_A_MunicipalOffice","CUP_CAStructuresBarn_W","CUP_CAStructures_Castle","CUP_CAStructuresHouse","CUP_CAStructuresHouse_A_FuelStation","CUP_CAStructuresHouse_A_Hospital","CUP_CAStructuresHouse_A_Office01","CUP_CAStructuresHouse_A_Office02","CUP_CAStructuresHouse_a_stationhouse","CUP_CAStructuresHouse_Church_02","CUP_CAStructuresHouse_Church_03","CUP_CAStructuresHouse_Church_05R","CUP_CAStructuresHouse_HouseBT","CUP_CAStructuresHouse_HouseV2","CUP_CAStructuresHouse_HouseV","CUP_CAStructuresLand_Ind_Stack_Big","CUP_CAStructures_IndPipe1","CUP_CAStructuresInd_Quarry","CUP_Ind_SawMill","CUP_CAStructures_Mil","CUP_CAStructures_Misc","CUP_CAStructures_Misc_Armory","CUP_CAStructures_Misc_Armory_Armor_Popuptarget","CUP_CAStructures_Misc_Powerlines","CUP_CAStructures_Nav","CUP_CAStructuresLand_Nav_Boathouse","CUP_CAStructures_Proxy_BuildingParts","CUP_CAStructures_Proxy_Ruins","CUP_CAStructures_Rail","CUP_CAStructuresHouse_rail_station_big","CUP_CAStructures_Ruins","CUP_CAStructuresShed_Small","CUP_CAStructuresHouse_Shed_Ind","CUP_CAStructures_Wall","CUP_New_Buildings","CUP_Editor_Buildings_Config","CUP_Editor_Structures_Config","CUP_Kamenolom_DBE1","CUP_Pila_DBE1","CUP_Vysilac_DBE1","CUP_Zakladna_DBE1","CUP_Buildings2_Config","CUP_A_Crane_02","CUP_A_GeneralStore_01","CUP_CABuildings2_A_Pub","CUP_A_statue","CUP_Barn_Metal","CUP_CABuildingParts","CUP_CABuildingParts_Signs","CUP_CATEC","CUP_Church_01","CUP_Farm_Cowshed","CUP_Farm_WTower","CUP_CAHouseBlock_A","CUP_CAHouseBlock_B","CUP_CAHouseBlock_C","CUP_CAHouseBlock_D","CUP_HouseRuins","CUP_Ind_Dopravnik","CUP_Ind_Expedice","CUP_Ind_MalyKomin","CUP_Ind_Mlyn","CUP_Ind_Pec","CUP_ind_silomale","CUP_Ind_SiloVelke","CUP_Ind_Vysypka","CUP_Ind_Garage01","CUP_CAStructures_IndPipe1_todo_delete","CUP_IndPipe2","CUP_Ind_Shed_01","CUP_Ind_Shed_02","CUP_Ind_Tank","CUP_Ind_Workshop01","CUP_CABuildings2_Misc_Cargo","CUP_Misc_PowerStation","CUP_Misc_WaterStation","CUP_Rail_House_01","CUP_Shed_small","CUP_Shed_wooden","CUP_particle_effects","CUP_CAMisc2","CUP_Misc3_Config","CUP_WarfareBuildings","CUP_Misc_e_Config","CUP_CAMisc_E_WF","CUP_CAMP_Armory_Misc","CUP_CAMP_Armory_Misc_Concrete_Wall","CUP_CAMP_Armory_Misc_Entrance_Gate","CUP_CAMP_Armory_Misc_Info_Board","CUP_CAMP_Armory_Misc_Infostands","CUP_CAMP_Armory_Misc_Laptop","CUP_CAMP_Armory_Misc_Loudspeakers","CUP_CAMP_Armory_Misc_Plasticpole","CUP_CAMP_Armory_Misc_Red_Light","CUP_CAMP_Armory_Misc_Sign_Armex","CUP_CAMP_Armory_Misc_Sign_Direction","CUP_CA_Plants2_Misc","CUP_CA_Plants_PMC","CUP_CARoads2Bridge","CUP_CARoads_PMC_Bridge","CUP_CASigns_E","CUP_A_TVTower","CUP_CAStructures_Nav_pier","CUP_CAStructures_Railway","CUP_Structures_e_Config","CUP_CAStructures_E_HouseA","CUP_CAStructures_E_HouseA_A_BuildingWIP","CUP_CAStructures_E_HouseA_A_CityGate1","CUP_CAStructures_E_HouseA_A_Minaret","CUP_CAStructures_E_HouseA_A_Minaret_Porto","CUP_CAStructures_E_HouseA_A_Mosque_big","CUP_CAStructures_E_HouseA_A_Mosque_small","CUP_CAStructures_E_HouseA_A_Office01","CUP_CAStructures_E_HouseA_a_stationhouse","CUP_CAStructures_E_HouseA_A_Statue","CUP_CAStructures_E_HouseA_A_Villa","CUP_CAStructures_E_HouseC","CUP_CAStructures_E_HouseK","CUP_CAStructures_E_HouseL","CUP_CAStructures_E_Ind","CUP_CAStructures_E_Ind_Ind_Coltan_Mine","CUP_CAStructures_E_Ind_Ind_FuelStation","CUP_CAStructures_E_Ind_Ind_Garage01","CUP_CAStructures_E_Ind_Oil_Mine","CUP_CAStructures_E_Ind_IndPipes","CUP_CAStructures_E_Ind_Misc_PowerStation","CUP_CAStructures_E_Ind_Ind_Shed","CUP_CAStructures_E_Mil","CUP_CAStructures_E_Misc","CUP_CAStructures_E_Misc_Misc_cables","CUP_CAStructures_E_Misc_Misc_Construction","CUP_CAStructures_E_Misc_Misc_Garbage","CUP_CAStructures_E_Misc_Misc_Interier","CUP_CAStructures_E_Misc_Misc_Lamp","CUP_CAStructures_E_Misc_Misc_Market","CUP_CAStructures_E_Misc_Misc_powerline","CUP_CAStructures_E_Misc_Misc_Water","CUP_CAStructures_E_Misc_Misc_Well","CUP_CAStructures_E_Wall","CUP_CAStructures_E_Wall_Wall_L","CUP_Structures_pmc_Config","CUP_CAStructures_PMC_Buildings","CUP_CAStructures_PMC_Buildings_Bunker","CUP_CAStructures_PMC_Buildings_GeneralStore_PMC","CUP_CAStructures_PMC_Buildings_Ruin_Cowshed","CUP_CAStructures_PMC_Ind","CUP_CAStructures_PMC_FuelStation","CUP_CAStructures_PMC_Misc","CUP_CAStructures_PMC_Misc_Shed","CUP_CAStructures_PMC_Ruins","CUP_CAStructures_PMC_Walls","CUP_Editor_Plants_Config","CUP_Editor_Rocks_Config","CUP_Editor_Signs_Config","CUP_ibr_plants","CUP_Data_baf_Config","CUP_CALanguage_Baf","CUP_CALanguageMissions_baf","CUP_Misc_acr_Config","CUP_CAMisc_ACR_3DMarkers","CUP_CAMisc_ACR_Container","CUP_CAMisc_ACR_Dog","CUP_CAMisc_ACR_Helpers","CUP_CAMisc_ACR_PBX","CUP_CAMisc_ACR_ScaffoldingSmall","CUP_CAMisc_ACR_Shooting_range","CUP_CAMisc_ACR_Sign_Mines","CUP_CAMisc_ACR_Targets","CUP_CAMisc_ACR_Targets_InvisibleTarget","CUP_CAMisc_ACR_TestSphere","CUP_CAMisc_BAF","CUP_BaseConfig_F","CUP_Core","CUP_StandaloneTerrains_Core","CUP_StandaloneTerrains_Core_Faction","CUP_StandaloneTerrains_Core_VehicleClass","CUP_Worlds","CUP_Worlds_Ambient","CUP_Worlds_Author","CUP_Worlds_Clutter","CUP_Worlds_ClutterDist","CUP_Worlds_Delete","CUP_Worlds_DustEffects","CUP_Worlds_EnvSounds","CUP_Worlds_FullDetailDist","CUP_Worlds_Grid","CUP_Worlds_Intros","CUP_Worlds_Lighting","CUP_CWA_Lighting","CUP_Desert_Lighting","CUP_European_Lighting","CUP_Tropical_Lighting","CUP_Worlds_MapSize","CUP_Worlds_MidDetailTexture","CUP_Worlds_NoDetailDist","CUP_Worlds_PictureMap","CUP_Worlds_PictureShot","CUP_Worlds_PreviewVideo","CUP_Worlds_Seabed","CUP_Worlds_SkyTexture","CUP_Worlds_StreetLamp","CUP_Worlds_Surfaces","CUP_Worlds_Water","CUP_A1_EditorObjects","CUP_A2_EditorObjects"};
		version = "2019-03-28";
	};
};
class CfgVehicles
{
	class AmmoCrate_NoInteractive_Base_EP1;
	class BarrelBase;
	class Base_CUP_A1_Road;
	class Base_CUP_A2_Road;
	class Base_CUP_Bush;
	class Base_CUP_Furniture;
	class Base_CUP_Plant;
	class Base_CUP_Railway;
	class Base_CUP_Sidewalk;
	class Base_CUP_Tree;
	class Base_WarfareBAircraftFactory;
	class BASE_WarfareBAntiAirRadar;
	class BASE_WarfareBArtilleryRadar;
	class Base_WarfareBBarrier10x;
	class Base_WarfareBBarrier10xTall;
	class BASE_WarfareBFieldhHospital;
	class Base_WarfareBHeavyFactory;
	class Base_WarfareBLightFactory;
	class Base_WarfareBUAVterminal;
	class Base_WarfareBVehicleServicePoint;
	class Camp_base;
	class Church;
	class CUP_Wall;
	class FlagCarrier;
	class FlagCarrierCore;
	class FootBridge_Base_ACR;
	class Helper_Base_EP1;
	class House;
	class House_EP1;
	class Lamps_base_F;
	class Land_CamoNetB_EAST;
	class Land_CamoNetB_NATO;
	class Land_CamoNetVar_EAST;
	class Land_CamoNetVar_NATO;
	class Land_Ind_TankSmall2;
	class Land_Vysilac_FM2;
	class Military_Item_NoInteractive;
	class Misc_thing;
	class Misc_thing_NoInteractive;
	class misc01;
	class NonStrategic;
	class PaletaBase;
	class Rocks_base_F;
	class Ruins;
	class Ruins_EP1;
	class Ship;
	class Sign_1L_Firstaid;
	class Sign_1L_Noentry;
	class Sign_Checkpoint;
	class Sign_MP_blu;
	class Sign_MP_ind;
	class Sign_MP_op;
	class Small_items;
	class Small_Items_EP1;
	class Small_items_NoInteractive;
	class Static;
	class StaticWeapon;
	class Strategic;
	class TargetBase;
	class TargetFakeTank;
	class TargetGrenadBase;
	class Thing;
	class Thing_EP1;
	class Thing_NoInteractive_EP1;
	class TK_GUE_WarfareBContructionSite1_Base_EP1;
	class TK_WarfareBContructionSite1_Base_EP1;
	class US_WarfareBContructionSite1_Base_EP1;
	class Wall_F;
	class Warfare_HQ_base_unfolded;
	class WarfareBBaseStructure;
	class WarfareBMGNest_M240_base;
	class Wreck_Base;

	class HeliH: NonStrategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dam_Conc_20: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class JeepWreck1: Strategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Fence: NonStrategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Company3_2: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class RoadCone: Thing
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Obstacle_saddle: NonStrategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class RampConcrete: NonStrategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class HumpsDirt: NonStrategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class BuoySmall: Ship
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class BuoyBig: Ship
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class RoadBarrier_light: NonStrategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Kontejner: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_popelnice: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_podlejzacka: Strategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_jezekbeton: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_fuel_tank_small: Strategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Barrels: BarrelBase
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Barrel1: BarrelBase
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Barrel2: BarrelBase
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Barrel3: BarrelBase
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Barrel4: BarrelBase
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Barrel5: BarrelBase
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Barrel6: BarrelBase
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Camera1: Thing
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Paleta1: PaletaBase
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Paleta2: PaletaBase
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Vec03: misc01
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_vez_ropa: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class FlagCarrierWest: FlagCarrier
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Danger: FlagCarrier
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class DangerWest: FlagCarrier
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class DangerEAST: FlagCarrier
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class DangerGUE: FlagCarrier
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Fire: NonStrategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class xxxPhotos: NonStrategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_water_tank: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TargetTraining: NonStrategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_drevtank_ruin: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_sloup_vn: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_sloup_vn_dratZ: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_sloup_vn_drat: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_sloup_vn_drat_d: Land_sloup_vn_drat
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_ladder: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_ladder_half: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class ClutterCutter: Thing
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class snowman: Static
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class snow: Static
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class CUP_A1_Road_asf10_100: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_asf10_25: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_asf10_50: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_asf10_75: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_asf12: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_asf12_var_1: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_asf25: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_asf6: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_asf6konec: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_asf6_prechod: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_asfaltka10_50: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_asfaltka10_75: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_asfaltka12: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_asfaltka25: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_asfaltka6: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_asfaltka6konec: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_asfatlka10_100: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_asfatlka10_25: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_Bilboard_HELLMART: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_ces10_100: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_ces10_25: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_ces10_50: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_ces10_75: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_ces12: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_ces25: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_ces6: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_ces6konec: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_ces_d10_100: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_ces_d10_25: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_ces_d10_50: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_ces_d10_75: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_ces_d12: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_ces_d25: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_ces_d6: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_ces_d6konec: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_dalnice_18: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_dalnice_18_redukce: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_empty6: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_kos10_100: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_kos10_25: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_kos10_50: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_kos10_75: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_kos12: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_kos25: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_kos6: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_kos6konec: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_kr_asfaltka_asfaltka_t: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_kr_asfaltka_cesta_t: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_kr_asfaltka_sil_t: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_kr_new_asf_asf_t: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_kr_new_asf_ces_t: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_kr_new_asf_sil_t: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_kr_new_kos: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_kr_new_kos_kos_t: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_kr_new_kos_sil_t: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_kr_new_silxsil: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_kr_new_sil_asf_t: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_kr_new_sil_ces_t: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_kr_new_sil_kos_t: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_kr_new_sil_sil_t: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_kr_new_sil_sil_t_test: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_kr_silnicexsilnice: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_kr_silnice_asfaltka_t: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_kr_silnice_cesta_t: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_kr_silnice_silnice_t: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_main10_25: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_main_25: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_najezd_benzina: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_pesina10_25: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_pesina12: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_pesina6: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_road_invisible: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_runway_beton: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_runway_dirt: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_runway_dirt_1: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_runway_dirt_2: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_runway_dirt_3: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_runway_end0: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_runway_end128: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_runway_end27: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_runway_end9: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_runway_main: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_runway_main_Tcross: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_runway_pojdraha: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_runway_poj_end27: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_runway_poj_end9: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_runway_poj_Tcross: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_sf6: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_sil10_100: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_sil10_25: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_sil10_50: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_sil10_75: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_sil12: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_sil25: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_sil6: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_sil6konec: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_sil6_prechod: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_silnice10_100: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_silnice10_25: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_silnice10_50: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_silnice10_75: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_silnice12: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_silnice25: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_silnice6: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_silnice6konec: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_Silnice_mesto_6: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_Silnice_mesto_LP_6: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_Silnice_mesto_predX_6: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_Silnice_mesto_T_12: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_Silnice_mesto_X_12: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_Silnice_mesto_zJZ: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_Silnice_mesto_zVS: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_Silnice_mesto_zZJ: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_Silnice_mesto_zZS: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_smazat10_100: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_smazat10_25: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_smazat10_25inv: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_smazat10_25_prava: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_smazat10_50: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_smazat10_75: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_smazat12: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_smazat12inv: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_smazat25: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_smazat6: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_VoidPath10_100: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_VoidPath10_25: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_VoidPath10_50: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_VoidPath10_75: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_VoidPathTVoidPath_left: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_VoidPathTVoidPath_right: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_VoidPathXVoidPath: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_VoidPath_12: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_VoidPath_25: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_VoidPath_6: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A1_Road_VoidPath_6_end: Base_CUP_A1_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf1_0_2000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf1_1_1000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf1_10_100: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf1_10_25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf1_10_50: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf1_10_75: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf1_12: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf1_15_75: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf1_22_50: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf1_25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf1_30_25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf1_6: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf1_60_10: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf1_6konec: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf1_6_crosswalk: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf1_7_100: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf2_0_2000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf2_1_1000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf2_10_100: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf2_10_25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf2_10_50: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf2_10_75: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf2_12: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf2_15_75: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf2_22_50: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf2_25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf2_30_25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf2_6: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf2_60_10: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf2_6konec: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf2_6_crosswalk: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf2_7_100: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf3_0_2000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf3_1_1000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf3_10_100: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf3_10_25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf3_10_50: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf3_10_75: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf3_12: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf3_15_75: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf3_22_50: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf3_25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf3_30_25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf3_6: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf3_60_10: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf3_6konec: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_asf3_7_100: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_city_0_2000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_city_1_1000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_city_10_100: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_city_10_25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_city_10_50: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_city_10_75: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_city_12: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_city_15_75: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_city_22_50: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_city_25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_city_30_25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_city_6: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_city_60_10: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_city_6konec: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_city_6_crosswalk: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_city_7_100: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_grav_0_2000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_grav_1_1000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_grav_10_100: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_grav_10_25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_grav_10_50: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_grav_10_75: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_grav_12: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_grav_15_75: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_grav_22_50: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_grav_25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_grav_30_25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_grav_6: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_grav_60_10: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_grav_6konec: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_grav_7_100: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_kr_t_asf1_asf2: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_kr_t_asf1_asf3: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_kr_t_asf1_city: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_kr_t_asf2_asf2: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_kr_t_asf2_asf3: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_kr_t_asf3_asf2: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_kr_t_asf3_asf3: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_kr_t_asf3_grav: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_kr_t_asf3_mud: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_kr_t_city_asf3: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_kr_t_city_city: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_kr_t_mud_mud: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_kr_x_asf1_asf3: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_kr_x_asf1_city: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_kr_x_asf2_asf3: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_kr_x_asf2_city: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_kr_x_city_asf3: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_kr_x_city_city: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_kr_x_city_city_asf3: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_mud_0_2000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_mud_1_1000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_mud_10_100: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_mud_10_25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_mud_10_50: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_mud_10_75: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_mud_12: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_mud_15_75: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_mud_22_50: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_mud_25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_mud_30_25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_mud_6: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_mud_60_10: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_mud_6konec: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_mud_7_100: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_path_0_2000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_path_1_1000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_path_10_100: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_path_10_25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_path_10_50: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_path_10_75: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_path_12: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_path_15_75: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_path_22_50: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_path_25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_path_30_25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_path_6: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_path_60_10: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_path_6konec: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_path_7_100: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_road_invisible: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_road_invisible_t: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_runwayold_40_main: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_runwayold_80_dirt: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_runway_beton: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_runway_beton_end1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_runway_beton_end2: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_runway_dirt_1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_runway_dirt_2: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_runway_dirt_3: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_runway_end15: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_runway_end33: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_runway_main: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_runway_main_40: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_runway_poj_draha: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_runway_poj_L_1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_runway_poj_L_1_end: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_runway_poj_L_2: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_runway_poj_L_2_end: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_runway_poj_spoj: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_runway_poj_spoj_2: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_runway_poj_T_1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_runway_poj_T_2: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_bridge_asf1_25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_Bridge_wood_25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf10_W10_A0_286_R2000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf10_W10_A0_573_R1000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf10_W10_A0_573_R2000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf10_W10_A11_459_R100: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf10_W10_A11_459_R50: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf10_W10_A15_279_R75: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf10_W10_A1_146_R1000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf10_W10_A1_146_R500: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf10_W10_A22_918_R25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf10_W10_A22_918_R50: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf10_W10_A2_292_R500: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf10_W10_A2_865_R200: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf10_W10_A45_837_R25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf10_W10_A5_73_R100: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf10_W10_A5_73_R200: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf10_W10_A7_639_R75: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf10_W10_L10: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf10_W10_L10_term: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf10_W10_L20: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf12_W12_A0_344_R2000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf12_W12_A0_688_R1000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf12_W12_A0_688_R2000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf12_W12_A13_751_R100: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf12_W12_A13_751_R50: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf12_W12_A18_335_R75: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf12_W12_A1_375_R1000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf12_W12_A1_375_R500: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf12_W12_A27_502_R25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf12_W12_A27_502_R50: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf12_W12_A2_75_R500: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf12_W12_A3_438_R200: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf12_W12_A55_004_R25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf12_W12_A68_755_R10: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf12_W12_A6_875_R100: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf12_W12_A6_875_R200: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf12_W12_A9_167_R75: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf12_W12_L12: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf12_W12_L12_term: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf12_W12_L24: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf1_02000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf1_11000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf1_12: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf1_1575: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf1_2250: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf1_25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf1_3025: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf1_6: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf1_6010: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf1_6konec: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf1_7100: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf2_02000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf2_11000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf2_12: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf2_1575: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf2_2250: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf2_25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf2_3025: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf2_6: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf2_6010: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf2_6konec: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_asf2_7100: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt10_W10_A0_286_R2000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt10_W10_A0_573_R1000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt10_W10_A0_573_R2000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt10_W10_A11_459_R100: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt10_W10_A11_459_R50: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt10_W10_A15_279_R75: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt10_W10_A1_146_R1000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt10_W10_A1_146_R500: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt10_W10_A22_918_R25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt10_W10_A22_918_R50: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt10_W10_A2_292_R500: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt10_W10_A2_865_R200: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt10_W10_A45_837_R25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt10_W10_A57_296_R10: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt10_W10_A5_73_R100: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt10_W10_A5_73_R200: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt10_W10_A7_639_R75: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt10_W10_L10: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt10_W10_L10_term: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt10_W10_L20: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt1_02000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt1_11000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt1_12: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt1_1575: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt1_2250: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt1_25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt1_3025: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt1_6: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt1_6010: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt1_6konec: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt1_7100: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt2_02000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt2_11000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt2_12: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt2_1575: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt2_2250: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt2_25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt2_3025: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt2_6: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt2_6010: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt2_6konec: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt2_7100: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt7_W7_A0_201_R2000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt7_W7_A0_401_R1000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt7_W7_A0_401_R2000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt7_W7_A0_802_R1000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt7_W7_A0_802_R500: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt7_W7_A10_695_R75: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt7_W7_A16_043_R25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt7_W7_A16_043_R50: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt7_W7_A1_604_R500: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt7_W7_A2_005_R200: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt7_W7_A32_086_R25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt7_W7_A40_107_R10: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt7_W7_A4_011_R100: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt7_W7_A4_011_R200: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt7_W7_A5_348_R75: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt7_W7_A80_214_R10: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt7_W7_A8_021_R100: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt7_W7_A8_021_R50: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt7_W7_L14: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt7_W7_L7: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_dirt7_W7_L7_term: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_kr_t_asf10_W10_asf10_W10_asf10_W10: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_kr_t_asf10_W10_asf10_W10_asf12_W12: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_kr_t_asf10_W10_asf10_W10_dirt10_W10: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_kr_t_asf10_W10_asf10_W10_dirt7_W7: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_kr_t_asf12_W12_asf12_W12_asf10_W10: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_kr_t_asf12_W12_asf12_W12_asf12_W12: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_kr_t_asf12_W12_asf12_W12_dirt10_W10: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_kr_t_asf12_W12_asf12_W12_dirt7_W7: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_kr_t_asf1_asf2: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_kr_t_asf1_dirt1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_kr_t_asf1_dirt2: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_kr_t_asf2_dirt2: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_kr_t_dirt10_W10_dirt10_W10_asf10_W10: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_kr_t_dirt10_W10_dirt10_W10_dirt10_W10: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_kr_t_dirt10_W10_dirt10_W10_dirt7_W7: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_kr_t_dirt7_W7_dirt7_W7_asf10_W10: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_kr_t_dirt7_W7_dirt7_W7_dirt10_W10: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_kr_t_dirt7_W7_dirt7_W7_dirt7_W7: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_kr_x_asf10_W10_dirt10_W10_asf10_W10_asf10_W10: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_kr_x_asf10_W10_dirt7_W7_asf10_W10_asf10_W10: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_kr_x_asf10_W10_dirt7_W7_dirt7_W7_asf10_W10: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_kr_x_asf12_W12_asf10_W10_asf12_W12_asf10_W10: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_kr_x_asf12_W12_asf10_W10_asf12_W12_asf12_W12: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_kr_x_asf12_W12_asf12_W12_asf12_W12_asf12_W12: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_kr_x_asf12_W12_dirt10_W10_asf12_W12_dirt10_W10: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_kr_x_asf12_W12_dirt7_W7_asf12_W12_asf10_W10: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_kr_x_asf12_W12_dirt7_W7_asf12_W12_dirt10_W10: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_kr_x_asf1_asf1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_kr_x_asf1_asf2: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_kr_x_asf1_dirt2: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_kr_x_dirt10_W10_asf10_W10_dirt10_W10_dirt10_W10: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_kr_x_dirt10_W10_dirt7_W7_dirt10_W10_dirt10_W10: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_kr_x_dirt10_W10_dirt7_W7_dirt10_W10_dirt7_W7: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_kr_x_dirt7_W7_dirt7_W7_dirt7_W7_dirt7_W7: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_path_02000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_path_11000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_path_12: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_path_1575: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_path_2250: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_path_25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_path_3025: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_path_6: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_path_6010: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_path_6konec: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_path_7100: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_road_invisible: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_road_invisible_t: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_runway_beton_end1_ep1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_runway_beton_end2_ep1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_runway_beton_ep1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_runway_dirt_1_ep1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_runway_dirt_2_ep1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_runway_dirt_3_ep1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_runway_end00_ep1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_runway_end04_ep1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_runway_end06_ep1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_runway_end09_ep1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_runway_end15_ep1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_runway_end18_ep1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_runway_end22_ep1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_runway_end24_ep1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_runway_end27_ep1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_runway_end33_ep1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_runway_main_40_ep1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_runway_main_ep1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_runway_poj_draha_ep1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_runway_poj_L_1_end_ep1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_runway_poj_L_1_ep1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_runway_poj_L_2_end_ep1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_runway_poj_L_2_ep1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_runway_poj_spoj_2_ep1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_runway_poj_spoj_ep1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_runway_poj_T_1_ep1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_runway_poj_T_2_ep1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_SW_A_body_6m_EP1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_SW_A_end_L_EP1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_SW_A_end_R_EP1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_SW_A_turn_EP1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_SW_B_body_6m_EP1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_SW_B_end_L_EP1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_SW_B_end_R_EP1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_SW_C_body_6m_EP1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_SW_C_crossT_EP1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_SW_C_end_L_EP1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_SW_C_end_R_EP1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_OA_SW_C_turn_EP1: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_PMC_asf1_02000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_PMC_asf1_11000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_PMC_asf1_12: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_PMC_asf1_25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_PMC_asf1_6: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_PMC_asf1_6konec: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_PMC_asf2_11000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_PMC_asf2_12: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_PMC_asf2_1575: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_PMC_asf2_25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_PMC_asf2_7100: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_PMC_dirt2_11000: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_PMC_dirt2_12: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_PMC_dirt2_1575: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_PMC_dirt2_2250: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_PMC_dirt2_25: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_PMC_dirt2_3025: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_PMC_dirt2_6konec: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_PMC_dirt2_7100: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_PMC_kr_t_asf12_W12_asf12_W12_asf10_W10: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_PMC_kr_t_asf12_W12_asf12_W12_dirt7_W7: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_PMC_kr_t_dirt7_W7_dirt7_W7_dirt7_W7: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_PMC_bridge_asf_PMC: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_PMC_SW_C_body_6m_PMC: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_PMC_SW_C_end_L_PMC: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_PMC_SW_C_end_R_PMC: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_A2_Road_PMC_SW_C_turn_PMC: Base_CUP_A2_Road
	{
		scope = 1;//2;
	};
	class CUP_Railway_railsN_25: Base_CUP_Railway
	{
		scope = 1;//2;
	};
	class CUP_Railway_railsN_40: Base_CUP_Railway
	{
		scope = 1;//2;
	};
	class CUP_Railway_railsN_curve_L25_10: Base_CUP_Railway
	{
		scope = 1;//2;
	};
	class CUP_Railway_railsN_curve_L25_5: Base_CUP_Railway
	{
		scope = 1;//2;
	};
	class CUP_Railway_railsN_curve_L30_20: Base_CUP_Railway
	{
		scope = 1;//2;
	};
	class CUP_Railway_railsN_curve_R25_10: Base_CUP_Railway
	{
		scope = 1;//2;
	};
	class CUP_Railway_railsN_curve_R25_5: Base_CUP_Railway
	{
		scope = 1;//2;
	};
	class CUP_Railway_railsN_curve_R30_20: Base_CUP_Railway
	{
		scope = 1;//2;
	};
	class CUP_Railway_railsN_turnout_L: Base_CUP_Railway
	{
		scope = 1;//2;
	};
	class CUP_Railway_railsN_turnout_R: Base_CUP_Railway
	{
		scope = 1;//2;
	};
	class CUP_Railway_rails_25: Base_CUP_Railway
	{
		scope = 1;//2;
	};
	class CUP_Railway_rails_curve_L25_10: Base_CUP_Railway
	{
		scope = 1;//2;
	};
	class CUP_Railway_rails_curve_R25_10: Base_CUP_Railway
	{
		scope = 1;//2;
	};
	class CUP_Railway_rails_down_25: Base_CUP_Railway
	{
		scope = 1;//2;
	};
	class CUP_Railway_rails_down_40: Base_CUP_Railway
	{
		scope = 1;//2;
	};
	class CUP_Railway_rails_linebreak_concrete: Base_CUP_Railway
	{
		scope = 1;//2;
	};
	class CUP_Railway_rails_linebreak_iron: Base_CUP_Railway
	{
		scope = 1;//2;
	};
	class CUP_Railway_rails_passing_10: Base_CUP_Railway
	{
		scope = 1;//2;
	};
	class CUP_Railway_rails_passing_25: Base_CUP_Railway
	{
		scope = 1;//2;
	};
	class CUP_Railway_rails_turnout_L: Base_CUP_Railway
	{
		scope = 1;//2;
	};
	class CUP_Railway_rails_turnout_R: Base_CUP_Railway
	{
		scope = 1;//2;
	};
	class CUP_Railway_rails_up_25: Base_CUP_Railway
	{
		scope = 1;//2;
	};
	class CUP_Railway_rails_up_40: Base_CUP_Railway
	{
		scope = 1;//2;
	};
	class CUP_Railway_rails_v1_LB_RE: Base_CUP_Railway
	{
		scope = 1;//2;
	};
	class CUP_Railway_rails_v1_LE_RB: Base_CUP_Railway
	{
		scope = 1;//2;
	};
	class CUP_Railway_rails_v_LB_RE: Base_CUP_Railway
	{
		scope = 1;//2;
	};
	class CUP_Railway_rails_v_LE_RB: Base_CUP_Railway
	{
		scope = 1;//2;
	};
	class CUP_Railway_rails_v_SP: Base_CUP_Railway
	{
		scope = 1;//2;
	};
	class CUP_Railway_rails_bridge_rail: Base_CUP_Railway
	{
		scope = 1;//2;
	};
	class CUP_Railway_rail_50km: Base_CUP_Railway
	{
		scope = 1;//2;
	};
	class CUP_Railway_rail_KoniecNastupista: Base_CUP_Railway
	{
		scope = 1;//2;
	};
	class CUP_Railway_rail_najazdovarampa: Base_CUP_Railway
	{
		scope = 1;//2;
	};
	class CUP_Railway_rail_o25m_Priecestie: Base_CUP_Railway
	{
		scope = 1;//2;
	};
	class CUP_Railway_rail_Priecestie: Base_CUP_Railway
	{
		scope = 1;//2;
	};
	class CUP_Railway_Rail_Platform_Cross: Base_CUP_Railway
	{
		scope = 1;//2;
	};
	class CUP_Railway_Rail_Platform_Segment: Base_CUP_Railway
	{
		scope = 1;//2;
	};
	class CUP_Railway_Rail_Platform_Start: Base_CUP_Railway
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_Sidewalk6Clear: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_Sidewalk6ClearLong: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_Sidewalk6ClearMiddle: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_Sidewalk6ClearShort: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_Sidewalk6Corner: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_Sidewalk6Crossing: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_Sidewalk6CrossingT: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_Sidewalk6ShortEnd: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_Sidewalk6Turn10deg: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_Sidewalk6Turn20deg: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_Sidewalk6Turn30deg: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_Sidewalk6Turn5deg: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_SidewalkClear: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_SidewalkClearLong: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_SidewalkClearMiddle: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_SidewalkClearShort: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_SidewalkCorner: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_SidewalkCrossing: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_SidewalkCrossingT: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_SidewalkSClear: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_SidewalkSClearLong: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_SidewalkSClearMiddle: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_SidewalkSClearShort: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_SidewalkSCorner: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_SidewalkSCrossing: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_SidewalkSCrossingT: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_SidewalkShortEnd: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_SidewalkSShortEnd: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_SidewalkSTurn10deg: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_SidewalkSTurn20deg: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_SidewalkSTurn30deg: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_SidewalkSTurn5deg: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_SidewalkSVRClear: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_SidewalkSVRClearLong: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_SidewalkSVRClearMiddle: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_SidewalkSVRClearShort: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_SidewalkSVRCorner: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_SidewalkSVRCrossing: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_SidewalkSVRCrossingT: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_SidewalkSVRShortEnd: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_SidewalkSVRTurn10deg: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_SidewalkSVRTurn20deg: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_SidewalkSVRTurn30deg: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_SidewalkSVRTurn5deg: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_SidewalkTurn10deg: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_SidewalkTurn20deg: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_SidewalkTurn30deg: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class CUP_Sidewalk_SidewalkTurn5deg: Base_CUP_Sidewalk
	{
		scope = 1;//2;
	};
	class Fortress1: Strategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Shed: Strategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Camp: Camp_base
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class CampEmpty: Camp
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class CampEast: Camp_base
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class CampEastC: Camp
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class ACamp: Camp_base
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class MASH: Camp_base
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Grave: NonStrategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class GraveCross1: Grave
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class GraveCross2: Grave
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class GraveCrossHelmet: Grave
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class WireFence: NonStrategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Wall1: CUP_Wall
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Zidka_branka: CUP_Wall
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Wallend: Wall_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Zidka03: Wall_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_AFbarabizna_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_AFDum_mesto2_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_AFDum_mesto2L_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_AFDum_mesto3_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_AFHospoda_mesto_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Army_hut2_int_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Army_hut2_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Army_hut3_long_int_ruins: Ruins
	{
		scope = 1;//
		scopeCurator = 1;//2;
	};
	class Land_Army_hut3_long_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Army_hut_int_ruins: Ruins
	{
		scope = 1;//
		scopeCurator = 1;//2;
	};
	class Land_Army_hut_storrage_ruins: Ruins
	{
		scope = 1;//
		scopeCurator = 1;//2;
	};
	class Land_Bouda1_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Bouda3_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Budova1_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Budova2_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Budova3_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Budova4_in_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Budova4_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Budova5_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Cihlovej_Dum_in_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Cihlovej_Dum_mini_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Cihlovej_Dum_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Deutshe_mini_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Deutshe_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Domek_rosa_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dulni_bs_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum01_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum02_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_istan2_01_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_istan2_02_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_istan2_03_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_istan2_03a_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_istan2_04a_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_istan2_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_istan2b_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_istan3_hromada2_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_istan3_hromada_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_istan3_pumpa_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_istan3_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_istan4_big_inverse_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_istan4_big_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_istan4_detaily1_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_istan4_inverse_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_istan4_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_m2_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_mesto2_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_mesto2L_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_mesto3_istan_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_mesto3_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_mesto_in_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_mesto_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_olez_istan1_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_olez_istan2_maly2_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_olez_istan2_maly_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_olez_istan2_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_rasovna_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dumruina_mini_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dumruina_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Garaz_mala_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Garaz_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Hangar_2_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Hlidac_budka_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Hospital_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Hospoda_mesto_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Hotel_riviera1_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Hotel_riviera2_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Hotel_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_y_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HruzDum_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Hut_old02_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Hut01_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Hut02_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Hut03_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Hut04_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Hut06_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Kasarna_brana_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Kasarna_prujezd_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Kasarna_rohova_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Kasarna_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Komin_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Kostel2_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Kostel3_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Kostel_mexico_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Kostel_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Kostelik_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Letistni_hala_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Majak_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Majak_v_celku_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_OrlHot_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Panelak2_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Panelak3_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Panelak_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Repair_center_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ryb_domek_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_domek01_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_domek02_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_domek03_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_domek04_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_domek05_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_domek_hospoda_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_domek_kovarna_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_domek_podhradi_1_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_domek_rosa_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_domek_ruina_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_domek_sedy_BEZ_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_domek_sedy_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_domek_vilka_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_domek_zluty_BEZ_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_domek_zluty_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_Dum_podloubi03klaster_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_Dum_podloubi03rovny_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_Hasic_zbroj_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_stodola2_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_stodola3_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_stodola_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_zluty_statek_in_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_zluty_statek_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Skola_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_SS_hangar_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_SS_hangarD_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Stanice_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Statek_hl_bud_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Statek_kulna_old_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Statek_kulna_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Stodola_old_open_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Stodola_old_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Stodola_open_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Strazni_vez_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Tovarna1_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Tovarna2_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Trafostanica_mala_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Trafostanica_velka_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Zalchata_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Leseni2x_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Leseni4x_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Statek_brana_open_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Statek_brana_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Brana02_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Brana02nodoor_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_zboreny_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_zboreny_total_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Kostel_trosky_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Posed_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Vez_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Telek1_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Watertower1_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Hlaska_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Nasypka_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Afbarabizna: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_AfDum_mesto2: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_AfDum_mesto2L: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_AfDum_mesto3: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Afhospoda_mesto: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ammostore2: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Majak: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Majak_podesta: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Molo_beton: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Molo_drevo: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Molo_drevo_bs: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Molo_drevo_end: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Molo_krychle: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Molo_krychle2: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Zastavka_jih: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Zastavka_sever: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Pletivo_dira: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Plot_zed_drevo1_branka: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Plot_istan1b_hole: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Plot_istan1_rovny_gate: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Plot_Ohrada_Pruchozi: Wall_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Plot_zboreny: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Stanek_1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Stanek_1B: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Stanek_1C: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Zed_dira: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Zed_dira_civil: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Zed_dira_desert: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Bouda1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Bouda3: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Brana02: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Brana02nodoor: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Budova1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Budova4_in: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Budova5: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Cihlovej_dum: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Cihlovej_Dum_in: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Cihlovej_Dum_mini: Land_Cihlovej_Dum_in
	{
		scopeCurator = 1;//2;
	};
	class Land_Deutshe: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Deutshe_mini: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Domek_rosa: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dulni_bs: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_istan2: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_istan2b: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_istan2_01: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_istan2_02: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_istan2_03: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_istan2_03a: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_istan2_04a: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_istan3: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_istan3_hromada: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_istan3_hromada2: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_istan3_pumpa: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_istan4: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_istan4_big: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_istan4_big_inverse: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_istan4_detaily1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_istan4_inverse: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_m2: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_mesto: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_mesto2l: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_mesto3: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_mesto3_istan: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_olez_istan2_maly2: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_zboreny: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_zboreny_total: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum01: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum02: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dumruina: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dumruina_mini: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Hospital: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Hospoda_mesto: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_y: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Hut_old02: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Kasarna_rohova: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Kasarna_brana: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Kasarna: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Kasarna_prujezd: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Kostel_trosky: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Kostelik: House
	{
		scopeCurator = 1;//2;
	};
	class Land_Kulna: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_OrlHot: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Podesta_1_cube: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Repair_center: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ryb_domek: House
	{
		scopeCurator = 1;//2;
	};
	class Land_Skola: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Stanice: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Statek_brana: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Statek_brana_open: Land_Statek_brana
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Psi_bouda: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Statek_hl_bud: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Statek_kulna: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Stodola_old: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Stodola_old_open: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Stodola_open: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Strazni_vez: House
	{
		scopeCurator = 1;//2;
	};
	class Land_ZalChata: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Army_hut_storrage: House
	{
		scopeCurator = 1;//2;
	};
	class Land_Army_hut_int: House
	{
		scopeCurator = 1;//2;
	};
	class Land_Army_hut2_int: House
	{
		scopeCurator = 1;//2;
	};
	class Land_Army_hut3_long: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Army_hut3_long_int: Land_Army_hut3_long
	{
		scopeCurator = 1;//2;
	};
	class Land_Hut01: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Hut02: Land_Hut01
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Hut03: Land_Hut01
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Hut04: Land_Hut01
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Hut06: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_domek01: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_domek02: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_domek03: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_domek04: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_domek05: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_domek_hospoda: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_domek_kovarna: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_domek_sedy_bez: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_domek_podhradi_1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_domek_rosa: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_domek_ruina: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_domek_vilka: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_domek_zluty_bez: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_Dum_podloubi03klaster: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_Dum_podloubi03rovny: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_hasic_zbroj: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_stodola: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_stodola2: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_stodola3: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_zluty_statek: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ruiny_3_dvere: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ruiny_3_prasklina: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ruiny_3_roh: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ruiny_3_stenazbor: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ruiny_3_stena: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ruiny_obvod_3: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Pumpa: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Helfenburk: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Bouda2_vnitrek: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Aut_zast: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Garaz_s_tankem: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Hruzdum: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_rasovna: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Hlaska: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Vez: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Kostel: Church
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Kostel2: Church
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Kostel3: Church
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Kostel_mexico: Church
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Panelak: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Letistni_hala: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_KBud: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Nasypka: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Tovarna1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Tovarna2: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Hlidac_budka: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_Domek_sedy: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Posed: House
	{
		scopeCurator = 1;//2;
	};
	class Land_Leseni4x: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Leseni2x: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_domek_zluty: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Vysilac_FM: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Watertower1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Telek1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Radar: Land_Vysilac_FM2
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sara_zluty_statek_in: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Panelak2: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Panelak3: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Hotel: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_mesto2: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Nabrezi: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Nabrezi_najezd: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Trafostanica_velka: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Trafostanica_mala: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Komin: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Hotel_riviera1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Hotel_riviera2: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Trafostanica_velka_draty: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Hangar_2: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Plot_green_branka: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Plot_green_vrata: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_olezlina_closed: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_olezlina: Land_Dum_olezlina_closed
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Benzina_schnell: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Garaz_mala: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Garaz: House
	{
		scopeCurator = 1;//2;
	};
	class Land_Garaz_long_open: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Budova2: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Bozi_muka: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Budova3: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_mesto_in_bare;
	class Land_Dum_mesto_in: Land_Dum_mesto_in_bare
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ss_hangard: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ss_hangar: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_olez_istan2_maly: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_olez_istan2: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_olez_istan1_closed;
	class Land_Dum_olez_istan1: Land_Dum_olez_istan1_closed
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Shop1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_mesto_in_open_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Benzina_schnell_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_olezlina_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_olez_istan2_open_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_olez_istan2_open2_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_olez_istan1_open_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dum_olez_istan1_open2_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Garaz_long_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_bouda_plech_open;
	class Land_Bouda_plech: Land_bouda_plech_open
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_zavora: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Lampa_sidl: Lamps_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Stoplight01: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class zed: CUP_Wall
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class zed_civil: CUP_Wall
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class zed_desert: CUP_Wall
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class zed_dira: CUP_Wall
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class zed_civil_dira: CUP_Wall
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class zed_dira_desert: CUP_Wall
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class zed_kamenna: CUP_Wall
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class zed_kamenna_desert: CUP_Wall
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class zed_podplaz: CUP_Wall
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class zed_podplaz_civil: CUP_Wall
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class zed_podplaz_desert: CUP_Wall
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class zed2: CUP_Wall
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class zed2_civil: CUP_Wall
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class zed3: CUP_Wall
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class zidka_branka: CUP_Wall
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class zidka01: CUP_Wall
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class zidka02: CUP_Wall
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class zidka03: CUP_Wall
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class zidka04: CUP_Wall
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_BuildingWIP: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_BuildingWIP_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_CraneCon: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_MunicipalOffice: NonStrategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_MunicipalOffice_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Barn_W_01: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Barn_W_01_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Barn_W_02_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_Bastion: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_Gate: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_Wall1_20: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_Wall1_20_Turn: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_Wall1_Corner: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_Wall1_End: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_Wall1_End_2: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_Wall2_30: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_Wall2_Corner: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_Wall2_End: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_Wall2_End_2: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_WallS_10: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_WallS_5_D: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_WallS_End: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_Stairs_A: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_Wall1_Corner_2: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_Wall2_Corner_2: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_Bergfrit: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_Bergfrit_dam: Land_A_Castle_Bergfrit
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_Donjon: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_Donjon_dam: Land_A_Castle_Donjon
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_Bergfrit_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_Donjon_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_Gate_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_Stairs_A_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_Wall1_20_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_Wall1_20_turn_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_Wall1_Corner_2_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_Wall1_Corner_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_Wall1_End_2_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_Wall1_End_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_Wall2_30_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_Wall2_Corner_2_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_Wall2_Corner_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_Wall2_End_2_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_Wall2_End_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_WallS_5_D_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_WallS_10_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_WallS_End_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Castle_Bastion_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_FuelStation_Feed: Strategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_FuelStation_Build: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_FuelStation_Shed: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Hospital: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Hospital_dam: Land_A_Hospital
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Hospital_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Office01: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Office01_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Office02: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Office02_dam: Land_A_Office02
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Office02_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_a_stationhouse: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_a_stationhouse_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Church_02: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Church_02_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Church_03: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Church_03_dam: Land_Church_03
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Church_03_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Church_05R: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseB_Tenement: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseB_Tenement_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseV2_01A: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseV2_01A_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseV2_01A_dam: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseV2_01B_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseV2_01B_dam: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseV2_02_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseV2_03_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseV2_03_dam: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseV2_03B_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseV2_03B_dam: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseV2_04_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseV2_05: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseV2_05_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseV_1I1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseV_1I1_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseV_1I1_dam: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseV_1I2: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseV_1I2_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseV_1I3: Land_HouseV_1I2
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseV_1I3_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseV_1I4: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseV_1I4_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseV_1L1_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseV_1L2_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseV_1T_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseV_2I_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseV_2L_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseV_2L_dam: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseV_2T1_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseV_2T2_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseV_3I1_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseV_3I2_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseV_3I3_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseV_3I4_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Stack_Big: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Stack_Big_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_IndPipe1_stair: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Quarry: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Quarry_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_SawMill: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_SawMillPen: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Mil_ControlTower: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Mil_House: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Mil_ControlTower_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Mil_House_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Mil_Barracks_i: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Mil_Barracks: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Mil_Barracks_L: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Mil_Guardhouse: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Mil_Guardhouse_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_PowLines_ConcL: Lamps_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_NAV_Lighthouse: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_NAV_Lighthouse_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Nav_Boathouse: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Nav_Boathouse_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Nav_Boathouse_PierL: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Nav_Boathouse_Pier_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Rail_Semafor: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Rail_Zavora: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_wagon_box: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_wagon_flat: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_wagon_tanker: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_loco_742_blue: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_rail_station_big: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_rail_station_big_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_sign_elektrozavodsk: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_ruin_corner_1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Shed_W4: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Shed_W4_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Shed_Ind02: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Shed_Ind02_dam: Land_Shed_Ind02
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Shed_Ind02_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Wall_CBrk_5_D: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Wall_CGry_5_D: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Wall_Gate_Ind1_L: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Wall_Gate_Kolchoz: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Panelak1_Grey: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Panelak2_Grey: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Panelak3_Grey: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class CUP_ammobednaX: NonStrategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class CUP_armchair: Base_CUP_Furniture
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class CUP_basin_a: Base_CUP_Furniture
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Kamenolom_budova: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_pila: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Vez_svetla: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Vysilac_budova: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Vysilac_vez: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Vysilac_chodba: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_vodni_vez: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Crane_02a: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Crane_02b: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_GeneralStore_01: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Pub_01: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_statue01: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_statue02: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_statue01_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Barn_Metal: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Barn_Metal_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Tec: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Church_01: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Church_01_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Farm_Cowshed_a: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Farm_Cowshed_b: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Farm_Cowshed_c: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Farm_Cowshed_a_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Farm_Cowshed_b_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Farm_Cowshed_c_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Farm_WTower: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Farm_WTower_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseBlock_A1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseBlock_A1_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseBlock_A1_1_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseBlock_A2_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseBlock_A2_1_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseBlock_A3_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseBlock_B1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseBlock_B1_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseBlock_B2_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseBlock_B3_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseBlock_B4_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseBlock_B5_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseBlock_B6_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseBlock_C1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseBlock_C1_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseBlock_C2_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseBlock_C3_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseBlock_C4_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseBlock_C5_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseBlock_D1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseBlock_D1_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HouseBlock_D2_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_R_A_GeneralStore_01a: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_D_Pec_Vez1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_D_Pec_Vez2: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_D_Vez_Mlyn: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_D_VSilo_Pec: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Vez_Pec: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Vez_Silo: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_D_Mlyn_Vys: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Expedice_1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Expedice_2: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Expedice_3: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Expedice_1_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_MalyKomin: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_MalyKomin_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Mlyn_01: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Mlyn_02: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Mlyn_03: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Mlyn_04: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Mlyn_D1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Mlyn_D2: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Mlyn_01_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Mlyn_02_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Mlyn_03_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Mlyn_04_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Pec_01: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Pec_02: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Pec_03: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Pec_01_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Pec_02_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Pec_03_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_ind_silomale: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_SiloMale_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_SiloVelke_01: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_SiloVelke_02: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_SiloVelke_most: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Silovelke_01_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Silovelke_02_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Vysypka: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Vysypka_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Garage01: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Garage01_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_IndPipe1_stair_todo_delete: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_IndPipe2_big_18: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_IndPipe2_big_9: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_IndPipe2_big_18ladder: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_IndPipe2_big_support: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_IndPipe2_bigL_R: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_IndPipe2_bigL_L: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_IndPipe2_bigBuild1_R: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_IndPipe2_bigBuild1_L: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_IndPipe2_bigBuild2_L: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_IndPipe2_bigBuild2_R: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_IndPipe2_big_ground1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_IndPipe2_big_ground2: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_IndPipe2_T_R: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_IndPipe2_T_L: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_IndPipe2_Small_9: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_IndPipe2_Small_ground1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_IndPipe2_Small_ground2: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_IndPipe2_SmallBuild1_L: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_IndPipe2_SmallBuild1_R: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_IndPipe2_SmallBuild2_R: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_IndPipe2_SmallBuild2_L: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_IndPipe2_SmallL_L: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_IndPipe2_SmallL_R: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Shed_01_main: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Shed_01_end: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Shed_01_main_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Shed_01_end_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Shed_02_main: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Shed_02_end: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Shed_02_main_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_TankBig: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_TankSmall: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_TankBig_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_TankSmall_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Workshop01_01: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Workshop01_02: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Workshop01_03: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Workshop01_04: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Workshop01_box: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Workshop01_L: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Workshop01_01_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Workshop01_02_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Workshop01_03_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Workshop01_04_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Workshop01_box_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Workshop01_L_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_Cargo1Ao: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_Cargo1Bo: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Misc_Cargo1Bo_military: Land_Misc_Cargo1Bo
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_Cargo1B: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_Cargo1C: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_Cargo1D: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_Cargo1E: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_Cargo1F: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_Cargo1G: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_Cargo2B: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_Cargo2C: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_Cargo2D: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_Cargo2E: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_PowerStation: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_PowerStation_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_WaterStation: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_WaterStation_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Rail_House_01: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Rail_House_01_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Shed_W01: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Shed_W02: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Shed_W03: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Shed_M01: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Shed_M02: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Shed_M03: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Shed_M01_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Shed_M02_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Shed_M03_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Shed_W01_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Shed_W02_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Shed_W03_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Shed_wooden: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Shed_wooden_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_HBarrier_large: NonStrategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_fort_bagfence_corner: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_fort_bagfence_long: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_fort_bagfence_round: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_fort_artillery_nest: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_fort_rampart: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Fort_RazorWire: NonStrategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Fort_Crate_wood: NonStrategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class WarfareBCamp: Fortress1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Fort_CAmp: WarfareBCamp
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_fortified_nest_big: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_fortified_nest_small: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Fort_Nest: Land_fortified_nest_small
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Fort_Watchtower: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Fort_Barracks_USMC: WarfareBBaseStructure
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Fort_Nest_M240: WarfareBMGNest_M240_base
	{
		scopeCurator = 1;//2;
	};
	class Hedgehog: Strategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Hhedgehog_concrete: Strategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Fort_EnvelopeSmall: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Fort_EnvelopeBig: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Fort_Barricade: Strategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class FoldTable: Thing
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class FoldChair: Thing
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Desk: Thing
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class SmallTable: Thing
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_CncBlock: NonStrategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_CncBlock_Stripes: NonStrategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_CncBlock_D: NonStrategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Toilet: NonStrategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Fire: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Misc_TyreHeap: Land_Fire
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Sr_border: NonStrategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_kolotoc: Strategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Barrel_empty: Thing
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Barrel_water: Thing
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Barrel_sand: Thing
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Wooden_barrel: BarrelBase
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Wooden_barrels: Wooden_barrel
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class FlagCarrierUSA: FlagCarrier
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_deerstand: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_GContainer_Big: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_Scaffolding: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_IlluminantTower: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Timbers: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_BoardsPack1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Fence_Ind: Fence
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Garbage_container: Strategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Garbage_can: Strategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Notice_board: Misc_thing
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Axe_woodblock: Misc_thing
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Haystack_small: Misc_thing
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_seno_balik: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Misc_concrete_High: Misc_thing
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Fuel_can: Misc_thing
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class PowerGenerator: Misc_thing
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Satelit: Misc_thing
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Pneu: Thing
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Misc_palletsfoiled_heap: Misc_thing_NoInteractive
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Misc_palletsfoiled: Misc_thing_NoInteractive
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Gunrack1: NonStrategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Sign_Danger: Thing
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Sign_Stop: Sign_Checkpoint
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Sign_1L_Border: Thing
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_arrows_desk_L: Thing
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_arrows_desk_R: Thing
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_arrows_yellow_R: Thing
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_arrows_yellow_L: Thing
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_coneLight: Thing
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_RedWhiteBarrier: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class FlagCarrierChecked: FlagCarrierCore
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class FlagCarrierSmall: NonStrategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Sign_circle: Thing
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class SkeetMachine: Small_items
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class SkeetDisk: Small_items
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class MetalBucket: Small_items
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class FloorMop: Small_items
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Baseball: Small_items
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Notebook: Small_items
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class SmallTV: Small_items
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Suitcase: Small_items
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Can_small: Small_items
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Radio: Small_items
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class SatPhone: Small_items
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class EvPhoto: Small_items
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Explosive: Small_items_NoInteractive
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Loudspeaker: Small_items_NoInteractive
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Misc_Wall_lamp: Small_items_NoInteractive
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Misc_Videoprojektor: Small_items_NoInteractive
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Misc_Videoprojektor_platno: Small_items_NoInteractive
	{
		scopeCurator = 1;//2;
	};
	class TargetPopUpTarget: TargetBase
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TargetEpopup: TargetBase
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TargetE: TargetBase
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TargetGrenade: TargetGrenadBase
	{
		scopeCurator = 1;//2;
	};
	class Land_A_tent: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_tent_east: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_GuardShed: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Antenna: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_CamoNet_NATO: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_CamoNet_EAST: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class 76n6ClamShell: Strategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class PowGen_Big: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_PowGen_Big_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Barrack2: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Barrack2: Land_Barrack2
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Misc_cargo_cont_small: Military_Item_NoInteractive
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Misc_cargo_cont_small2: Military_Item_NoInteractive
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Misc_cargo_cont_tiny: Military_Item_NoInteractive
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Misc_cargo_cont_net1: Military_Item_NoInteractive
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Misc_cargo_cont_net2: Military_Item_NoInteractive
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Misc_cargo_cont_net3: Military_Item_NoInteractive
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Misc_Backpackheap: Military_Item_NoInteractive
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Climbing_Obstacle: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_WoodenRamp: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_ConcreteRamp: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_ConcreteBlock: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dirthump01: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dirthump02: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dirthump03: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Shooting_range: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_BoatSmall_1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class HMMWVWreck: Wreck_Base
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class BMP2Wreck: Wreck_Base
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class UralWreck: Wreck_Base
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class datsun01Wreck: Wreck_Base
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class datsun02Wreck: Wreck_Base
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class hiluxWreck: Wreck_Base
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Mi8Wreck: Wreck_Base
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class T72Wreck: Wreck_Base
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class T72WreckTurret: Wreck_Base
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class BRDMWreck: Wreck_Base
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class UAZWreck: Wreck_Base
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class SKODAWreck: Wreck_Base
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class LADAWreck: Wreck_Base
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Body: Grave
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Church_tomb_1: Grave
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Church_tomb_2: Grave
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Church_tomb_3: Grave
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Mass_grave: Grave
	{
		scopeCurator = 1;//2;
	};
	class WarfareBDepot: WarfareBBaseStructure
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class WarfareBAirport: Land_Ss_hangar
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Base_WarfareBBarracks: WarfareBBaseStructure
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class USMC_WarfareBBarracks: Base_WarfareBBarracks
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class RU_WarfareBBarracks: Base_WarfareBBarracks
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class CDF_WarfareBBarracks: Base_WarfareBBarracks
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Ins_WarfareBBarracks: Base_WarfareBBarracks
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Gue_WarfareBBarracks: Base_WarfareBBarracks
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Base_WarfareBContructionSite: Strategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class USMC_WarfareBContructionSite: Base_WarfareBContructionSite
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class RU_WarfareBContructionSite: Base_WarfareBContructionSite
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class CDF_WarfareBContructionSite: Base_WarfareBContructionSite
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Ins_WarfareBContructionSite: Base_WarfareBContructionSite
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Gue_WarfareBContructionSite: Base_WarfareBContructionSite
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class USMC_WarfareBLightFactory: Base_WarfareBLightFactory
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class RU_WarfareBLightFactory: Base_WarfareBLightFactory
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class CDF_WarfareBLightFactory: Base_WarfareBLightFactory
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Ins_WarfareBLightFactory: Base_WarfareBLightFactory
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Gue_WarfareBLightFactory: Base_WarfareBLightFactory
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class USMC_WarfareBHeavyFactory: Base_WarfareBHeavyFactory
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class RU_WarfareBHeavyFactory: Base_WarfareBHeavyFactory
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class CDF_WarfareBHeavyFactory: Base_WarfareBHeavyFactory
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Ins_WarfareBHeavyFactory: Base_WarfareBHeavyFactory
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Gue_WarfareBHeavyFactory: Base_WarfareBHeavyFactory
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class USMC_WarfareBAircraftFactory: Base_WarfareBAircraftFactory
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class RU_WarfareBAircraftFactory: Base_WarfareBAircraftFactory
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class CDF_WarfareBAircraftFactory: Base_WarfareBAircraftFactory
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class WarfareBAircraftFactory_CDF: Base_WarfareBAircraftFactory
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class INS_WarfareBAircraftFactory: Base_WarfareBAircraftFactory
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class WarfareBAircraftFactory_Ins: Base_WarfareBAircraftFactory
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class GUE_WarfareBAircraftFactory: Base_WarfareBAircraftFactory
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class WarfareBAircraftFactory_Gue: Base_WarfareBAircraftFactory
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class USMC_WarfareBFieldhHospital: BASE_WarfareBFieldhHospital
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class RU_WarfareBFieldhHospital: BASE_WarfareBFieldhHospital
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class CDF_WarfareBFieldhHospital: BASE_WarfareBFieldhHospital
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class INS_WarfareBFieldhHospital: BASE_WarfareBFieldhHospital
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class GUE_WarfareBFieldhHospital: BASE_WarfareBFieldhHospital
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class USMC_WarfareBAntiAirRadar: BASE_WarfareBAntiAirRadar
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class RU_WarfareBAntiAirRadar: BASE_WarfareBAntiAirRadar
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class CDF_WarfareBAntiAirRadar: BASE_WarfareBAntiAirRadar
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class INS_WarfareBAntiAirRadar: BASE_WarfareBAntiAirRadar
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class GUE_WarfareBAntiAirRadar: BASE_WarfareBAntiAirRadar
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class USMC_WarfareBArtilleryRadar: BASE_WarfareBArtilleryRadar
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class RU_WarfareBArtilleryRadar: BASE_WarfareBArtilleryRadar
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class CDF_WarfareBArtilleryRadar: BASE_WarfareBArtilleryRadar
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Ins_WarfareBArtilleryRadar: BASE_WarfareBArtilleryRadar
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Gue_WarfareBArtilleryRadar: BASE_WarfareBArtilleryRadar
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class USMC_WarfareBUAVterminal: Base_WarfareBUAVterminal
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class RU_WarfareBUAVterminal: Base_WarfareBUAVterminal
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class CDF_WarfareBUAVterminal: Base_WarfareBUAVterminal
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class INS_WarfareBUAVterminal: Base_WarfareBUAVterminal
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class GUE_WarfareBUAVterminal: Base_WarfareBUAVterminal
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class USMC_WarfareBVehicleServicePoint: Base_WarfareBVehicleServicePoint
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class RU_WarfareBVehicleServicePoint: Base_WarfareBVehicleServicePoint
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class CDF_WarfareBVehicleServicePoint: Base_WarfareBVehicleServicePoint
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class INS_WarfareBVehicleServicePoint: Base_WarfareBVehicleServicePoint
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class GUE_WarfareBVehicleServicePoint: Base_WarfareBVehicleServicePoint
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Base_WarfareBBarrier5x: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class BRDM2_HQ_Gue_unfolded: Warfare_HQ_base_unfolded
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class BTR90_HQ_unfolded: Warfare_HQ_base_unfolded
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class LAV25_HQ_unfolded: Warfare_HQ_base_unfolded
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class BMP2_HQ_INS_unfolded: Warfare_HQ_base_unfolded
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class BMP2_HQ_CDF_unfolded: Warfare_HQ_base_unfolded
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class WarfareBunkerSign: NonStrategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class C130J_wreck_EP1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Wreck_C130J_EP1_ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class UH60_wreck_EP1: StaticWeapon
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class GraveCross2_EP1: GraveCross2
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class GraveCrossHelmet_EP1: GraveCrossHelmet
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_fort_artillery_nest_EP1: Land_fort_artillery_nest
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_fort_rampart_EP1: Land_fort_rampart
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_fortified_nest_big_EP1: Land_fortified_nest_big
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_fortified_nest_small_EP1: Land_fortified_nest_small
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Fort_Watchtower_EP1: Land_Fort_Watchtower
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Hedgehog_EP1: Hedgehog
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Fort_EnvelopeSmall_EP1: Fort_EnvelopeSmall
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Fort_EnvelopeBig_EP1: Fort_EnvelopeBig
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Fort_Barricade_EP1: Fort_Barricade
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_CamoNet_NATO_EP1: Land_CamoNet_NATO
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_CamoNetVar_NATO_EP1: Land_CamoNetVar_NATO
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_CamoNetB_NATO_EP1: Land_CamoNetB_NATO
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_CamoNet_EAST_EP1: Land_CamoNet_EAST
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_CamoNetVar_EAST_EP1: Land_CamoNetVar_EAST
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_CamoNetB_EAST_EP1: Land_CamoNetB_EAST
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class 76n6ClamShell_EP1: 76n6ClamShell
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class PowGen_Big_EP1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Barrack2_EP1: Barrack2
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class MASH_EP1: MASH
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Misc_cargo_cont_small_EP1: Misc_cargo_cont_small
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class AmmoCrate_NoInteractive_: AmmoCrate_NoInteractive_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class AmmoCrates_NoInteractive_Small: AmmoCrate_NoInteractive_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class AmmoCrates_NoInteractive_Medium: AmmoCrate_NoInteractive_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class AmmoCrates_NoInteractive_Large: AmmoCrate_NoInteractive_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Camp_EP1: Camp
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class CampEast_EP1: CampEast
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class ACamp_EP1: ACamp
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class C130J_static_EP1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Misc_TyreHeapEP1: Misc_TyreHeap
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_ladderEP1: Land_ladder
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_ladder_half_EP1: Land_ladder_half
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_radar_EP1: Land_Radar
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Notice_board_EP1: Notice_board
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class PowerGenerator_EP1: PowerGenerator
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Sign_MP_blu_EP1: Sign_MP_blu
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Sign_MP_op_EP1: Sign_MP_op
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Sign_MP_ind_EP1: Sign_MP_ind
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Sign_1L_Firstaid_EP1: Sign_1L_Firstaid
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Sign_1L_Noentry_EP1: Sign_1L_Noentry
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Sign_Checkpoint_US_EP1: Sign_Checkpoint
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Sign_Checkpoint_TK_EP1: Sign_Checkpoint
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dirthump01_EP1: Land_Dirthump01
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dirthump02_EP1: Land_Dirthump02
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Dirthump03_EP1: Land_Dirthump03
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TargetE_EP1: TargetE
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TargetFakeTank_Lockable_EP1: TargetFakeTank
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class GunrackUS_EP1: Gunrack1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Map_for_briefing_EP1: Notice_board
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Misc_Backpackheap_EP1: Misc_Backpackheap
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class FlagPole_EP1: FlagCarrier
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class FlagCarrierUNO_EP1: FlagCarrier
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Microphone1_ep1: Small_items_NoInteractive
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class ClutterCutter_EP1: Thing
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_Cargo1Ao_EP1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_Cargo1E_EP1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_Cargo1A_EP1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_Cargo1B_EP1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_Cargo1C_EP1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_Cargo1D_EP1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_Cargo2A_EP1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_Cargo2B_EP1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_Cargo2C_EP1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_Cargo2D_EP1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_CargoMarket1a_EP1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_Cargo1A_ruins_EP1: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Sign_circle_EP1: Helper_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Sign_sphere10cm_EP1: Helper_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Sign_sphere25cm_EP1: Helper_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Sign_sphere100cm_EP1: Helper_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Sign_arrow_down_EP1: Helper_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Sign_arrow_down_large_EP1: Helper_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class HMMWV_Ghost_EP1: Helper_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Training_target_EP1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Hlidac_Budka_EP1: Land_Hlidac_budka
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_TankSmall2_EP1: Land_Ind_TankSmall2
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_Cargo2E_EP1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Bleacher_EP1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class WaterBasin_conc_EP1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Dirtmount_EP1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Infostand_1_EP1: Thing
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class ProtectionZone_Ep1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class US_WarfareBBarracks_Base_EP1: Base_WarfareBBarracks
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_WarfareBBarracks_Base_EP1: Base_WarfareBBarracks
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_GUE_WarfareBBarracks_Base_EP1: Base_WarfareBBarracks
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class US_WarfareBBarracks_EP1: US_WarfareBBarracks_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_WarfareBBarracks_EP1: TK_WarfareBBarracks_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_GUE_WarfareBBarracks_EP1: TK_GUE_WarfareBBarracks_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class US_WarfareBContructionSite_Base_EP1: Base_WarfareBContructionSite
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_WarfareBContructionSite_Base_EP1: Base_WarfareBContructionSite
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_GUE_WarfareBContructionSite_Base_EP1: Base_WarfareBContructionSite
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class US_WarfareBContructionSite_EP1: US_WarfareBContructionSite_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class US_WarfareBContructionSite1_EP1: US_WarfareBContructionSite1_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_WarfareBContructionSite_EP1: TK_WarfareBContructionSite_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_WarfareBContructionSite1_EP1: TK_WarfareBContructionSite1_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_GUE_WarfareBContructionSite_EP1: TK_GUE_WarfareBContructionSite_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_GUE_WarfareBContructionSite1_EP1: TK_GUE_WarfareBContructionSite1_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class US_WarfareBLightFactory_base_EP1: Base_WarfareBLightFactory
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_WarfareBLightFactory_base_EP1: Base_WarfareBLightFactory
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_GUE_WarfareBLightFactory_base_EP1: Base_WarfareBLightFactory
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class US_WarfareBLightFactory_EP1: US_WarfareBLightFactory_base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_WarfareBLightFactory_EP1: TK_WarfareBLightFactory_base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_GUE_WarfareBLightFactory_EP1: TK_GUE_WarfareBLightFactory_base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class US_WarfareBHeavyFactory_Base_EP1: Base_WarfareBHeavyFactory
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_WarfareBHeavyFactory_Base_EP1: Base_WarfareBHeavyFactory
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_GUE_WarfareBHeavyFactory_Base_EP1: Base_WarfareBHeavyFactory
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class US_WarfareBHeavyFactory_EP1: US_WarfareBHeavyFactory_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_WarfareBHeavyFactory_EP1: TK_WarfareBHeavyFactory_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_GUE_WarfareBHeavyFactory_EP1: TK_GUE_WarfareBHeavyFactory_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class US_WarfareBAircraftFactory_Base_EP1: Base_WarfareBAircraftFactory
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_WarfareBAircraftFactory_Base_EP1: Base_WarfareBAircraftFactory
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_GUE_WarfareBAircraftFactory_Base_EP1: Base_WarfareBAircraftFactory
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class US_WarfareBAircraftFactory_EP1: US_WarfareBAircraftFactory_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_WarfareBAircraftFactory_EP1: TK_WarfareBAircraftFactory_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_GUE_WarfareBAircraftFactory_EP1: TK_GUE_WarfareBAircraftFactory_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class US_WarfareBFieldhHospital_Base_EP1: BASE_WarfareBFieldhHospital
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_WarfareBFieldhHospital_Base_EP1: BASE_WarfareBFieldhHospital
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_GUE_WarfareBFieldhHospital_Base_EP1: BASE_WarfareBFieldhHospital
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class US_WarfareBFieldhHospital_EP1: US_WarfareBFieldhHospital_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_WarfareBFieldhHospital_EP1: TK_WarfareBFieldhHospital_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_GUE_WarfareBFieldhHospital_EP1: TK_GUE_WarfareBFieldhHospital_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class US_WarfareBAntiAirRadar_Base_EP1: BASE_WarfareBAntiAirRadar
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_WarfareBAntiAirRadar_Base_EP1: BASE_WarfareBAntiAirRadar
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_GUE_WarfareBAntiAirRadar_Base_EP1: BASE_WarfareBAntiAirRadar
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class US_WarfareBAntiAirRadar_EP1: US_WarfareBAntiAirRadar_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_WarfareBAntiAirRadar_EP1: TK_WarfareBAntiAirRadar_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_GUE_WarfareBAntiAirRadar_EP1: TK_GUE_WarfareBAntiAirRadar_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class US_WarfareBArtilleryRadar_Base_EP1: BASE_WarfareBArtilleryRadar
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_WarfareBArtilleryRadar_Base_EP1: BASE_WarfareBArtilleryRadar
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_GUE_WarfareBArtilleryRadar_Base_EP1: BASE_WarfareBArtilleryRadar
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class US_WarfareBArtilleryRadar_EP1: US_WarfareBArtilleryRadar_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_WarfareBArtilleryRadar_EP1: TK_WarfareBArtilleryRadar_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_GUE_WarfareBArtilleryRadar_EP1: TK_GUE_WarfareBArtilleryRadar_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class US_WarfareBUAVterminal_Base_EP1: Base_WarfareBUAVterminal
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_WarfareBUAVterminal_Base_EP1: Base_WarfareBUAVterminal
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_GUE_WarfareBUAVterminal_Base_EP1: Base_WarfareBUAVterminal
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class US_WarfareBUAVterminal_EP1: US_WarfareBUAVterminal_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_WarfareBUAVterminal_EP1: TK_WarfareBUAVterminal_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_GUE_WarfareBUAVterminal_EP1: TK_GUE_WarfareBUAVterminal_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class US_WarfareBVehicleServicePoint_Base_EP1: Base_WarfareBVehicleServicePoint
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_WarfareBVehicleServicePoint_Base_EP1: Base_WarfareBVehicleServicePoint
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_GUE_WarfareBVehicleServicePoint_Base_EP1: Base_WarfareBVehicleServicePoint
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class US_WarfareBVehicleServicePoint_EP1: US_WarfareBVehicleServicePoint_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_WarfareBVehicleServicePoint_EP1: TK_WarfareBVehicleServicePoint_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_GUE_WarfareBVehicleServicePoint_EP1: TK_GUE_WarfareBVehicleServicePoint_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class US_WarfareBBarrier5x_EP1: Base_WarfareBBarrier5x
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class US_WarfareBBarrier10x_EP1: Base_WarfareBBarrier10x
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class US_WarfareBBarrier10xTall_EP1: Base_WarfareBBarrier10xTall
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_WarfareBBarrier5x_EP1: Base_WarfareBBarrier5x
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_WarfareBBarrier10x_EP1: Base_WarfareBBarrier10x
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_WarfareBBarrier10xTall_EP1: Base_WarfareBBarrier10xTall
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_GUE_WarfareBBarrier5x_EP1: Base_WarfareBBarrier5x
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_GUE_WarfareBBarrier10x_EP1: Base_WarfareBBarrier10x
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TK_GUE_WarfareBBarrier10xTall_EP1: Base_WarfareBBarrier10xTall
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class BRDM2_HQ_TK_GUE_unfolded_Base_EP1: Warfare_HQ_base_unfolded
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class BRDM2_HQ_TK_GUE_unfolded_EP1: BRDM2_HQ_TK_GUE_unfolded_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class M1130_HQ_unfolded_Base_EP1: Warfare_HQ_base_unfolded
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class M1130_HQ_unfolded_EP1: M1130_HQ_unfolded_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class BMP2_HQ_TK_unfolded_Base_EP1: Warfare_HQ_base_unfolded
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class BMP2_HQ_TK_unfolded_EP1: BMP2_HQ_TK_unfolded_Base_EP1
	{
		scopeCurator = 1;//2;
	};
	class FlagCarrierArmex_EP1: FlagCarrier
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Concrete_Wall_EP1: NonStrategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class EntranceGate_EP1: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Info_Board_EP1: NonStrategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Laptop_EP1: Small_items
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Loudspeakers_EP1: NonStrategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Plastic_Pole_EP1: Small_items_NoInteractive
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Red_Light_EP1: NonStrategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Red_Light_Blinking_EP1: Red_Light_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Sign_Armex_EP1: NonStrategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Sign_Direction_EP1: NonStrategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_TVTower_Base: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_TVTower_Mid: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_TVTower_Top: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_nav_pier_m_2: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_rails_bridge_40: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_BuildingWIP_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_BuildingWIP_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_CityGate1_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_CityGate1_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Minaret_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Minaret_dam_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Minaret_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Minaret_Porto_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Minaret_porto_dam_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Minaret_Porto_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Mosque_big_hq_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Mosque_big_addon_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Mosque_big_minaret_1_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Mosque_big_minaret_1_dam_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Mosque_big_minaret_2_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Mosque_big_minaret_2_dam_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Mosque_big_wall_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Mosque_big_wall_corner_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Mosque_big_wall_gate_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Mosque_big_wall_gate_dam_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Mosque_big_hq_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Mosque_big_addon_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Mosque_big_minaret_1_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Mosque_big_minaret_2_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Mosque_big_wall_gate_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Mosque_big_wall_corner_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Mosque_big_wall_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Mosque_small_1_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Mosque_small_1_dam_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Mosque_small_2_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Mosque_small_2_dam_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Mosque_small_1_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Mosque_small_2_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Office01_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Office01_ruins_ep1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Stationhouse_ep1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_a_stationhouse_ruins_ep1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Statue_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Villa_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Villa_dam_EP1: Land_A_Villa_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_A_Villa_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_C_1_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_C_1_dam_EP1: Land_House_C_1_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_C_1_v2_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_C_1_v2_dam_EP1: Land_House_C_1_v2_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_C_2_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_C_2_DAM_EP1: Land_House_C_2_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_C_3_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_C_3_dam_EP1: Land_House_C_3_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_C_4_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_C_4_dam_EP1: Land_House_C_4_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_C_5_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_C_5_dam_EP1: Land_House_C_5_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_C_5_V1_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_C_5_V1_dam_EP1: Land_House_C_5_V1_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_C_5_V2_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_C_5_V2_dam_EP1: Land_House_C_5_V2_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_C_5_V3_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_C_5_V3_dam_EP1: Land_House_C_5_V3_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_C_9_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_C_9_dam_EP1: Land_House_C_9_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_C_10_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_C_10_dam_EP1: Land_House_C_10_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_C_11_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_C_11_dam_EP1: Land_House_C_11_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_C_12_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_C_12_dam_EP1: Land_House_C_12_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_C_1_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_C_1_v2_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_C_2_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_C_3_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_C_4_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_C_5_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_C_9_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_C_10_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_C_11_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_C_12_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_K_2_basehide_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_K_3_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_K_3_dam_EP1: Land_House_K_3_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_K_5_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_K_5_dam_EP1: Land_House_K_5_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_K_6_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_K_6_dam_EP1: Land_House_K_6_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_K_7_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_K_7_dam_EP1: Land_House_K_7_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_K_8_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_K_8_dam_EP1: Land_House_K_8_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Terrace_K_1_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_K_1_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_K_5_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_K_3_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_K_6_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_K_7_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_K_8_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_L_1_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_L_3_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_L_3_H_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_L_3_dam_EP1: Land_House_L_3_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_L_4_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_L_4_dam_EP1: Land_House_L_4_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_L_6_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_L_6_dam_EP1: Land_House_L_6_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_L_7_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_L_7_dam_EP1: Land_House_L_7_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_L_8_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_L_8_dam_EP1: Land_House_L_8_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_L_9_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_L_1_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_L_3_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_L_4_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_L_6_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_L_7_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_L_8_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_House_L_9_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Coltan_Conv1_10_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Coltan_Conv1_Main_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Coltan_Conv1_end_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Coltan_Conv2_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Coltan_Hopper_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Coltan_Main_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_Coltan_Heap_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Coltan_Conv1_10_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Coltan_Conv1_Main_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Coltan_Conv2_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Coltan_Hopper_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Coltan_Main_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_FuelStation_Feed_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_FuelStation_Build_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_FuelStation_Shed_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_FuelStation_Build_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_FuelStation_Shed_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Garage01_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Garage01_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Oil_Pump_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Oil_Tower_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Oil_Tower_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Oil_Pump_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_IndPipe1_stair_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_IndPipe2_big_18ladder_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_IndPipe2_bigL_R_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_IndPipe2_bigL_L_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_IndPipe2_bigBuild1_R_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_IndPipe2_bigBuild1_L_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_IndPipe2_bigBuild2_L_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_IndPipe2_bigBuild2_R_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_IndPipe2_big_ground1_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_IndPipe2_big_ground2_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_PowerStation_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_PowerStation_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Shed_01_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Shed_01_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ind_Shed_02_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Mil_ControlTower_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Mil_ControlTower_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Mil_House_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Mil_House_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Mil_Barracks_i_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Mil_Barracks_i_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Mil_Barracks_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Mil_Barracks_Ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Mil_Barracks_L_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Mil_Barracks_L_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Mil_Guardhouse_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Mil_Guardhouse_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Mil_hangar_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Mil_hangar_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Mil_Repair_center_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Mil_Repair_center_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Shed_W02_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Shed_W03_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Shed_M01_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Shed_M01_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Shed_W03_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Shed_M03_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Com_tower_ep1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Com_tower_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_Cable_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_Cable_Rugs1_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_Cable_V_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_Coil_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_ConcBox_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_ConcOutlet_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_ConcPipeline_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_IronPipes_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_Rubble_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_Garb_3_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_Garb_4_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_Garb_Heap_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_Garb_Square_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Wheel_cart_EP1: Thing_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Reservoir_EP1: Thing_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Carpet_EP1: Thing_NoInteractive_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Table_small_EP1: Thing_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Blankets_EP1: Thing_NoInteractive_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Pillow_EP1: Small_Items_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Carpet_2_EP1: Thing_NoInteractive_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Cabinet_EP1: Thing_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Rack_EP1: Thing_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Chest_EP1: Thing_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Shelf_EP1: Thing_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Table_EP1: Thing_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Bench_EP1: Thing_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Crates_EP1: Thing_NoInteractive_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Bucket_EP1: Small_Items_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Bowl_EP1: Small_Items_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Teapot_EP1: Small_Items_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Water_pipe_EP1: Small_Items_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Boots_EP1: Thing_NoInteractive_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Chair_EP1: Small_Items_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Canister_EP1: Small_Items_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Vase_loam_EP1: Small_Items_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Vase_loam_2_EP1: Land_Vase_loam_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Vase_loam_3_EP1: Land_Vase_loam_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Urn_EP1: Small_Items_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Vase_EP1: Small_Items_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Wicker_basket_EP1: Small_Items_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Basket_EP1: Small_Items_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Bag_EP1: Small_Items_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Sack_EP1: Small_Items_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Lamp_Small_EP1: Lamps_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_tires_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_covering_hut_big_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_bags_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_bags_stack_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_cages_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_sunshade_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_covering_hut_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_transport_cart_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_transport_crates_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_transport_kiosk_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_stand_small_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_stand_meat_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_stand_waterl_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Crates_stack_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Carpet_rack_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Market_stalls_01_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Market_stalls_02_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Market_shelter_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_PowLineA_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_PowLineB_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_PowLines_Conc1_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_PowLines_Conc2_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_PowLines_Conc2A_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_PowLines_Transformer1_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_PowLine_wire_BB_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_PowLine_wire_AB_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_PowLine_wire_A_left_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_PowLine_wire_A_right_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_PowLines_Conc2L_EP1: Lamps_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_Well_L_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Misc_Well_C_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Fort_StoneWall_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Wall_L_2m5_gate_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Wall_L1_gate_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Wall_L3_gate_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Wall_L3_5m_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Wall_L3_pillar_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Wall_L3_5m_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Wall_L3_pillar_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Wall_L_Mosque_1_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Wall_L_Mosque_2_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Wall_L_Mosque_1_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Wall_L_Mosque_2_ruins_EP1: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Wall_FenW2_6_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Wall_L_2m5_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Wall_L1_2m5_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Wall_L1_5m_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Wall_L2_5m_EP1: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Bunker_PMC: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Bunker_PMC_Ruins: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_GeneralStore_01a_PMC: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_GeneralStore_01a_dam_PMC: Land_GeneralStore_01a_PMC
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_GeneralStore_01a_ruins_PMC: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ruin_Cowshed_a_PMC: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ruin_Cowshed_b_PMC: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ruin_Cowshed_c_PMC: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ruin_Cowshed_a_ruins_PMC: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ruin_Cowshed_b_ruins_PMC: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Ruin_Cowshed_c_ruins_PMC: Ruins
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_hopper_old_PMC: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_hopper_old_ruins_PMC: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_FuelStation_Feed_PMC: Strategic
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_FuelStation_Build_PMC: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_FuelStation_Shed_PMC: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_FuelStation_Build_ruins_PMC: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_FuelStation_Shed_ruins_PMC: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Shed_W02_PMC: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Shed_W03_PMC: House_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Shed_W03_ruins_PMC: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Shed_W02_ruins_PMC: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_ruin_corner_1_PMC: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class CUP_Akat02S: Base_CUP_Tree
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class CUP_bodlak_group: Base_CUP_Plant
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class CUP_DD_bush01: Base_CUP_Bush
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class CUP_Hromada_kameni: Rocks_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class CUP_R2_Boulder1: Rocks_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class CUP_R_Boulder_01_EP1: Rocks_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class CUP_sign_accomodation: Thing
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class CUP_SignM_DirDrillingArea_F: Thing
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class FlagCarrierBAF: FlagCarrier
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Marker_Basic_ACR: Helper_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Cube5cm_ACR: Helper_Base_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Scaffolding_ACR: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class FootBridge_0_ACR: FootBridge_Base_ACR
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class FootBridge_30_ACR: FootBridge_Base_ACR
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class ShootingRange_ACR: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Shooting_range_ruins_ACR: Ruins_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Sign_DangerMines_ACR: Sign_1L_Border
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TargetStatic_ACR: TargetE_EP1
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class TargetPopup_ACR: TargetEpopup
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};

	class Land_Mil_ControlTower_no_interior_EP1_CUP: House_EP1
	{
		scope = 1;//2;
	};
	class Land_Mil_House_no_interior_EP1_CUP: House_EP1
	{
		scope = 1;//2;
	};
	class Land_Mil_Barracks_no_interior_EP1_CUP: House_EP1
	{
		scope = 1;//2;
	};
	class Land_Mil_Guardhouse_no_interior_EP1_CUP: House_EP1
	{
		scope = 1;//2;
	};

	class Land_Plot_Wood_door: House
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
};
