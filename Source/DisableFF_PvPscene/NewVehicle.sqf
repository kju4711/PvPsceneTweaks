private["_unit"];

_vehicle = _this select 0;

if ((typeName _vehicle) == "ARRAY") then
{
	_vehicle = _vehicle select 0;
};

if (!(isNull _vehicle)) then
{
	if (time > 0) then//correct?
	{
		DisableFF_PvPscene_NewVehiclePing = true;
		publicVariable "DisableFF_PvPscene_NewVehiclePing";
	};
	[_vehicle] spawn DisableFF_PvPscene_InitVehicle;
	{
		[_x] spawn DisableFF_PvPscene_InitUnit;
	} forEach (crew _vehicle);
};