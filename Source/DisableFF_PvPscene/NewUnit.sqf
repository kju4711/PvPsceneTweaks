private["_unit"];

_unit = _this select 0;

if ((typeName _unit) == "ARRAY") then
{
	_unit = _unit select 0;
};

if (!(isNull _unit)) then
{
	if (time > 0) then//correct?
	{
		DisableFF_PvPscene_NewUnitPing = true;
		publicVariable "DisableFF_PvPscene_NewUnitPing";
	};
	[_unit] spawn DisableFF_PvPscene_InitUnit;
};