DisableFF_PvPscene_NewUnit = compile preprocessFileLineNumbers "\DisableFF_PvPscene\NewUnit.sqf";
DisableFF_PvPscene_NewVehicle = compile preprocessFileLineNumbers "\DisableFF_PvPscene\NewVehicle.sqf";
DisableFF_PvPscene_InitUnit = compile preprocessFileLineNumbers "\DisableFF_PvPscene\InitUnit.sqf";
DisableFF_PvPscene_InitVehicle = compile preprocessFileLineNumbers "\DisableFF_PvPscene\InitVehicle.sqf";
DisableFF_PvPscene_ProcessAllUnits = compile preprocessFileLineNumbers "\DisableFF_PvPscene\ProcessAllUnits.sqf";
DisableFF_PvPscene_ProcessAllVehicles = compile preprocessFileLineNumbers "\DisableFF_PvPscene\ProcessAllVehicles.sqf";

[] spawn DisableFF_PvPscene_ProcessAllVehicles;

[] spawn
{
	sleep 1;

	publicVariable "DisableFF_PvPscene_InitUnit";
	publicVariable "DisableFF_PvPscene_InitVehicle";
	publicVariable "DisableFF_PvPscene_ProcessAllUnits";
	publicVariable "DisableFF_PvPscene_ProcessAllVehicles";
};