private["_vehicle"];
_vehicle = _this select 0;

_vehicle addEventHandler ["HandleDamage",
{
	_damagedUnit = _this select 0;
	_selectionName = _this select 1;
	_amountDamage = _this select 2;
	_sourceUnit = _this select 3;

	_return = 0;

	if ((count (crew _damagedUnit)) > 0) then
	{
		_groupDamagedUnit = group _damagedUnit;
		_groupSourceUnit = group _sourceUnit;

		_sideDamagedUnit = side _groupDamagedUnit;
		_sideSourceUnit = side _groupSourceUnit;

		_sideRelation = _sideDamagedUnit getFriend _sideSourceUnit;

		if (_sideRelation >= 0.6) then
		{
			_return = 0;
		}
		else
		{
			_return = _amountDamage;
		};
	}
	else
	{
		_return = _amountDamage;
	};
	_return;
}];