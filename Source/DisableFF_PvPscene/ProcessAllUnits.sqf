{
	private["_unit"];
	_unit = _x;
	if (!(isNull _unit)) then
	{
		private["_alreadyProcessed"];
		_alreadyProcessed = _unit getVariable ["DisableFF_PvPscene_AlreadyProcessed",false];
		if (!(_alreadyProcessed)) then
		{
			_unit setVariable ["DisableFF_PvPscene_AlreadyProcessed",true];
			[_unit] spawn DisableFF_PvPscene_InitUnit;
		};
	};
} forEach allUnits;