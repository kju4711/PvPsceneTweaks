[] spawn DisableFF_PvPscene_ProcessAllUnits;

{
	private["_vehicle"];
	_vehicle = _x;
	if (!(isNull _vehicle)) then
	{
		private["_alreadyProcessed"];
		_alreadyProcessed = _vehicle getVariable ["DisableFF_PvPscene_AlreadyProcessed",false];
		if (!(_alreadyProcessed)) then
		{
			_vehicle setVariable ["DisableFF_PvPscene_AlreadyProcessed",true];
			[_vehicle] spawn DisableFF_PvPscene_InitVehicle;
		};
	};
} forEach vehicles;