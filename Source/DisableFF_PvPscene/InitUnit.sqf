private["_unit","_tickTime"];
_unit = _this select 0;

_tickTime = floor diag_tickTime;

_unit setVariable ["PvPscene_LatestUnitDamageTickTime",_tickTime,false];
_unit setVariable ["PvPscene_LatestHeadDamageTickTime",_tickTime,false];
_unit setVariable ["PvPscene_LatestBodyDamageTickTime",_tickTime,false];
_unit setVariable ["PvPscene_LatestHandsDamageTickTime",_tickTime,false];
_unit setVariable ["PvPscene_LatestLegsDamageTickTime",_tickTime,false];

_unit addEventHandler ["HandleDamage",
{
	_damagedUnit = _this select 0;
	_selectionName = _this select 1;
	_amountDamage = _this select 2;
	_sourceUnit = _this select 3;

	_tickTime = floor diag_tickTime;
	_return = 0;

	_variableName = switch (_selectionName) do
	{
		case "head_hit":	{"PvPscene_LatestHeadDamageTickTime"};
		case "body":		{"PvPscene_LatestBodyDamageTickTime"};
		case "hands":		{"PvPscene_LatestHandsDamageTickTime"};
		case "legs":		{"PvPscene_LatestLegsDamageTickTime"};
		default			{"PvPscene_LatestUnitDamageTickTime"};
	};

	if (_damagedUnit == _sourceUnit) then
	{
		_latestDamageTickTime = _damagedUnit getVariable [_variableName,0];

		if (_latestDamageTickTime == _tickTime) then
		{
			_damagedUnit setVelocity [0,0,0];
			_return = 0;
		}
		else
		{
			_return = _amountDamage;
		};
	}
	else
	{
		_groupDamagedUnit = group _damagedUnit;
		_groupSourceUnit = group _sourceUnit;

		_sideDamagedUnit = side _groupDamagedUnit;
		_sideSourceUnit = side _groupSourceUnit;

		_sideRelation = _sideDamagedUnit getFriend _sideSourceUnit;

		if (_sideRelation >= 0.6) then
		{
			_return = 0;
		}
		else
		{
			_return = _amountDamage;
		};
	};

	_damagedUnit setVariable [_variableName,_tickTime,false];

	_return;
}];