///////////////////////////////////////////////////////////////////////////////
#                                                                             #
#                               PvPscene Tweaks                               #
#                                                                             #
///////////////////////////////////////////////////////////////////////////////


#==============#
| Release date |
#==============#

2017-07-24

#===========#
| Component |
#===========#

NoIRSignatureForCarsAndMotorcycles_Gameplay_C_PvPscene

#================#
| Main author(s) |
#================#

kju

#===============#
| Local credits |
#===============#

None

#=============================#
| Rough component description |
#=============================#

Removes the IR signature for cars and motorcycles and they are therefore no
longer visible as items on the radar screen.

#=================#
| Contact details |
#=================#

Mail:	pvpscene@web.de



///////////////////////////////////////////////////////////////////////////////
#                                                                             #
#                               PvPscene Tweaks                               #
#                                                                             #
///////////////////////////////////////////////////////////////////////////////
//                                                                           //
#     Creative Commons License                                                #
//                                                                           //
#     License   http://creativecommons.org/licenses/by-nc-sa/3.0/de/deed.en   #
#     Title     PvPscene Projects FILE                                        #
#     By        PvPscene                                                      #
//                                                                           //
#     Is licensed under a Creative Commons                                    #
#     Attribution-Non-Commercial-Share Alike 3.0 License                      #
//                                                                           //
///////////////////////////////////////////////////////////////////////////////