class CfgPatches
{
	class NoIRSignatureForCarsAndMotorcycles_Gameplay_C_PvPscene
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Data_F_Loadorder","A3_Data_F_Curator_Loadorder","A3_Data_F_Kart_Loadorder","A3_Data_F_Bootcamp_Loadorder","A3_Data_F_Heli_Loadorder","A3_Data_F_Mark_Loadorder","A3_Data_F_Exp_A_Loadorder","A3_Data_F_Exp_B_Loadorder","A3_Data_F_Exp_Loadorder","A3_Data_F_Jets_Loadorder","A3_Data_F_Argo_Loadorder","A3_Data_F_Patrol_Loadorder","A3_Data_F_Orange_Loadorder","A3_Data_F_Tacops_Loadorder","A3_Data_F_Tank_Loadorder"};
		version = "2019-03-28";
	};
};
class CfgVehicles
{
	class LandVehicle;
	class Car_F;

	class Car: LandVehicle
	{
		irTarget = 0;
	};
	class Motorcycle: LandVehicle
	{
		irTarget = 0;
	};
	class Truck: Car
	{
		irTarget = 1;
	};
	class Wheeled_APC: Car
	{
		irTarget = 1;
	};
	class Wheeled_APC_F: Car_F
	{
		irTarget = 1;
	};
};
