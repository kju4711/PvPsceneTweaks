class CfgPatches
{
	class MoreHeadMovement_Gameplay_C_PvPscene
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Data_F_Loadorder","A3_Data_F_Curator_Loadorder","A3_Data_F_Kart_Loadorder","A3_Data_F_Bootcamp_Loadorder","A3_Data_F_Heli_Loadorder","A3_Data_F_Mark_Loadorder","A3_Data_F_Exp_A_Loadorder","A3_Data_F_Exp_B_Loadorder","A3_Data_F_Exp_Loadorder","A3_Data_F_Jets_Loadorder","A3_Data_F_Argo_Loadorder","A3_Data_F_Patrol_Loadorder","A3_Data_F_Orange_Loadorder","A3_Data_F_Tacops_Loadorder","A3_Data_F_Tank_Loadorder"};
		version = "2019-03-28";
	};
};
class CfgVehicles
{
	class Land;
	class Man: Land
	{
		class ViewPilot;
		class HeadLimits;
	};

	class CAManBase: Man
	{
		minGunElev = -89;
		maxGunElev = 89;
		class ViewPilot: ViewPilot
		{
			maxAngleX = 89;
			minAngleY = -155;
			maxAngleY = 155;
		};
	};
	class SoldierWB: CAManBase
	{
		class HeadLimits: HeadLimits
		{
			minAngleX = -45;
			maxAngleX = 60;
			minAngleY = -90;
			maxAngleY = 90;
		};
	};
	class SoldierEB: CAManBase
	{
		class HeadLimits: HeadLimits
		{
			minAngleX = -45;
			maxAngleX = 60;
			minAngleY = -90;
			maxAngleY = 90;
		};
	};
	class SoldierGB: CAManBase
	{
		class HeadLimits: HeadLimits
		{
			minAngleX = -45;
			maxAngleX = 60;
			minAngleY = -90;
			maxAngleY = 90;
		};
	};
	class Civilian: CAManBase
	{
		class HeadLimits: HeadLimits
		{
			minAngleX = -45;
			maxAngleX = 60;
		};
	};
};
