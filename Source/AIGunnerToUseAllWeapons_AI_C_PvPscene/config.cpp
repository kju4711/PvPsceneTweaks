class CfgPatches
{
	class AIGunnerToUseAllWeapons_AI_C_PvPscene
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Data_F_Loadorder","A3_Data_F_Curator_Loadorder","A3_Data_F_Kart_Loadorder","A3_Data_F_Bootcamp_Loadorder","A3_Data_F_Heli_Loadorder","A3_Data_F_Mark_Loadorder","A3_Data_F_Exp_A_Loadorder","A3_Data_F_Exp_B_Loadorder","A3_Data_F_Exp_Loadorder","A3_Data_F_Jets_Loadorder","A3_Data_F_Argo_Loadorder","A3_Data_F_Patrol_Loadorder","A3_Data_F_Orange_Loadorder","A3_Data_F_Tacops_Loadorder","A3_Data_F_Tank_Loadorder"};
		version = "2019-03-28";
	};
};
class Mode_SemiAuto;
//	autoFire = 0;
class Mode_FullAuto;
//	autoFire = 1;
class CfgWeapons
{
	class LauncherCore;
	class CannonCore;

//	class Default
//	{
//		autoFire = 0;
//	};
//	class MGun: MGunCore
//	{
//		autoFire = 1;
//	};

//	class SmokeLauncher: MGun
//	{
//		autoFire = 0;
//	};
//	class FlareLauncher: SmokeLauncher
//	{
//		autoFire = 0;
//	};

//	class GMG_F: MGun
//	{
//		autoFire = 1;
//	};

	class autocannon_Base_F: CannonCore
	{
//		autoFire = 1;
//		class player: Mode_FullAuto
	};
//	class autocannon_35mm: CannonCore
//		class manual: CannonCore
	class gatling_30mm_base: CannonCore
	{
//		autoFire = 1;
//		class LowROF: Mode_FullAuto
	};
//	class gatling_20mm: CannonCore
//		class manual: CannonCore
//	class gatling_25mm: CannonCore
//		class manual: CannonCore

	// tank main gun
	class cannon_105mm: CannonCore
	{
		autoFire = 1;//0;
		class player: Mode_SemiAuto
		{
			autoFire = 1;//0;
		};
	};
	class cannon_120mm: CannonCore
	{
		autoFire = 1;//0;
		class player: Mode_SemiAuto
		{
			autoFire = 1;//0;
		};
	};
	class cannon_125mm: CannonCore
	{
		autoFire = 1;//0;
		class player: Mode_SemiAuto
		{
			autoFire = 1;//0;
		};
	};


//	class Gatling_30mm_Plane_CAS_01_F: CannonCore
//	{
//		autoFire = 1;//0;
//		class LowROF: Mode_FullAuto
//		{
//			autoFire = 1;
//		};
//	};
//	class Cannon_30mm_Plane_CAS_02_F: CannonCore
//	{
//		autoFire = 1;
//		class LowROF: Mode_FullAuto
//		{
//			autoFire = 1;//(0)
//		};
//	};


//	class weapon_Cannon_Phalanx: CannonCore
//		class manual: CannonCore
//	class weapon_Fighter_Gun20mm_AA: CannonCore
//		class manual: CannonCore
//	class weapon_Fighter_Gun_30mm: CannonCore
//		class manual: CannonCore



	// guides missiles
	class MissileLauncher: LauncherCore
	{
		autoFire = 1;//(0)
	};

//	class missiles_ASRAAM: MissileLauncher
//	class missiles_titan: MissileLauncher
//	class missiles_Zephyr: MissileLauncher
//	class Missile_AGM_02_Plane_CAS_01_F: MissileLauncher
//	class weapon_rim116Launcher: MissileLauncher
//	class weapon_rim162Launcher: MissileLauncher
//	class weapon_AMRAAMLauncher: MissileLauncher
//	class weapon_BIM9xLauncher: MissileLauncher
	class weapon_R73Launcher: MissileLauncher
	{
		autoFire = 1;//0;
	};
	class weapon_R77Launcher: MissileLauncher
	{
		autoFire = 1;//0;
	};
	class weapon_AGM_KH25Launcher: MissileLauncher
	{
		autoFire = 1;//0;
	};



	// unguided rockets
	class RocketPods: LauncherCore
	{
//		autoFire = 1;
	};

	class missiles_DAGR: RocketPods
	{
		class Far_AI: RocketPods
		{
			autoFire = 1;//0;
		};
		class Burst: RocketPods
		{
			autoFire = 1;//0;
		};
	};
	class missiles_DAR: RocketPods
	{
		class Far_AI: RocketPods
		{
			autoFire = 1;//0;
		};
	};
	class missiles_SCALPEL: RocketPods
	{
		autoFire = 1;//0;
	};
	class Missile_AA_04_Plane_CAS_01_F: RocketPods
	{
		autoFire = 1;//0;
	};


	class rockets_Skyfire: RocketPods
	{
		class Far_AI: RocketPods
		{
			autoFire = 1;//0;
		};
	};
	class rockets_230mm_GAT: RocketPods
	{
		class Close: RocketPods
		{
			autoFire = 1;//0;
		};
	};
	class Rocket_04_HE_Plane_CAS_01_F: RocketPods
	{
		class Far_AI: RocketPods
		{
			autoFire = 1;//0;
		};
		class Burst: RocketPods
		{
			autoFire = 1;//0;
		};
	};


	//bombs
	class weapon_AGM_65Launcher: RocketPods
	{
		autoFire = 1;//0;
	};
	class weapon_GBU12Launcher: RocketPods
	{
		autoFire = 1;//0;
	};
	class weapon_KAB250Launcher: RocketPods
	{
		autoFire = 1;//0;
	};

//	class LMG_Zafir_F: Rifle_Long_Base_F
//	{
//		class Single: Mode_SemiAuto
//		{
//			autoFire = 1;//0;
//		};
//		class far_optic2: far_optic1
//		{
//			autoFire = 1;//0;
//		};
//	};
//	class MMG_01_base_F: Rifle_Long_Base_F
//	{
//		class far_optic2: far_optic1
//		{
//			autoFire = 1;//0;
//		};
//	};
//	class MMG_02_base_F: Rifle_Long_Base_F
//	{
//		class far_optic2: far_optic1
//		{
//			autoFire = 1;//0;
//		};
//	};
};
