class CfgPatches
{
	class WW2_Optional_WW2StyleCompassAndWatch_c
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"WW2_Core_c_IF_Gui_c"};
		version = "2017-12-07";
	};
};

class RscObject;
class RscCompass: RscObject
{
	model = "\WW2\Core_m\IF_Data_m\WW2_Compass.p3d";
	direction[] = {0,0.3,0.7};
	positionBack[] = {0,-0.01,0.075};
	scale = 0.64;
};
class RscWatch: RscObject
{
	model = "\WW2\Core_m\IF_Data_m\WW2_Watch.p3d";
};
class RscDisplayMainMap
{
	class objects
	{
//Warning: no idc entry inside class RscDisplayMainMap/objects/Watch
//		class Watch: RscObject
//		{
//			model = "\WW2\Core_m\IF_Data_m\WW2_Watch.p3d";
//			x = 0.08;
//			xBack = 0.4;
//			y = 0.925;
//			yBack = 0.5;
//			z = 0.21;
//			zBack = 0.11;
//			enableZoom = 1;
//			direction[] = {0,1,7.2};
//			up[] = {0,0,-1};
//			scale = 0.4;
//		};
		class Compass: RscObject
		{
			model = "\WW2\Core_m\IF_Data_m\WW2_Compass.p3d";
		};
	};
};
