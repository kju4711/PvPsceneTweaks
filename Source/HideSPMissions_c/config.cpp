﻿class CfgPatches
{
	class Optional_HideSPMissions_c
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Data_F_Loadorder","A3_Data_F_Curator_Loadorder","A3_Data_F_Kart_Loadorder","A3_Data_F_Bootcamp_Loadorder","A3_Data_F_Heli_Loadorder","A3_Data_F_Mark_Loadorder","A3_Data_F_Exp_A_Loadorder","A3_Data_F_Exp_B_Loadorder","A3_Data_F_Exp_Loadorder","A3_Data_F_Jets_Loadorder","A3_Data_F_Argo_Loadorder","A3_Data_F_Patrol_Loadorder","A3_Data_F_Orange_Loadorder","A3_Data_F_Tacops_Loadorder","A3_Data_F_Tank_Loadorder"};
		version = "2019-03-28";
	};
};
class CfgMissions
{
	class Showcases
	{
		delete Showcase_Infantry;
		delete Showcase_SCUBA;
		delete Showcase_Vehicles;
		delete Showcase_Helicopters;
		delete Showcase_Combined_arms;
		delete Showcase_Commanding_I;
		delete Showcase_Night;
		delete Showcase_Supports;
		delete Showcase_Tanks;
		delete Showcase_Arma;
		delete Showcase_Drones;
		delete Showcase_Gunships;
		delete Faction_BLUFOR;
		delete Faction_OPFOR;
		delete Faction_INDEPENDENT;
		delete Showcase_FixedWings;
		delete Showcase_Curator;
		delete Showcase_SlingLoading;
		delete Showcase_FiringFromVehicles;
		delete Showcase_Marksman;
		delete Showcase_EndGame;
		delete VTOL;
		delete Showcase_Jets;
		delete Faction_IDAP;
		delete Showcase_LawsOfWar;
		delete Showcase_TankDestroyers;
	};
};
