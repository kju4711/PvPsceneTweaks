class CfgPatches
{
	class ImprovedExtCameraPositionForTanks_Gameplay_C_PvPscene
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Data_F_Loadorder","A3_Data_F_Curator_Loadorder","A3_Data_F_Kart_Loadorder","A3_Data_F_Bootcamp_Loadorder","A3_Data_F_Heli_Loadorder","A3_Data_F_Mark_Loadorder","A3_Data_F_Exp_A_Loadorder","A3_Data_F_Exp_B_Loadorder","A3_Data_F_Exp_Loadorder","A3_Data_F_Jets_Loadorder","A3_Data_F_Argo_Loadorder","A3_Data_F_Patrol_Loadorder","A3_Data_F_Orange_Loadorder","A3_Data_F_Tacops_Loadorder","A3_Data_F_Tank_Loadorder"};
		version = "2019-03-28";
	};
};
class CfgVehicles
{
	class LandVehicle;

	class Tank: LandVehicle
	{
		extCameraPosition[] = {0,3,-14};//{0,1.5,-9};
	};
	class Tank_F: Tank
	{
		extCameraPosition[] = {0,3,-14};//{0,2,-10};
	};
	class APC_Tracked_02_base_F: Tank_F
	{
		extCameraPosition[] = {0,3,-14};//{0,2.25,-9};
	};
	class MBT_01_base_F: Tank_F
	{
		extCameraPosition[] = {0,3,-14};//{0,2.25,-9};
	};
	class MBT_02_base_F: Tank_F
	{
		extCameraPosition[] = {0,3,-14};//{0,2.25,-9};
	};
	class APC_Tracked_03_base_F: Tank_F
	{
		extCameraPosition[] = {0,3,-14};//{0,2.25,-9};
	};
	class MBT_03_base_F: Tank_F
	{
		extCameraPosition[] = {0,3,-14};//{0,2.25,-9};
	};
};
