class CfgPatches
{
	class DisabledButtonAndListFlashingOnFocus_Visuals_C_PvPscene
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Data_F_Loadorder","A3_Data_F_Curator_Loadorder","A3_Data_F_Kart_Loadorder","A3_Data_F_Bootcamp_Loadorder","A3_Data_F_Heli_Loadorder","A3_Data_F_Mark_Loadorder","A3_Data_F_Exp_A_Loadorder","A3_Data_F_Exp_B_Loadorder","A3_Data_F_Exp_Loadorder","A3_Data_F_Jets_Loadorder","A3_Data_F_Argo_Loadorder","A3_Data_F_Patrol_Loadorder","A3_Data_F_Orange_Loadorder","A3_Data_F_Tacops_Loadorder","A3_Data_F_Tank_Loadorder"};
		version = "2019-03-28";
	};
};

class RscButtonTextOnly;
class ctrlDefaultButton;
class ctrlDefaultText;
class RscStandardDisplay;

class RscShortcutButton
{
	periodFocus = 0;//1.2;
	periodOver = 0;//0.8;
	period = 0;//0.4;
};
class RscListBox
{
	period = 0;//1.2;
};
class RscListNBox
{
	period = 0;//1.2;
};
class RscMsgBoxError
{
	class Bottom
	{
		class Button1: RscShortcutButton
		{
			period = 0;//0.5;
		};
	};
};
class RscDisplayDiary
{
	class Controls
	{
		class ButtonPlayers: RscButtonTextOnly
		{
			period = 0;//1.2;
		};
	};
};
class RscDisplayMainMap
{
	class controls
	{
		class ButtonPlayers: RscButtonTextOnly
		{
			period = 0;//1.2;
		};
	};
};
class RscDisplayGetReady: RscDisplayMainMap
{
	class controls
	{
		class ButtonPlayers: RscButtonTextOnly
		{
			period = 0;//1.2;
		};
	};
};
class RscDisplayMultiplayer: RscStandardDisplay
{
	class controls
	{
		class CA_ServerColumn: RscButtonTextOnly
		{
			period = 0;//1.2;
		};
	};
};
class RscDisplayMultiplayerSetup: RscStandardDisplay
{
	class controls
	{
		class ButtonPlayers: RscButtonTextOnly
		{
			period = 0;//1.2;
		};
	};
};
class RscDisplayServerGetReady: RscDisplayGetReady
{
	class controls
	{
		class ButtonPlayers: RscButtonTextOnly
		{
			period = 0;//1.2;
		};
	};
};
class RscDisplayClientGetReady: RscDisplayGetReady
{
	class controls
	{
		class ButtonPlayers: RscButtonTextOnly
		{
			period = 0;//1.2;
		};
	};
};
class RscShortcutButtonMain: RscShortcutButton
{
	period = 0;//0.5;
};
class RscButtonMenu: RscShortcutButton
{
	period = 0;//1.2;
	periodFocus = 0;//1.2;
	periodOver = 0;//1.2;
};
class ctrlButton: ctrlDefaultButton
{
	period = 0;//0;
	periodFocus = 0;//2;
	periodOver = 0;//0.5;
};
class ctrlListbox: ctrlDefaultText
{
	period = 0;//1;
};
class ctrlShortcutButton: ctrlDefaultButton
{
	period = 0;//1;
	periodFocus = 0;//1;
	periodOver = 0;//0.5;
};
class ctrlListNBox: ctrlDefaultText
{
	period = 0;//1;
};
