///////////////////////////////////////////////////////////////////////////////
#                                                                             #
#                               PvPscene Tweaks                               #
#                                                                             #
///////////////////////////////////////////////////////////////////////////////


#==============#
| Release date |
#==============#

2017-07-24

#===========#
| Component |
#===========#

DisabledPeripheralVisionNeutral_Visuals_C_PvPscene

#================#
| Main author(s) |
#================#

kju

#===============#
| Local credits |
#===============#

None

#=============================#
| Rough component description |
#=============================#

Removes the white spheres indicating neutral units, animals or empty vehicles.
Only confuses people and not a good try to simulate peripheral vision.

#=================#
| Contact details |
#=================#

Mail:	pvpscene@web.de



///////////////////////////////////////////////////////////////////////////////
#                                                                             #
#                               PvPscene Tweaks                               #
#                                                                             #
///////////////////////////////////////////////////////////////////////////////
//                                                                           //
#     Creative Commons License                                                #
//                                                                           //
#     License   http://creativecommons.org/licenses/by-nc-sa/3.0/de/deed.en   #
#     Title     PvPscene Projects FILE                                        #
#     By        PvPscene                                                      #
//                                                                           //
#     Is licensed under a Creative Commons                                    #
#     Attribution-Non-Commercial-Share Alike 3.0 License                      #
//                                                                           //
///////////////////////////////////////////////////////////////////////////////