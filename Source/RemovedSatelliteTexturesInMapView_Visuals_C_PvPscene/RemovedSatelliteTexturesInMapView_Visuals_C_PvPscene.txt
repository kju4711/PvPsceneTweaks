///////////////////////////////////////////////////////////////////////////////
#                                                                             #
#                               PvPscene Tweaks                               #
#                                                                             #
///////////////////////////////////////////////////////////////////////////////


#==============#
| Release date |
#==============#

2017-07-24

#===========#
| Component |
#===========#

RemovedSatelliteTexturesInMapView_Visuals_C_PvPscene

#================#
| Main author(s) |
#================#

Rommel
NouberNou

#===============#
| Local credits |
#===============#

kju

#=============================#
| Rough component description |
#=============================#

Removes the satellite textures in the map view.

#=================#
| Contact details |
#=================#

Mail:	pvpscene@web.de



///////////////////////////////////////////////////////////////////////////////
#                                                                             #
#                               PvPscene Tweaks                               #
#                                                                             #
///////////////////////////////////////////////////////////////////////////////
//                                                                           //
#     Creative Commons License                                                #
//                                                                           //
#     License   http://creativecommons.org/licenses/by-nc-sa/3.0/de/deed.en   #
#     Title     PvPscene Projects FILE                                        #
#     By        PvPscene                                                      #
//                                                                           //
#     Is licensed under a Creative Commons                                    #
#     Attribution-Non-Commercial-Share Alike 3.0 License                      #
//                                                                           //
///////////////////////////////////////////////////////////////////////////////