class CfgPatches
{
	class ShowTextInsteadOfActionIcons_Visuals_C_PvPscene
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Data_F_Loadorder","A3_Data_F_Curator_Loadorder","A3_Data_F_Kart_Loadorder","A3_Data_F_Bootcamp_Loadorder","A3_Data_F_Heli_Loadorder","A3_Data_F_Mark_Loadorder","A3_Data_F_Exp_A_Loadorder","A3_Data_F_Exp_B_Loadorder","A3_Data_F_Exp_Loadorder","A3_Data_F_Jets_Loadorder","A3_Data_F_Argo_Loadorder","A3_Data_F_Patrol_Loadorder","A3_Data_F_Orange_Loadorder","A3_Data_F_Tacops_Loadorder","A3_Data_F_Tank_Loadorder"};
		version = "2019-03-28";
	};
};
class CfgActions
{
	class None;

	class GetInCommander: None
	{
		textDefault = "";
	};
	class GetInDriver: None
	{
		textDefault = "";
	};
	class GetInPilot: None
	{
		textDefault = "";
	};
	class GetInGunner: None
	{
		textDefault = "";
	};
	class GetInCargo: None
	{
		textDefault = "";
	};
	class GetInTurret: None
	{
		textDefault = "";
	};
	class Heal: None
	{
		textDefault = "";
	};
	class HealSoldier: None
	{
		textDefault = "";
	};
	class RepairVehicle: None
	{
		textDefault = "";
	};
	class FirstAid: None
	{
		textDefault = "";
	};
	class Repair: None
	{
		textDefault = "";
	};
	class Refuel: None
	{
		textDefault = "";
	};
	class Rearm: None
	{
		textDefault = "";
	};
	class GetOut: None
	{
		textDefault = "";
	};
	class LightOn: None
	{
		textDefault = "";
	};
	class LightOff: LightOn
	{
		textDefault = "";
	};
	class GunLightOn: None
	{
		textDefault = "";
	};
	class GunLightOff: None
	{
		textDefault = "";
	};
	class ArtilleryComputer: None
	{
		textDefault = "";
	};
	class EngineOn: None
	{
		textDefault = "";
	};
	class EngineOff: None
	{
		textDefault = "";
	};
	class SwitchWeapon: None
	{
		textDefault = "";
	};
	class HideWeapon: SwitchWeapon
	{
		textDefault = "";
	};
	class UseWeapon: None
	{
		textDefault = "";
	};
	class LoadMagazine: None
	{
		textDefault = "";
	};
	class LoadEmptyMagazine: LoadMagazine
	{
		textDefault = "";
	};
	class TakeWeapon: None
	{
		textDefault = "";
	};
	class TakeDropWeapon: TakeWeapon
	{
		textDefault = "";
	};
	class TakeMagazine: None
	{
		textDefault = "";
	};
	class TakeDropMagazine: TakeMagazine
	{
		textDefault = "";
	};
	class TakeFlag: None
	{
		textDefault = "";
	};
	class ReturnFlag: None
	{
		textDefault = "";
	};
	class TurnIn: None
	{
		textDefault = "";
	};
	class TurnOut: None
	{
		textDefault = "";
	};
	class WeaponInHand: None
	{
		textDefault = "";
	};
	class WeaponOnBack: None
	{
		textDefault = "";
	};
	class SitDown: None
	{
		textDefault = "";
	};
	class Land: None
	{
		textDefault = "";
	};
	class CancelLand: None
	{
		textDefault = "";
	};
	class Eject: None
	{
		textDefault = "";
	};
	class MoveToDriver: None
	{
		textDefault = "";
	};
	class MoveToPilot: MoveToDriver
	{
		textDefault = "";
	};
	class MoveToGunner: None
	{
		textDefault = "";
	};
	class MoveToCommander: None
	{
		textDefault = "";
	};
	class MoveToCargo: None
	{
		textDefault = "";
	};
	class MoveToTurret: None
	{
		textDefault = "";
	};
	class HideBody: None
	{
		textDefault = "";
	};
	class TouchOff: None
	{
		textDefault = "";
	};
	class SetTimer: None
	{
		textDefault = "";
	};
	class StartTimer: SetTimer
	{
		textDefault = "";
	};
	class Deactivate: None
	{
		textDefault = "";
	};
	class NVGoggles: None
	{
		textDefault = "";
	};
	class NVGogglesOff: NVGoggles
	{
		textDefault = "";
	};
	class ManualFire: None
	{
		textDefault = "";
	};
	class ManualFireCancel: ManualFire
	{
		textDefault = "";
	};
	class AutoHover: None
	{
		textDefault = "";
	};
	class AutoHoverCancel: AutoHover
	{
		textDefault = "";
	};
	class StrokeFist: None
	{
		textDefault = "";
	};
	class StrokeGun: None
	{
		textDefault = "";
	};
	class LadderUp: None
	{
		textDefault = "";
	};
	class LadderDown: None
	{
		textDefault = "";
	};
	class LadderOnDown: None
	{
		textDefault = "";
	};
	class LadderOnUp: None
	{
		textDefault = "";
	};
	class LadderOff: None
	{
		textDefault = "";
	};
	class FireInflame: None
	{
		textDefault = "";
	};
	class FirePutDown: None
	{
		textDefault = "";
	};
	class LandGear: None
	{
		textDefault = "";
	};
	class LandGearUp: LandGear
	{
		textDefault = "";
	};
	class FlapsDown: None
	{
		textDefault = "";
	};
	class FlapsUp: None
	{
		textDefault = "";
	};
	class Salute: None
	{
		textDefault = "";
	};
	class ScudLaunch: None
	{
		textDefault = "";
	};
	class ScudStart: None
	{
		textDefault = "";
	};
	class ScudCancel: None
	{
		textDefault = "";
	};
	class User: None
	{
		textDefault = "";
	};
	class DropWeapon: None
	{
		textDefault = "";
	};
	class PutWeapon: DropWeapon
	{
		textDefault = "";
	};
	class DropMagazine: None
	{
		textDefault = "";
	};
	class PutMagazine: DropMagazine
	{
		textDefault = "";
	};
	class UserType: None
	{
		textDefault = "";
	};
	class HandGunOn: None
	{
		textDefault = "";
	};
	class HandGunOnStand: HandGunOn
	{
		textDefault = "";
	};
	class HandGunOff: None
	{
		textDefault = "";
	};
	class HandGunOffStand: HandGunOff
	{
		textDefault = "";
	};
	class TakeMine: None
	{
		textDefault = "";
	};
	class DeactivateMine: None
	{
		textDefault = "";
	};
	class UseMagazine: None
	{
		textDefault = "";
	};
	class IngameMenu: None
	{
		textDefault = "";
	};
	class CancelTakeFlag: None
	{
		textDefault = "";
	};
	class CancelAction: None
	{
		textDefault = "";
	};
	class MarkEntity: None
	{
		textDefault = "";
	};
	class MarkWeapon: MarkEntity
	{
		textDefault = "";
	};
	class TeamSwitch: None
	{
		textDefault = "";
	};
	class Gear: None
	{
		textDefault = "";
	};
	class OpenBag: None
	{
		textDefault = "";
	};
	class TakeBag: None
	{
		textDefault = "";
	};
	class PutBag: None
	{
		textDefault = "";
	};
	class DropBag: None
	{
		textDefault = "";
	};
	class AddBag: None
	{
		textDefault = "";
	};
	class IRLaserOn: None
	{
		textDefault = "";
	};
	class IRLaserOff: None
	{
		textDefault = "";
	};
	class Assemble: None
	{
		textDefault = "";
	};
	class DisAssemble: None
	{
		textDefault = "";
	};
	class Talk: None
	{
		textDefault = "";
	};
	class Tell: None
	{
		textDefault = "";
	};
	class Surrender: None
	{
		textDefault = "";
	};
	class GetOver: None
	{
		textDefault = "";
	};
};
