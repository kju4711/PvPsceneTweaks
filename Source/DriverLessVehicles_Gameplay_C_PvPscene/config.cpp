﻿class CfgPatches
{
	class DriverLessVehicles_Gameplay_C_PvPscene
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Data_F_Loadorder","A3_Data_F_Curator_Loadorder","A3_Data_F_Kart_Loadorder","A3_Data_F_Bootcamp_Loadorder","A3_Data_F_Heli_Loadorder","A3_Data_F_Mark_Loadorder","A3_Data_F_Exp_A_Loadorder","A3_Data_F_Exp_B_Loadorder","A3_Data_F_Exp_Loadorder","A3_Data_F_Jets_Loadorder","A3_Data_F_Argo_Loadorder","A3_Data_F_Patrol_Loadorder","A3_Data_F_Orange_Loadorder","A3_Data_F_Tacops_Loadorder","A3_Data_F_Tank_Loadorder"};
		version = "2019-03-28";
	};
};
class cfgVehicles
{
	class B_APC_Tracked_01_rcws_F;
	class B_APC_Tracked_01_rcws_F_DLV: B_APC_Tracked_01_rcws_F
	{
		scopeArsenal = 1;
		vehicleClass = "Armored_DLV";
		hasDriver = -1;
	};
	class B_APC_Tracked_01_CRV_F;
	class B_APC_Tracked_01_CRV_F_DLV: B_APC_Tracked_01_CRV_F
	{
		scopeArsenal = 1;
		vehicleClass = "Support_DLV";
		hasDriver = -1;
	};
	class B_APC_Tracked_01_AA_F;
	class B_APC_Tracked_01_AA_F_DLV: B_APC_Tracked_01_AA_F
	{
		scopeArsenal = 1;
		vehicleClass = "Armored_DLV";
		hasDriver = -1;
	};
	class O_APC_Tracked_02_cannon_F;
	class O_APC_Tracked_02_cannon_F_DLV: O_APC_Tracked_02_cannon_F
	{
		scopeArsenal = 1;
		vehicleClass = "Armored_DLV";
		hasDriver = -1;
	};
	class O_APC_Tracked_02_AA_F;
	class O_APC_Tracked_02_AA_F_DLV: O_APC_Tracked_02_AA_F
	{
		scopeArsenal = 1;
		vehicleClass = "Armored_DLV";
		hasDriver = -1;
	};
	class B_MBT_01_cannon_F;
	class B_MBT_01_cannon_F_DLV: B_MBT_01_cannon_F
	{
		scopeArsenal = 1;
		vehicleClass = "Armored_DLV";
		hasDriver = -1;
	};
	class B_MBT_01_arty_F;
	class B_MBT_01_arty_F_DLV: B_MBT_01_arty_F
	{
		scopeArsenal = 1;
		vehicleClass = "Armored_DLV";
		hasDriver = -1;
	};
	class B_MBT_01_mlrs_F;
	class B_MBT_01_mlrs_F_DLV: B_MBT_01_mlrs_F
	{
		scopeArsenal = 1;
		vehicleClass = "Armored_DLV";
		hasDriver = -1;
	};
	class O_MBT_02_cannon_F;
	class O_MBT_02_cannon_F_DLV: O_MBT_02_cannon_F
	{
		scopeArsenal = 1;
		vehicleClass = "Armored_DLV";
		hasDriver = -1;
	};
	class O_MBT_02_arty_F;
	class O_MBT_02_arty_F_DLV: O_MBT_02_arty_F
	{
		scopeArsenal = 1;
		vehicleClass = "Armored_DLV";
		hasDriver = -1;
	};
	class B_MRAP_01_gmg_F;
	class B_MRAP_01_gmg_F_DLV: B_MRAP_01_gmg_F
	{
		scopeArsenal = 1;
		vehicleClass = "Car_DLV";
		hasDriver = -1;
	};
	class B_MRAP_01_hmg_F;
	class B_MRAP_01_hmg_F_DLV: B_MRAP_01_hmg_F
	{
		scopeArsenal = 1;
		vehicleClass = "Car_DLV";
		hasDriver = -1;
	};
	class O_MRAP_02_hmg_F;
	class O_MRAP_02_hmg_F_DLV: O_MRAP_02_hmg_F
	{
		scopeArsenal = 1;
		vehicleClass = "Car_DLV";
		hasDriver = -1;
	};
	class O_MRAP_02_gmg_F;
	class O_MRAP_02_gmg_F_DLV: O_MRAP_02_gmg_F
	{
		scopeArsenal = 1;
		vehicleClass = "Car_DLV";
		hasDriver = -1;
	};
	class I_G_Offroad_01_armed_F;
	class I_G_Offroad_01_armed_F_DLV: I_G_Offroad_01_armed_F
	{
		scopeArsenal = 1;
		vehicleClass = "Car_DLV";
		hasDriver = -1;
	};
	class B_G_Offroad_01_armed_F;
	class B_G_Offroad_01_armed_F_DLV: B_G_Offroad_01_armed_F
	{
		scopeArsenal = 1;
		vehicleClass = "Car_DLV";
		hasDriver = -1;
	};
	class O_G_Offroad_01_armed_F;
	class O_G_Offroad_01_armed_F_DLV: O_G_Offroad_01_armed_F
	{
		scopeArsenal = 1;
		vehicleClass = "Car_DLV";
		hasDriver = -1;
	};
	class I_MRAP_03_hmg_F;
	class I_MRAP_03_hmg_F_DLV: I_MRAP_03_hmg_F
	{
		scopeArsenal = 1;
		vehicleClass = "Car_DLV";
		hasDriver = -1;
	};
	class I_MRAP_03_gmg_F;
	class I_MRAP_03_gmg_F_DLV: I_MRAP_03_gmg_F
	{
		scopeArsenal = 1;
		vehicleClass = "Car_DLV";
		hasDriver = -1;
	};
	class B_APC_Wheeled_01_cannon_F;
	class B_APC_Wheeled_01_cannon_F_DLV: B_APC_Wheeled_01_cannon_F
	{
		scopeArsenal = 1;
		vehicleClass = "Armored_DLV";
		hasDriver = -1;
	};
	class O_APC_Wheeled_02_rcws_F;
	class O_APC_Wheeled_02_rcws_F_DLV: O_APC_Wheeled_02_rcws_F
	{
		scopeArsenal = 1;
		vehicleClass = "Armored_DLV";
		hasDriver = -1;
	};
	class O_APC_Wheeled_02_rcws_v2_F;
	class O_APC_Wheeled_02_rcws_v2_F_DLV: O_APC_Wheeled_02_rcws_v2_F
	{
		scopeArsenal = 1;
		vehicleClass = "Armored_DLV";
		hasDriver = -1;
	};
	class I_APC_tracked_03_cannon_F;
	class I_APC_tracked_03_cannon_F_DLV: I_APC_tracked_03_cannon_F
	{
		scopeArsenal = 1;
		vehicleClass = "Armored_DLV";
		hasDriver = -1;
	};
	class I_MBT_03_cannon_F;
	class I_MBT_03_cannon_F_DLV: I_MBT_03_cannon_F
	{
		scopeArsenal = 1;
		vehicleClass = "Armored_DLV";
		hasDriver = -1;
	};
	class B_MBT_01_TUSK_F;
	class B_MBT_01_TUSK_F_DLV: B_MBT_01_TUSK_F
	{
		scopeArsenal = 1;
		vehicleClass = "Armored_DLV";
		hasDriver = -1;
	};
	class I_APC_Wheeled_03_cannon_F;
	class I_APC_Wheeled_03_cannon_F_DLV: I_APC_Wheeled_03_cannon_F
	{
		scopeArsenal = 1;
		vehicleClass = "Armored_DLV";
		hasDriver = -1;
	};
	class B_T_LSV_01_armed_F;
	class B_T_LSV_01_armed_F_DLV: B_T_LSV_01_armed_F
	{
		scopeArsenal = 1;
		vehicleClass = "Car_DLV";
		hasDriver = -1;
	};
	class B_T_LSV_01_armed_CTRG_F;
	class B_T_LSV_01_armed_CTRG_F_DLV: B_T_LSV_01_armed_CTRG_F
	{
		scopeArsenal = 1;
		vehicleClass = "Car_DLV";
		hasDriver = -1;
	};
	class B_LSV_01_armed_F;
	class B_LSV_01_armed_F_DLV: B_LSV_01_armed_F
	{
		scopeArsenal = 1;
		vehicleClass = "Car_DLV";
		hasDriver = -1;
	};
	class B_LSV_01_armed_black_F;
	class B_LSV_01_armed_black_F_DLV: B_LSV_01_armed_black_F
	{
		scopeArsenal = 1;
		vehicleClass = "Car_DLV";
		hasDriver = -1;
	};
	class B_LSV_01_armed_olive_F;
	class B_LSV_01_armed_olive_F_DLV: B_LSV_01_armed_olive_F
	{
		scopeArsenal = 1;
		vehicleClass = "Car_DLV";
		hasDriver = -1;
	};
	class B_LSV_01_armed_sand_F;
	class B_LSV_01_armed_sand_F_DLV: B_LSV_01_armed_sand_F
	{
		scopeArsenal = 1;
		vehicleClass = "Car_DLV";
		hasDriver = -1;
	};
	class B_T_LSV_01_armed_black_F;
	class B_T_LSV_01_armed_black_F_DLV: B_T_LSV_01_armed_black_F
	{
		scopeArsenal = 1;
		vehicleClass = "Car_DLV";
		hasDriver = -1;
	};
	class B_T_LSV_01_armed_olive_F;
	class B_T_LSV_01_armed_olive_F_DLV: B_T_LSV_01_armed_olive_F
	{
		scopeArsenal = 1;
		vehicleClass = "Car_DLV";
		hasDriver = -1;
	};
	class B_T_LSV_01_armed_sand_F;
	class B_T_LSV_01_armed_sand_F_DLV: B_T_LSV_01_armed_sand_F
	{
		scopeArsenal = 1;
		vehicleClass = "Car_DLV";
		hasDriver = -1;
	};
	class O_T_LSV_02_armed_F;
	class O_T_LSV_02_armed_F_DLV: O_T_LSV_02_armed_F
	{
		scopeArsenal = 1;
		vehicleClass = "Car_DLV";
		hasDriver = -1;
	};
	class O_T_LSV_02_armed_viper_F;
	class O_T_LSV_02_armed_viper_F_DLV: O_T_LSV_02_armed_viper_F
	{
		scopeArsenal = 1;
		vehicleClass = "Car_DLV";
		hasDriver = -1;
	};
	class O_LSV_02_armed_F;
	class O_LSV_02_armed_F_DLV: O_LSV_02_armed_F
	{
		scopeArsenal = 1;
		vehicleClass = "Car_DLV";
		hasDriver = -1;
	};
	class O_LSV_02_armed_viper_F;
	class O_LSV_02_armed_viper_F_DLV: O_LSV_02_armed_viper_F
	{
		scopeArsenal = 1;
		vehicleClass = "Car_DLV";
		hasDriver = -1;
	};
	class O_T_LSV_02_armed_black_F;
	class O_T_LSV_02_armed_black_F_DLV: O_T_LSV_02_armed_black_F
	{
		scopeArsenal = 1;
		vehicleClass = "Car_DLV";
		hasDriver = -1;
	};
	class O_T_LSV_02_armed_ghex_F;
	class O_T_LSV_02_armed_ghex_F_DLV: O_T_LSV_02_armed_ghex_F
	{
		scopeArsenal = 1;
		vehicleClass = "Car_DLV";
		hasDriver = -1;
	};
	class O_T_LSV_02_armed_arid_F;
	class O_T_LSV_02_armed_arid_F_DLV: O_T_LSV_02_armed_arid_F
	{
		scopeArsenal = 1;
		vehicleClass = "Car_DLV";
		hasDriver = -1;
	};
	class O_LSV_02_armed_black_F;
	class O_LSV_02_armed_black_F_DLV: O_LSV_02_armed_black_F
	{
		scopeArsenal = 1;
		vehicleClass = "Car_DLV";
		hasDriver = -1;
	};
	class O_LSV_02_armed_ghex_F;
	class O_LSV_02_armed_ghex_F_DLV: O_LSV_02_armed_ghex_F
	{
		scopeArsenal = 1;
		vehicleClass = "Car_DLV";
		hasDriver = -1;
	};
	class O_LSV_02_armed_arid_F;
	class O_LSV_02_armed_arid_F_DLV: O_LSV_02_armed_arid_F
	{
		scopeArsenal = 1;
		vehicleClass = "Car_DLV";
		hasDriver = -1;
	};
	class B_T_MRAP_01_gmg_F;
	class B_T_MRAP_01_gmg_F_DLV: B_T_MRAP_01_gmg_F
	{
		scopeArsenal = 1;
		vehicleClass = "Car_DLV";
		hasDriver = -1;
	};
	class B_T_MRAP_01_hmg_F;
	class B_T_MRAP_01_hmg_F_DLV: B_T_MRAP_01_hmg_F
	{
		scopeArsenal = 1;
		vehicleClass = "Car_DLV";
		hasDriver = -1;
	};
	class O_T_MRAP_02_hmg_ghex_F;
	class O_T_MRAP_02_hmg_ghex_F_DLV: O_T_MRAP_02_hmg_ghex_F
	{
		scopeArsenal = 1;
		vehicleClass = "Car_DLV";
		hasDriver = -1;
	};
	class O_T_MRAP_02_gmg_ghex_F;
	class O_T_MRAP_02_gmg_ghex_F_DLV: O_T_MRAP_02_gmg_ghex_F
	{
		scopeArsenal = 1;
		vehicleClass = "Car_DLV";
		hasDriver = -1;
	};
	class B_T_APC_Tracked_01_AA_F;
	class B_T_APC_Tracked_01_AA_F_DLV: B_T_APC_Tracked_01_AA_F
	{
		scopeArsenal = 1;
		vehicleClass = "Armored_DLV";
		hasDriver = -1;
	};
	class B_T_APC_Tracked_01_CRV_F;
	class B_T_APC_Tracked_01_CRV_F_DLV: B_T_APC_Tracked_01_CRV_F
	{
		scopeArsenal = 1;
		vehicleClass = "Support_DLV";
		hasDriver = -1;
	};
	class B_T_APC_Tracked_01_rcws_F;
	class B_T_APC_Tracked_01_rcws_F_DLV: B_T_APC_Tracked_01_rcws_F
	{
		scopeArsenal = 1;
		vehicleClass = "Armored_DLV";
		hasDriver = -1;
	};
	class O_T_APC_Tracked_02_cannon_ghex_F;
	class O_T_APC_Tracked_02_cannon_ghex_F_DLV: O_T_APC_Tracked_02_cannon_ghex_F
	{
		scopeArsenal = 1;
		vehicleClass = "Armored_DLV";
		hasDriver = -1;
	};
	class O_T_APC_Tracked_02_AA_ghex_F;
	class O_T_APC_Tracked_02_AA_ghex_F_DLV: O_T_APC_Tracked_02_AA_ghex_F
	{
		scopeArsenal = 1;
		vehicleClass = "Armored_DLV";
		hasDriver = -1;
	};
	class B_T_APC_Wheeled_01_cannon_F;
	class B_T_APC_Wheeled_01_cannon_F_DLV: B_T_APC_Wheeled_01_cannon_F
	{
		scopeArsenal = 1;
		vehicleClass = "Armored_DLV";
		hasDriver = -1;
	};
	class O_T_APC_Wheeled_02_rcws_ghex_F;
	class O_T_APC_Wheeled_02_rcws_ghex_F_DLV: O_T_APC_Wheeled_02_rcws_ghex_F
	{
		scopeArsenal = 1;
		vehicleClass = "Armored_DLV";
		hasDriver = -1;
	};
	class O_T_APC_Wheeled_02_rcws_v2_ghex_F;
	class O_T_APC_Wheeled_02_rcws_v2_ghex_F_DLV: O_T_APC_Wheeled_02_rcws_v2_ghex_F
	{
		scopeArsenal = 1;
		vehicleClass = "Armored_DLV";
		hasDriver = -1;
	};
	class B_T_MBT_01_arty_F;
	class B_T_MBT_01_arty_F_DLV: B_T_MBT_01_arty_F
	{
		scopeArsenal = 1;
		vehicleClass = "Armored_DLV";
		hasDriver = -1;
	};
	class B_T_MBT_01_mlrs_F;
	class B_T_MBT_01_mlrs_F_DLV: B_T_MBT_01_mlrs_F
	{
		scopeArsenal = 1;
		vehicleClass = "Armored_DLV";
		hasDriver = -1;
	};
	class B_T_MBT_01_cannon_F;
	class B_T_MBT_01_cannon_F_DLV: B_T_MBT_01_cannon_F
	{
		scopeArsenal = 1;
		vehicleClass = "Armored_DLV";
		hasDriver = -1;
	};
	class B_T_MBT_01_TUSK_F;
	class B_T_MBT_01_TUSK_F_DLV: B_T_MBT_01_TUSK_F
	{
		scopeArsenal = 1;
		vehicleClass = "Armored_DLV";
		hasDriver = -1;
	};
	class O_T_MBT_02_cannon_ghex_F;
	class O_T_MBT_02_cannon_ghex_F_DLV: O_T_MBT_02_cannon_ghex_F
	{
		scopeArsenal = 1;
		vehicleClass = "Armored_DLV";
		hasDriver = -1;
	};
	class O_T_MBT_02_arty_ghex_F;
	class O_T_MBT_02_arty_ghex_F_DLV: O_T_MBT_02_arty_ghex_F
	{
		scopeArsenal = 1;
		vehicleClass = "Armored_DLV";
		hasDriver = -1;
	};
	class B_APC_Wheeled_03_cannon_F;
	class B_APC_Wheeled_03_cannon_F_DLV: B_APC_Wheeled_03_cannon_F
	{
		scopeArsenal = 1;
		vehicleClass = "Armored_DLV";
		hasDriver = -1;
	};
};
class CfgVehicleClasses
{
	class Armored_DLV
	{
		displayName = "Armored (DriverLess)";
	};
	class Support_DLV
	{
		displayName = "Supports (DriverLess)";
	};
	class Car_DLV
	{
		displayName = "Cars (DriverLess)";
	};
};
