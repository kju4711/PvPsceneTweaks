///////////////////////////////////////////////////////////////////////////////
#                                                                             #
#                               PvPscene Tweaks                               #
#                                                                             #
///////////////////////////////////////////////////////////////////////////////


#==============#
| Release date |
#==============#

2017-07-24

#===========#
| Component |
#===========#

xboxStyleAICommanding_Controls_C_PvPscene

#================#
| Main author(s) |
#================#

kju

#===============#
| Local credits |
#===============#

None

#=============================#
| Rough component description |
#=============================#

Changes the AI command menu to the (supposedly) OFP:Elite system.

Beware of the limitations and issues first:
-------------------------------------------
* It does NOT display a command bar (at the bottom) when the menu is closed.
* You CANNOT directly select units with the F-keys - you need to open the menu first.
* You CANNOT select a set of units with the menu open - only one or all.
* The "Tactial view" key action does the same as "Toggle view" - to get to the
  commander view you need to press "Select all units" while in third person view.
* When you have more than 18 units under your command, the first menu has a new
  entry called "select units" that gets you to a submenu with all units listed.
* You are probably better of with rebinding the follow key actions in controls:
** "Use selected action" (apply command/enter submenu)
** "Back" (close submenu/exit command menu)
** "Commanding mode" (here: open context sensitive menu)
** "Select" (here: basically broken)
** The backspace key seems to be hardcoded to close submenu/exit command menu.

Benefits of the system:
-----------------------
* No permanent, disturbing, command bar at the lower half of the screen.
* Somewhat the OFP like complex command menu back by pressing "Select all units"
  twice and use numbers or mouse wheel plus "Use selected action" to issue orders.
  To send out commands to all units simple and straight forward.
* Units in vehicles are group to one unit in the first menu level. Easy to use
  commands to the vehicle by selecting them with "Select all units", select the
  vehicle with the mouse wheel and "Use selected action" and issue orders as above.
* You can use the mouse and hold left mouse button (use selected action?) to draw
  a rectangle to select the units inside.
* You see the complex command menu by default and the commands of the currently
  selected submenu. Especially for those who do not know them by heart from OFP,
  this may be a big benefit.
* You can still use the context sensitive commands. Either by "Select all units"
  and "Use selected action" or open the context sensitive menu at any point with
  the "Command mode" key action.

#=================#
| Contact details |
#=================#

Mail:	pvpscene@web.de



///////////////////////////////////////////////////////////////////////////////
#                                                                             #
#                               PvPscene Tweaks                               #
#                                                                             #
///////////////////////////////////////////////////////////////////////////////
//                                                                           //
#     Creative Commons License                                                #
//                                                                           //
#     License   http://creativecommons.org/licenses/by-nc-sa/3.0/de/deed.en   #
#     Title     PvPscene Projects FILE                                        #
#     By        PvPscene                                                      #
//                                                                           //
#     Is licensed under a Creative Commons                                    #
#     Attribution-Non-Commercial-Share Alike 3.0 License                      #
//                                                                           //
///////////////////////////////////////////////////////////////////////////////