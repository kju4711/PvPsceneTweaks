#include "globalDefines.hpp"

disableSerialization;

private ["_currentDisplay","_heightControl","_heightBackgroundControl","_headingControl","_headingBackgroundControl","_gridControl","_gridBackgroundControl"];

_currentDisplay = (findDisplay PVPSCENE_IDD_FULLSCREENMAP);

if (shownCompass && ("ItemCompass" in (items player + assignedItems player))) then
{
	_headingControl = _currentDisplay displayCtrl (PVPSCENE_IDC_HEADINGTEXT);
	_headingControl ctrlSetText (str (round (getDir player)));

	_headingBackgroundControl = _currentDisplay displayCtrl (PVPSCENE_IDC_HEADINGBACKGROUND);
	_headingBackgroundControl ctrlSetBackgroundColor [0,0,0,1];
};

if (shownGPS && ("ItemGPS" in (items player + assignedItems player))) then
{
	_heightControl = _currentDisplay displayCtrl (PVPSCENE_IDC_HEIGHTTEXT);
	_heightControl ctrlSetText (str ((round (((getPosASL player) select 2))) + PvPscene_ElevationOffset));

	_heightBackgroundControl = _currentDisplay displayCtrl (PVPSCENE_IDC_HEIGHTBACKGROUND);
	_heightBackgroundControl ctrlSetBackgroundColor [0,0,0,1];

	_gridControl = _currentDisplay displayCtrl (PVPSCENE_IDC_GRIDTEXT);
	_gridControl ctrlSetText (mapGridPosition player);

	_gridBackgroundControl = _currentDisplay displayCtrl (PVPSCENE_IDC_GRIDBACKGROUND);
	_gridBackgroundControl ctrlSetBackgroundColor [0,0,0,1];
};
