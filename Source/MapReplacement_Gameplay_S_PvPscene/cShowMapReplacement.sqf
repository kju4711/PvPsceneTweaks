#include "globalDefines.hpp"

disableSerialization;

private ["_showMap"];

if (isNil "PvPscene_MapReplacementActiveForAll") then {PvPscene_MapReplacementActiveForAll = false;};

_showMap = switch (true) do
{
	case (PvPscene_MapReplacementActiveForAll): {true};
	case (player getVariable ["PvPscene_MapReplacementActiveForUnit",false]): {true};
	case (!(shownMap)): {false};
	case ("ItemMap" in (items player + assignedItems player)): {true};
	case (("ItemGPS" in (items player + assignedItems player)) && shownGPS): {true};
	default {false};
};

if (_showMap) then
{
	private ["_fullscreenMapControl"];
	PvPscene_InMapView = true;

	createDialog "PvPscene_MapReplacement";

	PvPscene_KeyDownMapReplacement = (findDisplay PVPSCENE_IDD_FULLSCREENMAP) displayAddEventHandler ["KeyDown","_this call PvPscene_fnc_cKeyDownMapReplacement"];

	if (isNil "PvPscene_MapPosition") then
	{
		PvPscene_MapScale = 0.19;
		PvPscene_MapPosition = position player;
	};

	_fullscreenMapControl = (findDisplay PVPSCENE_IDD_FULLSCREENMAP) displayCtrl PVPSCENE_IDC_FULLSCREENMAP;
	_fullscreenMapControl ctrlMapAnimAdd [0,PvPscene_MapScale,PvPscene_MapPosition];
	ctrlMapAnimCommit _fullscreenMapControl;
	_fullscreenMapControl ctrlCommit 0;
	waitUntil {ctrlCommitted _fullscreenMapControl};

	[] spawn PvPscene_fnc_cRefreshMapReplacementUIElements;
};