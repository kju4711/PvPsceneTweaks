#include "globalDefines.hpp"

//#include "RscMapControl.hpp"

class RscMapControl;

class PvPscene_Text
{
	type = 0;
	idc = -1;
	style = 0;
	colorText[] = {1,1,1,1};
	colorBackground[] = {0,0,0,0};
	font = "Bitstream";
	sizeEx = 0.025;
	w = 0.275;
	h = 0.04;
	text = "";
};
class PvPscene_LargeText: PvPscene_Text
{
	style = 2;
	sizeEx = 0.029;
};
class PvPscene_Background: PvPscene_Text
{
	//colorBackground[] = {0,0,0,1};
};

class PvPscene_MapReplacement
{
	idd = PVPSCENE_IDD_FULLSCREENMAP;
	movingEnable = false;
	controlsBackGround[] =
	{
		FullScreenMap
	};
	controls[] =
	{
		HeightBackground,
		HeightText,
		HeadingBackground,
		HeadingText,
		GridBackground,
		GridText
	};
	class FullScreenMap: RscMapControl
	{
		idc = PVPSCENE_IDC_FULLSCREENMAP;
		x = "SafeZoneX";
		y = "SafeZoneY";
		w = "SafeZoneW";
		h = "SafeZoneH";
		maxSatelliteAlpha = 1;//0.75;
		alphaFadeStartScale = 2;//0.15;
		alphaFadeEndScale = 3;//0.29;
	};
	class HeightBackground: PvPscene_Background
	{
		idc = PVPSCENE_IDC_HEIGHTBACKGROUND;
		x = "(38/100)	* SafeZoneW + SafeZoneX";
		y = "(2/100)	* SafeZoneH + SafeZoneY";
		w = "(6/100)	* SafeZoneW";
		h = "(3/100)	* SafeZoneH";
	};
	class HeightText: PvPscene_LargeText
	{
		idc = PVPSCENE_IDC_HEIGHTTEXT;
		x = "(38/100)	* SafeZoneW + SafeZoneX";
		y = "(2/100)	* SafeZoneH + SafeZoneY";
		w = "(6/100)	* SafeZoneW";
		h = "(3/100)	* SafeZoneH";
	};
	class HeadingBackground: PvPscene_Background
	{
		idc = PVPSCENE_IDC_HEADINGBACKGROUND;
		x = "(47/100)	* SafeZoneW + SafeZoneX";
		y = "(2/100)	* SafeZoneH + SafeZoneY";
		w = "(6/100)	* SafeZoneW";
		h = "(3/100)	* SafeZoneH";
	};
	class HeadingText: PvPscene_LargeText
	{
		idc = PVPSCENE_IDC_HEADINGTEXT;
		x = "(47/100)	* SafeZoneW + SafeZoneX";
		y = "(2/100)	* SafeZoneH + SafeZoneY";
		w = "(6/100)	* SafeZoneW";
		h = "(3/100)	* SafeZoneH";
	};
	class GridBackground: PvPscene_Background
	{
		idc = PVPSCENE_IDC_GRIDBACKGROUND;
		x = "(56/100)	* SafeZoneW + SafeZoneX";
		y = "(2/100)	* SafeZoneH + SafeZoneY";
		w = "(6/100)	* SafeZoneW";
		h = "(3/100)	* SafeZoneH";
	};
	class GridText: PvPscene_LargeText
	{
		idc = PVPSCENE_IDC_GRIDTEXT;
		x = "(56/100)	* SafeZoneW + SafeZoneX";
		y = "(2/100)	* SafeZoneH + SafeZoneY";
		w = "(6/100)	* SafeZoneW";
		h = "(3/100)	* SafeZoneH";
	};
};