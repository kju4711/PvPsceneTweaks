//PvPscene_fnc_cKeyDownMainDisplay = compile preprocessFileLineNumbers "cKeyDownMainDisplay.sqf";
//PvPscene_fnc_cKeyDownMapReplacement = compile preprocessFileLineNumbers "cKeyDownMapReplacement.sqf";
//PvPscene_fnc_cShowMapReplacement = compile preprocessFileLineNumbers "cShowMapReplacement.sqf";
//PvPscene_fnc_cRefreshMapReplacementUIElements = compile preprocessFileLineNumbers "cRefreshMapReplacementUIElements.sqf";
//PvPscene_fnc_cDeadHandler = compile preprocessFileLineNumbers "cDeadHandler.sqf";
//PvPscene_fnc_initRespawn = compile preprocessFileLineNumbers "initRespawn.sqf";

PvPscene_fnc_cKeyDownMainDisplay = compile preprocessFileLineNumbers "\MapReplacement_Gameplay_S_PvPscene\cKeyDownMainDisplay.sqf";
PvPscene_fnc_cKeyDownMapReplacement = compile preprocessFileLineNumbers "\MapReplacement_Gameplay_S_PvPscene\cKeyDownMapReplacement.sqf";
PvPscene_fnc_cShowMapReplacement = compile preprocessFileLineNumbers "\MapReplacement_Gameplay_S_PvPscene\cShowMapReplacement.sqf";
PvPscene_fnc_cRefreshMapReplacementUIElements = compile preprocessFileLineNumbers "\MapReplacement_Gameplay_S_PvPscene\cRefreshMapReplacementUIElements.sqf";
PvPscene_fnc_cDeadHandler = compile preprocessFileLineNumbers "\MapReplacement_Gameplay_S_PvPscene\cDeadHandler.sqf";
PvPscene_fnc_initRespawn = compile preprocessFileLineNumbers "\MapReplacement_Gameplay_S_PvPscene\initRespawn.sqf";

PvPscene_ElevationOffset = getNumber (configFile/"CfgWorlds"/worldName/"elevationOffset");
if (isNil "PvPscene_ElevationOffset") then {PvPscene_ElevationOffset = 0;};

[] spawn
{
	sleep 1;

	[] spawn PvPscene_fnc_initRespawn;
};