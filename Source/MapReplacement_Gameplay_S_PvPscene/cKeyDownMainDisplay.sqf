if (PvPscene_InMapView || visibleMap || (!(alive player))) exitWith {false};

private["_keypressed","_keypressedArray","_pressedShift","_pressedControl","_pressedAlt","_return"];

_keypressed = _this select 1;
_pressedShift = _this select 2;
_pressedControl = _this select 3;
_pressedAlt = _this select 4;

_keypressedArray = [_keypressed];
_return = false;

if (({_x in _keypressedArray} count (actionKeys "ShowMap")) > 0) then
{
	if (_pressedShift || _pressedControl || _pressedAlt) then
	{
		[] spawn PvPscene_fnc_cShowMapReplacement;
		_return = true;
	};
};
_return;