#include "globalDefines.hpp"

waitUntil {sleep 0.1; alive player};
waitUntil {sleep 0.1; (!(alive player))};

closeDialog PVPSCENE_IDD_FULLSCREENMAP;

[] spawn
{
	sleep 1;

	[] spawn PvPscene_fnc_initRespawn;
};