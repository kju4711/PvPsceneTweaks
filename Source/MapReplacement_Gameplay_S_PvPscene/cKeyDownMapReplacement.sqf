﻿#include "globalDefines.hpp"

if (!(PvPscene_InMapView)) exitWith {false};

disableSerialization;

private["_keypressed","_keypressedArray","_return","_found"];

_keypressed = _this select 1;

_keypressedArray = [_keypressed];
_return = false;
_found = false;

#define KEYSCODE_ESC_KEY	1
//#define KEYSCODE_M_KEY		50

switch (_keypressed) do
{
	case KEYSCODE_ESC_KEY:
	{
		_found = true;
	};
//	case KEYSCODE_M_KEY:
//	{
//		_found = true;
//	};
};
if (!(_return)) then
{
	if (({_x in _keypressedArray} count (actionKeys "HideMap")) > 0) then
	{
		_found = true;
	};
};

if (_found) then
{
	private["_mapControl"];
	PvPscene_InMapView = false;
	(findDisplay PVPSCENE_IDD_FULLSCREENMAP) displayRemoveEventHandler ["KeyDown",PvPscene_KeyDownMapReplacement];

	_mapControl = (findDisplay PVPSCENE_IDD_FULLSCREENMAP) displayCtrl PVPSCENE_IDC_FULLSCREENMAP;
	PvPscene_MapScale = ctrlMapScale _mapControl;
	PvPscene_MapPosition = _mapControl ctrlMapScreenToWorld [0.5,0.5];

	closeDialog PVPSCENE_IDD_FULLSCREENMAP;
	_return = true;
};

_return;