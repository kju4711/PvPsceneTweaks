class CfgPatches
{
	class RemovedVehicleCursors_Gameplay_C_PvPscene
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Data_F_Loadorder","A3_Data_F_Curator_Loadorder","A3_Data_F_Kart_Loadorder","A3_Data_F_Bootcamp_Loadorder","A3_Data_F_Heli_Loadorder","A3_Data_F_Mark_Loadorder","A3_Data_F_Exp_A_Loadorder","A3_Data_F_Exp_B_Loadorder","A3_Data_F_Exp_Loadorder","A3_Data_F_Jets_Loadorder","A3_Data_F_Argo_Loadorder","A3_Data_F_Patrol_Loadorder","A3_Data_F_Orange_Loadorder","A3_Data_F_Tacops_Loadorder","A3_Data_F_Tank_Loadorder"};
		version = "2019-03-28";
	};
};
class CfgWeapons
{
	class Binocular;
	class CannonCore;
	class Default;
	class LauncherCore;
	class LMG_Minigun;
	class LMG_RCWS;
	class MGunCore;

	class RocketPods: LauncherCore
	{
//		cursor = "EmptyCursor";
		cursorAim = "EmptyCursor";//"rocket";
	};
	class MGun: MGunCore
	{
//		cursor = "EmptyCursor";
		cursoraim = "EmptyCursor";//"mg";
	};
	class LMG_Minigun_heli: LMG_Minigun
	{
//		showAimCursorInternal = 0;
	};
	class HMG_127: LMG_RCWS
	{
//		showAimCursorInternal = 0;
	};
	class HMG_01: HMG_127
	{
//		showAimCursorInternal = 1;
	};
	class HMG_NSVT: HMG_127
	{
//		showAimCursorInternal = 0;
	};
	class M134_minigun: MGunCore
	{
//		cursor = "EmptyCursor";
		cursorAim = "EmptyCursor";//"mg";
	};
	class mortar_82mm: CannonCore
	{
		cursor = "EmptyCursor";//"mortar";
//		cursorAim = "EmptyCursor";
	};
	class MissileLauncher: LauncherCore
	{
//		cursor = "EmptyCursor";
		cursorAim = "EmptyCursor";//"missile";
	};
	class missiles_DAGR: RocketPods
	{
//		cursor = "EmptyCursor";
		cursorAim = "EmptyCursor";//"missile";
//		showAimCursorInternal = 1;
	};
	class missiles_DAR: RocketPods
	{
//		cursor = "EmptyCursor";
		cursorAim = "EmptyCursor";//"rocket";
	};
	class GMG_F: MGun
	{
//		cursor = "EmptyCursor";
		cursorAim = "EmptyCursor";//"gl";
	};
	class GMG_40mm: GMG_F
	{
//		showAimCursorInternal = 0;
	};
	class Laserdesignator_mounted: Default
	{
		cursor = "EmptyCursor";//"laserDesignator";
//		cursorAim = "EmptyCursor";
		cursorAimOn = "EmptyCursor";//"CursorAimOn";
//		showAimCursorInternal = 0;
	};
	class autocannon_Base_F: CannonCore
	{
//		cursor = "EmptyCursor";
		cursorAim = "EmptyCursor";//"cannon";
//		showAimCursorInternal = 0;
	};
	class gatling_20mm: CannonCore
	{
//		cursor = "EmptyCursor";
		cursorAim = "EmptyCursor";//"mg";
	};
	class gatling_30mm_base: CannonCore
	{
//		cursor = "EmptyCursor";
		cursorAim = "EmptyCursor";//"mg";
	};
	class missiles_ASRAAM: MissileLauncher
	{
//		showAimCursorInternal = 0;
	};
	class missiles_SCALPEL: RocketPods
	{
//		cursor = "EmptyCursor";
		cursorAim = "EmptyCursor";//"missile";
//		showAimCursorInternal = 0;
	};
	class SEARCHLIGHT: MGun
	{
//		cursorAim = "EmptyCursor";
//		cursor = "EmptyCursor";
	};
	class cannon_120mm: CannonCore
	{
//		cursor = "EmptyCursor";
		cursorAim = "EmptyCursor";//"cannon";
	};
	class cannon_125mm: CannonCore
	{
//		cursor = "EmptyCursor";
		cursorAim = "EmptyCursor";//"cannon";
	};
	class cannon_105mm: CannonCore
	{
//		cursor = "EmptyCursor";
		cursorAim = "EmptyCursor";//"cannon";
	};
	class gatling_25mm: CannonCore
	{
//		cursor = "EmptyCursor";
		cursorAim = "EmptyCursor";//"mg";
	};
	class autocannon_35mm: CannonCore
	{
//		cursor = "EmptyCursor";
		cursorAim = "EmptyCursor";//"mg";
	};
	class mortar_155mm_AMOS: CannonCore
	{
		cursor = "EmptyCursor";//"mortar";
//		cursorAim = "EmptyCursor";
	};
	class missiles_Zephyr: MissileLauncher
	{
//		showAimCursorInternal = 0;
	};
	class GBU12BombLauncher: RocketPods
	{
		cursorAim = "EmptyCursor";//"bomb";
//		showAimCursorInternal = 0;
	};
	class Mk82BombLauncher: RocketPods
	{
		cursorAim = "EmptyCursor";//"bomb";
//		showAimCursorInternal = 0;
	};
	class rockets_230mm_GAT: RocketPods
	{
//		cursorAim = "EmptyCursor";
	};
	class Twin_Cannon_20mm: gatling_20mm
	{
//		showAimCursorInternal = 0;
	};
	class Gatling_30mm_Plane_CAS_01_F: CannonCore
	{
//		cursor = "EmptyCursor";
		cursorAim = "EmptyCursor";//"mg";
//		showAimCursorInternal = 0;
	};
	class Missile_AA_04_Plane_CAS_01_F: RocketPods
	{
//		cursor = "EmptyCursor";
		cursorAim = "EmptyCursor";//"missile";
//		showAimCursorInternal = 0;
	};
	class Missile_AGM_02_Plane_CAS_01_F: MissileLauncher
	{
//		showAimCursorInternal = 0;
	};
	class Rocket_04_HE_Plane_CAS_01_F: RocketPods
	{
//		showAimCursorInternal = 0;
	};
	class Bomb_04_Plane_CAS_01_F: RocketPods
	{
		cursorAim = "EmptyCursor";//"bomb";
//		showAimCursorInternal = 0;
	};
	class Cannon_30mm_Plane_CAS_02_F: CannonCore
	{
//		cursor = "EmptyCursor";
		cursorAim = "EmptyCursor";//"mg";
//		showAimCursorInternal = 0;
	};
	class Laserdesignator: Binocular
	{
		cursor = "EmptyCursor";//"laserDesignator";
//		cursorAim = "EmptyCursor";
//		cursorAimOn = "EmptyCursor";
	};
	class weapon_Cannon_Phalanx: CannonCore
	{
//		cursor = "EmptyCursor";
		cursorAim = "EmptyCursor";//"mg";
	};
	class weapon_Fighter_Gun20mm_AA: CannonCore
	{
//		cursor = "EmptyCursor";
		cursorAim = "EmptyCursor";//"mg";
//		showAimCursorInternal = 0;
	};
	class weapon_AMRAAMLauncher: MissileLauncher
	{
//		showAimCursorInternal = 0;
	};
	class weapon_BIM9xLauncher: MissileLauncher
	{
//		cursor = "EmptyCursor";
//		showAimCursorInternal = 0;
	};
	class weapon_AGM_65Launcher: RocketPods
	{
//		cursor = "EmptyCursor";
		cursorAim = "EmptyCursor";//"missile";
//		showAimCursorInternal = 0;
	};
	class weapon_GBU12Launcher: RocketPods
	{
//		cursor = "EmptyCursor";
		cursorAim = "EmptyCursor";//"bomb";
//		showAimCursorInternal = 0;
	};
	class weapon_Fighter_Gun_30mm: CannonCore
	{
//		cursor = "EmptyCursor";
		cursorAim = "EmptyCursor";//"mg";
//		showAimCursorInternal = 0;
	};
	class weapon_R73Launcher: MissileLauncher
	{
//		cursor = "EmptyCursor";
		cursorAim = "EmptyCursor";//"missile";
//		showAimCursorInternal = 0;
	};
	class weapon_R77Launcher: MissileLauncher
	{
//		cursor = "EmptyCursor";
		cursorAim = "EmptyCursor";//"missile";
//		showAimCursorInternal = 0;
	};
	class weapon_AGM_KH25Launcher: MissileLauncher
	{
//		cursor = "EmptyCursor";
		cursorAim = "EmptyCursor";//"missile";
//		showAimCursorInternal = 0;
	};
	class weapon_KAB250Launcher: RocketPods
	{
//		cursor = "EmptyCursor";
		cursorAim = "EmptyCursor";//"bomb";
//		showAimCursorInternal = 0;
	};
};
class CfgInGameUI
{
	class Cursor
	{
		weapon = "A3\Weapons_F\Data\clear_empty.paa";
	};
};
