class CfgPatches
{
	class Optional_HideFromEditor_TerrainObjects_c
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Data_F_Loadorder","A3_Data_F_Curator_Loadorder","A3_Data_F_Kart_Loadorder","A3_Data_F_Bootcamp_Loadorder","A3_Data_F_Heli_Loadorder","A3_Data_F_Mark_Loadorder","A3_Data_F_Exp_A_Loadorder","A3_Data_F_Exp_B_Loadorder","A3_Data_F_Exp_Loadorder","A3_Data_F_Jets_Loadorder","A3_Data_F_Argo_Loadorder","A3_Data_F_Patrol_Loadorder","A3_Data_F_Orange_Loadorder","A3_Data_F_Tacops_Loadorder","A3_Data_F_Tank_Loadorder","A3_Data_F_Destroyer_Loadorder","A3_Data_F_Sams_Loadorder"};
		version = "2019-03-28";
	};
};
class CfgVehicles
{
	class AirportBase;
	class AncientRelics_base_F;
	class AttachedSigns_base_F;
	class BagBunker_base_F;
	class BagFence_base_F;
	class BasaltRuins_base_F;
	class Billboard_02_base_F;
	class Billboard_03_base_F;
	class Billboard_04_base_F;
	class Bridge_PathLod_base_F;
	class Buoy_base_F;
	class CamoNet_BLUFOR_big_F;
	class CamoNet_BLUFOR_open_F;
	class CamoNet_ghex_big_F;
	class CamoNet_ghex_open_F;
	class CamoNet_INDP_big_F;
	class CamoNet_INDP_open_F;
	class CamoNet_OPFOR_big_F;
	class CamoNet_OPFOR_open_F;
	class Camping_base_F;
	class Cargo_base_F;
	class Cargo_House_base_F;
	class Cargo_HQ_base_F;
	class Cargo_Patrol_base_F;
	class Cargo_Tower_base_F;
	class Cemetery_base_F;
	class Church_F;
	class Church_Small_F;
	class Constructions_base_F;
	class Dead_base_F;
	class FloatingStructure_F;
	class Furniture_base_F;
	class Garbage_base_F;
	class Graffiti_base_F;
	class HBarrier_base_F;
	class Helipad_base_F;
	class Hostage_PopUp_Moving_90deg_F;
	class Hostage_PopUp_Moving_F;
	class Hostage_PopUp2_Moving_90deg_F;
	class Hostage_PopUp2_Moving_F;
	class Hostage_PopUp3_Moving_90deg_F;
	class Hostage_PopUp3_Moving_F;
	class House_F;
	class House_Small_F;
	class i_House_Big_01_b_base_F;
	class i_House_Big_02_b_base_F;
	class i_House_Small_01_b_base_F;
	class i_House_Small_02_b_base_F;
	class i_House_Small_02_c_base_F;
	class i_Shop_02_b_base_F;
	class i_Stone_House_Big_01_b_base_F;
	class i_Stone_Shed_01_b_base_F;
	class i_Stone_Shed_01_c_base_F;
	class IndPipe_base_F;
	class IndPipe_Small_base_F;
	class Industry_base_F;
	class Infostand_base_F;
	class Infrastructure_base_F;
	class Items_base_F;
	class Lamps_base_F;
	class Land_Destroyer_01_Boat_Rack_01_Base_F;
	class Land_DirtPatch_02_base_F;
	class Land_Lighthouse_03_base_F;
	class Land_RepairDepot_01_base_F;
	class Land_RepairDepot_01_base_ruins_F;
	class Land_Target_Oval_Wall_Bottom_F;
	class Land_Target_Oval_Wall_Left_F;
	class Land_Target_Oval_Wall_Right_F;
	class Land_Target_Oval_Wall_Top_F;
	class Land_u_House_Big_01_V1_F;
	class Land_u_House_Big_02_V1_F;
	class Land_u_House_Small_01_V1_F;
	class Land_u_House_Small_02_V1_F;
	class Land_u_Shop_01_V1_F;
	class Land_u_Shop_02_V1_F;
	class LaserTargetBase;
	class Leaflet_base_F;
	class LuggageHeap_base_F;
	class Market_base_F;
	class Metal_Pole_Skeet_F;
	class NonStrategic;
	class Pavements_base_F;
	class Piers_base_F;
	class PlayGround_base_F;
	class Poster_base_F;
	class PowerLines_base_F;
	class PowerLines_Small_base_F;
	class PowerLines_Wires_base_F;
	class RowBoats_base_F;
	class Ruins_F;
	class Shelter_base_F;
	class Signs_base_F;
	class SportsGrounds_base_F;
	class Stall_base_F;
	class Static;
	class StaticShip;
	class Statues_base_F;
	class Strategic;
	class Target_PopUp_Moving_90deg_Acc1_F;
	class Target_PopUp_Moving_90deg_Acc2_F;
	class Target_PopUp_Moving_90deg_F;
	class Target_PopUp_Moving_Acc1_F;
	class Target_PopUp_Moving_Acc2_F;
	class Target_PopUp_Moving_F;
	class Target_PopUp2_Moving_90deg_Acc1_F;
	class Target_PopUp2_Moving_90deg_Acc2_F;
	class Target_PopUp2_Moving_90deg_F;
	class Target_PopUp2_Moving_Acc1_F;
	class Target_PopUp2_Moving_Acc2_F;
	class Target_PopUp2_Moving_F;
	class Target_PopUp3_Moving_90deg_Acc1_F;
	class Target_PopUp3_Moving_90deg_Acc2_F;
	class Target_PopUp3_Moving_90deg_F;
	class Target_PopUp3_Moving_Acc1_F;
	class Target_PopUp3_Moving_Acc2_F;
	class Target_PopUp3_Moving_F;
	class TargetBase;
	class TargetP_Civ_F;
	class TargetP_Civ2_F;
	class TargetP_Civ3_F;
	class TargetP_Inf_Acc1_F;
	class TargetP_Inf_Acc2_F;
	class TargetP_Inf2_Acc1_F;
	class TargetP_Inf2_Acc2_F;
	class TargetP_Inf2_F;
	class TargetP_Inf3_Acc1_F;
	class TargetP_Inf3_Acc2_F;
	class TargetP_Inf3_F;
	class TargetP_Zom_Acc1_F;
	class TargetP_Zom_F;
	class TargetSoldierBase;
	class Thing;
	class ThingX;
	class Wall_F;
	class WallCity_01_4m_base_F;
	class WallCity_01_4m_plain_base_F;
	class WallCity_01_4m_plain_dmg_base_F;
	class WallCity_01_8m_base_F;
	class WallCity_01_8m_dmg_base_F;
	class WallCity_01_8m_plain_base_F;
	class WallCity_01_gate_base_F;
	class WallCity_01_pillar_base_F;
	class WallCity_01_pillar_plain_dmg_base_F;
	class Zombie_PopUp_Moving_90deg_Acc1_F;
	class Zombie_PopUp_Moving_90deg_F;
	class Zombie_PopUp_Moving_Acc1_F;
	class Zombie_PopUp_Moving_F;

	class Land_Bridge_01_PathLod_F: Bridge_PathLod_base_F
	{
//		scope = 1;
	};
	class Land_Bridge_Asphalt_PathLod_F: Bridge_PathLod_base_F
	{
//		scope = 1;
	};
	class Land_Bridge_Concrete_PathLod_F: Bridge_PathLod_base_F
	{
//		scope = 1;
	};
	class Land_Bridge_HighWay_PathLod_F: Bridge_PathLod_base_F
	{
//		scope = 1;
	};
	class Land_Loudspeakers_F: NonStrategic
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Sink_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Timbers_F: NonStrategic
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_TreeBin_F: NonStrategic
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Water_source_F: NonStrategic
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WoodPile_F: NonStrategic
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_AncientPillar_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_AncientPillar_damaged_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_AncientPillar_fallen_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BellTower_01_V1_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BellTower_01_V2_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BellTower_02_V1_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_BellTower_02_V1_F: Church_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BellTower_02_V2_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_BellTower_02_V2_F: Church_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Calvary_01_V1_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Calvary_02_V1_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Calvary_02_V2_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Campfire_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Campfire_burning_F: Land_Campfire_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Camping_Light_off_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CampingChair_V1_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CampingChair_V1_folded_F: Camping_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CampingChair_V2_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CampingTable_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CampingTable_small_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FieldToilet_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FirePlace_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class FirePlace_burning_F: Land_FirePlace_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Ground_sheet_F: Camping_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Ground_sheet_OPFOR_F: Land_Ground_sheet_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Ground_sheet_blue_F: Land_Ground_sheet_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Ground_sheet_khaki_F: Land_Ground_sheet_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Ground_sheet_yellow_F: Land_Ground_sheet_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Ground_sheet_folded_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Ground_sheet_folded_OPFOR_F: Land_Ground_sheet_folded_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Ground_sheet_folded_blue_F: Land_Ground_sheet_folded_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Ground_sheet_folded_khaki_F: Land_Ground_sheet_folded_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Ground_sheet_folded_yellow_F: Land_Ground_sheet_folded_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Pillow_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Pillow_camouflage_F: Land_Pillow_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Pillow_grey_F: Land_Pillow_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Pillow_old_F: Land_Pillow_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Sleeping_bag_F: Camping_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Sleeping_bag_blue_F: Land_Sleeping_bag_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Sleeping_bag_brown_F: Land_Sleeping_bag_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Sleeping_bag_folded_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Sleeping_bag_blue_folded_F: Land_Sleeping_bag_folded_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Sleeping_bag_brown_folded_F: Land_Sleeping_bag_folded_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Sun_chair_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Sun_chair_green_F: Land_Sun_chair_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Sunshade_F: Camping_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_TentA_F: Camping_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_TentDome_F: Camping_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ToiletBox_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Chapel_V1_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Chapel_V1_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Chapel_V2_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Chapel_V2_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Chapel_Small_V1_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Chapel_Small_V1_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Chapel_Small_V2_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Chapel_Small_V2_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Bricks_V1_F: Constructions_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Bricks_V2_F: Constructions_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Bricks_V3_F: Constructions_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Bricks_V4_F: Constructions_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CinderBlocks_F: Constructions_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Coil_F: Constructions_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ConcretePipe_F: Constructions_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_IronPipes_F: NonStrategic
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Pallet_F: Constructions_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Pallet_vertical_F: Constructions_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Pallets_F: NonStrategic
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Pipes_large_F: Constructions_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Pipes_small_F: Constructions_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Scaffolding_F: NonStrategic
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WheelCart_F: Constructions_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WoodenBox_F: Constructions_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WorkStand_F: Constructions_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Grave_memorial_F: Cemetery_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Grave_monument_F: Cemetery_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Grave_obelisk_F: Cemetery_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Grave_soldier_F: Cemetery_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Grave_V1_F: Cemetery_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Grave_V2_F: Cemetery_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Grave_V3_F: Cemetery_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HumanSkeleton_F: Dead_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HumanSkull_F: Dead_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Garbage_line_F: Garbage_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Garbage_square3_F: Garbage_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Garbage_square5_F: Garbage_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_GarbageBags_F: Garbage_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_GarbagePallet_F: Garbage_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_GarbageWashingMachine_F: Garbage_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_JunkPile_F: Garbage_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Tyre_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Tyres_F: Garbage_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Billboard_F: NonStrategic
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_InfoStand_V1_F: Infostand_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_InfoStand_V2_F: Infostand_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MapBoard_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class MapBoard_altis_F: Land_MapBoard_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class MapBoard_stratis_F: Land_MapBoard_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Noticeboard_F: NonStrategic
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_LampAirport_off_F: Lamps_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_LampAirport_F: Land_LampAirport_off_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_LampDecor_off_F: Lamps_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_LampDecor_F: Lamps_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_LampHalogen_off_F: Lamps_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_LampHalogen_F: Lamps_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_LampHarbour_off_F: Lamps_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_LampHarbour_F: Lamps_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_LampShabby_off_F: Lamps_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_LampShabby_F: Lamps_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_LampSolar_off_F: Lamps_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_LampSolar_F: Lamps_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_LampStadium_F: Lamps_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_LampStreet_off_F: Lamps_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_LampStreet_F: Lamps_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_LampStreet_small_off_F: Lamps_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_LampStreet_small_F: Lamps_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Basket_F: Market_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cages_F: Stall_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CratesPlastic_F: Stall_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CratesShabby_F: Stall_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CratesWooden_F: Stall_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MarketShelter_F: Stall_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Sack_F: Stall_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Sacks_goods_F: Stall_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Sacks_heap_F: Stall_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_StallWater_F: Stall_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WoodenCart_F: Market_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Offices_01_V1_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Pavement_narrow_corner_F: Pavements_base_F
	{
		scope = 1;//2
	};
	class Land_Pavement_narrow_F: Pavements_base_F
	{
		scope = 1;//2
	};
	class Land_Pavement_wide_corner_F: Pavements_base_F
	{
		scope = 1;//2
	};
	class Land_Pavement_wide_F: Pavements_base_F
	{
		scope = 1;//2
	};
	class Land_Slide_F: PlayGround_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BC_Basket_F: SportsGrounds_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BC_Court_F: SportsGrounds_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Goal_F: SportsGrounds_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Tribune_F: SportsGrounds_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Maroula_base_F: Statues_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Maroula_F: Statues_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MolonLabe_F: Statues_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Amphitheater_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Castle_01_wall_01_F: House_F
	{
//		scope = 1;
	};
	class Land_Castle_01_tower_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Castle_01_tower_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Church_01_V1_F: Church_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Hospital_main_F: House_F
	{
//		scope = 1;
	};
	class Land_Hospital_side1_F: House_F
	{
//		scope = 1;
	};
	class Land_Hospital_side2_F: House_F
	{
//		scope = 1;
	};
	class Land_LightHouse_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_LightHouse_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Lighthouse_small_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Lighthouse_small_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WIP_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_WIP_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Bench_F: Furniture_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CashDesk_F: Furniture_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HeatPump_F: Furniture_base_F
	{
//		scope = 1;
	};
	class Land_ChairPlastic_F: Furniture_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ChairWood_F: Furniture_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Icebox_F: Furniture_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Metal_rack_F: Furniture_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Metal_rack_Tall_F: Furniture_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Metal_wooden_rack_F: Furniture_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Rack_F: Furniture_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ShelvesMetal_F: Furniture_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ShelvesWooden_blue_F: Furniture_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_TableDesk_F: Furniture_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Addon_01_V1_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_u_Addon_01_V1_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_u_Addon_01_V1_dam_F: Land_u_Addon_01_V1_F
	{
//		scope = 1;
	};
	class Land_Addon_02_V1_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_d_Addon_02_V1_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_u_Addon_02_V1_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Addon_03_V1_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_i_Addon_03_V1_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Addon_03mid_V1_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_i_Addon_03mid_V1_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Addon_04_V1_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_i_Addon_04_V1_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Garage_V1_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_i_Garage_V1_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_Garage_V1_dam_F: Land_i_Garage_V1_F
	{
//		scope = 1;
	};
	class Land_Metal_Shed_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Metal_Shed_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_House_Big_01_V1_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_i_House_Big_01_V1_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_House_Big_01_V1_dam_F: Land_i_House_Big_01_V1_F
	{
//		scope = 1;
	};
	class Land_u_House_Big_01_V1_dam_F: Land_u_House_Big_01_V1_F
	{
//		scope = 1;
	};
	class Land_d_House_Big_01_V1_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_House_Big_02_V1_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_i_House_Big_02_V1_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_House_Big_02_V1_dam_F: Land_i_House_Big_02_V1_F
	{
//		scope = 1;
	};
	class Land_u_House_Big_02_V1_dam_F: Land_u_House_Big_02_V1_F
	{
//		scope = 1;
	};
	class Land_d_House_Big_02_V1_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Shop_01_V1_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_i_Shop_01_V1_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_Shop_01_V1_dam_F: Land_i_Shop_01_V1_F
	{
//		scope = 1;
	};
	class Land_u_Shop_01_V1_dam_F: Land_u_Shop_01_V1_F
	{
//		scope = 1;
	};
	class Land_d_Shop_01_V1_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Shop_02_V1_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_i_Shop_02_V1_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_Shop_02_V1_dam_F: Land_i_Shop_02_V1_F
	{
//		scope = 1;
	};
	class Land_u_Shop_02_V1_dam_F: Land_u_Shop_02_V1_F
	{
//		scope = 1;
	};
	class Land_d_Shop_02_V1_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_House_Small_01_V1_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_i_House_Small_01_V1_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_House_Small_01_V1_dam_F: Land_i_House_Small_01_V1_F
	{
//		scope = 1;
	};
	class Land_u_House_Small_01_V1_dam_F: Land_u_House_Small_01_V1_F
	{
//		scope = 1;
	};
	class Land_d_House_Small_01_V1_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_House_Small_02_V1_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_i_House_Small_02_V1_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_House_Small_02_V1_dam_F: Land_i_House_Small_02_V1_F
	{
//		scope = 1;
	};
	class Land_u_House_Small_02_V1_dam_F: Land_u_House_Small_02_V1_F
	{
//		scope = 1;
	};
	class Land_d_House_Small_02_V1_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_House_Small_03_V1_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_i_House_Small_03_V1_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_House_Small_03_V1_dam_F: Land_i_House_Small_03_V1_F
	{
//		scope = 1;
	};
	class Land_cargo_addon01_V1_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_cargo_addon01_V2_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_cargo_addon02_V1_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_cargo_addon02_V2_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_cargo_house_slum_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_cargo_house_slum_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Slum_House01_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Slum_House01_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Slum_House02_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Slum_House02_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Slum_House03_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Slum_House03_F: Land_Slum_House01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Stone_HouseBig_V1_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_i_Stone_HouseBig_V1_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_Stone_HouseBig_V1_dam_F: Land_i_Stone_HouseBig_V1_F
	{
//		scope = 1;
	};
	class Land_d_Stone_HouseBig_V1_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Stone_Shed_V1_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_i_Stone_Shed_V1_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_Stone_Shed_V1_dam_F: Land_i_Stone_Shed_V1_F
	{
//		scope = 1;
	};
	class Land_d_Stone_Shed_V1_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Stone_HouseSmall_V1_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_i_Stone_HouseSmall_V1_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_Stone_HouseSmall_V1_dam_F: Land_i_Stone_HouseSmall_V1_F
	{
//		scope = 1;
	};
	class Land_d_Stone_HouseSmall_V1_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Unfinished_Building_01_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Unfinished_Building_01_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Unfinished_Building_02_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Unfinished_Building_02_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Airport_center_F: House_F
	{
//		scope = 1;
	};
	class Land_Airport_left_F: Land_Airport_center_F
	{
//		scope = 1;
	};
	class Land_Airport_right_F: Land_Airport_left_F
	{
//		scope = 1;
	};
	class Land_Airport_Tower_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Airport_Tower_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Airport_Tower_dam_F: Land_Airport_Tower_F
	{
//		scope = 1;
	};
	class Land_Hangar_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_LandMark_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CargoBox_V1_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo20_blue_F: Cargo_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo20_brick_red_F: Cargo_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo20_cyan_F: Cargo_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo20_grey_F: Cargo_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo20_light_blue_F: Cargo_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo20_light_green_F: Cargo_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo20_military_green_F: Cargo_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo20_orange_F: Cargo_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo20_red_F: Cargo_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo20_sand_F: Cargo_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo20_white_F: Cargo_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo20_yellow_F: Cargo_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo40_blue_F: Cargo_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo40_brick_red_F: Cargo_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo40_cyan_F: Cargo_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo40_grey_F: Cargo_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo40_light_blue_F: Cargo_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo40_light_green_F: Cargo_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo40_military_green_F: Cargo_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo40_orange_F: Cargo_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo40_red_F: Cargo_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo40_sand_F: Cargo_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo40_white_F: Cargo_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo40_yellow_F: Cargo_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo20_color_V1_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Cargo20_color_V2_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Cargo20_color_V3_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Cargo20_china_color_V1_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Cargo20_china_color_V2_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Cargo20_military_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Cargo40_color_V1_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Cargo40_color_V2_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Cargo40_color_V3_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Cargo40_china_color_V1_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Cargo40_china_color_V2_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Cargo40_military_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_CarService_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_cmp_Hopper_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_cmp_Hopper_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_cmp_Shed_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_cmp_Shed_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_cmp_Shed_dam_F: House_F
	{
//		scope = 1;
	};
	class Land_cmp_Tower_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_cmp_Tower_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Crane_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_dp_bigTank_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_dp_bigTank_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_dp_mainFactory_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_dp_smallFactory_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_dp_smallTank_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_dp_smallTank_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_dp_transformer_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Factory_Conv1_10_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Factory_Conv1_10_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Factory_Conv1_End_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Factory_Conv1_Main_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Factory_Conv1_Main_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Factory_Conv2_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Factory_Conv2_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Factory_Hopper_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Factory_Hopper_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Factory_Main_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Factory_Main_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Factory_Tunnel_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FuelStation_Build_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_FuelStation_Build_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FuelStation_Feed_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FuelStation_Shed_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_FuelStation_Shed_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FuelStation_Sign_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_fs_feed_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_fs_price_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_fs_roof_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_fs_sign_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_IndPipe1_20m_F: IndPipe_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_IndPipe1_90degL_F: IndPipe_Small_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_IndPipe1_90degR_F: IndPipe_Small_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_IndPipe1_ground_F: IndPipe_Small_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_IndPipe1_Uup_F: IndPipe_Small_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_IndPipe1_valve_F: IndPipe_Small_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_IndPipe2_big_9_F: IndPipe_Small_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_IndPipe2_big_18_F: IndPipe_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_IndPipe2_big_18ladder_F: IndPipe_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_IndPipe2_big_ground1_F: IndPipe_Small_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_IndPipe2_big_ground2_F: IndPipe_Small_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_IndPipe2_big_support_F: IndPipe_Small_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_IndPipe2_bigL_L_F: IndPipe_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_IndPipe2_bigL_R_F: IndPipe_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_IndPipe2_Small_9_F: IndPipe_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_IndPipe2_Small_ground1_F: IndPipe_Small_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_IndPipe2_Small_ground2_F: IndPipe_Small_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_IndPipe2_SmallL_L_F: IndPipe_Small_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_IndPipe2_SmallL_R_F: IndPipe_Small_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HighVoltageColumn_F: PowerLines_base_F
	{
		scope = 1;//2
	};
	class Land_HighVoltageColumnWire_F: PowerLines_Wires_base_F
	{
//		scope = 1;
	};
	class Land_HighVoltageEnd_F: PowerLines_base_F
	{
//		scope = 1;
	};
	class Land_HighVoltageTower_dam_F: PowerLines_base_F
	{
		scope = 1;//2
	};
	class Land_HighVoltageTower_F: PowerLines_base_F
	{
		scope = 1;//2
	};
	class Land_HighVoltageTower_large_F: PowerLines_base_F
	{
		scope = 1;//2
	};
	class Land_HighVoltageTower_largeCorner_F: PowerLines_base_F
	{
		scope = 1;//2
	};
	class Land_PowerCable_submarine_F: PowerLines_base_F
	{
		scope = 1;//2
	};
	class Land_PowerLine_distributor_F: PowerLines_base_F
	{
		scope = 1;//2
	};
	class Land_PowerLine_part_F: PowerLines_base_F
	{
		scope = 1;//2
	};
	class Land_PowerPoleConcrete_F: PowerLines_Small_base_F
	{
		scope = 1;//2
	};
	class Land_PowerPoleWooden_F: PowerLines_Small_base_F
	{
		scope = 1;//2
	};
	class Land_PowerPoleWooden_L_off_F: PowerLines_Small_base_F
	{
		scope = 1;//2
	};
	class Land_PowerPoleWooden_L_F: PowerLines_Small_base_F
	{
		scope = 1;//2
	};
	class Land_PowerPoleWooden_small_F: PowerLines_Small_base_F
	{
		scope = 1;//2
	};
	class Land_PowerWireBig_direct_F: PowerLines_Wires_base_F
	{
//		scope = 1;
	};
	class Land_PowerWireBig_direct_short_F: PowerLines_Wires_base_F
	{
//		scope = 1;
	};
	class Land_PowerWireBig_end_F: PowerLines_Wires_base_F
	{
//		scope = 1;
	};
	class Land_PowerWireBig_left_F: PowerLines_Wires_base_F
	{
//		scope = 1;
	};
	class Land_PowerWireBig_right_F: PowerLines_Wires_base_F
	{
//		scope = 1;
	};
	class Land_PowerWireSmall_damaged_F: PowerLines_Wires_base_F
	{
//		scope = 1;
	};
	class Land_PowerWireSmall_direct_F: PowerLines_Wires_base_F
	{
//		scope = 1;
	};
	class Land_PowerWireSmall_Left_F: PowerLines_Wires_base_F
	{
//		scope = 1;
	};
	class Land_PowerWireSmall_Right_F: PowerLines_Wires_base_F
	{
//		scope = 1;
	};
	class Land_PowLines_Transformer_F: PowerLines_base_F
	{
		scope = 1;//2
	};
	class Land_ReservoirTank_Airport_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_ReservoirTank_Airport_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ReservoirTank_Rust_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_ReservoirTank_Rust_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ReservoirTank_V1_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_ReservoirTank_V1_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ReservoirTower_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_ReservoirTower_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Shed_Big_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Shed_Big_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Shed_Small_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Shed_Small_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Shed_Ind_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_i_Shed_Ind_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_u_Shed_Ind_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SolarPanel_1_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SolarPanel_2_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SolarPanel_3_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_spp_Mirror_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_spp_Mirror_Broken_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_spp_Mirror_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_spp_Panel_Broken_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_spp_Panel_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_spp_Tower_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_spp_Tower_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_spp_Tower_dam_F: Land_spp_Tower_F
	{
//		scope = 1;
	};
	class Land_spp_Transformer_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_spp_Transformer_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Tank_rust_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Communication_anchor_F: House_F
	{
//		scope = 1;
		scopeCurator = 1;//2
	};
	class Land_Communication_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_TBox_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_TBox_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_TTowerBig_1_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_TTowerBig_1_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_TTowerBig_2_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_TTowerBig_2_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_TTowerSmall_1_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_TTowerSmall_2_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Rope_F: Thing
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WavePowerPlant_F: FloatingStructure_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WavePowerPlantBroken_F: FloatingStructure_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Windmill01_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_d_Windmill01_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_Windmill01_F: Land_d_Windmill01_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PowerGenerator_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_wpp_Turbine_V1_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_wpp_Turbine_V2_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_File1_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_File2_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FilePhotos_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Map_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Map_unfolded_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Notepad_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Photos_V1_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Photos_V2_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Photos_V3_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Photos_V4_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Photos_V5_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Photos_V6_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ExtensionCord_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FloodLight_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FMradio_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HandyCam_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Laptop_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Laptop_unfolded_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Laptop_unfolded_scripted_F: Land_Laptop_unfolded_F
	{
//		scope = 1;
	};
	class Land_MobilePhone_old_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MobilePhone_smart_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Portable_generator_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PortableLongRangeRadio_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SatellitePhone_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SurvivalRadio_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BottlePlastic_V1_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Can_Dented_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Can_Rusty_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Can_V1_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Can_V2_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Can_V3_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_TacticalBacon_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Compass_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Suitcase_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PenBlack_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PencilBlue_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PencilGreen_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PencilRed_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PencilYellow_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PenRed_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PensAndPencils_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Axe_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Axe_fire_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_DrillAku_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_DustMask_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_File_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Gloves_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Grinder_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Hammer_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Meter3m_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MultiMeter_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Pliers_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Saw_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Screwdriver_V1_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Screwdriver_V2_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wrench_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Money_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BarrelEmpty_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BarrelSand_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BarrelTrash_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BarrelWater_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Bucket_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Bucket_clean_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Bucket_painted_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BucketNavy_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CanisterFuel_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CanisterOil_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CanisterPlastic_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MetalBarrel_empty_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MetalBarrel_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WaterBarrel_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WaterTank_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BagBunker_Large_F: BagBunker_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BagBunker_Small_F: BagBunker_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BagBunker_Tower_F: BagBunker_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BagFence_Corner_F: BagFence_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BagFence_End_F: BagFence_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BagFence_Long_F: BagFence_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BagFence_Round_F: BagFence_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BagFence_Short_F: BagFence_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Barracks_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_i_Barracks_V1_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_Barracks_V1_dam_F: Land_i_Barracks_V1_F
	{
//		scope = 1;
	};
	class Land_Bunker_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo_House_V1_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Cargo_House_V1_F: Cargo_House_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo_House_V2_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Cargo_House_V2_F: Cargo_House_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo_House_V3_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Cargo_House_V3_F: Cargo_House_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo_HQ_V1_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Cargo_HQ_V1_F: Cargo_HQ_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo_HQ_V2_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Cargo_HQ_V2_F: Cargo_HQ_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo_HQ_V3_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Cargo_HQ_V3_F: Cargo_HQ_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo_Patrol_V1_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Cargo_Patrol_V1_F: Cargo_Patrol_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo_Patrol_V2_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Cargo_Patrol_V2_F: Cargo_Patrol_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo_Patrol_V3_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Cargo_Patrol_V3_F: Cargo_Patrol_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo_Tower_V1_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Cargo_Tower_V1_F: Cargo_Tower_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo_Tower_V2_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Cargo_Tower_V2_F: Cargo_Tower_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo_Tower_V3_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Cargo_Tower_V3_F: Cargo_Tower_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Medevac_house_V1_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Medevac_house_V1_F: Cargo_House_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Medevac_HQ_V1_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Medevac_HQ_V1_F: Cargo_HQ_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HBarrier_1_F: HBarrier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HBarrier_3_F: HBarrier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HBarrier_5_F: HBarrier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HBarrierBig_F: HBarrier_base_F
	{
//		scope = 1;
	};
	class Land_HBarrier_Big_F: Land_HBarrierBig_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HBarrierTower_F: HBarrier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HBarrierWall_corner_F: HBarrier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HBarrierWall_corridor_F: HBarrier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HBarrierWall4_F: HBarrier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HBarrierWall6_F: HBarrier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Razorwire_F: NonStrategic
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HelipadCircle_F: Helipad_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HelipadCivil_F: Helipad_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HelipadEmpty_F: Helipad_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HelipadRescue_F: Helipad_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HelipadSquare_F: Helipad_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_JumpTarget_F: Helipad_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MilOffices_V1_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Radar_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Radar_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Radar_Small_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Radar_Small_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class CamoNet_BLUFOR_F: Shelter_base_F
	{
		scope = 1;//2
	};
	class CamoNet_OPFOR_F: CamoNet_BLUFOR_F
	{
		scope = 1;//2
	};
	class CamoNet_INDP_F: CamoNet_BLUFOR_F
	{
		scope = 1;//2
	};
	class CamoNet_BLUFOR_Curator_F: CamoNet_BLUFOR_F
	{
//		scope = 1;
		scopeCurator = 1;//2
	};
	class CamoNet_OPFOR_Curator_F: CamoNet_OPFOR_F
	{
//		scope = 1;
		scopeCurator = 1;//2
	};
	class CamoNet_INDP_Curator_F: CamoNet_INDP_F
	{
//		scope = 1;
		scopeCurator = 1;//2
	};
	class CamoNet_BLUFOR_open_Curator_F: CamoNet_BLUFOR_open_F
	{
//		scope = 1;
		scopeCurator = 1;//2
	};
	class CamoNet_OPFOR_open_Curator_F: CamoNet_OPFOR_open_F
	{
//		scope = 1;
		scopeCurator = 1;//2
	};
	class CamoNet_INDP_open_Curator_F: CamoNet_INDP_open_F
	{
//		scope = 1;
		scopeCurator = 1;//2
	};
	class CamoNet_BLUFOR_big_Curator_F: CamoNet_BLUFOR_big_F
	{
//		scope = 1;
		scopeCurator = 1;//2
	};
	class CamoNet_OPFOR_big_Curator_F: CamoNet_OPFOR_big_F
	{
//		scope = 1;
		scopeCurator = 1;//2
	};
	class CamoNet_INDP_big_Curator_F: CamoNet_INDP_big_F
	{
//		scope = 1;
		scopeCurator = 1;//2
	};
	class Land_TentHangar_V1_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_TentHangar_V1_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_TentHangar_V1_dam_F: Land_TentHangar_V1_F
	{
//		scope = 1;
	};
	class Land_BuoyBig_F: Buoy_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_nav_pier_m_F: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Pier_addon: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Pier_Box_F: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Pier_F: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Pier_small_F: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Pier_wall_F: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PierLadder_F: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Pillar_Pier_F: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Sea_Wall_F: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_RowBoat_V1_F: RowBoats_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_RowBoat_V2_F: RowBoats_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_RowBoat_V3_F: RowBoats_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Dome_Big_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Dome_Small_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Research_house_V1_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Research_house_V1_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Research_HQ_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Research_HQ_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ClutterCutter_large_F: Thing
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ClutterCutter_medium_F: Land_ClutterCutter_large_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ClutterCutter_small_F: Land_ClutterCutter_large_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class ProtectionZone_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class BlockConcrete_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Dirthump_1_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Dirthump_4_F: NonStrategic
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Obstacle_Bridge_F: NonStrategic
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Obstacle_Climb_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Obstacle_Saddle_F: NonStrategic
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_RampConcrete_F: NonStrategic
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_RampConcreteHigh_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class ShootingPos_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ShootingPos_F: ShootingPos_F
	{
//		scope = 1;
	};
	class Target_Rail_F: TargetBase
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class TargetP_Inf_F: TargetBase
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class TargetP_Inf_NoPop_F: TargetP_Inf_F
	{
//		scope = 1;
	};
	class TargetP_Inf_Acc1_NoPop_F: TargetP_Inf_Acc1_F
	{
//		scope = 1;
	};
	class TargetP_Inf_Acc2_NoPop_F: TargetP_Inf_Acc2_F
	{
//		scope = 1;
	};
	class Target_PopUp_Moving_NoPop_F: Target_PopUp_Moving_F
	{
//		scope = 1;
	};
	class Target_PopUp_Moving_Acc1_NoPop_F: Target_PopUp_Moving_Acc1_F
	{
//		scope = 1;
	};
	class Target_PopUp_Moving_Acc2_NoPop_F: Target_PopUp_Moving_Acc2_F
	{
//		scope = 1;
	};
	class Target_PopUp_Moving_90deg_NoPop_F: Target_PopUp_Moving_90deg_F
	{
//		scope = 1;
	};
	class Target_PopUp_Moving_90deg_Acc1_NoPop_F: Target_PopUp_Moving_90deg_Acc1_F
	{
//		scope = 1;
	};
	class Target_PopUp_Moving_90deg_Acc2_NoPop_F: Target_PopUp_Moving_90deg_Acc2_F
	{
//		scope = 1;
	};
	class TargetP_Inf2_NoPop_F: TargetP_Inf2_F
	{
//		scope = 1;
	};
	class TargetP_Inf2_Acc1_NoPop_F: TargetP_Inf2_Acc1_F
	{
//		scope = 1;
	};
	class TargetP_Inf2_Acc2_NoPop_F: TargetP_Inf2_Acc2_F
	{
//		scope = 1;
	};
	class Target_PopUp2_Moving_NoPop_F: Target_PopUp2_Moving_F
	{
//		scope = 1;
	};
	class Target_PopUp2_Moving_Acc1_NoPop_F: Target_PopUp2_Moving_Acc1_F
	{
//		scope = 1;
	};
	class Target_PopUp2_Moving_Acc2_NoPop_F: Target_PopUp2_Moving_Acc2_F
	{
//		scope = 1;
	};
	class Target_PopUp2_Moving_90deg_NoPop_F: Target_PopUp2_Moving_90deg_F
	{
//		scope = 1;
	};
	class Target_PopUp2_Moving_90deg_Acc1_NoPop_F: Target_PopUp2_Moving_90deg_Acc1_F
	{
//		scope = 1;
	};
	class Target_PopUp2_Moving_90deg_Acc2_NoPop_F: Target_PopUp2_Moving_90deg_Acc2_F
	{
//		scope = 1;
	};
	class TargetP_Inf3_NoPop_F: TargetP_Inf3_F
	{
//		scope = 1;
	};
	class TargetP_Inf3_Acc1_NoPop_F: TargetP_Inf3_Acc1_F
	{
//		scope = 1;
	};
	class TargetP_Inf3_Acc2_NoPop_F: TargetP_Inf3_Acc2_F
	{
//		scope = 1;
	};
	class Target_PopUp3_Moving_NoPop_F: Target_PopUp3_Moving_F
	{
//		scope = 1;
	};
	class Target_PopUp3_Moving_Acc1_NoPop_F: Target_PopUp3_Moving_Acc1_F
	{
//		scope = 1;
	};
	class Target_PopUp3_Moving_Acc2_NoPop_F: Target_PopUp3_Moving_Acc2_F
	{
//		scope = 1;
	};
	class Target_PopUp3_Moving_90deg_NoPop_F: Target_PopUp3_Moving_90deg_F
	{
//		scope = 1;
	};
	class Target_PopUp3_Moving_90deg_Acc1_NoPop_F: Target_PopUp3_Moving_90deg_Acc1_F
	{
//		scope = 1;
	};
	class Target_PopUp3_Moving_90deg_Acc2_NoPop_F: Target_PopUp3_Moving_90deg_Acc2_F
	{
//		scope = 1;
	};
	class TargetP_Zom_NoPop_F: TargetP_Zom_F
	{
//		scope = 1;
	};
	class TargetP_Zom_Acc1_NoPop_F: TargetP_Zom_Acc1_F
	{
//		scope = 1;
	};
	class Zombie_PopUp_Moving_NoPop_F: Zombie_PopUp_Moving_F
	{
//		scope = 1;
	};
	class Zombie_PopUp_Moving_Acc1_NoPop_F: Zombie_PopUp_Moving_Acc1_F
	{
//		scope = 1;
	};
	class Zombie_PopUp_Moving_90deg_NoPop_F: Zombie_PopUp_Moving_90deg_F
	{
//		scope = 1;
	};
	class Zombie_PopUp_Moving_90deg_Acc1_NoPop_F: Zombie_PopUp_Moving_90deg_Acc1_F
	{
//		scope = 1;
	};
	class TargetP_Civ_NoPop_F: TargetP_Civ_F
	{
//		scope = 1;
	};
	class Hostage_PopUp_Moving_NoPop_F: Hostage_PopUp_Moving_F
	{
//		scope = 1;
	};
	class Hostage_PopUp_Moving_90deg_NoPop_F: Hostage_PopUp_Moving_90deg_F
	{
//		scope = 1;
	};
	class TargetP_Civ2_NoPop_F: TargetP_Civ2_F
	{
//		scope = 1;
	};
	class Hostage_PopUp2_Moving_NoPop_F: Hostage_PopUp2_Moving_F
	{
//		scope = 1;
	};
	class Hostage_PopUp2_Moving_90deg_NoPop_F: Hostage_PopUp2_Moving_90deg_F
	{
//		scope = 1;
	};
	class TargetP_Civ3_NoPop_F: TargetP_Civ3_F
	{
//		scope = 1;
	};
	class Hostage_PopUp3_Moving_NoPop_F: Hostage_PopUp3_Moving_F
	{
//		scope = 1;
	};
	class Hostage_PopUp3_Moving_90deg_NoPop_F: Hostage_PopUp3_Moving_90deg_F
	{
//		scope = 1;
	};
	class Target_F: TargetBase
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class B_TargetSoldier: TargetSoldierBase
	{
//		scope = 1;
	};
	class O_TargetSoldier: TargetSoldierBase
	{
//		scope = 1;
	};
	class I_TargetSoldier: TargetSoldierBase
	{
//		scope = 1;
	};
	class Land_Shoot_House_Wall_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Target_Oval_F: TargetBase
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Target_Oval_NoPop_F: Land_Target_Oval_F
	{
//		scope = 1;
	};
	class Land_Target_Oval_Wall_Left_NoPop_F: Land_Target_Oval_Wall_Left_F
	{
//		scope = 1;
	};
	class Land_Target_Oval_Wall_Right_NoPop_F: Land_Target_Oval_Wall_Right_F
	{
//		scope = 1;
	};
	class Land_Target_Oval_Wall_Top_NoPop_F: Land_Target_Oval_Wall_Top_F
	{
//		scope = 1;
	};
	class Land_Target_Oval_Wall_Bottom_NoPop_F: Land_Target_Oval_Wall_Bottom_F
	{
//		scope = 1;
	};
	class Skeet_Clay_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Skeet_Clay_NoPop_F: Skeet_Clay_F
	{
//		scope = 1;
	};
	class Steel_Plate_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Metal_Pole_F: Static
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Metal_Pole_Skeet_NoPop_F: Metal_Pole_Skeet_F
	{
//		scope = 1;
	};
	class TargetCenter: House_F
	{
//		scope = 1;
	};
	class Land_Ancient_Wall_4m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Ancient_Wall_8m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BarGate_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BarGate_01_open_F: Land_BarGate_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Canal_Wall_10m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Canal_Wall_D_center_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Canal_Wall_D_left_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Canal_Wall_D_right_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Canal_Wall_Stairs_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Canal_WallSmall_10m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_City_4m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_City_8m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_City_8mD_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_City_Gate_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_City_Pillar_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_City2_4m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_City2_8m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_City2_8mD_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_City2_PillarD_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CncBarrier_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CncBarrier_stripes_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CncBarrierMedium_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CncBarrierMedium4_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CncShelter_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CncWall1_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CncWall4_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Concrete_SmallWall_4m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Concrete_SmallWall_8m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Crash_barrier_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_IndFnc_3_D_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_IndFnc_3_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_IndFnc_3_Hole_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_IndFnc_9_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_IndFnc_Corner_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_IndFnc_Pole_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Mil_ConcreteWall_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Mil_WallBig_4m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Mil_WallBig_Corner_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Mil_WallBig_Gate_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Mil_WiredFence_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Mil_WiredFence_Gate_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Mil_WiredFenceD_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Mound01_8m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Mound02_8m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Net_Fence_4m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Net_Fence_8m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Net_Fence_Gate_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Net_Fence_pole_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Net_FenceD_8m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_New_WiredFence_5m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_New_WiredFence_10m_Dam_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_New_WiredFence_10m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_New_WiredFence_pole_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Pipe_fence_4m_F: Wall_F
	{
		scope = 1;//2
	};
	class Land_Pipe_fence_4mNoLC_F: Wall_F
	{
//		scope = 1;
		scopeCurator = 1;//2
	};
	class Land_PipeWall_concretel_8m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Rampart_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_RedWhitePole_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Slums01_8m: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Slums01_pole: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Slums02_4m: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Slums02_pole: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SportGround_fence_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Stone_4m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Stone_8m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Stone_8mD_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Stone_Gate_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Stone_pillar_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wall_IndCnc_2deco_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wall_IndCnc_4_D_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wall_IndCnc_4_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wall_IndCnc_End_2_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wall_IndCnc_Pole_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wall_Tin_4: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wall_Tin_4_2: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wall_Tin_Pole: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wired_Fence_4m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wired_Fence_4mD_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wired_Fence_8m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Wired_Fence_8mD_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WoodenLog_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WoodenTable_large_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WoodenTable_small_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Pallets_stack_F: Constructions_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PortableLight_single_F: Lamps_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PortableLight_double_F: Land_PortableLight_single_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Battery_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BakedBeans_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BottlePlastic_V2_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Canteen_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CerealsBox_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PowderedMilk_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_RiceBox_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Antibiotic_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Bandage_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BloodBag_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Defibrillator_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_DisinfectantSpray_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HeatPack_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PainKillers_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_VitaminBottle_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WaterPurificationTablets_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ButaneCanister_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ButaneTorch_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CanOpener_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_DuctTape_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FireExtinguisher_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_GasCanister_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_GasCooker_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Matches_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MetalWire_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Shovel_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_TinContainer_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_InvisibleBarrier_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PhoneBooth_01_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PhoneBooth_02_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WoodPile_large_F: NonStrategic
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Camping_Light_F: FloatingStructure_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Grave_dirt_F: Cemetery_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Grave_forest_F: Cemetery_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Grave_rocks_F: Cemetery_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_GarbageContainer_closed_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_GarbageContainer_open_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Graffiti_01_F: Graffiti_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Graffiti_02_F: Graffiti_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Graffiti_03_F: Graffiti_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Graffiti_04_F: Graffiti_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Graffiti_05_F: Graffiti_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SlideCastle_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_SlideCastle_F: PlayGround_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ShelvesWooden_F: Furniture_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ShelvesWooden_khaki_F: Furniture_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Leaflet_01_F: Leaflet_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Leaflet_02_F: Leaflet_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Leaflet_03_F: Leaflet_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Leaflet_04_F: Leaflet_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Map_altis_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Map_blank_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Map_stratis_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Poster_01_F: Poster_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Poster_02_F: Poster_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Poster_03_F: Poster_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Poster_04_F: Poster_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Poster_05_F: Poster_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Poster_06_F: Poster_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_LuggageHeap_01_F: LuggageHeap_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_LuggageHeap_02_F: LuggageHeap_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_LuggageHeap_03_F: LuggageHeap_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_LuggageHeap_04_F: LuggageHeap_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_LuggageHeap_05_F: LuggageHeap_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Ammobox_rounds_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Magazine_rifle_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BarrelEmpty_grey_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BarrelSand_grey_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BarrelTrash_grey_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BarrelWater_grey_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CrabCages_F: Garbage_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FishingGear_01_F: Garbage_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FishingGear_02_F: Garbage_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Atm_01_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Atm_02_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BeachBooth_01_F: NonStrategic
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Bench_01_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Bench_02_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_LifeguardTower_01_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_TablePlastic_01_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Sunshade_01_F: Camping_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_GarbageBarrel_01_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_GarbageBin_01_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class MapBoard_seismic_F: Land_MapBoard_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Kiosk_blueking_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Kiosk_blueking_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Kiosk_gyros_F: Land_Kiosk_blueking_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Kiosk_papers_F: Land_Kiosk_blueking_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Kiosk_redburger_F: Land_Kiosk_blueking_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Carousel_01_F: PlayGround_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Swing_01_F: PlayGround_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_TouristShelter_01_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_GH_Gazebo_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_GH_Gazebo_F: House_F
	{
		scope = 1;//2
	};
	class Land_GH_House_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_GH_House_1_F: House_F
	{
		scope = 1;//2
	};
	class Land_GH_House_2_F: House_F
	{
		scope = 1;//2
	};
	class Land_GH_MainBuilding_entry_F: House_F
	{
//		scope = 1;
	};
	class Land_GH_MainBuilding_left_F: House_F
	{
//		scope = 1;
	};
	class Land_GH_MainBuilding_middle_F: House_F
	{
//		scope = 1;
	};
	class Land_GH_MainBuilding_right_F: House_F
	{
//		scope = 1;
	};
	class Land_GH_Platform_F: NonStrategic
	{
		scope = 1;//2
	};
	class Land_GH_Pool_F: NonStrategic
	{
		scope = 1;//2
	};
	class Land_GH_Stairs_F: NonStrategic
	{
		scope = 1;//2
	};
	class Land_Stadium_p1_F: House_F
	{
//		scope = 1;
	};
	class Land_Stadium_p2_F: House_F
	{
//		scope = 1;
	};
	class Land_Stadium_p3_F: House_F
	{
//		scope = 1;
	};
	class Land_Stadium_p4_F: House_F
	{
//		scope = 1;
	};
	class Land_Stadium_p5_F: House_F
	{
//		scope = 1;
	};
	class Land_Stadium_p6_F: House_F
	{
//		scope = 1;
	};
	class Land_Stadium_p7_F: House_F
	{
//		scope = 1;
	};
	class Land_Stadium_p8_F: House_F
	{
//		scope = 1;
	};
	class Land_Stadium_p9_F: House_F
	{
//		scope = 1;
	};
	class Land_Document_01_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_File_research_F: Land_File2_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Device_assembled_F: Strategic
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Device_disassembled_F: Land_Device_assembled_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Laptop_device_F: Land_Laptop_unfolded_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SportGround_fence_noLC_F: Wall_F
	{
//		scope = 1;
	};
	class Land_Sign_Mines_F: NonStrategic
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Sign_Direction_F: NonStrategic
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Sign_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class RoadCone_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class RoadCone_L_F: FloatingStructure_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class RoadBarrier_F: RoadCone_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class RoadBarrier_small_F: RoadCone_L_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Pole_F: ThingX
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class TapeSign_F: Sign_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class ArrowDesk_L_F: Thing
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class SignAd_Sponsor_F: Sign_F
	{
		scope = 1;//2
	};

	class LaserTargetCBase: LaserTargetBase
	{
//		scope = 1;
	};
	class LaserTargetWBase: LaserTargetBase
	{
//		scope = 1;
	};
	class LaserTargetEBase: LaserTargetBase
	{
//		scope = 1;
	};

	class Land_Bench_03_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Bench_04_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Bench_05_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ClothesLine_01_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ClothesLine_01_full_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ClothesLine_01_short_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ConcreteBlock_01_F: House_Small_F
	{
//		scope = 1;
	};
	class Land_FireEscape_01_short_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FireEscape_01_tall_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_GarbageBin_02_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PicnicTable_01_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Plank_01_4m_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Plank_01_8m_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Pot_01_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Pot_02_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_GarageShelter_01_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_House_Big_01_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_House_Big_01_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_House_Big_02_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_House_Big_03_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_House_Big_04_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_House_Big_05_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_House_Native_01_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_House_Native_01_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_House_Native_02_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_House_Native_02_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_House_Small_01_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_House_Small_01_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_House_Small_02_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_House_Small_02_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_House_Small_03_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_House_Small_03_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_House_Small_04_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_House_Small_05_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_House_Small_06_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_School_01_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Shed_01_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Shed_01_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Shed_02_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Shed_02_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Shed_03_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Shed_03_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Shed_04_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Shed_04_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Shed_05_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Shed_05_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Shed_06_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Shed_06_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Shed_07_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Slum_01_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Slum_02_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Slum_03_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Slum_04_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Slum_05_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_RugbyGoal_01_F: SportsGrounds_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Addon_01_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Addon_01_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Addon_02_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Addon_02_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Addon_03_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Addon_03_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Addon_04_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Addon_04_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Addon_05_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Addon_05_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SignMonolith_01_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FuelStation_01_arrow_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FuelStation_01_prices_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FuelStation_01_pump_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FuelStation_01_roof_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FuelStation_01_shop_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FuelStation_01_workshop_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FuelStation_02_prices_F: Land_fs_price_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FuelStation_02_pump_F: Land_fs_feed_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FuelStation_02_roof_F: Land_fs_roof_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FuelStation_02_sign_F: Land_fs_sign_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FuelStation_02_workshop_F: Land_CarService_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Hotel_01_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Hotel_02_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ClothShelter_01_F: Stall_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ClothShelter_02_F: Stall_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MetalShelter_01_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_MetalShelter_01_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MetalShelter_02_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_MetalShelter_02_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WoodenShelter_01_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MultistoryBuilding_01_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MultistoryBuilding_03_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MultistoryBuilding_04_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Shop_City_01_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Shop_City_02_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Shop_City_03_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Shop_City_04_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Shop_City_05_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Shop_City_06_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Shop_City_07_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Shop_Town_01_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Shop_Town_01_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Shop_Town_02_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Shop_Town_02_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Shop_Town_03_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Shop_Town_03_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Shop_Town_04_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Shop_Town_04_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Shop_Town_05_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Shop_Town_05_addon_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Supermarket_01_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Warehouse_03_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_AncientHead_01_F: AncientRelics_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_AncientStatue_01_F: AncientRelics_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_AncientStatue_02_F: AncientRelics_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PetroglyphWall_01_F: AncientRelics_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PetroglyphWall_02_F: AncientRelics_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_RaiStone_01_F: AncientRelics_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_StoneTanoa_01_F: AncientRelics_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BasaltKerb_01_2m_F: BasaltRuins_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BasaltKerb_01_2m_d_F: BasaltRuins_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BasaltKerb_01_4m_F: BasaltRuins_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BasaltKerb_01_pile_F: BasaltRuins_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BasaltKerb_01_platform_F: BasaltRuins_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BasaltWall_01_4m_F: BasaltRuins_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BasaltWall_01_8m_F: BasaltRuins_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BasaltWall_01_d_left_F: BasaltRuins_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BasaltWall_01_d_right_F: BasaltRuins_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BasaltWall_01_gate_F: BasaltRuins_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cathedral_01_F: Church_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Grave_01_F: Cemetery_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Grave_02_F: Cemetery_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Grave_03_F: Cemetery_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Grave_04_F: Cemetery_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Grave_05_F: Cemetery_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Grave_06_F: Cemetery_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Grave_07_F: Cemetery_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Mausoleum_01_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Tomb_01_F: Cemetery_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Tombstone_01_F: Cemetery_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Tombstone_02_F: Cemetery_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Tombstone_03_F: Cemetery_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Church_01_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Church_01_F: Church_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Church_02_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Church_02_F: Church_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Church_03_F: Church_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Fortress_01_5m_F: House_F
	{
		scope = 1;//2
	};
	class Land_Fortress_01_10m_F: House_F
	{
		scope = 1;//2
	};
	class Land_Fortress_01_bricks_v1_F: House_F
	{
		scope = 1;//2
	};
	class Land_Fortress_01_bricks_v2_F: House_F
	{
		scope = 1;//2
	};
	class Land_Fortress_01_cannon_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Fortress_01_d_L_F: House_F
	{
		scope = 1;//2
	};
	class Land_Fortress_01_d_R_F: House_F
	{
		scope = 1;//2
	};
	class Land_Fortress_01_innerCorner_70_F: House_F
	{
		scope = 1;//2
	};
	class Land_Fortress_01_innerCorner_90_F: House_F
	{
		scope = 1;//2
	};
	class Land_Fortress_01_innerCorner_110_F: House_F
	{
		scope = 1;//2
	};
	class Land_Fortress_01_outterCorner_50_F: House_F
	{
		scope = 1;//2
	};
	class Land_Fortress_01_outterCorner_80_F: House_F
	{
		scope = 1;//2
	};
	class Land_Fortress_01_outterCorner_90_F: House_F
	{
		scope = 1;//2
	};
	class Land_Temple_Native_01_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Temple_Native_01_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PalmTotem_01_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PalmTotem_02_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PalmTotem_03_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_DPP_01_mainFactory_F: Land_dp_mainFactory_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_DPP_01_smallFactory_F: Land_dp_smallFactory_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_DPP_01_transformer_F: Land_dp_transformer_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_DPP_01_waterCooler_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_DPP_01_waterCooler_ladder_F: House_F
	{
//		scope = 1;
	};
	class Land_ConcreteWell_01_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CinderBlocks_01_F: NonStrategic
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_TimberPile_01_F: NonStrategic
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WoodenPlanks_01_F: NonStrategic
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WoodenPlanks_01_messy_F: NonStrategic
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ContainerCrane_01_F: Industry_base_F
	{
//		scope = 1;
	};
	class Land_ContainerCrane_01_arm_F: Industry_base_F
	{
//		scope = 1;
	};
	class Land_ContainerCrane_01_arm_lowered_F: Industry_base_F
	{
//		scope = 1;
	};
	class Land_ContainerLine_01_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ContainerLine_02_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ContainerLine_03_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CraneRail_01_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_DryDock_01_end_F: Industry_base_F
	{
//		scope = 1;
	};
	class Land_DryDock_01_middle_F: Industry_base_F
	{
//		scope = 1;
	};
	class Land_GantryCrane_01_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_GuardHouse_01_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MobileCrane_01_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_MobileCrane_01_hook_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_StorageTank_01_large_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_StorageTank_01_small_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Walkover_01_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Warehouse_01_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Warehouse_01_ladder_F: Industry_base_F
	{
//		scope = 1;
	};
	class Land_Warehouse_02_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Warehouse_02_ladder_F: Industry_base_F
	{
//		scope = 1;
	};
	class Land_WarehouseShelter_01_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SY_01_block_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SY_01_conveyor_end_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SY_01_conveyor_chute_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SY_01_conveyor_junction_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SY_01_conveyor_long_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SY_01_conveyor_reclaimer_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SY_01_conveyor_short_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SY_01_conveyor_slope_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SY_01_crusher_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SY_01_reclaimer_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SY_01_shiploader_arm_F: Industry_base_F
	{
//		scope = 1;
	};
	class Land_SY_01_shiploader_F: Industry_base_F
	{
//		scope = 1;
	};
	class Land_SY_01_stockpile_01_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SY_01_stockpile_02_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SY_01_tripper_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SCF_01_boilerBuilding_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SCF_01_clarifier_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SCF_01_condenser_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SCF_01_conveyor_8m_high_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SCF_01_conveyor_16m_high_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SCF_01_conveyor_16m_slope_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SCF_01_conveyor_columnBase_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SCF_01_conveyor_end_high_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SCF_01_conveyor_hole_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SCF_01_crystallizer_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SCF_01_crystallizerTowers_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SCF_01_diffuser_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SCF_01_feeder_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SCF_01_generalBuilding_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SCF_01_heap_bagasse_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SCF_01_heap_sugarcane_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SCF_01_chimney_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SCF_01_pipe_24m_high_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SCF_01_pipe_24m_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SCF_01_pipe_8m_high_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SCF_01_pipe_8m_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SCF_01_pipe_curve_high_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SCF_01_pipe_curve_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SCF_01_pipe_end_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SCF_01_pipe_up_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SCF_01_shed_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SCF_01_shredder_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SCF_01_storageBin_big_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SCF_01_storageBin_medium_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SCF_01_storageBin_small_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SCF_01_warehouse_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SCF_01_washer_F: Industry_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SM_01_reservoirTower_F: Land_ReservoirTower_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SM_01_shed_F: Land_i_Shed_Ind_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SM_01_shed_unfinished_F: Land_u_Shed_Ind_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SM_01_shelter_narrow_F: Land_Shed_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SM_01_shelter_wide_F: Land_Shed_Big_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Airport_01_controlTower_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Airport_01_hangar_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Airport_01_terminal_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Airport_02_controlTower_F: Infrastructure_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Airport_02_hangar_left_F: Infrastructure_base_F
	{
//		scope = 1;
	};
	class Land_Airport_02_hangar_right_F: Infrastructure_base_F
	{
//		scope = 1;
	};
	class Land_Airport_02_terminal_F: Infrastructure_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Airport_02_sign_aeroport_F: Infrastructure_base_F
	{
//		scope = 1;
	};
	class Land_Airport_02_sign_arrivees_F: Infrastructure_base_F
	{
//		scope = 1;
	};
	class Land_Airport_02_sign_de_F: Infrastructure_base_F
	{
//		scope = 1;
	};
	class Land_Airport_02_sign_departs_F: Infrastructure_base_F
	{
//		scope = 1;
	};
	class Land_Airport_02_sign_tanoa_F: Infrastructure_base_F
	{
//		scope = 1;
	};
	class Land_AirstripPlatform_01_F: Infrastructure_base_F
	{
		scope = 1;//2
	};
	class Land_AirstripPlatform_01_footer_F: Infrastructure_base_F
	{
		scope = 1;//2
	};
	class Land_BridgeSea_01_pillar_F: Infrastructure_base_F
	{
//		scope = 1;
	};
	class Land_BridgeWooden_01_pillar_F: Infrastructure_base_F
	{
//		scope = 1;
	};
	class Land_ConcreteKerb_01_2m_F: Pavements_base_F
	{
		scope = 1;//2
	};
	class Land_ConcreteKerb_01_4m_F: Pavements_base_F
	{
		scope = 1;//2
	};
	class Land_ConcreteKerb_01_8m_F: Pavements_base_F
	{
		scope = 1;//2
	};
	class Land_ConcreteKerb_02_1m_F: Pavements_base_F
	{
		scope = 1;//2
	};
	class Land_ConcreteKerb_02_2m_F: Pavements_base_F
	{
		scope = 1;//2
	};
	class Land_ConcreteKerb_02_4m_F: Pavements_base_F
	{
		scope = 1;//2
	};
	class Land_ConcreteKerb_02_8m_F: Pavements_base_F
	{
		scope = 1;//2
	};
	class Land_ConcreteKerb_03_BW_long_F: Pavements_base_F
	{
		scope = 1;//2
	};
	class Land_ConcreteKerb_03_BW_short_F: Pavements_base_F
	{
		scope = 1;//2
	};
	class Land_ConcreteKerb_03_BY_long_F: Pavements_base_F
	{
		scope = 1;//2
	};
	class Land_ConcreteKerb_03_BY_short_F: Pavements_base_F
	{
		scope = 1;//2
	};
	class Land_GardenPavement_01_F: Pavements_base_F
	{
		scope = 1;//2
	};
	class Land_GardenPavement_02_F: Pavements_base_F
	{
		scope = 1;//2
	};
	class Land_KerbIsland_01_start_F: Pavements_base_F
	{
		scope = 1;//2
	};
	class Land_KerbIsland_01_end_F: Pavements_base_F
	{
		scope = 1;//2
	};
	class Land_Sidewalk_01_4m_F: Pavements_base_F
	{
		scope = 1;//2
	};
	class Land_Sidewalk_01_8m_F: Pavements_base_F
	{
		scope = 1;//2
	};
	class Land_Sidewalk_01_corner_F: Pavements_base_F
	{
		scope = 1;//2
	};
	class Land_Sidewalk_01_narrow_2m_F: Pavements_base_F
	{
		scope = 1;//2
	};
	class Land_Sidewalk_01_narrow_4m_F: Pavements_base_F
	{
		scope = 1;//2
	};
	class Land_Sidewalk_01_narrow_8m_F: Pavements_base_F
	{
		scope = 1;//2
	};
	class Land_Sidewalk_02_4m_F: Pavements_base_F
	{
		scope = 1;//2
	};
	class Land_Sidewalk_02_8m_F: Pavements_base_F
	{
		scope = 1;//2
	};
	class Land_Sidewalk_02_corner_F: Pavements_base_F
	{
		scope = 1;//2
	};
	class Land_Sidewalk_02_narrow_2m_F: Pavements_base_F
	{
		scope = 1;//2
	};
	class Land_Sidewalk_02_narrow_4m_F: Pavements_base_F
	{
		scope = 1;//2
	};
	class Land_Sidewalk_02_narrow_8m_F: Pavements_base_F
	{
		scope = 1;//2
	};
	class Land_PowerLine_01_pole_end_v1_F: PowerLines_Small_base_F
	{
//		scope = 1;
	};
	class Land_PowerLine_01_pole_end_v2_F: PowerLines_Small_base_F
	{
//		scope = 1;
	};
	class Land_PowerLine_01_pole_junction_F: PowerLines_Small_base_F
	{
//		scope = 1;
	};
	class Land_PowerLine_01_pole_lamp_F: PowerLines_Small_base_F
	{
//		scope = 1;
	};
	class Land_PowerLine_01_pole_lamp_off_F: PowerLines_Small_base_F
	{
//		scope = 1;
	};
	class Land_PowerLine_01_pole_small_F: PowerLines_Small_base_F
	{
//		scope = 1;
	};
	class Land_PowerLine_01_pole_tall_F: PowerLines_Small_base_F
	{
//		scope = 1;
	};
	class Land_PowerLine_01_pole_transformer_F: PowerLines_Small_base_F
	{
//		scope = 1;
	};
	class Land_PowerLine_01_wire_50m_F: PowerLines_Wires_base_F
	{
//		scope = 1;
	};
	class Land_PowerLine_01_wire_50m_main_F: PowerLines_Wires_base_F
	{
//		scope = 1;
	};
	class Land_Track_01_3m_F: Infrastructure_base_F
	{
		scope = 1;//2
	};
	class Land_Track_01_7deg_F: Infrastructure_base_F
	{
		scope = 1;//2
	};
	class Land_Track_01_10m_F: Infrastructure_base_F
	{
		scope = 1;//2
	};
	class Land_Track_01_15deg_F: Infrastructure_base_F
	{
		scope = 1;//2
	};
	class Land_Track_01_20m_F: Infrastructure_base_F
	{
		scope = 1;//2
	};
	class Land_Track_01_30deg_F: Infrastructure_base_F
	{
		scope = 1;//2
	};
	class Land_Track_01_bridge_F: Infrastructure_base_F
	{
		scope = 1;//2
	};
	class Land_Track_01_bumper_F: Infrastructure_base_F
	{
		scope = 1;//2
	};
	class Land_Track_01_crossing_F: Infrastructure_base_F
	{
		scope = 1;//2
	};
	class Land_Track_01_switch_F: Infrastructure_base_F
	{
		scope = 1;//2
	};
	class Land_Track_01_turnout_left_F: Infrastructure_base_F
	{
		scope = 1;//2
	};
	class Land_Track_01_turnout_right_F: Infrastructure_base_F
	{
		scope = 1;//2
	};
	class Land_SewerCover_01_F: Infrastructure_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SewerCover_02_F: Infrastructure_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SewerCover_03_F: Infrastructure_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WaterTank_01_F: Infrastructure_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WaterTank_02_F: Infrastructure_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WaterTank_03_F: Infrastructure_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WaterTank_04_F: Infrastructure_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WaterTower_01_F: Infrastructure_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WindmillPump_01_F: Infrastructure_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Barracks_01_camo_F: Land_i_Barracks_V1_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class CamoNet_ghex_F: CamoNet_BLUFOR_F
	{
		scope = 1;//2
	};
	class CamoNet_ghex_Curator_F: CamoNet_ghex_F
	{
//		scope = 1;
		scopeCurator = 1;//2
	};
	class CamoNet_ghex_open_Curator_F: CamoNet_ghex_open_F
	{
//		scope = 1;
		scopeCurator = 1;//2
	};
	class CamoNet_ghex_big_Curator_F: CamoNet_ghex_big_F
	{
//		scope = 1;
		scopeCurator = 1;//2
	};
	class Land_Cargo_House_V4_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Cargo_House_V4_F: Cargo_House_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo_HQ_V4_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Cargo_HQ_V4_F: Cargo_HQ_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo_Patrol_V4_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Cargo_Patrol_V4_F: Cargo_Patrol_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo_Tower_V4_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Cargo_Tower_V4_F: Cargo_Tower_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_EmplacementGun_01_mossy_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_EmplacementGun_01_rusty_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_EmplacementGun_01_d_mossy_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_EmplacementGun_01_d_rusty_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BagBunker_01_large_green_F: Land_BagBunker_Large_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BagBunker_01_small_green_F: Land_BagBunker_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BagFence_01_corner_green_F: Land_BagFence_Corner_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BagFence_01_end_green_F: Land_BagFence_End_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BagFence_01_long_green_F: Land_BagFence_Long_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BagFence_01_round_green_F: Land_BagFence_Round_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BagFence_01_short_green_F: Land_BagFence_Short_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HBarrier_01_big_4_green_F: Land_HBarrierBig_F
	{
		scope = 1;//2
	};
	class Land_HBarrier_01_big_tower_green_F: Land_HBarrierTower_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HBarrier_01_line_1_green_F: Land_HBarrier_1_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HBarrier_01_line_3_green_F: Land_HBarrier_3_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HBarrier_01_line_5_green_F: Land_HBarrier_5_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HBarrier_01_tower_green_F: Land_BagBunker_Tower_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HBarrier_01_wall_4_green_F: Land_HBarrierWall4_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HBarrier_01_wall_6_green_F: Land_HBarrierWall6_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HBarrier_01_wall_corner_green_F: Land_HBarrierWall_corner_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_HBarrier_01_wall_corridor_green_F: Land_HBarrierWall_corridor_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PillboxBunker_01_big_F: BagBunker_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PillboxBunker_01_hex_F: BagBunker_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PillboxBunker_01_rectangle_F: BagBunker_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PillboxWall_01_3m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PillboxWall_01_3m_round_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PillboxWall_01_6m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PillboxWall_01_6m_round_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_TrenchFrame_01_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Trench_01_forest_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Trench_01_grass_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Canal_Dutch_01_15m_F: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Canal_Dutch_01_bridge_F: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Canal_Dutch_01_corner_F: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Canal_Dutch_01_plate_F: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Canal_Dutch_01_stairs_F: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Breakwater_01_F: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Breakwater_02_F: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_QuayConcrete_01_5m_ladder_F: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_QuayConcrete_01_20m_F: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_QuayConcrete_01_20m_wall_F: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_QuayConcrete_01_innerCorner_F: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_QuayConcrete_01_outterCorner_F: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_QuayConcrete_01_pier_F: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PierConcrete_01_4m_ladders_F: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PierConcrete_01_16m_F: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PierConcrete_01_30deg_F: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PierConcrete_01_end_F: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PierConcrete_01_steps_F: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PierWooden_01_10m_noRails_F: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PierWooden_01_16m_F: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PierWooden_01_dock_F: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PierWooden_01_hut_F: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PierWooden_01_ladder_F: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PierWooden_01_platform_F: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PierWooden_02_16m_F: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PierWooden_02_30deg_F: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PierWooden_02_barrel_F: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PierWooden_02_hut_F: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PierWooden_02_ladder_F: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PierWooden_03_F: Piers_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_AttachedSign_01_v1_F: AttachedSigns_base_F
	{
//		scope = 1;
	};
	class Land_AttachedSign_01_v2_F: AttachedSigns_base_F
	{
//		scope = 1;
	};
	class Land_AttachedSign_01_v3_F: AttachedSigns_base_F
	{
//		scope = 1;
	};
	class Land_AttachedSign_01_v4_F: AttachedSigns_base_F
	{
//		scope = 1;
	};
	class Land_AttachedSign_02_v1_F: AttachedSigns_base_F
	{
//		scope = 1;
	};
	class Land_AttachedSign_02_v2_F: AttachedSigns_base_F
	{
//		scope = 1;
	};
	class Land_AttachedSign_02_v3_F: AttachedSigns_base_F
	{
//		scope = 1;
	};
	class Land_AttachedSign_02_v4_F: AttachedSigns_base_F
	{
//		scope = 1;
	};
	class Land_AttachedSign_03_v1_F: AttachedSigns_base_F
	{
//		scope = 1;
	};
	class Land_AttachedSign_03_v2_F: AttachedSigns_base_F
	{
//		scope = 1;
	};
	class Land_Billboard_01_v1_F: AttachedSigns_base_F
	{
//		scope = 1;
	};
	class Land_Billboard_01_v2_F: AttachedSigns_base_F
	{
//		scope = 1;
	};
	class Land_Billboard_02_v1_F: AttachedSigns_base_F
	{
//		scope = 1;
	};
	class Land_Billboard_02_v2_F: AttachedSigns_base_F
	{
//		scope = 1;
	};
	class Land_CornerAds_01_v1_F: AttachedSigns_base_F
	{
//		scope = 1;
	};
	class Land_CornerAds_01_v2_F: AttachedSigns_base_F
	{
//		scope = 1;
	};
	class Land_CornerAds_01_v3_F: AttachedSigns_base_F
	{
//		scope = 1;
	};
	class Land_CornerAds_01_v4_F: AttachedSigns_base_F
	{
//		scope = 1;
	};
	class Land_CornerAds_02_v1_F: AttachedSigns_base_F
	{
//		scope = 1;
	};
	class Land_CornerAds_02_v2_F: AttachedSigns_base_F
	{
//		scope = 1;
	};
	class Land_CorporateSign_01_mine_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_CorporateSign_01_scf_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_CorporateSign_02_airport_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_CorporateSign_02_harbor_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_CorporateSign_03_ranch_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignWarning_02_crocodiles_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignCommand_01_giveWay_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignCommand_01_goStraight_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignCommand_01_priority_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignCommand_01_stop_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignCommand_01_turnLeft_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignCommand_01_turnLeftAhead_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignCommand_01_turnRight_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignCommand_01_turnRightAhead_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignInfo_01_deadEnd_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignInfo_01_firstAid_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignInfo_01_fuelStation_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignInfo_01_hotel_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignInfo_01_pedestrianCrossing_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignInfo_01_pedestrianZone_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignInfo_01_picnicSite_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignInfo_01_reduceSpeed_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignInfo_01_refreshments_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignInfo_01_restaurant_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignInfo_01_taxi_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignInfo_01_toilet_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignRestrict_01_noDriving_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignRestrict_01_noEntry_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignRestrict_01_noOvertaking_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignRestrict_01_noOvertakingTruck_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignRestrict_01_noParking_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignRestrict_01_noParkingAltEven_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignRestrict_01_noParkingAltOdd_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignRestrict_01_noPassage_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignRestrict_01_noPollutingVehicles_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignRestrict_01_noPoweredVehicles_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignRestrict_01_noStopping_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignRestrict_01_speedLimit_30_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignRestrict_01_speedLimit_50_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignRestrict_01_speedLimit_70_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignRestrict_01_speedLimit_80_d_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignRestrict_01_speedLimit_80_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignRestrict_01_speedLimit_end_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignRestrict_01_speedLimit_national_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignWarning_01_aircrafts_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignWarning_01_animals_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignWarning_01_bend_left_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignWarning_01_bend_right_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignWarning_01_crossRoad_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignWarning_01_doubleBend_left_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignWarning_01_doubleBend_right_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignWarning_01_herds_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignWarning_01_chippings_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignWarning_01_junction_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignWarning_01_junctionRoW_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignWarning_01_other_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignWarning_01_pedestrianCrossing_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignWarning_01_ramp_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignWarning_01_roadworks_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignWarning_01_rocks_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignWarning_01_steepDown_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignWarning_01_steepUp_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignWarning_01_stopOnRequest_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_SignWarning_01_unevenRoad_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_Bollard_01_F: Signs_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ParkingMeter_01_F: Signs_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_TownColumn_01_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_Sign_01_sharpBend_left_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_Sign_01_sharpBend_right_F: Signs_base_F
	{
//		scope = 1;
	};
	class Land_VergePost_01_F: Signs_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BackAlley_01_l_1m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BackAlley_01_l_gap_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BackAlley_01_l_gate_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BackAlley_02_l_1m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BambooFence_01_s_4m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BambooFence_01_s_8m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BambooFence_01_s_d_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BambooFence_01_s_pole_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ConcreteWall_01_l_4m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ConcreteWall_01_l_8m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ConcreteWall_01_l_d_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ConcreteWall_01_l_gate_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ConcreteWall_01_l_pole_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ConcreteWall_01_m_4m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ConcreteWall_01_m_8m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ConcreteWall_01_m_d_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ConcreteWall_01_m_gate_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ConcreteWall_01_m_pole_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ConcreteWall_02_m_2m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ConcreteWall_02_m_4m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ConcreteWall_02_m_8m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ConcreteWall_02_m_d_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ConcreteWall_02_m_gate_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ConcreteWall_02_m_pole_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CrashBarrier_01_end_L_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CrashBarrier_01_end_R_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CrashBarrier_01_4m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CrashBarrier_01_8m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Hedge_01_s_2m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Hedge_01_s_4m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_NetFence_01_m_4m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_NetFence_01_m_4m_noLC_F: Wall_F
	{
//		scope = 1;
	};
	class Land_NetFence_01_m_8m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_NetFence_01_m_8m_noLC_F: Wall_F
	{
//		scope = 1;
	};
	class Land_NetFence_01_m_d_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_NetFence_01_m_d_noLC_F: Wall_F
	{
//		scope = 1;
	};
	class Land_NetFence_01_m_gate_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_NetFence_01_m_pole_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_NetFence_02_m_2m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_NetFence_02_m_4m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_NetFence_02_m_8m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_NetFence_02_m_d_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_NetFence_02_m_gate_v1_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_NetFence_02_m_gate_v2_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_NetFence_02_m_pole_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PipeFence_01_m_2m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PipeFence_01_m_4m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PipeFence_01_m_8m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PipeFence_01_m_d_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PipeFence_01_m_gate_v1_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PipeFence_01_m_gate_v2_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PipeFence_01_m_pole_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PipeFence_02_s_4m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PipeFence_02_s_4m_noLC_F: Wall_F
	{
//		scope = 1;
	};
	class Land_PipeFence_02_s_8m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PipeFence_02_s_8m_noLC_F: Wall_F
	{
//		scope = 1;
	};
	class Land_PoleWall_01_pole_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PoleWall_01_3m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PoleWall_01_6m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_GuardRailing_01_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SlumWall_01_s_2m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SlumWall_01_s_4m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_StoneWall_01_s_10m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_StoneWall_01_s_d_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_TinWall_01_m_4m_v1_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_TinWall_01_m_4m_v2_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_TinWall_01_m_gate_v1_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_TinWall_01_m_gate_v2_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_TinWall_01_m_pole_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_TinWall_02_l_4m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_TinWall_02_l_8m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_TinWall_02_l_pole_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WiredFence_01_4m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WiredFence_01_8m_d_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WiredFence_01_8m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WiredFence_01_16m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WiredFence_01_gate_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WiredFence_01_pole_45_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WiredFence_01_pole_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WoodenWall_01_m_4m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WoodenWall_01_m_8m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WoodenWall_01_m_d_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WoodenWall_01_m_pole_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WoodenWall_02_s_2m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WoodenWall_02_s_4m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WoodenWall_02_s_8m_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WoodenWall_02_s_d_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WoodenWall_02_s_gate_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WoodenWall_02_s_pole_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Carrier_01_base_F: StaticShip
	{
		scope = 1;//2
	};
	class Land_Carrier_01_hull_base_F: House_F
	{
//		scope = 1;
	};
	class DynamicAirport_01_F: AirportBase
	{
//		scope = 1;
	};
	class MapBoard_Malden_F: Land_MapBoard_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class MapBoard_Tanoa_F: Land_MapBoard_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Map_Malden_F: Land_Map_blank_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Map_Tanoa_F: Land_Map_blank_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Map_unfolded_Altis_F: Land_Map_unfolded_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Map_unfolded_Malden_F: Land_Map_unfolded_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Map_unfolded_Tanoa_F: Land_Map_unfolded_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Laptop_02_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Laptop_02_unfolded_F: Land_Laptop_02_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BusStop_01_shelter_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BusStop_01_sign_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Addon_02_b_white_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_i_Addon_02_b_white_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_GarbageBarrel_01_english_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_House_Big_01_b_blue_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_House_Big_01_b_brown_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_House_Big_01_b_pink_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_House_Big_01_b_yellow_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_i_House_Big_01_b_blue_F: i_House_Big_01_b_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_House_Big_01_b_brown_F: i_House_Big_01_b_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_House_Big_01_b_pink_F: i_House_Big_01_b_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_House_Big_01_b_white_F: i_House_Big_01_b_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_House_Big_01_b_whiteblue_F: i_House_Big_01_b_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_House_Big_01_b_yellow_F: i_House_Big_01_b_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_House_Big_02_b_blue_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_House_Big_02_b_brown_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_House_Big_02_b_pink_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_House_Big_02_b_yellow_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_i_House_Big_02_b_blue_F: i_House_Big_02_b_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_House_Big_02_b_brown_F: i_House_Big_02_b_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_House_Big_02_b_pink_F: i_House_Big_02_b_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_House_Big_02_b_white_F: i_House_Big_02_b_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_House_Big_02_b_whiteblue_F: i_House_Big_02_b_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_House_Big_02_b_yellow_F: i_House_Big_02_b_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_House_Small_01_b_blue_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_House_Small_01_b_brown_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_House_Small_01_b_pink_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_House_Small_01_b_yellow_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_i_House_Small_01_b_blue_F: i_House_Small_01_b_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_House_Small_01_b_brown_F: i_House_Small_01_b_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_House_Small_01_b_pink_F: i_House_Small_01_b_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_House_Small_01_b_white_F: i_House_Small_01_b_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_House_Small_01_b_whiteblue_F: i_House_Small_01_b_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_House_Small_01_b_yellow_F: i_House_Small_01_b_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_House_Small_02_b_blue_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_House_Small_02_b_brown_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_House_Small_02_b_pink_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_House_Small_02_b_V1_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_House_Small_02_b_yellow_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_i_House_Small_02_b_blue_F: i_House_Small_02_b_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_House_Small_02_b_brown_F: i_House_Small_02_b_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_House_Small_02_b_pink_F: i_House_Small_02_b_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_House_Small_02_b_white_F: i_House_Small_02_b_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_House_Small_02_b_whiteblue_F: i_House_Small_02_b_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_House_Small_02_b_yellow_F: i_House_Small_02_b_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_House_Small_02_c_blue_F: i_House_Small_02_c_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_House_Small_02_c_brown_F: i_House_Small_02_c_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_House_Small_02_c_pink_F: i_House_Small_02_c_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_House_Small_02_c_white_F: i_House_Small_02_c_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_House_Small_02_c_whiteblue_F: i_House_Small_02_c_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_House_Small_02_c_yellow_F: i_House_Small_02_c_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_Stone_House_Big_01_b_clay_F: i_Stone_House_Big_01_b_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Stone_Shed_01_b_clay_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Stone_Shed_01_b_raw_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Stone_Shed_01_b_white_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_i_Stone_Shed_01_b_clay_F: i_Stone_Shed_01_b_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_Stone_Shed_01_b_raw_F: i_Stone_Shed_01_b_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_Stone_Shed_01_b_white_F: i_Stone_Shed_01_b_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_Stone_Shed_01_c_clay_F: i_Stone_Shed_01_c_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_Stone_Shed_01_c_raw_F: i_Stone_Shed_01_c_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_Stone_Shed_01_c_white_F: i_Stone_Shed_01_c_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Unfinished_Building_01_noLadder_F: House_F
	{
//		scope = 1;
	};
	class Land_ATM_01_malden_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ATM_02_malden_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PhoneBooth_01_malden_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PhoneBooth_02_malden_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Billboard_02_blank_F: Billboard_02_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Billboard_02_carrental_F: Billboard_02_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Billboard_02_chernarus_F: Billboard_02_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Billboard_02_chevre2_F: Billboard_02_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Billboard_02_ion_F: Billboard_02_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Billboard_02_leader_F: Billboard_02_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Billboard_02_mars2_F: Billboard_02_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Billboard_02_monte_F: Billboard_02_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Billboard_02_redstone_F: Billboard_02_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Billboard_02_surreal_F: Billboard_02_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Billboard_02_wine_F: Billboard_02_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Billboard_03_aan_F: Billboard_03_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Billboard_03_action_F: Billboard_03_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Billboard_03_argois_F: Billboard_03_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Billboard_03_blank_F: Billboard_03_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Billboard_03_bluking_F: Billboard_03_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Billboard_03_duckit_F: Billboard_03_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Billboard_03_getlost_F: Billboard_03_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Billboard_03_cheese_F: Billboard_03_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Billboard_03_chevre_F: Billboard_03_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Billboard_03_ionbase_F: Billboard_03_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Billboard_03_koke_F: Billboard_03_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Billboard_03_lyfe_F: Billboard_03_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Billboard_03_mars_F: Billboard_03_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Billboard_03_maskrtnik_F: Billboard_03_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Billboard_03_olives_F: Billboard_03_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Billboard_03_pate_F: Billboard_03_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Billboard_03_pills_F: Billboard_03_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Billboard_03_plane_F: Billboard_03_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Billboard_03_supermarket_F: Billboard_03_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Billboard_03_winery_F: Billboard_03_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Billboard_03_ygont_F: Billboard_03_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Billboard_04_blank_F: Billboard_04_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Billboard_04_koke_redstone_F: Billboard_04_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Billboard_04_mars_lyfe_F: Billboard_04_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Billboard_04_supermarket_maskrtnik_F: Billboard_04_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FuelStation_01_prices_malevil_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FuelStation_01_pump_malevil_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_FuelStation_01_roof_malevil_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Shop_02_b_blue_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Shop_02_b_brown_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Shop_02_b_pink_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Shop_02_b_yellow_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_i_Shop_02_b_blue_F: i_Shop_02_b_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_Shop_02_b_brown_F: i_Shop_02_b_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_Shop_02_b_pink_F: i_Shop_02_b_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_Shop_02_b_white_F: i_Shop_02_b_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_Shop_02_b_whiteblue_F: i_Shop_02_b_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_i_Shop_02_b_yellow_F: i_Shop_02_b_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Supermarket_01_malden_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Church_01_V2_F: Church_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Monument_01_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Pedestal_01_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Pedestal_02_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Statue_01_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Statue_02_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_DirtPatch_01_4x4_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_DirtPatch_01_6x8_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PedestrianCrossing_01_6m_4str_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PedestrianCrossing_01_6m_6str_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_PedestrianCrossing_01_8m_10str_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Puddle_01_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Puddle_02_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_RoadCrack_01_2x2_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_RoadCrack_01_4x4_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_RoadCrack_01_6x2_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Barn_01_brown_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Barn_01_brown_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Barn_01_grey_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Barn_01_grey_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Shed_08_brown_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Shed_08_brown_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Shed_08_grey_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Shed_08_grey_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_VineyardFence_01_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WoodenPlanks_01_pine_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WoodenPlanks_01_messy_pine_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Runway_end00_F: House_F
	{
//		scope = 1;
	};
	class Land_Runway_end18_F: House_F
	{
//		scope = 1;
	};
	class Land_Lighthouse_01_noLight_F: House_F
	{
//		scope = 1;
	};
	class Land_Lighthouse_03_green_F: Land_Lighthouse_03_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Lighthouse_03_red_F: Land_Lighthouse_03_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_ReservoirTank_01_military_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_ReservoirTank_01_military_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Bunker_01_big_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Bunker_01_blocks_1_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Bunker_01_blocks_3_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Bunker_01_HQ_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Bunker_01_small_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Bunker_01_tall_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Cargo_House_V3_derelict_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Cargo_HQ_V3_derelict_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Cargo_Patrol_V3_derelict_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Cargo_Tower_V3_derelict_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Dome_01_big_green_ruins_v1_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Dome_01_big_green_ruins_v2_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Dome_01_small_green_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_DomeDebris_01_hex_damaged_green_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_DomeDebris_01_hex_green_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_DomeDebris_01_struts_large_green_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_DomeDebris_01_struts_small_green_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_Barricade_01_4m_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Barricade_01_10m_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_CzechHedgehog_01_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SandbagBarricade_01_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SandbagBarricade_01_half_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SandbagBarricade_01_hole_F: House_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Turret_01_ruins_F: Ruins_F
	{
		scope = 1;//2
	};
	class Land_SignM_forRent_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SignM_forSale_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SignM_taxi_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SignM_WarningMilAreaSmall_english_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SignM_WarningMilitaryArea_english_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_SignM_WarningMilitaryVehicles_english_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WallCity_01_4m_blue_F: WallCity_01_4m_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WallCity_01_4m_grey_F: WallCity_01_4m_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WallCity_01_4m_pink_F: WallCity_01_4m_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WallCity_01_4m_whiteblue_F: WallCity_01_4m_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WallCity_01_4m_yellow_F: WallCity_01_4m_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WallCity_01_4m_plain_blue_F: WallCity_01_4m_plain_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WallCity_01_4m_plain_grey_F: WallCity_01_4m_plain_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WallCity_01_4m_plain_pink_F: WallCity_01_4m_plain_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WallCity_01_4m_plain_whiteblue_F: WallCity_01_4m_plain_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WallCity_01_4m_plain_yellow_F: WallCity_01_4m_plain_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WallCity_01_4m_plain_dmg_blue_F: WallCity_01_4m_plain_dmg_base_F
	{
//		scope = 1;
	};
	class Land_WallCity_01_4m_plain_dmg_grey_F: WallCity_01_4m_plain_dmg_base_F
	{
//		scope = 1;
	};
	class Land_WallCity_01_4m_plain_dmg_pink_F: WallCity_01_4m_plain_dmg_base_F
	{
//		scope = 1;
	};
	class Land_WallCity_01_4m_plain_dmg_whiteblue_F: WallCity_01_4m_plain_dmg_base_F
	{
//		scope = 1;
	};
	class Land_WallCity_01_4m_plain_dmg_yellow_F: WallCity_01_4m_plain_dmg_base_F
	{
//		scope = 1;
	};
	class Land_WallCity_01_8m_blue_F: WallCity_01_8m_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WallCity_01_8m_grey_F: WallCity_01_8m_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WallCity_01_8m_pink_F: WallCity_01_8m_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WallCity_01_8m_whiteblue_F: WallCity_01_8m_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WallCity_01_8m_yellow_F: WallCity_01_8m_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WallCity_01_8m_dmg_blue_F: WallCity_01_8m_dmg_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WallCity_01_8m_dmg_grey_F: WallCity_01_8m_dmg_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WallCity_01_8m_dmg_pink_F: WallCity_01_8m_dmg_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WallCity_01_8m_dmg_whiteblue_F: WallCity_01_8m_dmg_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WallCity_01_8m_dmg_yellow_F: WallCity_01_8m_dmg_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WallCity_01_8m_plain_blue_F: WallCity_01_8m_plain_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WallCity_01_8m_plain_grey_F: WallCity_01_8m_plain_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WallCity_01_8m_plain_pink_F: WallCity_01_8m_plain_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WallCity_01_8m_plain_whiteblue_F: WallCity_01_8m_plain_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WallCity_01_8m_plain_yellow_F: WallCity_01_8m_plain_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WallCity_01_gate_blue_F: WallCity_01_gate_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WallCity_01_gate_grey_F: WallCity_01_gate_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WallCity_01_gate_pink_F: WallCity_01_gate_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WallCity_01_gate_whiteblue_F: WallCity_01_gate_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WallCity_01_gate_yellow_F: WallCity_01_gate_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WallCity_01_pillar_blue_F: WallCity_01_pillar_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WallCity_01_pillar_grey_F: WallCity_01_pillar_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WallCity_01_pillar_pink_F: WallCity_01_pillar_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WallCity_01_pillar_whiteblue_F: WallCity_01_pillar_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WallCity_01_pillar_yellow_F: WallCity_01_pillar_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_WallCity_01_pillar_plain_dmg_blue_F: WallCity_01_pillar_plain_dmg_base_F
	{
//		scope = 1;
	};
	class Land_WallCity_01_pillar_plain_dmg_grey_F: WallCity_01_pillar_plain_dmg_base_F
	{
//		scope = 1;
	};
	class Land_WallCity_01_pillar_plain_dmg_pink_F: WallCity_01_pillar_plain_dmg_base_F
	{
//		scope = 1;
	};
	class Land_WallCity_01_pillar_plain_dmg_whiteblue_F: WallCity_01_pillar_plain_dmg_base_F
	{
//		scope = 1;
	};
	class Land_WallCity_01_pillar_plain_dmg_yellow_F: WallCity_01_pillar_plain_dmg_base_F
	{
//		scope = 1;
	};
	class Land_ConcreteWall_01_l_gate_closed_F: Wall_F
	{
//		scope = 1;
	};
	class Land_ConcreteWall_01_m_gate_closed_F: Wall_F
	{
//		scope = 1;
	};
	class Land_Mil_WallBig_4m_battered_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Mil_WallBig_corner_battered_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Mil_WallBig_4m_damaged_center_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Mil_WallBig_4m_damaged_left_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Mil_WallBig_4m_damaged_right_F: Wall_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Mil_WallBig_debris_F: House_Small_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_NetFence_02_m_gate_v1_closed_F: Wall_F
	{
//		scope = 1;
	};
	class Land_NetFence_02_m_gate_v2_closed_F: Wall_F
	{
//		scope = 1;
	};
	class Land_PipeFence_01_m_gate_v1_closed_F: Wall_F
	{
//		scope = 1;
	};
	class Land_PipeFence_01_m_gate_v2_closed_F: Wall_F
	{
//		scope = 1;
	};
	class Land_TinWall_01_m_gate_v1_closed_F: Wall_F
	{
//		scope = 1;
	};
	class Land_TinWall_01_m_gate_v2_closed_F: Wall_F
	{
//		scope = 1;
	};
	class Land_WoodenWall_02_s_gate_closed_F: Wall_F
	{
//		scope = 1;
	};
	class B_Patrol_Respawn_tent_F: Land_TentDome_F
	{
//		scope = 1;
	};

	class Land_DragonsTeeth_01_1x1_new_F: HBarrier_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_DragonsTeeth_01_1x1_old_F: HBarrier_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_DragonsTeeth_01_4x2_new_F: HBarrier_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_DragonsTeeth_01_4x2_old_F: HBarrier_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_CzechHedgehog_01_new_F: ThingX
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_ConcreteHedgehog_01_F: ThingX
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_ConcreteHedgehog_01_half_F: ThingX
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_ConcreteHedgehog_01_palette_F: ThingX
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_RepairDepot_01_civ_ruins_F: Land_RepairDepot_01_base_ruins_F
	{
		scope = 1;//2;
	};
	class Land_RepairDepot_01_tan_ruins_F: Land_RepairDepot_01_base_ruins_F
	{
		scope = 1;//2;
	};
	class Land_RepairDepot_01_green_ruins_F: Land_RepairDepot_01_base_ruins_F
	{
		scope = 1;//2;
	};
	class Land_RepairDepot_01_civ_F: Land_RepairDepot_01_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_RepairDepot_01_tan_F: Land_RepairDepot_01_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_RepairDepot_01_green_F: Land_RepairDepot_01_base_F
	{
		scope = 1;//2;
		scopeCurator = 1;//2;
	};
	class Land_Destroyer_01_base_F: StaticShip
	{
		scope = 1;//2
	};
	class Land_DirtPatch_02_F: Land_DirtPatch_02_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_DirtPatch_03_F: Land_DirtPatch_02_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_DirtPatch_04_F: Land_DirtPatch_02_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BriefingRoomDesk_01_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_BriefingRoomScreen_01_F: Items_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_Destroyer_01_Boat_Rack_01_F: Land_Destroyer_01_Boat_Rack_01_Base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};

	class Land_DragonsTeeth_01_1x1_new_redwhite_F: HBarrier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_DragonsTeeth_01_1x1_old_redwhite_F: HBarrier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_DragonsTeeth_01_4x2_new_redwhite_F: HBarrier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Land_DragonsTeeth_01_4x2_old_redwhite_F: HBarrier_base_F
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
};
