// PvPscene (pvpscene@web.de)

//ToughChopperRotor script by kju
//Used to avoid broken rotor for helicopters (normally from hard landings)

sleep 1;

private ["_chopper","_damage"];

_chopper = vehicle player;

if ((_chopper isKindOf "Helicopter") and (alive player)) then
{
	while {((driver _chopper) == player) && (alive player)} do
	{
		sleep(0.1);
		if (!(canMove _chopper)) then
		{
			_damage = getDammage _chopper;
			if (_damage < 1) then
			{
				_chopper setDamage _damage;
			};
		};
	};
};