class CfgPatches
{
	class DisabledFatigue_Gameplay_C_PvPscene
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Data_F_Loadorder","A3_Data_F_Curator_Loadorder","A3_Data_F_Kart_Loadorder","A3_Data_F_Bootcamp_Loadorder","A3_Data_F_Heli_Loadorder","A3_Data_F_Mark_Loadorder","A3_Data_F_Exp_A_Loadorder","A3_Data_F_Exp_B_Loadorder","A3_Data_F_Exp_Loadorder","A3_Data_F_Jets_Loadorder","A3_Data_F_Argo_Loadorder","A3_Data_F_Patrol_Loadorder","A3_Data_F_Orange_Loadorder","A3_Data_F_Tacops_Loadorder","A3_Data_F_Tank_Loadorder"};
		version = "2019-03-28";
	};
};
/*
class CfgFatigue
{
	MinValue1 = 0.1;
	MinValue2 = 1;
	NormalRunSpeed = 7.2;
	TiredRunSpeedLimit = 0.8;
};
*/
/*
class CfgMovesFatigue
{
	staminaDuration = 999999;
	staminaCooldown = 0;
	staminaRestoration = 1;
	aimPrecisionSpeedCoef = 1;
//	terrainDrainSprint = -1;
//	terrainDrainRun = -1;
//	terrainSpeedCoef = 0.9;
};
*/
class CfgMovesBasic
{
	class Default
	{
		stamina = 0;//1;
	};
	class StandBase;
};
class CfgMovesMaleSdr: CfgMovesBasic
{
	class States
	{
		class AadjPpneMstpSrasWpstDleft;
		class AadjPpneMstpSrasWpstDright;
		class AadjPpneMstpSrasWrflDleft;
		class AadjPpneMstpSrasWrflDright;
		class AbdvPercMstpSnonWnonDnon;
		class AbdvPercMstpSnonWrflDnon;
		class AbswPercMstpSnonWnonDnon;
		class AdvePercMstpSnonWnonDnon;
		class AdvePercMstpSnonWrflDnon;
		class AidlPercMstpSrasWlnrDnon_G0S;
		class AidlPercMstpSrasWpstDnon_G0S;
		class AidlPknlMstpSlowWrflDnon_G0S;
		class AidlPknlMstpSrasWlnrDnon_G0S;
		class AidlPknlMstpSrasWpstDnon_G0S;
		class AidlPpneMstpSnonWnonDnon_G0S;
		class AidlPpneMstpSrasWpstDnon_G0S;
		class AmovPercMstpSlowWlnrDnon;
		class AmovPercMstpSlowWrflDnon;
		class AmovPercMstpSnonWnonDnon;
		class AmovPercMstpSoptWbinDnon;
		class AmovPercMstpSrasWlnrDnon;
		class AmovPercMstpSrasWpstDnon;
		class AmovPercMstpSrasWrflDnon;
		class AmovPercMwlkSlowWpstDf;
		class AmovPercMwlkSlowWrflDf_ver2;
		class AmovPercMwlkSlowWrflDfl_ver2;
		class AmovPercMwlkSoptWbinDf;
		class AmovPercMwlkSrasWpstDf;
		class AmovPercMwlkSrasWrflDf;
		class AmovPknlMstpSnonWnonDnon;
		class AmovPknlMstpSoptWbinDnon;
		class AmovPknlMstpSrasWpstDnon;
		class AmovPknlMstpSrasWrflDnon;
		class AmovPknlMwlkSlowWpstDf;
		class AmovPknlMwlkSoptWbinDf;
		class AmovPknlMwlkSrasWpstDf;
		class AmovPpneMstpSnonWnonDnon;
		class AmovPpneMstpSoptWbinDnon;
		class AmovPpneMstpSrasWpstDnon;
		class AmovPpneMstpSrasWrflDnon;
		class AmovPpneMwlkSoptWbinDf;
		class AsdvPercMstpSnonWnonDnon;
		class AsdvPercMstpSnonWrflDnon;
		class AsswPercMstpSnonWnonDnon;
		class AswmPercMstpSnonWnonDnon;

		class SprintBaseDf: StandBase
		{
			stamina = 0;//-1;
		};
		class AovrPercMstpSlowWrflDf: AmovPercMstpSlowWrflDnon
		{
			stamina = 0;//0;
		};
		class AovrPercMstpSrasWrflDf: AmovPercMstpSrasWrflDnon
		{
			stamina = 0;//0;
		};
		class AmovPercMstpSrasWrflDnon_AadjPpneMstpSrasWrflDright: AadjPpneMstpSrasWrflDright
		{
			stamina = 0;//0;
		};
		class AmovPercMstpSrasWrflDnon_AadjPpneMstpSrasWrflDleft: AadjPpneMstpSrasWrflDleft
		{
			stamina = 0;//0;
		};
		class AmovPknlMstpSrasWrflDnon_AadjPpneMstpSrasWrflDright: AadjPpneMstpSrasWrflDright
		{
			stamina = 0;//0;
		};
		class AmovPknlMstpSrasWrflDnon_AadjPpneMstpSrasWrflDleft: AadjPpneMstpSrasWrflDleft
		{
			stamina = 0;//0;
		};
		class AadjPpneMstpSrasWrflDleft_AmovPercMstpSrasWrflDnon: AmovPercMstpSrasWrflDnon
		{
			stamina = 0;//0;
		};
		class AadjPpneMstpSrasWrflDleft_AmovPknlMstpSrasWrflDnon: AmovPknlMstpSrasWrflDnon
		{
			stamina = 0;//0;
		};
		class AovrPercMstpSrasWpstDf: AidlPercMstpSrasWpstDnon_G0S
		{
			stamina = 0;//0;
		};
		class AmovPknlMtacSlowWpstDf: AmovPknlMwlkSlowWpstDf
		{
			stamina = 0;//-0.1;
		};
		class AmovPknlMrunSlowWpstDf: AmovPknlMwlkSlowWpstDf
		{
			stamina = 0;//-0.1;
		};
		class AmovPercMstpSrasWpstDnon_AadjPpneMstpSrasWpstDright: AadjPpneMstpSrasWpstDright
		{
			stamina = 0;//0;
		};
		class AmovPercMstpSrasWpstDnon_AadjPpneMstpSrasWpstDleft: AadjPpneMstpSrasWpstDleft
		{
			stamina = 0;//0;
		};
		class AmovPknlMstpSrasWpstDnon_AadjPpneMstpSrasWpstDright: AadjPpneMstpSrasWpstDright
		{
			stamina = 0;//0;
		};
		class AmovPknlMstpSrasWpstDnon_AadjPpneMstpSrasWpstDleft: AadjPpneMstpSrasWpstDleft
		{
			stamina = 0;//0;
		};
		class AadjPpneMstpSrasWpstDleft_AmovPercMstpSrasWpstDnon: AmovPercMstpSrasWpstDnon
		{
			stamina = 0;//0;
		};
		class AadjPpneMstpSrasWpstDleft_AmovPknlMstpSrasWpstDnon: AmovPknlMstpSrasWpstDnon
		{
			stamina = 0;//0;
		};
		class AovrPercMstpSnonWnonDf: AmovPercMstpSnonWnonDnon
		{
			stamina = 0;//0;
		};
		class AmovPercMrunSlowWrflDf: AmovPercMstpSlowWrflDnon
		{
			stamina = 0;//-0.1;
		};
		class AmovPercMtacSrasWrflDf: AmovPercMwlkSrasWrflDf
		{
			stamina = 0;//-0.1;
		};
		class AmovPercMtacSlowWrflDf_ver2: AmovPercMwlkSlowWrflDf_ver2
		{
			stamina = 0;//-0.1;
		};
		class AmovPercMtacSlowWrflDfl_ver2: AmovPercMwlkSlowWrflDfl_ver2
		{
			stamina = 0;//-0.1;
		};
		class AmovPercMrunSrasWrflDf: AmovPercMstpSrasWrflDnon
		{
			stamina = 0;//-0.1;
		};
		class AmovPercMrunSnonWnonDf: AmovPercMstpSnonWnonDnon
		{
			stamina = 0;//-0.1;
		};
		class AmovPknlMrunSlowWrflDf: AidlPknlMstpSlowWrflDnon_G0S
		{
			stamina = 0;//-0.1;
		};
		class AmovPknlMtacSlowWrflDf: AmovPknlMrunSlowWrflDf
		{
			stamina = 0;//-0.1;
		};
		class AmovPknlMrunSrasWrflDf: AmovPknlMstpSrasWrflDnon
		{
			stamina = 0;//-0.1;
		};
		class AmovPknlMtacSrasWrflDf: AmovPknlMrunSrasWrflDf
		{
			stamina = 0;//-0.1;
		};
		class AmovPercMtacSlowWpstDf: AmovPercMwlkSlowWpstDf
		{
			stamina = 0;//-0.1;
		};
		class AmovPercMtacSrasWpstDf: AmovPercMwlkSrasWpstDf
		{
			stamina = 0;//-0.1;
		};
		class AmovPercMrunSrasWpstDf: AmovPercMstpSrasWpstDnon
		{
			stamina = 0;//-0.1;
		};
		class AmovPercMrunSlowWpstDf: AmovPercMrunSrasWpstDf
		{
			stamina = 0;//-0.1;
		};
		class AmovPknlMrunSrasWpstDf: AidlPknlMstpSrasWpstDnon_G0S
		{
			stamina = 0;//-0.1;
		};
		class AmovPknlMtacSrasWpstDf: AmovPknlMwlkSrasWpstDf
		{
			stamina = 0;//-0.1;
		};
		class AmovPercMrunSlowWlnrDf: AidlPknlMstpSrasWlnrDnon_G0S
		{
			stamina = 0;//-0.1;
		};
		class AmovPknlMrunSrasWlnrDf: AmovPercMrunSlowWlnrDf
		{
			stamina = 0;//-0.1;
		};
		class AmovPercMevaSlowWlnrDf: AmovPercMrunSlowWlnrDf
		{
			stamina = 0;//-1;
		};
		class AmovPknlMevaSlowWlnrDf: AmovPercMrunSlowWlnrDf
		{
			stamina = 0;//-1;
		};
		class AmovPknlMtacSrasWlnrDf: AidlPknlMstpSrasWlnrDnon_G0S
		{
			stamina = 0;//-0.1;
		};
		class AmovPknlMrunSnonWnonDf: AmovPknlMstpSnonWnonDnon
		{
			stamina = 0;//-0.1;
		};
		class AmovPknlMwlkSnonWnonDf: AmovPknlMrunSnonWnonDf
		{
			stamina = 0;//1;
		};
		class AmovPpneMrunSlowWrflDf: AmovPpneMstpSrasWrflDnon
		{
			stamina = 0;//-0.1;
		};
		class AmovPpneMevaSlowWrflDf: AmovPpneMrunSlowWrflDf
		{
			stamina = 0;//-1;
		};
		class AmovPpneMrunSlowWpstDf: AidlPpneMstpSrasWpstDnon_G0S
		{
			stamina = 0;//-0.1;
		};
		class AmovPpneMsprSlowWpstDf: AmovPpneMrunSlowWpstDf
		{
			stamina = 0;//-1;
		};
		class AmovPpneMrunSnonWnonDf: AidlPpneMstpSnonWnonDnon_G0S
		{
			stamina = 0;//-0.1;
		};
		class AmovPpneMsprSnonWnonDf: AmovPpneMrunSnonWnonDf
		{
			stamina = 0;//-1;
		};
		class TransAnimBase: Default
		{
			stamina = 0;//0;
		};
		class AmovPpneMstpSrasWlnrDnon_AmovPercMstpSrasWlnrDnon: TransAnimBase
		{
			stamina = 0;//0;
		};
		class AmovPpneMstpSrasWlnrDnon_AmovPknlMstpSrasWlnrDnon: TransAnimBase
		{
			stamina = 0;//0;
		};
		class AmovPercMstpSrasWlnrDnon_AmovPpneMstpSnonWnonDnon: TransAnimBase
		{
			stamina = 0;//0;
		};
		class AmovPercMstpSrasWrflDnon_AmovPpneMstpSrasWrflDnon: AmovPpneMstpSrasWrflDnon
		{
			stamina = 0;//0;
		};
		class AmovPercMstpSlowWrflDnon_AmovPpneMstpSrasWrflDnon: AmovPpneMstpSrasWrflDnon
		{
			stamina = 0;//0;
		};
		class AmovPknlMstpSlowWrflDnon_AmovPpneMstpSrasWrflDnon: AmovPpneMstpSrasWrflDnon
		{
			stamina = 0;//0;
		};
		class AmovPercMsprSlowWrflDf_AmovPpneMstpSrasWrflDnon: TransAnimBase
		{
			stamina = 0;//0;
		};
		class AmovPercMrunSlowWrflDf_AmovPpneMstpSrasWrflDnon: AmovPercMstpSrasWrflDnon
		{
			stamina = 0;//0;
		};
		class AmovPercMrunSlowWrflDf_AmovPpneMstpSrasWrflDnon_end: AmovPpneMstpSrasWrflDnon
		{
			stamina = 0;//0;
		};
		class AmovPercMrunSlowWrflDf_AmovPpneMstpSrasWrflDnon_old: AmovPpneMstpSrasWrflDnon
		{
			stamina = 0;//0;
		};
		class AmovPknlMstpSrasWrflDnon_AmovPpneMstpSrasWrflDnon: AmovPpneMstpSrasWrflDnon
		{
			stamina = 0;//0;
		};
		class AmovPpneMstpSrasWrflDnon_AmovPercMstpSrasWrflDnon: AmovPercMstpSrasWrflDnon
		{
			stamina = 0;//0;
		};
		class AmovPpneMstpSrasWrflDnon_AmovPercMsprSlowWrflDf: TransAnimBase
		{
			stamina = 0;//0;
		};
		class AmovPpneMstpSrasWrflDnon_AmovPknlMstpSrasWrflDnon: AmovPknlMstpSrasWrflDnon
		{
			stamina = 0;//0;
		};
		class AmovPercMsprSlowWpstDf_AmovPpneMstpSrasWpstDnon: TransAnimBase
		{
			stamina = 0;//0;
		};
		class AmovPercMsprSlowWpstDf_AmovPpneMstpSrasWpstDnon_2: AmovPpneMstpSrasWpstDnon
		{
			stamina = 0;//0;
		};
		class AmovPercMstpSrasWpstDnon_AmovPpneMstpSrasWpstDnon: AmovPpneMstpSrasWpstDnon
		{
			stamina = 0;//0;
		};
		class AmovPknlMstpSrasWpstDnon_AmovPpneMstpSrasWpstDnon: AmovPpneMstpSrasWpstDnon
		{
			stamina = 0;//0;
		};
		class AmovPpneMstpSrasWpstDnon_AmovPercMstpSrasWpstDnon: AmovPercMstpSrasWpstDnon
		{
			stamina = 0;//0;
		};
		class AmovPpneMstpSrasWpstDnon_AmovPknlMstpSrasWpstDnon: AmovPknlMstpSrasWpstDnon
		{
			stamina = 0;//0;
		};
		class AmovPpneMstpSrasWpstDnon_AmovPercMsprSlowWpstDf: TransAnimBase
		{
			stamina = 0;//0;
		};
		class AmovPercMsprSnonWnonDf_AmovPpneMstpSnonWnonDnon: TransAnimBase
		{
			stamina = 0;//0;
		};
		class AmovPercMsprSnonWnonDf_AmovPpneMstpSnonWnonDnon_2: AmovPpneMstpSnonWnonDnon
		{
			stamina = 0;//0;
		};
		class AmovPercMstpSnonWnonDnon_AmovPpneMstpSnonWnonDnon: AmovPpneMstpSnonWnonDnon
		{
			stamina = 0;//0;
		};
		class AmovPknlMstpSnonWnonDnon_AmovPpneMstpSnonWnonDnon: AmovPpneMstpSnonWnonDnon
		{
			stamina = 0;//0;
		};
		class AmovPpneMstpSnonWnonDnon_AmovPercMstpSnonWnonDnon: AmovPercMstpSnonWnonDnon
		{
			stamina = 0;//0;
		};
		class AmovPpneMstpSnonWnonDnon_AmovPknlMstpSnonWnonDnon: AmovPknlMstpSnonWnonDnon
		{
			stamina = 0;//0;
		};
		class AmovPpneMstpSnonWnonDnon_AmovPercMsprSnonWnonDf: TransAnimBase
		{
			stamina = 0;//0;
		};
		class AmovPpneMstpSrasWrflDnon_AmovPpneMevaSlowWrflDl: AmovPpneMstpSrasWrflDnon
		{
			stamina = 0;//-1;
		};
		class AmovPpneMstpSrasWpstDnon_AmovPpneMevaSlowWpstDl: AmovPpneMstpSrasWpstDnon
		{
			stamina = 0;//-1;
		};
		class AmovPpneMstpSnonWnonDnon_AmovPpneMevaSnonWnonDl: AidlPpneMstpSnonWnonDnon_G0S
		{
			stamina = 0;//-1;
		};
		class AmovPpneMstpSnonWnonDnon_AmovPpneMevaSnonWnonDr: AidlPpneMstpSnonWnonDnon_G0S
		{
			stamina = 0;//-1;
		};
		class AswmPercMstpSnonWnonDnon_goup: AswmPercMstpSnonWnonDnon
		{
			stamina = 0;//-0.1;
		};
		class AswmPercMstpSnonWnonDnon_godown: AswmPercMstpSnonWnonDnon
		{
			stamina = 0;//-0.1;
		};
		class AswmPercMrunSnonWnonDf: AswmPercMstpSnonWnonDnon
		{
			stamina = 0;//-0.1;
		};
		class AsswPercMstpSnonWnonDnon_goup: AsswPercMstpSnonWnonDnon
		{
			stamina = 0;//-0.1;
		};
		class AsswPercMstpSnonWnonDnon_goDown: AsswPercMstpSnonWnonDnon
		{
			stamina = 0;//-0.1;
		};
		class AsswPercMrunSnonWnonDf: AsswPercMstpSnonWnonDnon
		{
			stamina = 0;//0;
		};
		class AbswPercMstpSnonWnonDnon_goup: AbswPercMstpSnonWnonDnon
		{
			stamina = 0;//-0.1;
		};
		class AbswPercMstpSnonWnonDnon_goDown: AbswPercMstpSnonWnonDnon
		{
			stamina = 0;//-0.1;
		};
		class AbswPercMrunSnonWnonDf: AbswPercMstpSnonWnonDnon
		{
			stamina = 0;//-0.1;
		};
		class AdvePercMstpSnonWnonDnon_goup: AdvePercMstpSnonWnonDnon
		{
			stamina = 0;//-0.1;
		};
		class AdvePercMstpSnonWnonDnon_godown: AdvePercMstpSnonWnonDnon
		{
			stamina = 0;//-0.1;
		};
		class AdvePercMwlkSnonWnonDf: AdvePercMstpSnonWnonDnon
		{
			stamina = 0;//-0.1;
		};
		class AdvePercMrunSnonWnonDf: AdvePercMstpSnonWnonDnon
		{
			stamina = 0;//-0.1;
		};
		class AdvePercMsprSnonWnonDf: AdvePercMrunSnonWnonDf
		{
			stamina = 0;//-1;
		};
		class AdvePercMstpSnonWrflDnon_goup: AdvePercMstpSnonWrflDnon
		{
			stamina = 0;//-0.1;
		};
		class AdvePercMstpSnonWrflDnon_godown: AdvePercMstpSnonWrflDnon
		{
			stamina = 0;//-0.1;
		};
		class AdvePercMwlkSnonWrflDf: AdvePercMstpSnonWrflDnon
		{
			stamina = 0;//-0.1;
		};
		class AdvePercMrunSnonWrflDf: AdvePercMstpSnonWrflDnon
		{
			stamina = 0;//-0.1;
		};
		class AdvePercMsprSnonWrflDf: AdvePercMrunSnonWrflDf
		{
			stamina = 0;//-1;
		};
		class AsdvPercMstpSnonWnonDnon_goup: AsdvPercMstpSnonWnonDnon
		{
			stamina = 0;//-0.1;
		};
		class AsdvPercMstpSnonWnonDnon_godown: AsdvPercMstpSnonWnonDnon
		{
			stamina = 0;//-0.1;
		};
		class AsdvPercMwlkSnonWnonDf: AsdvPercMstpSnonWnonDnon
		{
			stamina = 0;//-0.1;
		};
		class AsdvPercMtacSnonWnonDf: AsdvPercMstpSnonWnonDnon
		{
			stamina = 0;//-0.1;
		};
		class AsdvPercMrunSnonWnonDf: AsdvPercMstpSnonWnonDnon
		{
			stamina = 0;//-0.1;
		};
		class AsdvPercMsprSnonWnonDf: AsdvPercMrunSnonWnonDf
		{
			stamina = 0;//-1;
		};
		class AsdvPercMstpSnonWrflDnon_goup: AsdvPercMstpSnonWrflDnon
		{
			stamina = 0;//-0.1;
		};
		class AsdvPercMstpSnonWrflDnon_godown: AsdvPercMstpSnonWrflDnon
		{
			stamina = 0;//-0.1;
		};
		class AsdvPercMwlkSnonWrflDf: AsdvPercMstpSnonWrflDnon
		{
			stamina = 0;//-0.1;
		};
		class AsdvPercMtacSnonWrflDf: AsdvPercMstpSnonWrflDnon
		{
			stamina = 0;//-0.1;
		};
		class AsdvPercMrunSnonWrflDf: AsdvPercMstpSnonWrflDnon
		{
			stamina = 0;//-0.1;
		};
		class AsdvPercMsprSnonWrflDf: AsdvPercMrunSnonWrflDf
		{
			stamina = 0;//-1;
		};
		class AbdvPercMstpSnonWnonDnon_goup: AbdvPercMstpSnonWnonDnon
		{
			stamina = 0;//-0.1;
		};
		class AbdvPercMstpSnonWnonDnon_godown: AbdvPercMstpSnonWnonDnon
		{
			stamina = 0;//-0.1;
		};
		class AbdvPercMwlkSnonWnonDf: AbdvPercMstpSnonWnonDnon
		{
			stamina = 0;//-0.1;
		};
		class AbdvPercMtacSnonWnonDf: AbdvPercMstpSnonWnonDnon
		{
			stamina = 0;//-0.1;
		};
		class AbdvPercMrunSnonWnonDf: AbdvPercMstpSnonWnonDnon
		{
			stamina = 0;//-0.1;
		};
		class AbdvPercMsprSnonWnonDf: AbdvPercMrunSnonWnonDf
		{
			stamina = 0;//-1;
		};
		class AbdvPercMstpSnonWrflDnon_goup: AbdvPercMstpSnonWrflDnon
		{
			stamina = 0;//-0.1;
		};
		class AbdvPercMstpSnonWrflDnon_godown: AbdvPercMstpSnonWrflDnon
		{
			stamina = 0;//-0.1;
		};
		class AbdvPercMwlkSnonWrflDf: AbdvPercMstpSnonWrflDnon
		{
			stamina = 0;//-0.1;
		};
		class AbdvPercMtacSnonWrflDf: AbdvPercMstpSnonWrflDnon
		{
			stamina = 0;//-0.1;
		};
		class AbdvPercMrunSnonWrflDf: AbdvPercMstpSnonWrflDnon
		{
			stamina = 0;//-0.1;
		};
		class AbdvPercMsprSnonWrflDf: AbdvPercMrunSnonWrflDf
		{
			stamina = 0;//-1;
		};
		class AmovPercMrunSnonWbinDf: AmovPercMwlkSoptWbinDf
		{
			stamina = 0;//-0.1;
		};
		class AmovPercMevaSnonWbinDf: AmovPercMrunSnonWbinDf
		{
			stamina = 0;//-1;
		};
		class AovrPercMstpSoptWbinDf: AmovPercMstpSoptWbinDnon
		{
			stamina = 0;//0;
		};
		class AmovPknlMrunSnonWbinDf: AmovPknlMwlkSoptWbinDf
		{
			stamina = 0;//-0.1;
		};
		class AmovPknlMevaSnonWbinDf: AmovPknlMrunSnonWbinDf
		{
			stamina = 0;//-1;
		};
		class AmovPpneMrunSnonWbinDf: AmovPpneMwlkSoptWbinDf
		{
			stamina = 0;//-0.1;
		};
		class AmovPpneMevaSnonWbinDf: AmovPpneMrunSnonWbinDf
		{
			stamina = 0;//-1;
		};
		class AmovPercMstpSoptWbinDnon_AmovPpneMstpSoptWbinDnon: AmovPpneMstpSoptWbinDnon
		{
			stamina = 0;//0;
		};
		class AmovPknlMstpSoptWbinDnon_AmovPpneMstpSoptWbinDnon: AmovPpneMstpSoptWbinDnon
		{
			stamina = 0;//0;
		};
		class AmovPpneMstpSoptWbinDnon_AmovPercMstpSoptWbinDnon: AmovPercMstpSoptWbinDnon
		{
			stamina = 0;//0;
		};
		class AmovPpneMstpSoptWbinDnon_AmovPknlMstpSoptWbinDnon: AmovPknlMstpSoptWbinDnon
		{
			stamina = 0;//0;
		};
		class AovrPercMstpSrasWlnrDf: AmovPercMstpSrasWlnrDnon
		{
			stamina = 0;//0;
		};
		class AovrPercMstpSlowWlnrDf: AmovPercMstpSlowWlnrDnon
		{
			stamina = 0;//0;
		};
		class AmovPercMtacSrasWlnrDf: AidlPercMstpSrasWlnrDnon_G0S
		{
			stamina = 0;//-0.1;
		};
	};
};
/*
class CfgMovesBasic
{
	class Default
	{
		duty = -10;
		affectedByFatigue = 0;
	};
	class HealBase: Default
	{
		duty = -10;
	};
	class InjuredMovedBase;
	class StandBase;
};
class CfgMovesMaleSdr: CfgMovesBasic
{
	class States
	{
		class AadjPercMstpSrasWpstDdown;
		class AadjPercMstpSrasWpstDleft;
		class AadjPercMstpSrasWpstDright;
		class AadjPercMstpSrasWpstDup;
		class AadjPercMstpSrasWrflDdown;
		class AadjPercMstpSrasWrflDleft;
		class AadjPercMstpSrasWrflDright;
		class AadjPercMstpSrasWrflDup;
		class AadjPknlMstpSrasWpstDdown;
		class AadjPknlMstpSrasWpstDleft;
		class AadjPknlMstpSrasWpstDright;
		class AadjPknlMstpSrasWpstDup;
		class AadjPknlMstpSrasWrflDdown;
		class AadjPknlMstpSrasWrflDleft;
		class AadjPknlMstpSrasWrflDright;
		class AadjPknlMstpSrasWrflDup;
		class AadjPpneMstpSrasWpstDup;
		class AadjPpneMstpSrasWrflDup;
		class AbdvPercMstpSnonWnonDnon;
		class AbdvPercMstpSnonWrflDnon;
		class AbswPercMstpSnonWnonDnon;
		class AcinPercMrunSnonWnonDf_death;
		class AcinPercMrunSrasWrflDf_death;
		class AdvePercMstpSnonWnonDnon;
		class AdvePercMstpSnonWrflDnon;
		class AidlPercMstpSlowWpstDnon_G0S;
		class AidlPercMstpSlowWrflDnon_G0S;
		class AidlPercMstpSnonWnonDnon_G0S;
		class AidlPercMstpSrasWlnrDnon_G0S;
		class AidlPercMstpSrasWpstDnon_G0S;
		class AidlPercMstpSrasWrflDnon_G0S;
		class AidlPknlMstpSlowWpstDnon_G0S;
		class AidlPknlMstpSlowWrflDnon_G0S;
		class AidlPknlMstpSrasWlnrDnon_G0S;
		class AidlPknlMstpSrasWpstDnon_G0S;
		class AidlPpneMstpSnonWnonDnon_G0S;
		class AidlPpneMstpSrasWpstDnon_G0S;
		class AidlPpneMstpSrasWrflDnon_G0S;
		class AinjPfalMstpSnonWnonDnon_carried_Up;
		class AinjPfalMstpSnonWrflDnon_carried_Up;
		class AinjPpneMstpSnonWnonDnon;
		class AinjPpneMstpSnonWrflDnon;
		class AmovPercMrunSlowWlnrDfr;
		class AmovPercMstpSlowWlnrDnon;
		class AmovPercMstpSoptWbinDnon;
		class AmovPercMstpSrasWpstDnon_AmovPercMstpSrasWrflDnon_end;
		class AmovPercMstpSrasWrflDnon_AmovPercMstpSrasWpstDnon_end;
		class AmovPercMwlkSlowWrflDfl_ver2;
		class AmovPknlMrunSlowWrflDb;
		class AmovPknlMrunSlowWrflDbl;
		class AmovPknlMrunSlowWrflDbr;
		class AmovPknlMrunSlowWrflDfl;
		class AmovPknlMrunSlowWrflDfr;
		class AmovPknlMrunSlowWrflDl;
		class AmovPknlMrunSlowWrflDr;
		class AmovPknlMstpSoptWbinDnon;
		class AmovPpneMstpSnonWnonDnon_injured;
		class AmovPpneMstpSoptWbinDnon;
		class AmovPpneMstpSrasWrflDnon_injured;
		class ApanPercMstpSnonWnonDnon;
		class ApanPknlMstpSnonWnonDnon;
		class ApanPpneMstpSnonWnonDnon;
		class AsdvPercMstpSnonWnonDnon;
		class AsdvPercMstpSnonWrflDnon;
		class AsswPercMstpSnonWnonDnon;
		class AwopPercMstpSoptWbinDnon_lnr;
		class AwopPercMstpSoptWbinDnon_non;
		class AwopPercMstpSoptWbinDnon_pst;
		class AwopPercMstpSoptWbinDnon_rfl;
		class AwopPknlMstpSoptWbinDnon_lnr;
		class AwopPknlMstpSoptWbinDnon_non;
		class AwopPknlMstpSoptWbinDnon_pst;
		class AwopPknlMstpSoptWbinDnon_rfl;
		class AwopPpneMstpSoptWbinDnon_lnr;
		class AwopPpneMstpSoptWbinDnon_non;
		class AwopPpneMstpSoptWbinDnon_pst;
		class AwopPpneMstpSoptWbinDnon_rfl;
		class CutSceneAnimationBase;
		class SprintCivilBaseDf;
		class TransAnimBase;
		class TransAnimBase_noIK;

		class SprintBaseDf: StandBase
		{
			duty = -10;
		};
		class AmovPercMstpSlowWrflDnon: StandBase
		{
			duty = -10;
		};
		class AovrPercMstpSlowWrflDf: AmovPercMstpSlowWrflDnon
		{
			duty = -10;
		};
		class AmovPercMstpSlowWrflDnon_turnL: AidlPercMstpSlowWrflDnon_G0S
		{
			duty = -10;
		};
		class AmovPercMstpSlowWrflDnon_turnR: AidlPercMstpSlowWrflDnon_G0S
		{
			duty = -10;
		};
		class AmovPercMstpSrasWrflDnon: AmovPercMstpSlowWrflDnon
		{
			duty = -10;
		};
		class AmovPercMstpSrasWrflDnon_turnL: AidlPercMstpSrasWrflDnon_G0S
		{
			duty = -10;
		};
		class AmovPercMstpSrasWrflDnon_turnR: AidlPercMstpSrasWrflDnon_G0S
		{
			duty = -10;
		};
		class AovrPercMstpSrasWrflDf: AmovPercMstpSrasWrflDnon
		{
			duty = -10;
		};
		class AovrPercMrunSrasWrflDf: AovrPercMstpSrasWrflDf
		{
			duty = -10;
		};
		class AmovPknlMstpSlowWrflDnon: AmovPercMstpSlowWrflDnon
		{
			duty = -10;
		};
		class AmovPknlMstpSlowWrflDnon_turnL: AidlPknlMstpSlowWrflDnon_G0S
		{
			duty = -10;
		};
		class AmovPknlMstpSlowWrflDnon_turnR: AidlPknlMstpSlowWrflDnon_G0S
		{
			duty = -10;
		};
		class AmovPknlMstpSrasWrflDnon: AmovPknlMstpSlowWrflDnon
		{
			duty = -10;
		};
		class AmovPknlMstpSrasWrflDnon_turnL: AmovPknlMstpSrasWrflDnon
		{
			duty = -10;
		};
		class AmovPknlMstpSrasWrflDnon_turnR: AmovPknlMstpSrasWrflDnon
		{
			duty = -10;
		};
		class AmovPpneMstpSrasWrflDnon: AmovPercMstpSrasWrflDnon
		{
			duty = -10;
		};
		class AmovPpneMstpSrasWrflDnon_turnL: AidlPpneMstpSrasWrflDnon_G0S
		{
			duty = -10;
		};
		class AmovPpneMstpSrasWrflDnon_turnR: AidlPpneMstpSrasWrflDnon_G0S
		{
			duty = -10;
		};
		class AmovPercMstpSrasWpstDnon: StandBase
		{
			duty = -10;
		};
		class AovrPercMstpSrasWpstDf: AidlPercMstpSrasWpstDnon_G0S
		{
			duty = -10;
		};
		class AmovPercMstpSlowWpstDnon: AmovPercMstpSrasWpstDnon
		{
			duty = -10;
		};
		class AmovPercMstpSlowWpstDnon_turnL: AidlPercMstpSlowWpstDnon_G0S
		{
			duty = -10;
		};
		class AmovPercMstpSlowWpstDnon_turnR: AidlPercMstpSlowWpstDnon_G0S
		{
			duty = -10;
		};
		class AmovPercMstpSrasWpstDnon_turnL: AmovPercMstpSrasWpstDnon
		{
			duty = -10;
		};
		class AmovPercMstpSrasWpstDnon_turnR: AmovPercMstpSrasWpstDnon
		{
			duty = -10;
		};
		class AmovPknlMstpSrasWpstDnon: AmovPercMstpSrasWpstDnon
		{
			duty = -10;
		};
		class AmovPknlMstpSlowWpstDnon: AmovPknlMstpSrasWpstDnon
		{
			duty = -10;
		};
		class AmovPknlMstpSlowWpstDnon_turnL: AidlPknlMstpSlowWpstDnon_G0S
		{
			duty = -10;
		};
		class AmovPknlMstpSlowWpstDnon_turnR: AidlPknlMstpSlowWpstDnon_G0S
		{
			duty = -10;
		};
		class AmovPknlMwlkSlowWpstDf: AidlPknlMstpSlowWpstDnon_G0S
		{
			duty = -10;
		};
		class AmovPknlMtacSlowWpstDf: AmovPknlMwlkSlowWpstDf
		{
			duty = -10;
		};
		class AmovPknlMrunSlowWpstDf: AmovPknlMwlkSlowWpstDf
		{
			duty = -10;
		};
		class AmovPknlMstpSrasWpstDnon_turnL: AmovPknlMstpSrasWpstDnon
		{
			duty = -10;
		};
		class AmovPknlMstpSrasWpstDnon_turnR: AmovPknlMstpSrasWpstDnon
		{
			duty = -10;
		};
		class AmovPpneMstpSrasWpstDnon: AmovPercMstpSrasWpstDnon
		{
			duty = -10;
		};
		class AmovPpneMstpSrasWpstDnon_turnL: AidlPpneMstpSrasWpstDnon_G0S
		{
			duty = -10;
		};
		class AmovPpneMstpSrasWpstDnon_turnR: AidlPpneMstpSrasWpstDnon_G0S
		{
			duty = -10;
		};
		class AmovPknlMstpSrasWlnrDnon: Default
		{
			duty = -10;
		};
		class ReloadRPGKneel: AidlPknlMstpSrasWlnrDnon_G0S
		{
			duty = -10;
		};
		class ReloadRPG7VKneel: AidlPknlMstpSrasWlnrDnon_G0S
		{
			duty = -10;
		};
		class AmovPknlMstpSrasWlnrDnon_turnL: AidlPknlMstpSrasWlnrDnon_G0S
		{
			duty = -10;
		};
		class AmovPknlMstpSrasWlnrDnon_turnR: AidlPknlMstpSrasWlnrDnon_G0S
		{
			duty = -10;
		};
		class AmovPpneMstpSrasWlnrDnon: AidlPknlMstpSrasWlnrDnon_G0S
		{
			duty = -10;
		};
		class AmovPpneMrunSrasWlnrDf: AmovPpneMstpSrasWlnrDnon
		{
			duty = -10;
		};
		class AmovPercMstpSnonWnonDnon: StandBase
		{
			duty = -10;
		};
		class AovrPercMstpSnonWnonDf: AmovPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AmovPknlMstpSnonWnonDnon: AmovPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AmovPpneMstpSnonWnonDnon: AmovPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AmovPercMwlkSlowWrflDf_ver2: AmovPercMstpSlowWrflDnon
		{
			duty = -10;
		};
		class AmovPercMrunSlowWrflDf: AmovPercMstpSlowWrflDnon
		{
			duty = -10;
		};
		class AmovPercMrunSlowWrflDfl: AmovPercMrunSlowWrflDf
		{
			duty = -10;
		};
		class AmovPercMrunSlowWrflDbl: AmovPercMrunSlowWrflDfl
		{
			duty = -10;
		};
		class AmovPercMrunSlowWrflDb: AmovPercMrunSlowWrflDfl
		{
			duty = -10;
		};
		class AmovPercMrunSlowWrflDbr: AmovPercMrunSlowWrflDfl
		{
			duty = -10;
		};
		class AmovPercMrunSlowWrflDfr: AmovPercMrunSlowWrflDfl
		{
			duty = -10;
		};
		class AmovPercMwlkSrasWrflDf: AmovPercMstpSrasWrflDnon
		{
			duty = -10;
		};
		class AmovPercMtacSrasWrflDf: AmovPercMwlkSrasWrflDf
		{
			duty = -10;
		};
		class AmovPercMtacSlowWrflDf_ver2: AmovPercMwlkSlowWrflDf_ver2
		{
			duty = -10;
		};
		class AmovPercMtacSlowWrflDfl_ver2: AmovPercMwlkSlowWrflDfl_ver2
		{
			duty = -10;
		};
		class AmovPercMrunSrasWrflDf: AmovPercMstpSrasWrflDnon
		{
			duty = -10;
		};
		class AmovPercMwlkSnonWnonDf: AidlPercMstpSnonWnonDnon_G0S
		{
			duty = -10;
		};
		class AmovPercMrunSnonWnonDf: AmovPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AmovPknlMwlkSlowWrflDf: AidlPknlMstpSlowWpstDnon_G0S
		{
			duty = -10;
		};
		class AmovPknlMrunSlowWrflDf: AidlPknlMstpSlowWrflDnon_G0S
		{
			duty = -10;
		};
		class AmovPknlMtacSlowWrflDf: AmovPknlMrunSlowWrflDf
		{
			duty = -10;
		};
		class AmovPknlMtacSlowWrflDfl: AmovPknlMrunSlowWrflDfl
		{
			duty = -10;
		};
		class AmovPknlMtacSlowWrflDl: AmovPknlMrunSlowWrflDl
		{
			duty = -10;
		};
		class AmovPknlMtacSlowWrflDbl: AmovPknlMrunSlowWrflDbl
		{
			duty = -10;
		};
		class AmovPknlMtacSlowWrflDb: AmovPknlMrunSlowWrflDb
		{
			duty = -10;
		};
		class AmovPknlMtacSlowWrflDbr: AmovPknlMrunSlowWrflDbr
		{
			duty = -10;
		};
		class AmovPknlMtacSlowWrflDr: AmovPknlMrunSlowWrflDr
		{
			duty = -10;
		};
		class AmovPknlMtacSlowWrflDfr: AmovPknlMrunSlowWrflDfr
		{
			duty = -10;
		};
		class AmovPknlMwlkSrasWrflDf: AmovPknlMstpSrasWrflDnon
		{
			duty = -10;
		};
		class AmovPknlMrunSrasWrflDf: AmovPknlMstpSrasWrflDnon
		{
			duty = -10;
		};
		class AmovPknlMtacSrasWrflDf: AmovPknlMrunSrasWrflDf
		{
			duty = -10;
		};
		class AmovPercMwlkSrasWpstDf: AmovPercMstpSrasWpstDnon
		{
			duty = -10;
		};
		class AmovPercMwlkSlowWpstDf: AmovPercMwlkSrasWpstDf
		{
			duty = -10;
		};
		class AmovPercMtacSlowWpstDf: AmovPercMwlkSlowWpstDf
		{
			duty = -10;
		};
		class AmovPercMtacSrasWpstDf: AmovPercMwlkSrasWpstDf
		{
			duty = -10;
		};
		class AmovPercMrunSrasWpstDf: AmovPercMstpSrasWpstDnon
		{
			duty = -10;
		};
		class AmovPknlMwlkSrasWpstDf: AmovPknlMstpSrasWpstDnon
		{
			duty = -10;
		};
		class AmovPknlMrunSrasWpstDf: AidlPknlMstpSrasWpstDnon_G0S
		{
			duty = -10;
		};
		class AmovPknlMtacSrasWpstDf: AmovPknlMwlkSrasWpstDf
		{
			duty = -10;
		};
		class AmovPercMrunSlowWlnrDf: AidlPknlMstpSrasWlnrDnon_G0S
		{
			duty = -10;
		};
		class AmovPknlMrunSrasWlnrDf: AmovPercMrunSlowWlnrDf
		{
			duty = -10;
		};
		class AmovPknlMrunSrasWlnrDfr: AmovPercMrunSlowWlnrDfr
		{
			duty = -10;
		};
		class AmovPercMevaSlowWlnrDf: AmovPercMrunSlowWlnrDf
		{
			duty = -10;
		};
		class AmovPknlMevaSlowWlnrDf: AmovPercMrunSlowWlnrDf
		{
			duty = -10;
		};
		class AmovPknlMwlkSrasWlnrDf: AidlPknlMstpSrasWlnrDnon_G0S
		{
			duty = -10;
		};
		class AmovPknlMtacSrasWlnrDf: AidlPknlMstpSrasWlnrDnon_G0S
		{
			duty = -10;
		};
		class AmovPknlMrunSnonWnonDf: AmovPknlMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AmovPknlMwlkSnonWnonDf: AmovPknlMrunSnonWnonDf
		{
			duty = -10;
		};
		class AmovPpneMrunSlowWrflDf: AmovPpneMstpSrasWrflDnon
		{
			duty = -10;
		};
		class AmovPpneMsprSlowWrflDf: AmovPpneMrunSlowWrflDf
		{
			duty = -10;
		};
		class AmovPpneMevaSlowWrflDf: AmovPpneMrunSlowWrflDf
		{
			duty = -10;
		};
		class AmovPpneMsprSlowWrflDf_injured: AmovPpneMstpSrasWrflDnon_injured
		{
			duty = -10;
		};
		class AmovPpneMrunSlowWpstDf: AidlPpneMstpSrasWpstDnon_G0S
		{
			duty = -10;
		};
		class AmovPpneMsprSlowWpstDf: AmovPpneMrunSlowWpstDf
		{
			duty = -10;
		};
		class AmovPpneMrunSnonWnonDf: AidlPpneMstpSnonWnonDnon_G0S
		{
			duty = -10;
		};
		class AmovPpneMsprSnonWnonDf: AmovPpneMrunSnonWnonDf
		{
			duty = -10;
		};
		class RifleReloadProneBase: Default
		{
			duty = -10;
		};
		class WeaponMagazineReloadStand: Default
		{
			duty = -10;
		};
		class PistolMagazineReloadStand: Default
		{
			duty = -10;
		};
		class LauncherReloadKneel: Default
		{
			duty = -10;
		};
		class AadjPpneMstpSrasWrflDup_AadjPknlMstpSrasWrflDdown: AadjPknlMstpSrasWrflDdown
		{
			duty = -10;
		};
		class AadjPknlMstpSrasWrflDdown_AadjPpneMstpSrasWrflDup: AadjPpneMstpSrasWrflDup
		{
			duty = -10;
		};
		class AmovPercMstpSrasWlnrDnon_AmovPercMstpSrasWrflDnon: TransAnimBase
		{
			duty = -10;
		};
		class AmovPercMstpSrasWlnrDnon_AmovPercMstpSrasWrflDnon_end: TransAnimBase
		{
			duty = -10;
		};
		class AmovPercMstpSrasWrflDnon_AmovPercMstpSrasWlnrDnon_end: TransAnimBase
		{
			duty = -10;
		};
		class AmovPercMstpSrasWrflDnon_AmovPercMstpSrasWlnrDnon: TransAnimBase_noIK
		{
			duty = -10;
		};
		class AmovPercMstpSrasWlnrDnon_AmovPknlMstpSrasWlnrDnon: TransAnimBase
		{
			duty = -10;
		};
		class AmovPpneMstpSrasWlnrDnon_AmovPercMstpSrasWlnrDnon: TransAnimBase
		{
			duty = -10;
		};
		class AmovPercMstpSrasWlnrDnon_AmovPpneMstpSrasWlnrDnon: TransAnimBase
		{
			duty = -10;
		};
		class AmovPpneMstpSrasWlnrDnon_AmovPknlMstpSrasWlnrDnon: TransAnimBase
		{
			duty = -10;
		};
		class AmovPknlMstpSrasWlnrDnon_AmovPercMstpSrasWlnrDnon: TransAnimBase
		{
			duty = -10;
		};
		class AmovPercMstpSrasWlnrDnon_AmovPpneMstpSnonWnonDnon: TransAnimBase
		{
			duty = -10;
		};
		class AmovPercMstpSrasWrflDnon_AmovPknlMstpSrasWrflDnon: AmovPknlMstpSrasWrflDnon
		{
			duty = -10;
		};
		class AmovPercMstpSrasWrflDnon_AmovPpneMstpSrasWrflDnon: AmovPpneMstpSrasWrflDnon
		{
			duty = -10;
		};
		class AmovPercMsprSlowWrflDf_AmovPpneMstpSrasWrflDnon: TransAnimBase
		{
			duty = -10;
		};
		class AmovPercMrunSlowWrflDf_AmovPpneMstpSrasWrflDnon: AmovPercMstpSrasWrflDnon
		{
			duty = -10;
		};
		class AmovPercMrunSlowWrflDf_AmovPpneMstpSrasWrflDnon_end: AmovPpneMstpSrasWrflDnon
		{
			duty = -10;
		};
		class AmovPercMrunSlowWrflDf_AmovPpneMstpSrasWrflDnon_old: AmovPpneMstpSrasWrflDnon
		{
			duty = -10;
		};
		class AmovPknlMstpSrasWrflDnon_AmovPercMstpSrasWrflDnon: AmovPercMstpSrasWrflDnon
		{
			duty = -10;
		};
		class AmovPknlMstpSrasWrflDnon_AmovPpneMstpSrasWrflDnon: AmovPpneMstpSrasWrflDnon
		{
			duty = -10;
		};
		class AmovPpneMstpSrasWrflDnon_AmovPercMstpSrasWrflDnon: AmovPercMstpSrasWrflDnon
		{
			duty = -10;
		};
		class AmovPpneMstpSrasWrflDnon_AmovPercMsprSlowWrflDf: TransAnimBase
		{
			duty = -10;
		};
		class AmovPpneMstpSrasWrflDnon_AmovPknlMstpSrasWrflDnon: AmovPknlMstpSrasWrflDnon
		{
			duty = -10;
		};
		class AmovPercMstpSrasWpstDnon_AmovPknlMstpSrasWpstDnon: AmovPknlMstpSrasWpstDnon
		{
			duty = -10;
		};
		class AmovPercMsprSlowWpstDf_AmovPpneMstpSrasWpstDnon: TransAnimBase
		{
			duty = -10;
		};
		class AmovPercMstpSrasWpstDnon_AmovPpneMstpSrasWpstDnon: AmovPpneMstpSrasWpstDnon
		{
			duty = -10;
		};
		class AmovPknlMstpSrasWpstDnon_AmovPercMstpSrasWpstDnon: AmovPercMstpSrasWpstDnon
		{
			duty = -10;
		};
		class AmovPknlMstpSrasWpstDnon_AmovPpneMstpSrasWpstDnon: AmovPpneMstpSrasWpstDnon
		{
			duty = -10;
		};
		class AmovPpneMstpSrasWpstDnon_AmovPercMstpSrasWpstDnon: AmovPercMstpSrasWpstDnon
		{
			duty = -10;
		};
		class AmovPpneMstpSrasWpstDnon_AmovPknlMstpSrasWpstDnon: AmovPknlMstpSrasWpstDnon
		{
			duty = -10;
		};
		class AmovPpneMstpSrasWpstDnon_AmovPercMsprSlowWpstDf: TransAnimBase
		{
			duty = -10;
		};
		class AmovPercMstpSnonWnonDnon_AmovPknlMstpSnonWnonDnon: AmovPknlMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AmovPercMsprSnonWnonDf_AmovPpneMstpSnonWnonDnon: TransAnimBase
		{
			duty = -10;
		};
		class AmovPercMsprSnonWnonDf_AmovPpneMstpSnonWnonDnon_2: AmovPpneMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AmovPercMstpSnonWnonDnon_AmovPpneMstpSnonWnonDnon: AmovPpneMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AmovPknlMstpSnonWnonDnon_AmovPercMstpSnonWnonDnon: AmovPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AmovPknlMstpSnonWnonDnon_AmovPpneMstpSnonWnonDnon: AmovPpneMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AmovPpneMstpSnonWnonDnon_AmovPercMstpSnonWnonDnon: AmovPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AmovPpneMstpSnonWnonDnon_AmovPknlMstpSnonWnonDnon: AmovPknlMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AmovPercMstpSrasWrflDnon_AmovPercMstpSrasWpstDnon: TransAnimBase
		{
			duty = -10;
		};
		class AmovPknlMstpSrasWrflDnon_AmovPknlMstpSrasWpstDnon: AmovPercMstpSrasWrflDnon_AmovPercMstpSrasWpstDnon
		{
			duty = -10;
		};
		class AmovPknlMstpSrasWrflDnon_AmovPknlMstpSrasWpstDnon_end: AmovPercMstpSrasWrflDnon_AmovPercMstpSrasWpstDnon_end
		{
			duty = -10;
		};
		class AmovPpneMstpSrasWrflDnon_AmovPpneMstpSrasWpstDnon: TransAnimBase
		{
			duty = -10;
		};
		class AmovPknlMstpSrasWrflDnon_AmovPknlMstpSrasWlnrDnon: AmovPercMstpSrasWrflDnon_AmovPercMstpSrasWlnrDnon
		{
			duty = -10;
		};
		class AmovPknlMstpSrasWrflDnon_AmovPknlMstpSrasWlnrDnon_end: AmovPercMstpSrasWrflDnon_AmovPercMstpSrasWlnrDnon_end
		{
			duty = -10;
		};
		class AmovPercMstpSrasWpstDnon_AmovPercMstpSrasWrflDnon: TransAnimBase
		{
			duty = -10;
		};
		class AmovPknlMstpSrasWpstDnon_AmovPknlMstpSrasWrflDnon: AmovPercMstpSrasWpstDnon_AmovPercMstpSrasWrflDnon
		{
			duty = -10;
		};
		class AmovPknlMstpSrasWpstDnon_AmovPknlMstpSrasWrflDnon_end: AmovPercMstpSrasWpstDnon_AmovPercMstpSrasWrflDnon_end
		{
			duty = -10;
		};
		class AmovPpneMstpSrasWpstDnon_AmovPpneMstpSrasWrflDnon: TransAnimBase
		{
			duty = -10;
		};
		class AmovPercMstpSrasWlnrDnon_AmovPercMstpSrasWpstDnon: TransAnimBase
		{
			duty = -10;
		};
		class AmovPercMstpSrasWlnrDnon_AmovPercMstpSrasWpstDnon_end: TransAnimBase
		{
			duty = -10;
		};
		class AmovPercMstpSrasWpstDnon_AmovPercMstpSrasWlnrDnon: TransAnimBase
		{
			duty = -10;
		};
		class AmovPercMstpSrasWpstDnon_AmovPercMstpSrasWlnrDnon_end: TransAnimBase
		{
			duty = -10;
		};
		class AmovPknlMstpSrasWpstDnon_AmovPknlMstpSrasWlnrDnon: AmovPercMstpSrasWpstDnon_AmovPercMstpSrasWlnrDnon
		{
			duty = -10;
		};
		class AmovPknlMstpSrasWpstDnon_AmovPknlMstpSrasWlnrDnon_end: AmovPercMstpSrasWpstDnon_AmovPercMstpSrasWlnrDnon_end
		{
			duty = -10;
		};
		class AmovPknlMstpSrasWlnrDnon_AmovPknlMstpSrasWrflDnon: AmovPercMstpSrasWlnrDnon_AmovPercMstpSrasWrflDnon
		{
			duty = -10;
		};
		class AmovPknlMstpSrasWlnrDnon_AmovPknlMstpSrasWrflDnon_end: AmovPercMstpSrasWlnrDnon_AmovPercMstpSrasWrflDnon_end
		{
			duty = -10;
		};
		class AmovPknlMstpSrasWlnrDnon_AmovPknlMstpSrasWpstDnon: AmovPercMstpSrasWlnrDnon_AmovPercMstpSrasWpstDnon
		{
			duty = -10;
		};
		class AmovPknlMstpSrasWlnrDnon_AmovPknlMstpSrasWpstDnon_end: AmovPercMstpSrasWlnrDnon_AmovPercMstpSrasWpstDnon_end
		{
			duty = -10;
		};
		class AmovPercMevaSrasWrflDf: SprintBaseDf
		{
			duty = -10;
		};
		class AmovPknlMevaSrasWrflDf: AmovPercMevaSrasWrflDf
		{
			duty = -10;
		};
		class AmovPpneMstpSrasWrflDnon_AmovPpneMevaSlowWrflDl: AmovPpneMstpSrasWrflDnon
		{
			duty = -10;
		};
		class AmovPercMevaSrasWpstDf: SprintCivilBaseDf
		{
			duty = -10;
		};
		class AmovPknlMevaSrasWpstDf: AmovPercMevaSrasWpstDf
		{
			duty = -10;
		};
		class AmovPpneMstpSrasWpstDnon_AmovPpneMevaSlowWpstDl: AmovPpneMstpSrasWpstDnon
		{
			duty = -10;
		};
		class AmovPercMevaSnonWnonDf: SprintCivilBaseDf
		{
			duty = -10;
		};
		class AmovPknlMevaSnonWnonDf: SprintCivilBaseDf
		{
			duty = -10;
		};
		class AmovPpneMstpSnonWnonDnon_AmovPpneMevaSnonWnonDl: AidlPpneMstpSnonWnonDnon_G0S
		{
			duty = -10;
		};
		class AmovPpneMstpSnonWnonDnon_AmovPpneMevaSnonWnonDr: AidlPpneMstpSnonWnonDnon_G0S
		{
			duty = -10;
		};
		class AmovPercMstpSnonWnonDnon_exercisekneeBendA: CutSceneAnimationBase
		{
			duty = -10;
		};
		class AmovPercMstpSnonWnonDnon_exercisePushup: CutSceneAnimationBase
		{
			duty = -10;
		};
		class TestDance: AmovPercMstpSrasWrflDnon
		{
			duty = -10;
		};
		class TestSurrender: TestDance
		{
			duty = -10;
		};
		class AwopPercMstpSgthWrflDnon_Start1: Default
		{
			duty = -10;
		};
		class AmovPercMrunSlowWrflDf_AmovPercMstpSrasWrflDnon_gthStart: Default
		{
			duty = -10;
		};
		class AmovPercMwlkSrasWrflDf_AmovPercMstpSrasWrflDnon_gthStart: Default
		{
			duty = -10;
		};
		class AwopPknlMstpSgthWrflDnon_Start: Default
		{
			duty = -10;
		};
		class AwopPpneMstpSgthWrflDnon_Fast_Start: Default
		{
			duty = -10;
		};
		class AwopPpneMstpSgthWpstDnon_Fast_Start: Default
		{
			duty = -10;
		};
		class AwopPpneMstpSgthWnonDnon_Fast_Start: Default
		{
			duty = -10;
		};
		class AwopPpneMstpSgthWrflDnon_Start: Default
		{
			duty = -10;
		};
		class AwopPercMstpSgthWpstDnon_Part1: Default
		{
			duty = -10;
		};
		class AmovPercMrunSlowWpstDf_AmovPercMstpSrasWpstDnon_gthStart: Default
		{
			duty = -10;
		};
		class AmovPercMwlkSrasWpstDf_AwopPercMrunSgthWnonDf_1: Default
		{
			duty = -10;
		};
		class AmovPercMwlkSrasWpstDf_AmovPercMstpSrasWpstDnon_gthStart: Default
		{
			duty = -10;
		};
		class AwopPknlMstpSgthWpstDnon_Part1: Default
		{
			duty = -10;
		};
		class AwopPpneMstpSgthWpstDnon_Part1: Default
		{
			duty = -10;
		};
		class AwopPercMstpSgthWnonDnon_start: Default
		{
			duty = -10;
		};
		class AmovPercMrunSnonWnonDf_AmovPercMstpSnonWnonDnon_gthStart: Default
		{
			duty = -10;
		};
		class AwopPpneMstpSgthWnonDnon_start: Default
		{
			duty = -10;
		};
		class LadderCivilStatic: StandBase
		{
			duty = -10;
		};
		class LadderCivilUpLoop: LadderCivilStatic
		{
			duty = -10;
		};
		class LadderCivilDownLoop: LadderCivilUpLoop
		{
			duty = -10;
		};
		class AswmPercMstpSnonWnonDnon: AmovPercMstpSrasWrflDnon
		{
			duty = -10;
		};
		class AswmPercMstpSnonWnonDnon_goup: AswmPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AswmPercMstpSnonWnonDnon_godown: AswmPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AswmPercMstpSnonWnonDnon_putDown: AswmPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AswmPercMrunSnonWnonDf: AswmPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AswmPercMstpSnonWnonDnon_AswmPercMrunSnonWnonDf: AswmPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AsswPercMstpSnonWnonDnon_goup: AsswPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AsswPercMstpSnonWnonDnon_goDown: AsswPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AsswPercMstpSnonWnonDnon_putDown: AsswPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AsswPercMrunSnonWnonDf: AsswPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AsswPercMstpSnonWnonDnon_AsswPercMrunSnonWnonDf: AsswPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AbswPercMstpSnonWnonDnon_goup: AbswPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AbswPercMstpSnonWnonDnon_goDown: AbswPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AbswPercMstpSnonWnonDnon_putDown: AbswPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AbswPercMrunSnonWnonDf: AbswPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AbswPercMstpSnonWnonDnon_AbswPercMrunSnonWnonDf: AbswPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AdvePercMstpSnonWnonDnon_GetInSDV: AdvePercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AdvePercMstpSnonWnonDnon_GetOutSDV: AdvePercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AdvePercMstpSnonWnonDnon_goup: AdvePercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AdvePercMstpSnonWnonDnon_godown: AdvePercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AmovPercMwlkSrasWrflDf_AdvePercMrunSnonWnonDf: AmovPercMstpSrasWrflDnon
		{
			duty = -10;
		};
		class AdvePercMstpSnonWnonDnon_turnL: AdvePercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AdvePercMstpSnonWnonDnon_turnR: AdvePercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AdvePercMwlkSnonWnonDf: AdvePercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AdvePercMtacSnonWnonDf: AdvePercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AdvePercMrunSnonWnonDf: AdvePercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AdvePercMsprSnonWnonDf: AdvePercMrunSnonWnonDf
		{
			duty = -10;
		};
		class AdvePercMstpSnonWrflDnon_GetInSDV: AdvePercMstpSnonWrflDnon
		{
			duty = -10;
		};
		class AdvePercMstpSnonWrflDnon_GetOutSDV: AdvePercMstpSnonWrflDnon
		{
			duty = -10;
		};
		class AdvePercMstpSnonWrflDnon_goup: AdvePercMstpSnonWrflDnon
		{
			duty = -10;
		};
		class AdvePercMstpSnonWrflDnon_godown: AdvePercMstpSnonWrflDnon
		{
			duty = -10;
		};
		class AmovPercMwlkSrasWrflDf_AdvePercMrunSnonWrflDf: AmovPercMstpSrasWrflDnon
		{
			duty = -10;
		};
		class AdvePercMstpSnonWrflDnon_turnL: AdvePercMstpSnonWrflDnon
		{
			duty = -10;
		};
		class AdvePercMstpSnonWrflDnon_turnR: AdvePercMstpSnonWrflDnon
		{
			duty = -10;
		};
		class AdvePercMwlkSnonWrflDf: AdvePercMstpSnonWrflDnon
		{
			duty = -10;
		};
		class AdvePercMtacSnonWrflDf: AdvePercMstpSnonWrflDnon
		{
			duty = -10;
		};
		class AdvePercMrunSnonWrflDf: AdvePercMstpSnonWrflDnon
		{
			duty = -10;
		};
		class AdvePercMsprSnonWrflDf: AdvePercMrunSnonWrflDf
		{
			duty = -10;
		};
		class AsdvPercMstpSnonWnonDnon_GetInSDV: AsdvPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AsdvPercMstpSnonWnonDnon_GetOutSDV: AsdvPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AsdvPercMstpSnonWnonDnon_goup: AsdvPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AsdvPercMstpSnonWnonDnon_godown: AsdvPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AsdvPercMstpSnonWnonDnon_turnL: AsdvPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AsdvPercMstpSnonWnonDnon_turnR: AsdvPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AsdvPercMwlkSnonWnonDf: AsdvPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AsdvPercMtacSnonWnonDf: AsdvPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AsdvPercMrunSnonWnonDf: AsdvPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AsdvPercMsprSnonWnonDf: AsdvPercMrunSnonWnonDf
		{
			duty = -10;
		};
		class AsdvPercMstpSnonWrflDnon_GetInSDV: AsdvPercMstpSnonWrflDnon
		{
			duty = -10;
		};
		class AsdvPercMstpSnonWrflDnon_GetOutSDV: AsdvPercMstpSnonWrflDnon
		{
			duty = -10;
		};
		class AsdvPercMstpSnonWrflDnon_goup: AsdvPercMstpSnonWrflDnon
		{
			duty = -10;
		};
		class AsdvPercMstpSnonWrflDnon_godown: AsdvPercMstpSnonWrflDnon
		{
			duty = -10;
		};
		class AsdvPercMstpSnonWrflDnon_turnL: AsdvPercMstpSnonWrflDnon
		{
			duty = -10;
		};
		class AsdvPercMstpSnonWrflDnon_turnR: AsdvPercMstpSnonWrflDnon
		{
			duty = -10;
		};
		class AsdvPercMwlkSnonWrflDf: AsdvPercMstpSnonWrflDnon
		{
			duty = -10;
		};
		class AsdvPercMtacSnonWrflDf: AsdvPercMstpSnonWrflDnon
		{
			duty = -10;
		};
		class AsdvPercMrunSnonWrflDf: AsdvPercMstpSnonWrflDnon
		{
			duty = -10;
		};
		class AsdvPercMsprSnonWrflDf: AsdvPercMrunSnonWrflDf
		{
			duty = -10;
		};
		class AbdvPercMstpSnonWnonDnon_GetInSDV: AbdvPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AbdvPercMstpSnonWnonDnon_GetOutSDV: AbdvPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AbdvPercMstpSnonWnonDnon_goup: AbdvPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AbdvPercMstpSnonWnonDnon_godown: AbdvPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AbdvPercMstpSnonWnonDnon_turnL: AbdvPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AbdvPercMstpSnonWnonDnon_turnR: AbdvPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AbdvPercMwlkSnonWnonDf: AbdvPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AbdvPercMtacSnonWnonDf: AbdvPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AbdvPercMrunSnonWnonDf: AbdvPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AbdvPercMsprSnonWnonDf: AbdvPercMrunSnonWnonDf
		{
			duty = -10;
		};
		class AbdvPercMstpSnonWrflDnon_GetInSDV: AbdvPercMstpSnonWrflDnon
		{
			duty = -10;
		};
		class AbdvPercMstpSnonWrflDnon_GetOutSDV: AbdvPercMstpSnonWrflDnon
		{
			duty = -10;
		};
		class AbdvPercMstpSnonWrflDnon_goup: AbdvPercMstpSnonWrflDnon
		{
			duty = -10;
		};
		class AbdvPercMstpSnonWrflDnon_godown: AbdvPercMstpSnonWrflDnon
		{
			duty = -10;
		};
		class AbdvPercMstpSnonWrflDnon_turnL: AbdvPercMstpSnonWrflDnon
		{
			duty = -10;
		};
		class AbdvPercMstpSnonWrflDnon_turnR: AbdvPercMstpSnonWrflDnon
		{
			duty = -10;
		};
		class AbdvPercMwlkSnonWrflDf: AbdvPercMstpSnonWrflDnon
		{
			duty = -10;
		};
		class AbdvPercMtacSnonWrflDf: AbdvPercMstpSnonWrflDnon
		{
			duty = -10;
		};
		class AbdvPercMrunSnonWrflDf: AbdvPercMstpSnonWrflDnon
		{
			duty = -10;
		};
		class AbdvPercMsprSnonWrflDf: AbdvPercMrunSnonWrflDf
		{
			duty = -10;
		};
		class AmovPercMwlkSoptWbinDf: AmovPercMstpSoptWbinDnon
		{
			duty = -10;
		};
		class AmovPercMrunSnonWbinDf: AmovPercMwlkSoptWbinDf
		{
			duty = -10;
		};
		class AmovPercMevaSnonWbinDf: AmovPercMrunSnonWbinDf
		{
			duty = -10;
		};
		class AovrPercMstpSoptWbinDf: AmovPercMstpSoptWbinDnon
		{
			duty = -10;
		};
		class AmovPknlMwlkSoptWbinDf: AmovPknlMstpSoptWbinDnon
		{
			duty = -10;
		};
		class AmovPknlMrunSnonWbinDf: AmovPknlMwlkSoptWbinDf
		{
			duty = -10;
		};
		class AmovPknlMevaSnonWbinDf: AmovPknlMrunSnonWbinDf
		{
			duty = -10;
		};
		class AmovPpneMwlkSoptWbinDf: AmovPpneMstpSoptWbinDnon
		{
			duty = -10;
		};
		class AmovPpneMrunSnonWbinDf: AmovPpneMwlkSoptWbinDf
		{
			duty = -10;
		};
		class AmovPpneMevaSnonWbinDf: AmovPpneMrunSnonWbinDf
		{
			duty = -10;
		};
		class AmovPercMwlkSoptWbinDf_rfl: AwopPercMstpSoptWbinDnon_rfl
		{
			duty = -10;
		};
		class AmovPercMrunSnonWbinDf_rfl: AmovPercMwlkSoptWbinDf_rfl
		{
			duty = -10;
		};
		class AmovPercMevaSnonWbinDf_rfl: AmovPercMrunSnonWbinDf_rfl
		{
			duty = -10;
		};
		class AmovPknlMwlkSoptWbinDf_rfl: AwopPknlMstpSoptWbinDnon_rfl
		{
			duty = -10;
		};
		class AmovPknlMrunSnonWbinDf_rfl: AmovPknlMwlkSoptWbinDf_rfl
		{
			duty = -10;
		};
		class AmovPknlMevaSnonWbinDf_rfl: AmovPknlMrunSnonWbinDf_rfl
		{
			duty = -10;
		};
		class AmovPpneMwlkSoptWbinDf_rfl: AwopPpneMstpSoptWbinDnon_rfl
		{
			duty = -10;
		};
		class AmovPpneMrunSnonWbinDf_rfl: AmovPpneMwlkSoptWbinDf_rfl
		{
			duty = -10;
		};
		class AmovPpneMevaSnonWbinDf_rfl: AmovPpneMrunSnonWbinDf_rfl
		{
			duty = -10;
		};
		class AmovPercMwlkSoptWbinDf_pst: AwopPercMstpSoptWbinDnon_pst
		{
			duty = -10;
		};
		class AmovPercMrunSnonWbinDf_pst: AmovPercMwlkSoptWbinDf_pst
		{
			duty = -10;
		};
		class AmovPercMevaSnonWbinDf_pst: AmovPercMrunSnonWbinDf_pst
		{
			duty = -10;
		};
		class AmovPknlMwlkSoptWbinDf_pst: AwopPknlMstpSoptWbinDnon_pst
		{
			duty = -10;
		};
		class AmovPknlMrunSnonWbinDf_pst: AmovPknlMwlkSoptWbinDf_pst
		{
			duty = -10;
		};
		class AmovPknlMevaSnonWbinDf_pst: AmovPknlMrunSnonWbinDf_pst
		{
			duty = -10;
		};
		class AmovPpneMwlkSoptWbinDf_pst: AwopPpneMstpSoptWbinDnon_pst
		{
			duty = -10;
		};
		class AmovPpneMrunSnonWbinDf_pst: AmovPpneMwlkSoptWbinDf_pst
		{
			duty = -10;
		};
		class AmovPpneMevaSnonWbinDf_pst: AmovPpneMrunSnonWbinDf_pst
		{
			duty = -10;
		};
		class AmovPercMwlkSoptWbinDf_lnr: AwopPercMstpSoptWbinDnon_lnr
		{
			duty = -10;
		};
		class AmovPercMrunSnonWbinDf_lnr: AmovPercMwlkSoptWbinDf_lnr
		{
			duty = -10;
		};
		class AmovPercMevaSnonWbinDf_lnr: AmovPercMrunSnonWbinDf_lnr
		{
			duty = -10;
		};
		class AmovPknlMwlkSoptWbinDf_lnr: AwopPknlMstpSoptWbinDnon_lnr
		{
			duty = -10;
		};
		class AmovPknlMrunSnonWbinDf_lnr: AmovPknlMwlkSoptWbinDf_lnr
		{
			duty = -10;
		};
		class AmovPknlMevaSnonWbinDf_lnr: AmovPknlMrunSnonWbinDf_lnr
		{
			duty = -10;
		};
		class AmovPpneMwlkSoptWbinDf_lnr: AwopPpneMstpSoptWbinDnon_lnr
		{
			duty = -10;
		};
		class AmovPpneMrunSnonWbinDf_lnr: AmovPpneMwlkSoptWbinDf_lnr
		{
			duty = -10;
		};
		class AmovPpneMevaSnonWbinDf_lnr: AmovPpneMrunSnonWbinDf_lnr
		{
			duty = -10;
		};
		class AmovPercMwlkSoptWbinDf_non: AwopPercMstpSoptWbinDnon_non
		{
			duty = -10;
		};
		class AmovPercMrunSnonWbinDf_non: AmovPercMwlkSoptWbinDf_non
		{
			duty = -10;
		};
		class AmovPercMevaSnonWbinDf_non: AmovPercMrunSnonWbinDf_non
		{
			duty = -10;
		};
		class AmovPknlMwlkSoptWbinDf_non: AwopPknlMstpSoptWbinDnon_non
		{
			duty = -10;
		};
		class AmovPknlMrunSnonWbinDf_non: AmovPknlMwlkSoptWbinDf_non
		{
			duty = -10;
		};
		class AmovPknlMevaSnonWbinDf_non: AmovPknlMrunSnonWbinDf_non
		{
			duty = -10;
		};
		class AmovPpneMwlkSoptWbinDf_non: AwopPpneMstpSoptWbinDnon_non
		{
			duty = -10;
		};
		class AmovPpneMrunSnonWbinDf_non: AmovPpneMwlkSoptWbinDf_non
		{
			duty = -10;
		};
		class AmovPpneMevaSnonWbinDf_non: AmovPpneMrunSnonWbinDf_non
		{
			duty = -10;
		};
		class AmovPercMstpSoptWbinDnon_AmovPknlMstpSoptWbinDnon: AmovPknlMstpSoptWbinDnon
		{
			duty = -10;
		};
		class AmovPercMstpSoptWbinDnon_AmovPpneMstpSoptWbinDnon: AmovPpneMstpSoptWbinDnon
		{
			duty = -10;
		};
		class AmovPknlMstpSoptWbinDnon_AmovPercMstpSoptWbinDnon: AmovPercMstpSoptWbinDnon
		{
			duty = -10;
		};
		class AmovPknlMstpSoptWbinDnon_AmovPpneMstpSoptWbinDnon: AmovPpneMstpSoptWbinDnon
		{
			duty = -10;
		};
		class AmovPpneMstpSoptWbinDnon_AmovPercMstpSoptWbinDnon: AmovPercMstpSoptWbinDnon
		{
			duty = -10;
		};
		class AmovPpneMstpSoptWbinDnon_AmovPknlMstpSoptWbinDnon: AmovPknlMstpSoptWbinDnon
		{
			duty = -10;
		};
		class AinjPpneMstpSnonWrflDnon_injuredHealed: AinjPpneMstpSnonWrflDnon
		{
			duty = -10;
		};
		class AinjPpneMstpSnonWrflDnon_rolltoback: AinjPpneMstpSnonWrflDnon
		{
			duty = -10;
		};
		class AmovPercMstpSlowWrflDnon_AcinPknlMwlkSlowWrflDb_1: Default
		{
			duty = -10;
		};
		class AcinPknlMstpSrasWrflDnon: AmovPercMstpSlowWrflDnon_AcinPknlMwlkSlowWrflDb_1
		{
			duty = -10;
		};
		class DraggerBase: Default
		{
			duty = -10;
		};
		class AcinPknlMwlkSlowWrflDb_AmovPercMstpSlowWrflDnon: AmovPercMstpSlowWrflDnon_AcinPknlMwlkSlowWrflDb_1
		{
			duty = -10;
		};
		class AcinPknlMwlkSrasWrflDb: AmovPercMstpSlowWrflDnon_AcinPknlMwlkSlowWrflDb_1
		{
			duty = -10;
		};
		class AcinPercMrunSrasWrflDf: InjuredMovedBase
		{
			duty = -10;
		};
		class AcinPercMrunSrasWrflDf_agony: AcinPercMrunSrasWrflDf_death
		{
			duty = -10;
		};
		class AcinPknlMstpSrasWrflDnon_AcinPercMrunSrasWrflDnon: InjuredMovedBase
		{
			duty = -10;
		};
		class AcinPercMstpSrasWrflDnon: AcinPknlMstpSrasWrflDnon_AcinPercMrunSrasWrflDnon
		{
			duty = -10;
		};
		class AinjPfalMstpSnonWrflDnon_carried_still: AinjPfalMstpSnonWrflDnon_carried_Up
		{
			duty = -10;
		};
		class AmovPpneMsprSnonWnonDf_injured: AmovPpneMstpSnonWnonDnon_injured
		{
			duty = -10;
		};
		class AinjPpneMstpSnonWnonDnon_injuredHealed: AinjPpneMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AinjPpneMstpSnonWnonDnon_rolltoback: AinjPpneMstpSnonWnonDnon
		{
			duty = -10;
		};
		class AmovPercMstpSnonWnonDnon_AcinPknlMwlkSnonWnonDb_1: Default
		{
			duty = -10;
		};
		class AcinPknlMstpSnonWnonDnon: AmovPercMstpSnonWnonDnon_AcinPknlMwlkSnonWnonDb_1
		{
			duty = -10;
		};
		class AcinPknlMwlkSnonWnonDb_AmovPercMstpSnonWnonDnon: AmovPercMstpSnonWnonDnon_AcinPknlMwlkSnonWnonDb_1
		{
			duty = -10;
		};
		class AcinPknlMwlkSnonWnonDb: AmovPercMstpSnonWnonDnon_AcinPknlMwlkSnonWnonDb_1
		{
			duty = -10;
		};
		class AcinPercMrunSnonWnonDf: InjuredMovedBase
		{
			duty = -10;
		};
		class AcinPercMrunSnonWnonDf_agony: AcinPercMrunSnonWnonDf_death
		{
			duty = -10;
		};
		class AcinPknlMstpSnonWnonDnon_AcinPercMrunSnonWnonDnon: InjuredMovedBase
		{
			duty = -10;
		};
		class AcinPercMstpSnonWnonDnon: AcinPknlMstpSnonWnonDnon_AcinPercMrunSnonWnonDnon
		{
			duty = -10;
		};
		class AinjPfalMstpSnonWnonDnon_carried_still: AinjPfalMstpSnonWnonDnon_carried_Up
		{
			duty = -10;
		};
		class AmovPercMstpSrasWlnrDnon: AmovPknlMstpSrasWlnrDnon
		{
			duty = -10;
		};
		class ReloadRPG: AidlPercMstpSrasWlnrDnon_G0S
		{
			duty = -10;
		};
		class ReloadRPG7V: AidlPercMstpSrasWlnrDnon_G0S
		{
			duty = -10;
		};
		class AovrPercMstpSrasWlnrDf: AmovPercMstpSrasWlnrDnon
		{
			duty = -10;
		};
		class AovrPercMstpSlowWlnrDf: AmovPercMstpSlowWlnrDnon
		{
			duty = -10;
		};
		class AmovPercMwlkSlowWlnrDf: AmovPercMstpSlowWlnrDnon
		{
			duty = -10;
		};
		class AmovPercMwlkSrasWlnrDf: AidlPercMstpSrasWlnrDnon_G0S
		{
			duty = -10;
		};
		class AmovPercMtacSrasWlnrDf: AidlPercMstpSrasWlnrDnon_G0S
		{
			duty = -10;
		};
		class AmovPercMrunSrasWlnrDf: AmovPercMrunSlowWlnrDf
		{
			duty = -10;
		};
		class AmovPercMrunSrasWlnrDfl: AmovPercMrunSrasWlnrDf
		{
			duty = -10;
		};
		class AmovPercMrunSrasWlnrDfr: AmovPercMrunSrasWlnrDf
		{
			duty = -10;
		};
		class AmovPercMstpSrasWlnrDnon_turnL: AmovPercMstpSrasWlnrDnon
		{
			duty = -10;
		};
		class AmovPercMstpSrasWlnrDnon_turnR: AmovPercMstpSrasWlnrDnon
		{
			duty = -10;
		};
		class AadjPercMstpSrasWrflDleft_AadjPknlMstpSrasWrflDleft: AadjPknlMstpSrasWrflDleft
		{
			duty = -10;
		};
		class AadjPknlMstpSrasWrflDleft_AadjPercMstpSrasWrflDleft: AadjPercMstpSrasWrflDleft
		{
			duty = -10;
		};
		class AadjPercMstpSrasWrflDright_AadjPknlMstpSrasWrflDright: AadjPknlMstpSrasWrflDright
		{
			duty = -10;
		};
		class AadjPknlMstpSrasWrflDright_AadjPercMstpSrasWrflDright: AadjPercMstpSrasWrflDright
		{
			duty = -10;
		};
		class AadjPercMstpSrasWrflDdown_AadjPknlMstpSrasWrflDdown: AadjPknlMstpSrasWrflDdown
		{
			duty = -10;
		};
		class AadjPercMstpSrasWrflDdown_AadjPknlMstpSrasWrflDup: AadjPknlMstpSrasWrflDup
		{
			duty = -10;
		};
		class AadjPknlMstpSrasWrflDdown_AadjPercMstpSrasWrflDdown: AadjPercMstpSrasWrflDdown
		{
			duty = -10;
		};
		class AadjPercMstpSrasWrflDup_AadjPknlMstpSrasWrflDup: AadjPknlMstpSrasWrflDup
		{
			duty = -10;
		};
		class AadjPknlMstpSrasWrflDup_AadjPercMstpSrasWrflDup: AadjPercMstpSrasWrflDup
		{
			duty = -10;
		};
		class AadjPknlMstpSrasWrflDup_AadjPercMstpSrasWrflDdown: AadjPercMstpSrasWrflDdown
		{
			duty = -10;
		};
		class AadjPercMstpSrasWpstDleft_AadjPknlMstpSrasWpstDleft: AadjPknlMstpSrasWpstDleft
		{
			duty = -10;
		};
		class AadjPknlMstpSrasWpstDleft_AadjPercMstpSrasWpstDleft: AadjPercMstpSrasWpstDleft
		{
			duty = -10;
		};
		class AadjPercMstpSrasWpstDright_AadjPknlMstpSrasWpstDright: AadjPknlMstpSrasWpstDright
		{
			duty = -10;
		};
		class AadjPknlMstpSrasWpstDright_AadjPercMstpSrasWpstDright: AadjPercMstpSrasWpstDright
		{
			duty = -10;
		};
		class AadjPercMstpSrasWpstDdown_AadjPknlMstpSrasWpstDdown: AadjPknlMstpSrasWpstDdown
		{
			duty = -10;
		};
		class AadjPercMstpSrasWpstDdown_AadjPknlMstpSrasWpstDup: AadjPknlMstpSrasWpstDup
		{
			duty = -10;
		};
		class AadjPknlMstpSrasWpstDdown_AadjPercMstpSrasWpstDdown: AadjPercMstpSrasWpstDdown
		{
			duty = -10;
		};
		class AadjPercMstpSrasWpstDup_AadjPknlMstpSrasWpstDup: AadjPknlMstpSrasWpstDup
		{
			duty = -10;
		};
		class AadjPknlMstpSrasWpstDup_AadjPercMstpSrasWpstDup: AadjPercMstpSrasWpstDup
		{
			duty = -10;
		};
		class AadjPknlMstpSrasWpstDup_AadjPercMstpSrasWpstDdown: AadjPercMstpSrasWpstDdown
		{
			duty = -10;
		};
		class AadjPknlMstpSrasWpstDdown_AadjPpneMstpSrasWpstDup: AadjPpneMstpSrasWpstDup
		{
			duty = -10;
		};
		class AadjPpneMstpSrasWpstDup_AadjPknlMstpSrasWpstDdown: AadjPknlMstpSrasWpstDdown
		{
			duty = -10;
		};
		class LaceyTest2a: CutSceneAnimationBase
		{
			duty = -10;
		};
		class ApanPercMrunSnonWnonDf: ApanPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class ApanPknlMrunSnonWnonDf: ApanPknlMstpSnonWnonDnon
		{
			duty = -10;
		};
		class ApanPpneMrunSnonWnonDf: ApanPpneMstpSnonWnonDnon
		{
			duty = -10;
		};
		class ApanPercMwlkSnonWnonDf: ApanPercMrunSnonWnonDf
		{
			duty = -10;
		};
		class ApanPknlMwlkSnonWnonDf: ApanPknlMrunSnonWnonDf
		{
			duty = -10;
		};
		class ApanPercMsprSnonWnonDf: ApanPercMrunSnonWnonDf
		{
			duty = -10;
		};
		class ApanPknlMsprSnonWnonDf: ApanPknlMrunSnonWnonDf
		{
			duty = -10;
		};
		class ApanPpneMsprSnonWnonDf: ApanPpneMrunSnonWnonDf
		{
			duty = -10;
		};
		class ApanPercMstpSnonWnonDnon_ApanPknlMstpSnonWnonDnon: ApanPknlMstpSnonWnonDnon
		{
			duty = -10;
		};
		class ApanPercMstpSnonWnonDnon_ApanPpneMstpSnonWnonDnon: ApanPpneMstpSnonWnonDnon
		{
			duty = -10;
		};
		class ApanPknlMstpSnonWnonDnon_ApanPercMstpSnonWnonDnon: ApanPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class ApanPknlMstpSnonWnonDnon_ApanPpneMstpSnonWnonDnon: ApanPpneMstpSnonWnonDnon
		{
			duty = -10;
		};
		class ApanPpneMstpSnonWnonDnon_ApanPercMstpSnonWnonDnon: ApanPercMstpSnonWnonDnon
		{
			duty = -10;
		};
		class ApanPpneMstpSnonWnonDnon_ApanPknlMstpSnonWnonDnon: ApanPknlMstpSnonWnonDnon
		{
			duty = -10;
		};
	};
};
*/
