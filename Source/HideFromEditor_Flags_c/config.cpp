class CfgPatches
{
	class Optional_HideFromEditor_Flags_c
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Data_F_Loadorder","A3_Data_F_Curator_Loadorder","A3_Data_F_Kart_Loadorder","A3_Data_F_Bootcamp_Loadorder","A3_Data_F_Heli_Loadorder","A3_Data_F_Mark_Loadorder","A3_Data_F_Exp_A_Loadorder","A3_Data_F_Exp_B_Loadorder","A3_Data_F_Exp_Loadorder","A3_Data_F_Jets_Loadorder","A3_Data_F_Argo_Loadorder","A3_Data_F_Patrol_Loadorder","A3_Data_F_Orange_Loadorder","A3_Data_F_Tacops_Loadorder","A3_Data_F_Tank_Loadorder"};
		version = "2019-03-28";
	};
};
class CfgVehicles
{
	class FlagCarrier;
	class FlagCarrier_Asym;
	class FlagCarrierCore;
	class NonStrategic;

	class FlagPole_F: FlagCarrier
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class FlagChecked_F: FlagCarrierCore
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class FlagSmall_F: NonStrategic
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class FlagMarker_01_F: NonStrategic
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_CTRG_F: FlagCarrier_Asym
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_Gendarmerie_F: FlagCarrier
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_HorizonIslands_F: FlagCarrier
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_Syndikat_F: FlagCarrier
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_Viper_F: FlagCarrier
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_Blueking_F: FlagCarrier_Asym
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_Blueking_inverted_F: FlagCarrier_Asym
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_Fuel_F: FlagCarrier_Asym
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_Fuel_inverted_F: FlagCarrier_Asym
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_Burstkoke_F: FlagCarrier_Asym
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_Burstkoke_inverted_F: FlagCarrier_Asym
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_Redburger_F: FlagCarrier_Asym
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_Redstone_F: FlagCarrier_Asym
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_Suatmm_F: FlagCarrier_Asym
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_FD_Purple_F: FlagCarrier
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_Vrana_F: FlagCarrier_Asym
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_ARMEX_F: FlagCarrier_Asym
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_Quontrol_F: FlagCarrier_Asym
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_ION_F: FlagCarrier_Asym
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_Larkin_F: FlagCarrier_Asym
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_FD_Green_F: FlagCarrier
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_FD_Red_F: FlagCarrier
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_FD_Blue_F: FlagCarrier
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_FD_Orange_F: FlagCarrier
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_NATO_F: FlagCarrier
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_UK_F: FlagCarrier
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_US_F: FlagCarrier
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_AAF_F: FlagCarrier
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_Altis_F: FlagCarrier
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_FIA_F: FlagCarrier
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_CSAT_F: FlagCarrier
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_AltisColonial_F: FlagCarrier
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_Blue_F: FlagCarrier
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_Red_F: FlagCarrier
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_Green_F: FlagCarrier
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_White_F: FlagCarrier
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_RedCrystal_F: FlagCarrier
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_POWMIA_F: FlagCarrier_Asym
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_UNO_F: FlagCarrier_Asym
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_BI_F: FlagCarrier_Asym
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
	class Flag_IDAP_F: FlagCarrier_Asym
	{
		scope = 1;//2
		scopeCurator = 1;//2
	};
};
