﻿class CfgPatches
{
	class WW2_Optional_RevertChangesToGUI_c
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"WW2_Core_c_IF_Gui_c"};
		version = "2017-12-07";
	};
};
class RscControlsGroupNoScrollbars;
class RscMapControl
{
	alphaFadeEndScale = 2;
	alphaFadeStartScale = 2;
	colorCountlines[] = {0.572,0.354,0.188,0.25};
	colorCountlinesWater[] = {0.491,0.577,0.702,0.3};
	colorForest[] = {0.624,0.78,0.388,0.5};
	colorForestBorder[] = {0,0,0,0};
	colorLevels[] = {0.286,0.177,0.094,0.5};
	colorMainCountlines[] = {0.572,0.354,0.188,0.5};
	colorMainCountlinesWater[] = {0.491,0.577,0.702,0.6};
	colorRocks[] = {0,0,0,0.3};
	colorRocksBorder[] = {0,0,0,0};
	colorSea[] = {0.467,0.631,0.851,0.5};
	maxSatelliteAlpha = 0.85;
	ptsPerSquareFor = 9;
	ptsPerSquareForEdge = 9;
	ptsPerSquareObj = 9;
	ptsPerSquareRoad = 6;
	ptsPerSquareSea = 5;
	ptsPerSquareTxt = 20;
	sizeExGrid = 0.02;
	sizeExInfo = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 0.8)";
	sizeExLabel = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 0.8)";
	sizeExLevel = 0.02;
	sizeExNames = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 0.8) * 2";
	sizeExUnits = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 0.8)";
	text = "#(argb,8,8,3)color(1,1,1,1)";
	class Bunker
	{
		icon = "\A3\ui_f\data\map\mapcontrol\bunker_ca.paa";
	};
	class Bush
	{
		icon = "\A3\ui_f\data\map\mapcontrol\bush_ca.paa";
		importance = "0.2 * 14 * 0.05 * 0.05";
		size = "14/2";
	};
	class BusStop
	{
		coefMax = 1;
		coefMin = 0.85;
		color[] = {1,1,1,1};
		icon = "\A3\ui_f\data\map\mapcontrol\busstop_CA.paa";
		importance = 1;
		size = 24;
	};
	class Chapel
	{
		coefMax = 1;
		coefMin = 0.85;
		icon = "\A3\ui_f\data\map\mapcontrol\Chapel_CA.paa";
		importance = 1;
		size = 24;
	};
	class Church
	{
		coefMax = 1;
		coefMin = 0.85;
		color[] = {1,1,1,1};
		icon = "\A3\ui_f\data\map\mapcontrol\church_CA.paa";
		importance = 1;
		size = 24;
	};
	class Command
	{
		color[] = {1,1,1,1};
		icon = "\a3\ui_f\data\map\mapcontrol\waypoint_ca.paa";
	};
	class Cross
	{
		coefMax = 1;
		coefMin = 0.85;
		color[] = {0,0,0,1};
		icon = "\A3\ui_f\data\map\mapcontrol\Cross_CA.paa";
		importance = 1;
		size = 24;
	};
	class CustomMark
	{
		color[] = {1,1,1,1};
		icon = "\a3\ui_f\data\map\mapcontrol\custommark_ca.paa";
	};
	class Fortress
	{
		color[] = {0,0,0,1};
		icon = "\A3\ui_f\data\map\mapcontrol\bunker_ca.paa";
	};
	class Fountain
	{
		color[] = {0,0,0,1};
		icon = "\A3\ui_f\data\map\mapcontrol\fountain_ca.paa";
	};
	class Fuelstation
	{
		coefMax = 1;
		coefMin = 0.85;
		color[] = {1,1,1,1};
		icon = "\A3\ui_f\data\map\mapcontrol\fuelstation_CA.paa";
		importance = 1;
		size = 24;
	};
	class Hospital
	{
		color[] = {1,1,1,1};
		icon = "\A3\ui_f\data\map\mapcontrol\hospital_CA.paa";
		importance = 1;
	};
	class Legend
	{
		colorBackground[] = {1,1,1,0.5};
	};
	class Lighthouse
	{
		coefMax = 1;
		coefMin = 0.85;
		color[] = {1,1,1,1};
		icon = "\A3\ui_f\data\map\mapcontrol\lighthouse_CA.paa";
		importance = 1;
		size = 24;
	};
	class Quay
	{
		coefMax = 1;
		coefMin = 0.85;
		color[] = {1,1,1,1};
		icon = "\A3\ui_f\data\map\mapcontrol\quay_CA.paa";
		importance = 1;
		size = 24;
	};
	class Rock
	{
		icon = "\A3\ui_f\data\map\mapcontrol\rock_ca.paa";
	};
	class Ruin
	{
		color[] = {0,0,0,1};
		icon = "\A3\ui_f\data\map\mapcontrol\ruin_ca.paa";
	};
	class SmallTree
	{
		icon = "\A3\ui_f\data\map\mapcontrol\bush_ca.paa";
	};
	class Stack
	{
		color[] = {0,0,0,1};
		icon = "\A3\ui_f\data\map\mapcontrol\stack_ca.paa";
	};
	class Task
	{
		color[] = {"(profilenamespace getvariable ['IGUI_TEXT_RGB_R',0])","(profilenamespace getvariable ['IGUI_TEXT_RGB_G',1])","(profilenamespace getvariable ['IGUI_TEXT_RGB_B',1])","(profilenamespace getvariable ['IGUI_TEXT_RGB_A',0.8])"};
		colorCanceled[] = {0.7,0.7,0.7,1};
		colorCreated[] = {1,1,1,1};
		colorDone[] = {0.7,1,0.3,1};
		colorFailed[] = {1,0.3,0.2,1};
	};
	class Tourism
	{
		icon = "\A3\ui_f\data\map\mapcontrol\tourism_ca.paa";
	};
	class Transmitter
	{
		coefMax = 1;
		coefMin = 0.85;
		color[] = {1,1,1,1};
		icon = "\A3\ui_f\data\map\mapcontrol\transmitter_CA.paa";
		importance = 1;
		size = 24;
	};
	class Tree
	{
		icon = "\A3\ui_f\data\map\mapcontrol\bush_ca.paa";
	};
	class ViewTower
	{
		color[] = {0,0,0,1};
		icon = "\A3\ui_f\data\map\mapcontrol\viewtower_ca.paa";
	};
	class Watertower
	{
		coefMax = 1;
		coefMin = 0.85;
		color[] = {1,1,1,1};
		icon = "\A3\ui_f\data\map\mapcontrol\watertower_CA.paa";
		importance = 1;
		size = 24;
	};
	class Waypoint
	{
		icon = "\a3\ui_f\data\map\mapcontrol\waypoint_ca.paa";
	};
	class WaypointCompleted
	{
		icon = "\a3\ui_f\data\map\mapcontrol\waypointcompleted_ca.paa";
	};
};
class RscMiniMap
{
	class controlsBackground
	{
		class GroupIndicator: RscMapControl
		{
			class Bunker
			{
				coefMax = 4;
				coefMin = 0.25;
				color[] = {0,0,0,1};
				icon = "\A3\ui_f\data\map\mapcontrol\bunker_ca.paa";
				importance = "1.5 * 14 * 0.05";
				size = 14;
			};
			class Bush
			{
				coefMax = 4;
				coefMin = 0.25;
				color[] = {0.45,0.64,0.33,0.4};
				icon = "\A3\ui_f\data\map\mapcontrol\bush_ca.paa";
				importance = "0.2 * 14 * 0.05 * 0.05";
				size = "14/2";
			};
			class BusStop
			{
				coefMax = 1;
				coefMin = 0.85;
				color[] = {1,1,1,1};
				icon = "\A3\ui_f\data\map\mapcontrol\busstop_CA.paa";
				importance = 1;
				size = 24;
			};
			class Chapel
			{
				coefMax = 1;
				coefMin = 0.85;
				color[] = {0,0,0,1};
				icon = "\A3\ui_f\data\map\mapcontrol\Chapel_CA.paa";
				importance = 1;
				size = 24;
			};
			class Church
			{
				coefMax = 1;
				coefMin = 0.85;
				color[] = {1,1,1,1};
				icon = "\A3\ui_f\data\map\mapcontrol\church_CA.paa";
				importance = 1;
				size = 24;
			};
			class Command
			{
				coefMax = 1;
				coefMin = 1;
				color[] = {1,1,1,1};
				icon = "\a3\ui_f\data\map\mapcontrol\waypoint_ca.paa";
				importance = 1;
				size = 18;
			};
			class Cross
			{
				coefMax = 1;
				coefMin = 0.85;
				color[] = {0,0,0,1};
				icon = "\A3\ui_f\data\map\mapcontrol\Cross_CA.paa";
				importance = 1;
				size = 24;
			};
			class CustomMark
			{
				coefMax = 1;
				coefMin = 1;
				color[] = {1,1,1,1};
				icon = "\a3\ui_f\data\map\mapcontrol\custommark_ca.paa";
				importance = 1;
				size = 18;
			};
			class Fortress
			{
				coefMax = 4;
				coefMin = 0.25;
				color[] = {0,0,0,1};
				icon = "\A3\ui_f\data\map\mapcontrol\bunker_ca.paa";
				importance = "2 * 16 * 0.05";
				size = 16;
			};
			class Fountain
			{
				coefMax = 4;
				coefMin = 0.25;
				color[] = {0,0,0,1};
				icon = "\A3\ui_f\data\map\mapcontrol\fountain_ca.paa";
				importance = "1 * 12 * 0.05";
				size = 11;
			};
			class Fuelstation
			{
				coefMax = 1;
				coefMin = 0.85;
				color[] = {1,1,1,1};
				icon = "\A3\ui_f\data\map\mapcontrol\fuelstation_CA.paa";
				importance = 1;
				size = 24;
			};
			class Hospital
			{
				coefMax = 1;
				coefMin = 0.85;
				color[] = {1,1,1,1};
				icon = "\A3\ui_f\data\map\mapcontrol\hospital_CA.paa";
				importance = 1;
				size = 24;
			};
			class Legend
			{
				color[] = {0,0,0,1};
				colorBackground[] = {1,1,1,0.5};
				font = "RobotoCondensed";
				h = "3.5 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
				sizeEx = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 0.8)";
				w = "10 * (((safezoneW / safezoneH) min 1.2) / 40)";
				x = "SafeZoneX + (((safezoneW / safezoneH) min 1.2) / 40)";
				y = "SafeZoneY + safezoneH - 4.5 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			};
			class Lighthouse
			{
				coefMax = 1;
				coefMin = 0.85;
				color[] = {1,1,1,1};
				icon = "\A3\ui_f\data\map\mapcontrol\lighthouse_CA.paa";
				importance = 1;
				size = 24;
			};
			class Quay
			{
				coefMax = 1;
				coefMin = 0.85;
				color[] = {1,1,1,1};
				icon = "\A3\ui_f\data\map\mapcontrol\quay_CA.paa";
				importance = 1;
				size = 24;
			};
			class Rock
			{
				coefMax = 4;
				coefMin = 0.25;
				color[] = {0.1,0.1,0.1,0.8};
				icon = "\A3\ui_f\data\map\mapcontrol\rock_ca.paa";
				importance = "0.5 * 12 * 0.05";
				size = 12;
			};
			class Ruin
			{
				coefMax = 4;
				coefMin = 1;
				color[] = {0,0,0,1};
				icon = "\A3\ui_f\data\map\mapcontrol\ruin_ca.paa";
				importance = "1.2 * 16 * 0.05";
				size = 16;
			};
			class SmallTree
			{
				coefMax = 4;
				coefMin = 0.25;
				color[] = {0.45,0.64,0.33,0.4};
				icon = "\A3\ui_f\data\map\mapcontrol\bush_ca.paa";
				importance = "0.6 * 12 * 0.05";
				size = 12;
			};
			class Stack
			{
				coefMax = 4;
				coefMin = 0.9;
				color[] = {0,0,0,1};
				icon = "\A3\ui_f\data\map\mapcontrol\stack_ca.paa";
				importance = "2 * 16 * 0.05";
				size = 20;
			};
			class Tourism
			{
				coefMax = 4;
				coefMin = 0.7;
				color[] = {0,0,0,1};
				icon = "\A3\ui_f\data\map\mapcontrol\tourism_ca.paa";
				importance = "1 * 16 * 0.05";
				size = 16;
			};
			class Transmitter
			{
				coefMax = 1;
				coefMin = 0.85;
				color[] = {1,1,1,1};
				icon = "\A3\ui_f\data\map\mapcontrol\transmitter_CA.paa";
				importance = 1;
				size = 24;
			};
			class Tree
			{
				coefMax = 4;
				coefMin = 0.25;
				color[] = {0.45,0.64,0.33,0.4};
				icon = "\A3\ui_f\data\map\mapcontrol\bush_ca.paa";
				importance = "0.9 * 16 * 0.05";
				size = 12;
			};
			class ViewTower
			{
				coefMax = 4;
				coefMin = 0.5;
				color[] = {0,0,0,1};
				icon = "\A3\ui_f\data\map\mapcontrol\viewtower_ca.paa";
				importance = "2.5 * 16 * 0.05";
				size = 16;
			};
			class Watertower
			{
				coefMax = 1;
				coefMin = 0.85;
				color[] = {1,1,1,1};
				icon = "\A3\ui_f\data\map\mapcontrol\watertower_CA.paa";
				importance = 1;
				size = 24;
			};
			class Waypoint
			{
				coefMax = 1;
				coefMin = 1;
				color[] = {1,1,1,1};
				icon = "\a3\ui_f\data\map\mapcontrol\waypoint_ca.paa";
				importance = 1;
				size = 18;
			};
			class WaypointCompleted
			{
				coefMax = 1;
				coefMin = 1;
				color[] = {1,1,1,1};
				icon = "\a3\ui_f\data\map\mapcontrol\waypointcompleted_ca.paa";
				importance = 1;
				size = 18;
			};
		};
		class MiniMap: RscControlsGroupNoScrollbars
		{
			class Controls
			{
				class CA_MiniMap: RscMapControl
				{
					alphaFadeEndScale = 10;
					alphaFadeStartScale = 10;
					colorCountlines[] = {"(profilenamespace getvariable ['IGUI_BCG_RGB_R',0])","(profilenamespace getvariable ['IGUI_BCG_RGB_G',1])","(profilenamespace getvariable ['IGUI_BCG_RGB_B',1])",0.2};
					colorCountlinesWater[] = {"(profilenamespace getvariable ['IGUI_TEXT_RGB_R',0])","(profilenamespace getvariable ['IGUI_TEXT_RGB_G',1])","(profilenamespace getvariable ['IGUI_TEXT_RGB_B',1])",0.4};
					colorForest[] = {"(profilenamespace getvariable ['IGUI_BCG_RGB_R',0])","(profilenamespace getvariable ['IGUI_BCG_RGB_G',1])","(profilenamespace getvariable ['IGUI_BCG_RGB_B',1])",0.7};
					colorForestBorder[] = {"(profilenamespace getvariable ['IGUI_BCG_RGB_R',0])","(profilenamespace getvariable ['IGUI_BCG_RGB_G',1])","(profilenamespace getvariable ['IGUI_BCG_RGB_B',1])",0.5};
					colorLevels[] = {0,0,0,0};
					colorMainCountlines[] = {"(profilenamespace getvariable ['IGUI_BCG_RGB_R',0])","(profilenamespace getvariable ['IGUI_BCG_RGB_G',1])","(profilenamespace getvariable ['IGUI_BCG_RGB_B',1])",0.6};
					colorMainCountlinesWater[] = {"(profilenamespace getvariable ['IGUI_TEXT_RGB_R',0])","(profilenamespace getvariable ['IGUI_TEXT_RGB_G',1])","(profilenamespace getvariable ['IGUI_TEXT_RGB_B',1])",0.7};
					colorPowerLines[] = {"(profilenamespace getvariable ['IGUI_TEXT_RGB_R',0])","(profilenamespace getvariable ['IGUI_TEXT_RGB_G',1])","(profilenamespace getvariable ['IGUI_TEXT_RGB_B',1])",0.5};
					colorRailWay[] = {"(profilenamespace getvariable ['IGUI_TEXT_RGB_R',0])","(profilenamespace getvariable ['IGUI_TEXT_RGB_G',1])","(profilenamespace getvariable ['IGUI_TEXT_RGB_B',1])",1};
					colorRocks[] = {"(profilenamespace getvariable ['IGUI_TEXT_RGB_R',0])","(profilenamespace getvariable ['IGUI_TEXT_RGB_G',1])","(profilenamespace getvariable ['IGUI_TEXT_RGB_B',1])",0.1};
					colorRocksBorder[] = {"(profilenamespace getvariable ['IGUI_TEXT_RGB_R',0])","(profilenamespace getvariable ['IGUI_TEXT_RGB_G',1])","(profilenamespace getvariable ['IGUI_TEXT_RGB_B',1])",0.5};
					colorSea[] = {0,0,0,0.4};
					ptsPerSquareSea = 5;
					sizeExGrid = 0.02;
					sizeExInfo = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 0.8)";
					sizeExLabel = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 0.8)";
					sizeExLevel = 0.02;
					sizeExNames = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 0.8) * 2";
					sizeExUnits = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 0.8)";
					text = "#(argb,8,8,3)color(1,1,1,1)";
					class Bunker
					{
						coefMax = 4;
						coefMin = 0.25;
						color[] = {0,0,0,1};
						icon = "\A3\ui_f\data\map\mapcontrol\bunker_ca.paa";
					};
					class bush: Bush
					{
						color[] = {"(profilenamespace getvariable ['IGUI_BCG_RGB_R',0])","(profilenamespace getvariable ['IGUI_BCG_RGB_G',1])","(profilenamespace getvariable ['IGUI_BCG_RGB_B',1])",0.4};
						icon = "\A3\ui_f\data\map\mapcontrol\bush_ca.paa";
						importance = "0.2 * 14 * 0.05 * 0.05";
						size = "14/2";
					};
					class busstop: BusStop
					{
						coefMax = 1;
						coefMin = 0.85;
						color[] = {"(profilenamespace getvariable ['IGUI_TEXT_RGB_R',0])","(profilenamespace getvariable ['IGUI_TEXT_RGB_G',1])","(profilenamespace getvariable ['IGUI_TEXT_RGB_B',1])",1};
						icon = "\A3\ui_f\data\map\mapcontrol\busstop_CA.paa";
						importance = 1;
						size = 24;
					};
					class Chapel
					{
						coefMax = 1;
						coefMin = 0.85;
						color[] = {0,0,0,1};
						icon = "\A3\ui_f\data\map\mapcontrol\Chapel_CA.paa";
						importance = 1;
						size = 24;
					};
					class church: Church
					{
						coefMax = 1;
						coefMin = 0.85;
						icon = "\A3\ui_f\data\map\mapcontrol\church_CA.paa";
						importance = 1;
						size = 24;
					};
					class Command
					{
						color[] = {1,1,1,1};
						icon = "\a3\ui_f\data\map\mapcontrol\waypoint_ca.paa";
					};
					class Cross
					{
						coefMax = 1;
						coefMin = 0.85;
						color[] = {0,0,0,1};
						icon = "\A3\ui_f\data\map\mapcontrol\Cross_CA.paa";
						importance = 1;
						size = 24;
					};
					class CustomMark
					{
						color[] = {1,1,1,1};
						icon = "\a3\ui_f\data\map\mapcontrol\custommark_ca.paa";
					};
					class Fortress
					{
						color[] = {0,0,0,1};
						icon = "\A3\ui_f\data\map\mapcontrol\bunker_ca.paa";
					};
					class Fountain
					{
						color[] = {0,0,0,1};
						icon = "\A3\ui_f\data\map\mapcontrol\fountain_ca.paa";
						size = 11;
					};
					class fuelstation: Fuelstation
					{
						coefMax = 1;
						coefMin = 0.85;
						icon = "\A3\ui_f\data\map\mapcontrol\fuelstation_CA.paa";
						importance = 1;
						size = 24;
					};
					class hospital: Hospital
					{
						coefMax = 1;
						coefMin = 0.85;
						icon = "\A3\ui_f\data\map\mapcontrol\hospital_CA.paa";
						importance = 1;
						size = 24;
					};
					class Legend
					{
						colorBackground[] = {1,1,1,0.5};
						h = "3.5 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
						sizeEx = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 0.8)";
						w = "10 * (((safezoneW / safezoneH) min 1.2) / 40)";
						x = "SafeZoneX + (((safezoneW / safezoneH) min 1.2) / 40)";
						y = "SafeZoneY + safezoneH - 4.5 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
					};
					class lighthouse: Lighthouse
					{
						coefMax = 1;
						coefMin = 0.85;
						color[] = {"(profilenamespace getvariable ['IGUI_TEXT_RGB_R',0])","(profilenamespace getvariable ['IGUI_TEXT_RGB_G',1])","(profilenamespace getvariable ['IGUI_TEXT_RGB_B',1])",1};
						icon = "\A3\ui_f\data\map\mapcontrol\lighthouse_CA.paa";
						importance = 1;
						size = 24;
					};
					class quay: Quay
					{
						coefMax = 1;
						coefMin = 0.85;
						icon = "\A3\ui_f\data\map\mapcontrol\quay_CA.paa";
						importance = 1;
						size = 24;
					};
					class rock: Rock
					{
						color[] = {"(profilenamespace getvariable ['IGUI_BCG_RGB_R',0])","(profilenamespace getvariable ['IGUI_BCG_RGB_G',1])","(profilenamespace getvariable ['IGUI_BCG_RGB_B',1])",0.8};
						icon = "\A3\ui_f\data\map\mapcontrol\rock_ca.paa";
					};
					class Ruin
					{
						color[] = {0,0,0,1};
						icon = "\A3\ui_f\data\map\mapcontrol\ruin_ca.paa";
					};
					class smalltree: SmallTree
					{
						color[] = {"(profilenamespace getvariable ['IGUI_BCG_RGB_R',0])","(profilenamespace getvariable ['IGUI_BCG_RGB_G',1])","(profilenamespace getvariable ['IGUI_BCG_RGB_B',1])",0.4};
						icon = "\A3\ui_f\data\map\mapcontrol\bush_ca.paa";
					};
					class Stack
					{
						color[] = {0,0,0,1};
						icon = "\A3\ui_f\data\map\mapcontrol\stack_ca.paa";
					};
					class Task
					{
						coefMax = 1;
						coefMin = 1;
						color[] = {"(profilenamespace getvariable ['IGUI_TEXT_RGB_R',0])","(profilenamespace getvariable ['IGUI_TEXT_RGB_G',1])","(profilenamespace getvariable ['IGUI_TEXT_RGB_B',1])","(profilenamespace getvariable ['IGUI_TEXT_RGB_A',0.8])"};
						colorCanceled[] = {0.7,0.7,0.7,1};
						colorCreated[] = {1,1,1,1};
						colorDone[] = {0.7,1,0.3,1};
						colorFailed[] = {1,0.3,0.2,1};
						icon = "\A3\ui_f\data\map\mapcontrol\taskIcon_CA.paa";
						iconCanceled = "\A3\ui_f\data\map\mapcontrol\taskIconCanceled_CA.paa";
						iconCreated = "\A3\ui_f\data\map\mapcontrol\taskIconCreated_CA.paa";
						iconDone = "\A3\ui_f\data\map\mapcontrol\taskIconDone_CA.paa";
						iconFailed = "\A3\ui_f\data\map\mapcontrol\taskIconFailed_CA.paa";
						importance = 1;
						size = 27;
						taskAssigned = "#(argb,8,8,3)color(1,1,1,1)";
						taskCanceled = "#(argb,8,8,3)color(1,0.5,0,1)";
						taskCreated = "#(argb,8,8,3)color(0,0,0,1)";
						taskFailed = "#(argb,8,8,3)color(1,0,0,1)";
						taskNone = "#(argb,8,8,3)color(0,0,0,0)";
						taskSucceeded = "#(argb,8,8,3)color(0,1,0,1)";
					};
					class Tourism
					{
						color[] = {0,0,0,1};
						icon = "\A3\ui_f\data\map\mapcontrol\tourism_ca.paa";
					};
					class transmitter: Transmitter
					{
						coefMax = 1;
						coefMin = 0.85;
						color[] = {"(profilenamespace getvariable ['IGUI_TEXT_RGB_R',0])","(profilenamespace getvariable ['IGUI_TEXT_RGB_G',1])","(profilenamespace getvariable ['IGUI_TEXT_RGB_B',1])",1};
						icon = "\A3\ui_f\data\map\mapcontrol\transmitter_CA.paa";
						importance = 1;
						size = 24;
					};
					class tree: Tree
					{
						color[] = {"(profilenamespace getvariable ['IGUI_BCG_RGB_R',0])","(profilenamespace getvariable ['IGUI_BCG_RGB_G',1])","(profilenamespace getvariable ['IGUI_BCG_RGB_B',1])",0.4};
						icon = "\A3\ui_f\data\map\mapcontrol\bush_ca.paa";
					};
					class ViewTower
					{
						coefMax = 4;
						coefMin = 0.5;
						color[] = {0,0,0,1};
						icon = "\A3\ui_f\data\map\mapcontrol\viewtower_ca.paa";
						size = 16;
					};
					class watertower: Watertower
					{
						coefMax = 1;
						coefMin = 0.85;
						color[] = {"(profilenamespace getvariable ['IGUI_TEXT_RGB_R',0])","(profilenamespace getvariable ['IGUI_TEXT_RGB_G',1])","(profilenamespace getvariable ['IGUI_TEXT_RGB_B',1])",1};
						icon = "\A3\ui_f\data\map\mapcontrol\watertower_CA.paa";
						importance = 1;
						size = 24;
					};
					class Waypoint
					{
						coefMax = 1;
						coefMin = 1;
						color[] = {1,1,1,1};
						icon = "\a3\ui_f\data\map\mapcontrol\waypoint_ca.paa";
						importance = 1;
						size = 18;
					};
					class WaypointCompleted
					{
						coefMax = 1;
						coefMin = 1;
						color[] = {1,1,1,1};
						icon = "\a3\ui_f\data\map\mapcontrol\waypointcompleted_ca.paa";
						importance = 1;
						size = 18;
					};
				};
			};
		};
	};
};
class RscObjectives
{
	active = "\a3\ui_f\data\map\diary\icons\taskcreated_ca.paa";
	cancled = "\a3\ui_f\data\map\diary\icons\taskcanceled_ca.paa";
	done = "\a3\ui_f\data\map\diary\icons\tasksucceeded_ca.paa";
	failed = "\a3\ui_f\data\map\diary\icons\taskfailed_ca.paa";
};
class cfgGroupIcons
{
	class Empty
	{
		markerClass = "System";
	};
	class Flag
	{
		icon = "\A3\ui_f\data\map\markers\system\dummy_ca.paa";
	};
	class b_unknown: Flag
	{
		icon = "\A3\ui_f\data\map\markers\nato\b_unknown.paa";
		markerClass = "NATO_BLUFOR";
		name = "Unknown";
	};
	class b_air: b_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\b_air.paa";
		markerClass = "NATO_BLUFOR";
		name = "Helicopter";
	};
	class b_armor: b_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\b_armor.paa";
		markerClass = "NATO_BLUFOR";
		name = "Armor";
	};
	class b_art: b_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\b_art.paa";
		markerClass = "NATO_BLUFOR";
		name = "Artillery";
	};
	class b_hq: b_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\b_hq.paa";
		name = "HQ";
	};
	class b_inf: b_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\b_inf.paa";
		markerClass = "NATO_BLUFOR";
		name = "Infantry";
	};
	class b_installation: b_unknown
	{
		markerClass = "NATO_BLUFOR";
	};
	class b_maint: b_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\b_maint.paa";
		markerClass = "NATO_BLUFOR";
		name = "Maintenance";
	};
	class b_mech_inf: b_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\b_mech_inf.paa";
		markerClass = "NATO_BLUFOR";
		name = "Mechanized";
	};
	class b_med: b_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\b_med.paa";
		markerClass = "NATO_BLUFOR";
		name = "Medical";
	};
	class b_mortar: b_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\b_mortar.paa";
		markerClass = "NATO_BLUFOR";
		name = "Mortar";
	};
	class b_motor_inf: b_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\b_motor_inf.paa";
		markerClass = "NATO_BLUFOR";
		name = "Motorized";
	};
	class b_naval: b_unknown
	{
		markerClass = "NATO_BLUFOR";
	};
	class b_plane: b_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\b_plane.paa";
		markerClass = "NATO_BLUFOR";
		name = "Plane";
	};
	class b_recon: b_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\b_recon.paa";
		markerClass = "NATO_BLUFOR";
		name = "Recon";
	};
	class b_service: b_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\b_service.paa";
		markerClass = "NATO_BLUFOR";
		name = "Service";
	};
	class b_support: b_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\b_support.paa";
		markerClass = "NATO_BLUFOR";
		name = "Support";
	};
	class b_uav: b_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\b_uav.paa";
		markerClass = "NATO_BLUFOR";
		name = "UAV";
	};
	class c_unknown: b_unknown
	{
		markerClass = "NATO_Civilian";
		name = "Unknown";
	};
	class c_air: c_unknown
	{
		markerClass = "NATO_Civilian";
	};
	class c_car: c_unknown
	{
		markerClass = "NATO_Civilian";
	};
	class c_plane: c_unknown
	{
		markerClass = "NATO_Civilian";
	};
	class c_ship: c_unknown
	{
		markerClass = "NATO_Civilian";
	};
	class Dummy: Flag
	{
		icon = "\A3\ui_f\data\map\markers\system\dummy_ca.paa";
	};
	class group_0: b_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\group_0.paa";
		markerClass = "NATO_Sizes";
		name = "Fire Team";
	};
	class group_1: group_0
	{
		icon = "\A3\ui_f\data\map\markers\nato\group_1.paa";
		markerClass = "NATO_Sizes";
		name = "Squad";
	};
	class group_10: group_0
	{
		icon = "\A3\ui_f\data\map\markers\nato\group_10.paa";
		markerClass = "NATO_Sizes";
		name = "Army";
	};
	class group_11: group_0
	{
		icon = "\A3\ui_f\data\map\markers\nato\group_11.paa";
		markerClass = "NATO_Sizes";
		name = "Army Group";
	};
	class group_2: group_0
	{
		icon = "\A3\ui_f\data\map\markers\nato\group_2.paa";
		markerClass = "NATO_Sizes";
		name = "Section";
	};
	class group_3: group_0
	{
		icon = "\A3\ui_f\data\map\markers\nato\group_3.paa";
		markerClass = "NATO_Sizes";
		name = "Platoon";
	};
	class group_4: group_0
	{
		icon = "\A3\ui_f\data\map\markers\nato\group_4.paa";
		markerClass = "NATO_Sizes";
		name = "Company";
	};
	class group_5: group_0
	{
		icon = "\A3\ui_f\data\map\markers\nato\group_5.paa";
		markerClass = "NATO_Sizes";
		name = "Battalion";
	};
	class group_6: group_0
	{
		icon = "\A3\ui_f\data\map\markers\nato\group_6.paa";
		markerClass = "NATO_Sizes";
		name = "Regiment";
	};
	class group_7: group_0
	{
		icon = "\A3\ui_f\data\map\markers\nato\group_7.paa";
		markerClass = "NATO_Sizes";
		name = "Brigade";
	};
	class group_8: group_0
	{
		icon = "\A3\ui_f\data\map\markers\nato\group_8.paa";
		markerClass = "NATO_Sizes";
		name = "Division";
	};
	class group_9: group_0
	{
		icon = "\A3\ui_f\data\map\markers\nato\group_9.paa";
		markerClass = "NATO_Sizes";
		name = "Corps";
	};
	class hc_selectable
	{
		icon = "\A3\ui_f\data\map\groupicons\selector_selectable_ca.paa";
	};
	class hc_selected
	{
		icon = "\A3\ui_f\data\map\groupicons\selector_selected_ca.paa";
	};
	class hc_selectedEnemy
	{
		icon = "\A3\ui_f\data\map\groupicons\selector_selectedEnemy_ca.paa";
	};
	class n_unknown: b_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\n_unknown.paa";
		markerClass = "NATO_Independent";
		name = "Unknown";
	};
	class n_air: n_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\n_air.paa";
		markerClass = "NATO_Independent";
		name = "Helicopter";
	};
	class n_armor: n_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\n_armor.paa";
		markerClass = "NATO_Independent";
		name = "Armor";
	};
	class n_art: n_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\n_art.paa";
		markerClass = "NATO_Independent";
		name = "Artillery";
	};
	class n_hq: n_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\n_hq.paa";
		name = "HQ";
	};
	class n_inf: n_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\n_inf.paa";
		markerClass = "NATO_Independent";
		name = "Infantry";
	};
	class n_installation: n_unknown
	{
		markerClass = "NATO_Independent";
	};
	class n_maint: n_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\n_maint.paa";
		markerClass = "NATO_Independent";
		name = "Maintenance";
	};
	class n_mech_inf: n_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\n_mech_inf.paa";
		markerClass = "NATO_Independent";
		name = "Mechanized";
	};
	class n_med: n_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\n_med.paa";
		markerClass = "NATO_Independent";
		name = "Medical";
	};
	class n_mortar: n_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\n_mortar.paa";
		markerClass = "NATO_Independent";
		name = "Mortar";
	};
	class n_motor_inf: n_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\n_motor_inf.paa";
		markerClass = "NATO_Independent";
		name = "Motorized";
	};
	class n_naval: n_unknown
	{
		markerClass = "NATO_Independent";
	};
	class n_plane: n_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\n_plane.paa";
		markerClass = "NATO_Independent";
		name = "Plane";
	};
	class n_recon: n_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\n_recon.paa";
		markerClass = "NATO_Independent";
		name = "Recon";
	};
	class n_service: n_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\n_service.paa";
		markerClass = "NATO_Independent";
		name = "Service";
	};
	class n_support: n_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\n_support.paa";
		markerClass = "NATO_Independent";
		name = "Support";
	};
	class n_uav: n_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\n_uav.paa";
		markerClass = "NATO_Independent";
		name = "UAV";
	};
	class o_unknown: b_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\o_unknown.paa";
		markerClass = "NATO_OPFOR";
		name = "Unknown";
	};
	class o_air: o_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\o_air.paa";
		markerClass = "NATO_OPFOR";
		name = "Helicopter";
	};
	class o_armor: o_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\o_armor.paa";
		markerClass = "NATO_OPFOR";
		name = "Armor";
	};
	class o_art: o_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\o_art.paa";
		markerClass = "NATO_OPFOR";
		name = "Artillery";
	};
	class o_hq: o_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\o_hq.paa";
		name = "HQ";
	};
	class o_inf: o_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
		markerClass = "NATO_OPFOR";
		name = "Infantry";
	};
	class o_installation: o_unknown
	{
		markerClass = "NATO_OPFOR";
	};
	class o_maint: o_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\o_maint.paa";
		markerClass = "NATO_OPFOR";
		name = "Maintenance";
	};
	class o_mech_inf: o_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\o_mech_inf.paa";
		markerClass = "NATO_OPFOR";
		name = "Mechanized";
	};
	class o_med: o_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\o_med.paa";
		markerClass = "NATO_OPFOR";
		name = "Medical";
	};
	class o_mortar: o_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\o_mortar.paa";
		markerClass = "NATO_OPFOR";
		name = "Mortar";
	};
	class o_motor_inf: o_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\o_motor_inf.paa";
		markerClass = "NATO_OPFOR";
		name = "Motorized";
	};
	class o_naval: o_unknown
	{
		markerClass = "NATO_OPFOR";
	};
	class o_plane: o_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\o_plane.paa";
		markerClass = "NATO_OPFOR";
		name = "Plane";
	};
	class o_recon: o_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\o_recon.paa";
		markerClass = "NATO_OPFOR";
		name = "Recon";
	};
	class o_service: o_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\o_service.paa";
		markerClass = "NATO_OPFOR";
		name = "Service";
	};
	class o_support: o_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\o_support.paa";
		markerClass = "NATO_OPFOR";
		name = "Support";
	};
	class o_uav: o_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\o_uav.paa";
		markerClass = "NATO_OPFOR";
		name = "UAV";
	};
	class respawn_air: Flag
	{
		markerClass = "NATO_Respawn";
	};
	class respawn_armor: Flag
	{
		markerClass = "NATO_Respawn";
	};
	class respawn_inf: Flag
	{
		markerClass = "NATO_Respawn";
	};
	class respawn_motor: Flag
	{
		markerClass = "NATO_Respawn";
	};
	class respawn_naval: Flag
	{
		markerClass = "NATO_Respawn";
	};
	class respawn_para: Flag
	{
		markerClass = "NATO_Respawn";
	};
	class respawn_plane: Flag
	{
		markerClass = "NATO_Respawn";
	};
	class respawn_unknown: Flag
	{
		markerClass = "NATO_Respawn";
	};
	class Select: Flag
	{
		markerClass = "System";
	};
	class waypoint
	{
		icon = "\A3\ui_f\data\map\groupicons\waypoint.paa";
	};
	class selector_selectable: waypoint
	{
		icon = "\A3\ui_f\data\map\groupicons\selector_selectable_ca.paa";
	};
	class selector_selectedEnemy: waypoint
	{
		icon = "\A3\ui_f\data\map\groupicons\selector_selectedEnemy_ca.paa";
	};
	class selector_selectedFriendly: waypoint
	{
		icon = "\A3\ui_f\data\map\groupicons\selector_selectedFriendly_ca.paa";
	};
	class selector_selectedMission: waypoint
	{
		icon = "\A3\ui_f\data\map\groupicons\selector_selectedMission_ca.paa";
	};
	class u_installation: n_unknown
	{
		markerClass = "NATO_Independent";
	};
};
class CfgMarkers
{
	class Empty
	{
		icon = "\A3\ui_f\data\map\markers\system\empty_ca.paa";
		markerClass = "System";
		shadow = 0;
	};
	class Flag
	{
		color[] = {1,0,0,1};
		icon = "\A3\ui_f\data\map\markers\military\flag_CA.paa";
		name = "Flag";
	};
	class b_unknown: Flag
	{
		icon = "\A3\ui_f\data\map\markers\nato\b_unknown.paa";
		markerClass = "NATO_BLUFOR";
		scope = 1;
		size = 29;
	};
	class b_air: b_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\b_air.paa";
		markerClass = "NATO_BLUFOR";
		scope = 1;
		size = 29;
	};
	class b_armor: b_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\b_armor.paa";
		markerClass = "NATO_BLUFOR";
		scope = 1;
		size = 29;
	};
	class b_art: b_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\b_art.paa";
		markerClass = "NATO_BLUFOR";
		scope = 1;
		size = 29;
	};
	class b_hq: b_unknown
	{
		color[] = {"(profilenamespace getvariable ['Map_BLUFOR_R',0])","(profilenamespace getvariable ['Map_BLUFOR_G',1])","(profilenamespace getvariable ['Map_BLUFOR_B',1])","(profilenamespace getvariable ['Map_BLUFOR_A',0.8])"};
		icon = "\A3\ui_f\data\map\markers\nato\b_hq.paa";
		scope = 1;
		size = 29;
	};
	class b_inf: b_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\b_inf.paa";
		markerClass = "NATO_BLUFOR";
		scope = 1;
		size = 29;
	};
	class b_installation: b_unknown
	{
		markerClass = "NATO_BLUFOR";
		scope = 1;
		size = 29;
	};
	class b_maint: b_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\b_maint.paa";
		markerClass = "NATO_BLUFOR";
		scope = 1;
		size = 29;
	};
	class b_mech_inf: b_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\b_mech_inf.paa";
		markerClass = "NATO_BLUFOR";
		scope = 1;
		size = 29;
	};
	class b_med: b_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\b_med.paa";
		markerClass = "NATO_BLUFOR";
		scope = 1;
		size = 29;
	};
	class b_mortar: b_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\b_mortar.paa";
		markerClass = "NATO_BLUFOR";
		scope = 1;
		size = 29;
	};
	class b_motor_inf: b_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\b_motor_inf.paa";
		markerClass = "NATO_BLUFOR";
		scope = 1;
		size = 29;
	};
	class b_naval: b_unknown
	{
		markerClass = "NATO_BLUFOR";
		scope = 1;
		size = 29;
	};
	class b_plane: b_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\b_plane.paa";
		markerClass = "NATO_BLUFOR";
		scope = 1;
		size = 29;
	};
	class b_recon: b_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\b_recon.paa";
		markerClass = "NATO_BLUFOR";
		scope = 1;
		size = 29;
	};
	class b_service: b_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\b_service.paa";
		markerClass = "NATO_BLUFOR";
		scope = 1;
		size = 29;
	};
	class b_support: b_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\b_support.paa";
		markerClass = "NATO_BLUFOR";
		scope = 1;
		size = 29;
	};
	class b_uav: b_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\b_uav.paa";
		markerClass = "NATO_BLUFOR";
		scope = 1;
		size = 29;
	};
	class c_unknown: b_unknown
	{
		markerClass = "NATO_Civilian";
		scope = 1;
		size = 29;
	};
	class c_air: c_unknown
	{
		markerClass = "NATO_Civilian";
		scope = 1;
		size = 29;
	};
	class c_car: c_unknown
	{
		markerClass = "NATO_Civilian";
		scope = 1;
		size = 29;
	};
	class c_plane: c_unknown
	{
		markerClass = "NATO_Civilian";
		scope = 1;
		size = 29;
	};
	class c_ship: c_unknown
	{
		markerClass = "NATO_Civilian";
		scope = 1;
		size = 29;
	};
	class flag_NATO: Flag
	{
		markerClass = "Flags";
	};
	class flag_AAF: flag_NATO
	{
		markerClass = "Flags";
	};
	class flag_Altis: flag_NATO
	{
		markerClass = "Flags";
	};
	class flag_AltisColonial: flag_NATO
	{
		markerClass = "Flags";
	};
	class flag_Belgium: flag_NATO
	{
		markerClass = "Flags";
	};
	class flag_Canada: flag_NATO
	{
		markerClass = "Flags";
	};
	class flag_Catalonia: flag_NATO
	{
		markerClass = "Flags";
	};
	class flag_Croatia: flag_NATO
	{
		markerClass = "Flags";
	};
	class flag_CSAT: flag_NATO
	{
		markerClass = "Flags";
	};
	class flag_CTRG: flag_NATO
	{
		markerClass = "Flags";
	};
	class flag_CzechRepublic: flag_NATO
	{
		markerClass = "Flags";
	};
	class flag_Denmark: flag_NATO
	{
		markerClass = "Flags";
	};
	class flag_EU: flag_NATO
	{
		markerClass = "Flags";
	};
	class flag_FIA: flag_NATO
	{
		markerClass = "Flags";
	};
	class flag_France: flag_NATO
	{
		markerClass = "Flags";
	};
	class flag_Georgia: flag_NATO
	{
		markerClass = "Flags";
	};
	class flag_Germany: flag_NATO
	{
		markerClass = "Flags";
	};
	class flag_Greece: flag_NATO
	{
		markerClass = "Flags";
	};
	class flag_Hungary: flag_NATO
	{
		markerClass = "Flags";
	};
	class flag_Iceland: flag_NATO
	{
		markerClass = "Flags";
	};
	class flag_Italy: flag_NATO
	{
		markerClass = "Flags";
	};
	class flag_Luxembourg: flag_NATO
	{
		markerClass = "Flags";
	};
	class flag_Netherlands: flag_NATO
	{
		markerClass = "Flags";
	};
	class flag_Norway: flag_NATO
	{
		markerClass = "Flags";
	};
	class flag_Poland: flag_NATO
	{
		markerClass = "Flags";
	};
	class flag_Portugal: flag_NATO
	{
		markerClass = "Flags";
	};
	class flag_Slovakia: flag_NATO
	{
		markerClass = "Flags";
	};
	class flag_Slovenia: flag_NATO
	{
		markerClass = "Flags";
	};
	class flag_Spain: flag_NATO
	{
		markerClass = "Flags";
	};
	class flag_Syndicat: flag_NATO
	{
		markerClass = "Flags";
	};
	class flag_Tanoa: flag_NATO
	{
		markerClass = "Flags";
	};
	class flag_TanoaGendarmerie: flag_NATO
	{
		markerClass = "Flags";
	};
	class flag_UK: flag_NATO
	{
		markerClass = "Flags";
	};
	class flag_UN: flag_NATO
	{
		markerClass = "Flags";
	};
	class flag_USA: flag_NATO
	{
		markerClass = "Flags";
	};
	class flag_Viper: flag_NATO
	{
		markerClass = "Flags";
	};
	class GroundSupport_CAS_WEST
	{
		markerClass = "GroundSupport_CAS_WEST";
	};
	class GroundSupport_ARTY_WEST: GroundSupport_CAS_WEST
	{
		markerClass = "GroundSupport_ARTY_WEST";
	};
	class GroundSupport_ARTY_EAST: GroundSupport_ARTY_WEST
	{
		markerClass = "GroundSupport_ARTY_EAST";
	};
	class GroundSupport_ARTY_RESISTANCE: GroundSupport_ARTY_WEST
	{
		markerClass = "GroundSupport_ARTY_RESISTANCE";
	};
	class GroundSupport_CAS_EAST: GroundSupport_CAS_WEST
	{
		markerClass = "GroundSupport_CAS_EAST";
	};
	class GroundSupport_CAS_RESISTANCE: GroundSupport_CAS_WEST
	{
		markerClass = "GroundSupport_CAS_RESISTANCE";
	};
	class group_0: b_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\group_0.paa";
		markerClass = "NATO_Sizes";
		scope = 1;
		size = 29;
	};
	class group_1: group_0
	{
		icon = "\A3\ui_f\data\map\markers\nato\group_1.paa";
		markerClass = "NATO_Sizes";
		scope = 1;
		size = 29;
	};
	class group_10: group_0
	{
		icon = "\A3\ui_f\data\map\markers\nato\group_10.paa";
		markerClass = "NATO_Sizes";
		scope = 1;
		size = 29;
	};
	class group_11: group_0
	{
		icon = "\A3\ui_f\data\map\markers\nato\group_11.paa";
		markerClass = "NATO_Sizes";
		scope = 1;
		size = 29;
	};
	class group_2: group_0
	{
		icon = "\A3\ui_f\data\map\markers\nato\group_2.paa";
		markerClass = "NATO_Sizes";
		scope = 1;
		size = 29;
	};
	class group_3: group_0
	{
		icon = "\A3\ui_f\data\map\markers\nato\group_3.paa";
		markerClass = "NATO_Sizes";
		scope = 1;
		size = 29;
	};
	class group_4: group_0
	{
		icon = "\A3\ui_f\data\map\markers\nato\group_4.paa";
		markerClass = "NATO_Sizes";
		scope = 1;
		size = 29;
	};
	class group_5: group_0
	{
		icon = "\A3\ui_f\data\map\markers\nato\group_5.paa";
		markerClass = "NATO_Sizes";
		scope = 1;
		size = 29;
	};
	class group_6: group_0
	{
		icon = "\A3\ui_f\data\map\markers\nato\group_6.paa";
		markerClass = "NATO_Sizes";
		scope = 1;
		size = 29;
	};
	class group_7: group_0
	{
		icon = "\A3\ui_f\data\map\markers\nato\group_7.paa";
		markerClass = "NATO_Sizes";
		scope = 1;
		size = 29;
	};
	class group_8: group_0
	{
		icon = "\A3\ui_f\data\map\markers\nato\group_8.paa";
		markerClass = "NATO_Sizes";
		scope = 1;
		size = 29;
	};
	class group_9: group_0
	{
		icon = "\A3\ui_f\data\map\markers\nato\group_9.paa";
		markerClass = "NATO_Sizes";
		scope = 1;
		size = 29;
	};
	class hd_dot: Flag
	{
		icon = "\A3\ui_f\data\map\markers\handdrawn\dot_CA.paa";
		markerClass = "draw";
	};
	class hd_ambush: hd_dot
	{
		color[] = {0,0,0,1};
		icon = "\A3\ui_f\data\map\markers\handdrawn\ambush_CA.paa";
		markerClass = "draw";
		scope = 2;
	};
	class hd_ambush_noShadow: hd_ambush
	{
		color[] = {0,0,0,1};
		icon = "\A3\ui_f\data\map\markers\handdrawn\ambush_CA.paa";
		markerClass = "draw";
	};
	class hd_arrow: hd_dot
	{
		color[] = {0,0,0,1};
		icon = "\A3\ui_f\data\map\markers\handdrawn\arrow_CA.paa";
		markerClass = "draw";
		shadow = 1;
	};
	class hd_arrow_noShadow: hd_arrow
	{
		color[] = {0,0,0,1};
		icon = "\A3\ui_f\data\map\markers\handdrawn\arrow_CA.paa";
		markerClass = "draw";
	};
	class hd_destroy: hd_dot
	{
		color[] = {0,0,0,1};
		icon = "\A3\ui_f\data\map\markers\handdrawn\destroy_CA.paa";
		markerClass = "draw";
		scope = 2;
	};
	class hd_destroy_noShadow: hd_destroy
	{
		color[] = {0,0,0,1};
		icon = "\A3\ui_f\data\map\markers\handdrawn\destroy_CA.paa";
		markerClass = "draw";
	};
	class hd_dot_noShadow: hd_dot
	{
		icon = "\A3\ui_f\data\map\markers\handdrawn\dot_CA.paa";
		markerClass = "draw";
	};
	class hd_end: hd_dot
	{
		icon = "\A3\ui_f\data\map\markers\handdrawn\end_CA.paa";
		markerClass = "draw";
		scope = 2;
	};
	class hd_end_noShadow: hd_end
	{
		icon = "\A3\ui_f\data\map\markers\handdrawn\end_CA.paa";
		markerClass = "draw";
	};
	class hd_flag: hd_dot
	{
		icon = "\A3\ui_f\data\map\markers\handdrawn\flag_CA.paa";
		markerClass = "draw";
	};
	class hd_flag_noShadow: hd_flag
	{
		icon = "\A3\ui_f\data\map\markers\handdrawn\flag_CA.paa";
		markerClass = "draw";
	};
	class hd_join: hd_dot
	{
		icon = "\A3\ui_f\data\map\markers\handdrawn\join_CA.paa";
		markerClass = "draw";
	};
	class hd_join_noShadow: hd_join
	{
		icon = "\A3\ui_f\data\map\markers\handdrawn\join_CA.paa";
		markerClass = "draw";
	};
	class hd_objective: hd_dot
	{
		icon = "\A3\ui_f\data\map\markers\handdrawn\objective_CA.paa";
		markerClass = "draw";
	};
	class hd_objective_noShadow: hd_objective
	{
		icon = "\A3\ui_f\data\map\markers\handdrawn\objective_CA.paa";
		markerClass = "draw";
	};
	class hd_pickup: hd_dot
	{
		icon = "\A3\ui_f\data\map\markers\handdrawn\pickup_CA.paa";
		markerClass = "draw";
	};
	class hd_pickup_noShadow: hd_pickup
	{
		icon = "\A3\ui_f\data\map\markers\handdrawn\pickup_CA.paa";
		markerClass = "draw";
	};
	class hd_start: hd_dot
	{
		icon = "\A3\ui_f\data\map\markers\handdrawn\start_CA.paa";
		markerClass = "draw";
	};
	class hd_start_noShadow: hd_start
	{
		icon = "\A3\ui_f\data\map\markers\handdrawn\start_CA.paa";
		markerClass = "draw";
	};
	class hd_unknown: hd_dot
	{
		icon = "\A3\ui_f\data\map\markers\handdrawn\unknown_CA.paa";
		markerClass = "draw";
	};
	class hd_unknown_noShadow: hd_unknown
	{
		icon = "\A3\ui_f\data\map\markers\handdrawn\unknown_CA.paa";
		markerClass = "draw";
	};
	class hd_warning: hd_dot
	{
		icon = "\A3\ui_f\data\map\markers\handdrawn\warning_CA.paa";
		markerClass = "draw";
	};
	class hd_warning_noShadow: hd_warning
	{
		icon = "\A3\ui_f\data\map\markers\handdrawn\warning_CA.paa";
		markerClass = "draw";
	};
	class loc_Tree: Flag
	{
		markerClass = "Locations";
	};
	class loc_Bunker: loc_Tree
	{
		markerClass = "Locations";
	};
	class loc_Bush: loc_Tree
	{
		markerClass = "Locations";
	};
	class loc_BusStop: loc_Tree
	{
		markerClass = "Locations";
	};
	class loc_Chapel: loc_Tree
	{
		markerClass = "Locations";
	};
	class loc_Church: loc_Tree
	{
		markerClass = "Locations";
	};
	class loc_Cross: loc_Tree
	{
		markerClass = "Locations";
	};
	class loc_Fortress: loc_Bunker
	{
		markerClass = "Locations";
	};
	class loc_Fountain: loc_Tree
	{
		markerClass = "Locations";
	};
	class loc_Fuelstation: loc_Tree
	{
		markerClass = "Locations";
	};
	class loc_Hospital: loc_Tree
	{
		markerClass = "Locations";
	};
	class loc_Lighthouse: loc_Tree
	{
		markerClass = "Locations";
	};
	class loc_Power: loc_Tree
	{
		markerClass = "Locations";
	};
	class loc_PowerSolar: loc_Tree
	{
		markerClass = "Locations";
	};
	class loc_PowerWave: loc_Tree
	{
		markerClass = "Locations";
	};
	class loc_PowerWind: loc_Tree
	{
		markerClass = "Locations";
	};
	class loc_Quay: loc_Tree
	{
		markerClass = "Locations";
	};
	class loc_Rock: loc_Tree
	{
		markerClass = "Locations";
	};
	class loc_Ruin: loc_Tree
	{
		markerClass = "Locations";
	};
	class loc_SmallTree: loc_Tree
	{
		markerClass = "Locations";
	};
	class loc_Stack: loc_Tree
	{
		markerClass = "Locations";
	};
	class loc_Tourism: loc_Tree
	{
		markerClass = "Locations";
	};
	class loc_Transmitter: loc_Tree
	{
		markerClass = "Locations";
	};
	class loc_ViewTower: loc_Tree
	{
		markerClass = "Locations";
	};
	class loc_WaterTower: loc_Tree
	{
		markerClass = "Locations";
	};
	class mil_objective: Flag
	{
		icon = "\A3\ui_f\data\map\markers\military\objective_CA.paa";
		markerClass = "Military";
		name = "Objective";
		scope = 1;
		shadow = 1;
	};
	class mil_ambush: mil_objective
	{
		color[] = {0,0,0,1};
		icon = "\A3\ui_f\data\map\markers\military\ambush_CA.paa";
		markerClass = "Military";
		name = "Ambush";
		scope = 1;
	};
	class mil_ambush_noShadow: mil_ambush
	{
		color[] = {0,0,0,1};
		icon = "\A3\ui_f\data\map\markers\military\ambush_CA.paa";
		markerClass = "Military";
		scope = 0;
		shadow = 0;
	};
	class mil_arrow: mil_objective
	{
		color[] = {0,0,0,1};
		icon = "\A3\ui_f\data\map\markers\military\arrow_CA.paa";
		markerClass = "Military";
		name = "Arrow";
		scope = 1;
		shadow = 1;
	};
	class mil_arrow2: mil_objective
	{
		color[] = {0,0,0,1};
		icon = "\A3\ui_f\data\map\markers\military\arrow2_CA.paa";
		markerClass = "Military";
		name = "Arrow (filled)";
	};
	class mil_arrow2_noShadow: mil_arrow2
	{
		color[] = {0,0,0,1};
		icon = "\A3\ui_f\data\map\markers\military\arrow2_CA.paa";
		markerClass = "Military";
		scope = 0;
		shadow = 0;
	};
	class mil_arrow_noShadow: mil_arrow
	{
		color[] = {0,0,0,1};
		icon = "\A3\ui_f\data\map\markers\military\arrow_CA.paa";
		markerClass = "Military";
		name = "Arrow";
	};
	class mil_box: mil_objective
	{
		color[] = {0,0,0,1};
		icon = "\A3\ui_f\data\map\markers\military\box_CA.paa";
		markerClass = "Military";
		name = "Square";
		scope = 1;
		shadow = 1;
	};
	class mil_box_noShadow: mil_box
	{
		icon = "\A3\ui_f\data\map\markers\military\box_CA.paa";
		markerClass = "Military";
		shadow = 0;
	};
	class mil_circle: mil_objective
	{
		icon = "\A3\ui_f\data\map\markers\military\circle_CA.paa";
		markerClass = "Military";
		name = "Circle";
		scope = 1;
		shadow = 1;
	};
	class mil_circle_noShadow: mil_circle
	{
		icon = "\A3\ui_f\data\map\markers\military\circle_CA.paa";
		markerClass = "Military";
		scope = 0;
		shadow = 0;
	};
	class mil_destroy: mil_objective
	{
		color[] = {0,0,0,1};
		icon = "\A3\ui_f\data\map\markers\military\destroy_CA.paa";
		markerClass = "Military";
		name = "Destroy";
		scope = 1;
		shadow = 1;
	};
	class mil_destroy_noShadow: mil_destroy
	{
		color[] = {0,0,0,1};
		icon = "\A3\ui_f\data\map\markers\military\destroy_CA.paa";
		markerClass = "Military";
		scope = 0;
		shadow = 0;
	};
	class mil_dot: mil_objective
	{
		color[] = {0,0,0,1};
		icon = "\A3\ui_f\data\map\markers\military\dot_CA.paa";
		markerClass = "Military";
		name = "Dot";
		scope = 1;
		shadow = 1;
	};
	class mil_dot_noShadow: mil_dot
	{
		icon = "\A3\ui_f\data\map\markers\military\dot_CA.paa";
		markerClass = "Military";
		scope = 0;
		shadow = 0;
	};
	class mil_end: mil_objective
	{
		icon = "\A3\ui_f\data\map\markers\military\end_CA.paa";
		markerClass = "Military";
		name = "End";
		scope = 1;
		shadow = 1;
	};
	class mil_end_noShadow: mil_end
	{
		icon = "\A3\ui_f\data\map\markers\military\end_CA.paa";
		markerClass = "Military";
		scope = 0;
		shadow = 0;
	};
	class mil_flag: mil_objective
	{
		icon = "\A3\ui_f\data\map\markers\military\flag_CA.paa";
		markerClass = "Military";
		name = "Flag";
		scope = 1;
		shadow = 1;
	};
	class mil_flag_noShadow: mil_flag
	{
		icon = "\A3\ui_f\data\map\markers\military\flag_CA.paa";
		markerClass = "Military";
		scope = 0;
		shadow = 0;
	};
	class mil_join: mil_objective
	{
		icon = "\A3\ui_f\data\map\markers\military\join_CA.paa";
		markerClass = "Military";
		name = "Join";
		scope = 1;
		shadow = 1;
	};
	class mil_join_noShadow: mil_join
	{
		icon = "\A3\ui_f\data\map\markers\military\join_CA.paa";
		markerClass = "Military";
		scope = 0;
		shadow = 0;
	};
	class mil_marker: mil_objective
	{
		icon = "\A3\ui_f\data\map\markers\military\marker_CA.paa";
		markerClass = "Military";
		name = "Marker";
		scope = 1;
		shadow = 1;
	};
	class mil_marker_noShadow: mil_marker
	{
		icon = "\A3\ui_f\data\map\markers\military\marker_CA.paa";
		markerClass = "Military";
		scope = 0;
		shadow = 0;
	};
	class mil_objective_noShadow: mil_objective
	{
		icon = "\A3\ui_f\data\map\markers\military\objective_CA.paa";
		markerClass = "Military";
		scope = 0;
		shadow = 0;
	};
	class mil_pickup: mil_objective
	{
		icon = "\A3\ui_f\data\map\markers\military\pickup_CA.paa";
		markerClass = "Military";
		name = "Pick Up";
		scope = 1;
		shadow = 1;
	};
	class mil_pickup_noShadow: mil_pickup
	{
		icon = "\A3\ui_f\data\map\markers\military\pickup_CA.paa";
		markerClass = "Military";
		scope = 0;
		shadow = 0;
	};
	class mil_start: mil_objective
	{
		icon = "\A3\ui_f\data\map\markers\military\start_CA.paa";
		markerClass = "Military";
		name = "Start";
		scope = 1;
		shadow = 1;
	};
	class mil_start_noShadow: mil_start
	{
		icon = "\A3\ui_f\data\map\markers\military\start_CA.paa";
		markerClass = "Military";
		scope = 0;
		shadow = 0;
	};
	class mil_triangle: mil_objective
	{
		icon = "\A3\ui_f\data\map\markers\military\triangle_CA.paa";
		markerClass = "Military";
		name = "Triangle";
		scope = 1;
		shadow = 1;
	};
	class mil_triangle_noShadow: mil_triangle
	{
		icon = "\A3\ui_f\data\map\markers\military\triangle_CA.paa";
		markerClass = "Military";
		scope = 0;
		shadow = 0;
	};
	class mil_unknown: mil_objective
	{
		icon = "\A3\ui_f\data\map\markers\military\unknown_CA.paa";
		markerClass = "Military";
		name = "Unknown";
		scope = 1;
		shadow = 1;
	};
	class mil_unknown_noShadow: mil_unknown
	{
		icon = "\A3\ui_f\data\map\markers\military\unknown_CA.paa";
		markerClass = "Military";
		shadow = 0;
	};
	class mil_warning: mil_objective
	{
		icon = "\A3\ui_f\data\map\markers\military\warning_CA.paa";
		markerClass = "Military";
		name = "Warning";
		scope = 1;
		shadow = 1;
	};
	class mil_warning_noShadow: mil_warning
	{
		icon = "\A3\ui_f\data\map\markers\military\warning_CA.paa";
		markerClass = "Military";
		scope = 0;
		shadow = 0;
	};
	class n_unknown: b_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\n_unknown.paa";
		markerClass = "NATO_Independent";
		scope = 1;
		size = 29;
	};
	class n_air: n_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\n_air.paa";
		markerClass = "NATO_Independent";
		scope = 1;
		size = 29;
	};
	class n_armor: n_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\n_armor.paa";
		markerClass = "NATO_Independent";
		scope = 1;
		size = 29;
	};
	class n_art: n_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\n_art.paa";
		markerClass = "NATO_Independent";
		scope = 1;
		size = 29;
	};
	class n_hq: n_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\n_hq.paa";
		markerClass = "NATO_Independent";
		scope = 1;
		size = 29;
	};
	class n_inf: n_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\n_inf.paa";
		markerClass = "NATO_Independent";
		scope = 1;
		size = 29;
	};
	class n_installation: n_unknown
	{
		markerClass = "NATO_Independent";
		scope = 1;
		size = 29;
	};
	class n_maint: n_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\n_maint.paa";
		markerClass = "NATO_Independent";
		scope = 1;
		size = 29;
	};
	class n_mech_inf: n_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\n_mech_inf.paa";
		markerClass = "NATO_Independent";
		scope = 1;
		size = 29;
	};
	class n_med: n_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\n_med.paa";
		markerClass = "NATO_Independent";
		scope = 1;
		size = 29;
	};
	class n_mortar: n_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\n_mortar.paa";
		markerClass = "NATO_Independent";
		scope = 1;
		size = 29;
	};
	class n_motor_inf: n_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\n_motor_inf.paa";
		markerClass = "NATO_Independent";
		scope = 1;
		size = 29;
	};
	class n_naval: n_unknown
	{
		markerClass = "NATO_Independent";
		scope = 1;
		size = 29;
	};
	class n_plane: n_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\n_plane.paa";
		markerClass = "NATO_Independent";
		scope = 1;
		size = 29;
	};
	class n_recon: n_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\n_recon.paa";
		markerClass = "NATO_Independent";
		scope = 1;
		size = 29;
	};
	class n_service: n_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\n_service.paa";
		markerClass = "NATO_Independent";
		scope = 1;
		size = 29;
	};
	class n_support: n_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\n_support.paa";
		markerClass = "NATO_Independent";
		scope = 1;
		size = 29;
	};
	class n_uav: n_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\n_uav.paa";
		markerClass = "NATO_Independent";
		scope = 1;
		size = 29;
	};
	class o_unknown: b_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\o_unknown.paa";
		markerClass = "NATO_OPFOR";
		scope = 1;
		size = 29;
	};
	class o_air: o_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\o_air.paa";
		markerClass = "NATO_OPFOR";
		scope = 1;
		size = 29;
	};
	class o_armor: o_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\o_armor.paa";
		markerClass = "NATO_OPFOR";
		scope = 1;
		size = 29;
	};
	class o_art: o_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\o_art.paa";
		markerClass = "NATO_OPFOR";
		scope = 1;
		size = 29;
	};
	class o_hq: o_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\o_hq.paa";
		markerClass = "NATO_OPFOR";
		scope = 1;
		size = 29;
	};
	class o_inf: o_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
		markerClass = "NATO_OPFOR";
		scope = 1;
		size = 29;
	};
	class o_installation: o_unknown
	{
		markerClass = "NATO_OPFOR";
		scope = 1;
		size = 29;
	};
	class o_maint: o_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\o_maint.paa";
		markerClass = "NATO_OPFOR";
		scope = 1;
		size = 29;
	};
	class o_mech_inf: o_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\o_mech_inf.paa";
		markerClass = "NATO_OPFOR";
		scope = 1;
		size = 29;
	};
	class o_med: o_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\o_med.paa";
		markerClass = "NATO_OPFOR";
		scope = 1;
		size = 29;
	};
	class o_mortar: o_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\o_mortar.paa";
		markerClass = "NATO_OPFOR";
		scope = 1;
		size = 29;
	};
	class o_motor_inf: o_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\o_motor_inf.paa";
		markerClass = "NATO_OPFOR";
		scope = 1;
		size = 29;
	};
	class o_naval: o_unknown
	{
		markerClass = "NATO_OPFOR";
		scope = 1;
		size = 29;
	};
	class o_plane: o_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\o_plane.paa";
		markerClass = "NATO_OPFOR";
		scope = 1;
		size = 29;
	};
	class o_recon: o_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\o_recon.paa";
		markerClass = "NATO_OPFOR";
		scope = 1;
		size = 29;
	};
	class o_service: o_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\o_service.paa";
		markerClass = "NATO_OPFOR";
		scope = 1;
		size = 29;
	};
	class o_support: o_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\o_support.paa";
		markerClass = "NATO_OPFOR";
		scope = 1;
		size = 29;
	};
	class o_uav: o_unknown
	{
		icon = "\A3\ui_f\data\map\markers\nato\o_uav.paa";
		markerClass = "NATO_OPFOR";
		scope = 1;
		size = 29;
	};
	class respawn_air: Flag
	{
		markerClass = "NATO_Respawn";
	};
	class respawn_armor: Flag
	{
		markerClass = "NATO_Respawn";
	};
	class respawn_inf: Flag
	{
		markerClass = "NATO_Respawn";
	};
	class respawn_motor: Flag
	{
		markerClass = "NATO_Respawn";
	};
	class respawn_naval: Flag
	{
		markerClass = "NATO_Respawn";
	};
	class respawn_para: Flag
	{
		markerClass = "NATO_Respawn";
	};
	class respawn_plane: Flag
	{
		markerClass = "NATO_Respawn";
	};
	class respawn_unknown: Flag
	{
		markerClass = "NATO_Respawn";
	};
	class Select: Flag
	{
		icon = "\A3\ui_f\data\igui\cfg\islandmap\iconplayer_ca.paa";
		markerClass = "System";
	};
	class waypoint
	{
		icon = "\A3\ui_f\data\map\groupicons\waypoint.paa";
	};
	class selector_selectable: waypoint
	{
		icon = "\A3\ui_f\data\map\groupicons\selector_selectable_ca.paa";
	};
	class selector_selectedEnemy: waypoint
	{
		icon = "\A3\ui_f\data\map\groupicons\selector_selectedEnemy_ca.paa";
	};
	class selector_selectedFriendly: waypoint
	{
		icon = "\A3\ui_f\data\map\groupicons\selector_selectedFriendly_ca.paa";
	};
	class selector_selectedMission: waypoint
	{
		icon = "\A3\ui_f\data\map\groupicons\selector_selectedMission_ca.paa";
	};
	class u_installation: n_unknown
	{
		markerClass = "NATO_Independent";
		scope = 1;
		size = 29;
	};
};
class CfgMarkerColors
{
	class Default
	{
		scope = 1;
	};
	class ColorBlack: Default
	{
		color[] = {0,0,0,1};
	};
	class ColorBlue: Default
	{
		color[] = {0,0,1,1};
	};
	class ColorBrown: Default
	{
		color[] = {0.5,0.25,0,1};
		scope = 1;
	};
	class ColorGreen: Default
	{
		color[] = {0,0.8,0,1};
	};
	class ColorGrey: Default
	{
		scope = 1;
	};
	class ColorKhaki: Default
	{
		color[] = {0.5,0.6,0.4,1};
		scope = 1;
	};
	class ColorOrange: Default
	{
		color[] = {0.85,0.4,0,1};
		scope = 1;
	};
	class ColorPink: Default
	{
		color[] = {1,0.3,0.4,1};
		scope = 1;
	};
	class ColorRed: Default
	{
		color[] = {0.9,0,0,1};
	};
	class ColorWhite: Default
	{
		color[] = {1,1,1,1};
	};
	class ColorYellow: Default
	{
		color[] = {0.85,0.85,0,1};
	};
};
class CfgMarkerBrushes
{
	class Solid
	{
		scope = 1;
	};
	class BDiagonal: Solid
	{
		scope = 1;
		texture = "\A3\ui_f\data\map\markerbrushes\bdiagonal_ca.paa";
	};
	class Border: Solid
	{
		scope = 1;
	};
	class Cross: Solid
	{
		scope = 1;
		texture = "\A3\ui_f\data\map\markerbrushes\cross_ca.paa";
	};
	class DiagGrid: Solid
	{
		scope = 1;
		texture = "\A3\ui_f\data\map\markerbrushes\diaggrid_ca.paa";
	};
	class FDiagonal: Solid
	{
		scope = 1;
		texture = "\A3\ui_f\data\map\markerbrushes\fdiagonal_ca.paa";
	};
	class Grid: Solid
	{
		scope = 1;
		texture = "\A3\ui_f\data\map\markerbrushes\grid_ca.paa";
	};
	class Horizontal: Solid
	{
		scope = 1;
		texture = "\A3\ui_f\data\map\markerbrushes\horizontal_ca.paa";
	};
	class SolidFull: Solid
	{
		scope = 1;
	};
	class Vertical: Solid
	{
		scope = 1;
		texture = "\A3\ui_f\data\map\markerbrushes\vertical_ca.paa";
	};
};
class CfgLocationTypes
{
	class Name;
	class Hill: Name
	{
		texture = "\A3\ui_f\data\map\locationtypes\hill_ca.paa";
	};
	class Flag: Hill
	{
		texture = "\A3\ui_f\data\map\locationtypes\hill_ca.paa";
	};
	class b_unknown: Flag
	{
		markerClass = "NATO_BLUFOR";
	};
	class b_air: b_unknown
	{
		markerClass = "NATO_BLUFOR";
	};
	class b_armor: b_unknown
	{
		markerClass = "NATO_BLUFOR";
	};
	class b_art: b_unknown
	{
		markerClass = "NATO_BLUFOR";
	};
	class b_hq: b_unknown
	{
		markerClass = "NATO_BLUFOR";
	};
	class b_inf: b_unknown
	{
		markerClass = "NATO_BLUFOR";
	};
	class b_installation: b_unknown
	{
		markerClass = "NATO_BLUFOR";
	};
	class b_maint: b_unknown
	{
		markerClass = "NATO_BLUFOR";
	};
	class b_mech_inf: b_unknown
	{
		markerClass = "NATO_BLUFOR";
	};
	class b_med: b_unknown
	{
		markerClass = "NATO_BLUFOR";
	};
	class b_mortar: b_unknown
	{
		markerClass = "NATO_BLUFOR";
	};
	class b_motor_inf: b_unknown
	{
		markerClass = "NATO_BLUFOR";
	};
	class b_naval: b_unknown
	{
		markerClass = "NATO_BLUFOR";
	};
	class b_plane: b_unknown
	{
		markerClass = "NATO_BLUFOR";
	};
	class b_recon: b_unknown
	{
		markerClass = "NATO_BLUFOR";
	};
	class b_service: b_unknown
	{
		markerClass = "NATO_BLUFOR";
	};
	class b_support: b_unknown
	{
		markerClass = "NATO_BLUFOR";
	};
	class b_uav: b_unknown
	{
		markerClass = "NATO_BLUFOR";
	};
	class BorderCrossing: Hill
	{
		texture = "\A3\ui_f\data\map\locationtypes\bordercrossing_ca.paa";
	};
	class c_unknown: b_unknown
	{
		markerClass = "NATO_Civilian";
	};
	class c_air: c_unknown
	{
		markerClass = "NATO_Civilian";
	};
	class c_car: c_unknown
	{
		markerClass = "NATO_Civilian";
	};
	class c_plane: c_unknown
	{
		markerClass = "NATO_Civilian";
	};
	class c_ship: c_unknown
	{
		markerClass = "NATO_Civilian";
	};
	class Strategic: Name
	{
		texture = "\A3\ui_f\data\map\locationtypes\viewpoint_ca.paa";
	};
	class CityCenter: Strategic
	{
		texture = "\A3\ui_f\data\map\locationtypes\viewpoint_ca.paa";
	};
	class FlatArea: Strategic
	{
		texture = "\A3\ui_f\data\map\locationtypes\viewpoint_ca.paa";
	};
	class FlatAreaCity: FlatArea
	{
		texture = "\A3\ui_f\data\map\locationtypes\viewpoint_ca.paa";
	};
	class FlatAreaCitySmall: FlatAreaCity
	{
		texture = "\A3\ui_f\data\map\locationtypes\viewpoint_ca.paa";
	};
	class group_0: b_unknown
	{
		markerClass = "NATO_Sizes";
	};
	class group_1: group_0
	{
		markerClass = "NATO_Sizes";
	};
	class group_10: group_0
	{
		markerClass = "NATO_Sizes";
	};
	class group_11: group_0
	{
		markerClass = "NATO_Sizes";
	};
	class group_2: group_0
	{
		markerClass = "NATO_Sizes";
	};
	class group_3: group_0
	{
		markerClass = "NATO_Sizes";
	};
	class group_4: group_0
	{
		markerClass = "NATO_Sizes";
	};
	class group_5: group_0
	{
		markerClass = "NATO_Sizes";
	};
	class group_6: group_0
	{
		markerClass = "NATO_Sizes";
	};
	class group_7: group_0
	{
		markerClass = "NATO_Sizes";
	};
	class group_8: group_0
	{
		markerClass = "NATO_Sizes";
	};
	class group_9: group_0
	{
		markerClass = "NATO_Sizes";
	};
	class n_unknown: b_unknown
	{
		markerClass = "NATO_Independent";
	};
	class n_air: n_unknown
	{
		markerClass = "NATO_Independent";
	};
	class n_armor: n_unknown
	{
		markerClass = "NATO_Independent";
	};
	class n_art: n_unknown
	{
		markerClass = "NATO_Independent";
	};
	class n_hq: n_unknown
	{
		markerClass = "NATO_Independent";
	};
	class n_inf: n_unknown
	{
		markerClass = "NATO_Independent";
	};
	class n_installation: n_unknown
	{
		markerClass = "NATO_Independent";
	};
	class n_maint: n_unknown
	{
		markerClass = "NATO_Independent";
	};
	class n_mech_inf: n_unknown
	{
		markerClass = "NATO_Independent";
	};
	class n_med: n_unknown
	{
		markerClass = "NATO_Independent";
	};
	class n_mortar: n_unknown
	{
		markerClass = "NATO_Independent";
	};
	class n_motor_inf: n_unknown
	{
		markerClass = "NATO_Independent";
	};
	class n_naval: n_unknown
	{
		markerClass = "NATO_Independent";
	};
	class n_plane: n_unknown
	{
		markerClass = "NATO_Independent";
	};
	class n_recon: n_unknown
	{
		markerClass = "NATO_Independent";
	};
	class n_service: n_unknown
	{
		markerClass = "NATO_Independent";
	};
	class n_support: n_unknown
	{
		markerClass = "NATO_Independent";
	};
	class n_uav: n_unknown
	{
		markerClass = "NATO_Independent";
	};
	class o_unknown: b_unknown
	{
		markerClass = "NATO_OPFOR";
	};
	class o_air: o_unknown
	{
		markerClass = "NATO_OPFOR";
	};
	class o_armor: o_unknown
	{
		markerClass = "NATO_OPFOR";
	};
	class o_art: o_unknown
	{
		markerClass = "NATO_OPFOR";
	};
	class o_hq: o_unknown
	{
		markerClass = "NATO_OPFOR";
	};
	class o_inf: o_unknown
	{
		markerClass = "NATO_OPFOR";
	};
	class o_installation: o_unknown
	{
		markerClass = "NATO_OPFOR";
	};
	class o_maint: o_unknown
	{
		markerClass = "NATO_OPFOR";
	};
	class o_mech_inf: o_unknown
	{
		markerClass = "NATO_OPFOR";
	};
	class o_med: o_unknown
	{
		markerClass = "NATO_OPFOR";
	};
	class o_mortar: o_unknown
	{
		markerClass = "NATO_OPFOR";
	};
	class o_motor_inf: o_unknown
	{
		markerClass = "NATO_OPFOR";
	};
	class o_naval: o_unknown
	{
		markerClass = "NATO_OPFOR";
	};
	class o_plane: o_unknown
	{
		markerClass = "NATO_OPFOR";
	};
	class o_recon: o_unknown
	{
		markerClass = "NATO_OPFOR";
	};
	class o_service: o_unknown
	{
		markerClass = "NATO_OPFOR";
	};
	class o_support: o_unknown
	{
		markerClass = "NATO_OPFOR";
	};
	class o_uav: o_unknown
	{
		markerClass = "NATO_OPFOR";
	};
	class respawn_air: Flag
	{
		markerClass = "NATO_Respawn";
	};
	class respawn_armor: Flag
	{
		markerClass = "NATO_Respawn";
	};
	class respawn_inf: Flag
	{
		markerClass = "NATO_Respawn";
	};
	class respawn_motor: Flag
	{
		markerClass = "NATO_Respawn";
	};
	class respawn_naval: Flag
	{
		markerClass = "NATO_Respawn";
	};
	class respawn_para: Flag
	{
		markerClass = "NATO_Respawn";
	};
	class respawn_plane: Flag
	{
		markerClass = "NATO_Respawn";
	};
	class respawn_unknown: Flag
	{
		markerClass = "NATO_Respawn";
	};
	class RockArea: Hill
	{
		texture = "\A3\ui_f\data\map\locationtypes\rockarea_ca.paa";
	};
	class StrongpointArea: Strategic
	{
		texture = "\A3\ui_f\data\map\locationtypes\viewpoint_ca.paa";
	};
	class u_installation: n_unknown
	{
		markerClass = "NATO_Independent";
	};
	class VegetationBroadleaf: Hill
	{
		texture = "\A3\ui_f\data\map\locationtypes\vegetationbroadleaf_ca.paa";
	};
	class VegetationFir: Hill
	{
		texture = "\A3\ui_f\data\map\locationtypes\vegetationfir_ca.paa";
	};
	class VegetationPalm: Hill
	{
		texture = "\A3\ui_f\data\map\locationtypes\vegetationpalm_ca.paa";
	};
	class VegetationVineyard: Hill
	{
		texture = "\A3\ui_f\data\map\locationtypes\vegetationvineyard_ca.paa";
	};
	class ViewPoint: Hill
	{
		texture = "\A3\ui_f\data\map\locationtypes\viewpoint_ca.paa";
	};
};
