class CfgPatches
{
	class DisabledSupersonicCrack_Sounds_C_PvPscene
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Data_F_Loadorder","A3_Data_F_Curator_Loadorder","A3_Data_F_Kart_Loadorder","A3_Data_F_Bootcamp_Loadorder","A3_Data_F_Heli_Loadorder","A3_Data_F_Mark_Loadorder","A3_Data_F_Exp_A_Loadorder","A3_Data_F_Exp_B_Loadorder","A3_Data_F_Exp_Loadorder","A3_Data_F_Jets_Loadorder","A3_Data_F_Argo_Loadorder","A3_Data_F_Patrol_Loadorder","A3_Data_F_Orange_Loadorder","A3_Data_F_Tacops_Loadorder","A3_Data_F_Tank_Loadorder"};
		version = "2019-03-28";
	};
};
class CfgAmmo
{
	class BulletCore;
	class GrenadeCore;
	class MissileCore;
	class RocketCore;
	class ShellCore;

	class BulletBase: BulletCore
	{
		supersonicCrackNear[] = {"",1,1,1};//{"A3\sounds_f\arsenal\sfx\supersonic_crack\scrack_close",3.16228,1,200};
		supersonicCrackFar[] = {"",1,1,1};//{"A3\sounds_f\arsenal\sfx\supersonic_crack\scrack_middle",3.16228,1,200};
		class SuperSonicCrack
		{
			superSonicCrack[] = {"",1,1,1};//{"A3\sounds_f\arsenal\sfx\supersonic_crack\sc_meadow1",3.16228,1,200};
			class SCrackForest
			{
				sound1[] = {"",1,1,1};//{"A3\sounds_f\arsenal\sfx\supersonic_crack\sc_forest1",1,1,500};
				sound2[] = {"",1,1,1};//{"A3\sounds_f\arsenal\sfx\supersonic_crack\sc_forest2",1,1,500};
				sound3[] = {"",1,1,1};//{"A3\sounds_f\arsenal\sfx\supersonic_crack\sc_forest3",1,1,500};
				sounds[] = {"sound1",0.333,"sound2",0.333,"sound3",0.333};
			};
			class SCrackTrees
			{
				sound1[] = {"",1,1,1};//{"A3\sounds_f\arsenal\sfx\supersonic_crack\sc_trees1",1,1,500};
				sound2[] = {"",1,1,1};//{"A3\sounds_f\arsenal\sfx\supersonic_crack\sc_trees2",1,1,500};
				sound3[] = {"",1,1,1};//{"A3\sounds_f\arsenal\sfx\supersonic_crack\sc_trees3",1,1,500};
			};
			class SCrackMeadow
			{
				sound1[] = {"",1,1,1};//{"A3\sounds_f\arsenal\sfx\supersonic_crack\sc_meadow1",1,1,500};
				sound2[] = {"",1,1,1};//{"A3\sounds_f\arsenal\sfx\supersonic_crack\sc_meadow2",1,1,500};
				sound3[] = {"",1,1,1};//{"A3\sounds_f\arsenal\sfx\supersonic_crack\sc_meadow3",1,1,500};
			};
			class SCrackHouses
			{
				sound1[] = {"",1,1,1};//{"A3\sounds_f\arsenal\sfx\supersonic_crack\sc_houses1",1,1,500};
				sound2[] = {"",1,1,1};//{"A3\sounds_f\arsenal\sfx\supersonic_crack\sc_houses2",1,1,500};
				sound3[] = {"",1,1,1};//{"A3\sounds_f\arsenal\sfx\supersonic_crack\sc_houses3",1,1,500};
			};
		};
	};
	class ShellBase: ShellCore
	{
		supersonicCrackNear[] = {"",1,1,1};//{"A3\Sounds_F\weapons\Explosion\supersonic_crack_close",0.316228,1,50};
		supersonicCrackFar[] = {"",1,1,1};//{"A3\Sounds_F\weapons\Explosion\supersonic_crack_50meters",0.223872,1,150};
	};
	class MissileBase: MissileCore
	{
		supersonicCrackNear[] = {"",1,1,1};//{"A3\Sounds_F\weapons\Explosion\supersonic_crack_close",0.398107,1,20};
		supersonicCrackFar[] = {"",1,1,1};//{"A3\Sounds_F\weapons\Explosion\supersonic_crack_50meters",0.316228,1,50};
	};
	class RocketBase: RocketCore
	{
		supersonicCrackNear[] = {"",1,1,1};//{"",1,1,50};
		supersonicCrackFar[] = {"",1,1,1};//{"",1,1,150};
	};
	class GrenadeBase: GrenadeCore
	{
		supersonicCrackNear[] = {"",1,1,1};//{"A3\Sounds_F\weapons\Explosion\supersonic_crack_close",0.316228,1,50};
		supersonicCrackFar[] = {"",1,1,1};//{"A3\Sounds_F\weapons\Explosion\supersonic_crack_50meters",0.223872,1,75};
	};
};
