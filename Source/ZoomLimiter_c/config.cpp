class CfgPatches
{
	class WW2_Optional_Zoom_Limiter_c
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"WW2_Assets_c_Weapons_InfantryWeapons_c"};
		version = "2017-12-07";
	};
};
class cfgWeapons
{
	class Binocular;
	class Launcher_Base_F;
	class MGun;
	class Pistol_Base_F;
	class Rifle_Base_F;
	class Rifle_Long_Base_F;
	class Rifle_Short_Base_F;
	class LIB_DT;

	class LIB_Binocular_base: Binocular
	{
		class OpticsModes
		{
			class Scope
			{
				opticsZoomMin = 0.066;
				opticsZoomInit = 0.066;
				opticsZoomMax = 0.066;
			};
			class Eyes
			{
				opticsZoomMin = 0.4;
				opticsZoomInit = 0.4;
				opticsZoomMax = 0.4;
			};
		};
	};
	class LIB_PISTOL: Pistol_Base_F
	{
		opticsZoomMin = 0.4;
		opticsZoomInit = 1;
		opticsZoomMax = 0.4;
	};
	class LIB_SMG: Rifle_Short_Base_F
	{
		opticsZoomMin = 0.4;
		opticsZoomInit = 1;
		opticsZoomMax = 0.4;
	};
	class LIB_RIFLE: Rifle_Base_F
	{
		opticsZoomMin = 0.4;
		opticsZoomInit = 1;
		opticsZoomMax = 0.4;
	};
	class LIB_LMG: Rifle_Long_Base_F
	{
		opticsZoomMin = 0.4;
		opticsZoomInit = 1;
		opticsZoomMax = 0.4;
	};
	class LIB_SRIFLE: Rifle_Long_Base_F
	{
		opticsZoomMin = 0.4;
		opticsZoomInit = 1;
		opticsZoomMax = 0.4;
	};
	class LIB_K98ZF39: LIB_SRIFLE
	{
		class OpticsModes
		{
			class Scope
			{
				opticsZoomMin = 0.1;
				opticsZoomInit = 0.1;
				opticsZoomMax = 0.1;
				discretefov[] = {0.1,0.1};
				discreteInitIndex = 0;
			};
			class Ironsights
			{
				opticsZoomMin = 0.4;
				opticsZoomInit = 1;
				opticsZoomMax = 0.4;
				discretefov[] = {0.4,0.4};
				discreteInitIndex = 0;
			};
		};
	};
	class LIB_M9130PU: LIB_SRIFLE
	{
		class OpticsModes
		{
			class Scope
			{
				opticsZoomMin = 0.114;
				opticsZoomInit = 0.114;
				opticsZoomMax = 0.114;
				discretefov[] = {0.114,0.114};
				discreteInitIndex = 0;
			};
			class Ironsights
			{
				opticsZoomMin = 0.4;
				opticsZoomInit = 1;
				opticsZoomMax = 0.4;
				discretefov[] = {0.4,0.4};
				discreteInitIndex = 0;
			};
		};
	};
	class LIB_M1903A4_Springfield: LIB_SRIFLE
	{
		opticsZoomMin = 0.18;
		opticsZoomInit = 0.18;
		opticsZoomMax = 0.18;
	};
	class LIB_LAUNCHER: Launcher_Base_F
	{
		opticsZoomMin = 0.4;
		opticsZoomInit = 1;
		opticsZoomMax = 0.4;
	};
	class LIB_PzFaust_30m: LIB_LAUNCHER
	{
		opticsZoomMin = 0.6;
		opticsZoomInit = 1;
		opticsZoomMax = 0.6;
	};
	class LIB_DT_OPTIC: LIB_DT
	{
		opticsZoomMin = 0.194;
		opticsZoomInit = 0.194;
		opticsZoomMax = 0.194;
	};
};
