class CfgPatches
{
	class ShortendModline_Server_C_A3_PvPscene
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Data_F_Loadorder","A3_Data_F_Curator_Loadorder","A3_Data_F_Kart_Loadorder","A3_Data_F_Bootcamp_Loadorder","A3_Data_F_Heli_Loadorder","A3_Data_F_Mark_Loadorder","A3_Data_F_Exp_A_Loadorder","A3_Data_F_Exp_B_Loadorder","A3_Data_F_Exp_Loadorder","A3_Data_F_Jets_Loadorder","A3_Data_F_Argo_Loadorder","A3_Data_F_Patrol_Loadorder","A3_Data_F_Orange_Loadorder","A3_Data_F_Tacops_Loadorder","A3_Data_F_Tank_Loadorder"};
		version = "2019-03-28";
	};
};
class CfgMods
{
	class Mod_Base;

	class A3: Mod_Base
	{
		name = "A3";
	};
	class DLCBundle: Mod_Base
	{
		name = "DLC1";
	};
	class Curator: Mod_Base
	{
		name = "Zeus";
	};
	class Kart: Mod_Base
	{
		name = "Karts";
	};
	class Heli: Mod_Base
	{
		name = "Heli";
	};
	class Mark: Mod_Base
	{
		name = "Mark";
	};
	class Expansion: Mod_Base
	{
		name = "Apex";
	};
	class DLCBundle2: Mod_Base
	{
		name = "DLC2";
	};
	class Jets: Mod_Base
	{
		name = "Jets";
	};
	class Argo: Mod_Base
	{
		name = "Argo";
	};
};
