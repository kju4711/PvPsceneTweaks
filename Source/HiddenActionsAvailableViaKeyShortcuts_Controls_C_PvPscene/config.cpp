class CfgPatches
{
	class HiddenActionsAvailableViaKeyShortcuts_Controls_C_PvPscene
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Data_F_Loadorder","A3_Data_F_Curator_Loadorder","A3_Data_F_Kart_Loadorder","A3_Data_F_Bootcamp_Loadorder","A3_Data_F_Heli_Loadorder","A3_Data_F_Mark_Loadorder","A3_Data_F_Exp_A_Loadorder","A3_Data_F_Exp_B_Loadorder","A3_Data_F_Exp_Loadorder","A3_Data_F_Jets_Loadorder","A3_Data_F_Argo_Loadorder","A3_Data_F_Patrol_Loadorder","A3_Data_F_Orange_Loadorder","A3_Data_F_Tacops_Loadorder","A3_Data_F_Tank_Loadorder"};
		version = "2019-03-28";
	};
};
class CfgActions
{
	class None
	{
////		show = 1;
////		showWindow = 0;
//		shortcut = "";
//		text = "";
	};
	class HookCargo: None
	{
//		text = "Hook";
	};
	class UnhookCargo: None
	{
//		text = "Unhook";
	};
	class LoadVehicle: None
	{
//		text = "Load vehicle";
	};
	class UnloadVehicle: None
	{
//		text = "Unload vehicle";
	};
	class UnloadAllVehicles: None
	{
//		text = "Unload all vehicles";
	};
	class GetInCommander: None
	{
//		text = "Get in %1 as commander";
//		showWindow = 1;
//		showIn3D = 1;
	};
	class GetInDriver: None
	{
//		text = "Get in %1 as Driver";
//		showWindow = 1;
//		showIn3D = 1;
	};
	class GetInPilot: None
	{
//		text = "Get in %1 as Pilot";
//		showWindow = 1;
//		showIn3D = 1;
	};
	class GetInGunner: None
	{
//		text = "Get in %1 as gunner";
//		showWindow = 1;
//		showIn3D = 1;
	};
	class GetInCargo: None
	{
//		text = "Get in %1 Ride in back";
//		showWindow = 1;
//		showIn3D = 1;
	};
	class GetInTurret: None
	{
//		text = "Get in %1 as %2";
//		showWindow = 1;
//		showIn3D = 1;
	};
	class PutInPilot: None
	{
//		showWindow = 1;
//		text = "Load In Pilot";
	};
	class PutInDriver: None
	{
//		showWindow = 1;
//		text = "Load In Driver";
	};
	class PutInCargo: None
	{
//		showWindow = 1;
//		text = "Load In Cargo";
	};
	class Heal: None
	{
//		text = "Treat at %1";
//		showWindow = 1;
	};
	class HealSoldier: None
	{
//		text = "Treat %1";
//		showWindow = 1;
	};
	class RepairVehicle: None
	{
//		text = "Repair %1";
//		showWindow = 1;
	};
	class FirstAid: None
	{
//		text = "First Aid";
//		showWindow = 1;
	};
	class DragSoldier: None
	{
//		text = "Drag %1";
	};
	class DragSoldierInterrupt: None
	{
//		text = "Drop Draged";
	};
	class CarrySoldier: None
	{
//		text = "Carry %1";
	};
	class CarrySoldierInterrupt: None
	{
//		text = "Drop Body";
	};
	class DropCarried: None
	{
//		text = "Drop Carried";
	};
	class Repair: None
	{
//		text = "Repair at %1";
//		showWindow = 1;
	};
	class Refuel: None
	{
//		text = "Refuel at %1";
//		showWindow = 1;
	};
	class Rearm: None
	{
//		text = "Rearm at %1";
//		showWindow = 1;
	};
	class GetOut: None
	{
//		shortcut = "GetOut";
		hideActions[] = {"GetOut"};
//		text = "Get out";
//		showWindow = 0;
	};
	class LightOn: None
	{
//		shortcut = "Headlights";
		hideActions[] = {"Headlights"};
//		text = "Lights on";
//		showIn3D = 87;
//		showWindow = 0;
	};
	class LightOff: LightOn
	{
//		shortcut = "Headlights";
		hideActions[] = {"Headlights"};
//		text = "Lights off";
	};
	class SearchLightOn: None
	{
//		show = 1;
//		text = "Searchlight on";
	};
	class SearchLightOff: SearchLightOn
	{
//		text = "Searchlight off";
	};
	class CollisionLightOn: None
	{
//		show = 1;
//		text = "Collision lights on";
//		showIn3D = 87;
//		showWindow = 0;
	};
	class CollisionLightOff: CollisionLightOn
	{
//		text = "Collision lights off";
	};
	class GunLightOn: None
	{
//		show = 0;
//		text = "Light on";
	};
	class GunLightOff: None
	{
//		show = 0;
//		text = "Light off";
	};
	class ArtilleryComputer: None
	{
//		show = 1;
//		text = "Artillery computer";
	};
	class EngineOn: None
	{
//		shortcut = "EngineToggle";
		hideActions[] = {"EngineToggle"};
//		text = "Engine on";
//		showWindow = 0;
	};
	class EngineOff: None
	{
//		shortcut = "EngineToggle";
		hideActions[] = {"EngineToggle"};
//		text = "Engine off";
//		show = 1;
//		showWindow = 0;
	};
	class ActiveSensorsOn: None
	{
//		showWindow = 0;
//		shortcut = "ActiveSensorsToggle";
//		text = "Radar on";
//		hideActions[] = {"ActiveSensorsToggle"};
	};
	class ActiveSensorsOff: ActiveSensorsOn
	{
//		shortcut = "ActiveSensorsToggle";
//		showWindow = 0;
//		text = "Radar off";
	};
	class TakeVehicleControl: None
	{
//		text = "Take controls";
	};
	class SuspendVehicleControl: None
	{
//		text = "Release controls";
	};
	class LockVehicleControl: None
	{
//		text = "Lock controls";
	};
	class UnlockVehicleControl: None
	{
//		text = "Unlock controls";
	};
	class SwitchWeapon: None
	{
//		hideActions[] = {"SwitchSecondary"};
//		shortcut = "SwitchWeapon";
//		text = "Weapon %1";
	};
	class SwitchMagazine: SwitchWeapon
	{
//		shortcut = "ReloadMagazine";
		hideActions[] = {"ReloadMagazine"};
	};
	class HideWeapon: SwitchWeapon
	{
//		text = "Hide %1";
	};
	class UseWeapon: None
	{
//		text = "%1";
	};
	class LoadMagazine: None
	{
//		shortcut = "ReloadMagazine";
		hideActions[] = {"ReloadMagazine"};//???
//		text = "Reload %1";
//		show = 0;
	};
	class LoadOtherMagazine: LoadMagazine
	{
//		show = 1;
		showWindow = 1;//0;
	};
	class LoadEmptyMagazine: LoadMagazine
	{
//		show = 1;
//		showWindow = 1;
	};
	class TakeWeapon: None
	{
//		text = "Take %1%2";
//		showWindow = 1;
	};
	class TakeDropWeapon: TakeWeapon
	{
//		text = "Take %1 (drop %2)%3";
	};
	class TakeMagazine: None
	{
//		text = "Take %1%2";
//		showWindow = 1;
	};
	class TakeDropMagazine: TakeMagazine
	{
//		text = "Take %1 (drop %2)%3";
	};
	class TakeFlag: None
	{
//		text = "Take Flag";
//		showWindow = 1;
	};
	class ReturnFlag: None
	{
//		text = "Return Flag";
//		showWindow = 1;
	};
	class TurnIn: None
	{
//		shortcut = "TurnIn";
		hideActions[] = {"TurnIn"};
//		text = "Turn in";
	};
	class TurnOut: None
	{
//		shortcut = "TurnOut";
		hideActions[] = {"TurnOut"};
//		text = "Turn out";
	};
	class WeaponInHand: None
	{
//		text = "%1 in hand";
//		show = 0;
	};
	class WeaponOnBack: None
	{
//		text = "%1 on back";
//		show = 0;
	};
	class SitDown: None
	{
//		shortcut = "SitDown";
		hideActions[] = {"SitDown"};
//		show = 0;
//		text = "Sit down";
	};
	class Land: None
	{
//		text = "Landing autopilot";
	};
	class CancelLand: None
	{
//		text = "Landing autopilot off";
	};
	class Eject: None
	{
//		shortcut = "Eject";
		hideActions[] = {"Eject"};
//		text = "Eject";
//		showWindow = 0;
	};
	class MoveToDriver: None
	{
//		shortcut = "SwapGunner";
//		hideActions[] = {"SwapGunner"};
//		text = "To Driver's seat";
//		showWindow = 0;
	};
	class MoveToPilot: MoveToDriver
	{
//		text = "To Pilot's seat";
	};
	class MoveToGunner: None
	{
//		shortcut = "SwapGunner";
		hideActions[] = {"SwapGunner"};
//		text = "To Gunner's seat";
//		showWindow = 0;
	};
	class MoveToCommander: None
	{
//		shortcut = "SwapGunner";
		hideActions[] = {"SwapGunner"};
//		text = "To Commander's seat";
//		showWindow = 0;
	};
	class MoveToCargo: None
	{
//		text = "To Passenger seat";
//		showWindow = 0;
	};
	class MoveToTurret: None
	{
//		shortcut = "SwapGunner";
//		hideActions[] = {"SwapGunner"};
//		text = "To %2's seat";
//		showWindow = 0;
	};
	class HideBody: None
	{
//		text = "Hide body";
	};
	class TouchOff: None
	{
//		text = "Touch off %1 bomb(s)";
//		showWindow = 1;
	};
	class TouchOffMines: None
	{
//		text = "Touch off %1 bomb(s)";
//		showWindow = 1;
	};
	class SetTimer: None
	{
//		text = "Set timer +%1 sec. (%2 remaining)";
//		showWindow = 1;
	};
	class StartTimer: SetTimer
	{
//		text = "Set timer on (%1 seconds)";
	};
	class Deactivate: None
	{
//		text = "Deactivate bomb";
//		showWindow = 1;
	};
	class NVGoggles: None
	{
//		text = "Put on NV goggles";
//		show = 0;
	};
	class NVGogglesOff: NVGoggles
	{
//		text = "Take off NV goggles";
//		show = 0;
	};
	class ManualFire: None
	{
//		shortcut = "HeliManualFire";
		hideActions[] = {"HeliManualFire"};
//		text = "Manual fire";
	};
	class ManualFireCancel: ManualFire
	{
//		text = "Cancel manual fire";
	};
	class AutoHover: None
	{
//		shortcut = "AutoHover";
		hideActions[] = {"AutoHover"};
//		showWindow = 0;
//		text = "Auto-hover on";
	};
	class AutoHoverCancel: AutoHover
	{
//		shortcut = "AutoHoverCancel";
		hideActions[] = {"AutoHoverCancel"};
//		text = "Auto-hover off";
//		showWindow = 0;
	};
	class VTOLVectoring: AutoHover
	{
//		shortcut = "VTOLVectoringCancel";
		hideActions[] = {"VTOLVectoringCancel"};
//		text = "Auto-vectoring off";
//		showWindow = 0;
	};
	class VTOLVectoringCancel: AutoHover
	{
//		shortcut = "VTOLVectoring";
		hideActions[] = {"VTOLVectoring"};
//		text = "Auto-vectoring on";
//		showWindow = 0;
	};
	class StrokeFist: None
	{
//		text = "Strike with fist";
	};
	class StrokeGun: None
	{
//		text = "Strike with weapon";
	};
	class LadderUp: None
	{
//		text = "Climb Ladder Up";
	};
	class LadderDown: None
	{
//		text = "Climb Ladder Down";
	};
	class LadderOnDown: None
	{
//		text = "Climb Ladder Down";
//		showWindow = 1;
	};
	class LadderOnUp: None
	{
//		text = "Climb Ladder Up";
//		showWindow = 1;
	};
	class LadderOff: None
	{
//		showWindow = 1;
//		text = "Drop Down Ladder";
	};
	class FireInflame: None
	{
//		showWindow = 1;
//		text = "Light fire";
	};
	class FirePutDown: None
	{
//		showWindow = 1;
//		text = "Put out fire";
	};
	class LandGear: None
	{
//		shortcut = "LandGear";
		hideActions[] = {"LandGear"};
//		text = "Gear down";
	};
	class LandGearUp: LandGear
	{
//		shortcut = "LandGearUp";
		hideActions[] = {"LandGearUp"};
//		text = "Gear up";
	};
	class FlapsDown: None
	{
//		shortcut = "FlapsDown";
		hideActions[] = {"FlapsDown"};
//		text = "Flaps down";
	};
	class FlapsUp: None
	{
//		shortcut = "FlapsUp";
		hideActions[] = {"FlapsUp"};
//		text = "Flaps up";
	};
	class VectoringDown: None
	{
//		shortcut = "FlapsUp";
		hideActions[] = {"FlapsUp"};
//		text = "Vectoring decrease";
	};
	class VectoringUp: None
	{
//		shortcut = "FlapsDown";
		hideActions[] = {"FlapsDown"};
//		text = "Vectoring increase";
	};
	class Salute: None
	{
//		shortcut = "Salute";
		hideActions[] = {"Salute"};
//		show = 0;
//		text = "Salute";
	};
	class ScudLaunch: None
	{
//		text = "Prepare Scud launch";
	};
	class ScudStart: None
	{
//		text = "Launch Scud";
	};
	class ScudCancel: None
	{
//		text = "Cancel Scud launch";
	};
	class User: None
	{
//		showWindow = 1;
//		text = "%1";
	};
	class DropWeapon: None
	{
//		showWindow = 1;
//		text = "Drop %1";
	};
	class PutWeapon: DropWeapon
	{
//		showWindow = 1;
//		text = "Put %1 to %2";
	};
	class DropMagazine: None
	{
//		text = "Drop %1";
	};
	class PutMagazine: DropMagazine
	{
//		text = "Put %1 to %2";
	};
	class UserType: None
	{
//		showWindow = 1;
//		text = "%1";
	};
	class HandGunOn: None
	{
//		text = "Weapon %1";
//		hideActions[] = {"SwitchHandGun"};
	};
	class HandGunOnStand: HandGunOn
	{
//		text = "%1 in hand";
	};
	class HandGunOff: None
	{
//		text = "Weapon %1";
//		hideActions[] = {"SwitchPrimary"};
	};
	class HandGunOffStand: HandGunOff
	{
//		text = "%1 in hand";
	};
	class TakeMine: None
	{
//		showWindow = 1;
//		text = "Take mine";
	};
	class UseContainerMagazine: None
	{
//		text = "Activate mine";
//		showWindow = 1;
	};
	class ActivateMine: None
	{
//		text = "Activate mine";
//		showWindow = 1;
	};
	class DeactivateMine: None
	{
//		text = "Deactivate mine";
//		showWindow = 1;
	};
	class UseMagazine: None
	{
//		text = "%1";
	};
	class IngameMenu: None
	{
//		shortcut = "MenuSelect";
		hideActions[] = {"MenuSelect"};
//		text = "Command menu";
	};
	class CancelTakeFlag: None
	{
//		text = "Cancel action";
//		showWindow = 1;
	};
	class CancelAction: None
	{
//		shortcut = "CancelAction";
		hideActions[] = {"CancelAction"};
//		text = "Cancel action";
//		showWindow = 0;
	};
	class MarkEntity: None
	{
//		showWindow = 1;
//		text = "Collect from %1";
	};
	class MarkWeapon: MarkEntity
	{
//		text = "Collect weapon";
	};
	class TeamSwitch: None
	{
//		shortcut = "TeamSwitch";
		hideActions[] = {"TeamSwitch"};
//		text = "Team switch";
//		show = 0;
	};
	class Gear: None
	{
//		showWindow = 1;
//		shortcut = "Gear";
		hideActions[] = {"Gear"};
//		text = "Inventory";
	};
	class GearOpen: None
	{
//		showWindow = 1;
//		text = "Open subordinate's inventory";
	};
	class OpenBag: None
	{
//		showWindow = 1;
//		text = "Open %1";
	};
	class TakeBag: None
	{
//		text = "Take %1";
//		showWindow = 1;
	};
	class PutBag: None
	{
//		showWindow = 0;
//		text = "Drop %1";
	};
	class DropBag: None
	{
//		showWindow = 0;
//		text = "Drop %1";
	};
	class AddBag: None
	{
//		showWindow = 0;
//		text = "Take %1";
	};
	class IRLaserOn: None
	{
//		show = 0;
//		text = "Laser Enable";
	};
	class IRLaserOff: None
	{
//		show = 0;
//		text = "Laser Disable";
	};
	class Assemble: None
	{
//		showWindow = 0;
//		text = "Assemble %1";
	};
	class DisAssemble: None
	{
//		showWindow = 0;
//		text = "Disassemble %1";
	};
	class Talk: None
	{
//		showWindow = 1;
//		text = "Talk to %1";
	};
	class Tell: None
	{
//		showWindow = 1;
//		text = """%1""";
	};
	class Surrender: None
	{
//		shortcut = "Surrender";
		hideActions[] = {"Surrender"};
//		show = 0;
//		text = "Surrender";
	};
	class GetOver: None
	{
//		shortcut = "GetOver";
		hideActions[] = {"GetOver"};
//		show = 0;
//		text = "Step over";
	};
	class OpenParachute: None
	{
//		text = "Open Parachute";
	};
	class HelicopterAutoTrimOn: None
	{
//		text = "Auto-trim on";
//		show = 1;
	};
	class HelicopterAutoTrimOff: None
	{
//		text = "Auto-trim off";
//		show = 1;
	};
	class HelicopterTrimOn: None
	{
//		text = "Manual trim set";
//		show = 1;
	};
	class HelicopterTrimOff: None
	{
//		text = "Manual trim release";
//		show = 1;
	};
	class WheelsBrakeOn: None
	{
//		show = 1;
//		text = "Wheel brake on";
//		showIn3D = 87;
//		showWindow = 0;
	};
	class WheelsBrakeOff: WheelsBrakeOn
	{
//		text = "Wheel brake off";
	};
	class PeriscopeDepthOn: None
	{
//		text = "Maintain periscope depth";
	};
	class PeriscopeDepthOff: None
	{
//		text = "Leave periscope depth";
	};
	class UAVTerminalOpen: None
	{
//		text = "Open UAV Terminal";
	};
	class UAVTerminalMakeConnection: None
	{
//		text = "Connect terminal to UAV";
	};
	class UAVTerminalReleaseConnection: None
	{
//		text = "Disconnect terminal from UAV";
	};
	class UAVTerminalHackConnection: None
	{
//		text = "Hack UAV";
	};
	class BackFromUAV: None
	{
//		text = "Release UAV controls";
	};
	class SwitchToUAVDriver: None
	{
//		text = "Take UAV controls";
	};
	class SwitchToUAVGunner: None
	{
//		text = "Take UAV turret controls";
	};
	class ListRightVehicleDisplay: None
	{
//		showWindow = 0;
//		shortcut = "ListRightVehicleDisplay";
//		text = "Right Panel next";
//		hideActions[] = {"ListRightVehicleDisplay"};
	};
	class ListLeftVehicleDisplay: ListRightVehicleDisplay
	{
//		showWindow = 0;
//		show = 0;
//		shortcut = "ListLeftVehicleDisplay";
//		text = "Left Panel next";
//		hideActions[] = {"ListLeftVehicleDisplay"};
	};
	class ListPrevRightVehicleDisplay: None
	{
//		showWindow = 0;
//		show = 0;
//		shortcut = "ListPrevRightVehicleDisplay";
//		text = "Right Panel prev.";
//		hideActions[] = {"ListPrevRightVehicleDisplay"};
	};
	class ListPrevLeftVehicleDisplay: ListPrevRightVehicleDisplay
	{
//		showWindow = 0;
//		shortcut = "ListPrevLeftVehicleDisplay";
//		text = "Left Panel prev.";
//		hideActions[] = {"ListPrevLeftVehicleDisplay"};
	};
	class CloseRightVehicleDisplay: None
	{
//		showWindow = 0;
//		show = 0;
//		shortcut = "CloseRightVehicleDisplay";
//		text = "Close Right Panel";
//		hideActions[] = {"CloseRightVehicleDisplay"};
	};
	class CloseLeftVehicleDisplay: CloseRightVehicleDisplay
	{
//		showWindow = 0;
//		show = 0;
//		shortcut = "CloseLeftVehicleDisplay";
//		text = "Close Left Panel";
//		hideActions[] = {"CloseLeftVehicleDisplay"};
	};
	class NextModeRightVehicleDisplay: None
	{
//		shortcut = "NextModeRightVehicleDisplay";
//		text = "Right Panel mode";
//		showWindow = 0;
//		hideActions[] = {"NextModeRightVehicleDisplay"};
	};
	class NextModeLeftVehicleDisplay: NextModeRightVehicleDisplay
	{
//		showWindow = 0;
//		show = 0;
//		shortcut = "NextModeLeftVehicleDisplay";
//		text = "Left Panel mode";
//		hideActions[] = {"NextModeLeftVehicleDisplay"};
	};
	class Sleep: None
	{
//		text = "Sleep";
	};
	class WakeUp: None
	{
//		text = "WakeUp";
	};
	class UnmountItem: None
	{
//		showWindow = 1;
//		text = "Take it off";
	};
	class MountItem: None
	{
//		showWindow = 1;
//		text = "Put it on";
	};
	class DropItem: None
	{
//		showWindow = 1;
//		text = "Drop it";
	};
	class TakeItem: None
	{
//		showWindow = 1;
//		text = "Take %1";
	};
	class UnloadMagazine: None
	{
//		showWindow = 1;
//		text = "Unload magazine";
	};
	class ChangeUniformWithBody: None
	{
//		showWindow = 0;
//		text = "Change uniform from body";
	};
	class DropItemFromBody: None
	{
//		showWindow = 0;
//		text = "Drop item from body";
	};
	class TakeItemFromBody: None
	{
//		showWindow = 0;
//		text = "Take item from body";
	};
	class ChangeBackpackFromBackpack: None
	{
//		showWindow = 0;
//		text = "Change backpack from backpack";
	};
	class TakeWeaponFromBody: None
	{
//		showWindow = 0;
//		text = "Take weapon from body";
	};
	class TakeBackpackFromBody: None
	{
//		showWindow = 0;
//		text = "Take backpack from body";
	};
	class UnmountUniformItem: None
	{
//		showWindow = 0;
//		text = "Take off uniform";
	};
	class MountUniformItem: None
	{
//		showWindow = 0;
//		text = "Put on uniform";
	};
	class MountWeaponFromInv: None
	{
//		showWindow = 1;
//		text = "Take weapon from inventory";
	};
	class UnmountWeaponToInv: None
	{
//		showWindow = 1;
//		text = "Take weapon to inventory";
	};
	class OpenParachuteSteerable: None
	{
//		text = "Open steerable parachute";
	};
	class OpenParachuteNonSteerable: None
	{
//		text = "Open non-steerable parachute";
	};
	class ActivateBreathingBomb: None
	{
//		text = "Activate oxygen tube";
	};
	class DeactivateBreathingBomb: None
	{
//		text = "Deactivate oxygen tube";
	};
	class PatchSoldier: None
	{
//		text = "Provide help";
//		showWindow = 1;
	};
	class HealSoldierSelf: None
	{
//		text = "Treat yourself";
//		showWindow = 1;
	};
	class AIAssemble: None
	{
//		showWindow = 0;
//		text = "Assemble %1";
	};
	class PutInGunner: None
	{
//		text = "Load In Gunner";
	};
	class PutInCommander: None
	{
//		text = "Load In Commander";
	};
	class PutInTurret: None
	{
//		text = "Load In Turret";
	};
	class UnloadFromDriver: None
	{
//		text = "Unload From Driver";
	};
	class UnloadFromPilot: None
	{
//		text = "Unload From Pilot";
	};
	class UnloadFromCargo: None
	{
//		text = "Unload From Cargo";
	};
	class UnloadFromCommander: None
	{
//		text = "Unload From Commander";
	};
	class UnloadFromGunner: None
	{
//		text = "Unload From Gunner";
	};
	class UnloadFromTurret: None
	{
//		text = "Unload From Turret";
	};
	class HealBleedingOnly: None
	{
//		text = "Staunch bleeding";
	};
	class HealBleedingSelfOnly: None
	{
//		text = "Staunch your bleeding";
	};
	class HealSoldierAuto: None
	{
//		text = "Heal soldier";
	};
	class HealBleedingAuto: None
	{
//		text = "Staunch bleeding";
	};
	class ActivateFins: None
	{
//		text = "Activate fins";
	};
	class DeactivateFins: None
	{
//		text = "Deactivate fins";
	};
	class BatteriesOn: None
	{
//		text = "Batteries on";
//		showIn3D = 87;
//		showWindow = 1;
	};
	class BatteriesOff: BatteriesOn
	{
//		text = "Batteries off";
	};
	class APUOn: None
	{
//		showIn3D = 87;
//		showWindow = 1;
//		text = "APU on";
	};
	class APUOff: APUOn
	{
//		text = "APU off";
	};
	class StarterOn1: None
	{
//		text = "Starter on (engine 1)";
//		showIn3D = 87;
//		showWindow = 1;
	};
	class StarterOff1: StarterOn1
	{
//		text = "Starter off (engine 1)";
	};
	class StarterOn2: None
	{
//		text = "Starter on (engine 2)";
//		showIn3D = 87;
//		showWindow = 1;
	};
	class StarterOff2: StarterOn2
	{
//		text = "Starter off (engine 2)";
	};
	class StarterOn3: None
	{
//		text = "Starter on (engine 3)";
//		showIn3D = 87;
//		showWindow = 1;
	};
	class StarterOff3: StarterOn3
	{
//		text = "Starter off (engine 3)";
	};
	class ThrottleOff1: None
	{
//		text = "Throttle closed (engine 1)";
//		showIn3D = 87;
//		showWindow = 1;
	};
	class ThrottleIdle1: ThrottleOff1
	{
//		text = "Throttle idle (engine 1)";
	};
	class ThrottleFull1: ThrottleOff1
	{
//		text = "Throttle full (engine 1)";
	};
	class ThrottleOff2: None
	{
//		text = "Throttle closed (engine 2)";
//		showIn3D = 87;
//		showWindow = 1;
	};
	class ThrottleIdle2: ThrottleOff2
	{
//		text = "Throttle idle (engine 2)";
	};
	class ThrottleFull2: ThrottleOff2
	{
//		text = "Throttle full (engine 2)";
	};
	class ThrottleOff3: None
	{
//		text = "Throttle closed (engine 3)";
//		showIn3D = 87;
//		showWindow = 1;
	};
	class ThrottleIdle3: ThrottleOff3
	{
//		text = "Throttle idle (engine 3)";
	};
	class ThrottleFull3: ThrottleOff3
	{
//		text = "Throttle full (engine 3)";
	};
	class RotorBrakeOn: None
	{
//		text = "Rotor brake on";
//		showIn3D = 87;
//		showWindow = 0;
	};
	class RotorBrakeOff: RotorBrakeOn
	{
//		text = "Rotor brake off";
	};
};
