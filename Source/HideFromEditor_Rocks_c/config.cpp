class CfgPatches
{
	class Optional_HideFromEditor_Rocks_c
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Data_F_Loadorder","A3_Data_F_Curator_Loadorder","A3_Data_F_Kart_Loadorder","A3_Data_F_Bootcamp_Loadorder","A3_Data_F_Heli_Loadorder","A3_Data_F_Mark_Loadorder","A3_Data_F_Exp_A_Loadorder","A3_Data_F_Exp_B_Loadorder","A3_Data_F_Exp_Loadorder","A3_Data_F_Jets_Loadorder","A3_Data_F_Argo_Loadorder","A3_Data_F_Patrol_Loadorder","A3_Data_F_Orange_Loadorder","A3_Data_F_Tacops_Loadorder","A3_Data_F_Tank_Loadorder"};
		version = "2019-03-28";
	};
};
class CfgVehicles
{
	class Rocks_base_F;

	class Land_Small_Stone_01_F: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_Small_Stone_02_F: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_Stone_big_F: Rocks_base_F
	{
//		scope = 1;
	};
	class Land_Stone_medium_F: Rocks_base_F
	{
//		scope = 1;
	};
	class Land_Stone_small_F: Rocks_base_F
	{
//		scope = 1;
	};
	class Land_StoneSharp_big: Rocks_base_F
	{
//		scope = 1;
	};
	class Land_StoneSharp_medium: Rocks_base_F
	{
//		scope = 1;
	};
	class Land_StoneSharp_small: Rocks_base_F
	{
//		scope = 1;
	};
	class Land_StoneSharp_wall: Rocks_base_F
	{
//		scope = 1;
	};
	class Land_BluntRock_apart: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_BluntRock_monolith: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_BluntRock_spike: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_BluntRock_wallH: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_BluntRock_wallV: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_BluntStone_01: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_BluntStone_02: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_BluntStone_03: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_BluntStones_erosion: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_SharpRock_apart: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_SharpRock_monolith: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_SharpRock_spike: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_SharpRock_wallH: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_SharpRock_wallV: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_SharpStone_01: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_SharpStone_02: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_SharpStone_03: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_SharpStones_erosion: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_Stone_big_W: Rocks_base_F
	{
//		scope = 1;
	};
	class Land_Stone_medium_W: Rocks_base_F
	{
//		scope = 1;
	};
	class Land_Stone_small_W: Rocks_base_F
	{
//		scope = 1;
	};
	class Land_StoneSharp_big_W: Rocks_base_F
	{
//		scope = 1;
	};
	class Land_StoneSharp_medium_W: Rocks_base_F
	{
//		scope = 1;
	};
	class Land_StoneSharp_small_W: Rocks_base_F
	{
//		scope = 1;
	};
	class Land_StoneSharp_Wall_W: Rocks_base_F
	{
//		scope = 1;
	};
	class Land_W_sharpRock_apart: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_W_sharpRock_monolith: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_W_sharpRock_spike: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_W_sharpRock_wallH: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_W_sharpRock_wallV: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_W_sharpStone_01: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_W_sharpStone_02: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_W_sharpStone_03: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_W_sharpStones_erosion: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_Cliff_boulder_F: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_Cliff_peak_F: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_Cliff_stone_big_F: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_Cliff_stone_medium_F: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_Cliff_stone_small_F: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_Cliff_stoneCluster_F: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_Cliff_surfaceMine_F: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_Cliff_wall_long_F: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_Cliff_wall_round_F: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_Cliff_wall_tall_F: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_Lavaboulder_01_F: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_Lavaboulder_02_F: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_Lavaboulder_03_F: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_Lavaboulder_04_F: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_LavaStone_big_F: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_LavaStone_small_F: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_LavaStoneCluster_large_F: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_LavaStoneCluster_small_F: Rocks_base_F
	{
		scope = 1;//2
	class Land_Limestone_01_apart_F: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_Limestone_01_monolith_F: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_Limestone_01_spike_F: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_Limestone_01_wallH_F: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_Limestone_01_wallV_F: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_Limestone_01_01_F: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_Limestone_01_02_F: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_Limestone_01_03_F: Rocks_base_F
	{
		scope = 1;//2
	};
	class Land_Limestone_01_erosion_F: Rocks_base_F
	{
		scope = 1;//2
	};
	};
	class Land_Limestone_01_apart_F: Rocks_base_F
	{
		scope = 1;//2;
	};
	class Land_Limestone_01_monolith_F: Rocks_base_F
	{
		scope = 1;//2;
	};
	class Land_Limestone_01_spike_F: Rocks_base_F
	{
		scope = 1;//2;
	};
	class Land_Limestone_01_wallH_F: Rocks_base_F
	{
		scope = 1;//2;
	};
	class Land_Limestone_01_wallV_F: Rocks_base_F
	{
		scope = 1;//2;
	};
	class Land_Limestone_01_01_F: Rocks_base_F
	{
		scope = 1;//2;
	};
	class Land_Limestone_01_02_F: Rocks_base_F
	{
		scope = 1;//2;
	};
	class Land_Limestone_01_03_F: Rocks_base_F
	{
		scope = 1;//2;
	};
	class Land_Limestone_01_erosion_F: Rocks_base_F
	{
		scope = 1;//2;
	};
};
